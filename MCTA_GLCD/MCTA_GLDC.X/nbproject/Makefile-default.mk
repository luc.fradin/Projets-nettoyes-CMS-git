#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=cof
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/MCTA_GLDC.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/MCTA_GLDC.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../project/ecrans.c ../project/ftoa.c ../project/GLCD.c ../project/inituc.c ../project/delay.c ../project/eeprom_i2c.c ../project/analog.c ../project/i2cds.c ../project/rtc.c ../project/clock.c ../project/TESTS.c ../project/main.c ../project/mesures.c ../project/memo.c ../project/rs232.c ../project/codage.c ../project/eeprom.c ../project/lect_mem.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1989816886/ecrans.o ${OBJECTDIR}/_ext/1989816886/ftoa.o ${OBJECTDIR}/_ext/1989816886/GLCD.o ${OBJECTDIR}/_ext/1989816886/inituc.o ${OBJECTDIR}/_ext/1989816886/delay.o ${OBJECTDIR}/_ext/1989816886/eeprom_i2c.o ${OBJECTDIR}/_ext/1989816886/analog.o ${OBJECTDIR}/_ext/1989816886/i2cds.o ${OBJECTDIR}/_ext/1989816886/rtc.o ${OBJECTDIR}/_ext/1989816886/clock.o ${OBJECTDIR}/_ext/1989816886/TESTS.o ${OBJECTDIR}/_ext/1989816886/main.o ${OBJECTDIR}/_ext/1989816886/mesures.o ${OBJECTDIR}/_ext/1989816886/memo.o ${OBJECTDIR}/_ext/1989816886/rs232.o ${OBJECTDIR}/_ext/1989816886/codage.o ${OBJECTDIR}/_ext/1989816886/eeprom.o ${OBJECTDIR}/_ext/1989816886/lect_mem.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1989816886/ecrans.o.d ${OBJECTDIR}/_ext/1989816886/ftoa.o.d ${OBJECTDIR}/_ext/1989816886/GLCD.o.d ${OBJECTDIR}/_ext/1989816886/inituc.o.d ${OBJECTDIR}/_ext/1989816886/delay.o.d ${OBJECTDIR}/_ext/1989816886/eeprom_i2c.o.d ${OBJECTDIR}/_ext/1989816886/analog.o.d ${OBJECTDIR}/_ext/1989816886/i2cds.o.d ${OBJECTDIR}/_ext/1989816886/rtc.o.d ${OBJECTDIR}/_ext/1989816886/clock.o.d ${OBJECTDIR}/_ext/1989816886/TESTS.o.d ${OBJECTDIR}/_ext/1989816886/main.o.d ${OBJECTDIR}/_ext/1989816886/mesures.o.d ${OBJECTDIR}/_ext/1989816886/memo.o.d ${OBJECTDIR}/_ext/1989816886/rs232.o.d ${OBJECTDIR}/_ext/1989816886/codage.o.d ${OBJECTDIR}/_ext/1989816886/eeprom.o.d ${OBJECTDIR}/_ext/1989816886/lect_mem.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1989816886/ecrans.o ${OBJECTDIR}/_ext/1989816886/ftoa.o ${OBJECTDIR}/_ext/1989816886/GLCD.o ${OBJECTDIR}/_ext/1989816886/inituc.o ${OBJECTDIR}/_ext/1989816886/delay.o ${OBJECTDIR}/_ext/1989816886/eeprom_i2c.o ${OBJECTDIR}/_ext/1989816886/analog.o ${OBJECTDIR}/_ext/1989816886/i2cds.o ${OBJECTDIR}/_ext/1989816886/rtc.o ${OBJECTDIR}/_ext/1989816886/clock.o ${OBJECTDIR}/_ext/1989816886/TESTS.o ${OBJECTDIR}/_ext/1989816886/main.o ${OBJECTDIR}/_ext/1989816886/mesures.o ${OBJECTDIR}/_ext/1989816886/memo.o ${OBJECTDIR}/_ext/1989816886/rs232.o ${OBJECTDIR}/_ext/1989816886/codage.o ${OBJECTDIR}/_ext/1989816886/eeprom.o ${OBJECTDIR}/_ext/1989816886/lect_mem.o

# Source Files
SOURCEFILES=../project/ecrans.c ../project/ftoa.c ../project/GLCD.c ../project/inituc.c ../project/delay.c ../project/eeprom_i2c.c ../project/analog.c ../project/i2cds.c ../project/rtc.c ../project/clock.c ../project/TESTS.c ../project/main.c ../project/mesures.c ../project/memo.c ../project/rs232.c ../project/codage.c ../project/eeprom.c ../project/lect_mem.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/MCTA_GLDC.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F8723
MP_PROCESSOR_OPTION_LD=18f8723
MP_LINKER_DEBUG_OPTION= -u_DEBUGCODESTART=0x1fd30 -u_DEBUGCODELEN=0x2d0 -u_DEBUGDATASTART=0xef4 -u_DEBUGDATALEN=0xb
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1989816886/ecrans.o: ../project/ecrans.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/ecrans.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/ecrans.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/ecrans.o   ../project/ecrans.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/ecrans.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/ecrans.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/ftoa.o: ../project/ftoa.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/ftoa.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/ftoa.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/ftoa.o   ../project/ftoa.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/ftoa.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/ftoa.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/GLCD.o: ../project/GLCD.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/GLCD.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/GLCD.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/GLCD.o   ../project/GLCD.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/GLCD.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/GLCD.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/inituc.o: ../project/inituc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/inituc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/inituc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/inituc.o   ../project/inituc.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/inituc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/inituc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/delay.o: ../project/delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/delay.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/delay.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/delay.o   ../project/delay.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/delay.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/delay.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/eeprom_i2c.o: ../project/eeprom_i2c.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/eeprom_i2c.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/eeprom_i2c.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/eeprom_i2c.o   ../project/eeprom_i2c.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/eeprom_i2c.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/eeprom_i2c.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/analog.o: ../project/analog.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/analog.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/analog.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/analog.o   ../project/analog.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/analog.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/analog.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/i2cds.o: ../project/i2cds.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/i2cds.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/i2cds.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/i2cds.o   ../project/i2cds.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/i2cds.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/i2cds.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/rtc.o: ../project/rtc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/rtc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/rtc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/rtc.o   ../project/rtc.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/rtc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/rtc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/clock.o: ../project/clock.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/clock.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/clock.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/clock.o   ../project/clock.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/clock.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/clock.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/TESTS.o: ../project/TESTS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/TESTS.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/TESTS.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/TESTS.o   ../project/TESTS.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/TESTS.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/TESTS.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/main.o: ../project/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/main.o   ../project/main.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/mesures.o: ../project/mesures.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/mesures.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/mesures.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/mesures.o   ../project/mesures.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/mesures.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/mesures.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/memo.o: ../project/memo.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/memo.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/memo.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/memo.o   ../project/memo.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/memo.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/memo.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/rs232.o: ../project/rs232.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/rs232.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/rs232.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/rs232.o   ../project/rs232.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/rs232.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/rs232.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/codage.o: ../project/codage.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/codage.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/codage.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/codage.o   ../project/codage.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/codage.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/codage.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/eeprom.o: ../project/eeprom.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/eeprom.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/eeprom.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/eeprom.o   ../project/eeprom.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/eeprom.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/eeprom.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/lect_mem.o: ../project/lect_mem.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/lect_mem.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/lect_mem.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/lect_mem.o   ../project/lect_mem.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/lect_mem.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/lect_mem.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
else
${OBJECTDIR}/_ext/1989816886/ecrans.o: ../project/ecrans.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/ecrans.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/ecrans.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/ecrans.o   ../project/ecrans.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/ecrans.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/ecrans.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/ftoa.o: ../project/ftoa.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/ftoa.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/ftoa.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/ftoa.o   ../project/ftoa.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/ftoa.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/ftoa.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/GLCD.o: ../project/GLCD.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/GLCD.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/GLCD.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/GLCD.o   ../project/GLCD.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/GLCD.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/GLCD.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/inituc.o: ../project/inituc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/inituc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/inituc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/inituc.o   ../project/inituc.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/inituc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/inituc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/delay.o: ../project/delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/delay.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/delay.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/delay.o   ../project/delay.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/delay.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/delay.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/eeprom_i2c.o: ../project/eeprom_i2c.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/eeprom_i2c.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/eeprom_i2c.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/eeprom_i2c.o   ../project/eeprom_i2c.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/eeprom_i2c.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/eeprom_i2c.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/analog.o: ../project/analog.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/analog.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/analog.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/analog.o   ../project/analog.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/analog.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/analog.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/i2cds.o: ../project/i2cds.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/i2cds.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/i2cds.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/i2cds.o   ../project/i2cds.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/i2cds.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/i2cds.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/rtc.o: ../project/rtc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/rtc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/rtc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/rtc.o   ../project/rtc.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/rtc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/rtc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/clock.o: ../project/clock.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/clock.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/clock.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/clock.o   ../project/clock.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/clock.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/clock.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/TESTS.o: ../project/TESTS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/TESTS.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/TESTS.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/TESTS.o   ../project/TESTS.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/TESTS.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/TESTS.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/main.o: ../project/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/main.o   ../project/main.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/mesures.o: ../project/mesures.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/mesures.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/mesures.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/mesures.o   ../project/mesures.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/mesures.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/mesures.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/memo.o: ../project/memo.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/memo.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/memo.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/memo.o   ../project/memo.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/memo.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/memo.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/rs232.o: ../project/rs232.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/rs232.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/rs232.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/rs232.o   ../project/rs232.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/rs232.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/rs232.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/codage.o: ../project/codage.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/codage.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/codage.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/codage.o   ../project/codage.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/codage.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/codage.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/eeprom.o: ../project/eeprom.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/eeprom.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/eeprom.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/eeprom.o   ../project/eeprom.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/eeprom.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/eeprom.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/lect_mem.o: ../project/lect_mem.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/lect_mem.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/lect_mem.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -I"." -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/lect_mem.o   ../project/lect_mem.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/lect_mem.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/lect_mem.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/MCTA_GLDC.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    ../project/18f8723_bigdata.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "..\project\18f8723_bigdata.lkr"  -p$(MP_PROCESSOR_OPTION_LD)  -w -x -u_DEBUG -m"$(BINDIR_)$(TARGETBASE).map" -l"../../MCC18/src/traditional/math" -l"../../mcc18/lib" -l"."  -z__MPLAB_BUILD=1  -u_CRUNTIME -z__MPLAB_DEBUG=1 -z__MPLAB_DEBUGGER_ICD3=1 $(MP_LINKER_DEBUG_OPTION) -l ${MP_CC_DIR}\\..\\lib  -o dist/${CND_CONF}/${IMAGE_TYPE}/MCTA_GLDC.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}   
else
dist/${CND_CONF}/${IMAGE_TYPE}/MCTA_GLDC.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   ../project/18f8723_bigdata.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "..\project\18f8723_bigdata.lkr"  -p$(MP_PROCESSOR_OPTION_LD)  -w  -m"$(BINDIR_)$(TARGETBASE).map" -l"../../MCC18/src/traditional/math" -l"../../mcc18/lib" -l"."  -z__MPLAB_BUILD=1  -u_CRUNTIME -l ${MP_CC_DIR}\\..\\lib  -o dist/${CND_CONF}/${IMAGE_TYPE}/MCTA_GLDC.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}   
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default
