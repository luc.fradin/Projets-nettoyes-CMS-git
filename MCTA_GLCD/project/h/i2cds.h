/*
 * Project: Digital_Clock_V2.0
 * File Name: i2c.h
 * Author: Siddharth Chandrasekaran
 * Created on July 20, 2012, 6:12 PM
 * Visit http://embedjournal.com for more codes.
*/
#ifndef I2CDS_H
#define I2CDS_H

void i2c_start(void);
void i2c_restart(void);
void i2c_device(void);
unsigned int i2c_read(void);
void i2c_write(unsigned int data);
void i2c_stop(void);

#endif