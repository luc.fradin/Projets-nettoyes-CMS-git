




void menu_principal (void);
void menu_principal_C0 (void);
void menu_principal_C1 (void);
void menu_principal_C2 (void);
void menu_principal_C3 (void);
void menu_principal_C4 (void);
void menu_principal_C5 (void);	// Param�trages

void menu_TPAD (void);
void menu_TPAD_C0 (void);
void menu_TPAD_C1 (void);
void menu_TPAD_C2 (void);
void menu_TPAD_C3 (void);

void Ecran_Constitution_complete(void);


void Scrolling_Ecran_tpa(void);
void ecran_adressables_complet (void);
void ecran_aff_TPA_complet(void);
void Affiche_complet (char tp,char ltp,char Ligne); 
void ecran_adressables_reduit (void);
void ecran_aff_TPA_reduit(void);
void Affiche_reduit (char tp,char ltp,char Ligne);
void switch_Etat(char Etat,char Ligne);
void switch_Present(char i);
void switch_Present_def(char i);
void switch_Absent(char i);

void Sauvegarde_TA_TR(void); 
void eff_valeurs_complet(void);
void Display_Num_C_Prog(void);
void Display_Num_C_Lect(void);

void ecran_groupe(void);
void aff_mes_groupe(void);


void ecran_resistifs(void);
void ecran_resistifs2(void);
void ecran_TPR_Aff_memo(void);	

void ecran_TPR_Mes(void);	
void Mesure_TPR(void);

void ecran_debitmetres(void);
void ecran_DEB_mes(void);
void Calcul_Deb (void);
void ecran_debitmetres_memo(void);

void ecran_codage(void);
void ecran_lect_mem(void);

void niveau_batt(void);
void Mesure_V_Batt (void);

void reset_leds(void);
void flip_flop_back_light(void);
void decodeH(void);
void encodeH(void);

void ecran_param(void);
void menu_param_C0(void);
void menu_param_C1(void);
void menu_param_C2(void);
void menu_param_C3(void);
void ecran_adecef(void);
void ecran_configAT(void);
void ecran_Configuration (void);


void ecran_memo_TPresistifs(void);
void ecran_memo_TPadressables(void);
void ecran_memo_Debitmetres(void); // �cran ou sont sauvegard�s voie, comment et central
void Ecran_Constitution (void);

void ecran_memoire(void);

void ecran_Raz_Memoire(unsigned char F_der_tous);
void menu_memoire_C0(void);				    // lecture memoire		 
void menu_memoire_C1(void);				    // RAZ 1 ENR.		 
void menu_memoire_C2(void);				    // RAZ TOUS ENR.		 
void Buzzer_on(void);				

void ecran_resistifs_detect_TPA(void);

void Ajuste_Gains(void);	 // AJUSTEMENT DES GAINS DE CORRECTION I_Conso I_Repos I_Modul 
void Mot_de_passe(void);					

