

unsigned char ReadEEPROM(unsigned char ad); 
void WriteEEPROM(unsigned char ad,unsigned char data); 
void eepmess(unsigned int , unsigned char *);
void Wr_eeprom_int( unsigned int adr_eeprom,  int data);
unsigned int Rd_eeprom_int( unsigned int adr_eeprom);
void Wr_eeprom_str ( unsigned int adr_eeprom,  char *string, unsigned int long_string );
void Rd_eeprom_str ( unsigned int adr_eeprom,  char *string, unsigned int long_string );
void Rd_eeprom_double( unsigned int adr_eeprom, char *data );
void Wr_eeprom_double( unsigned int adr_eeprom, char *data);
