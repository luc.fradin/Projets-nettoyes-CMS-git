#define _ADIF	PIR1bits.ADIF	// INTERRUPTION FIN CONVERSION ANALOGIQUE
#define _ADON	ADCON0bits.ADON	// VALIDE LE CAN
#define _ADGO	ADCON0bits.GO	// LANCE ACQUISITION ET CONVERSION
#define _ADDONE	ADCON0bits.DONE	// TEST SI ACQUISITION ET CONVERSION EN COURS

unsigned int acqui_ana (unsigned char );		// Acquisition de la valeur du CAN a l'adresse specifi�e	
void Mesure_V_Batt (void);								// Acquisition de la valeur CAN de la tension batterie
void Mesure_V_48v (void);
void Mesure_V_Resistif(void);	// Acquisition de la valeur CAN de la tension ligne AVal capteur  avec un gain de 8.50
void Mesure_V_Resistif2(void);	// Acquisition de la valeur CAN de la tension ligne AVal capteur  avec un gain de 4.75
//void Mesure_V_48v (float V_48_Calc);								// Acquisition de la valeur CAN de la tension du convertisseur 48V
void Mesure_I_Modul (void);							// Acquisition de la valeur CAN du courant de modulation
void Mesure_I_Conso(void);								// Acquisition de la valeur CAN du courant de consommation
void Mesure_I_Repos(void);								// Acquisition de la valeur CAN du courant de repos
void Mesure_V_Ligne_AM(void);								// Acquisition de la valeur CAN de la tension ligne AMont capteur
void Mesure_V_Ligne_AV(void);								// Acquisition de la valeur CAN de la tension ligne AVal capteur
//void Mesure_V_Sinus_AV (void);							// Acquisition de la valeur CAN de la tension de r�ponse Sinus	
unsigned int acqui_ana_16 (unsigned char adcon ); 
unsigned int acqui_ana_4 (unsigned char adcon );
unsigned int acqui_ana_2 (unsigned char adcon );
unsigned int acqui_ana_16_plus (unsigned char adcon );

/*int Calcul_V_batt (unsigned int);	//Calcul V-Batt
int Calcul_V_48v (void);			//Calcul V_48v
int Calcul_I_modul (void);			// Calcul courant de modulation
int Calcul_I_conso (void);			// Calcul courant de consommation
int Calcul_V_ligne(void);			// Calcul tension de ligne
int Calcul_V_sinus (void);			// Calcul sinus*/





