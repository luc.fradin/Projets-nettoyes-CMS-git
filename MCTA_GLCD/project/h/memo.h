
void Save_tpa_i2c (void);
void Lect_tpa_i2c (void);
void Lect_MesEnCours_tpa_i2c(unsigned int INTValue);
void Save_MesEnCours_tpa_i2c(unsigned int INTValue);



void Test_eeprom_i2c_int (void);  // pour tester le fonctionnement de l'eeprom



/**********************************************************************
 * Fonction extern.
 **********************************************************************/

extern void Wr_eeprom_i2c_str ( unsigned int adr_eeprom,  char *string, unsigned int long_string );
extern void Rd_eeprom_i2c_str ( unsigned int adr_eeprom,  char *string, unsigned int long_string );

extern void Wr_eeprom_i2c_char( unsigned int adr_eeprom,  char data_char);
extern unsigned char Rd_eeprom_i2c_char( unsigned int adr_eeprom);

extern void Wr_eeprom_i2c_int( unsigned int adr_eeprom,  int data);
extern unsigned int Rd_eeprom_i2c_int( unsigned int adr_eeprom);

extern void Rd_eeprom_i2c_double( unsigned int adr_eeprom, char *data);extern void Wr_eeprom_i2c_double( unsigned int adr_eeprom, char *data);






