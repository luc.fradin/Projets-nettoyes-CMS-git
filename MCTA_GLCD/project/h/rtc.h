/*
 * Project: Digital_Clock_V2.0
 * File Name: rtc.h
 * Author: Siddharth Chandrasekaran
 * Created on July 20, 2012, 6:12 PM
 * Visit http://embedjournal.com for more codes.
*/
#ifndef RTC_H
#define RTC_H

#define RTC_ADR 0b11010000
#define I2C_READ 1
#define I2C_WRITE 0

void reset_time(unsigned int SetHeures,unsigned int SetMinutes,unsigned int SetSecondes,unsigned int Jour,unsigned int SetDate,unsigned int SetMois,unsigned int SetAnnee);       // resets the time-keeping register
void disp_frame();
unsigned int get_time(unsigned int address);
void set_time(unsigned int address, unsigned int x);

#endif