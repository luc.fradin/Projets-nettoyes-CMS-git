
#ifndef __DELAY_H

/**********  DECLARATION DES SOUS PROGRAMMES ********/

void DELAY_SEC(int sec);//avec while(!(INTCONbits.TMR0IF));
void DELAY_SW_1SEC(void);//sans while(!(INTCONbits.TMR0IF)) � rajouter dans le prgramme!!!;
void DELAY_SW_2SEC(void);//sans while(!(INTCONbits.TMR0IF)) � rajouter dans le prgramme!!!;
void DELAY_MS(int ms);//avec while(!(INTCONbits.TMR0IF));
void DELAY_100US(int us);//avec while(!(INTCONbits.TMR0IF));
void DELAY_125MS(int ms);//avec while(!(INTCONbits.TMR0IF));
void DELAY_SW_250MS(void);//sans while(!(INTCONbits.TMR0IF)) � rajouter dans le prgramme!!!;
void DELAY_500MS(int ms);//avec while(!(INTCONbits.TMR0IF));
void InitTimer0(void);
#endif
