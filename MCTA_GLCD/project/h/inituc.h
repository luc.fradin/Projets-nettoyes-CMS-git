
#ifndef __INITUC_H

/************************ DEFINE  ***************************/

// "PORTXbits" pour lecture d'un port
// "LATXbits" pour �criture sur un port

// LECTURE CLAVIER
#define 	touche_BP0	PORTGbits.RG0		//entr�e BP0		OKv2
#define 	touche_BP1	PORTFbits.RF2		//entr�e BP1		OKv2
#define 	touche_BP2	PORTFbits.RF3		//entr�e BP2		OKv2
#define 	touche_BP3	PORTFbits.RF4		//entr�e BP3		OKv2
#define 	touche_BP4	PORTFbits.RF5		//entr�e BP4		OKv2
#define 	touche_BP5	PORTFbits.RF6		//entr�e BP5		OKv2
#define 	touche_BP6	PORTFbits.RF7		//entr�e BP6		OKv2

// ECRITURE �tat LEDs
#define 	Led_jaune 	LATEbits.LATE3		//sortie DL2		OKv2
#define 	Led_verte 	LATEbits.LATE4		//sortie DL3		OKv2
#define 	Led_rouge 	LATEbits.LATE5		//sortie DL4		OKv2


// DEFINE pour BCTA et transtest
#define		DECH_LIGN	LATHbits.LATH0		//(bcta RC0) D�charge ligne capteur		OKv2
#define		HT_ON		LATHbits.LATH4		//(bcta RC4) mise en route de la HT		OKv2
#define		PONT_ON		LATHbits.LATH5		//(bcta RC3) -39v							OKv2
#define		UN_CPT		LATHbits.LATH6		//(bcta RC6) 60v/80v						OKv2
#define		CPT_ON		LATHbits.LATH7		//(bcta RC7) valide tension sur capteur		OKv2 
#define		REP_CPT		PORTBbits.RB1		// reponse en courant du capteur		OKv2			
#define		Synchro1s	PORTBbits.RB3		// entr�e synchro de l'horloge du ds1307			



#define 	IMP_BCTA	LATJbits.LATJ2		// commande impulsion relais s�lection bcta				OKv2
#define 	IMP_TRANS	LATJbits.LATJ3		// commande impulsion relais s�lection transtest		OKv2
#define 	Buzzer		LATJbits.LATJ4		// Cde Buzzer											OKv2
#define 	Back_light	LATJbits.LATJ5		// backlight afficheur									OKv2
#define		Cde_Off		LATJbits.LATJ6		// Arr�t automatique du MCTA							OKv2
#define		pt1			LATEbits.LATE6		// point test 1		OKv2
#define		pt2			LATEbits.LATE7		// point test 2		OKv2
#define		pt3			LATJbits.LATJ7		// point test 3		OKv2






/**********  DECLARATION DES SOUS PROGRAMMES ********/

void init_uc (void);  //initialisation du microcontrolleur

#endif
