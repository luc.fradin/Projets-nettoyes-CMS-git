
/* FONCTION POUR CONVERTIR UN FLOAT EN STRING */

/************************ INCLUDE *****************************/
#include <p18f8723.h>

/**************** DECLARATION DES SOUS PROGRAMMES *************/

void ftoa(float f,char* String,char Places){
long power = 0;
char n = 0,i = 0;
    power=1;
    while(power*10<=f)
        power*=10;
    while(power>=1){
        n=(char)(f/power);
        f-=n*power;
        power/=10;
        *String++=n+'0';
    }
    *String++='.';
    for(i=0;i<Places;i++){
        f*=10;
        n=(char)f;
        f-=n;
        *String++=n+'0';
    }
    *String=0;
}

