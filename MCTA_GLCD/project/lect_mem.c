
#include <inituc.h>
#include <ecrans.h>
#include <eeprom.h>
#include <GLCD.h>
#include <delay.h>						/* Defs DELAY functions */
#include <memo.h>


//prototype
void gestion_lecture_memoire (void);
void Gestion_Enreg_Ecriture(void);
void Increment_adresse(void);
void Decrement_adresse(void);
void Raffraichissement_Aff(void);

extern  void Init_Tab_tpa(void);  //reset tableau de mesures des tpa

extern unsigned char F_ecran_adres;
extern unsigned char Num_Capteur ;
extern unsigned char Num_Ligne;
extern unsigned char Nb_Capteurs_max;
extern unsigned char Tab_Code[17];
extern unsigned int Tab_I_Repos[17];	// lecture du courant de repos pour voir la fin de la mesure enregistr�e		  
extern unsigned char Tab_Central[14];
extern unsigned char Tab_Etat[17];			//17 octets  

extern unsigned int Offset_Lect; // adresse de l'enregistrement en lecture eeprom i2c
extern unsigned int Der_Enreg;  // adresse du dernier enregistrement en EEprom i2c
unsigned int Aff_Enreg; // adresse de l'enregistrement affich�
extern unsigned int No_der_enreg;
extern unsigned char F_der_tous;  			//  flag de s�lection du dernier enregistrement ou, tous les enregistrements � effacer

extern unsigned char Tab_VoieRobTete[10];	//10 octets 		VOIE 3 + ROB 2 + TETE 5	 (ROB 2 + TETE 5 devient Comment)

int tpa;

extern unsigned char F_sauve_mes_tpA; 		// flag pour m�mo sauvegarde valid�e des mesures des tp adressables
extern unsigned char F_sauve_mes_tpR; 		// flag pour m�mo sauvegarde valid�e des mesures des tp r�sistifs
extern unsigned char F_sauve_mes_Deb;
unsigned int modif_aff; //si changement de numero d'enregistrement
extern unsigned char Val_Voie; // valeur n� de voie TPA pour la constitution
//*******************************************************************************************************************************************
//*******************************************************************************************************************************************


void gestion_lecture_memoire()
{
unsigned int modif_aff; //si changement de numero d'enregistrement
unsigned char i;

				modif_aff=1;
				Aff_Enreg = No_der_enreg;
				Init_Tab_tpa();  // reset des tableaux            
				Nb_Capteurs_max = 17;
				Num_Capteur =0;
				Num_Ligne = 0;

				 

		while (!touche_BP6 ); // attend touche relach�e

				Raffraichissement_Aff();
//				Scrolling_Ecran_tpa();

//				Lect_tpa_i2c ();   //LECTURE EEPROM-I2C

   	while (touche_BP5 !=0 && No_der_enreg>0)
	{
// �cran adressable
				i=0;
				SetPos(0,8);								// Positionner le curseur
				for (i=0;i<=13;i++)
				{
				if(Tab_Central[i]!=0x20){  // = espace
				PutChar(Tab_Central[i]);
						 	}				
				}	

//ATTENTION � partir de la version 1.26   VOIE 2octets + ROB 2octets + TETE 6octets devient VOIE 3octets + Obs 7octets pour les tableaux d'enregistrement et de sauvegarde
//Le nom dans le tableau reste  Tab_VoieRobTete[10];
				SetPos(0,56);								// Positionner le curseur
				PutMessage("Voie");		//  Pour affichage "Voies" ecran memo
				SetPos(20,56);								// Positionner le curseur

//				Tab_VoieRobTete[0] est maintenant un nombre de 1 a 100
				Val_Voie=Tab_VoieRobTete[0];
				SetPos(20,56);
  				PutMessage("     "); 					
				displayNum (20,56,Val_Voie);					
				DELAY_MS (10);



//				if (Tab_VoieRobTete[0]>=48 && Tab_VoieRobTete[0]<=57)PutChar(Tab_VoieRobTete[0]);
//				if (Tab_VoieRobTete[1]>=48 && Tab_VoieRobTete[1]<=57)PutChar(Tab_VoieRobTete[1]);	 
//				if (Tab_VoieRobTete[2]>=48 && Tab_VoieRobTete[2]<=57)PutChar(Tab_VoieRobTete[2]);	 // erreur ? 2 � la place de [1] modif du 20/02/2016


			//modif du 20/02/2016
				SetPos(40,56);	
				for (i=3;i<=9;i++)    //Affichage de comment: 			
				{
				PutChar(Tab_VoieRobTete[i]);
				}
			//fin modif du 20/02/2016


				SetPos(90,56);								// Positionner le curseur
				PutMessage("Enr:");		// 
				displayNum (107,56,Aff_Enreg);

			// choix affichage �cran complet ou reduit avec les touches < ou >  POUR LES TPA
		if(F_sauve_mes_tpA==1)  		// flag pour m�mo sauvegarde valid�e des mesures des tp adressables
			{

			if ((touche_BP2 == 0 || touche_BP1 == 0) && touche_BP4 == 1 && touche_BP6 == 1)
				{
				ClearScreen ();								// Effacement de tout l'�cran
				if (touche_BP2 == 0)  // si touche > alors �crans complet
					{ 
//				ClearScreen ();								// Effacement de tout l'�cran
				    F_ecran_adres=1;						// affichage �cran complet (bit = 1)
				ecran_adressables_complet();
					}
				Scrolling_Ecran_tpa();
				while (touche_BP2 == 0);

				if (touche_BP1 == 0)  // si touche < alors �crans r�duit
					{ 
//				ClearScreen ();								// Effacement de tout l'�cran
		     	    F_ecran_adres=0;						// affichage �cran r�duit (bit = 0)
				ecran_adressables_reduit();
					}
				Scrolling_Ecran_tpa();
				while (touche_BP1== 0 );           
				}

//				Scrolling_Ecran_tpa(); //� ajouter si on supprime la ligne dessus =>	Init_Tab_tpa();  // reset des tableaux  !� supprimer pour r�affichage valeurs!

			if (touche_BP6!=0){ 
				if (touche_BP0==0 && Num_Ligne>3 )    	// scrolling vertical si > 3 mesures et si scrutation termin�e	&& F_sup3mes_tpa
					{
					Num_Ligne--;
 					DELAY_MS (10);
					}
				if (touche_BP3==0 && Num_Ligne<Nb_Capteurs_max && Num_Ligne<16)		// scrolling vertical si <16 mesures et si scrutation termin�e   (Nb_Capteurs_max-1)
					{
					Num_Ligne++;
	 				DELAY_MS (10);
					}
				if (touche_BP0==0||touche_BP3==0)	
					{
					Scrolling_Ecran_tpa();
	 				DELAY_MS (150);
//				while (touche_BP3== 0 || touche_BP0== 0);           
					}
				}	
// *************FIN    scrolling vertical si > 3 mesures ET SI TPA***************

			}  // fin 	if(F_sauve_mes_tpA==1)

		else if(F_sauve_mes_tpR==1 && modif_aff==1)
			{
			ecran_TPR_Aff_memo();
			modif_aff=0;
			}
		else if(F_sauve_mes_Deb==1 && modif_aff==1) 
			{
			ecran_debitmetres_memo();
			modif_aff=0;
			}
		


		
// *****************decrementation incrementation adresse memoire******
		if (touche_BP6==0)   // while devient if 	modif du 21/02/2016  
			{
				if (touche_BP1==0)    // 3	
					{
				Decrement_adresse();
				modif_aff=1;
				Raffraichissement_Aff();
// 				modif_aff=0;
				DELAY_MS (20);
				while (touche_BP3== 0);
 				DELAY_MS (20);
					}

				if (touche_BP2==0)	//0
					{
				Increment_adresse();
				modif_aff=1;
				Raffraichissement_Aff();
// 				modif_aff=0;
  			DELAY_MS (20);
				while (touche_BP0== 0 );           
 				DELAY_MS (20);
					} 
			}



//**************** effacement dernier enregistrement ****************
	while (touche_BP4==0)  
		{
		if (touche_BP6==0)
			{
				ClearScreen ();			// Effacement de tout l'�cran
				F_der_tous=0;   // SI EFFACEMENT DU DERNIER ENREGISTREMENT
				ecran_Raz_Memoire(F_der_tous); // pour effacer les enregistrements 
				while (touche_BP6==0); // attente touche relach�e
				Decrement_adresse();
				modif_aff=1;
				Raffraichissement_Aff();
				if (No_der_enreg!=0)
					{

				Aff_Enreg = No_der_enreg;
				SetPos(90,56);								// Positionner le curseur
				PutMessage("Enr:");		// 
				displayNum (107,56,Aff_Enreg);
					}
				else
					{
//				DELAY_MS (500);
				ClearScreen ();								// Effacement de tout l'�cran
 				SetPos(10,24);
 		 		PutMessage("Aucun Enregistrement!");
				DELAY_MS (1000);
					} 
			}  	// FIN if (touche_BP6==0)
		
		}  	// FIN 	while (touche_BP4==0) 

//****************FIN effacement dernier enregistrement ****************




	}//	while (touche_BP5)

} // FIN de  gestion_lecture_memoire()




void Raffraichissement_Aff()
{
unsigned char i=0;
//				Aff_Enreg = No_der_enreg;
				Init_Tab_tpa();  // reset des tableaux            
				Offset_Lect= (Aff_Enreg-1)*0x180;


				Lect_tpa_i2c ();   //LECTURE mesures et infos dansEEPROM-I2C


				SetPos(0,8);								// Positionner le curseur
				for (i=0;i<=13;i++)
				{
				if(Tab_Central[i]!=0x20){  // = espace
				PutChar(Tab_Central[i]);
						 	}				
				}	

//ATTENTION � partir de la version 1.26   VOIE 2octets + ROB 2octets + TETE 6octets devient VOIE 3octets + Obs 7octets pour les tableaux d'enregistrement et de sauvegarde
//Le nom dans le tableau reste  Tab_VoieRobTete[10];
				SetPos(0,56);								// Positionner le curseur
				PutMessage("Voie:");		// 
				SetPos(25,56);								// Positionner le curseur
				if (Tab_VoieRobTete[0]>=48 && Tab_VoieRobTete[0]<=57)PutChar(Tab_VoieRobTete[0]);
				if (Tab_VoieRobTete[1]>=48 && Tab_VoieRobTete[1]<=57)PutChar(Tab_VoieRobTete[1]);	 
				if (Tab_VoieRobTete[2]>=48 && Tab_VoieRobTete[2]<=57)PutChar(Tab_VoieRobTete[2]);	 // erreur ? Tab_VoieRobTete[1]>=48    modif du 20/02/2016 => Tab_VoieRobTete[1]>=48
				Num_Capteur =0;
				ClearScreen ();								// Effacement de tout l'�cran






	if (F_sauve_mes_tpA==1) // Adressables
					{

		for (tpa=0; tpa <= 16 ;tpa++)  //tpa=0 POUR LECTURE EN ZERO (DEBITMETRE)
			{
				if (Tab_Etat[tpa]==0 && tpa>0 )  // tpa > 0 pour passer le d�bitm�tre  modif du 28/02/2016
					{
//				if (Tab_I_Repos[tpa] == 0 ){    // modif du 28/02/2016					
				Nb_Capteurs_max = tpa-1;		// modif du 28/02/2016
				tpa=17;							// modif du 28/02/2016
									}
				Tab_Code[tpa] = tpa;	  // � voir si � conserver ainsi que Num_Capteur

			}
//				Nb_Capteurs_max = 0x04;// pour debug
				Num_Ligne = 3;		// num�ro de la ligne de mesure pour affichage				
				Scrolling_Ecran_tpa();
					}

	else if (F_sauve_mes_tpR==1 && modif_aff==1)    // Resistif
			{		
			ecran_TPR_Aff_memo();	//Utilis� dans l'affichage des TPR, des enregistrements en m�moire 
			modif_aff=0;
			}

	else if (F_sauve_mes_Deb==1 && modif_aff==1)    // Resistif
			{		
			ecran_debitmetres_memo();	//Utilis� dans l'affichage des enregistrements en m�moire 
			modif_aff=0;
			}




}


void Increment_adresse()
{
			if (Aff_Enreg < No_der_enreg)   // 
			{
			Aff_Enreg += 1;
			SetPos(90,56);								// Positionner le curseur
			PutMessage("Enr:");		// 
			displayNum (107,56,Aff_Enreg);
			}	
}

void Decrement_adresse()
{
//unsigned int Offset_Enr; // adresse de l'enregistrement en lecture
//unsigned int Der_Enreg;  // adresse du dernier enregistrement en EEprom
//unsigned int Aff_Enreg;  // adresse de l'enregistrement affich�


			if (Aff_Enreg >= 2)   // 
			{
			Aff_Enreg -= 1;
			SetPos(90,56);								// Positionner le curseur
			PutMessage("Enr:");		// 
			displayNum (107,56,Aff_Enreg);
			}
}





