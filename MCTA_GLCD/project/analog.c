// Analogique.c


#include <p18f8723.h>				

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <delays.h>
#include <math.h>
#include <analog.h>
#include "inituc.h"


#define AN0		0b00000000			// V_batt
#define AN1		0b00000100			// V_48v
#define AN2		0b00001000			// I_Modul
#define AN3		0b00001100			// I_Conso
#define AN4		0b00010000			// V_Ligne_AM
#define AN5		0b00010100			// V_Ligne_AV
#define AN6		0b00011000			// V_Ligne_AV2



float V_Batt_Calc;
float V_48_Calc;
float V_Ligne_AM_Calc;
float V_Ligne_AV_Calc;
float Val_Temp;

//unsigned int V_Can_Max;   // max 4095 pour le convertisseur (12bits)
//unsigned int V_Batt_Max;   // max 15V sur Vbatt
//unsigned int V_48_Max;   // max 80V sur V_48V
unsigned char I_Conso;
unsigned char I_Modul;
unsigned int I_Repos;
unsigned int V_Ana;   // valeur convertisseur analogique
float V_Resistif;       // TENSION AUX BORNES DE LA RESISTANCE DE MESURE AVEC UN GAIN DE 8.50
float V_Resistif2;       // TENSION AUX BORNES DE LA RESISTANCE DE MESURE AVEC UN GAIN DE 4.75

#define V_Can_Max  	4095   	// max 4095 pour le convertisseur (12bits)
#define I_Conso_Max  200    //<==� revoir 		 multipli� par 10 et affichage 			avant modif(200=20mA)
#define I_Modul_Max  150	// car multipli� par 10 et affichage 10.0

#define I_Repos_Max  619	//	�A avec nouveau gain de 8.50
//calcul=> 5000mV(sortie de l'ampli)/8.5(gain)/0.95Kohm (r�sistance de mesure) = 618.9    = 588 avec 1.0Kohm 
//#define I_Repos_Max2  1000	//	�A avec nouveau gain de 4.75  A VOIR SI ON UTILISE!!!!!!!!!!!!!!
//calcul=> 5000mV(sortie de l'ampli)/4.75(gain)/0.95Kohm (r�sistance de mesure) = 1000   A REVERIFIER!!!!!!!!!!!!!!

#define V_Batt_Max 	 15   	// max 15V sur Vbatt
#define V_48_Max 	 80   	// max 80V sur V_48V avec gain ampli 1/16
#define V_48V_min    50    // = 50volts => si tension inf�rieure on stoppe le programme et on affiche anomalie 48V

// Coefficients de correction
unsigned int Coeff_I_Modul;   	// pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53
unsigned int Coeff_I_Conso;   	// pour ajuster I_Conso (coeffx100) exemple 153 => coeff 1.53
unsigned int Coeff_I_Repos;   	// pour ajuster I_Repos (coeffx100) exemple 153 => coeff 1.53
unsigned int Coeff_V_TPR;	 		// pour ajuster V_1K pour TPR (coeffx100) exemple 153 => coeff 1.53
unsigned int Coeff_V_TPR2;	 		// pour ajuster V_1K pour TPR (coeffx100) exemple 153 => coeff 1.53


#define V_1K_Max 	 5000   	// � revoir 
#define V_1K_Max2 	 5000   	// � revoir 
// la valeur max V_1K_Max est calcul�e pour une tension de 56V et 100K sur R1K et un gain de 8.50 pour l'ampli  AVEC NOUVEAU GAIN r88=150k et r89=20k  => (150/20)+1 = 8.50
// la valeur max V_1K_Max2 est � revoir avec gain2 de 4.75



/* Resume allocation of romdata into the default section */
//# pragma romdata
#pragma		code	USER_ROM   // retour � la zone de code*/



// --------- Mesure batterie ----------------
void Mesure_V_Batt (void)
{
		V_Ana = acqui_ana_16 (AN0) ;    //(16 mesures)
			V_Batt_Calc = V_Ana;
		V_Batt_Calc = ((V_Batt_Calc * V_Batt_Max)  / V_Can_Max) ;
} // fin

// --------- Mesure convertisseur 48V ----------------
void Mesure_V_48v ()
//void Mesure_V_48v (float V_48_Calc)
{
		V_Ana = acqui_ana_16 (AN1) ;    //(16 mesures)
			V_48_Calc = V_Ana;
		V_48_Calc = ((V_48_Calc * V_48_Max)  / V_Can_Max) ;
} // fin

// --------- Mesure ligne en amont du capteur ----------------
void Mesure_V_Ligne_AM()								// Acquisition de la valeur CAN de la tension ligne AMont capteur
{
		V_Ana = acqui_ana_16 (AN4) ;    //(16 mesures)
			V_Ligne_AM_Calc = V_Ana;
		V_Ligne_AM_Calc = ((V_Ligne_AM_Calc * V_48_Max)  / V_Can_Max) ;
} // fin

// --------- Mesure ligne en aval du capteur ---A REVOIR MAIS PAS UTILIS�-------------
//void Mesure_V_Ligne_AV()								// Acquisition de la valeur CAN de la tension ligne AVal capteur
//{
//		V_Ana = acqui_ana_16 (AN5) ;    //(16 mesures)
//			V_Ligne_AV_Calc = V_Ana;
//		V_Ligne_AV_Calc = (V_Ligne_AV_Calc * V_1K_Max  / V_Can_Max) ;
//		V_Ligne_AV_Calc = (V_Ligne_AV_Calc * V_48_Max  / V_Can_Max) ;
//} // fin

// --------- Mesure courant de modulation du capteur ----------------
void Mesure_I_Modul ()							// Acquisition de la valeur CAN du courant de modulation
{
		V_Ana = acqui_ana_16_plus (AN2) ;    //(16 mesures avec tempo entre les mesures)
		Val_Temp = V_Ana;
		Val_Temp = ((Val_Temp * I_Modul_Max)  / V_Can_Max) ;
		Val_Temp = Val_Temp * Coeff_I_Modul / 100;     // AJUSTEMENT GAIN
		I_Modul = (char)Val_Temp;
}


// --------- Mesure courant de consommation du capteur ----------------

void Mesure_I_Conso()								// Acquisition de la valeur CAN du courant de consommation
{
		V_Ana = acqui_ana_16_plus (AN3) ;    //(16 mesures avec tempo entre les mesures)
		Val_Temp = V_Ana;
		Val_Temp = ((Val_Temp * I_Conso_Max ) / V_Can_Max) ;
		Val_Temp = Val_Temp * Coeff_I_Conso / 100;     // AJUSTEMENT GAIN
		I_Conso = (char)Val_Temp;
//		I_Conso = (int)Val_Temp;
}


// --------- Mesure courant de repos par ligne en aval du capteur ----------------
void Mesure_I_Repos()								// Acquisition de la valeur CAN de la tension ligne AVal capteur
{
		V_Ana = 0x0000;
		Val_Temp = 0x0000;
		I_Repos= 0x0000;

		V_Ana = acqui_ana_16_plus (AN5) ;    //(16 mesures avec tempo entre les mesures)
		Val_Temp = V_Ana;
		Val_Temp = ((Val_Temp * I_Repos_Max)  / V_Can_Max) ;
		Val_Temp = Val_Temp * Coeff_I_Repos / 100;     // AJUSTEMENT GAIN
		I_Repos= (int)Val_Temp;

} // fin



// --------- Mesure tension ligne en aval du capteur pour transducteurs r�sistifs GAIN DE 8.50 ----------------
void Mesure_V_Resistif()	// Acquisition de la valeur CAN de la tension ligne AVal capteur  avec un gain de 8.50
{
		V_Ana = acqui_ana_16_plus (AN5) ;    //(16 mesures avec tempo entre les mesures)
		Val_Temp = V_Ana;
		Val_Temp = ((Val_Temp * V_1K_Max)  / V_Can_Max) ;
//		Val_Temp = Val_Temp / 100;      
		Val_Temp = Val_Temp * Coeff_V_TPR / 100;     // AJUSTEMENT GAIN R�sistifs   Modif du 01/05/2016
		V_Resistif= Val_Temp; 

} // fin



// --------- Mesure tension ligne en aval du capteur pour transducteurs r�sistifs GAIN DE 4.75 ----------------
/////////////pour mesure V_Resistif avec une tension de 80v et pour ne pas saturer ampli avec gain de 8.50
void Mesure_V_Resistif2()	// Acquisition de la valeur CAN de la tension ligne AVal capteur  avec un gain de 4.75
{
	Coeff_V_TPR2=100;  // VALEUR PAR DEFAUT, � voir si m�me coeff que celui du gain de 8.50
		V_Ana = acqui_ana_16_plus (AN6) ;    //(16 mesures avec tempo entre les mesures)
		Val_Temp = V_Ana;
		Val_Temp = ((Val_Temp * V_1K_Max2)  / V_Can_Max) ;
//		Val_Temp = Val_Temp / 100;      
		Val_Temp = Val_Temp * Coeff_V_TPR2 / 100;     // AJUSTEMENT GAIN R�sistifs   Modif du 01/05/2016
		V_Resistif2= Val_Temp; 

} // fin



//---------------- Acquisition mesure analogique -------------------------
unsigned int acqui_ana (unsigned char adcon )
{
//LED1_ALIM_VL = ~PORTJbits.RJ1 ;		// test
	ADCON0 = adcon ;  	// adress + abort la mesure en cours
	_ADON = 1 ;	
	_ADGO = 1 ;
	
	while ( _ADDONE ) 
	{
	//	Nop();
	}

	return ADRES ;	
}	// fin acqui_ana ()

//---------------- Acquisition mesure analogique -------------------------
unsigned int acqui_ana_2 (unsigned char adcon )
{
//	unsigned long II;
	unsigned int I;
//	unsigned int N ; // Variable usage general
//	int i ; // Variable usage general

//	N = 0 ;
	I = 0 ;
//	II = 0;
//	for ( i = 0; i < 0x2; i++ )	// 2 MESURES POUR LA MOYENNE
//	{
//		N = acqui_ana (adcon) ;
//		II = II + N;
//	}
//	II >>= 1 ;		// Moyenne pour 2 mesures

//	N = (int) II;		// sauvegarde de Long en Int
//	return (N);

// MODIF 2 ET 1
//	I = acqui_ana (adcon) ;
//	I += acqui_ana (adcon) ;
//	I >>= 1 ;		// Moyenne pour 2 mesures
//	return (I);

//	return ADRES ;	

//LED1_ALIM_VL = ~PORTJbits.RJ1 ;		// test

	ADCON0 = adcon ;  	// adress + abort la mesure en cours
	_ADON = 1 ;	
	_ADGO = 1 ;
	
	while ( _ADDONE ) 
	{
	//	Nop();
	}
	I = ADRES ;

	_ADGO = 1 ;
	while ( _ADDONE ) 
	{
	//	Nop();
	}

	I += ADRES ;
	I >>= 1 ;		// Moyenne pour 2 mesures
	return (I);

//LED1_ALIM_VL = ~PORTJbits.RJ1 ;		// test

}	// fin acqui_ana_2()
//---------------- Acquisition mesure analogique -------------------------
unsigned int acqui_ana_4 (unsigned char adcon )
{
	unsigned long II;
	unsigned int N ; // Variable usage general
	int i ; // Variable usage general

	N = 0 ;
	II = 0;
	for ( i = 0; i < 0x4; i++ )	// 4 MESURES POUR LA MOYENNE
	{
		N = acqui_ana (adcon) ;
		II = II + N;
	}
	II >>= 2 ;		// Moyenne pour 4 mesures

	N = (int) II;		// sauvegarde de Long en Int
	return (N);

}	// fin acqui_ana_4()
/*----------------- acqui_ana_16 -----------------------------------------------*/
unsigned int acqui_ana_16 (unsigned char adcon )
{
	unsigned long II;
	unsigned int N ; // Variable usage general
	int i ; // Variable usage general

	N = 0 ;
	II = 0;
	for ( i = 0; i < 0x10; i++ )	// 16 MESURES POUR LA MOYENNE
	{
		N = acqui_ana (adcon) ;
		II = II + N;
	}
	II >>= 4 ;		// Moyenne pour 16 mesures

	N = (int) II;		// sauvegarde de Long en Int
	return (N);

}

/*----------------- acqui_ana_16_plus -----------------------------------------------*/
		//16 mesures avec tempo entre les mesures)
unsigned int acqui_ana_16_plus (unsigned char adcon )
{
	unsigned long II;
	unsigned int N ; // Variable usage general
	int i ; // Variable usage general

	N = 0 ;
	II = 0;
	for ( i = 0; i < 0x10; i++ )	// 16 MESURES POUR LA MOYENNE
	{
		N = acqui_ana (adcon) ;
		II = II + N;
	Delay10KTCYx(1);
	}
	II >>= 4 ;		// Moyenne pour 16 mesures

	N = (int) II;		// sauvegarde de Long en Int
	return (N);

}




	// fin acqui_ana_16_plus ()





 




////////////////////////////////////////////////////////////////////////////////////////////////

