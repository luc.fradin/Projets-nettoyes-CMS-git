

#include <p18f8723.h>             /* C18 compiler processor defs */
#include <delays.h>                /* Delays defs */
#include <delay.h>

//	Delay10TCYx(0x3C);  //delai de 10�sec


//***************************************************************************
//Nom   : void DELAY_SEC(int sec)
//Role  : Delay de 1 seconde / Quartz 24Mhz (HS)
//utilise timer0
//***************************************************************************

void DELAY_SEC(int sec)
//Prescaler 1:128; TMR0 Preload = 18661; Actual Interrupt Time : 1sec
{
int i;
T0CON = 0x86; /* enable TMR0, select instruction clock, prescaler set to 1.128 */
for (i = 0; i < sec; i++)
	{
		InitTimer0();
	}
return;
}


//***************************************************************************
//Nom   : void DELAY_1SEC()
//Role  : Delay de 1 seconde UNIQUE / Quartz 24Mhz (HS)
//utilise timer0
//***************************************************************************
void DELAY_SW_1SEC()
//Prescaler 1:128; TMR0 Preload = 18661; Actual Interrupt Time : 1sec
{
	T0CON 	= 0x86; /* enable TMR0, select instruction clock, prescaler set to 1.128 */
  	TMR0H	 = 0x48; //Preload = 18661
  	TMR0L	 = 0xE5;
  	INTCONbits.TMR0IE	 = 0;
  	INTCONbits.TMR0IF  = 0;
// while(!(INTCONbits.TMR0IF)); /* wait until TMR0 rolls over */=> dans le programme apr�s l'instruction delay
return;
}
//***************************************************************************
//Nom   : void DELAY_2SEC()
//Role  : Delay de 2 secondes UNIQUES / Quartz 24Mhz (HS)
//utilise timer0
//***************************************************************************
void DELAY_SW_2SEC()
//Prescaler 1:256; TMR0 Preload = 18661; Actual Interrupt Time : 1sec
{
	T0CON = 0x87; /* enable TMR0, select instruction clock, prescaler set to 1.256 */
	TMR0H	 = 0x48; //Preload = 18661
  	TMR0L	 = 0xE5;
  	INTCONbits.TMR0IE	 = 0;
  	INTCONbits.TMR0IF  = 0;
// while(!(INTCONbits.TMR0IF)); /* wait until TMR0 rolls over */=> dans le programme apr�s l'instruction delay
return;
}

//***************************************************************************
//Nom   : void DELAY_MS(int ms)
//Role  : Delay de 1 milliseconde / Quartz 24Mhz (HS)
//utilise timer0
//*****************************************************************************/
void DELAY_MS(int ms)
//Timer0
//Prescaler 1:1; TMR0 Preload = 59536; Actual Interrupt Time : 1 ms
{
int i;
T0CON = 0x88; /* enable TMR0, select instruction clock, prescaler set to 1.1 */
for (i = 0; i < ms; i++) 
	{
  TMR0H	 = 0xE8; //Preload = 59536
  TMR0L	 = 0x90;
  INTCONbits.TMR0IE	 = 0;
  INTCONbits.TMR0IF = 0;
  while(!(INTCONbits.TMR0IF)); /* wait until TMR0 rolls over */
	}
return;
}


///***************************************************************************
//Nom   : void DELAY_100US(int us)
//Role  : Delay de 100 us / avec Quartz 24Mhz /4  tps de cycle = 1/6�S * 10 * 60=100�sec
//*****************************************************************************/
void DELAY_100US(int us) 
{
int i;
int j;
i,j=0;
for (j=0; j<us; j++) // il faut tenir compte du nb de cycle de for....donc -3
	{
	Delay10TCYx(57);  // 60-3     donne 100�S  !!max 255
	}
return;
}



//***************************************************************************
//Nom   : void DELAY_125mS(int ms)
//Role  : Delay de 125ms  / Quartz 24Mhz (HS)
//utilise timer0
//*****************************************************************************/
void DELAY_125MS(int ms)
//Timer0
//Prescaler 1:16; TMR0 Preload = 18661; Actual Interrupt Time : 125 ms
{
int i;
T0CON = 0x83; /* enable TMR0, select instruction clock, prescaler set to 1.16 */
for (i = 0; i < ms; i++) 
	{
 InitTimer0();
  	}
return;
}

//***************************************************************************
//Nom   : void DELAY_250mS()
//Role  : Delay de 250ms unique / Quartz 24Mhz (HS)
//utilise timer0
//*****************************************************************************/
void DELAY_SW_250MS()
//Timer0
//Prescaler 1:32; TMR0 Preload = 18661; Actual Interrupt Time : 125 ms
{

	T0CON = 0x84; /* enable TMR0, select instruction clock, prescaler set to 1.16 */
  	TMR0H	 = 0x48; //Preload = 18661
  	TMR0L	 = 0xE5;
  	INTCONbits.TMR0IE = 0;
  	INTCONbits.TMR0IF = 0;
// while(!(INTCONbits.TMR0IF)); /* wait until TMR0 rolls over */=> dans le programme apr�s l'instruction delay
return;
}


//***************************************************************************
//Nom   : void DELAY_500mS(int ms)
//Role  : Delay de 500ms / Quartz 24Mhz (HS)
//utilise timer0
//*****************************************************************************/
void DELAY_500MS(int ms)
//Timer0
//Prescaler 1:64; TMR0 Preload = 18661; Actual Interrupt Time : 500 ms
{
int i;
T0CON = 0x85; /* enable TMR0, select instruction clock, prescaler set to 1.64 */
for (i = 0; i < ms; i++) 
	{
		InitTimer0();
	}
return;
}


void InitTimer0()
{
  TMR0H	 = 0x48; //Preload = 18661
  TMR0L	 = 0xE5;
  INTCONbits.TMR0IE	 = 0;
  INTCONbits.TMR0IF  = 0;
  while(!(INTCONbits.TMR0IF)); /* wait until TMR0 rolls over */
}










