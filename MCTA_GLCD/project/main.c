/*
PROGRAMME pour MCTA  (BCTA + TRANSTEST) 
EN COURS DE MODIF POUR NOUVEAU CI

Version 1.88 Pb prog codage des capteurs et verification du codage checksum 0x0cea
Version 1.87 Pb dectection TPA sur menu resistif et interdiction des touches droite et gauche
Version 1.86 Pb detection auto + mesures adressables
Version 1.85 BUG ENREGISTREMENT TPR PUIS MESURE TPA
Version 1.84 (annulation led rouge front d'horloge ) 
Version 1.83 (correction probl�me de d�marrage convertisseur perturbant l'horloge
Version 1.79 modifs suite � r�union pressu � Lyon du 16 17 mars 2016 + AUTRES MODIFS SUR TPR + PREPARATION AMPLI GAIN2 POUR MESURE RESISTIFS
+ d�tection pr�sence TPA par lecture code TPA + affichage dans menu TPR des messages pour tpa


ATTENTION � partir de la version 1.26   VOIE 2octets + ROB 2octets + TETE 6octets devient VOIE 3octets + Obs 7octets pour les tableaux d'enregistrement et de sauvegarde
Le nom dans le tableau reste  Tab_VoieRobTete[10];

VOIR LIGNE 1518 du main si pb dans le choix de la config
*/
//================== R�servation m�moires dans leeprom du PIC========================================================

//coche_constit � l'adresse 0x00 de l'eeprom du PIC18
//val1TP     		adresse 0x01 de l'eeprom du PIC18
//boucleTPR  		adresse 0x02 de l'eeprom du PIC18
//boucleDEB  		adresse 0x03 de l'eeprom du PIC18
//ConsAffTPA  		adresse 0x04 de l'eeprom du PIC18
//ValBuzzer  		adresse 0x05 de l'eeprom du PIC18
//Verif_codageAT  	adresse 0x06 de l'eeprom du PIC18
//Aff_Debit_tpa  	adresse 0x07 de l'eeprom du PIC18   pour bloquer l'affichage du d�bitm�tre dans les �crans TPA
//ad_der_central  	adresse 0x10  � 0x0D  dans l'eeprom du pic  Nom du dernier central
//Coeff_I_Modul   	adresse 0x20 pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53  	valeur max 255!!
//Coeff_I_Conso 	adresse 0x21 pour ajuster I_Conso (coeffx100) exemple 153 => coeff 1.53			valeur max 255!!
//Coeff_I_Repos 	adresse 0x22 pour ajuster I_Repos (coeffx100) exemple 153 => coeff 1.53			valeur max 255!!
//Coeff_V_1K	 	adresse 0x23 pour ajuster V_1K pour TPR (coeffx100) exemple 153 => coeff 1.53			valeur max 255!!

//ad_No_der_enreg = 0x050;  // adresse o� est stock�e, dans le pic, le num�ro du dernier enregistrement 
//ad_Der_Enreg = 0x052;  // adresse de l'eeprom du pic, de la valeur de l'adresse en i2c de l'enregistrment corespondant
//ad_Mem_Num_Ligne=0x054; // adresse pic0x54  memorisation du num�ro de ligne de mesure TPA pour affichage
//ad_sensibilite_TPA=0x056; // son adresse en eeprom du pic
//adr_mesureTPA = 0x7E00; // adresse de l'eeprom ext (I2C) pour sauvegarder les mesures en cours et r�affichages des TPA



//====================================================================================================================





/****************CONFIGURATION BITS ************************/
#pragma list P=PIC18F8723

#pragma config OSC = HS			// OSCILLATOR
#pragma config FCMEN = OFF		// Fail Safe Clock Monitor
#pragma config IESO = OFF 		// Internal External Osc. Switch Disabled
#pragma config PWRT = OFF		// Power Up Timer Disabled
#pragma config BOREN = OFF		// Brown-out Reset
//#pragma config BORV = 3		    // Brown-out  voltage bits
#pragma config WDT = OFF			// WATCHDOG Enabled
#pragma config WDTPS = 1024		// Watchdog Prescaler  512 -> 2,048sec
#pragma config MCLRE = ON		// MCLR Pin Enable
#pragma config LPT1OSC = OFF	// Low Power Timer
//#pragma config CCP2MX = PORTE	// CCP2 MUX bit
#pragma config STVREN = OFF 	// Stack Full Reset 
#pragma config LVP = OFF		// Low Voltage Programming Disabled
#pragma config BBSIZ = BB2K		//Boot Block Size
#pragma config XINST = OFF		//Extended Instruction Set Enable bit !!!MODIF CG!!!
#pragma config DEBUG = OFF		//DEBUG Disabled

#pragma config CP0 = OFF		// Code protection bit Block 0 (ON->protected / OFF->not protected) 
#pragma config CP1 = OFF		// Code protection bit Block 1 (ON->protected / OFF->not protected) 
#pragma config CP2 = OFF		// Code protection bit Block 2 (ON->protected / OFF->not protected) 
#pragma config CP3 = OFF		// Code protection bit Block 3 (ON->protected / OFF->not protected) 
#pragma config CP4 = OFF		// Code protection bit Block 4 (ON->protected / OFF->not protected) 
#pragma config CP5 = OFF		// Code protection bit Block 5 (ON->protected / OFF->not protected) 
#pragma config CP6 = OFF		// Code protection bit Block 6 (ON->protected / OFF->not protected) 
#pragma config CP7 = OFF		// Code protection bit Block 7 (ON->protected / OFF->not protected)

#pragma config CPB = OFF		// Boot Block Code Protection bit (ON->protected / OFF->not protected) 
#pragma config CPD = OFF		// Data EEPROM Code Protection bit (ON->protected / OFF->not protected) 

#pragma config WRT0 = OFF		// Write Protection bit Block 0 (ON->protected / OFF->not protected) 
#pragma config WRT1 = OFF		// Write Protection bit Block 1 (ON->protected / OFF->not protected) 
#pragma config WRT2 = OFF		// Write Protection bit Block 2 (ON->protected / OFF->not protected) 
#pragma config WRT3 = OFF		// Write Protection bit Block 3 (ON->protected / OFF->not protected) 
#pragma config WRT4 = OFF		// Write Protection bit Block 4 (ON->protected / OFF->not protected) 
#pragma config WRT5 = OFF		// Write Protection bit Block 5 (ON->protected / OFF->not protected) 
#pragma config WRT6 = OFF		// Write Protection bit Block 6 (ON->protected / OFF->not protected) 
#pragma config WRT7 = OFF		// Write Protection bit Block 7 (ON->protected / OFF->not protected) 

#pragma config WRTB = OFF		// Boot Block Write Protection bit (ON->protected / OFF->not protected) 
#pragma config WRTC = OFF		// Configuration Register Write Protection bit (ON->protected / OFF->not protected) 
#pragma config WRTD = OFF		// Data EEPROM Write Protection bit (ON->protected / OFF->not protected) 

#pragma config EBTR0 = OFF		// Table Read Protection bit Block 0(ON->protected / OFF->not protected) 
#pragma config EBTR1 = OFF		// Table Read Protection bit Block 1(ON->protected / OFF->not protected) 
#pragma config EBTR2 = OFF		// Table Read Protection bit Block 2(ON->protected / OFF->not protected) 
#pragma config EBTR3 = OFF		// Table Read Protection bit Block 3(ON->protected / OFF->not protected) 
#pragma config EBTR4 = OFF		// Table Read Protection bit Block 4(ON->protected / OFF->not protected) 
#pragma config EBTR5 = OFF		// Table Read Protection bit Block 5(ON->protected / OFF->not protected) 
#pragma config EBTR6 = OFF		// Table Read Protection bit Block 6(ON->protected / OFF->not protected) 
#pragma config EBTR7 = OFF		// Table Read Protection bit Block 7(ON->protected / OFF->not protected) 

#pragma config EBTRB = OFF		// Boot Block Table Read Protection bit (ON->protected / OFF->not protected) 


/************************ INCLUDE ***************************/
#include <p18f8723.h>					/* Defs PIC 18F8723 */
//#include <timers.h>						/* timers (interruptions)*/
//#include <delay.h>						/* Defs DELAY functions */
#include <GLCD.h>						/* Gestion GLCD */
#include <inituc.h>                     /* Initialisation defs */
#include <ftoa.h>						/* Adapt Chaine de caract�re */
////#include <logoAT.h>						/* Logo de pr�sentation*/
#include <ecrans.h>
#include <menus.h>
#include <clock.h>
#include <rtc.h>
//#include <codage.h>
//#include <analog.h>
#include <TESTS.h>
#include <portb.h>
#include <mesures.h>
#include <usart.h>
#include <rs232.h>
#include <eeprom.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
#include <memo.h>

//#include <string.h>

/************************ DEFINES PLACES DANS INITUC.H  ***************************/


// case menu principal
#define	A0 0
#define	A1 1
#define A2 2
#define A3 3

// case menu TP adressables
#define	B0 0
#define	B1 1
#define B2 2

unsigned char memo_ecran;     //position dans les menus

unsigned char memo_touche;     /*m�morise la derni�re position utilis�e dans le menu pricipal*/
unsigned char memo_touche_1a;  /*m�morise la derni�re touche utilis�e  �cran tp adressables*/
unsigned char memo_touche_5a;  /*m�morise la derni�re touche utilis�e  �cran lecture m�moire*/
unsigned char memo_touche_6a;  /*m�morise la derni�re touche utilis�e  �cran param�trage*/



unsigned char F_ecran_adres;	 	// flag pour affichage �cran TPA r�duit(0) ou complet(1)
unsigned char F_aff_sauve_tpA; 		// flag pour m�mo affichage: Sauvegarde ? (sauvegarde des mesures pour TP adressables)
unsigned char F_aff_sauve_tpR; 		// flag pour m�mo affichage: Sauvegarde ? (sauvegarde des mesures pour TP r�sistifs)
unsigned char F_aff_sauve_Deb; 		// flag pour m�mo sauvegarde Sauvegarde ? des mesures des d�bitm�tres
unsigned char F_sauve_mes_tpA; 		// flag pour m�mo sauvegarde valid�e des mesures des tp adressables
unsigned char F_sauve_mes_tpR; 		// flag pour m�mo sauvegarde valid�e des mesures des tp r�sistifs
unsigned char F_sauve_mes_Deb; 		// flag pour m�mo sauvegarde valid�e des mesures des d�bitm�tres
unsigned char F_retour_sauve;		// flag pour indiquer fin de sauvegarde
unsigned char F_memo_back_light; 	// flag pour m�mo sauvegarde position back_light				
unsigned char F_mode_scrut; 		// flag pour signaler d�but et fin de scrutation
unsigned char F_sup3mes_tpa;	 	// flag pour indiquer > � 3 mesures
unsigned char F_mode_RS232=0;  		// flag pour indiquer si USB/RS232  non connect� si 0   ou    connect� si 1
unsigned char compteurX;  			// compteur pour boucle de temps
unsigned char F_der_tous;  			//  flag de s�lection du dernier enregistrement ou, tous les enregistrements � effacer
unsigned char F_lecture_eep;  		//  flag pour ne pas relire l'eeprom dans le cas d'un r�affichage des mesures
unsigned char F_new_mes; 			// indique qu'il y a eu une nouvelle mesure et qu'il faut enregistrer dans le cas du r�affichage de la derni�re mesure
unsigned char Detection_TPA;			// flag pour indiquer si d�tection TPA ou non dans l'�cran de mesure TPR
unsigned char Detection_TPA_V1_86; //flag pour version 1.86
unsigned char Detection_TPA_V1_87; //flag pour version 1.87

extern unsigned char Code_Param[6];

/*	#define b_ecran_adres MemoBits.Bit0 	// bit pour m�mo �cran TP adressables r�duit(0) ou complet(1)
	#define b_mes_ecran_adres MemoBits.Bit1	// bit pour m�mo affichage: Sauvegarde ? (sauvegarde des mesures pour TP adressables)
	#define b_sav_mes_TPadres MemoBits.Bit2	// bit pour m�mo sauvegarde valid�e des mesures des tp adressables
	#define b_memo_back_light MemoBits.Bit3	// bit pour m�mo sauvegarde position back_light				
	#define	b_ModeScrut MemoBits.Bit4		// bit pour signaler d�but et fin de scrutation
*/


/**********************************************************************
 * Fonction prototypes.
 **********************************************************************/
void main (void);
void Init_I2C(void);
//void Init_eeprom_i2c(void);
void Init_RS232(void);
void Init_Tab_tpa(void);
//void Boucle_Mesure_PCPG(void);
void LECTURE_PARAM(void);
void Sauvegarde_TA_TR_Deb(void);

//void ecran_aff_TPA_complet(unsigned char Num_Capteur, unsigned char Code, unsigned int Pression, unsigned int I_Repos, unsigned char I_Modul, unsigned char I_Conso,unsigned char Etat);   //�cran affichage TPadressables mode complet
//void ecran_aff_TPA_reduit(unsigned char Num_Capteur, unsigned char Code, unsigned int Pression, unsigned char Etat);  //�cran affichage TPadressables mode r�duit


#pragma udata BIGDATA  //section BIGDATA pour tableau ==> voir modif dans le fichier: 18f8723_bigdata.lkr plac� dans "projet"; ce fichier remplace 18f8723_g.lkr de mcc18/bin/lkr. 
						//Ne pas oubler de le d�clarer dans: Projet/build options.../projet/directories/linker-script search path et ajouter c:\MCTA_GLCD\projet et d'ajouter le fichier 18f8723_bigdata.lkr par Projet/Add Files to Projet...

unsigned int 		Tab_No_Enr[1];					//2 enregistre le num�ro d'enregistrement sur 2 octets
unsigned char 	Tab_Type_Enr[1];  				//1 octets  pour m�moriser le type d'enregistrement (TPA=0,Groupe=1,D�bitm�tre=2,TPR=3,Icra=4)
unsigned char 	Tab_Horodatage[10];		//10 octets	 //10 caract�res	  
unsigned char 	Tab_Constit[17][10];	//170 octets //constitution (1� 16 pour chaque capteur(16) + le 0 pour le d�bitm�tre? ne sert � rien?! => � voir)
unsigned char 	Tab_VoieRobTete[10];	//10 octets //VOIE 2 + ROB 2 + TETE 6	 ATTENTION � partir de la version 1.26 remplac� par VOIE 3 + Obs 7
											//  	VOIE 3 + ROB 2 + TETE 5	 (ROB 2 + TETE 5 devient Comment)
unsigned char 	Tab_Central[14];		//14 octets	 //14 caract�res	  
unsigned int 	Tab_Pression[17];		//34 octets   en 0 valeur du d�bit  si ffff => pas de d�bitm�tre   !!! METTRE UN CODE SI TP RESISTIF
unsigned int 	Tab_I_Repos[17];		//34 octets 17 caract�res	  1 d�bitmetre et 16 TP
unsigned char 	Tab_I_Modul[17];		//17 octets	17 caract�res	  1 d�bitmetre et 16 TP
unsigned int 	Tab_I_Conso[17];		//34 octets	17 caract�res	  1 d�bitmetre et 16 TP
unsigned char 	Tab_Etat[17];			//17 octets  
								   // Dans ce cas les diff�rentes valeurs de TPA, TPR, D�bitm�tre, Groupe, Icra seront enregistr�es dans Tab_Pression[17] 
							  // total:  340+1+2 = 343 octets


/////// Ajouter les mesures du groupe   	 total = 20 octets   soit au total 340+20 = 360 octets  => pour avoir une valeur ronde => (256+128) 384  octets => de 000 � 17F chaque enregistrement
// Voir aussi: un r�sistif, en cas d'ICRA peut-�tre confondu avec un d�bitm�tre ou un capteur en r�ponse permanente: 
  // il faudra faire un test  si meme valeur de fr�quence des capteurs pour d�tecter si ICRA
   
#pragma udata			// retour � defaut section





/**********************************************************************
 * Fonction extern.
 **********************************************************************/

//extern void Rd_eeprom_i2c_str ( unsigned int adr_eeprom,  char *string, unsigned int long_string );
//extern void Wr_eeprom_i2c_char( unsigned int adr_eeprom,  char data_char);
//extern unsigned char Rd_eeprom_i2c_char( unsigned int adr_eeprom);
//extern unsigned int Rd_eeprom_i2c_int( unsigned int adr_eeprom);




/****************** DECLARATION DES VARIABLES ******************/

unsigned char 	Tab_Code[17];			//17 octets 17 caract�res	  1 d�bitmetre et 16 TP 

short int annee =	001;
char niv_batt;
char DATAtoLCD[30];
extern const rom char ADECEF_MCTA[];

//exemples
unsigned char Num_Capteur = 0;
extern unsigned char I_Conso ;  // pour 8.5
extern unsigned char I_Modul ;  // pour 4.3
extern unsigned int I_Repos ;
unsigned int Pression = 1478;
unsigned char Code = 1;
unsigned char Etat = 1;
unsigned char Boucle_TP;
unsigned char Mode_Scrolling;  //utilit�?
unsigned int Code_Prog;
unsigned int Code_Lect;
extern unsigned char Num_Ligne; 	// num�ro de la ligne de mesure pour affichage
extern unsigned char Nb_Capteurs_max;   //16 � voir dans mesures.c
unsigned char Pres_Debit;  // indique pr�sence d�bitm�tre pour affichage  0=absent,  	1= pr�sent, 	 2 = pas encore mesur�
extern unsigned char mes_groupe; // pour indiquer (dans la boucle de mesure des pcpg) que c'est une mesure du groupe de pressu 

extern unsigned char mes_debit; // pour indiquer (dans la boucle de mesure des pcpg) une mesure du d�bitm�tre 


extern unsigned int Timer1L; // pour suavegarder les valeurs � l'interruption de INT2 (utilis� dans la mesure de fr�quence
extern unsigned int Timer1H;

//char chaine1[] = "TEST CHAINE";

extern unsigned int codeTPbcta; 

extern unsigned int V_Batt;
extern float V_Batt_Calc;
extern float V_Batt_Aff;
extern float V_48_Calc;
extern float V_Ligne_AM_Calc;
//float V_48_Calc;


// Coefficients de correction
extern unsigned int Coeff_I_Modul;   	// pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53
extern unsigned int Coeff_I_Conso;   	// pour ajuster I_Conso (coeffx100) exemple 153 => coeff 1.53
extern unsigned int Coeff_I_Repos;   	// pour ajuster I_Repos (coeffx100) exemple 153 => coeff 1.53
extern unsigned int Coeff_V_TPR;	 		// pour ajuster V_1K pour TPR (coeffx100) exemple 153 => coeff 1.53




//unsigned	char temp;
//extern unsigned int t1ov_cnt;
unsigned int t1ov_cnt;	//	compteur de d�bordement du timer1  (de 65535 � 0000)
unsigned int cpt_int2;

unsigned char	valid_sens;   // validation de l'affichage et du r�glage de la sensibilit� dans la lecture des TPR


// parametres stock�s en eeprom du pic
extern unsigned char config_adecef;   // flag pour autoriser la config adecef

extern unsigned char coche_constit; 	// bloque ou non la saisie de la constitution 
extern unsigned char val1TP;   			// valid l'arr�t de scrutation si 1 TP
extern unsigned char boucleTPR;   		// valid lecture en boucle des TPR
extern unsigned char boucleDEB;   		// valid lecture en boucle du d�bit
extern unsigned char ConsAffTPA;   		// valid conserve affichage TPA lorsqu'on quitte le mode mesure tpa
extern unsigned char ValBuzzer;   		// valid buzzer (dans configuration)
extern unsigned char Verif_codageAT;	// Bloque ou non la v�rification du codage des TPA
extern unsigned char Aff_Debit_tpa;   // Bloque ou non l'affichage du d�bit dans tableau des TPA
extern unsigned char Val_Voie; // valeur n� de voie TPA pour la constitution

extern unsigned int Unites,Dizaines;        // Global var (unites et dizaines pour l'ann�e) si = 0 => l'horloge n'est pas � l'heure
//unsigned char mem_bcl_tpr;   //mem boucleTPR 
extern unsigned char TPA_detecte;
//unsigned int Offset_Enr; // adresse de l'enregistrement en lecture

unsigned int ad_No_der_enreg = 0x050;  // adresse o� est stock�e, dans le pic, le num�ro du dernier enregistrement 
unsigned int No_der_enreg ; // valeur du num�ro du dernier enregistrement   entre 0 et 80

unsigned int ad_Der_Enreg = 0x052;  // adresse de l'eeprom du pic, de la valeur de l'adresse en i2c de l'enregistrment corespondant
unsigned int Der_Enreg;  // valeur de l'adresse m�moire i2c du dernier enregistrement en EEprom

unsigned int Mem_Num_Ligne;  //valeur num�ro de ligne TPA pour r�affichage derni�res mesures TPA (num_ligne effac�e par une mesure debit ou groupe)
unsigned int ad_Mem_Num_Ligne=0x054; // adresse pic0x54  memorisation du num�ro de ligne de mesure TPA pour affichage

unsigned int sensibilite_TPA;  // variable pour fixer le niveau de d�tection des TPA dans la lecture des PTR (d�clar�e dans main.c)
unsigned int ad_sensibilite_TPA=0x056; // son adresse en eeprom du pic

unsigned int adr_mesureTPA = 0x7E00; // adresse de l'eeprom ext (I2C) pour sauvegarder les mesures en cours et r�affichages des TPA


#pragma		code	USER_ROM   // retour � la zone de code

//________________________________________INTERRUPTIONS__________________________________________

/* fonction interruptions */
void MyInterrupt1(void);				// code de l'interruption
//void MyInterrupt2(void);				// code de l'interruption
 
#pragma code highVector=0x08			//valeur 0x08 pour interruptions prioritaite 
void highVector(void)
{
_asm GOTO MyInterrupt1 _endasm		// on doit �xecuter le code de la fonction MyInterrupt
}
/*
#pragma code lowVector=0x18			//valeur 0x18 pour interruptions  NON prioritaire
void lowVector(void)
{
_asm GOTO MyInterrupt2 _endasm		// on doit �xecuter le code de la fonction MyInterrupt
}
*/




// ************************
// ****  Interruptions ****
// ************************
#pragma interrupt MyInterrupt1 
void MyInterrupt1(void)
{
unsigned char sauv1;
unsigned char sauv2;
	char dummy; 
char buffer_test[2];

	sauv1 = PRODL; // sauvegarde le contenu des registres de calcul
	sauv2 = PRODH;	

//===========TIMER1
	if (PIR1bits.TMR1IF) 		// v�rifie que l'IT est Timer1
	{
		t1ov_cnt ++;		// incr�mente le compteur de d�bordement
		PIR1bits.TMR1IF = 0; // efface le drapeau d'IT du Timer1
	}

//===========INT sur RB0


	if (INTCONbits.INT0IF==1)    // v�rifie que l'IT est INT0, origine RB0=0		
 		{

			INTCONbits.INT0IF=0; //efface le drapeau d'IT sur RB0
		  } 		 



//===========INT sur RB2
		if (INTCON3bits.INT2IF==1)    // v�rifie que l'IT est INT2, origine RB2=2		
			{
			if (cpt_int2==0){
			TMR1H=0;
			TMR1L=0;
			t1ov_cnt=0;
			T1CONbits.TMR1ON = 1;  // timer 1 ON
			PIR1bits.TMR1IF = 0; // efface le drapeau d'IT du Timer1
			}	

////			INTCON3bits.INT2IE = 0;	// arret interruption INT2
			if (cpt_int2>=20)
				{
				T1CONbits.TMR1ON = 0;  // timer 1 OFF
				Timer1H = TMR1H;
				Timer1L = TMR1L;  
				INTCON3bits.INT2IE = 0;	// arret interruption INT2
				}
			cpt_int2++;
			INTCON3bits.INT2IF=0; //efface le drapeau d'IT sur RB2
////			INTCON3bits.INT2IE=1;	// valide interruption INT2
  		    }

 if (F_mode_RS232==1)
	{
    if(RCSTA1bits.FERR == 1){ 			// check if Framing error 
        					// AN774 P.12
		dummy = RCREG;      		// read data byte with error
		TXSTA1bits.TXEN=0; 
		TXSTA1bits.TXEN=1; 
    }
	else if (RCSTA1bits.OERR==1){ 		// check for Overflow Error 
						// AN774 P.12
		dummy = RCREG; 			// read data byte in the FIFO with possible error
		dummy = RCREG; 			// read data byte in the FIFO with possible error
		RCSTA1bits.CREN=0; 		// if error present, reset OERR so that reception can continue
		RCSTA1bits.CREN=1;
    }
	else {
		gets1USART((char *)buffer_test,1 );	// read 1 chr
		buffer_test[1]=0;				// Terminate string with Null chr
		puts1USART((char *)buffer_test); 		// echo back the data recieved back to host
	}				
}
				
	PRODL = sauv1;	// on restaure les registres de calcul
	PRODH = sauv2;		
//	INTCONbits.GIE = 1; 
//	INTCONbits.PEIE = 1;

}



/*
#pragma interrupt MyInterrupt2 
void MyInterrupt2(void)
		{
		}
*/





//________________________________________FIN  INTERRUPTIONS__________________________________________





/********************************* PROGRAMME PRINCIPAL *******************************/


void main(void)
{



//initialisations
			unsigned int tpa;
			unsigned char mem_bcl_Tpr;   //mem boucleTPR
			unsigned char mem_bcl_Deb;   //mem boucleDEB
			unsigned char flag_param;

Detection_TPA_V1_86=0; //V1.86
Detection_TPA_V1_87=0; //V1.87
TPADETECT:		// 1.86 bug detection et mesure sur adressable



///float jj;			
	F_mode_RS232 = 0;   
	INTCONbits.GIE = 0; 
	INTCONbits.PEIE = 0;
	INTCON2bits.RBIP = 0;	// interrupt Hight priorit� sur port B
	INTCONbits.RBIE = 0;	//valide  interrupt sur port B

	init_uc ();			        // ****Initialisation du microcontroleur**** 
	DELAY_MS(10);
	Init_I2C();
	DELAY_MS(10);
	Init_GLCD(); 				// Initialisation de l'afficheur
	DELAY_MS(10);
	Init_Tab_tpa();				// Initialisation des tableaux de mesures des tpa


// Valeurs par d�faut mais corrig�e apr�s lecture eeprom dans LECTURE_PARAM();
	Coeff_I_Modul = 100;  	// pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53
	Coeff_I_Conso = 100;   	// pour ajuster I_Conso (coeffx100) exemple 153 => coeff 1.53
	Coeff_I_Repos = 100;   	// pour ajuster I_Repos (coeffx100) exemple 153 => coeff 1.53
	Coeff_V_TPR = 100;   	// pour ajuster V_TPR (coeffx100) exemple 153 => coeff 1.53
	Val_Voie=1; //N�de voie affich�e dans l'�cran de saisie infos au d�marrege 
	LECTURE_PARAM();   //Lecture des param�tres de config stock�s en eeprom du pic

	if (F_mode_RS232 == 1)  //si 0 => non connect�  => � voir comment le d�tecter!
		{
	Init_RS232();
		}

//		Ecran_Constitution ();

				F_ecran_adres=0;  		// par d�faut(0) �cran r�duit pour TP adressables
				F_aff_sauve_tpA=0;	// si 0 pas d'affichage: Sauvegarde ? (des mesures pour TP adressables)
				F_sauve_mes_tpA=0;	// bit pour m�mo sauvegarde valid�e des mesures des tp adressables
				F_aff_sauve_tpR=0;	// si 0 pas d'affichage: Sauvegarde ? (des mesures pour TP R�sistifs)
				F_sauve_mes_tpR=0;	// bit pour m�mo sauvegarde valid�e des mesures des tp R�sistifs
				F_aff_sauve_Deb=0;	// si 0 pas d'affichage: Sauvegarde ? d�bitm�tres
				F_sauve_mes_Deb=0;	// bit pour m�mo sauvegarde valid�e des mesures des d�bitm�tres
				Back_light = 0;
 				F_memo_back_light=0; 	//bit pour m�mo sauvegarde position back_light	
				Detection_TPA = 1;  // pour valider la d�tection des TPA sur ligne r�sistive
				memo_touche=0;

				HT_ON = 0;		// ARRET du convertisseur
				UN_CPT = 0; // convertisseur sur 80V OFF
				DECH_LIGN = 0;   // pas de d�charge ligne
				PONT_ON = 1;	// zener off
				CPT_ON = 0;		// d�valide ligne de mesure 
				Led_verte = 0;
				Led_jaune = 0;
				Led_rouge = 0;
				Num_Ligne = 0;
				mes_groupe = 0;
				mes_debit = 0;
				valid_sens = 0;  // validation de l'affichage et du r�glage de la sensibilit� dans la lecture des TPR
				flag_param=0;
				F_lecture_eep=0;

//				niveau_batt();   
//				boucle1(95,2);	// pour indicateur mesures en boucle des TPR et D�bim�tres
//				while(1);



/*				if (Val_Voie >100 && Val_Voie < 1){
				Val_Voie = 1;
				SetPos(25,30);
 				PutMessage("     "); 					
				displayNum (25,30,Val_Voie);					
*/

//				boucle_8(90,4);


///				jj=92;	
///				jj/=100;
///				ftoa (jj,DATAtoLCD,2);				// Convertir un float en une chaine de caract�res
///				SetPos(25,28);	//x , y							// Positionner le curseur	
/// 			PutFloat(DATAtoLCD);

//				while(1);
//  test E/L eeprom
//				Save_tpa_i2c();
// 				DELAY_MS (10);
//				Lect_tpa_i2c();
//				Led_verte =1;
//				while (1);





	// test � et � et �
/*
 				SetPos(10,32);								// Positionner le curseur
				PutMessage   (" Debitmetres    ");				  		// Ecrire un message  
				plot(23,32);	// pour afficher le "�"
				plot(22,33);		// pour afficher le "�"
 				plot(46,32);	// pour afficher le "�"
				plot(47,33);		// pour afficher le "�"
				while (1);
*/



//				plot(19,57);  // pour �
//				plot(20,56);  // pour �
//				plot(21,57);  // pour �
//				while(1);

				if (ValBuzzer)  
					{
				Buzzer = 1;
 				DELAY_MS (20);
				Buzzer = 0;
					}

// pour programmer sqr/out � 1Hz

//				DisplayTime();
//					reset_time(1,2,1,1,3,3,3);					/* � valider en premier pour d�marer l'horloge puis d�valider ensuite
//				Set_Horloge(); // pour programmer l'horloge     /* puis en second pour mettre � l'heure
//					DELAY_MS(400);
//		    	ClearScreen ();
				


	//pour afficher l'horloge au d�marrage
 				 if(touche_BP1 == 0 && touche_BP2 == 0 && touche_BP4==1)
					{
			  while(touche_BP5 == 1 )   //attente BP5 press�e
   							{
					DisplayTime();
							}
				  while(touche_BP5 == 0 );   //attente BP5 relach�e
					}
 	//FIN pour afficher l'horloge au d�marrage

	//pour programmer l'horloge au d�marrage
 				 if(touche_BP1 == 0 && touche_BP2 == 0 && touche_BP4==0)
					{
			  while(touche_BP5 == 1 )   //attente BP5 press�e
   							{
				Set_Horloge(); // pour programmer l'horloge 
							}
				  while(touche_BP5 == 0 );   //attente BP5 relach�e
					}
 	//FIN pour programmer l'horloge au d�marrage





				ClearScreen ();
//				niveau_batt();   //Affiche la jauge batterie
				memo_ecran = 0; 
				DELAY_500MS (1);

			// Test si convertisseur 48V fonctionne 
				if (Detection_TPA_V1_86==0) //V1.86
				Test_48V();
				// le programme s'arr�te ici si anomalie 48v
 				DELAY_MS (250);

				// Test des alimentations
//				SetPos(20,20);		//x , y						 
//				PutMessage("Test des alimentations");				  	 
//				Test_Tensions(); 	
 
/*				// test d�charge ligne
				DECH_LIGN = 1;   // d�charge ligne
				Led_rouge = 1;	// on
 				DELAY_MS (1000);
				DECH_LIGN = 0;   // pas de d�charge ligne
				Led_rouge = 0;	// off
				while(1);
*/

//**********************
				if (Detection_TPA_V1_86==0) //V1.86
				PutLogo((rom far unsigned char*)ADECEF_MCTA);	// Afficher une image (logiciel BitMapToLCD pour la conversion)
				niveau_batt();   //Affiche la jauge batterie
//				reset_leds();
 
				if (Detection_TPA_V1_86==0) //V1.86
				DELAY_MS (1500);

				

				ClearScreen ();
				Mesure_V_Batt();
				V_Batt =(int)(V_Batt_Calc*10);				// 
				if (V_Batt<=69){
				SetPos(20,30);								// Positionner le curseur
				PutMessage("BATTERIE FAIBLE !");					// Ecrire un message  nu_retour


				if (Detection_TPA_V1_86==0) //V1.86
 				DELAY_MS (4000);


				}				

				// mise ne route horloge 1hz pour synchroniser la mesure des capteurs
				set_time(0x07,0x10); // adrese= 0x07 valeur 0x00 pour stopper SQR/OUT et 0x10 pour valider � 1Hz

				memo_touche = 0;


 while (1)  //BOUCLE PRINCIPALE DU MAIN
	{
				if (Detection_TPA_V1_86==1) //V1.86
				{
				Detection_TPA_V1_87=0;	
				Detection_TPA_V1_86=2;	
				goto TPADETECT;
				}				// V1.86

		



				menu_principal();
				memo_ecran = 0;
//				memo_touche = 0;
 				DELAY_MS (10);
				
			
				

			  	while (touche_BP6 == 0);
 				DELAY_MS (1);
			  while (touche_BP6 != 0)

				{
				flip_flop_back_light();		//permet de changer la luminosit� du back-light par BP1 et BP2 simultann�es

			//	ici test si pr�sence RS322

		
				switch (memo_touche)
						    {
						case 0:
				menu_principal_C0 ();   // TP adressables
//					F_lecture_eep=0;  // permet de relire l'eeprom pour les valeurs de la derni�re mesure des  TPA 
						break;
						case 1:
				menu_principal_C1 ();	// TP r�sistifs
						break;
 						case 2:
				menu_principal_C2 ();	// D�bitm�tres
						break;
						case 3:
				menu_principal_C3 ();	// Groupe de pressu
						break;
						case 4:
				menu_principal_C4 ();	// Lecture M�moire
						break;
						case 5:
				menu_principal_C5 ();	// Param�trages
						break;
					    }
    		 	if (touche_BP0 ==0||touche_BP3==0)
					{
	   				if (touche_BP3 == 0 && memo_touche <=4)   //
						{
						memo_touche +=1 ;
					while (touche_BP3 == 0);
						}
		   			if (touche_BP0 == 0 && memo_touche >0)
						{
						memo_touche -=1 ;

					while (touche_BP0 == 0);

						}
					}
				niveau_batt();   //Affiche la jauge batterie

				
				if (Detection_TPA_V1_86==2) //V1.86
				{
				Detection_TPA_V1_86=3;
				goto redic0;
				}


				}
redic0: //V1.86
				memo_ecran = memo_touche+=1;


				while (touche_BP6 ==0 );	// pour eviter le scintillement de l'�cran

//#################################### CHOIX TP ADRESSABLES ###############################

//***************** CAPTEURS ADRESSABLES ************





	while (memo_ecran == 1 )	 
	 {
				menu_TPAD();				
 	
				if (Detection_TPA_V1_86==6) //V1.86
				{
				Detection_TPA_V1_86=7; //V1.86
				goto TPADETECT; //V1.86
				}//V1.86


			while (touche_BP5 ==0);		// pour retour menu pricipal depuis le menu des TP adressables
			memo_touche_1a = 0;
				compteurX=0;

		  	while (touche_BP6 && memo_ecran)
				{						


  			if (touche_BP5 ==0)			// pour retour menu pricipal depuis le menu des TP adressables
					{
			memo_ecran = 0;				//		"
				F_aff_sauve_tpA=0; 		// modif ajout du 5/4/2015				
				F_sauve_mes_tpA=0;	// bit pour m�mo sauvegarde valid�e des mesures des tp adressables  // modif ajout du 5/4/2015
				F_sauve_mes_tpA=0;	// bit pour m�mo sauvegarde valid�e des mesures des tp adressables // modif ajout du 5/4/2015
				F_sauve_mes_tpR=0;	// bit pour m�mo sauvegarde valid�e des mesures des tp r�sistifs // modif ajout du 5/4/2015
//			memo_touche = 0;			//		"
				}


					switch (memo_touche_1a)  		// S�lection pour transtest
					    {
						case 0:						// S�lection Mesure
				menu_TPAD_C0();
						break;
						case 1:
				menu_TPAD_C1();						// S�lection codage
						break;
// 						case 2:
//				menu_TPAD_C2();						// Lecture m�moire
//						break;
// 						case 3:
//				menu_TPAD_C3();						// RETOUR
//						break;
					    }
    			if (touche_BP0 ==0||touche_BP3==0)
				{
	   			if (touche_BP3 == 0 && memo_touche_1a ==0)   // avec 1 ligne menu en plus <=2
						{
						memo_touche_1a +=1 ;
				while (touche_BP3 == 0);
						}
		   			if (touche_BP0 == 0 && memo_touche_1a ==1)
						{
						memo_touche_1a -=1 ;
					while (touche_BP0 == 0);

						}
					}

				if (Detection_TPA_V1_86==3) //V1.86
				{
				Detection_TPA_V1_86=4; //V1.86
				goto redic1; //V1.86
				}//V1.86

			



				}




//				while (touche_BP6 ==1 );	// attente touche relach�e


//////////==>						} // de if (memo_ecran == 1)

   	if ((touche_BP6 ==0 && memo_touche_1a==0 && memo_touche!=0) )	//modif  

		{
// �cran adressable
redic1: //V1.86 				

				DELAY_MS (20);
	

				Tab_Type_Enr[0]=0;  			//1 octets  pour m�moriser le type d'enregistrement (TPA=0,Groupe=1,D�bitm�tre=2,TPR=3,Icra=4)  

//D�plac�e dans lecture param�tres
//==============>Mem_Num_Ligne= Rd_eeprom_int (ad_Mem_Num_Ligne);  // adresse pic0x54  memorisation du num�ro de ligne de mesure TPA pour affichage

		if (!F_lecture_eep)//pour ne passer qu'1 fois
			{
				Mem_Num_Ligne= Rd_eeprom_int (ad_Mem_Num_Ligne);  // adresse 0x54  memorisation du num�ro de ligne de mesure TPA pour affichage
				Nb_Capteurs_max = (char)Mem_Num_Ligne;		// nombre de capteurs max pour affichage				
				Num_Ligne = (char)Mem_Num_Ligne;		// num�ro de la ligne de mesure pour affichage	
			}
			else
			{
				Nb_Capteurs_max = (char)Mem_Num_Ligne;		// num�ro de la ligne de mesure pour affichage				
   			}



		if (!F_lecture_eep)//pour ne passer qu'1 fois
			{
				if (ConsAffTPA){    // POUR CONSERVER L'AFFICHAGE DE LA DERNIERE MESURE DES TPA QUAND ON QUITTE LE MODE MESURE ET QUE L'ON Y REVIENT
				//toto recharge derni�re mesure
				Lect_MesEnCours_tpa_i2c(adr_mesureTPA); // relecture des derni�res valeurs en eeprom i2c ATTENTION DIFFERENT DE L'ENRGISTREMENT AVEC N� ET ADRESSE MOBILE  	ICI UNE ADRESSE FIXE EST SPECIFIEE DANS MEMO.C
//				Mode_Scrolling = 1;
//				Scrolling_Ecran_tpa(); //r�affichage valeurs!toto
				}
				else{    		// si en mode  CONSERVER L'AFFICHAGE DE LA DERNIERE MESURE DES TPA
				Init_Tab_tpa();  // reset des tableaux  !� �viter pour r�affichage des valeurs!  
//				Mode_Scrolling = 1;
				ClearScreen ();								// Effacement de tout l'�cran
				F_aff_sauve_tpA=0;	// positionne le bit � 0 pour ne pas afficher Sauvegarde ?
				}			

				// Pres_Debit;   indique pr�sence d�bitm�tre pour affichage  0=absent,  	1= pr�sent, 	 2 = pas encore mesur�
				Pres_Debit=2;   

//				F_sauve_mes_tpa=0;	// bit pour m�mo sauvegarde valid�e des mesures des tp adressables
				F_retour_sauve=0;  // indique le retour de sauvegarde si � 1


				ClearScreen ();
			if (F_ecran_adres )		// affichage �cran complet ou reduit selon valeur du bit
					{
				ecran_adressables_complet();
					}
				else
					{
				ecran_adressables_reduit();
					}   
			F_lecture_eep=1;

		}

///////				Num_Ligne=3;
				Scrolling_Ecran_tpa(); //� ajouter si on supprime la ligne dessus =>	Init_Tab_tpa();  // reset des tableaux  !� supprimer pour r�affichage valeurs!


		while ((touche_BP5 != 0 && F_retour_sauve==0)&& compteurX<=3)  // RETOUR par touche BP5  
			{
				SetPos(0,56);								// Positionner le curseur
				PutMessage("Pret Mesures");				  		// Ecrire un message  L=17caract
				plot(13,57); //pour �
				plot(14,56); //pour �
				plot(15,57); //pour �
 				DELAY_MS (100);

/*				if (F_sauve_mes_tpA==0)
				{
				SetPos(0,60);								// Positionner le curseur
				PutMessage("Pret mesures");				  		// Ecrire un message  L=17caract
				SetPos(0,0);	// Positionne le curseur
//				F_aff_sauve_tpA=1;
				
								
				if (F_aff_sauve_tpA	==1)
					{
//				SetPos(0,0);	// Positionne le curseur
//				PutMessage("Sauvegarde ?        ");				  		// Ecrire un message  
					}
				}
*/
			// choix affichage �cran complet ou reduit avec les touches < ou >
			if (touche_BP2 == 0 ||touche_BP1 == 0 ) 
				{
				if (touche_BP2 == 0)  // si touche > alors �crans complet
					{ 
			  F_ecran_adres=1;						// affichage �cran complet (bit = 1)
					}
				while (touche_BP2 == 0);

				if (touche_BP1 == 0)  // si touche < alors �crans r�duit
					{ 
	     	    F_ecran_adres=0;						// affichage �cran r�duit (bit = 0)
					}
				Scrolling_Ecran_tpa();

				while (touche_BP1== 0 || touche_BP2== 0);           
				}


// *************scrolling vertical si > 3 mesures***************
			if(touche_BP3== 0 || touche_BP0== 0)
				{
 				DELAY_MS (1);

//				if (touche_BP0==0 && Num_Ligne>3 && F_aff_sauve_tpA )    	// scrolling vertical si > 3 mesures et si scrutation termin�e	&& F_sup3mes_tpa
				if (touche_BP0==0 && Num_Ligne>3 )    	// scrolling vertical si > 3 mesures et si scrutation termin�e	&& F_sup3mes_tpa
					{
					Num_Ligne--;
					}
//				if (touche_BP3==0  && F_aff_sauve_tpA && Num_Ligne<Nb_Capteurs_max && Num_Ligne<16)		// scrolling vertical si > 3 mesures et si scrutation termin�e   (Nb_Capteurs_max-1)
				if (touche_BP3==0  && Num_Ligne<Nb_Capteurs_max && Num_Ligne<16)		// scrolling vertical si > 3 mesures et si scrutation termin�e   (Nb_Capteurs_max-1)
					{
					Num_Ligne++;
					}
				if (touche_BP0==0||touche_BP3==0)	
					{
					Mode_Scrolling = 1;
					Scrolling_Ecran_tpa();

				if(F_aff_sauve_tpA==1)
				{
				SetPos(0,56);								
				PutMessage("Pret Mesures/Enregistrer");				  	
				plot(13,57);
				plot(14,56);
				plot(15,57);
				}		

				while (touche_BP3== 0 || touche_BP0== 0);           
					}
				}
// *************FIN    scrolling vertical si > 3 mesures***************



				//-----------------SAUVEGARDE DES MESURES-----------------
				if ((touche_BP6==0) && (F_aff_sauve_tpA==1))		// 	validation de la sauvegarde des mesures	
					{
				compteurX=0;
				F_sauve_mes_tpA=1;		// bit pour m�mo sauvegarde valid�e des mesures des tp adressables
				F_sauve_mes_tpR=0;		// bit pour m�mo sauvegarde d�valid�e des mesures des tp r�sistifs
				F_sauve_mes_Deb=0;		// idem pour d�bitm�tre
				while(touche_BP6==0);		// attente touche relach�e
				
				if (coche_constit==0){
				ClearScreen ();								// Effacement de tout l'�cran
				SetPos(0,0);
  				PutMessage("TP Adressables");
				Ecran_Constitution();
//   				ecran_memo_TPadressables();       //sauvegarde les infos de constitution et des capteurs
								}					
				ClearScreen ();	
				Sauvegarde_TA_TR_Deb();				//sauvegarde seulement les infos des capteurs sans la constitution
				F_retour_sauve=0;  // indique fin de sauvegarde  <=================================== � voir
 				DELAY_MS (20);
				while (compteurX<=3){
					compteurX+=1;
 				DELAY_MS (200);
							}				
					}

				//-----------------------

				
				if (Detection_TPA_V1_86==4) //V1.86
				{
				Detection_TPA_V1_86=5; //V1.86
				goto redic2; //V1.86
				}//V1.86



				if (touche_BP4 == 0 )
				{
redic2: //V1.86
				Init_Tab_tpa();  // reset des tableaux            

				// Pres_Debit;   indique pr�sence d�bitm�tre pour affichage  0=absent,  	1= pr�sent, 	 2 = pas encore mesur�
				Pres_Debit=2 ;  // reset pr�sence d�bitm�tre pour affichage

				Boucle_TP = 0;				//bit pour indiquer si scrutation
				Num_Capteur =0;
				F_retour_sauve=0;  // reset le retour de sauvegarde si � 1
				while (touche_BP4 ==0);
				ClearScreen ();								// Effacement de tout l'�cran
				if (F_ecran_adres)						// r�affichage �cran s�lectionn� complet ou r�duit				
					{
				ecran_adressables_complet();				// affiche l'�cran complet
					}
				else
					{
				ecran_adressables_reduit();					//affiche l'�cran r�duit
					}
			
				SetPos(0,56);								// Positionner le curseur
				PutMessage("Mesures en cours ");				  		// Ecrire un message  L=17 caract�res
 				Boucle_TP=1;
				
				DECH_LIGN = 1;   // D�charge la ligne
				DELAY_MS (100);

///				while (Boucle_TP==1)
///			{

				//ICI LE PROGRAMME DE MESURES DES TPA   <====================================================================================

				mes_groupe = 0; 		// pour indiquer que c'est une mesure de pcpg et non de groupe de pressu						 
				Boucle_Mesure_PCPG();	// BOUCLE DE MESURE DES CAPTEURS ADRESSABLE, DEBITMETRE et PARAMETRES GROUPE
				F_aff_sauve_tpA=1;				// positionne le bit � 1 pour afficher la demande de sauvegarde 
				F_new_mes=1;
//pr�voir ici une sauvegarde en eeprom_i2c pour conserver l'affichage de la derni�re mesure si quitte l'interrogation des tpa 



	




				if (touche_BP4 == 0 )		
					{
				SetPos(0,56);								// Positionner le curseur
				PutMessage("Fin des Mesures  ");				  		// Ecrire un message  L=17 caract
 				Boucle_TP=0;
				Num_Capteur = 0; 
///				DELAY_MS (100);

		
				
				

					}
 				while (touche_BP4 ==0);  // attente touche relach�e
///				DELAY_MS (200);
///				}	// fin ModeScrut		
 			}// fin if BP4=0
	



	


			
		}	// fin while (touche_BP5 != 0)										
//			if(F_retour_sauve==0)
//			{
//			break;
//			}


				F_aff_sauve_tpA=0; 		// modif ajout du 5/4/2015				
				F_sauve_mes_tpA=0;	// bit pour m�mo sauvegarde valid�e des mesures des tp adressables // modif ajout du 5/4/2015
				F_sauve_mes_tpR=0;	// bit pour m�mo sauvegarde valid�e des mesures des tp r�sistifs // modif ajout du 5/4/2015

				F_aff_sauve_Deb=0;  
				F_sauve_mes_Deb=0;

				if (ConsAffTPA&&F_new_mes)  {    // POUR CONSERVER L'AFFICHAGE DE LA DERNIERE MESURE DES TPA QUAND ON QUITTE LE MODE MESURE ET QUE L'ON Y REVIENT
												// F_new_mes permet d'indiquer une nouvelle mesure pour sauvegarder

		// sauvergarde mesure � l'adresse fixe
				Save_MesEnCours_tpa_i2c(adr_mesureTPA); // sauvegarde des derni�res valeurs en eeprom i2c ATTENTION DIFFERENT DE L'ENRGISTREMENT AVEC N� ET ADRESSE MOBILE  	ICI UNE ADRESSE FIXE EST SPECIFIEE DANS MEMO.C
		// sauvegarde �galement le num�ro de ligne pour affichage dans scrolling
				Wr_eeprom_int (ad_Mem_Num_Ligne,(int)Mem_Num_Ligne);  //valeur num�ro de ligne TPA pour r�affichage derni�res mesures TPA (num_ligne effac�e par une mesure debit ou groupe)
				F_new_mes=0;
				}
				else{
				DELAY_MS (200);
				}	
					ClearScreen ();   // NE PAS TOUCHER IMPORTANT


				if (Detection_TPA_V1_86==5) //V1.86
				{
				Detection_TPA_V1_86=6; //V1.86
				}//V1.86
			

		
   	}      // FIN if (touche_BP6 ==1 && memo_touche_1a==0 && memo_touche!=0)


       
//**************** ECRAN CODAGE *********

     	if (touche_BP6 ==0 && memo_touche_1a==1&& memo_touche!=0)	//modif
						{
				ClearScreen ();
				Code_Prog=1;
				Code_Lect=1;								
				ecran_codage();
//		    	displayHNum (95,25,annee);

// 				while (touche_BP6 !=0);
 				DELAY_MS (20);
			while (touche_BP5 !=0);	//RETOUR par la touche BP5  � voir quelle touche utiliser
						}
//**************** fin ECRAN CODAGE *********


//**************** ECRAN LECTURE MEMOIRE *********
/*
     	if (touche_BP6 ==1 && memo_touche_1a==2&& memo_touche!=0)	//modif
						{


				ClearScreen ();								
				if (coche_constit==0){
				ecran_lect_mem();
				}
				else {
				SetPos(10,24);								// Positionner le curseur
				PutMessage("Carte memoire absente");
				}
					
// 				while (touche_BP6 !=0);
 				while (touche_BP5 !=0);	//RETOUR par la touche BP5  � voir quelle touche utiliser
						}  */
//**************** fin lecture m�moire *********


//				while (touche_BP5 == 0);
//				ClearScreen ();	

		}  // FIN PARTIE TP ADRESSABLES


//#################################### FIN TP ADRESSABLES ###############################


//#################################### CHOIX TP RESISTIFS ############################

//**************** CAPTEURS RESISTIFS *********

				if (memo_ecran == 2)	
				    {
			Detection_TPA = 1;  // valeur par d�faut = 1 =  d�tection TPA valid�e  ajout, modif du 01/05/2016
				sensibilite_TPA = Rd_eeprom_int (ad_sensibilite_TPA); // lecture de la valeur de la sensibilit� sauvegard�e en eeprom
				ClearScreen ();								// Effacement de tout l'�cran

			 	while (touche_BP6 == 0);  // attend touche relach�e du menu
				
 // 			   	if (touche_BP6 ==1 )
//						{

				TPA_detecte=0;			// reset le flag pour indiquer pr�sence d'un TPA
				Tab_Type_Enr[0]=3;  			//1 octets  pour m�moriser le type d'enregistrement (TPA=0,Groupe=1,D�bitm�tre=2,TPR=3,Icra=4)  
				ecran_resistifs();
//				F_aff_sauve_tpR=1;				// positionne le bit � 1 pour afficher la demande de sauvegarde 


		  		while (touche_BP5 != 0) 	// RETOUR par touche BP5
				{
				mem_bcl_Tpr = boucleTPR;   		// m�mo si boucle ou pas


//***********  REGLAGE SENSIBILITE DE DETECTION des TPA dans la mesure des TPR  **********
				if ((!touche_BP1) && (!touche_BP2) && (!touche_BP6))
					{
					valid_sens=1;   // pour validation de l'affichage et r�glage sensibilit� dans mesure TPR
									// pour d�valider il suffit d'arr�ter et rallumer le MCT
				SetPos(8,8);
  				PutMessage("Sensibilite:                ");		// Ecrire un message  nu_retour
				displayNum (70,8,sensibilite_TPA); 
					}
				while ((!touche_BP1) && (!touche_BP2) && (!touche_BP6));


			//**
			if (valid_sens==1)  // validation de l'affichage et r�glage sensibilit�   N'est plus utilis�!!!
			 {


				if ((!touche_BP0)||(!touche_BP3))
						{
				SetPos(8,8);
  				PutMessage("Sensibilite:                ");		// Ecrire un message  nu_retour
						}

				if (!touche_BP0)
					{
				sensibilite_TPA+=10 ;
//				SetPos(8,8);
// 				PutMessage("Sensibilite:                ");		// Ecrire un message  nu_retour
				displayNum (70,8,sensibilite_TPA); 
				Wr_eeprom_int (ad_sensibilite_TPA,sensibilite_TPA); // Sensibilit� sauvegard�e en eeprom
					}
				if (!touche_BP3)
					{
				sensibilite_TPA-=10;
//				SetPos(8,8);
// 				PutMessage("Sensibilite:                ");		// Ecrire un message  nu_retour
				displayNum (70,8,sensibilite_TPA); 
				Wr_eeprom_int (ad_sensibilite_TPA,sensibilite_TPA); // Sensibilit� sauvegard�e en eeprom
					}				
				while ((!touche_BP0) || (!touche_BP3));
				displayNum (70,8,sensibilite_TPA);
			}
 

				if (F_aff_sauve_tpR==0){
				SetPos(0,56);								// Positionner le curseur
	     		PutMessage("Pret pour Mesure         ");	// ok 
				plot(13,57);
				plot(14,56);
				plot(15,57);
				DELAY_MS (100);
				}			  
//***********  FIN  REGLAGE SENSIBILITE DE DETECTION des TPA dans la mesure des TPR  **********

//*****************************Validation D�tection TPA dans une voie r�sistive ***********************************
				if (!touche_BP2){
					if (Detection_TPA_V1_87==0) //V1.87
					{	
					Detection_TPA = 1;  // valeur par d�faut = 1 =  d�tection valid�e
					TPA_detecte=0;
					ecran_resistifs();
					}
				}		
				if (!touche_BP1){ 
 					if (Detection_TPA_V1_87==0) //V1.87
					{
					Detection_TPA = 0;  //  d�tection d�valid�e
					TPA_detecte=0;
					ecran_resistifs();
 					}	
				}
				while (!touche_BP1 || !touche_BP2) ;
//*****************************FIN Validation D�tection TPA dans une voie r�sistive ***********************************

								
				if (!touche_BP4)
					{
				while (!touche_BP4 || mem_bcl_Tpr) 
						{
					TPA_detecte=0;
  					Led_rouge = 0;  // dans le cas o� allum�e en d�tection TPA
					ClearScreen ();								// Effacement de tout l'�cran
					if(Detection_TPA == 1){  //****** valeur par d�faut = 1 =  d�tection valid�e
					ecran_resistifs2();
					}
						else
					{
					ecran_resistifs();
					}	
					ecran_TPR_Mes();
					F_aff_sauve_tpR=1;				// positionne le bit � 1 pour afficher la demande de sauvegarde 
 					DELAY_MS (1500);

					if (!touche_BP4 ||  TPA_detecte)  // si on d�tecte un TPA on ne boucle pas sur la mesure
							{
						mem_bcl_Tpr=0;
						SetPos(0,56);								// Positionner le curseur
						PutMessage("Arret boucle de mesure  ");	// Ecrire un message  
				plot(19,57);  // pour �
				plot(20,56);  // pour �
				plot(21,57);  // pour �
	 					DELAY_MS (100);
						 	}     
					while (!touche_BP4);
	 					DELAY_MS (500);

//SI BOUCLAGE MESURE ON N'AFFICHE PAS
			if (mem_bcl_Tpr || !TPA_detecte)  // modif du 24/02/2016
					{
						if (F_aff_sauve_tpR==1 && !TPA_detecte )
						{
						SetPos(0,56);								// Positionner le curseur
						PutMessage("Pret Mesure/Enregistrer");
						plot(13,57);  // �
						plot(14,56); // �
						plot(15,57); // �
			    		}
					else if(TPA_detecte)
						{
						F_aff_sauve_tpR=0;
						SetPos(0,56);								// Positionner le curseur
			     		PutMessage("Pret pour Mesure         ");	// ok 
						plot(13,57);  // �
						plot(14,56); // �
						plot(15,57); // �
				DELAY_MS (500);								
						}	
  					Led_rouge = 0;  // dans le cas o� allum�e en d�tection TPA
					  }// FIN	 if (!mem_bcl_Tpr) 

					} // FIN  while (!touche_BP4 || mem_bcl_Tpr) 						

				} // FIN  	if (!touche_BP4)


				//-----------------SAUVEGARDE DES MESURES TPR----------------
				if ((touche_BP6==0) && (F_aff_sauve_tpR==1 && !TPA_detecte))		// 	validation de la sauvegarde de la mesure	
					{
				F_sauve_mes_tpR=1;		// sauvegarde valid�e des mesures des tp r�sistifs
				F_sauve_mes_tpA=0;		// sauvegarde d�valid�e des mesures des tpA
				F_sauve_mes_Deb=0;			// sauvegarde d�valid�e des mesures des d�bitm�tres
				while(touche_BP6==0);		// attente touche relach�e
				
				if (coche_constit==0){
   				ecran_memo_TPresistifs();       //renseigne les infos voie, comment, central
								}					
				ClearScreen ();	
				Sauvegarde_TA_TR_Deb();				//sauvegarde seulement les infos des capteurs sans la constitution
				F_retour_sauve=1;               // indique fin de sauvegarde
				ClearScreen ();								
				ecran_resistifs();
				F_aff_sauve_tpR=0;				// positionne le bit � 1 pour afficher la demande de sauvegarde apres la mesure TITITOTO
 				DELAY_MS (20);
					}

				//-----------------FIN SAUVEGARDE TPR--------------

				}  //	FIN 	while (touche_BP5 != 0) 	// RETOUR par touche BP5
				Led_rouge = 0; 
				memo_ecran = 0;
//						} // FIN while (touche_BP6 != 0)
					} // ==>   FIN  if (memo_ecran == 2)






//#################################### FIN TP RESISTIFS ###############################



//#################################### CHOIX DEBITMETRES  ###########################


				if (memo_ecran == 3)	
			{
			 	while (touche_BP6 == 0);
				
				TPA_detecte=0;
				ClearScreen ();								// Effacement de tout l'�cran
				Tab_Type_Enr[0]=2;  			//1 octets  pour m�moriser le type d'enregistrement (TPA=0,Groupe=1,D�bitm�tre=2,TPR=3,Icra=4)  
				ecran_debitmetres();
		 while (touche_BP5 != 0)   // RETOUR par touche BP5
			{
				if (!F_aff_sauve_Deb)  // si pas de boucle
					{ 
				mem_bcl_Deb = boucleDEB;
				SetPos(0,56);								// Positionner le curseur
				PutMessage("Pret Mesure ");	// Ecrire un message  
						plot(13,57);  // �
						plot(14,56); // �
						plot(15,57); // �
				DELAY_MS (200);
					}

				if (!touche_BP4)
						{
				mem_bcl_Deb = boucleDEB;    // m�mo si boucle ou pas
				while (!touche_BP4 || mem_bcl_Deb)
							{
				mes_debit=1;
				ecran_DEB_mes();
				F_aff_sauve_Deb=1;
				mes_debit=0;
				if (!touche_BP4){
					mem_bcl_Deb=0;
				FillWhiteZone(0,55,127,63);
				SetPos(0,56);								// Positionner le curseur
				PutMessage("Arret boucle de mesure");	// Ecrire un message  
					plot(19,57);  // pour �
					plot(20,56);  // pour �
					plot(21,57);  // pour �
				DELAY_MS (1000);
								}
				while (!touche_BP4);
//SI BOUCLAGE MESURE ON N'AFFICHE PAS
			if (!mem_bcl_Deb)  // modif du 24/02/2016
						{
						if (F_aff_sauve_Deb==1){
				FillWhiteZone(0,55,127,63);
						SetPos(0,56);								// Positionner le curseur
						PutMessage("Pret Mesure/Enregistrer");
						plot(13,57);  // �
						plot(14,56); // �
						plot(15,57); // �
				DELAY_MS (200);
								}	
						}

							} //FIN while (!touche_BP4 || mem_bcl_Deb)

						}// FIN	if (!touche_BP4)


				//-----------------SAUVEGARDE DES MESURES DES DEBITMETRES----------------
				if ((touche_BP6==0) && (F_aff_sauve_Deb==1))		// 	validation de la sauvegarde de la mesure	
					{
				F_sauve_mes_tpR=0;		// sauvegarde valid�e des mesures des tp r�sistifs
				F_sauve_mes_tpA=0;		// sauvegarde d�valid�e des mesures des tpA
				F_sauve_mes_Deb=1;			// sauvegarde d�valid�e des mesures des d�bitm�tres
				while(touche_BP6==0);		// attente touche relach�e
				
				if (coche_constit==0){
   				ecran_memo_Debitmetres();       //renseigne les infos voie, comment, central
								}					
				ClearScreen ();	
				Sauvegarde_TA_TR_Deb();				//sauvegarde seulement les infos des capteurs sans la constitution
				F_retour_sauve=1;               // indique fin de sauvegarde
				ClearScreen ();								
				ecran_debitmetres_memo();
				F_aff_sauve_Deb=0;				// positionne le bit � 1 pour afficher la demande de sauvegarde apres la mesure TITITOTO
 				DELAY_MS (20);
					}


					} // FIN while (touche_BP5 != 0)   // RETOUR par touche BP5

				memo_ecran = 0;
				F_lecture_eep=0;	// permet de relire l'eeprom pour les valeurs de la derni�re mesure des  TPA 
//				} // FIN while (touche_BP6 != 0)
			} // ==>   FIN  if (memo_ecran == 3)

//#################################### FIN DEBITMETRES ###############################


//#################################### CHOIX GROUPE  ###########################


	if (memo_ecran == 4)	
   {
  		while (touche_BP5 == 0); // RETOUR par touche BP5
			 	mes_groupe=0;  // utilis� dans l'�cran de la saisie de la constitution pour n'afficher que central........

			while (touche_BP6 == 0&& memo_ecran == 4);  

//  			   	if (touche_BP6 ==1 )
//						{
				ClearScreen ();								// Effacement de tout l'�cran
				Tab_Type_Enr[0]=1;  			//1 octets  pour m�moriser le type d'enregistrement (TPA=0,Groupe=1,D�bitm�tre=2,TPR=3,Icra=4)  
				ecran_groupe();
				niveau_batt();
				DELAY_MS (1000);
				SetPos(86,56);								// Positionner le curseur
				PutMessage("Pret    ");	// Ecrire un message  
				plot(99,57);  // pour �
				plot(100,56);  // pour �
				plot(101,57);  // pour �
				hline(118,127,56);
				compteurX=0;
		 	while (touche_BP5 != 0 )     //(retour ici apr�s sauvegarde groupe annul�e ou termin�e)
//		 	while (touche_BP5 != 0 && compteurX<=4)    /// � impl�menter et tester  //(retour ici apr�s sauvegarde groupe annul�e ou termin�e)
			{

 				DELAY_MS (20);
//				DELAY_MS (1000);      
//				compteurX+=1;
	
				SetPos(0,0);								// Positionner le curseur
//				PutMessage("GROUPE DE PRESSU");  
				if (!touche_BP4)
								{
				DELAY_MS (10);
				while (touche_BP4 ==0);
				ClearScreen ();							 
				ecran_groupe();
				mes_groupe=1; // pour indiquer (dans la boucle de mesure des pcpg) que c'est une mesure du groupe de pressu 
				SetPos(86,56);								// Positionner le curseur
				PutMessage("Lecture ");	// Ecrire un message  
				hline(118,127,56);
				Boucle_Mesure_PCPG();   // utilis�e pour mesurer les r�ponses de la voie 0 (groupe)
				DELAY_MS (200);
				SetPos(86,56);								// Positionner le curseur
				PutMessage("Fin     ");	// Ecrire un message  
				hline(118,127,56);
				DELAY_MS (1000);
				mes_groupe=0; // pour indiquer (dans la boucle de mesure des pcpg) que c'est une mesure du groupe de pressu 

//SUPPRIM� PROVISOIREMENT POUR NE PAS SAUVEGARDER TANT QUE LA LECTURE N'EST PAS PRETE
/*				SetPos(86,56);								// Positionner le curseur
				PutMessage("Pret enr");	// Ecrire un message  
				hline(118,127,56);
				DELAY_MS (1000);
*/
//FIN DE SUPPRIM� PROVISOIREMENT POUR NE PAS SAUVEGARDER TANT QUE LA LECTURE N'EST PAS PRETE
	
				while (!touche_BP4);
				DELAY_MS (50);

//				aff_mes_groupe();   ==> se trouve dans mesure
								}




//SUPPRIM� PROVISOIREMENT POUR NE PAS SAUVEGARDER TANT QUE LA LECTURE N'EST PAS PRETE
/*
				if (touche_BP6==0){ 		// 	validation de la sauvegarde des  mesures	
     				ClearScreen ();							 
							SetPos(0,0);								// Positionner le curseur
					mes_groupe =1; // utilis� dans l'�cran de la saisie de la constitution pour n'afficher que central........
				Ecran_Constitution();
				 	mes_groupe=0;  // utilis� dans l'�cran de la saisie de la constitution pour n'afficher que central........
				ClearScreen ();	
				Sauvegarde_TA_TR();				//sauvegarde  les infos des capteurs 
				compteurX=0      ; //compteur 
				while (touche_BP4 ==0);
									}					
*/
//FIN DE SUPPRIM� PROVISOIREMENT POUR NE PAS SAUVEGARDER TANT QUE LA LECTURE N'EST PAS PRETE




			}
				memo_ecran = 0;
				if (!ConsAffTPA){    // en mode CONSERVER L'AFFICHAGE DE LA DERNIERE MESURE , il ne faut pas effacer par  Init_Tab_tpa();
				Init_Tab_tpa();   // effacement des lectures pr�c�dentes pour ne pas afficher en mesures pcpg apres lecture groupe
				}	
				F_lecture_eep=0;	// permet de relire l'eeprom pour les valeurs de la derni�re mesure des  TPA 
//						} // FIN while (touche_BP6 != 0)
		
		} // ==>   FIN  if (memo_ecran == 4)

//#################################### FIN GROUPE ###############################



//#################################### CHOIX  MEMOIRE  ###########################
	if (memo_ecran == 5)	
	{
				ClearScreen ();			// Effacement de tout l'�cran
 				ecran_memoire();
			while (touche_BP5 ==0);		// pour retour menu pricipal depuis le menu parametrage
			memo_touche_5a = 0;

  	while (touche_BP6 != 0 && memo_ecran ==5)  //
				{						


  			if (touche_BP5 ==0)			// pour retour menu pricipal depuis le menu parametrage
					{
			memo_ecran = 0;				//		"
//			memo_touche = 0;			//		"
	 				}	

					switch (memo_touche_5a)  		// S�lection pour param�trages
						    {
						case 0:					
					menu_memoire_C0();				    // lecture memoire		 
						break;
						case 1:
			    	menu_memoire_C1();					// raz memoire	Dernier Enr.
						break;
						case 2:
			    	menu_memoire_C2();					// raz memoire	Tous les Enr.
						break;
					    	}
    			if (touche_BP0 ==0||touche_BP3==0)
				   {
	   			if (touche_BP3 == 0 && memo_touche_5a <2)   
						{
						memo_touche_5a +=1 ;
				while (touche_BP3 == 0);
						}
		   			if (touche_BP0 == 0 && memo_touche_5a >0)
						{
						memo_touche_5a -=1 ;
					while (touche_BP0 == 0);
						}

					}
//			}




	

//**************** ECRAN LECTURE MEMOIRE *********

     	if (touche_BP6 ==0 && memo_touche_5a==0 )	//&& memo_touche!=0
						{
			     	ClearScreen ();								

				if (No_der_enreg==0)
				{
				ClearScreen ();								// Effacement de tout l'�cran
 				SetPos(10,24);
 		 		PutMessage("Aucun Enregistrement!");
				DELAY_MS (1000);
				while (touche_BP6 ==0);
				}
				else{
			    ecran_lect_mem(); // pour afficher les enregistrements 
//				while (!touche_BP5);	//ATTEND  touche BP5 relach�e
// 				DELAY_MS (20);
				}	
				while (!touche_BP5);	//attend  touche BP5 
				DELAY_MS (20);

				ClearScreen ();			// Effacement de tout l'�cran
 				ecran_memoire();
						}
//**************** FIN ECRAN lECTURE MEMOIRE *********

/*
	if (No_der_enreg==0)
	{
				ClearScreen ();								// Effacement de tout l'�cran
 				SetPos(10,24);
 		 		PutMessage("Aucun Enregistrement!");
				DELAY_MS (1000);
//				while (touche_BP6 ==0);
	}
*/	
//**************** ECRAN RAZ Der Enregristrement *********

     	if (touche_BP6 ==0 && memo_touche_5a==1 && No_der_enreg>0)	
						{
		     	ClearScreen ();								
				F_der_tous=0;   // SI EFFACEMENT DU DERNIER ENREGISTREMENT
				ecran_Raz_Memoire(F_der_tous); // pour effacer les enregistrements 
//				while (touche_BP5);	//RETOUR par la touche BP5 
				ClearScreen ();			// Effacement de tout l'�cran
 				ecran_memoire();
						}
			
//**************** FIN RAZ der enr MEMOIRE *********


//**************** ECRAN RAZ tous les Enregristrements *********

     	if (touche_BP6 ==0 && memo_touche_5a==2  && No_der_enreg>0)	
						{
		     	ClearScreen ();								
				F_der_tous=1;   // SI EFFACEMENT DE TOUS LES ENREGISTREMENTS
				ecran_Raz_Memoire(F_der_tous); // pour effacer les enregistrements 
				ClearScreen ();			// Effacement de tout l'�cran
 				ecran_memoire();
//				while (touche_BP5);	//RETOUR par la touche BP5 
						}
//**************** FIN RAZ lECTURE MEMOIRE *********


//				while (touche_BP6 ==0 );	// attente touche relach�e

	if (touche_BP6 ==0 && (memo_touche_5a==2 || memo_touche_5a==1) && No_der_enreg==0)
	{
				ClearScreen ();								// Effacement de tout l'�cran
 				SetPos(10,24);
 		 		PutMessage("Aucun Enregistrement!");
 				DELAY_MS (1000);
				ClearScreen ();								// Effacement de tout l'�cran
				ecran_memoire();
//				while (touche_BP6 ==0);
	}



	} //FIN de while (touche_BP6 != 0 && memo_ecran ==5)  

}// FIN de 	if (memo_ecran == 5)

//#################################### FIN  MEMOIRE ###############################




//#################################### CHOIX PARAMETRAGES  ###########################


	while (memo_ecran == 6 )	// en cours de test IF ou WHILE?
		 {
				ClearScreen ();			// Effacement de tout l'�cran
				ecran_param();
 	
			while (touche_BP5 ==0);		// pour retour menu pricipal depuis le menu parametrage
			memo_touche_6a = 0;

		  	while (touche_BP6 != 0 && memo_ecran == 6)
				{						


  			if (touche_BP5 ==0)			// pour retour menu pricipal depuis le menu parametrage
					{
			memo_ecran = 0;				//		"
//			memo_touche-=1;			//		"
					}


					switch (memo_touche_6a)  		// S�lection pour param�trages
						    {
						case 0:					
				menu_param_C0();						//Configuration 
						break;
						case 1:
				menu_param_C1();					// Param�trage horloge devient // Configuration ADECEF
						break;
// 						case 2:		// modif du 10/02/2016	
//				menu_param_C2();	// modif du 10/02/2016						// Configuration ADECEF
//						break;		// modif du 10/02/2016	
// 						case 3:
//				menu_param_C3();					// 	LIBRE
//						break;
					    }
    			if (touche_BP0 ==0||touche_BP3==0)
				   {
//	   			if (touche_BP3 == 0 && memo_touche_6a <=1)   // avec 1 ligne menu en plus <=2
	   			if (touche_BP3 == 0 && memo_touche_6a ==0)   // modif du 10/02/2016
						{
						memo_touche_6a +=1 ;
				while (touche_BP3 == 0);
						}
		   			if (touche_BP0 == 0 && memo_touche_6a >0)
						{
						memo_touche_6a -=1 ;
					while (touche_BP0 == 0);

						}
					}

				}
				while (touche_BP6 ==0 );	// attente touche relach�e




//**************** ECRAN Configuration *********

     	if (touche_BP6 ==1 && memo_touche_6a==0&& memo_touche!=0)	//modif
						{
			     	ClearScreen ();								
				ecran_Configuration(); // pour configurer certains param�tres d'utilisation
			     	ClearScreen ();								
				while (!touche_BP5);	//RETOUR par la touche BP5 
						}
//**************** fin ECRAN Configuration *********


//**************** ECRAN r�serv� ADECEF *********

     	if (touche_BP6 ==1 && memo_touche_6a==1&& memo_touche!=0)	//modif
						{
			     	ClearScreen ();
								
//				config_adecef=0; //d�valide autorisation config adecef
				ecran_adecef(); // pour entrer le mot de passe
				if  (config_adecef==1)
					{
				ecran_configAT();
				config_adecef=0;
					}						
 			     	ClearScreen ();								
				while (!touche_BP6);
				while (!touche_BP5);	//RETOUR par la touche BP5 
						}
//**************** fin ECRAN r�serv� ADECEF *********
/*
//**************** ECRAN libre *********

     	if (touche_BP6 ==1 && memo_touche_6a==3&& memo_touche!=0)	//modif
						{
			     	ClearScreen ();								
				ecran_Libre!!!(); // pour effacer les enregistrements en m�moire
				while (touche_BP5 !=0);	//RETOUR par la touche BP5 
						}
//**************** fin ECRAN RAZ m�moire *********
*/



			}  // fin ecran param�trages

//#################################### FIN PARAMETRAGES ###############################

	memo_touche-=1;
 	}  		  // fin WHILE(1)

} 		  // fin MAIN()





/*

//#################################### CHOIX PARAMETRAGES  ###########################
	if (memo_ecran == 6)
	{
			memo_touche_6a = 0;



	while ((touche_BP5 && memo_ecran == 6) || flag_param==1)	
		 {
				ClearScreen ();			// Effacement de tout l'�cran
				ecran_param();
 				DELAY_MS (1);
//			while (touche_BP5 ==0);		// pour retour menu pricipal depuis le menu parametrage
//			memo_touche_6a = 0;
//			memo_touche=5;
// 				DELAY_MS (20);
	

	  	while (touche_BP6 != 0 && memo_ecran == 6)
				{

  			if (touche_BP5 ==0)			// pour retour menu pricipal depuis le menu parametrage
					{
	     	ClearScreen ();								
			memo_ecran = 0;				//		"
//			memo_touche_6a = 0;
			memo_touche=5;
	 		flag_param+=1;
		}	
 				DELAY_MS (20);



					switch (memo_touche_6a)  		// S�lection pour param�trages
						    {
						case 0:					
				menu_param_C0();						//Configuration 
						break;
						case 1:
				menu_param_C1();					// Param�trage horloge devient // Configuration ADECEF
						break;
// 						case 2:		// modif du 10/02/2016	
//				menu_param_C2();	// modif du 10/02/2016						// Configuration ADECEF
//						break;		// modif du 10/02/2016	
// 						case 3:
//				menu_param_C3();					// 	LIBRE
//						break;
					    }
    			if (touche_BP0 ==0||touche_BP3==0)
				   {
//	   			if (touche_BP3 == 0 && memo_touche_6a <=1)   // avec 1 ligne menu en plus <=2
	   			if (touche_BP3 == 0 && memo_touche_6a ==0)   // modif du 10/02/2016
						{
						memo_touche_6a +=1 ;
				while (touche_BP3 == 0);
						}
		   			if (touche_BP0 == 0 && memo_touche_6a >0)
						{
						memo_touche_6a -=1 ;
					while (touche_BP0 == 0);

						}
					}

				}
				while (touche_BP6 ==0 );	// attente touche relach�e




//**************** ECRAN Configuration *********

     	if (touche_BP6==1 && touche_BP5!=0 && memo_touche_6a==0)	//modif  && memo_touche!=0
						{
					flag_param=1;
			     	ClearScreen ();								
				ecran_Configuration(); // pour configurer certains param�tres d'utilisation
				while (!touche_BP6);	
						}
//**************** fin ECRAN Configuration *********

//**************** ECRAN r�serv� ADECEF *********

     	if (touche_BP6 ==1 && touche_BP5!=0 && memo_touche_6a==1)	//modif  && memo_touche!=0
				{
			     	ClearScreen ();
								
//				config_adecef=0; //d�valide autorisation config adecef
				ecran_adecef(); // pour entrer le mot de passe
//				while (touche_BP6!=0 && touche_BP5);
				if  (config_adecef==1)
					{ 
				ecran_configAT();
				config_adecef=0;
					}						
				while (!touche_BP6);	//
				}
 				DELAY_MS (1);
 				DELAY_MS (1);
//**************** fin ECRAN r�serv� ADECEF *********

		}  // fin 	while (touche_BP5 && memo_ecran == 6 )
	     	ClearScreen ();			
			memo_touche=5;
			if( flag_param==2){
			memo_ecran=6;
				}
//#################################### FIN PARAMETRAGES ###############################

		}	// 	FIN de if (memo_ecran == 6)	


 	}  		  // fin WHILE(1)

} 		  // fin MAIN()

*/







void LECTURE_PARAM(void)   // lecture des param�tres de config stock�s en eeprom du pic
{
char i;
				ReadEEPROM(0x00);  //return EEDATA
				coche_constit=EEDATA;
				DELAY_MS (5);
				ReadEEPROM(0x01);  //return EEDATA
				val1TP=EEDATA;
				DELAY_MS (5);
				ReadEEPROM(0x02);  //return EEDATA
				boucleTPR=EEDATA;
				DELAY_MS (5);
				ReadEEPROM(0x03);  //return EEDATA
				boucleDEB=EEDATA;
				DELAY_MS (5);
				ReadEEPROM(0x04);  //return EEDATA
				ConsAffTPA=EEDATA;
				DELAY_MS (5);
				ReadEEPROM(0x05);  //return EEDATA
				ValBuzzer=EEDATA;
				DELAY_MS (5);
				ReadEEPROM(0x06);  //return EEDATA
				Verif_codageAT=EEDATA;
				DELAY_MS (5);
				ReadEEPROM(0x07);  //return EEDATA
				Aff_Debit_tpa=EEDATA;   //  Aff_Debit_tpa==0  => l'affichage D�bit n'est pas bloqu� dans �cran TPA
				DELAY_MS (5);
//ad_der_central  	adresse 0x10  � 0x0D  dans l'eeprom du pic  Nom du dernier central



				ReadEEPROM(0x20);  //return EEDATA
				Coeff_I_Modul=EEDATA;//Coeff_I_Modul   	adresse 0x20 pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53  	valeur max 255!!
				if (Coeff_I_Modul==0)
					{
				WriteEEPROM(0x20,100); // Si valeur � 0 on met par d�faut le coeff 1 donc 100   O est impossible
				Coeff_I_Modul=100;
					}
				DELAY_MS (5);

				ReadEEPROM(0x21);  //return EEDATA
				Coeff_I_Conso=EEDATA;//Coeff_I_Conso 	adresse 0x21 pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53			valeur max 255!!
				if (Coeff_I_Conso==0)
					{
				WriteEEPROM(0x21,100); // Si valeur � 0 on met par d�faut le coeff 1 donc 100   O est impossible
				Coeff_I_Conso=100;
					}
				DELAY_MS (5);

				ReadEEPROM(0x22);  //Coeff_I_Repos
				Coeff_I_Repos=EEDATA;//Coeff_I_Repos 	adresse 0x22 pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53			valeur max 255!!
				if (Coeff_I_Repos==0)
					{
				WriteEEPROM(0x22,100); // Si valeur � 0 on met par d�faut le coeff 1 donc 100   O est impossible
				Coeff_I_Repos=100;
					}
				DELAY_MS (5);

				ReadEEPROM(0x23);  //Coeff_V_TPR
				Coeff_V_TPR=EEDATA;//Coeff_V_TPR 	adresse 0x22 pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53			valeur max 255!!
				if (Coeff_V_TPR==0)
					{
				WriteEEPROM(0x23,100); // Si valeur � 0 on met par d�faut le coeff 1 donc 100   O est impossible
				Coeff_V_TPR=100;
					}
				DELAY_MS (5);

				ReadEEPROM(0x24);  //Mot de passe valeur 1
				Code_Param[0] = EEDATA;
				DELAY_MS (5);
				ReadEEPROM(0x25);  //Mot de passe valeur 2
				Code_Param[1] = EEDATA;
				DELAY_MS (5);
				ReadEEPROM(0x26);  //Mot de passe valeur 3
				Code_Param[2] = EEDATA;
				DELAY_MS (5);
				ReadEEPROM(0x27);  //Mot de passe valeur 4
				Code_Param[3] = EEDATA;
				DELAY_MS (5);
//				ReadEEPROM(0x28);  //Mot de passe valeur 5
//				Code_Param[4] = EEDATA;
//				DELAY_MS (5);
//				ReadEEPROM(0x29);  //Mot de passe valeur 6
//				Code_Param[5] = EEDATA;
//				DELAY_MS (5);

//(*)(Code_Param[num]) = 2;
//Code_Param[num]

				No_der_enreg = Rd_eeprom_int (ad_No_der_enreg); //adresse 0x50
				Der_Enreg = Rd_eeprom_int (No_der_enreg);       //adresse 0x52
				Mem_Num_Ligne= Rd_eeprom_int (ad_Mem_Num_Ligne);  // adresse 0x54  memorisation du num�ro de ligne de mesure TPA pour affichage


}


void Init_Tab_tpa(void)  //reset tableau de mesures des tpa
{
int i,j;
			for (i=0;i<=16;i++)
			{
		
			Tab_Code[i]=0;			//17 caract�res	  1 d�bitmetre et 16 TP
			Tab_Pression[i]=0;		//17 caract�res	
			Tab_I_Repos[i]=0;		//17 caract�res	  
			Tab_I_Modul[i]=0;		//17 caract�res		
			Tab_I_Conso[i]=0;		//34 caract�res	
			Tab_Etat[i]=0;			//17 caract�res	
//			Tab_Etat[i]=0xFF;			//17 caract�res	 pour essai (tpa non mesur� )

				for (j=0;j<=9;j++)   //16 X 10 caract�res
						{
				Tab_Constit[i][j]=32;  // 32 = espace
						}	
			}

				for (j=0;j<=9;j++)  //10 caract�res
					{
				Tab_Horodatage[j]=32;	 // 32 = espace
				Tab_VoieRobTete[j]=32;	 // 32 = espace
					}						


				for (j=0;j<=13;j++)  //14 caract�res
					{
				Tab_Central[j]=32;	 // 32 = espace
					}						
}




/**********************************************************************
 * INIT DU PORT I2C
 *
 * @return void
 **********************************************************************/

void Init_I2C(void)
{
//	TRISCbits.TRISC3 = 1;
//	TRISCbits.TRISC4 = 1;

  //here is the I2C setup from the Seeval32 code.
	DDRCbits.RC3 = 1; //Configure SCL as Input
	DDRCbits.RC4 = 1; //Configure SDA as Input
	SSP1STAT = 0x80;   //Disable SMBus & Slew Rate Control 80
	SSP1CON1 = 0x28;  //Enable MSSP Master  28
	SSP1CON2 = 0x00;   //Clear MSSP Conrol Bits
	SSP1ADD  = 0x3B;    // 0x3B for 100kHz  (24mhz/1OOkHz)-1 = 59 ou 3B en hex
}


/**********************************************************************
 * INIT DU PORT RS232
 *
 * @return void
 **********************************************************************/
/*
EXAMPLE 20-1: CALCULATING BAUD RATE ERROR
For a device with FOSC of 16 MHz, desired baud rate of 9600, Asynchronous mode, 8-bit BRG:
Desired Baud Rate = FOSC/(64 ([SPBRGHx:SPBRGx] + 1))
Solving for SPBRGHx:SPBRGx:
X = ((FOSC/Desired Baud Rate)/64) � 1
= ((16000000/9600)/64) � 1
= [25.042] = 25
Calculated Baud Rate= 16000000/(64 (25 + 1))
= 9615
Error = (Calculated Baud Rate � Desired Baud Rate)/Desired Baud Rate
= (9615 � 9600)/9600 = 0.16%
*/
/*
For a device with FOSC of 40 MHz, desired baud rate of 9600, Asynchronous mode, 16-bit BRG:
BRG16 =1  et  BRGH = 1 et SYN = 0   => 16-bit/Asynchronous
FOSC/[16 (n + 1)]
X = ((FOSC/Desired Baud Rate)/4) � 1
X = ((40000000/9600)/4) � 1 = 1040.66

For a device with FOSC of 24 MHz, desired baud rate of 9600, Asynchronous mode, 16-bit BRG:
BRG16 =1  et  BRGH = 1 et SYN = 0   => 16-bit/Asynchronous
FOSC/[16 (n + 1)]
X = ((FOSC/Desired Baud Rate)/4) � 1
X = ((24000000/9600)/4) � 1 = 624

*/

void Init_RS232(void)
{
unsigned int spbrg ; // variable pour la d�termination de la vitesse de transmission

	

			/* Reset USART registers to POR state */
			TXSTA1 = 0; 
			RCSTA1 = 0;
			BAUDCON1 = 0;
		
		// Change baud rate here  [cf datasheet]
			// SPBRG =  624; for 9600 [24MHZ only]
			// SPBRG =  832; for 9600 [32MHZ only]
			// SPBRG = 1040; for 9600 [40MHZ only]

		// 16-bit/Asynchronous
			TXSTA1bits.BRGH  	= 1	;
			BAUDCON1bits.BRG16	= 0 ;  ////<=== 1
		
		// Modifi� pour 24Mhz 
////			spbrg = 624 ;			// for 9600 [24MHZ only]
			SPBRG = 155;				//<=== SPGRB
			//spbrg = 832 ;			// for 9600 [32MHZ only]
			//spbrg = 1040 ;			// for 9600 [40MHZ only]
////			SPBRG1 = spbrg;			// Write baudrate to SPBRG1
////			SPBRGH1 = spbrg >> 8;	// For 16-bit baud rate generation
		
			TXSTA1bits.SYNC = 0;		// ASYNCHRONE
			RCSTA1bits.SPEN = 1;		// PERIPH ENABLE
			TXSTA1bits.TXEN = 1;		// TRANSMIT ENABLE   <====0!!!
			RCSTA1bits.CREN = 1;		// RECEIVER ENABLE
			TRISCbits.TRISC7 = 1;		// RX1 INPUT 
			TRISCbits.TRISC6 = 0;		// TX1 OUPUT
		
			PIR1bits.TX1IF = 0 ;	// interrup si buffer TXREG vide
			PIR1bits.RC1IF = 0 ;	// interrup si buffer RCREG vide
		
			PIE1bits.RC1IE  = 1 ;	// enable RC1IF
			PIE1bits.TX1IE  = 0 ; 	// Disable TX1IF 

//pour test rs232	
	RCONbits.IPEN = 1;		// Enable interrupt priority 
	IPR1bits.RCIP = 1; 		// Make receive interrupt high priority 
	INTCONbits.GIEH = 1; 		// Enable all high priority interrupts 
//FIN pour test rs232	


		
}  // fin Init_RS232( )



#pragma		code
/* End of File : main.c */
