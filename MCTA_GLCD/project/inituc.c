/*
    Initialisation du microcontroleur
*/

/************************ INCLUDE ***************************/
#include <p18f8723.h>          /* C18 compiler processor defs */
#include <inituc.h> 		   /* initialisation defs */

/**************** DECLARATION DES SOUS PROGRAMMES *************/

/***************************************************************
Nom   : void init_uc (void)
Role  : Initialisation du microcontroleur
****************************************************************/
void init_uc (void)
{

// OKV2 = v�rifi� pour le circuit imprim� V2

//  1 = entr�e   0 = sortie
// I/O Configuration
// TRISA = 0x3F;		// INPUTs: A0,A1,A2,A3,A4,A5 - OUPUTS: A6,A7	
 TRISA = 0b01111111;		// INPUTs: A0,A1,A2,A3,A4,A5,A6 - OSC: A7	OKv2   modif du 09/06/2016 erreur manquait A6 en input
 TRISB = 0x1F;		// INPUTs: BO,B1,B2,B3,B4 - ICSP: B5,B6,B7     OKv2
 TRISC = 0x98;		// OUPUTS: C2,C6,  Input C3,C4, C7  OSC	C0,C1 libre C5   OKv2
 TRISD = 0x00;		// OUPUTS: D0,D1,D2,D3,D4,D5,D6,D7   			 OKv2
 TRISE = 0x00;		// OUPUTS: E0,E1,E2,E3,E4,E5,E6,E7				 OKv2
 TRISF = 0xFF;		// INPUTs: F0,F1,F2,F3,F4,F5,F6,F7 	ANA:F0,F1		     OKv2
 TRISG = 0b00011001;// INPUTs: G0,G3,G4- OUPUTS: G1,G2(en remplacement de rh7 hs) ICSP G5		 OKv2
 TRISH = 0x00;		// OUPUTS: H0,H1,H2,H3,H4,H5,H6,H7				 OKv2
 TRISJ = 0x00;		// OUPUTS: H0,H1,H2,H3,H4,H5,H6,H7				 OKv2
 PORTJ = 0x00;

// A/D PORT Configuration
 ADCON1 = 0x08;		// ANALOG: AN0 -> AN6 - (DIGITAL: AN7 -> AN15, Vref+ = AVDD(5v) et Vref =AVSS(0v) ) OKv2

 PORTAbits.RA4 = 1;  // double emploi!!?
 PORTGbits.RG0 = 1;  // double emploi!!?
 PORTGbits.RG1 = 0;  // double emploi!!?

// ENTREES ANALOGIQUES   OKV2
	TRISAbits.TRISA0 = 1;	// AN0   T_Batt  	mesure tension batterie
	TRISAbits.TRISA1 = 1;	// AN1	 T_+48V	 	mesure tension convertisseur 48V
	TRISAbits.TRISA2 = 1;	// AN2	 Imodul  	mesure tension pour calcul courant de modulation
	TRISAbits.TRISA3 = 1;	// AN3	 Iconso  	mesure tension pour calcul courant de consommation
	TRISAbits.TRISA5 = 1;	// AN4	 V_ligne_AM mesure tension ligne capteur (pour anomalie sur capteur)
	TRISFbits.TRISF0 = 1;	// AN5   V_ligne_AV mesure tension ligne r�ponse capteur  Gain de 8.50 (150k/20k)+1
	TRISFbits.TRISF1 = 1;	// AN6	 V_ligne_AV2 mesure tension ligne r�ponse capteur  Gain de 4.75 (75k/20k)+1
//	TRISFbits.TRISF2 = 1;	// AN7
//	TRISFbits.TRISF3 = 1;	// AN8
//	TRISFbits.TRISF4 = 1;	// AN9





	ADCON0 = 0 ;// CAN AU REPOS

	ADCON1bits.VCFG1 = 0 ;	// VDD
	ADCON1bits.VCFG0 = 0 ;	// VSS

	ADCON1bits.PCFG3 = 1 ;	// SELECTION DES ENTREES ANALOGIQUES
	ADCON1bits.PCFG2 = 0 ;	// AN0 a AN6 INCLUS     
	ADCON1bits.PCFG1 = 0 ;	
	ADCON1bits.PCFG0 = 0 ;	

	ADCON2bits.ADFM = 1 ;	// FORMAT DU RESULTAT JUSTIFI� A DROITE
	ADCON2bits.ACQT2 = 1 ;	// ACQUISITION = 12 TAD
	ADCON2bits.ACQT1 = 0 ;	
	ADCON2bits.ACQT0 = 1 ;	

	ADCON2bits.ADCS2 = 0 ;	// HORLOGE TAD 1�S POUR F = 32MHz
	ADCON2bits.ADCS1 = 1 ;	// % 32
	ADCON2bits.ADCS0 = 0 ;	


}


