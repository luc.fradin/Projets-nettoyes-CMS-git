
#include <TESTS.h>
#include <GLCD.h>						/* Gestion GLCD */
#include <inituc.h>
#include <delay.h>						/* Defs DELAY functions */
#include <ftoa.h>						/* Adapt Chaine de caract�re */
#include <ecrans.h>

#include <analog.h>

extern char DATAtoLCD[30];


extern unsigned int V_Batt;
extern float V_Batt_Calc;
extern float V_Batt_Aff;
extern float V_48_Calc;
extern float V_Ligne_AM_Calc;


#define V_48_min 50   //  valeur en dessous de laquelle il y a anomalie du 48V qui normalement doit �tre proche de 54V

void Test_Tensions()

{
				// Test des alimentations
				SetPos(20,20);		//x , y						 
				PutMessage("Test des alimentations");				  	 

				IMP_TRANS=1;    //*impulsion sur relais pour mode transtest
				DELAY_MS (50);	//*
				IMP_TRANS=0;	//*

/*				while(touche_BP5 == 1 )   //tant que  BP5 n'est pas press�e 
				{
 
*/				niveau_batt();  					 //Affiche la jauge batterie
				Mesure_V_Batt();      //lecture V batt
				ftoa (V_Batt_Aff,DATAtoLCD,2);				// Convertir un float en une chaine de caract�res
				SetPos(25,28);	//x , y							// Positionner le curseur	
 				PutFloat(DATAtoLCD);
				SetPos(55,28);		//x , y						 
				PutMessage("V (Vbatt)");				  	 

  				// test le convertisseur 
				HT_ON = 1;									 // convertisseur 60V ON
				DELAY_MS (100);
				Led_jaune = 1;          //1
		    	Mesure_V_48v ();		//lecture V 48v
				ftoa (V_48_Calc,DATAtoLCD,2);				// Convertir un float en une chaine de caract�res
				SetPos(25,36);	//x , y							// Positionner le curseur	
 				PutFloat(DATAtoLCD);
				SetPos(55,36);		//x , y						 
				PutMessage("V (48V)");				  	 

				DELAY_MS (100);
 				Mesure_V_Ligne_AM();
				ftoa(V_Ligne_AM_Calc,DATAtoLCD,2);				// Convertir un float en une chaine de caract�res
				SetPos(25,44);	//x , y							// Positionner le curseur	
 				PutFloat(DATAtoLCD);
				SetPos(55,44);		//x , y						 
				PutMessage("V ligne AM");				  	 


				DELAY_500MS (1);


}



void Test_48V()

{

  				// test le convertisseur 
				HT_ON = 1;									 // convertisseur 60V ON
				Led_jaune = 1;          //1
				Led_verte = 1;          //1
				Led_rouge = 1;          //1
				DELAY_MS (200);
		    	Mesure_V_48v ();		//lecture V 48v
				HT_ON = 0;									 // convertisseur 60V ON
				Led_jaune = 0;          //1
				Led_verte = 0;          //1

				if (V_48_Calc < V_48_min)
				{
				ftoa (V_48_Calc,DATAtoLCD,1);				// Convertir un float en une chaine de caract�res
				SetPos(50,40);	//x , y							// Positionner le curseur	
 				PutFloat(DATAtoLCD);
				SetPos(28,24);		//x , y						 
				PutMessage("ANOMALIE 48V");	
				SetPos(72,40);	//x , y							// Positionner le curseur	
				PutMessage("V");	
				Led_rouge = 1;          
			Buzzer_on();
				DELAY_MS (75);
			Buzzer_on();
				while(1);
			  	 
				}
				Led_rouge = 0;          //1

}




