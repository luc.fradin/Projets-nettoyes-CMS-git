
#include <inituc.h>
#include <ecrans.h>
#include <GLCD.h>
#include <delay.h>						/* Defs DELAY functions */
#include <ftoa.h>						/* Adapt Chaine de caract�re */
#include <rtc.h>						/* Adapt Chaine de caract�re */
#include <clock.h>
#include <codage.h>
#include <mesures.h>

//#include <int_eeprom.h>
#include <eeprom.h>


extern char DATAtoLCD[30];


extern void Save_tpa_i2c(void);
extern void gestion_lecture_memoire(void);

extern unsigned int Unites,Dizaines;        // Global var.
extern unsigned int Tval;

extern unsigned int 	Tab_No_Enr[1];					//2 enregistre le num�ro d'enregistrement sur 2 octets
extern unsigned char 	Tab_Type_Enr[1];  				//1 octets  pour m�moriser le type d'enregistrement (TPA, TPR, D�bitm�tre, Groupe, Icra)
extern unsigned char 	Tab_Horodatage[10];		//10 octets	 //10 caract�res	  
extern unsigned char 	Tab_Constit[17][10];	//170 octets //constitution 10 caract�res
extern unsigned char 	Tab_VoieRobTete[10];		//10 octets 	VOIE 2 + ROB 2 + TETE 6	 caract�res
extern unsigned char 	Tab_Central[14];			//14 octets	 //14 caract�res	  
extern unsigned int 	Tab_Pression[17];			//34 octets   en 0 valeur du d�bit  si ffff => pas de d�bitm�tre
extern unsigned int 	Tab_I_Repos[17];			//34 octets 17 caract�res	  1 d�bitmetre et 16 TP
extern unsigned char 	Tab_I_Modul[17];			//17 octets	17 caract�res	  1 d�bitmetre et 16 TP
extern unsigned int 	Tab_I_Conso[17];			//34 octets	17 caract�res	  1 d�bitmetre et 16 TP
extern unsigned char 	Tab_Etat[17];				//17 octets  
							  // total:  340+1+2 = 343 octets

extern unsigned char F_ecran_adres;	 	// flag pour affichage �cran TPA r�duit(0) ou complet(1)
extern unsigned char F_aff_sauve_tpA; 	// flag pour m�mo affichage: Sauvegarde ? (sauvegarde des mesures pour TP adressables)
extern unsigned char F_sauve_mes_tpA; 	// flag pour m�mo sauvegarde valid�e des mesures des tp adressables
extern unsigned char F_retour_sauve;	// flag pour indiquer fin de sauvegarde
extern unsigned char F_aff_sauve_tpR; 	// flag pour m�mo affichage: Sauvegarde ? (sauvegarde des mesures pour TP r�sistifs)
extern unsigned char F_sauve_mes_tpR; 	// flag pour m�mo sauvegarde valid�e des mesures des tp r�sistifs
extern unsigned char F_sauve_mes_Deb; 		// flag pour m�mo sauvegarde valid�e des mesures des d�bitm�tres
extern unsigned char F_aff_sauve_Deb; 	// flag pour m�mo affichage: Sauvegarde ? (sauvegarde des mesures des d�bitm�tres)

extern unsigned char	valid_sens;   // validation de l'affichage et du r�glage de la sensibilit� dans la lecture des TPR

extern unsigned char F_memo_back_light; 	// flag pour m�mo sauvegarde position back_light				
extern unsigned char F_mode_scrut; 		// flag pour signaler d�but et fin de scrutation
extern unsigned char F_sup3mes_tpa;	 	// flag pour indiquer si ecran complet ou r�duit et > � 3 mesures

extern unsigned char 	Tab_Code[17];				//17 octets 17 caract�res	  1 d�bitmetre et 16 TP 

		
unsigned char Num_Ligne; 	// num�ro de la ligne de mesure pour affichage
extern unsigned char Nb_TPA_ok; // indique le nb de capteurs qui ont r�pondus lors d'un interrogation
//#define c1 1  
//#define c2 2
//#define c3 3
//#define c4 4
//#define c5 1
//#define c6 2

/*unsigned char MdP1;
unsigned char MdP2;
unsigned char MdP3;
unsigned char MdP4;
unsigned char MdP5;
unsigned char MdP6;
*/

unsigned char Code_Param[4];  //CODE SECRET POUR CONFIG ADECEF


#define def_i_conso 	80 		// defaut si > 8.0 mA
#define def_i_modul  	40 		// defaut si > 4.0 mA
#define def_i_repos  	150 	// defaut si > 150 �A
#define def_pression_B  800 	// defaut si < 800 mbar
#define def_pression_H  1800 	// defaut si > 1800 mbar


#define def_tampon_H  4200 		// defaut si pression tampon > 4200 mbar
#define def_tampon_B  1500 		// defaut si pression tampon < 1500 mbar
#define def_cable_H    550 		// defaut si pression cable > 550 mbar
#define def_cable_B    450 		// defaut si pression cable < 450 mbar
#define def_secours_B1  3000 	// defaut si pression secours < 3000 mbar
#define def_secours_B2  1500 	// defaut si pression secours < 3000 mbar
#define def_rosee_H     253 	// defaut si pint de ros�e > -20�C   253�K



//extern unsigned int V_Batt;
unsigned int V_Batt;
extern float V_Batt_Calc;
float V_Batt_Aff;



//unsigned int Hval;
//unsigned int Dizaines,Unites;
unsigned char eff=0;
extern unsigned char Pres_Debit;  // indique pr�sence d�bitm�tre pour affichage
extern unsigned char mes_debit; // pour indiquer (dans la boucle de mesure des pcpg) une mesure du d�bitm�tre 

extern unsigned int Freq_R;
extern unsigned char TPA_detecte;			// Si un capteur trouv� dans mesure TPRextern unsigned int Pression_TPR;	// Valeur de pression pour TPR
extern unsigned int Etat_TPR;	//	Etat TPR 1 = OK, 2 = absent, 3 = D�faut, 4 = TPA pr�sent
extern unsigned int Etat_Deb;	// Etat pour d�bitm�tre   2 = absent,  1 = OK

unsigned Etat_tpa;				// indique l'�tat des tpa apres mesure (pr�sents ok, absent, d�fauts...)
extern unsigned char fin_mesures; // si 1 indique que l'interrogation est termin�e (c'est pour valider l'analyse des courants pour indiquer les d�fauts et le affichages inverses



unsigned char config_adecef;   // flag pour autoriser la config adecef

//coche_constit � l'adresse 0x00 de l'eeprom du PIC18
unsigned char coche_constit;  	 // Bloque ou non la saisie des renseignements pour la sauvergarde de la mesure TPA et TPR 

//val1TP � l'adresse 0x01 de l'eeprom du PIC18
unsigned char val1TP;   		// valid l'arr�t de scrutation si 1 TP

//boucleTPR � l'adresse 0x02 de l'eeprom du PIC18
unsigned char boucleTPR;  		 // valid lecture en boucle des TPR

//boucleDEB � l'adresse 0x03 de l'eeprom du PIC18
unsigned char boucleDEB;  		// valid lecture en boucle du d�bit

//ConsAffTPA � l'adresse 0x04 de l'eeprom du PIC18
unsigned char ConsAffTPA;   	// valid conserve affichage TPA lorsqu'on quitte le mode mesure tpa

//ValBuzzer � l'adresse 0x05 de l'eeprom du PIC18
unsigned char ValBuzzer;  		// valid buzzer (dans configuration)

//Verif_codageAT � l'adresse 0x06 de l'eeprom du PIC18
unsigned char Verif_codageAT;   // Bloque ou non la possibilit� de v�rif du codage d'un TPA

//Aff_Debit_tpa � l'adresse 0x07 de l'eeprom du PIC18
unsigned char Aff_Debit_tpa;   // Bloque ou non l'affichage du d�bit dans tableau des TPA



unsigned char TA_TR;		// index table constitution
unsigned char tp2_constit;			// indique si on est au tp2 ou plus, pour afficher les champs communs d�ja remplis

extern unsigned int Offset_Enr; // adresse de l'enregistrement en lecture
extern unsigned int Der_Enreg;  // adresse du dernier enregistrement en EEprom
extern unsigned int Aff_Enreg;  // adresse de l'enregistrement affich�
extern unsigned char mes_groupe;

extern unsigned int ad_No_der_enreg;  // adresse o� est stock�e, dans le pic, le num�ro du dernier enregistrement 
extern unsigned int No_der_enreg ; // valeur du num�ro du dernier enregistrement   entre 0 et 80

extern unsigned int ad_Der_Enreg;  // adresse de l'eeprom du pic, de la valeur de l'adresse en i2c de l'enregistrment corespondant
extern unsigned int Der_Enreg;  // valeur de l'adresse m�moire i2c du dernier enregistrement en EEprom

								
extern unsigned char Detection_TPA;  	// flag pour indiquer si d�tection TPA valid�e ou non, dans l'�cran de mesure TPR

unsigned char ad_der_central; // adresse = 0x10 dans l'eeprom du pic		
extern unsigned char memo_touche_5a;  /*m�morise la derni�re touche utilis�e  �cran lecture m�moire*/

// Coefficients de correction
extern unsigned int Coeff_I_Modul;   	// pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53
extern unsigned int Coeff_I_Conso;   	// pour ajuster I_Conso (coeffx100) exemple 153 => coeff 1.53
extern unsigned int Coeff_I_Repos;   	// pour ajuster I_Repos (coeffx100) exemple 153 => coeff 1.53
extern unsigned int Coeff_V_TPR; 		// pour ajuster V_1K pour TPR (coeffx100) exemple 153 => coeff 1.53

unsigned char Val_Voie; // valeur n� de voie TPA pour la constitution
extern unsigned char F_sauve_mes_Deb; 		// flag pour m�mo sauvegarde valid�e des mesures des d�bitm�tres
unsigned int debit_cable;		// D�bit mesur� en cours
extern unsigned char erreur_codage;   //  erreurs sur bcta

extern unsigned char Code_Lect;
extern unsigned char Code_Prog;
extern unsigned char Mode_Scrolling;
// MENUS
//#pragma		code	USER_ROM   // retour � la zone de code

extern unsigned char Detection_TPA_V1_86; //flag pour V1.86
extern unsigned char Detection_TPA_V1_87; //flag pour V1.87

void menu_principal()
{

				ClearScreen();								// Effacement de tout l'�cran
				niveau_batt();
				DELAY_MS(200);
				SetPos(10,0);								// Positionner le curseur
				PutMessage(" Menu Principal ");					// Ecrire un message  nu_retour
				hline(0,127,10) ;							// R�aliser une ligne horizontale
				SetPos(10,16);								// Positionner le curseur
				PutMessage   (" TP Adressables ");					// Ecrire un message  nu_retour
 				SetPos(10,24);								// Positionner le curseur
				PutMessage   (" TP Resistifs   ");					// Ecrire un message
				plot(39,24);  // pour afficher le "�"
				plot(38,25);  // pour afficher le "�"
 				SetPos(10,32);								// Positionner le curseur
				PutMessage   (" Debitmetres    ");				  		// Ecrire un message  
				plot(23,32);	// pour afficher le "�"
				plot(22,33);		// pour afficher le "�"
  				plot(46,32);	// pour afficher le "�"
				plot(47,33);		// pour afficher le "�"
				SetPos(10,40);								// Positionner le curseur
				PutMessage   (" Groupe         ");				  		// Ecrire un message  
				SetPos(10,48);								// Positionner le curseur
				PutMessage   (" Memoire        ");				  		// Ecrire un message  
 				plot(23,48);	// pour afficher le "�"
				plot(22,49);		// pour afficher le "�"
				SetPos(10,56);								// Positionner le curseur
				PutMessage   (" Parametrages   ");				  		// Ecrire un message  
 				plot(46,56);	// pour afficher le "�"
				plot(45,57);		// pour afficher le "�"

}
void menu_principal_C0()
{
			
				SetPos(10,16);								// Positionner le curseur
				PutMessageInv(" TP Adressables ");					// Ecrire un message  nu_retour
 				SetPos(10,24);								// Positionner le curseur
				PutMessage   (" TP Resistifs   ");					// Ecrire un message
				plot(39,24);	// pour afficher le "�"
				plot(38,25);	// pour afficher le "�"
				SetPos(10,32);								// Positionner le curseur
				PutMessage   (" Debitmetres    ");				  		// Ecrire un message  
 				plot(23,32);	// pour afficher le "�"
				plot(22,33);		// pour afficher le "�"
 				plot(46,32);	// pour afficher le "�"
				plot(47,33);		// pour afficher le "�"
				SetPos(10,40);								// Positionner le curseur
				PutMessage   (" Groupe         ");				  		// Ecrire un message  
				SetPos(10,48);								// Positionner le curseur
				PutMessage   (" Memoire        ");				  		// Ecrire un message  
 				plot(23,48);	// pour afficher le "�"
				plot(22,49);		// pour afficher le "�"
				SetPos(10,56);								// Positionner le curseur
				PutMessage   (" Parametrages   ");				  		// Ecrire un message  
 				plot(46,56);	// pour afficher le "�"
				plot(45,57);		// pour afficher le "�"
}	
				
void menu_principal_C1()
{
				F_retour_sauve=0; //V1.85 BUG 10/10/2016
				SetPos(10,16);								// Positionner le curseur
				PutMessage   (" TP Adressables ");					// Ecrire un message  nu_retour
 				SetPos(10,24);								// Positionner le curseur
				PutMessageInv(" TP Resistifs   ");					// Ecrire un message
				unplot(39,24);	// pour afficher le "�"
				unplot(38,25);	// pour afficher le "�"
 				SetPos(10,32);								// Positionner le curseur
				PutMessage   (" Debitmetres    ");				  		// Ecrire un message  
 				plot(23,32);	// pour afficher le "�"
				plot(22,33);		// pour afficher le "�"
 				plot(46,32);	// pour afficher le "�"
				plot(47,33);		// pour afficher le "�"
 				SetPos(10,40);								// Positionner le curseur
				PutMessage   (" Groupe         ");				  		// Ecrire un message  
				SetPos(10,48);								// Positionner le curseur
				PutMessage   (" Memoire        ");				  		// Ecrire un message  
 				plot(23,48);	// pour afficher le "�"
				plot(22,49);		// pour afficher le "�"
				SetPos(10,56);								// Positionner le curseur
				PutMessage   (" Parametrages   ");				  		// Ecrire un message  
 				plot(46,56);	// pour afficher le "�"
				plot(45,57);		// pour afficher le "�"
}	
				
void menu_principal_C2()
{
				F_retour_sauve=0; //V1.85 BUG 10/10/2016
				SetPos(10,16);								// Positionner le curseur
				PutMessage   (" TP Adressables ");					// Ecrire un message  nu_retour
 				SetPos(10,24);								// Positionner le curseur
				PutMessage   (" TP Resistifs   ");					// Ecrire un message
				plot(39,24);	// pour afficher le "�"
				plot(38,25);	// pour afficher le "�"
 				SetPos(10,32);								// Positionner le curseur
				PutMessageInv(" Debitmetres    ");				  		// Ecrire un message  
  				unplot(23,32);	// pour afficher le "�"
				unplot(22,33);		// pour afficher le "�"
 				unplot(46,32);	// pour afficher le "�"
				unplot(47,33);		// pour afficher le "�"
				SetPos(10,40);								// Positionner le curseur
				PutMessage   (" Groupe         ");				  		// Ecrire un message  
				SetPos(10,48);								// Positionner le curseur
				PutMessage   (" Memoire        ");				  		// Ecrire un message  
 				plot(23,48);	// pour afficher le "�"
				plot(22,49);		// pour afficher le "�"
				SetPos(10,56);								// Positionner le curseur
				PutMessage   (" Parametrages   ");				  		// Ecrire un message  
 				plot(46,56);	// pour afficher le "�"
				plot(45,57);		// pour afficher le "�"
}
					
void menu_principal_C3()
{
				SetPos(10,16);								// Positionner le curseur
				PutMessage   (" TP Adressables ");					// Ecrire un message  nu_retour
 				SetPos(10,24);								// Positionner le curseur
				PutMessage   (" TP Resistifs   ");					// Ecrire un message
				plot(39,24);	// pour afficher le "�"
				plot(38,25);	// pour afficher le "�"
 				SetPos(10,32);								// Positionner le curseur
				PutMessage   (" Debitmetres    ");				  		// Ecrire un message  
 				plot(23,32);	// pour afficher le "�"
				plot(22,33);		// pour afficher le "�"
 				plot(46,32);	// pour afficher le "�"
				plot(47,33);		// pour afficher le "�"
 				SetPos(10,40);								// Positionner le curseur
				PutMessageInv(" Groupe         ");				  		// Ecrire un message  
				SetPos(10,48);								// Positionner le curseur
				PutMessage   (" Memoire        ");				  		// Ecrire un message  
 				plot(23,48);	// pour afficher le "�"
				plot(22,49);		// pour afficher le "�"
				SetPos(10,56);								// Positionner le curseur
				PutMessage   (" Parametrages   ");				  		// Ecrire un message  
 				plot(46,56);	// pour afficher le "�"
				plot(45,57);		// pour afficher le "�"
}	
void menu_principal_C4()
{
				SetPos(10,16);								// Positionner le curseur
				PutMessage   (" TP Adressables ");					// Ecrire un message  nu_retour
 				SetPos(10,24);								// Positionner le curseur
				PutMessage   (" TP Resistifs   ");					// Ecrire un message
				plot(39,24);	// pour afficher le "�"
				plot(38,25);	// pour afficher le "�"
 				SetPos(10,32);								// Positionner le curseur
				PutMessage   (" Debitmetres    ");				  		// Ecrire un message  
 				plot(23,32);	// pour afficher le "�"
				plot(22,33);		// pour afficher le "�"
  				plot(46,32);	// pour afficher le "�"
				plot(47,33);		// pour afficher le "�"
				SetPos(10,40);								// Positionner le curseur
				PutMessage   (" Groupe         ");				  		// Ecrire un message  
				SetPos(10,48);								// Positionner le curseur
				PutMessageInv(" Memoire        ");				  		// Ecrire un message  
 				unplot(23,48);	// pour afficher le "�"
				unplot(22,49);		// pour afficher le "�"
				SetPos(10,56);								// Positionner le curseur
				PutMessage   (" Parametrages   ");				  		// Ecrire un message  
 				plot(46,56);	// pour afficher le "�"
				plot(45,57);		// pour afficher le "�"
}					
void menu_principal_C5()
{

				SetPos(10,16);								// Positionner le curseur
				PutMessage   (" TP Adressables ");					// Ecrire un message  nu_retour
 				SetPos(10,24);								// Positionner le curseur
				PutMessage   (" TP Resistifs   ");					// Ecrire un message
				plot(39,24);	// pour afficher le "�"
				plot(38,25);	// pour afficher le "�"
 				SetPos(10,32);								// Positionner le curseur
				PutMessage   (" Debitmetres    ");				  		// Ecrire un message  
 				plot(23,32);	// pour afficher le "�"
				plot(22,33);		// pour afficher le "�"
 				plot(46,32);	// pour afficher le "�"
				plot(47,33);		// pour afficher le "�"
 				SetPos(10,40);								// Positionner le curseur
				PutMessage   (" Groupe         ");				  		// Ecrire un message  
				SetPos(10,48);								// Positionner le curseur
				PutMessage   (" Memoire        ");				  		// Ecrire un message  
 				plot(23,48);	// pour afficher le "�"
				plot(22,49);		// pour afficher le "�"
				SetPos(10,56);								// Positionner le curseur
				PutMessageInv(" Parametrages   ");				  		// Ecrire un message  
 				unplot(46,56);	// pour afficher le "�"
				unplot(45,57);		// pour afficher le "�"
}					

//******************************************************************************************************

void menu_TPAD()
{
				ClearScreen ();
				niveau_batt();
				SetPos(10,10);								// Positionner le curseur
				PutMessage(" TP Adressables ");					// Ecrire un message  nu_retour
				hline(0,127,20) ;							// R�aliser une ligne horizontale
				SetPos(10,30);								// Positionner le curseur
				PutMessage(" Mesures    ");					// Ecran Mesures des TPA
				SetPos(10,38);								// Positionner le curseur
				PutMessage(" Codage     ");				  		// Ecrire un message  

}
void menu_TPAD_C0()
{
				SetPos(10,30);								// Positionner le curseur
				PutMessageInv(" Mesures    ");					// Ecran Mesures des TPA
				SetPos(10,38);								// Positionner le curseur
				PutMessage(" Codage     ");				  		// Ecrire un message  
}	
				
void menu_TPAD_C1()
{
				SetPos(10,30);								// Positionner le curseur
				PutMessage(" Mesures    ");					// Ecran Mesures des TPA
				SetPos(10,38);								// Positionner le curseur
				PutMessageInv(" Codage     ");				  		// Ecrire un message  
}	
				
/*

void menu_TPAD_C2()
{
				SetPos(10,30);								// Positionner le curseur
				PutMessage(" Mesures    ");					// Ecran Mesures des TPA
				SetPos(10,38);								// Positionner le curseur
				PutMessage(" Codage     ");				  		// Ecrire un message  
}
*/					


// FIN  MENUS




// ECRANS DE MESURES TPA pour afficher en mode scrolling

void Scrolling_Ecran_tpa()
{
unsigned char tp;
unsigned char Ligne;		// position x Ligne d'�criture dans le tableau
unsigned char ltp;

				tp = Num_Ligne;
				ClearScreen ();


		if (Num_Ligne >= 3 || Num_Ligne == 0)
			{
	     	    if (F_ecran_adres)						// affichage �cran complet (bit = 1)
					{
				ecran_adressables_complet();//				
				if (Num_Ligne != 0){
				Affiche_complet(tp-2,1,Ligne);
				Affiche_complet(tp-1,2,Ligne);
				Affiche_complet(tp,3,Ligne);
									}											
					}
					else
					{
				ecran_adressables_reduit();//				
				if (Num_Ligne != 0){
				Affiche_reduit(tp-2,1,Ligne);
				Affiche_reduit(tp-1,2,Ligne);
				Affiche_reduit(tp,3,Ligne);						
									}											
					}
			}
		else if(Num_Ligne == 2)
			{

	     	    if (F_ecran_adres)						// affichage �cran complet (bit = 1)
					{
				ecran_adressables_complet();//				
				if (Num_Ligne != 0){
//				Affiche_complet(tp-2,1,Ligne);
				Affiche_complet(tp-1,1,Ligne);
				Affiche_complet(tp,2,Ligne);
									}											
					}
					else
					{
				ecran_adressables_reduit();//				
				if (Num_Ligne != 0){
//				Affiche_reduit(tp-2,1,Ligne);
				Affiche_reduit(tp-1,1,Ligne);
				Affiche_reduit(tp,2,Ligne);						
									}											
					}
			}
		else if(Num_Ligne == 1)
			{

	     	    if (F_ecran_adres)						// affichage �cran complet (bit = 1)
					{
				ecran_adressables_complet();//				
				if (Num_Ligne != 0){
				Affiche_complet(tp,1,Ligne);
									}											
					}
					else
					{
				ecran_adressables_reduit();//				
				if (Num_Ligne != 0){
				Affiche_reduit(tp,1,Ligne);						
									}											
					}
			}


}




void ecran_adressables_complet()
{
				niveau_batt();
//				if (Pres_Debit==2 || Aff_Debit_tpa) //si == 1 => d�bitm�tre pr�sent => pas d'affichage TP Adressable
//					{
				SetPos(0,0);
  				PutMessage("TP Adressables   ");		// Ecrire un message  nu_retour
//					}
				SetPos(1,16);
  				PutMessage("TP");		// Ecrire un message  nu_retour
				SetPos(15,16);
  				PutMessage("Press");	// Ecrire un message  nu_retour
				SetPos(43,16);
  				PutMessage("Mod");	// Ecrire un message  nu_retour
				SetPos(62,16);
  				PutMessage("Cons");	// Ecrire un message  nu_retour
				SetPos(86,16);
  				PutMessage("Repo");	// Ecrire un message  nu_retour
				SetPos(110,16);
  				PutMessage("Etat");	// Ecrire un message  nu_retour
				SetPos(16,24);
  				PutMessage("mbar");	// Ecrire un message  nu_retour
				SetPos(44,24);
  				PutMessage("mA");	// Ecrire un message  nu_retour
				SetPos(66,24);
  				PutMessage("mA");	// Ecrire un message  nu_retour
				SetPos(91,24);
  				PutMessage("mA");	// Ecrire un message  nu_retour


				hline(0,128,14) ;							// ligne horizontale
				hline(0,128,30) ;							// "
				vline (13,15,56);							// ligne verticale ICIICI
				vline (41,15,56);							// "
				vline (60,15,56);							// ligne verticale
				vline (84,15,56);							// "
				vline (108,15,56);							// "


}  // fin �cran adressables	complet



void ecran_adressables_reduit()
{

				niveau_batt();

//				if (Pres_Debit==2 || Aff_Debit_tpa) //si == 1 => d�bitm�tre pr�sent => pas d'affichage TP Adressable
//					{
				SetPos(0,0);
  				PutMessage("TP Adressables   ");		// Ecrire un message  nu_retour
//					}
				SetPos(7,16);
  				PutMessage("TP");		// Ecrire un message  nu_retour
				SetPos(27,16);
  				PutMessage("mbar");	
				SetPos(55,16);
  				PutMessage("Etat");	// Ecrire un message  nu_retour
				hline(2,76,14) ;							// ligne horizontale
				hline(2,75,28) ;							// "
				vline (2,15,55);							// ligne verticale
				vline (22,15,55);							// ligne verticale
				vline (52,15,55);							// "
				vline (75,15,55);							// "

				SetPos(82,16);
  				PutMessage("Presents");	// Ecrire un message  nu_retour
				plot(98,16);	// pour afficher le "�"
				plot(97,17);		// pour afficher le "�"

				hline(77,127,14) ;							// "
				vline (77,15,54);							// ligne verticale
				vline (126,15,54);							// ligne verticale
				hline(77,81,54) ;							// "
				hline(123,127,54) ;							// "

//				box(77,14,127,55);						//x1,y1,x2,y2


}  // fin �cran adressables	r�duit



void ecran_aff_TPA_reduit()  //�cran affichage TPadressables mode r�duit
{
unsigned char tp;
unsigned char Ligne;		// position x Ligne d'�criture dans le tableau
unsigned char ltp;

				tp = Num_Ligne;

// AFFICHAGE MESURE �cran reduit

				if (Num_Ligne > 3)
				{
				F_sup3mes_tpa = 1;   //flag pour m�moriser si sup � 3 mesures
				ClearScreen ();
				SetPos(0,56);								// Positionne le curseur
				PutMessage("Mesures en cours");				// 17 caract�res	
				ecran_adressables_reduit();//				
				Affiche_reduit(tp-2,1,Ligne);
				Affiche_reduit(tp-1,2,Ligne);
				Affiche_reduit(tp,3,Ligne);						
				}
				else 
				{
				ltp = Num_Ligne;
				Affiche_reduit(tp,ltp,Ligne);
				F_sup3mes_tpa = 0;   //flag pour m�moriser si sup � 3 mesures
				}
//PREMIERE MESURE �cran r�duit
//	  				displayNum (20,Ligne,Tab_Code[tp]);
//					displayNum (42,Ligne,Tab_Pression[tp]);					// Ecrire un INT en "normal" (-> Chiffres + Lettres )
//					SetPos(78,Ligne);
///*******					switch_Etat(Etat);
//FIN PREMIERE MESURE �cran r�duit
}


void Affiche_reduit(char tp,char ltp,char Ligne)
{
unsigned char n_tp;  // pour afficher tp pr�sents ou absents
				n_tp=0;


				switch (ltp)
					{
				case 0:
	  				// N� Ligne utilis�e pour d�bitm�tres
				break;
				case 1:
	  			Ligne = 35 ;	// position x Ligne d'�criture dans le tableau
				break;
				case 2:
	  			Ligne = 44 ;	// position x Ligne d'�criture dans le tableau
				break;
				case 3:
	  			Ligne = 51 ;	// position x Ligne d'�criture dans le tableau
				break;
					}

				Etat_tpa = 0x00;		// pour affichage commentaire de la case Etat


				if (Pres_Debit !=2)
				{
				Calcul_Deb (); // test si d�bitm�tre pr�sent et mise � l'�chelle du d�bit
				}	


//  if (Tab_Code[tp]==0  &&	Pres_Debit == 1)  //  	 modif du 5/4/2015								 
  if (Pres_Debit == 1 && Aff_Debit_tpa==0)   //  Aff_Debit_tpa==0  => l'affichage n'est pas bloqu�								 
	{ 
	// ici cadre d�bitm�tre et affichage valeur 
				SetPos(0,0);
  				PutMessage("DEBIT:            ");		
		//		debit_cable=42;   //pour test
				displayNum (50,0,debit_cable);				// DEBIT  affichage normal
  				PutMessage(" L/H");
	}
//	else if (Tab_Code[tp]==0  &&  Pres_Debit == 0)  //  	 modif du 5/4/2015	
	else if ( Pres_Debit == 0 && Aff_Debit_tpa==0)   //  Aff_Debit_tpa==0  => l'affichage n'est pas bloqu�	 
	{
				SetPos(0,0);
  				PutMessage("Debitmetre Absent");
 				plot(9,0);	// pour afficher le "�"
				plot(8,1);		// pour afficher le "�"
 				plot(32,0);	// pour afficher le "�"
				plot(33,1);		// pour afficher le "�"
 	}
 





///		if (Tab_Code[tp]>0 &&Tab_Pression[tp]!=0)   // pour ne pas afficher le capteur 0(d�bitm�tre) en premiere ligne du tableau et n'afficher que les capteurs pr�sents
		if (Tab_Code[tp]>0 )   // pour ne pas afficher le capteur 0(d�bitm�tre) en premiere ligne du tableau des �tats
			{



				if (Tab_Code[tp]>=10)   // pour bien positionner les chiffres
				{
  				displayNum (8,Ligne,Tab_Code[tp]);					// Code
				}
				else
				{
  				displayNum (13,Ligne,Tab_Code[tp]);					// Code
				}

				//* PRESSION
				if (Tab_Pression[tp]>=1000)		//	>= 1000
				{
					if (Tab_Pression[tp]>def_pression_H)			// Si Pression sup�rieure � 1800mbar => affichage inverse
							{
						displayNumInv (27,Ligne,Tab_Pression[tp]);
						Etat_tpa=0x10;		//- pour affichage case etat HG
							}	
						else 	
							{
						displayNum (27,Ligne,Tab_Pression[tp]);				// Pression  affichage normal
						Etat_tpa=0x01;		//- pour affichage case etat OK
							}	
				}
				else if (Tab_Pression[tp]<1000 && Tab_Pression[tp]!=0)	// Si Pression	< 1000 et != de 0
				{
					if (Tab_Pression[tp]<def_pression_B)				// Si Pression inf�rieure � 800mbar => affichage inverse
							{
						displayNumInv (30,Ligne,Tab_Pression[tp]);
						Etat_tpa=0x10;		//- pour affichage case etat HG
							}	
						else 
							{
						displayNum (30,Ligne,Tab_Pression[tp]);				// Pression  affichage normal
						Etat_tpa=0x01;		//- pour affichage case etat OK
							}	
				}
				else // Si Pression	=  0
				{
				SetPos(27,Ligne);		// avant modif valeur = 35    				modif du 10/02/2016
	  			PutMessage("0000");	// capteur r�ponse absent   avant modif "-"   	modif du 10/02/2016
				Etat_tpa=0x02;		//- pour affichage case etat ABSENT
				}

// DEBUT TEST COURANTS

				//*	TEST COURANT DE REPOS si on arrive au 16�me capteur uniquement car on ne peut pas savoir combien il y a de TP en ligne
		if ( fin_mesures == 1)
			{
				if (Tab_I_Repos[tp] > (def_i_repos+def_i_repos*Nb_TPA_ok))
				{				
				Etat_tpa=0x08;		//- pour affichage case Etat d�faut courant
				}
					
			
				//* COURANT DE CONSOMMATION
				if (Tab_I_Conso[tp] > (def_i_conso + (Tab_I_Repos[tp]/100))) // par exemple i_conso= 80 pour 8.0mA et i_repos = 121 �A donc en mA*10 on aura 0.121mA => 80+1,21 => 81 si un seul capteur
				{
				Etat_tpa=0x08;		//- pour affichage case Etat d�faut courant
				}		
			}

// FIN TEST COURANTS


				//* ETAT
///		if ( fin_mesures == 1)
///			{//-
				SetPos(56,Ligne);
				switch_Etat(Etat_tpa,Ligne);
				Tab_Etat[tp]= Etat_tpa;
										
///			}//-
				
				for (n_tp=1;n_tp<=16;n_tp++){
				if (Tab_Etat[n_tp]==1){
				switch_Present(n_tp);       	// affichage pr�sent pour le tableau  
				}
				else if (Tab_Etat[n_tp]==2){
				switch_Absent(n_tp);  			// affichage absent pour le tableau  
				}
				else if (Tab_Etat[n_tp]>2){
				switch_Present(n_tp);  			// affichage pr�sent avec d�fauts pour le tableau 
				}
				else if (Tab_Etat[n_tp]==0){
				}
				//	
			}
		
		Mode_Scrolling =0; 
		}
}
 // fin �cran affichage TPadressables mode r�duit








//#########################################�cran affichage TP adressables mode complet###########################################
void ecran_aff_TPA_complet() 
{
unsigned char tp;
unsigned char ltp;
char DATAtoLCD[30];
unsigned char Ligne;
float temp_float;

				tp = Num_Ligne;

// AFFICHAGE MESURE �cran complet

				if (Num_Ligne > 3)
				{
				F_sup3mes_tpa = 1;   //flag pour m�moriser si sup � 3 mesures
				ClearScreen ();
				SetPos(0,56);								// Positionne le curseur
				PutMessage("Mesures en cours ");		// 17 caracteres
				ecran_adressables_complet();//				
				Affiche_complet(tp-2,1,Ligne);
				Affiche_complet(tp-1,2,Ligne);
				Affiche_complet(tp,3,Ligne);						
				}
				else 
				{
				ltp = Num_Ligne;
				Affiche_complet(tp,ltp,Ligne);
				F_sup3mes_tpa = 0;   //flag pour m�moriser si sup � 3 mesures
				}
//				displaySmallNum(tp*6,7,tp);
}





void Affiche_complet(char tp,char ltp,char Ligne)
{
float temp_float;
char DATAtoLCD[30];
//unsigned char Ligne;

				switch (ltp)
					{
				case 0:
	  				// N� Ligne utilis�e pour les d�bitm�tre
				break;
				case 1:
	  			Ligne = 35 ;	// position x Ligne d'�criture dans le tableau
				break;
				case 2:
	  			Ligne = 44 ;	// position x Ligne d'�criture dans le tableau
				break;
				case 3:
	  			Ligne = 51 ;	// position x Ligne d'�criture dans le tableau
				break;
					}
				Etat_tpa = 0x00;		// pour affichage case etat

				if (Pres_Debit !=2)
				{
				Calcul_Deb (); // test si d�bitm�tre pr�sent et mise � l'�chelle du d�bit
				}	


//  if (Tab_Code[tp]==0  &&	Pres_Debit == 1)  //  	 modif du 5/4/2015								 
  if (Pres_Debit == 1&& Aff_Debit_tpa==0)   //  Aff_Debit_tpa==0  => l'affichage n'est pas bloqu�   	 //  	 modif du 5/4/2015									 
	{ 
	// ici cadre d�bitm�tre et affichage valeur 
				SetPos(0,0);
  				PutMessage("DEBIT:            ");		
		//		debit_cable=42;   //pour test
				displayNum (50,0,debit_cable);				// DEBIT  affichage normal
  				PutMessage(" L/H");
	}
//	else if (Tab_Code[tp]==0  &&  Pres_Debit == 0)  //  	 modif du 5/4/2015	
	else if ( Pres_Debit == 0&& Aff_Debit_tpa==0)   //  Aff_Debit_tpa==0  => l'affichage n'est pas bloqu�
	{
				SetPos(0,0);
  				PutMessage("Debitmetre Absent");
 				plot(9,0);	// pour afficher le "�"
				plot(8,1);		// pour afficher le "�"
  				plot(32,0);	// pour afficher le "�"
				plot(33,1);		// pour afficher le "�"
	}


		if (Tab_Code[tp]>0)   // pour ne pas afficher le capteur 0(d�bitm�tre) en premiere ligne du tableau
			{

				if (Tab_Code[tp]>=10){
  				displayNum (1,Ligne,Tab_Code[tp]);					// Code
				}
				else{
  				displayNum (6,Ligne,Tab_Code[tp]);					// Code
				}
				//* PRESSION
				if (Tab_Pression[tp]>=1000)		//	>= 1000
				{
					if (Tab_Pression[tp]>def_pression_H)			// Si Pression sup�rieure � 1800mbar => affichage inverse
							{
						displayNumInv (16,Ligne,Tab_Pression[tp]);
						Etat_tpa=0x10;		//- pour affichage case etat HG
							}	
						else 	
							{
						displayNum (16,Ligne,Tab_Pression[tp]);				// Pression  affichage normal
						Etat_tpa=0x01;		//- pour affichage case etat OK
							}	
				}
				else if (Tab_Pression[tp]<1000 && Tab_Pression[tp]!=0)	// Si Pression	< 1000 et != de 0
				{
					if (Tab_Pression[tp]<def_pression_B)				// Si Pression inf�rieure � 800mbar => affichage inverse
							{
						displayNumInv (20,Ligne,Tab_Pression[tp]);
						Etat_tpa=0x10;		//- pour affichage case etat HG
							}	
						else 
							{
						displayNum (20,Ligne,Tab_Pression[tp]);				// Pression  affichage normal
						Etat_tpa=0x01;		//- pour affichage case etat ok
							}	
				}
				else // Si Pression	=  0
				{
				SetPos(16,Ligne);		// avant modif valeur = 27    				modif du 10/02/2016
	  			PutMessage("0000");	// capteur r�ponse absent   avant modif "-"   	modif du 10/02/2016
				Etat_tpa=0x02;		//- pour affichage case etat ABSENT
				}


				//*	COURANT DE REPOS
//===========> MODIF Pour afficher en mA

///				if (Tab_I_Repos[tp] < 100) {
///				displayNum (91,Ligne,Tab_I_Repos[tp]);   //45
///				}	
///				if (Tab_I_Repos[tp] >= 100) {
///				displayNum (88,Ligne,Tab_I_Repos[tp]);   //42

				temp_float=(float)Tab_I_Repos[tp];		// remet l'int en float pour l'affichage, pour diviser /10  et afficher avec virgule
				temp_float/=0x3E8;   //1000
				ftoa (temp_float,DATAtoLCD,2);				// Convertir un float en une chaine de caract�res

//				if (Tab_I_Repos[tp] >= 100) 
//					{
				SetPos(87,Ligne);								// Positionner le curseur
				PutFloat (DATAtoLCD);						// Afficher le float en normal

//					}		

					
				if (fin_mesures == 1){
					if (Tab_I_Repos[tp] > (def_i_repos+(def_i_repos*Nb_TPA_ok)))
						{				
///						displayNumInv (88,Ligne,Tab_I_Repos[tp]);
				SetPos(87,Ligne);								// Positionner le curseur
				PutFloatInv(DATAtoLCD);						// Afficher le float en INVERSE
						Etat_tpa=0x08;		//- pour affichage case Etat d�faut courant
						}					
					}

				//* COURANT DE CONSOMMATION
				temp_float=(float)Tab_I_Conso[tp];		// remet l'int en float pour l'affichage, pour diviser /10  et afficher avec virgule
				temp_float/=10;
				ftoa (temp_float,DATAtoLCD,1);				// Convertir un float en une chaine de caract�res
				if (Tab_I_Conso[tp]<100){
				SetPos(67,Ligne);								// Positionner le curseur
				PutFloat (DATAtoLCD);						// Afficher le float en normal
				}
				else {
				SetPos(63,Ligne);								// Positionner le curseur
				PutFloat (DATAtoLCD);						// Afficher le float en normal
					}	

				if (fin_mesures == 1){
				if ((Tab_I_Conso[tp] > (def_i_conso + Tab_I_Repos[tp]/100))) // par exemple i_conso= 80 pour 8.0mA et i_repos = 121 �A donc en mA*10 on aura 0.121mA => 80+1,21 => 81 si un seul capteur
					{
				if (Tab_I_Conso[tp]<100){
				SetPos(67,Ligne);	
				PutFloatInv (DATAtoLCD); // Affiche le float en inverse
				Etat_tpa=0x08;		//- pour affichage case Etat d�faut courant
						}		
				else {
				SetPos(63,Ligne);								// Positionner le curseur
				PutFloatInv (DATAtoLCD);						// Afficher le float en normal
				Etat_tpa=0x08;		//- pour affichage case Etat d�faut courant
					 }	
					}		
				}

				//* COURANT DE MODULATION
				temp_float=(float)Tab_I_Modul[tp];		// remet l'int en float pour l'affichage, pour diviser /10  et afficher avec virgule
				temp_float/=10;
				ftoa (temp_float,DATAtoLCD,1);				// Convertir un float en une chaine de caract�res		
				SetPos(45,Ligne);								// Positionner le curseur
				PutFloat (DATAtoLCD);						// Afficher le float (taille normale)

				//* ETAT
//				if (fin_mesures == 1){            // rep�re1
				SetPos(110,Ligne);
				switch_Etat(Etat_tpa,Ligne);
				Tab_Etat[tp]= Etat_tpa;
//				}	
			} //	if (Tab_Code[tp]!=0)

}
 // fin �cran affichage TPadressables mode complet




void eff_valeurs_complet()
{

				ClearScreen ();
				ecran_adressables_complet();//				
}



void switch_Etat(char Etat,char Ligne)
	{
		if (F_ecran_adres)		// complet
		{						
		
				switch (Etat)
					{
				case 0x00:
									//00= capteur non mesur�
				break;
				case 0x01:
	  			PutMessage("Ok");	//01= capteur r�ponse OK
				break;
				case 0x02:
	  			PutMessage("Abs");	//02= capteur r�ponse Absent
				break;
				case 0x04:
	  			PutMessage("N/R");	//04= capteur r�ponse Non R�ponse
				break;
				case 0x08:
	  			PutMessage("D/C");	//08= capteur r�ponse D�faut Courant
				break;
				case 0x10:
	  			PutMessage("HG");	//10= capteur r�ponse Hors Gamme
				break;
				case 0x20:
	  			PutMessage("CC");	//20= capteur r�ponse Court Circuit
				break;
				case 0x40:
	  			PutMessage("Def");	//40= capteur r�ponse Court Circuit  =>  pour indiquer un d�faut en mode �cran r�duit
				break;
				case 0x80:
	  			PutMessage("Ins");	//80= capteur r�ponse Instable =>  pour indiquer un d�faut en mode �cran r�duit
				break;
					}
		}
		else  // si �cran r�duit
		{
				switch (Etat)
					{
				case 0x00:
									//00= capteur non mesur�
				break;
				case 0x01:
	  			PutMessage(" Ok");	//01= capteur r�ponse OK
				break;
				case 0x02:
	  			PutMessage("Abs");	//02= capteur r�ponse Absent
				break;
				case 0x04:
	  			PutMessageInv("N/R");	//04= capteur r�ponse Non R�ponse
				break;
				case 0x08:
	  			PutMessageInv("D/C");	//08= capteur r�ponse D�faut Courant
				break;
				case 0x10:
	  			PutMessageInv("HG");	//10= capteur r�ponse Hors Gamme
				break;
				case 0x20:
	  			PutMessageInv("CC");	//20= capteur r�ponse Court Circuit
				break;
				case 0x40:
	  			PutMessageInv("Def");	//40= capteur r�ponse Court Circuit  =>  pour indiquer un d�faut en mode �cran r�duit
				break;
				case 0x80:
	  			PutMessageInv("Ins");	//80= capteur r�ponse Instable =>  pour indiquer un d�faut en mode �cran r�duit
				break;
					}

		}
	}



void switch_Present(char i)  // affichage pour tableau "Presents" du mode r�duit
	{
				switch (i)
					{
				case 0:
				break;
				case 1:
				displaySmallNum(83,24,1);
				break;
				case 2:
 				displaySmallNum(94,24,2);
				break;
				case 3:
				displaySmallNum(105,24,3);
				break;
				case 4:
				displaySmallNum(116,24,4);
				break;
				case 5:
				displaySmallNum(83,32,5);
				break;
				case 6:
 				displaySmallNum(94,32,6);
				break;
				case 7:
				displaySmallNum(105,32,7);
				break;
				case 8:
				displaySmallNum(116,32,8);
				break;
				case 9:
				displaySmallNum(82,40,9); 
				break;
				case 10:
				displaySmallNum(90,40,10);
				break;
				case 11:
				displaySmallNum(103,40,11);
				break;
				case 12:
				displaySmallNum(115,40,12);
				break;
				case 13:
				displaySmallNum(78,48,13); 
				break;
				case 14:
				displaySmallNum(90,48,14);
				break;
				case 15:
				displaySmallNum(104,48,15);
				break;
				case 16:
				displaySmallNum(116,48,16); 
				break;

					}

	}

void switch_Present_def(char i)  // affichage pour tableau Presents mais avec d�fauts du mode r�duit
	{
				switch (i)
					{
				case 0:
				break;
				case 1:
				displaySmallNumInv(83,24,1);
				break;
				case 2:
 				displaySmallNumInv(94,24,2);
				break;
				case 3:
				displaySmallNumInv(105,24,3);
				break;
				case 4:
				displaySmallNumInv(116,24,4);
				break;
				case 5:
				displaySmallNumInv(83,32,5);
				break;
				case 6:
 				displaySmallNumInv(94,32,6);
				break;
				case 7:
				displaySmallNumInv(105,32,7);
				break;
				case 8:
				displaySmallNumInv(116,32,8);
				break;
				case 9:
				displaySmallNumInv(82,40,9); 
				break;
				case 10:
				displaySmallNumInv(90,40,10);
				break;
				case 11:
				displaySmallNumInv(103,40,11);
				break;
				case 12:
				displaySmallNumInv(115,40,12);
				break;
				case 13:
				displaySmallNumInv(78,48,13); 
				break;
				case 14:
				displaySmallNumInv(90,48,14);
				break;
				case 15:
				displaySmallNumInv(104,48,15);
				break;
				case 16:
				displaySmallNumInv(116,48,16); 
				break;

					}

	}




void switch_Absent(char i)			// affichage pour tableau "Presents" du mode r�duit
	{
				switch (i)
					{
				case 0:
				break;
				case 1:
				SetPos(83,24);
				break;
				case 2:
				SetPos(94,24);
				break;
				case 3:
				SetPos(105,24);
				break;
				case 4:
				SetPos(116,24);
				break;
				case 5:
				SetPos(83,32);
				break;
				case 6:
 				SetPos(94,32);
				break;
				case 7:
				SetPos(105,32);
				break;
				case 8:
				SetPos(116,32);
				break;
				case 9:
				SetPos(83,40); 
				break;
				case 10:
				SetPos(94,40);
				break;
				case 11:
				SetPos(105,40);
				break;
				case 12:
				SetPos(116,40);
				break;
				case 13:
				SetPos(83,48); 
				break;
				case 14:
				SetPos(94,48);
				break;
				case 15:
				SetPos(105,48);
				break;
				case 16:
				SetPos(116,48); 
				break;
					}
 				PutMessage("-");

	}





void ecran_memo_TPadressables() 
{
//			unsigned char TA;           // num�ro du capteur � renseigner
			unsigned char unTP;         // pour ne pas sauvegarder si pas de TP   =>0   si 1 il y a au moins 1 TP
				tp2_constit=0;  // indique si on est au tp n�2 ou plus, pour r�-afficher les champs communs d�ja remplis


			unTP=0; // reset: il n'y a pas de TP 

	for (TA_TR=1;TA_TR<=16;TA_TR++)		// boucle pour renseigner chaque capteur trouv�
		{

		if  (Tab_Etat[TA_TR]==0)			
			{		// pour stopper la boucle si 0 (fin capteurs ou la constitution n'est pas valid�e) ou si capteurs r�sistifs
			TA_TR=17;  // il n'y a plus de capteurs,=> stop de la boucle					
			}		

//		if  (Tab_Etat[TA_TR]==1) 	// test le bit � 1 pour afficher la demande de sauvegarde   //!!//
		if  (Tab_Etat[TA_TR]!=2 && Tab_Etat[TA_TR]>0 && TA_TR<17) 	// test le bit � 1 pour afficher la demande de sauvegarde   //!!//
			{

				ClearScreen ();								// Effacement de tout l'�cran
				unTP=1; // il a au moins 1 TP
 				if (F_sauve_mes_tpA)	// bit � 1 pour  TP Adressables
					{			
				SetPos(0,0);
  				PutMessage("TP Adressables");
				displayNum (80,0,TA_TR);		//AFFICHE NUMERO DE CAPTEUR				
				Ecran_Constitution ();	
				tp2_constit++;
					}	


			}          // fin de if (Tab_Etat[TA])

		}		// fin de for(TA=1;.....
}




void ecran_memo_TPresistifs() // �cran ou sont sauvegard�s voie, comment et central
{
			unsigned char TR;           // num�ro du capteur � renseigner
				tp2_constit=0;  // utilis� pour tp adressables � voir si n�cessaire

				ClearScreen ();								// Effacement de tout l'�cran
 				if (F_sauve_mes_tpR)
						{			 
				SetPos(0,0);
  				PutMessage("TP Resistif");
				plot(26,0);  // pour �
				plot(25,1);	// pour �
						}				
				Ecran_Constitution ();
}

void ecran_memo_Debitmetres() // �cran ou sont sauvegard�s voie, comment et central
{
			unsigned char Deb;           // num�ro du capteur � renseigner
				tp2_constit=0;  // utilis� pour tp adressables � voir si n�cessaire

				ClearScreen ();								// Effacement de tout l'�cran
 				if (F_sauve_mes_Deb)
						{			 
				SetPos(0,0);
  				PutMessage("DEBITMETRE");
//				plot(26,0);  // pour �
//				plot(25,1);	// pour �
						}				
				Ecran_Constitution ();
}




//version light de saisie de la constitution
void Ecran_Constitution ()   // =============================== VERSION LIGHT !!!!!!=====================================
//void Ecran_Constitution_light ()
{
			unsigned char i;			//code ascii du caract�re s�lectionn� (lettres)
			unsigned char j;			//code ascii du caract�re s�lectionn� (chiffres)
			unsigned char k;			// variable 
			unsigned char posC;			// position du caract�re pour la constitution
			unsigned char posV;			// position du caract�re pour la 2eme ligne (Voie)
			unsigned char posR;			// position du caract�re pour la 2eme ligne (Robinet)
			unsigned char posT;			// position du caract�re pour la 2eme ligne (Tete)
			unsigned char posCe;		// position du caract�re pour le central
			unsigned char posCinit;		// position du 1er caract�re pour la constitution
			unsigned char posVinit;		// position du 1er caract�re pour la Voie
			unsigned char posRinit;		// position du 1er caract�re pour le Robinet
			unsigned char posTinit;		// position du 1er caract�re pour la T�te
			unsigned char posCeinit;	// position du 1er caract�re pour le Central
			unsigned char lar;			// largeur 1 caract�re 
			unsigned char caractere;	// si 0 caract�res A B C......si 1 caract�res 0 1 2 3
			unsigned char mem;			// autorisation de sauvegarde (=0)
			unsigned char NoCaractCo;		// num�ro du caract�re entr� pour la Constitution
			unsigned char NoCaractV;	// num�ro du caract�re entr� pour la Voie (2 caract�res)
			unsigned char NoCaractR;	// num�ro du caract�re entr� pour le Robinet (2 caract�res)
			unsigned char NoCaractT;	// num�ro du caract�re entr� pour la Tete
			unsigned char NoCaractCe;	// num�ro du caract�re entr� pour le Central
			unsigned char L_cv;			// pour indiquer si on est sur la ligne "constit" ou sur la ligne "Voie,Rob ou ....
//			unsigned char TA;           // num�ro du capteur � renseigner
			unsigned char valTA;        // validation des infos du capteur � renseigner
//			unsigned char tp2_constit;			// indique si on est au tp2 ou plus, pour afficher les champs communs d�ja remplis
//				Tab_Type_Enr[0]=0;  			//1 octets  pour m�moriser le type d'enregistrement (TPA=0,Groupe=1,D�bitm�tre=2,TPR=3,Icra=4)  PAR DEFAUT SUR TPA
				mem=0;			// 
				valTA=0;
				caractere = 0;  //chiffres (1) ou lettres (0)
				NoCaractCo=0;		// num�ro du caract�re entr� pour la constitution (de 0 � 9)
				NoCaractV=0;	// num�ro du caract�re entr� pour la voie et le robinet (de 0 � 1)
				NoCaractR=0;	// num�ro du caract�re entr� pour le robinet (de 0 � 1)
				NoCaractT=4;	// num�ro du caract�re entr� pour la Tete (de 4 � 9) de 4 parce qu'apres Voie et Rob dans tab
				NoCaractCe=0;	// num�ro du caract�re entr� pour le Central (de 0 � 14)  15 caract�res
				L_cv = 0;       //0 = �criture sur ligne Voie,  1 = �criture sur ligne Rob, 2 = �criture sur central

//				niveau_batt();
//				SetPos(0,0);
//  				PutMessage("SAUVEGARDE DES MESURES");				// Ecrire un message  nu_retour




//				k=0;
//				posC = k+43; // position du 1er caract�re pour constit
				posC = 44;		// position du 1er caract�re pour constit
				posCinit=posC;	//sauvegarde la position du 1er caract�re pour constit	
				posV=24;			// position du premier caractere pour Voie
				posVinit=posV;	//sauvegarde la position du 1er caract�re pour voie	
//				posR=51;			// position du premier caractere pour Robinet
				posR=45;			// position du premier caractere pour Robinet  86
				posRinit=posR;	//sauvegarde la position du 1er caract�re pour Robinet	
				posT=25;			// position du premier caractere pour t�te
//				posT=83;			// position du premier caractere pour t�te
				posTinit=posT;	//sauvegarde la position du 1er caract�re pour t�te	
				posCe=41;			// position du premier caractere pour central
				posCeinit=posCe;//sauvegarde la position du 1er caract�re pour central

//				mes_groupe = 1;  // POUR TEST UNIQUEMENT 
		if (!mes_groupe){  // <==  si diff�rent d'un enregistrement de groupe de pressu


				SetPos(2,30);
  				PutMessageInv("Voie");					
				vline(1,24,32);
//				plot(posV,30);  	 //22
//				plot(posV+6,30);	//28
//				plot(posV+12,30);	//34
				SetPos(25,30);
 				PutMessage("     "); 					
				displayNum (25,30,Val_Voie);					

				SetPos(2,38);
  				PutMessage("Comment");				
				plot(posR,38);   //51
				plot(posR+6,38);	 //57
				plot(posR+12,38);	 //57
				plot(posR+18,38);	 //57
				plot(posR+24,38);	 //57
				plot(posR+30,38);	 //57
				plot(posR+36,38);	 //57

				SetPos(2,40); //46
  		   		PutMessage("Central . . . . . . . . . . . . . . ");



	// LECTURE DERNIER Central m�moris�
				ad_der_central = 0x10;  
					ReadEEPROM(ad_der_central);     // lecture eeprom pic
					k=EEDATA;
			if(k>0){
				for (i=0;i<=13;i++)
					{
					ReadEEPROM(ad_der_central+i);     // lecture eeprom pic
					Tab_Central[i]=EEDATA;
					DELAY_MS (5);	//*
 				if (Tab_Central[0]!=32){   //32 = " "
					j=Tab_Central[i];
					SetPos(posCe,40);  //42
					PutChar(j);
					if (j==32){
					plot(posCe,46);	 //57  //42
					}	
					posCe+=6;
					posCe+1;
//					NoCaractCe+=1;  // pour positionner au dernier caract�re du champ central
						}
					}							
//					else {
//					break;
//						}
//					NoCaractCe-=1;  // -1 car pour positionner au dernier caract�re du champ central sinon valeur = 14
				}	// fin de if(k>0)	
		}		// fin de 	if (!mes_groupe)

				DELAY_MS (1);
				DELAY_MS (1);
				DELAY_MS (1);



		// remise � 0 de voie et comment

//				for (i=0;i<=9;i++)
//					{
				Tab_VoieRobTete[0]=Val_Voie;		// m�morise  voie
//					}


		if (mes_groupe){ // <== si =  enregistrement de groupe de pressu
				Tab_Type_Enr[0]=1;  			//1 octets  pour m�moriser le type d'enregistrement (TPA=0,Groupe=1,D�bitm�tre=2,TPR=3,Icra=4)
				SetPos(0,0);
  		   		PutMessage("GROUPE DE PRESSU");
				SetPos(10,18);
  		   		PutMessage("Pour sauvegarder,");
				SetPos(10,24);
  		   		PutMessage("precisez le nom :");
				plot(25,24);  // pour �
				plot(24,25);  // pour �
				SetPos(2,40); //46
  		   		PutMessage("Central: . . . . . . . . . . . . . ");
				while (!touche_BP6); // attente touche relach�e
							}// <==

			
				i=65; // lettre A par d�faut
				j=49; // chiffre 1 par d�faut

				// lettres par d�faut s�lectionn�
				FillBlackZone(39,54,51,63);
				SetPos(43,63);	 //52			// par d�faut choix caract�re A 
				PutCharInv(i);
				// chiffres
				FillWhiteZone(79,54,91,63);
				SetPos(83,63);	 //52			// par d�faut choix chiffre 1 
				PutChar(j);					

//				SetPos(pos_curseur+6,36);
//				PutChar(94);		//CURSEUR caract�re "^"			


				L_cv = 0;	//	pour d�marrer sur Voie
	//######BOUCLE DE SAISIE INFORMATIONS###########

//				while(touche_BP5!=0 )		// attente touche  
				while(touche_BP5!=0 && valTA==0 )		// attente touche  et  saisie capteur termin�e


	{		// boucle principale

				niveau_batt();

				lar = 6;			// largeur caract�re standard sauf W...
			

		if (!mes_groupe){ // <== si diff�rent d'un enregistrement de groupe de pressu

				 if((touche_BP4==0 && touche_BP3==0 && L_cv<2)||(touche_BP4==0 && touche_BP0==0 && L_cv>0))
					{ 
					if (touche_BP3==0){	
					L_cv += 1;
					}				
					 if(touche_BP0==0){					
					L_cv -= 1;
					}
	
				switch (L_cv)  // s�lection du champ � compl�ter
					{
				case 0:
				SetPos(2,30);
  				PutMessageInv("Voie"); 					
				vline(1,24,32);
				SetPos(2,38);
  				PutMessage("Comment");					
				unvline(1,32,40);
				SetPos(2,46);
  				PutMessage("Central");					
				unvline(1,40,48);
				break;
				case 1:
				SetPos(2,30);
  				PutMessage("Voie"); 					
				unvline(1,24,32);
				SetPos(2,38);
  				PutMessageInv("Comment");					
				vline(1,32,40);
				SetPos(2,46);
  				PutMessage("Central");					
				unvline(1,40,48);
				break;
				case 2:
				SetPos(2,30);
  				PutMessage("Voie"); 					
				unvline(1,24,32);
				SetPos(2,38);
  				PutMessage("Comment");					
				unvline(1,32,40);
				SetPos(2,46);
  				PutMessageInv("Central");					
				vline(1,40,48);
				break;
						}
		      	//######CHOIX CHIFFRES###########
				 if(L_cv == 0)
					 		{ 	
				// chiffres
				box(39,54,51,63);
				box(79,54,91,63);
				FillBlackZone(80,55,90,62);
				SetPos(83,63);	 //52		
				PutCharInv(j);					
				FillWhiteZone(40,55,50,62);
				SetPos(43,63);	 //52			
				PutChar(i);
				caractere = 1;
							}
		      	//######CHOIX LETTRES###########  si Obs: ou central
					else  
					{
				// lettres par d�faut
				box(39,54,51,63);
				box(79,54,91,63);
				FillWhiteZone(80,55,90,62);
				SetPos(83,63);	 //52			
				PutChar(j);					
				FillBlackZone(40,55,50,62);
				SetPos(43,63);	 //52			// par d�faut choix caract�res A.....
				PutCharInv(i);
				caractere = 0;
				DELAY_MS (20);
					}

					
				DELAY_MS (20);
				while(!touche_BP3 || !touche_BP0);	    // attente touches relach�es
				DELAY_MS (20);
		   			 } 
				 
				}//	 fin de 	if (!mes_groupe){


		if (mes_groupe)  // si mesure groupe
				{	
				L_cv = 2; //pour �crire sur central:.....
//				while (touche_BP6==0); // attente touche relach�e
				}			




				box(39,54,51,63);
				box(79,54,91,63);

		      	//######CHOIX CHIFFRES###########
				 if((touche_BP2==0 && touche_BP4!=0)|| (L_cv==0))   // L_cv = 0 si VOIE
//				 if(touche_BP2==0 && touche_BP4!=0)
					 		{ 	
				// chiffres
				box(39,54,51,63);
				box(79,54,91,63);
				FillBlackZone(80,55,90,62);
				SetPos(83,63);	 //52		
				PutCharInv(j);					
				FillWhiteZone(40,55,50,62);
				SetPos(43,63);	 //52			
				PutChar(i);
				caractere = 1;
							}
		      	//######CHOIX LETTRES###########
//		       if((touche_BP1==0 && touche_BP4!=0) || (L_cv==2)) 
				if(touche_BP1==0 && touche_BP4!=0)
					{
				// lettres par d�faut
				box(39,54,51,63);
				box(79,54,91,63);
				FillWhiteZone(80,55,90,62);
				SetPos(83,63);	 //52			
				PutChar(j);					
				FillBlackZone(40,55,50,62);
				SetPos(43,63);	 //52			// par d�faut choix caract�res A.....
				PutCharInv(i);
				caractere = 0;
				DELAY_MS (20);
							}


				//############Les lettres#############
			if (caractere ==0)     // Les lettres
				{
		       if(touche_BP0==0 && touche_BP4!=0 && (i<122))	
					{ 	
				FillBlackZone(39,54,51,63);
				SetPos(43,63); 	//52
				PutCharInv(++i);					
				DELAY_MS (100);
				while(touche_BP0==0)	    // attente touche relach�e
						{
					if (touche_BP0==0)
							{
					DELAY_MS (500);
						while(touche_BP0==0 && touche_BP4!=0 && (i<122))
								{
							FillBlackZone(39,54,51,63);
							SetPos(43,63); 	//52
							PutCharInv(++i);					
							DELAY_MS (200);
								}	
							}
						} // FIN  while(touche_BP0==0)		// attente touche relach�e 

					}

		       if(touche_BP3 ==0  && touche_BP4!=0 && (i>65))	
					{ 							
				FillBlackZone(39,54,51,63);
				SetPos(43,63);   //52
				PutCharInv(--i);					
				DELAY_MS (100);
				while(touche_BP3==0)		// attente touche relach�e
						{
					if (touche_BP3==0)
							{
					DELAY_MS (500);
						while(touche_BP3==0 && touche_BP4!=0 && (i>65))
								{
							FillBlackZone(39,54,51,63);
							SetPos(43,63);   //52
							PutCharInv(--i);					
							DELAY_MS (300);
								}	
							}
						}

					}				 
				 }    	// fin if caractere = 0
				//#########################




				//############Les chiffres#############
			if (caractere ==1)      // Les chiffres
				{
    	       if(touche_BP0==0 && touche_BP4!=0 && j<57 && L_cv > 0)	
				{ 	
				FillBlackZone(79,54,91,63);
				SetPos(83,63); //52
				PutCharInv(++j);					
				DELAY_MS (100);
				while(touche_BP0==0)		// attente touche relach�e
						{
					if (touche_BP0==0)
							{
					DELAY_MS (500);
						while(touche_BP0==0 && touche_BP4!=0 && (j<57))
								{
							FillBlackZone(79,54,91,63);
							SetPos(83,63); //52
							PutCharInv(++j);					
							DELAY_MS (300);
								}	
							}
						} // FIN  while(touche_BP0==0)		// attente touche relach�e 
					}
			

		       if(touche_BP3 ==0  && touche_BP4!=0 && j>44 && L_cv > 0)	
					 { 							
				FillBlackZone(79,54,91,63);
				SetPos(83,63); //52
				PutCharInv(--j);					
				DELAY_MS (100);
				while(touche_BP3==0)		// attente touche relach�e
						{
					if (touche_BP3==0)
							{
					DELAY_MS (500);
						while(touche_BP3==0 && touche_BP4!=0 && (j>44))
								{
						FillBlackZone(79,54,91,63);
						SetPos(83,63); //52
						PutCharInv(--j);					
						DELAY_MS (300);
								}	
							}
						} // FIN  while(touche_BP3==0)		// attente touche relach�e 

					}				 
				 }    	// fin if caractere = 1
				//#########################



 
			
				//######################### ECRITURE  VOIE ###############
			// Incr�mentation n� de voie
	       if(!touche_BP0 && touche_BP4 && touche_BP6  && L_cv == 0 && Val_Voie<=99  && Val_Voie>=1)	
				{ 	
				Val_Voie+=1;
				SetPos(25,30);
  				PutMessage("     "); 					
				displayNum (25,30,Val_Voie);					
				Tab_VoieRobTete[0]=Val_Voie;		// m�morise  voie
				DELAY_MS (75);
				while(touche_BP0==0)		// attente touche relach�e
						{
					if (touche_BP0==0)
							{
					DELAY_MS (500);
						while(!touche_BP0 && touche_BP4 && touche_BP6   &&  Val_Voie<=99  && Val_Voie>=1)
								{
							Val_Voie+=1;
				SetPos(25,30);
  				PutMessage("     "); 					
				displayNum (25,30,Val_Voie);					
				Tab_VoieRobTete[0]=Val_Voie;		// m�morise  voie
							DELAY_MS (250);
								}	
							}
						} // FIN  while(touche_BP0==0)		// attente touche relach�e 
					}

			// D�cr�mentation n� de voie

		       if(!touche_BP3 && touche_BP4 && touche_BP6    && L_cv == 0 && Val_Voie<=99  && Val_Voie>1)	
					 { 							
						Val_Voie-=1;
				SetPos(25,30);
  				PutMessage("     "); 					
				displayNum (25,30,Val_Voie);					
				Tab_VoieRobTete[0]=Val_Voie;		// m�morise  voie
				DELAY_MS (75);
				while(touche_BP3==0)		// attente touche relach�e
						{
					if (touche_BP3==0)
							{
					DELAY_MS (500);
						while(!touche_BP3 && touche_BP4 && touche_BP6   && Val_Voie<=99  && Val_Voie>1)
								{
						Val_Voie-=1;
				SetPos(25,30);
  				PutMessage("     "); 					
				displayNum (25,30,Val_Voie);					
				Tab_VoieRobTete[0]=Val_Voie;		// m�morise  voie
						DELAY_MS (250);
								}	
							}
						} // FIN  while(touche_BP3==0)		// attente touche relach�e 

					}

				//#########################FIN ECRITURE  VOIE ###############

	

		


				//######################### ECRITURE Comment. ###############  L_cv = 1
		        if(!touche_BP6 &&  touche_BP4 && L_cv == 1 && NoCaractR<=6) // 	VOIE 3 + ROB 2 + TETE 5	 (ROB 2 + TETE 5 devient Comment7)
					{
				DELAY_MS (20);
//				if (caractere==1)	//pour n'�crire que des chiffres (1) )
//						{		   

						if (NoCaractR==0)
							{
						posR=posRinit;
						SetPos(posR,38);  // 
							}
						else
							{
						SetPos(posR+=6,38);
							}

				if (caractere==0){
				PutChar(i);
				Tab_VoieRobTete[NoCaractR+3]=i; 
				}
				else if (caractere==1){
				PutChar(j);
				Tab_VoieRobTete[NoCaractR+3]=j; 
				}



//						PutChar(j);     // j pour les chiffres
//						Tab_VoieRobTete[NoCaractR+3]=j;		// m�morise caract�res voie + Comment.   Le +3 est d� aux 3 caract�res de voie
//						NoCaractR++;
//						}		
				NoCaractR++;
				DELAY_MS (20);
				while(!touche_BP6);		// attente touche relach�e
					}
			
	//#########################FIN ECRITURE LIGNE Comment.###############

		

	//#########################CORRECTION Comment. ###############
				if(!touche_BP4 && !touche_BP1 &&  L_cv==1 && NoCaractR>0)		
					{
				DELAY_MS (20);
				NoCaractR-=1;

					if (NoCaractR==0)
							{
					posR=posRinit;
					SetPos(posR,38);  // 
							}
					else if (NoCaractR==1)
							{
					posR=posRinit+6;	
					SetPos(posR,38);
							}


					 
				FillWhiteZone(posR,24,posR+5,38);
				Tab_VoieRobTete[NoCaractR+3]=0;      // pour effacer les caract�res dans TAB
				plot(posR+1,38);  //pour r�afficher le point apr�s effacement			
				if (posR >=6) {posR-=6;}

				DELAY_MS (20);
				while(!touche_BP1);		// attente touche relach�e
					}
	//#########################FIN CORRECTION Comment. ###############
			


				//######################### ECRITURE CENTRAL ############### L_cv = 2

		        if(!touche_BP6 &&  touche_BP4 &&  L_cv == 2 && NoCaractCe<=13)   
			{
				if (NoCaractCe == 0)
					{
				SetPos(2,40); //46
  		   		PutMessage("Central . . . . . . . . . . . . . . ");
				SetPos(2,40); //46
  				PutMessageInv("Central");					
				for (k=0;k<=13;k++)
						{
					WriteEEPROM(ad_der_central+k,0x00);     // �criture int
					Tab_Central[k]=0x00;
					DELAY_MS (1);
						}

					posCe = posCeinit;
					SetPos(posCe,42);
					}
					else
					{
					SetPos(posCe+=6,46); //42
					}
				if (caractere==0){
				PutChar(i);
				Tab_Central[NoCaractCe]=i; 
				}
				else if (caractere==1){
				PutChar(j);
				Tab_Central[NoCaractCe]=j; 
				}
				NoCaractCe++;				
				DELAY_MS (20);
				while(touche_BP6==0);		// attente touche relach�e
			}
				//######################### FIN ECRITURE CENTRAL ###############


				//############# CORRECTION CENTRAL ############
				if(!touche_BP4 && !touche_BP1 &&  L_cv==2 && NoCaractCe>0)		
					{
				DELAY_MS (20);
				NoCaractCe-=1;

				FillWhiteZone(posCe,39,posCe+5,47);
				Tab_Central[NoCaractCe]=0;				  // pour effacer les caract�res dans TAB
				plot(posCe,46); //46  //pour r�afficher le point apr�s effacement
				if (posCe >=6) {posCe-=6;}
			
				DELAY_MS (20);
				while(!touche_BP1);		// attente touche relach�e
					}
				//############# FIN CORRECTION CENTRAL ############



					DELAY_MS (20);

			//#########################POUR Sauvegarde de la page###############				
				if(touche_BP4==0 && touche_BP6==0)
					{
					valTA=1;    // valide la fin des informations pour le capteur en cours
				DELAY_MS (20);
				while(touche_BP4==0 && touche_BP6==0);		// attente touches relach�es				
					}

	} 	//######FIN  SAISIE COMMENTAIRES###########

}		//FIN ecran_constitution    // ===============================FIN VERSION LIGHT !!!!!!=====================================







void Sauvegarde_TA_TR_Deb()
{
 unsigned char mem;
unsigned char i;
			//############SAUVEGARDE############
//				DELAY_MS (50);
//				while (touche_BP5==0);
//				DELAY_MS (50);
				mem = 1; //pointe OUI
				SetPos(0,0);

 				if (F_sauve_mes_tpA==1){				// bit � 1 sauvegarde des TP Adressables
 				PutMessage("TP Adressables");		// 
				Tab_Type_Enr[0]=0; // (TPA=0,Groupe=1,D�bitm�tre=2,TPR=3,Icra=4)
					}			
 				if (F_sauve_mes_tpR==1){				// bit � 1 sauvegarde des TP R�sistifs
 				PutMessage("TP Resistif");		// 
				plot(25,0);  // pour �
				plot(24,1);  // pour �
				Tab_Type_Enr[0]=3; // (TPA=0,Groupe=1,D�bitm�tre=2,TPR=3,Icra=4)
					}			
 				if (F_sauve_mes_Deb==1){				// bit � 1 sauvegarde des TP R�sistifs
				SetPos(0,0);
 				PutMessage("DEBITMETRE");		// 
//				plot(25,0);  // pour �  // voir position du �
//				plot(24,1);  // pour �
				Tab_Type_Enr[0]=2; // (TPA=0,Groupe=1,D�bitm�tre=2,TPR=3,Icra=4)
					}			

				SetPos(5,24);
  				PutMessage("Confirmez la sauvegarde");		// Ecrire un message  nu_retour
				SetPos(25,32);
  				PutMessage("des informations");		// Ecrire un message  nu_retour

				SetPos(35,50);
  				PutMessage("OUI");		// Ecrire OUI
				SetPos(75,50);
  				PutMessage("NON");		// Ecrire NON
				flecheH(40,60);								



				while (touche_BP6!=0 )
					{
				if(touche_BP2==0){		 
					flecheH(80,60);								// fl�che pour NON et m�mo � 0
				FillWhiteZone(40,57,48,60);
				mem = 0;
							  	}			
				if(touche_BP1==0){		
					flecheH(40,60);								// fl�che pour OUI et m�mo � 1
				FillWhiteZone(80,57,88,60);
				mem = 1;
							  	}
					}				


				if (touche_BP6==0 && mem==1)		
					{
// ###############################################ici �crire caract�res de l'horodatage dans tab_constit pour tableau de sauvegarde
// valeurs de l'horodatage en ascii   => � voir
				Tval = get_time(0x04);		//date
				decode();
					Tab_Horodatage[0]=Dizaines+48;
					Tab_Horodatage[1]=Unites+48;
				Tval = get_time(0x05);		//mois
				decode();
					Tab_Horodatage[2]=Dizaines+48;
					Tab_Horodatage[3]=Unites+48;
				Tval = get_time(0x06);		//ann�e
				decode();
					Tab_Horodatage[4]=Dizaines+48;
					Tab_Horodatage[5]=Unites+48;
				Tval = get_time(0x02);		//heures
				decode();
					Tab_Horodatage[6]=Dizaines+48;
					Tab_Horodatage[7]=Unites+48;
				Tval = get_time(0x01);		//minutes
				decode();
					Tab_Horodatage[8]=Dizaines+48;
					Tab_Horodatage[9]=Unites+48;

				
// ##############################################################sous-programme de sauvegarde ici
				ClearScreen ();
	
				//***********Dernier Central m�moris� ***********  pour ne pas avoir � le ressaisir � chaque fois dans l'�cran de renseignements
				ad_der_central = 0x10;  // Dans eeprom du pic!!
				for (i=0;i<=13;i++)
					{
				WriteEEPROM(ad_der_central+i, Tab_Central[i]);     // �criture int
					}



 				Save_tpa_i2c();  //<================= SAUVEGARDE DES MESURES, DES INFOS DU CENTRAL, VOIE etc.........

//				FillWhiteZone(0,40,127,63);
				SetPos(10,40);
		 		PutMessage("Sauvegarde effectuee    ");		   
				plot(107,40);  // pour �
				plot(106,41);  // pour �
				F_aff_sauve_tpA=0;	// positionne le bit � 0 pour ne pas afficher Sauvegarde ?   
				F_aff_sauve_tpR=0;	// positionne le bit � 0 pour ne pas afficher Sauvegarde des TP r�sistifs?   
				F_aff_sauve_Deb=0;	// idem pour le d�bit
				F_sauve_mes_tpA=0;	// bit pour m�mo sauvegarde valid�e des mesures des tp adressables
				F_sauve_mes_tpR=0;	// bit pour m�mo sauvegarde valid�e des mesures des tp r�sistifs
				F_sauve_mes_Deb=0; 		// flag pour m�mo sauvegarde valid�e des mesures des d�bitm�tres
					}

				else if (touche_BP6==0 && mem==0)
					{						
				ClearScreen ();	
//				FillWhiteZone(0,40,127,63);
 				SetPos(10,24);
 		 		PutMessage("Sauvegarde annulee     ");	 
				plot(99,24);  // pour �
				plot(98,25);  // pour �
				F_aff_sauve_tpA=0;	// positionne le bit � 0 pour ne pas afficher Sauvegarde ?     modif ajout� le 5/5/2015
				F_aff_sauve_tpR=0;	// positionne le bit � 0 pour ne pas afficher Sauvegarde des TP r�sistifs?   modif: ajout� le  5/5/2015
				F_aff_sauve_Deb=0;	// idem pour le d�bit
					}

//				DELAY_SEC (1);
				DELAY_MS (500);
				
				while (touche_BP6==0 );   // attend touche relach�e
				
	//############FIN SAUVEGARDE############


}   //############FIN SAUVEGARDE_TA_TR############





void ecran_groupe()
{
char posz;      //position des labels en x
char posx;      //positions en x des parametres
				posz = 70;	//position des labels en x
				posx = 0;

				niveau_batt();
				SetPos(120,8);								
				PutMessage("G");  
				SetPos(120,16);								
				PutMessage("R");  
				SetPos(120,24);								
				PutMessage("O");  
				SetPos(120,32);								
				PutMessage("U");  
				SetPos(120,40);								
				PutMessage("P");  
				SetPos(120,48);								
				PutMessage("E");  
				vline(117,7,56);
				vline(127,7,56);
				hline(118,127,6);
				hline(118,127,56);

				SetPos(posx,0);								
				PutMessage	("P.Tampon         "); 
				SetPos(posz+3,0);								
				PutMessage(" mBar"); 
 
				SetPos(posx,8);								
				PutMessage	("P.Sortie          "); 
				SetPos(posz+3,8);								
				PutMessage(" mBar"); 

 				SetPos(posx,16);								
				PutMessage	("P.Secours        ");  
				SetPos(posz+3,16);								
				PutMessage(" mBar");
 
 				SetPos(posx,24);								
				PutMessage("Pt.Rosee                  ");
				plot(posx+31,24);  // pour �
				plot(posx+30,25);  // pour �
				SetPos(posz-3,24);								
				PutMessage(" K");
//				unbox(posz+29,24,posz+31,26);
				box(posz+30,24,posz+32,26);
 				SetPos(posz+34,24);		//34						
				PutMessage("C");
 
				SetPos(posx,32);								
				PutMessage("Debit G         "); 
				plot(posx+9,32);  // pour �
				plot(posx+8,33);  // pour �
 				SetPos(posz-3,32);								
				PutMessage(" L/H ");

				SetPos(posx,40);								
				PutMessage("T.Cycle         ");
 				SetPos(posz-3,40);								
				PutMessage(" Sec ");
  
				SetPos(posx,48);								
				PutMessage("T.Moteur       ");
 				SetPos(posz-3,48);								
				PutMessage(" H   ");
  
				SetPos(0,56);								
				PutMessage("P.220");
				voyant_off(33,59);
  
				SetPos(41,56);								
				PutMessage(" P.48");  
				voyant_off(72,59);


}





void aff_mes_groupe()
{
char test;
char i;
char posw;		//position des valeurs en x
char posy;		//position des valeurs en y
char posz;      //position des labels en x
char posx;		//�tiquettes
unsigned int pression_tampon;    							// 4 octets                            
unsigned int pression_cable;    							// 4 octets  
unsigned int pression_secours;    							// 4 octets  
 float pt_roseeK;   // en Kelvin    						// 4 octets  en int
unsigned char pt_roseeC;	// en �C    					// 2 octets  
unsigned char signe;		// signe si pt de rosee en �C   // 2 octets  
unsigned int debit;    										// 4 octets  
unsigned int tps_cycle_mot;    								// 4 octets  
unsigned int tps_fonct_mot;    								// 4 octets  
															// total = 32 octets
//extern unsigned char mes_groupe;// utilis� dans l'�cran de la saisie de la constitution pour n'afficher que central........

unsigned int temp;		//variable temporaire
 
//METTRE UNE SECURITE POUR LA SOUSTRACTION Xval-900

		test = 0;    //�  utiliser pour tester l'affichage et les calculs 			0 =  normal
//		test = 1;    //permet de  tester l'affichage et les calculs avec des valeurs pr�charg�es		1 = test 

				posx = 0;
				posw = 50;   //position des valeurs en x
				posz = 70;	//position des labels en x
				i=Num_Ligne;

		//SECURITE POUR LA SOUSTRACTION Xval-900		
		if	(Tab_Pression[i]<900 && Tab_Pression[i]>=897)   // 3 hz de marge
						{
					Tab_Pression[i]=900;
						}  
			else if (Tab_Pression[i]<897) 
						{
					(Tab_Pression[i]=0x0000);
						}  // d�faut r�ponse

		switch (i)
			{
//		if	(Tab_Pression[i]==0)
//				{	break;}


		case 0:
			// N� Ligne pas utilis�e (pour d�bitm�tres)
		break;


		case 1:
		//* PRESSION TAMPON  
		posy = 0 ;	// position x Ligne d'�criture dans le tableau
		if (Tab_Pression[i] >= 900 || test==1)
			{
				pression_tampon = ((Tab_Pression[i])-900)*10 ;  //mise � l'�chelle tampon
				if (test==1){	pression_tampon = (1253-900)*10 ; } //test

					if (pression_tampon <1000 && pression_tampon >=100)
							{		
						posw+=6;
							}		
					if (pression_tampon <100 && pression_tampon >=10)
							{		
						posw+=12;
							}		
					if (pression_tampon <10 )
							{		
						posw+=18;
							}
					if ((pression_tampon>=def_tampon_H)||(pression_tampon<=def_tampon_B))			// Si d�faut => affichage inverse
							{
				SetPos(posx,0);								
				PutMessageInv("P.Tampon         "); 
				SetPos(posz+3,0);								
				PutMessageInv(" mBar"); 
						displayNumInv (posw,posy,pression_tampon);
							}	
						else 	
							{
						displayNum (posw,posy,pression_tampon);				// Pression  affichage normal
							}	
			}
			else {
				SetPos(posx,0);								
				PutMessageInv("P.Tampon         "); 
				SetPos(posz+3,0);								
				PutMessageInv(" mBar"); 
 				}	
		break;

		case 2:
		//* PRESSION CABLE
		posy = 8 ;	// position x Ligne d'�criture dans le tableau

		if (Tab_Pression[i] >= 900 || test==1)
			{
						  //mise � l'�chelle cable
						pression_cable = ((Tab_Pression[i])-900) ;  //mise � l'�chelle cable
				if (test==1){pression_cable = ((1463)-900) ; } //test

					if (pression_cable <1000 && pression_cable >=100)
							{		
						posw+=6;
							}		
					if (pression_cable <100 && pression_cable >=10)
							{		
						posw+=12;
							}		
					if (pression_cable <10 )
							{		
						posw+=18;
							}

					if ((pression_cable>=def_cable_H)||(pression_cable<=def_cable_B))			// Si d�faut => affichage inverse
							{
				SetPos(posx,8);								
				PutMessageInv("P.Sortie          "); 
				SetPos(posz+3,8);								
				PutMessageInv(" mBar"); 
						displayNumInv (posw,posy,pression_cable);
							}	
						else 	
							{
						displayNum (posw,posy,pression_cable);				// Pression  affichage normal
							}
			}
			else {
				SetPos(posx,8);								
				PutMessageInv("P.Sortie          "); 
				SetPos(posz+3,8);								
				PutMessageInv(" mBar");
				}	
		break;



		case 3:
		//* PRESSION SECOURS  
		posy = 16 ;	// position x Ligne d'�criture dans le tableau
		if (Tab_Pression[i] >= 900 || test==1)
			{
						pression_secours = ((Tab_Pression[i])-900)*10 ;  //mise � l'�chelle secours
				if (test==1){pression_secours = (1221-900)*10 ; } //test

					if (pression_secours <1000){posw+=6;}
			
					if (pression_secours<=def_secours_B1)			// Si d�faut => affichage inverse
							{
 				SetPos(posx,16);								
				PutMessageInv("P.Secours        ");  
				SetPos(posz+3,16);								
				PutMessageInv(" mBar");
						displayNumInv (posw,posy,pression_secours);
							}	
						else 	
							{
						displayNum (posw,posy,pression_secours);				// Pression  affichage normal
							}
			}	
			else {
 				SetPos(posx,16);								
				PutMessageInv	("P.Secours        ");  
				SetPos(posz+3,16);								
				PutMessageInv(" mBar");
				}	
		break;





		case 4:
		//* PT DE ROSEE
		posy = 24 ;	// position x Ligne d'�criture dans le tableau

		if (Tab_Pression[i] >= 900 || test==1)
			{
						pt_roseeK = ((Tab_Pression[i]-900)*0.0314)+235 ;  //mise � l'�chelle  pt d ros�e
				if(test==1){pt_roseeK = ((980-900)*0.0314)+235 ;}  //test
						pt_roseeK = (int)pt_roseeK;		
							if (pt_roseeK<273)   // convertion pt de rosee en �C
								{
							pt_roseeC = 273-pt_roseeK;
							signe = 0;   // -
								}
							else		
								{
							pt_roseeC = pt_roseeK - 273;
							signe = 1;   // +
								}

					if (pt_roseeK>=def_rosee_H)			// Si d�faut => affichage inverse
							{

				SetPos(posx,24);								
				PutMessageInv("Pt.Rosee                  ");
				SetPos(posz-3,24);								
				PutMessageInv(" K");
						displayNumInv (posw+1,posy,pt_roseeK);
							if (signe == 0)
								{
							SetPos(posw+30,posy);								
							PutMessageInv("-");
								}
				unbox(posz+30,24,posz+32,26);
 				SetPos(posz+34,24);		//34						
				PutMessageInv("C");
						displayNumInv (posw+36,posy,pt_roseeC);

							}	
						else 	
							{
						displayNum(posw+1,posy,pt_roseeK);				// Pression  affichage normal  +1
							if (signe == 0)
								{
							SetPos(posw+30,posy);								
							PutMessage("-");
								}
						displayNum(posw+36,posy,pt_roseeC);
							}
				}	
			else {
 				SetPos(posx,24);								
				PutMessageInv("Pt.Rosee                  ");
				SetPos(posz-3,24);								
				PutMessageInv(" K");
				unbox(posz+30,24,posz+32,26);
 				SetPos(posz+34,24);		//34						
				PutMessageInv("C");
				}	
		break;


		case 5:
		//* DEBIT  
		posy = 32 ;	// position x Ligne d'�criture dans le tableau

		if (Tab_Pression[i] >= 900 || test==1)
			{
					// Mise � l'�chelle d�bit
					temp = (Tab_Pression[i]);    
			if (test==1){temp=1200;}
					if (temp>=900 && temp <=1100)	//900 � 1100
							{
						debit = (temp-900)*2;
							}
					if (temp>=1101 && temp <=1300)	//1101 � 1300
							{
						debit = ((temp-1101) * 4)+400;
							}
					if (temp>=1301 && temp <=1600)	//1301 � 1600
							{
						debit = ((temp-1301) * 10)+1200;
							}

					if (debit >= 1000 )
							{		
						displayNum (posw-5,posy,debit);				// DEBIT  affichage normal
			
							}
					if (debit <1000 && debit >=100)
							{		
						displayNum (posw+1,posy,debit);				// DEBIT  affichage normal
							}		
					if (debit <100 && debit >=10)
							{		
						displayNum (posw+7,posy,debit);				// DEBIT  affichage normal
							}		
					if (debit <10 )
							{		
						displayNum (posw+13,posy,debit);				// DEBIT  affichage normal
							}		
			}	
			else {
				SetPos(posx,32);								
				PutMessageInv("Debit G         "); 
 				SetPos(posz-3,32);								
				PutMessageInv(" L/H  ");
				}	
		break;


		case 6:
		//* TEMPS DE CYCLE MOTEUR  
		posy = 40 ;	// position x Ligne d'�criture dans le tableau

		if (Tab_Pression[i] >= 900 || test==1)
			{
					// Mise � l'�chelle temps de cycle moteur					   
					temp = (Tab_Pression[i]);    
	if (test ==1){ temp=965;}
				if (temp>=900 && temp <=1100)	//900 � 1100
							{
						tps_cycle_mot = temp-900;
							}
					if (temp>=1101 && temp <=1300)	//1101 � 1300
							{
						tps_cycle_mot = ((temp-1101) * 8)+200;
							}
					if (temp>=1301 && temp <=1600)	//1301 � 1600
							{
						tps_cycle_mot = ((temp-1301) * 18)+1800;
							}


					if (tps_cycle_mot >=1000)
							{		
						displayNum (posw-5,posy,tps_cycle_mot);				// tps_cycle  affichage normal
							}		
					if (tps_cycle_mot <1000 && tps_cycle_mot >=100)
							{		
						displayNum (posw+1,posy,tps_cycle_mot);				// tps_cycle  affichage normal
							}		
					if (tps_cycle_mot <100 && tps_cycle_mot >=10)
							{		
						displayNum (posw+7,posy,tps_cycle_mot);				// tps_cycle affichage normal
							}		
					if (tps_cycle_mot <10 )
							{		
						displayNum (posw+13,posy,tps_cycle_mot);				// tps_cycle  affichage normal
							}		
			}	
			else {
				SetPos(posx,40);								
				PutMessageInv("T.Cycle         ");
 				SetPos(posz-3,40);								
				PutMessageInv(" Sec  ");
				}	
		break;


		case 7:		// pas utilis�e
		break;


		case 8:
		//* TEMPS DE FONCTIONNEMENT MOTEUR  
		posy = 48 ;	// position x Ligne d'�criture dans le tableau
		if (Tab_Pression[i] >= 900 || test==1 )
			{
					// Mise � l'�chelle temps de fonctionnement moteur
					temp = (Tab_Pression[i]);    
	if (test ==1){ temp=1364;}
					if (temp>=900 && temp <=1100)	//900 � 1100
							{
						tps_fonct_mot = temp-900;
							}
					if (temp>=1101 && temp <=1300)	//1101 � 1300
							{
						tps_fonct_mot = ((temp-1101) * 8)+200;
							}
					if (temp>=1301 && temp <=1600)	//1301 � 1600
							{
						tps_fonct_mot = ((temp-1301) * 18)+1800;
							}

					if (tps_fonct_mot >=1000 )
							{		
						displayNum (posw-5,posy,tps_fonct_mot);				//   affichage normal
							}		
					else if (tps_fonct_mot >=100 && tps_fonct_mot <1000)
							{		
						displayNum (posw+1,posy,tps_fonct_mot);				//  affichage normal
							}		
					else if (tps_fonct_mot>=10 && tps_fonct_mot<100)
							{		
						displayNum (posw+7,posy,tps_fonct_mot);				//   affichage normal
							}		
					else  
							{		
						displayNum (posw+13,posy,tps_fonct_mot);				// affichage normal
							}		
			}

			else {
				SetPos(posx,48);								
				PutMessageInv("T.Moteur         ");
 				SetPos(posz-3 ,48);								
				PutMessageInv(" H    ");
				}	
		break;


		case 9:
		// PRESENCE 220V
		posy = 56 ;	// position x Ligne d'�criture dans le tableau
		if (Tab_Pression[i] >= 900 || test==1)
			{
					temp=(Tab_Pression[i]);    
		
		if ( test==1){	temp=1600;}
					if (temp>1300)
						{
					voyant_on(33,60);
//					SetPos(36,posy);								
//					PutMessage("ON");  
						}
					else
						{
					SetPos(0,posy);								
					PutMessageInv("P.220");  
//					SetPos(36,posy);								
//					PutMessageInv("OFF");  
						}
			}
			else {
				SetPos(0,posy);								
				PutMessageInv("P.220");  
				}	

		break;


		case 10:
		// PRESENCE 48V
	 	posy = 56 ;	// position x Ligne d'�criture dans le tableau
		if (Tab_Pression[i] >= 900 || test==1)
			{
					temp=(Tab_Pression[i]);    
		if ( test==1){	temp=1000;}
					if (temp>1300)
						{
					voyant_on(72,60);
					SetPos(98,posy);								
//					PutMessage("ON");  
						}
					else
						{
					SetPos(41,posy);								
					PutMessageInv(" P.48");  
//					SetPos(98,posy);								
//					PutMessageInv("OFF");  
						}
			}
			else {
				SetPos(41,posy);								
				PutMessageInv(" P.48");  
			}	

		break;
		}
	
}	 //  FIN : void aff_mes_groupe()



void ecran_TPR_Aff_memo()	//Utilis� dans l'affichage des TPR, des enregistrements en m�moire
		{
				niveau_batt();

				SetPos(0,0);
  				PutMessage("TP Resistif    ");		// Ecrire un message  nu_retour
				plot(26,0);  // pour �
				plot(25,1);	// pour �

				FillWhiteZone(10,30,119,45);  
				box(5,29,78,46);							// R�aliser un rectangle
				box(83,29,120,46);							// R�aliser un rectangle
				SetPos(0,20);								// Positionner le curseur
	     		PutMessage("                               ");	 
				SetPos(8,20);								// Positionner le curseur
  				PutMessage("Mesure (mbar)");	
 				SetPos(90,20);								// Positionner le curseur
				PutMessage("Etat");				  		// Ecrire un message  


			// AFFICHAGE VALEURS
				if (Freq_R >= 1000)
				{
				displayHNum (25,34,Freq_R); 
				}
				else if (Freq_R >= 900 && Freq_R < 1000){
				displayHNum (33,34,Freq_R);
				}
				else {		//pour afficher 0000
				displayHNum (26,34,Freq_R);  // affichage = 0   modif du 10/02/2016
				displayHNum (35,34,Freq_R);  // affichage = 0	modif du 10/02/2016
				displayHNum (44,34,Freq_R);  // affichage = 0	modif du 10/02/2016
				displayHNum (53,34,Freq_R);  // affichage = 0	modif du 10/02/2016
				}	
				hline(6,79,46) ;							//ligne horizontale
				
			//	Etat TPR 1 = OK, 2 = absent, 3 = D�faut, 4 = TPA pr�sent
				if ( Etat_TPR==2){
 				SetPos(86,36);								
				PutMessage("Absent");				  		
		 			}		
				else if (Etat_TPR==3){
				SetPos(86,32);								
				PutMessage("Defaut");				  		
				plot(95,32);  // pour �
				plot(94,33);  // pour �
					}		
				else if (Etat_TPR==4){			// Si un capteur trouv� dans D�tection TPA				
 				SetPos(25,32);							  
				PutMessageInv(" TPA PRESENT! ");			 
					}
				else if(Etat_TPR==1){
 				SetPos(95,36);								
				PutMessage("OK");				  			
					}	

		}  // fin �cran r�sistifs POUR l'affichage m�moire





void ecran_resistifs()	
		{
				ClearScreen ();								// Effacement de tout l'�cran

				niveau_batt();
				if(boucleTPR)
					{
				boucle1(95,2);
					}

				SetPos(0,0);
  				PutMessage("TP Resistif    ");		// Ecrire un message  nu_retour
				plot(26,0);  // pour �
				plot(25,1); // pour �



//			if (valid_sens==1)  // validation de l'affichage et r�glage sensibilit�  (N'est plus utilis�)
//				{
//				SetPos(8,8);
// 				PutMessage("Sensibilite:");		// Ecrire un message  nu_retour
//				}
//				displayNum (70,8,sensibilite_TPA); 
				
//				FillWhiteZone(5,29,120,48);  
				Led_rouge = 0; 
				FillWhiteZone(10,30,119,45);  
				box(5,29,78,46);							// R�aliser un rectangle
				box(83,29,120,46);							// R�aliser un rectangle
				SetPos(0,16);								// Positionner le curseur
	     		PutMessage("                               ");	 
				SetPos(8,16);								// Positionner le curseur
  				PutMessage("Mesure (mbar)");	
 				SetPos(90,16);								// Positionner le curseur
				PutMessage("Etat");				  		// Ecrire un message  

				if (Detection_TPA)  // valeur du flag par d�faut = 1 =  d�tection valid�e
				{
				SetPos(0,16);								// Positionner le curseur  
	     		PutMessage("DETECTION TP Adressable");	 

				}	
				else
				{
				SetPos(0,52);								// Positionner le curseur
	     		PutMessage("                               ");	 
				}

				SetPos(0,56);								// Positionner le curseur
	     		PutMessage("Pret pour Mesure         ");   //ok	 
				plot(13,57);
				plot(14,56);
				plot(15,57);
				DELAY_MS (100);
				//icimodif
		}  // fin �cran r�sistifs



void ecran_resistifs2()	
		{
int i;
				ClearScreen ();								// Effacement de tout l'�cran

				niveau_batt();
				if(boucleTPR)
					{
				boucle1(95,2);
					}

				SetPos(0,0);
  				PutMessage("TP Resistif    ");		// Ecrire un message  nu_retour
				plot(26,0);  // pour �
				plot(25,1); // pour �


				Led_rouge = 0; 
				FillWhiteZone(10,30,119,45);  
				box(5,29,78,46);							// R�aliser un rectangle
				box(83,29,120,46);							// R�aliser un rectangle
				SetPos(0,16);								// Positionner le curseur
	     		PutMessage("                               ");	 
				SetPos(8,16);								// Positionner le curseur
  				PutMessage("Mesure (mbar)");	
 				SetPos(90,16);								// Positionner le curseur
				PutMessage("Etat");				  		// Ecrire un message  

				if (Detection_TPA)  // valeur du flag par d�faut = 1 =  d�tection valid�e
				{
				SetPos(0,16);								// Positionner le curseur  
	     		PutMessage("DETECTION TP Adressable");	 
				SetPos(0,24);								// Positionner le curseur  
	     		PutMessage("La mesure d'un TPA a la ");	 
				plot(102,24);  // pour �
				plot(103,25); // pour �
				SetPos(0,32);								// Positionner le curseur  
	     		PutMessage("place d'un TPR indique   ");	 
				SetPos(0,40);								// Positionner le curseur  
	     		PutMessage("une valeur fausse !       ");	 
			
				SetPos(0,48);								// Positionner le curseur  
	     		PutMessage("La detection d'un TPA en");	 
				plot(23,48);  // pour �
				plot(22,49); // pour �
				SetPos(0,56);								// Positionner le curseur  
	     		PutMessage("panne est non garantie.   ");	 

				i=0;
				while (touche_BP6 && i<100 ){
				DELAY_MS (100);
				i++;
					}

				ClearScreen ();								// Effacement de tout l'�cran
				niveau_batt();
				SetPos(0,0);
  				PutMessage("TP Resistif    ");		// Ecrire un message  nu_retour
				plot(26,0);  // pour �
				plot(25,1); // pour �
				FillWhiteZone(10,30,119,45);  
				box(5,29,78,46);							// R�aliser un rectangle
				box(83,29,120,46);							// R�aliser un rectangle
				SetPos(0,16);								// Positionner le curseur
	     		PutMessage("                               ");	 
				SetPos(8,16);								// Positionner le curseur
  				PutMessage("Mesure (mbar)");	
 				SetPos(90,16);								// Positionner le curseur
				PutMessage("Etat");				  		// Ecrire un message  
				SetPos(0,16);								// Positionner le curseur  
	     		PutMessage("DETECTION TP Adressable");	 

				}	
				else
				{
				SetPos(0,52);								// Positionner le curseur
	     		PutMessage("                               ");	 
				}

				SetPos(0,56);								// Positionner le curseur
				PutMessage("Detection en cours        ");				  	 
				plot(9,56);  // pour �
				plot(8,57);  // pour �
//				plot(13,57);
//				plot(14,56);
//				plot(15,57);
				DELAY_MS (100);
				//icimodif
		}  // fin �cran r�sistifs







void ecran_TPR_Mes()
		{
int detect_CC; // si d�tection de court-circuit

Detection_TPA_V1_86=0; //flag pour version 1.86
Detection_TPA_V1_87=0; //flag pour version 1.87


	if (Detection_TPA) // si flag valid� on test la pr�sence d'un TPA
			{
				SetPos(0,56);								// Positionner le curseur
				PutMessage("Detection en cours        ");				  	 
				plot(9,56);  // pour �
				plot(8,57);  // pour �
				DELAY_MS (100);
				TPA_detecte = 0;
				detect_CC = 0;   // si d�tecte un court circuit sur la ligne, inutile de faire la d�tection TPA
				Mesure_TPR(); //test court-circuit ligne avant de d�tecter un TPA
			if (Freq_R==0)
			{
				detect_CC = 1;   // si d�tecte un court circuit sur la ligne, inutile de faire la d�tection TPA
			}		
			else
			{
				Test_ReponseCode();	// Test suppl�mentaire pour d�tection pr�sence TPA par la r�ponse � l'impulsion 80V pour lecture codage	
							// r�ponse dans codange.c   si TPA_detecte=1 => pr�sence TPA + led rouge
					if(TPA_detecte==0)  // si le test pr�c�dant ne d�tecte pas de TPA, on fait la boucle, sinon si TPA d�tect� c'est inutile
						{
						Boucle_Detection_TPA();
						}
			}
		}  // fin  de :	if (Detection_TPA)
				else
				{
//				FillWhiteZone(0,60,127,67);  
//				SetPos(0,56);								// Positionner le curseur
//				PutMessage("Mesure en cours     ");				  		// Ecrire un message  
				}

			if	(TPA_detecte )			// Si un capteur trouv� dans D�tection TPA
				{
//				FillWhiteZone(0,60,127,67);  
					Buzzer_on();  // active le buzzer 75mSec
				DELAY_MS (75);
					Buzzer_on();  // active le buzzer 75mSec
				FillBlackZone(5,29,120,46);	// avant 45
 				SetPos(25,32);
				Detection_TPA_V1_86=1; //flag pour V1.86
				Detection_TPA_V1_87=1; //FLAG V1.87							  
				PutMessageInv(" TPA PRESENT! ");			 
				SetPos(25,32);							  
				SetPos(0,56);								 
				PutMessage("Fin de detection         ");		// ou Erreur de mesure : � voir
				plot(43,56);  // pour �
				plot(42,57);  // pour �
				DELAY_MS (1000);
				return;
				}
			else if (detect_CC == 1)   // si d�tecte un court circuit sur la ligne, inutile de faire la d�tection TPA
				{
					Buzzer_on();  // active le buzzer 75mSec
				DELAY_MS (75);
					Buzzer_on();  // active le buzzer 75mSec
				Led_rouge = 1;				FillBlackZone(5,29,120,46);	// avant 45
 				SetPos(40,32);							  
				PutMessageInv("C/C Ligne! ");			 
				SetPos(30,32);							  
				SetPos(0,56);								 
				PutMessage("Fin de detection         ");		// ou Erreur de mesure : � voir
				plot(43,56);  // pour �
				plot(42,57);  // pour �
				DELAY_MS (500);
				Detection_TPA = 0;  //  d�tection d�valid�e puisque pas test possible de TPA
				return;
				}
				else 
				{
				Detection_TPA = 0;  //  d�tection d�valid�e puisque pas de pr�sence de TPA
				DELAY_MS (500);
				SetPos(95,36);								//pour effacer  OK  �crit dans mesures.c
				PutMessage("  ");				  		// OK
//				SetPos(95,36);								// Positionner le curseur
//				PutMessage("  ");				  		// OK
				}
				FillWhiteZone(0,56,127,67);  
				FillWhiteZone(6,30,77,45);  
				FillWhiteZone(84,30,119,45);  // ==119==
				SetPos(95,36);								// Positionner le curseur
				PutMessage("  ");				  		// OK
//				SetPos(0,52);								// Positionner le curseur
//	     		PutMessage("                               ");	 
				DELAY_MS (500);
				SetPos(0,56);								// Positionner le curseur
				PutMessage("Mesure en cours     ");				  		// Ecrire un message  
				SetPos(0,20);								// Positionner le curseur
	     		PutMessage("                               ");	 
				SetPos(8,20);								// Positionner le curseur
  				PutMessage("Mesure (mbar)");	
 				SetPos(90,20);								// Positionner le curseur
				PutMessage("Etat");				  		// Ecrire un message  
//				FillWhiteZone(6,30,77,45);  
//				FillWhiteZone(84,30,119,45);  // ==119==
				DELAY_MS (500);
				Mesure_TPR();
//				FillWhiteZone(6,30,77,45);  
//				FillWhiteZone(84,30,119,45);  // ==119==
//				DELAY_MS (200);


				SetPos(0,56);								// Positionner le curseur
				PutMessage("Fin de Mesure           ");				  		// Ecrire un message  


			// AFFICHAGE VALEUR
			


				if (Freq_R >= 1000)
				{
				displayHNum (25,34,Freq_R); 
				}
				else if (Freq_R >= 900 && Freq_R < 1000){
				displayHNum (33,34,Freq_R);
				}
				else {		//pour afficher 0000
				Buzzer_on();  // active le buzzer 75mSec
				Led_rouge = 1;				displayHNum (26,34,Freq_R);  // affichage = 0   modif du 10/02/2016
				displayHNum (35,34,Freq_R);  // affichage = 0	modif du 10/02/2016
				displayHNum (44,34,Freq_R);  // affichage = 0	modif du 10/02/2016
				displayHNum (53,34,Freq_R);  // affichage = 0	modif du 10/02/2016
				}	
				hline(6,79,46) ;							//ligne horizontale

				if ( Freq_R==9999){
 				SetPos(86,32);								
				PutMessage("Absent");				  		
				Etat_TPR=2;				// Absent code 2
		 			}		
				else if ( Freq_R==0){
				SetPos(86,32);								
				PutMessage("Defaut");				  		
				plot(95,32);  // pour �
				plot(94,33);  // pour �
				Etat_TPR=3;				// D�faut code 3
					}		
				else if (TPA_detecte){			// Si un capteur trouv� dans D�tection TPA				
				Etat_TPR=4;				// TPA d�tect� code 4 
					}
				else {
 				SetPos(95,36);								
				PutMessage("OK");				  			
				Etat_TPR=1;				// OK code 1 
					}	
//				Pression_TPR=Freq_R;
//				DELAY_MS (1000);



		}  // fin �cran TPR

void ecran_debitmetres_memo()

{
				niveau_batt();
//				if(mem_DEB)
//					{
//				boucle_8(90,3);
//					}

				SetPos(0,0);
  				PutMessage("DEBITMETRE   ");		// le "S" de "DEBITMETRES" supprim� 		modif du 10/02/2016
				box(5,29,78,48);							// R�aliser un rectangle
				box(83,29,123,48);							// R�aliser un rectangle

 				SetPos(8,20);								// Positionner le curseur
  				PutMessage("Mesure (L/H)");	
 				SetPos(95,20);								// Positionner le curseur
				PutMessage("Etat");				  		// Ecrire un message  
  				SetPos(15,60);								// Positionner le curseur

				if (Pres_Debit == 0) //soit absent
					{
				SetPos(88,36);							
				PutMessage("Absent");
				Etat_Deb=2;				  	 
					}
				else
					{
				if (debit_cable < 10){
				displayHNum (47,34,debit_cable);
						}	
				if (debit_cable >9 && debit_cable<100){
				displayHNum (38,34,debit_cable);
						}	
				if (debit_cable >99){
				displayHNum (29,34,debit_cable);
						}	
				SetPos(88,36);							
				PutMessage("OK");
				Etat_Deb=1;				  	 
					}
				DELAY_MS (200);
	

}


void ecran_debitmetres()

{
				niveau_batt();
				if(boucleDEB)
					{
				boucle1(95,2);
					}
				SetPos(0,0);
  				PutMessage("DEBITMETRE   ");		// le "S" de "DEBITMETRES" supprim� 		modif du 10/02/2016
				box(5,29,78,48);							// R�aliser un rectangle
				box(83,29,123,48);							// R�aliser un rectangle
 				SetPos(8,20);								// Positionner le curseur
  				PutMessage("Mesure (L/H)");	
 				SetPos(95,20);								// Positionner le curseur
				PutMessage("Etat");				  		// Ecrire un message  
  				SetPos(15,60);								// Positionner le curseur
				SetPos(0,56);								// Positionner le curseur
				PutMessage("Pret Mesure ");	// Ecrire un message  
}


void ecran_DEB_mes()
{
unsigned int temp;

//				FillWhiteZone(6,30,77,47);  
//				FillWhiteZone(84,30,122,47);  


				FillWhiteZone(0,55,127,63);
				SetPos(0,56);								// Positionner le curseur
				PutMessage("Mesure en cours           ");				  		// Ecrire un message  
				Boucle_Mesure_PCPG();
				mes_debit=0;   // utilis� dans la boucle mesure (d�bit)
				FillWhiteZone(6,30,77,47);  
				FillWhiteZone(84,30,122,47);  
				DELAY_MS (200);
				SetPos(0,60);								// Positionner le curseur
				PutMessage("                         ");				  		// Ecrire un message  

				Calcul_Deb (); // mise � l'�chelle du d�bit

				if (Pres_Debit == 0) //soit absent
					{
				SetPos(88,36);							
				PutMessage("Absent");
				Etat_Deb=2;				  	 
					}
				else
					{
				if (debit_cable < 10){
				displayHNum (47,34,debit_cable);
						}	
				if (debit_cable >9 && debit_cable<100){
				displayHNum (38,34,debit_cable);
						}	
				if (debit_cable >99){
				displayHNum (29,34,debit_cable);
						}	//				displayHNum (28,34,debit_cable);
				SetPos(88,36);							
				PutMessage("OK");
				Etat_Deb=1;				  	 
					}
				DELAY_MS (1000);
				DELAY_MS (500);

}

void Calcul_Deb ()

{
//unsigned int temp;
			// Echelle d�bitm�tre 1000 � 1500Hz pour 0L/H � 1000 et 500L/H � 1500Hz
//					Tab_Pression[0]=1175;  //POUR TESTER A SUPPRIMER
				if (Tab_Pression[0]>=0x03E5 && Tab_Pression[0]<=0x03E8)  // 3Hz de tol�rance � voir !!
						{
					Tab_Pression[0]=0x03E8;   //1000
						}
					else if (Tab_Pression[0]<0x03E5)  //997
						{
					Tab_Pression[0]=0x270F;		//9999
					Pres_Debit = 0; //d�bitm�tre absent
						}
				
  				 if (Tab_Pression[0]==0x270F)  	//9999
						{
					debit_cable = 0x270F;  //pour indiquer que pas de r�ponse
					Pres_Debit = 0; //d�bitm�tre absent
						}
					else
						{
					debit_cable = Tab_Pression[0]-0x03E8;  // -1000
					Pres_Debit = 1; //flag pour d�bitm�tre pr�sent
					Tab_Pression[0]=debit_cable;
						}

}


//#####################################    MODE BCTA   ##############################################
void ecran_codage()

{
				unsigned char mem_cod_lec;			// ?????autorisation de sauvegarde (=1)
				int lect_en_cours =0;
				int prog_en_cours =0;
//				mem_cod_lec = 1;					//	ajout�		modif du 10/02/2016
 
//				niveau_batt();
 //   			Code_Prog = 1;

				SetPos(3,15);
  				PutMessage("CODAGE TP ADRESSABLES");		// Ecrire un message  nu_retour
//				hline(0,127,15) ;							// R�aliser une ligne horizontale

 				SetPos(20,48);								// Positionner le curseur
  				PutMessage("Codage");	
  				SetPos(60,48);								// Positionner le curseur
				PutMessage("Verification");				  		// Ecrire un message  
				plot(69,48);  // pour �
				plot(68,49);  // pour �
 // 			SetPos(70,50);								// Positionner le curseur
 //				PutMessage("Lecture");				  		// Ecrire un message  
//				FillWhiteZone(80,57,88,60);  
				mem_cod_lec = 1;
				flecheH(33,60);								// fl�che pour codage et m�mo � 1
				Display_Num_C_Prog();


				while (touche_BP5 != 0 )
		{
				niveau_batt();

	//############# MODE LECTURE ############mem_cod_lec = 0#
			if (!Verif_codageAT)  // si = 0 il n'y a pas de blocage pour enchainer la programmation + la v�rification
			{
				if(touche_BP2==0)
					{		 
					flecheH(83,60);								// fl�che pour lecture et m�mo � 0
				FillWhiteZone(33,57,41,60);
//				FillWhiteZone(20,24,100,44);   
					FillWhiteZone(0,40,127,47);
				mem_cod_lec = 0;
//				Display_Num_C_Lect();
					}
	//############# FIN MODE LECTURE #############
			}

	//############# MODE CODAGE #############mem_cod_lec = 1#			
				if(touche_BP1==0)
					{		
					flecheH(33,60);								// fl�che pour codage et m�mo � 1
				FillWhiteZone(83,57,91,60);
				FillWhiteZone(20,24,100,44); 
				mem_cod_lec = 1;
				Display_Num_C_Prog();
					}				
				DELAY_MS (10);
     //#############FIN MODE CODAGE#############

				if (mem_cod_lec==1)
				{
	//############# INCREMENTATION NUMERO #############
				if(touche_BP0==0 && Code_Prog<=15)
							{		 
							Code_Prog ++;
							Code_Lect=Code_Prog;
							eff=1;    // pour ne pas effacer sans arr�t
							}
//				while (touche_BP0 == 0);  // attente touche relach�e
	//#############FIN INCREMENTATION NUMERO #############

	//############# DECREMENTATION NUMERO #############
				if(touche_BP3==0 && Code_Prog>=2)		//  modif du 10/02/2016   
							{		 
							Code_Prog --;
							Code_Lect=Code_Prog;
							eff=1;    // pour ne pas effacer sans arr�t
							}
//				while (touche_BP3 == 0);  // attente touche relach�e
						if (eff==1)
							{
						Display_Num_C_Prog();
							}

				while (touche_BP0 == 0 || touche_BP3 == 0);  // attente touches relach�es
	//############# DECREMENTATION NUMERO #############



			//### FIN incr�mentation num�ro ###

				}
				if (mem_cod_lec==0 && lect_en_cours != 1)   
							{
// � voir							FillWhiteZone(40,24,100,36);   //(80,24,100,36)
							}

	//>>>>>>>>>>>>>>>>>>>	LECTURE CODE   <<<<<<<<<<<<<<<<<<<<<<<<
	
				if (touche_BP4 == 0 && mem_cod_lec==0 )
							{
/*					flecheH(83,60);								// fl�che pour lecture et m�mo � 0
					FillWhiteZone(33,57,41,60);
					FillWhiteZone(20,24,100,36);   
					mem_cod_lec = 0;   
 */
	
							lect_en_cours = 1;
 							FillWhiteZone(20,24,100,36);   //(80,24,100,36)
							DELAY_MS (100);
					FillWhiteZone(0,40,127,47);
					SetPos(40,40);								// Positionner le curseur
 					PutMessage("Patientez...");


// 							SetPos(38,30);								// Positionner le curseur
//							PutMessage("Transfert...");
						erreur_codage=0;  // reset flag erreur codage (voir dans codage.c)
							TSTcc();
							LectCodeTP();
////							DELAY_MS (1000);
						if(erreur_codage>0)
							{
						FillWhiteZone(0,40,127,47);
						SetPos(20,40);								// Positionner le curseur
	 					PutMessage("DEFAUT LECTURE!");
							DELAY_MS (1500);
						FillWhiteZone(0,40,127,47);
//							Led_verte=0;
							Led_rouge=0;
							}
						else
							{
					    	lect_en_cours=0;
							Display_Num_C_Lect();
	
////							SetPos(20,35);								
////  							PutMessage("Terminee  ");
//							Code_Prog = Code_Lect;
//							DELAY_MS (1000);
							Led_rouge=0;
							Led_verte=0;
							Led_jaune=0;
						erreur_codage=0;  // reset flag erreur codage (voir dans codage.c)
							}	
	
						}  // fin de 	if (touche_BP4 == 0 && mem_cod_lec==0 )

	//>>>>>>>>>>>>>>>>>>> FIN LECTURE CODE   <<<<<<<<<<<<<<<<<<<<<<<<


	//>>>>>>>>>>>>>>>>>>>  PROGRAMMATION CODE   <<<<<<<<<<<<<<<<<<<<<<<<

				if (touche_BP4 == 0 && mem_cod_lec==1)
						{

						prog_en_cours = 1;
						DELAY_MS (100);
					FillWhiteZone(0,40,127,47);
					SetPos(40,40);								// Positionner le curseur
// 					PutMessage("Patientez...          ");
 					PutMessage("Patientez...");
						ProgCodeTP();
						DELAY_MS (100);  //400

//+++++++++++++++++
					// modif pour pb passage en v�rif si prog capteur absent et code 1
					if (erreur_codage>0)   //  erreurs sur bcta==1)  //si = 0 la programmation s'est bien d�roul�e docn on peut v�rifier le code
						{
						Led_rouge=1;
						Buzzer_on();  // active le buzzer 75mSec
						Led_jaune=0;
						FillWhiteZone(0,40,127,47);
						SetPos(20,40);								// Positionner le curseur
	 					PutMessage("DEFAUT CODAGE!");
							DELAY_MS (1500);
						Led_rouge=0;
//						FillWhiteZone(0,40,127,47);
						}
					else
						{
					flecheH(83,60);								// fl�che Lecture pour lecture 
					FillWhiteZone(33,57,41,60);
					FillWhiteZone(20,24,100,36);   
						DELAY_MS (100);  //MODIF d�lai ajout� le 31-05-2015
						lect_en_cours = 1;  // en plus
							TSTcc();//MODIF ajout� le 31-05-2015
						erreur_codage=0;  // reset flag erreur codage (voir dans codage.c)
//						Code_Lect=0;
						LectCodeTP();
						lect_en_cours = 0;
						if (Code_Lect != Code_Prog)
							{
						Led_verte=0;
						Led_rouge=1;
						Buzzer_on();  // active le buzzer 75mSec
						Led_jaune=0;
						FillWhiteZone(0,40,127,47);
						SetPos(20,40);								// Positionner le curseur
	 					PutMessage("DEFAUT LECTURE!");
							DELAY_MS (1500);
						Led_rouge=0;
						FillWhiteZone(0,40,127,47);
							}
						else
							{
						Led_verte=1;
						Led_rouge=0;
						Led_jaune=0;
						erreur_codage=0;  // reset flag erreur codage (voir dans codage.c)
						Display_Num_C_Lect();
					FillWhiteZone(0,40,127,47);
						SetPos(40,40);								// Positionner le curseur
	 					PutMessage("CODAGE OK");
							DELAY_MS (1500);
						Led_verte=0;
							}

						DELAY_MS (2000);
//						flecheH(33,60);								// fl�che pour codage et m�mo � 1
//						FillWhiteZone(83,57,91,60);
						Led_rouge=0;
						Led_verte=0;
						Led_jaune=0;
						erreur_codage=0;  // reset flag erreur codage (voir dans codage.c)
//	ajout�	jusqu'� ligne 2971	modif du 10/02/2016
				flecheH(33,60);								// fl�che pour codage et m�mo � 1
				FillWhiteZone(83,57,91,60);
				FillWhiteZone(20,24,100,44); 
				mem_cod_lec = 1;
				Display_Num_C_Prog();
						}

					}    //+++++++++++

	//>>>>>>>>>>>>>>>>>>>FIN  PROGRAMMATION CODE   <<<<<<<<<<<<<<<<<<<<<<<<

	}

}    // FIN :  void ecran_codage()




void Display_Num_C_Prog()
{
						FillWhiteZone(20,24,100,46);  
							eff=0;
  	 				if	(Code_Prog < 10)
							{
 							displayHNum (60,28,Code_Prog);    //91,28 -> 60,28
							}
					else
							{
							displayHNum (51,28,Code_Prog);    //82,28 -> 51,28
							}

}

void Display_Num_C_Lect()
{
				FillWhiteZone(20,24,100,46);   
				if (!erreur_codage)  //si pas de d�faut
						{
					if(Code_Lect < 10 )
							{
						displayHNum (60,28,Code_Lect);     //91,28 -> 60,28
							}
					else
							{
						displayHNum (51,28,Code_Lect);   //82,28 -> 50,28
							}
   				    	}	
}



// FIN ECRANS DE MESURES




void ecran_param()
{
				niveau_batt();
				SetPos(10,10);
  				PutMessage("PARAMETRAGES");		// Ecrire un message  nu_retour
				hline(0,127,20) ;							// R�aliser une ligne horizontale
				SetPos(10,30);								// Positionner le curseur
				PutMessage   (" Configuration  ");				  		// Ecrire un message  
				SetPos(10,38);								// Positionner le curseur
// 				PutMessage   (" Reglage horloge");					// modif du 10/02/2016	
//				SetPos(10,46);								// // modif du 10/02/2016	
   				PutMessage   (" Config ADECEF  ");	
// 				SetPos(10,54);								// Positionner le curseur
// 				PutMessage   (" Libre    ");	
}



void menu_param_C0()
{
				SetPos(10,30);								// Positionner le curseur
				PutMessageInv(" Configuration  ");				  		// Ecrire un message  
				SetPos(10,38);								// Positionner le curseur
// 				PutMessage   (" Reglage horloge");					// // modif du 10/02/2016			
//				SetPos(10,46);								// // modif du 10/02/2016	
   				PutMessage   (" Config ADECEF  ");	
// 				SetPos(10,54);								// Positionner le curseur
// 				PutMessage   (" Libre    ");	
}	
				
void menu_param_C1()
{
				SetPos(10,30);								// Positionner le curseur
				PutMessage   (" Configuration  ");				  		// Ecrire un message  
				SetPos(10,38);								// 	
// 				PutMessageInv(" Reglage horloge");				// modif du 10/02/2016			
//				SetPos(10,46);								    // modif du 10/02/2016	
   				PutMessageInv(" Config ADECEF  ");				// modif du 10/02/2016	
// 				SetPos(10,54);								// Positionner le curseur
// 				PutMessage   (" Libre    ");	
}	




void ecran_Configuration()  
{
unsigned int pos_config;
unsigned int i;
				i=0;   // si une touche a �t� press�e = 1
				pos_config=0;
				niveau_batt();
				SetPos(0,0);
  				PutMessage("CONFIGURATION");		// Ecrire un message  nu_retour
				hline(0,127,10) ;							// R�aliser une ligne horizontale
				SetPos(0,16);
  				PutMessage(">"); 


/*	sont lus au d�but du main.c
				ReadEEPROM(0x01);  //return EEDATA
				val1TP=EEDATA;
				DELAY_MS (5);
				ReadEEPROM(0x02);  //return EEDATA
				boucleTPR=EEDATA;
				DELAY_MS (5);
				ReadEEPROM(0x03);  //return EEDATA
				boucleDEB=EEDATA;
				DELAY_MS (5);
*/
						SetPos(7,16);
		  				PutMessage("Arret scrutation 1 TP");
				plot(26,17);  // pour �
				plot(27,16);  // pour �
				plot(28,17);  // pour �
						SetPos(7,24);
		  				PutMessage("Boucle lecture TP Res");
						SetPos(7,32);
		  				PutMessage("Boucle lecture Debit ");
				plot(90,32);  // pour �
				plot(89,33);  // pour �
						SetPos(7,40);
		  				PutMessage("Conserve affichageTPA");
						SetPos(7,48);
		  				PutMessage("Validation buzzer    ");


					if (!val1TP)   // si variable ==0
						{
						coche_off(120,16);   // x  , y
						}							
					else 
						{
						coche_on(120,16);   // x  , y
						}
						
					if (!boucleTPR)
						{
						coche_off(120,24);   // x  , y
						}							
					else
						{
						coche_on(120,24);   // x  , y
						}

					if (!boucleDEB)
						{
						coche_off(120,32);   // x  , y
						}							
					else
						{
						coche_on(120,32);   // x  , y
						}

					if (!ConsAffTPA)
						{
						coche_off(120,40);   // x  , y
						}							
					else
						{
						coche_on(120,40);   // x  , y
						}
					if (!ValBuzzer)
						{
						coche_off(120,48);   // x  , y
						}							
					else
						{
						coche_on(120,48);   // x  , y
						}






			while (touche_BP5 )
				{
				DELAY_MS (30);

				if (touche_BP3==0 && (pos_config<4))
					{
				i=1;
				pos_config++;

					}

				if (touche_BP0==0 && (pos_config>0))
					{
				i=1;
				pos_config--;
					}
//				DELAY_MS (30);

			if (i)
				{
					switch (pos_config)
					{
			
				case 0:  //Stop lect apres 1 TP:
				SetPos(0,16);
  				PutMessage(">"); 
				SetPos(0,24);
  				PutMessage(" ");	  				 
				SetPos(0,32);
  				PutMessage(" ");	  				 
				SetPos(0,40);
  				PutMessage(" ");	  			 
				SetPos(0,48);
  				PutMessage(" ");	  			 
				break;
					
				case 1:	// Boucle Lecture TPR  :
				SetPos(0,16);
  				PutMessage(" ");	  				 
				SetPos(0,24);
  				PutMessage(">");	  				 
				SetPos(0,32);
  				PutMessage(" ");	  				 
				SetPos(0,40);
  				PutMessage(" ");	  			 
				SetPos(0,48);
  				PutMessage(" ");	  			 
				break;
					
				case 2:	// Boucle lecture Debit:
				SetPos(0,16);
  				PutMessage(" ");	  				 
				SetPos(0,24);
  				PutMessage(" ");	  				 
				SetPos(0,32);
  				PutMessage(">");	  			 
				SetPos(0,40);
  				PutMessage(" ");	  			 
				SetPos(0,48);
  				PutMessage(" ");	  			 
				break;

				case 3:	// Conserve affichage des TPA en quittant le modes TP adressables:
				SetPos(0,16);
  				PutMessage(" ");	  				 
				SetPos(0,24);
  				PutMessage(" ");	  				 
				SetPos(0,32);
  				PutMessage(" ");	  				 
				SetPos(0,40);
  				PutMessage(">");	  			 
				SetPos(0,48);
  				PutMessage(" ");	  			 
				break;

				case 4:	// Conserve affichage des TPA en quittant le modes TP adressables:
				SetPos(0,16);
  				PutMessage(" ");	  				 
				SetPos(0,24);
  				PutMessage(" ");	  				 
				SetPos(0,32);
  				PutMessage(" ");	  				 
				SetPos(0,40);
  				PutMessage(" ");	  			 
				SetPos(0,48);
  				PutMessage(">");	  			 
				break;
					}
				}
				i=0;
				while (touche_BP0==0 || touche_BP3==0); 

				 if (touche_BP4==0)
				{
				DELAY_MS (20);

					while (touche_BP4==0);  // attente touche relach�e


					switch (pos_config)
					{
				case 0:  //Stop lect apres 1 TP:
					if (!val1TP)   // si variable ==0
						{
						val1TP=1;
						coche_on(120,16);   // x  , y
						WriteEEPROM(0x01,0x01);    // m�morise 1 � l'adresse 0x01 de l'eeprom du PIC18
						}							
					else 
						{
						WriteEEPROM(0x01,0x00);    // m�morise 0 � l'adresse 0x01 de l'eeprom du PIC18
						coche_off(120,16);   // x  , y
						val1TP=0;
						}
				break;
				case 1:  // Boucle Lecture TPR  :
					if (!boucleTPR)
						{
						boucleTPR=1;
						coche_on(120,24);   // x  , y
						WriteEEPROM(0x02,0x01);    // m�morise 1 � l'adresse 0x02 de l'eeprom du PIC18
						}							
					else
						{
						WriteEEPROM(0x02,0x00);    // m�morise 0 � l'adresse 0x02 de l'eeprom du PIC18
						coche_off(120,24);   // x  , y
						boucleTPR=0;
						}
				break;
				case 2:  // Boucle Lecture D�bit  :
					if (!boucleDEB)
						{
						boucleDEB=1;
						coche_on(120,32);   // x  , y
						WriteEEPROM(0x03,0x01);    // m�morise 1 � l'adresse 0x03 de l'eeprom du PIC18
						}							
					else
						{
						WriteEEPROM(0x03,0x00);    // m�morise 0 � l'adresse 0x03 de l'eeprom du PIC18
						coche_off(120,32);   // x  , y
						boucleDEB=0;
						}
				break;

				case 3:  // Conserve affichage TPA  :
					if (!ConsAffTPA)
						{
						ConsAffTPA=1;
						coche_on(120,40);   // x  , y
						WriteEEPROM(0x04,0x01);    // m�morise 1 � l'adresse 0x04 de l'eeprom du PIC18
						}							
					else
						{
						WriteEEPROM(0x04,0x00);    // m�morise 0 � l'adresse 0x04 de l'eeprom du PIC18
						coche_off(120,40);   // x  , y
						ConsAffTPA=0;
						}
				break;
					
				case 4:  // Conserve affichage TPA  :
					if (!ValBuzzer)
						{
						ValBuzzer=1;
						coche_on(120,48);   // x  , y
						WriteEEPROM(0x05,0x01);    // m�morise 1 � l'adresse 0x05 de l'eeprom du PIC18
						}							
					else
						{
						WriteEEPROM(0x05,0x00);    // m�morise 0 � l'adresse 0x05 de l'eeprom du PIC18
						coche_off(120,48);   // x  , y
						ValBuzzer=0;
						}
					}	

				}

		}

}



void ecran_memoire() 
{
				niveau_batt();
				SetPos(10,10);
  				PutMessage("MEMOIRE");		// Ecrire un message  nu_retour
				hline(0,127,20) ;							// R�aliser une ligne horizontale
				SetPos(10,30);								// Positionner le curseur
				PutMessage   (" Lecture memoire");					// Ecrire un message  nu_retour			
				SetPos(10,38);								// Positionner le curseur
				PutMessage   (" Effacement Der. Enr. ");		  		// Ecrire un message  
				SetPos(10,46);								// Positionner le curseur
				PutMessage   (" Effacement Tous Enr. ");		  		// Ecrire un message  
}



void menu_memoire_C0()
{
				SetPos(10,30);								// Positionner le curseur
				PutMessageInv(" Lecture memoire");					// Ecrire un message  nu_retour			
				SetPos(10,38);								// Positionner le curseur
				PutMessage   (" Effacement Der. Enr. ");		  		// Ecrire un message  
				SetPos(10,46);								// Positionner le curseur
				PutMessage   (" Effacement Tous Enr. ");		  		// Ecrire un message  
}	
				
void menu_memoire_C1()
{
				SetPos(10,30);								// Positionner le curseur
				PutMessage   (" Lecture memoire");					// Ecrire un message  nu_retour			
				SetPos(10,38);								// Positionner le curseur
				PutMessageInv(" Effacement Der. Enr.");		  		// Ecrire un message  
				SetPos(10,46);								// Positionner le curseur
				PutMessage   (" Effacement Tous Enr.");		  		// Ecrire un message  
}	

void menu_memoire_C2()
{
				SetPos(10,30);								// Positionner le curseur
				PutMessage   (" Lecture memoire");					// Ecrire un message  nu_retour			
				SetPos(10,38);								// Positionner le curseur
				PutMessage   (" Effacement Der. Enr.");		  		// Ecrire un message  
				SetPos(10,46);								// Positionner le curseur
				PutMessageInv(" Effacement Tous Enr.");		  		// Ecrire un message  
}	



void ecran_lect_mem(void)
{


				ClearScreen ();
				niveau_batt();

					
//				while (!touche_BP6); //attente touche relach�e
				gestion_lecture_memoire();

//				while (!touche_BP5); //attente touche relach�e


}







void ecran_Raz_Memoire(unsigned char F_der_tous)
{
unsigned char mem;
unsigned char cpt;
unsigned char raz;

				raz=0;
				mem=1;		//pointe OUI
				niveau_batt();
				SetPos(0,10);
				if (F_der_tous){
  				PutMessage("RAZ Tous Enregistrements");		// Ecrire un message  nu_retour
				}
				else	{	
 				PutMessage("RAZ Der. Enregistrement");		// Ecrire un message  nu_retour
						}		
				hline(0,127,20) ;							// R�aliser une ligne horizontale
				SetPos(10,24);
  				PutMessage("Confirmez l'effacement");		// Ecrire un message  nu_retour

				SetPos(35,50);
  				PutMessage("OUI");		// Ecrire OUI
				SetPos(75,50);
  				PutMessage("NON");		// Ecrire NON
				flecheH(40,60);								


			while(touche_BP6==0); 
				DELAY_MS (30);


		while (touche_BP6!=0 && raz==0)
			{
				if(touche_BP2==0)
						{		 
					flecheH(80,60);								// fl�che pour NON et m�mo � 0
				FillWhiteZone(40,57,48,60);
				mem = 0;
					  	}			
				if(touche_BP1==0){		
					flecheH(40,60);								// fl�che pour OUI et m�mo � 1
				FillWhiteZone(80,57,88,60);
				mem = 1;
							  	}
//					}				
				while (touche_BP6==0);
				DELAY_MS (30);

				if (touche_BP6==0 && mem==1)		
					{
				if (F_der_tous){	//tous les enreg
//??				Der_Enreg = Offset_Enr;

				No_der_enreg =0;
				Der_Enreg=0x000;  //-=
				Wr_eeprom_int (ad_Der_Enreg,Der_Enreg);  // m�morise en eeprom du pic la derniere adresse stock�e en eeprom_i2c
				Wr_eeprom_int (ad_No_der_enreg,No_der_enreg);  // m�morise en eeprom du pic le num�ro de l'enregistrement stock� en eeprom_i2c
				}	
				else 
			{				//Der enreg

				No_der_enreg = Rd_eeprom_int (ad_No_der_enreg);  //num�ro
				Der_Enreg = Rd_eeprom_int (No_der_enreg);   // valeur
				if ((No_der_enreg)>1){   //!error
				No_der_enreg -=1;
				Der_Enreg-=0x180;
				}
				else {
				Der_Enreg=0x000;
				No_der_enreg=0;
				}	
				Wr_eeprom_int (ad_Der_Enreg,Der_Enreg);  // m�morise en eeprom du pic la derniere adresse stock�e en eeprom_i2c
				Wr_eeprom_int (ad_No_der_enreg,No_der_enreg);  // m�morise en eeprom du pic le num�ro de l'enregistrement stock� en eeprom_i2c
			}

				ClearScreen ();	
				SetPos(30,24);
		 		PutMessage("RAZ effectuee    ");
				plot(90,24);  // pour �
				plot(89,25);  // pour �
				raz=1;		   
		

			}  // fin de:  if (touche_BP6==0 && mem==1)		


 
				else if (touche_BP6==0 && mem==0)
					{						
				ClearScreen ();	
 				SetPos(30,24);
 		 		PutMessage("RAZ annulee     ");	 
				plot(82,24);  // pour �
				plot(81,25);  // pour �
					}
	
		}	// fin de:    while (touche_BP6!=0)
			

				cpt=0;
				while (touche_BP5 && cpt<=5 ){	//RETOUR par la touche BP5 
				DELAY_MS (250);
				cpt++;
				}
}







// FIN  MENUS




void niveau_batt()   // symbole de la batterie avec curseur de remplissage
{
				char niv_batt;
				box(109,0,124,4);							// rectangle batterie
				box(124,1,125,3);							// rectangle batterie
				Mesure_V_Batt();
				V_Batt =(int)(V_Batt_Calc*10);				// 
				V_Batt_Aff = V_Batt_Calc;					// pour. m�moriser et afficher la valeur

				if (V_Batt >= 67){
				V_Batt-=67;}	// min 6.7v x10 = 67  82-67 = 15 				
				else{V_Batt=0;}		


				if (V_Batt > 15){V_Batt=15;}
													// 123-15=110
				niv_batt = V_Batt + 109;	
				FillBlackZone (109,1,niv_batt,3);		// zone remplie = 15 donc >=8.2V
				niv_batt = V_Batt + 110;	
				FillWhiteZone (niv_batt,1,123,3);    	// zone vide = 0 donc <=6.7V

}	





void reset_leds()

{
				Led_rouge =1;
				DELAY_MS (15);
				Led_verte =1;
				DELAY_MS (15);
				Led_jaune =1;
				DELAY_MS (15);
//				Back_light=1;
				DELAY_MS (100);
				Led_rouge =0;
				Led_verte =0;
				Led_jaune =0;

}



void flip_flop_back_light()
{
				if (touche_BP1==0 && touche_BP2==0 && F_memo_back_light == 1)
				{
				Back_light = 0;
				F_memo_back_light = 0;				
				while (touche_BP1==0 || touche_BP2==0);
				}
				if (touche_BP1==0 && touche_BP2==0 && F_memo_back_light == 0)
				{
				Back_light = 1;
				F_memo_back_light = 1;			
				while (touche_BP1==0 || touche_BP2==0);
				}
}



void Buzzer_on(void)
{
				if (ValBuzzer)  
					{
				Buzzer = 1;
 				DELAY_MS (75);
				Buzzer = 0;
					}
}




void ecran_adecef()
{
char i;
char j;
char m;
char n;
int pos;
char num;
char Code[4];
char test_code;
//unsigned int CodeConfig;

    			i=49;  //EN ASCII
				j=50;
				m=51;
				n=52;
				pos=72;
				num=0;
				test_code=0;
				SetPos(10,32);
  				PutMessage("Mot de passe:  . . . . ");		// Ecrire un message  nu_retour
				DELAY_MS (10);

				while (num<4 && touche_BP5 && test_code!=4)  // codage avec 4 chiffres
					{



			if (touche_BP6&&touche_BP4) // si touches pas press�e (= 1)
				{
					if (!touche_BP0) // si touche BP6=1 + touche BP0=0 donne 0
						{
						DELAY_MS (30);
						SetPos(pos+=6,32);
						PutChar(48);   //0
						Code[num]=0;
						num++;
						DELAY_MS (30);
						while (!touche_BP0);
						}		
					if (!touche_BP1) // si touche BP6=1 + touche BP1=0 donne 1
						{
						DELAY_MS (30);
						SetPos(pos+=6,32);
						PutChar(49);
						Code[num]= 1;  //1
						num++;
						DELAY_MS (30);
						while (!touche_BP1);
						}		
					if (!touche_BP2)  // si touche BP6=1 + touche BP2=0 donne 2
						{
						DELAY_MS (30);
						SetPos(pos+=6,32);
						PutChar(50);
						Code[num]= 2 ;  //2
						num++;
						DELAY_MS (30);
						while (!touche_BP2);
						}	
					if (!touche_BP3)  // si touche BP6=1 + touche BP3=0 donne 3
						{
						DELAY_MS (30);
						SetPos(pos+=6,32);
						PutChar(51);
						Code[num]=3;	//3
						num++;
						DELAY_MS (30);
						while (!touche_BP3);
						}	
				}


			if (!touche_BP6&&touche_BP4)   // si touche press�e (= 0)
				{
					if (!touche_BP0) // si touche BP6=0 + touche BP0=0 donne 4
						{
						DELAY_MS (30);
						SetPos(pos+=6,32);
						PutChar(52);   //4
						Code[num]=4;
						num++;
						DELAY_MS (30);
						while (!touche_BP0);
						}		
					if (!touche_BP1) // si touche BP6=0 + touche BP1=0 donne 5
						{
						DELAY_MS (30);
						SetPos(pos+=6,32);
						PutChar(53);
						Code[num]= 5;  //5
						num++;
						DELAY_MS (30);
						while (!touche_BP1);
						}		
					if (!touche_BP2)  // si touche BP6=0 + touche BP2=0 donne 6
						{
						DELAY_MS (30);
						SetPos(pos+=6,32);
						PutChar(54);
						Code[num]= 6 ;  //6
						num++;
						DELAY_MS (30);
						while (!touche_BP2);
						}	
					if (!touche_BP3)  // si touche BP6=0 + touche BP3=0 donne 7
						{
						DELAY_MS (30);
						SetPos(pos+=6,32);
						PutChar(55);
						Code[num]=7;	//7
						num++;
						DELAY_MS (30);
						while (!touche_BP3);
						}	
				}


			if (!touche_BP4&&touche_BP6)   // si touche BP5 press�e (= 0)
				{
					if (!touche_BP0) // si touche BP4=0 + touche BP0=0 donne 8
						{
						DELAY_MS (30);
						SetPos(pos+=6,32);
						PutChar(56);   //8
						Code[num]=8;
						num++;
						DELAY_MS (30);
						while (!touche_BP0);
						}		
					if (!touche_BP1) // si touche BP4=0 + touche BP1=0 donne 9
						{
						DELAY_MS (30);
						SetPos(pos+=6,32);
						PutChar(57);
						Code[num]= 9;  //9
						num++;
						DELAY_MS (30);
						while (!touche_BP1);
						}		
				}	





						DELAY_MS (50);
					}
// v�rification code
				
				for(num=0;num<4;num++)
				{
				if(Code[num]==Code_Param[num])
					{
				test_code+=1;
					}
				else{
				 test_code=0;				
					}
				}				
				DELAY_MS (400);

//		if (touche_BP5 && test_code==6)	// 	 

				if (test_code==4)	// 	 
					{
				config_adecef=1; //valide autorisation config adecef  
					}
				else
					{
				config_adecef=0; //valide autorisation config adecef  
				ClearScreen();								// Effacement de tout l'�cran
				SetPos(10,34);
  				PutMessage("Mot de passe MAUVAIS");		// Ecrire un message  nu_retour
				DELAY_MS (1000);
					}
				

//				DELAY_MS (30);
				while (!touche_BP5);  
				
}




void Mot_de_passe()
{
char i;
char j;
char m;
char n;
int pos;
char num;
char Code[4];
char test_code;
char adr_mot_de_passe;
char ok_modif;
//unsigned int CodeConfig;

				adr_mot_de_passe=0x24;
				ok_modif=0;
    			i=49;  //EN ASCII
				j=50;
				m=51;
				n=52;
				pos=65;
				num=0;
				test_code=0;
				ClearScreen();		
				niveau_batt();
				SetPos(0,0);
  				PutMessage("CONFIG ADECEF ");		// Ecrire un message  nu_retour
				SetPos(0,16);
  				PutMessage("Modif du mot de passe"); 
				SetPos(0,32);
  				PutMessage("Nouveau code:  . . . . ");		// Ecrire un message  nu_retour


		while (num<4 && touche_BP5 && test_code!=4)  // codage avec 4 chiffres
			{



			if (touche_BP6&&touche_BP4) // si touches pas press�e (= 1)
				{
					if (!touche_BP0) // si touche BP6=1 + touche BP0=0 donne 0
						{
						DELAY_MS (30);
						SetPos(pos+=6,32);
						PutChar(48);   //0
						Code[num]=0;
						num++;
						DELAY_MS (30);
						while (!touche_BP0);
						}		
					if (!touche_BP1) // si touche BP6=1 + touche BP1=0 donne 1
						{
						DELAY_MS (30);
						SetPos(pos+=6,32);
						PutChar(49);
						Code[num]= 1;  //1
						num++;
						DELAY_MS (30);
						while (!touche_BP1);
						}		
					if (!touche_BP2)  // si touche BP6=1 + touche BP2=0 donne 2
						{
						DELAY_MS (30);
						SetPos(pos+=6,32);
						PutChar(50);
						Code[num]= 2 ;  //2
						num++;
						DELAY_MS (30);
						while (!touche_BP2);
						}	
					if (!touche_BP3)  // si touche BP6=1 + touche BP3=0 donne 3
						{
						DELAY_MS (30);
						SetPos(pos+=6,32);
						PutChar(51);
						Code[num]=3;	//3
						num++;
						DELAY_MS (30);
						while (!touche_BP3);
						}	
				}


			if (!touche_BP6&&touche_BP4)   // si touche press�e (= 0)
				{
					if (!touche_BP0) // si touche BP6=0 + touche BP0=0 donne 4
						{
						DELAY_MS (30);
						SetPos(pos+=6,32);
						PutChar(52);   //4
						Code[num]=4;
						num++;
						DELAY_MS (30);
						while (!touche_BP0);
						}		
					if (!touche_BP1) // si touche BP6=0 + touche BP1=0 donne 5
						{
						DELAY_MS (30);
						SetPos(pos+=6,32);
						PutChar(53);
						Code[num]= 5;  //5
						num++;
						DELAY_MS (30);
						while (!touche_BP1);
						}		
					if (!touche_BP2)  // si touche BP6=0 + touche BP2=0 donne 6
						{
						DELAY_MS (30);
						SetPos(pos+=6,32);
						PutChar(54);
						Code[num]= 6 ;  //6
						num++;
						DELAY_MS (30);
						while (!touche_BP2);
						}	
					if (!touche_BP3)  // si touche BP6=0 + touche BP3=0 donne 7
						{
						DELAY_MS (30);
						SetPos(pos+=6,32);
						PutChar(55);
						Code[num]=7;	//7
						num++;
						DELAY_MS (30);
						while (!touche_BP3);
						}	
				}


			if (!touche_BP4&&touche_BP6)   // si touche BP5 press�e (= 0)
				{
					if (!touche_BP0) // si touche BP4=0 + touche BP0=0 donne 8
						{
						DELAY_MS (30);
						SetPos(pos+=6,32);
						PutChar(56);   //8
						Code[num]=8;
						num++;
						DELAY_MS (30);
						while (!touche_BP0);
						}		
					if (!touche_BP1) // si touche BP4=0 + touche BP1=0 donne 9
						{
						DELAY_MS (30);
						SetPos(pos+=6,32);
						PutChar(57);
						Code[num]= 9;  //9
						num++;
						DELAY_MS (30);
						while (!touche_BP1);
						}		
				}	
						DELAY_MS (50);	
 
						ok_modif=1;
		}  // FIN de: while (num<4 && touche_BP5 && test_code!=4)  // codage avec 4 chiffres


						adr_mot_de_passe=0x24;
						for(num=0;num<=4;num++)
						{						
						WriteEEPROM(adr_mot_de_passe,Code[num]); 
						adr_mot_de_passe+=1;
						}
	
						SetPos(10,56);
		  				PutMessage("Mot de passe OK");		// Ecrire un message  nu_retour
						DELAY_MS (750);
} 









 
void ecran_configAT(void)  
{
unsigned int pos_configAT;
unsigned int i;
				i=0;   // si une touche a �t� press�e = 1
				ClearScreen();								// Effacement de tout l'�cran
				pos_configAT=0;
				niveau_batt();
				SetPos(0,0);
  				PutMessage("CONFIG ADECEF ");		// Ecrire un message  nu_retour
				hline(0,127,10) ;							// R�aliser une ligne horizontale
				SetPos(0,16);
  				PutMessage(">"); 


				SetPos(7,16);
  				PutMessage("Reglage Horloge");
				plot(16,16);  // pour �
				plot(15,17);  // pour �
				SetPos(7,24);
  				PutMessage("Bloquer infos central");
//				plot(52,24);  // pour �
//				plot(51,25);  // pour �
				SetPos(7,32);
  				PutMessage("Bloquer verif codage");
				plot(58,32);  // pour �
				plot(57,33);  // pour �
				SetPos(7,40);
  				PutMessage("Bloquer affiche Debit");
				SetPos(7,48);
  				PutMessage("Ajustement gains");
				SetPos(7,56);
  				PutMessage("Modif mot de passe");



					if (coche_constit==0)
						{
						coche_off(120,24);   // x  , y
						}							
					else
						{
						coche_on(120,24);   // x  , y
						}
		
						
					if (Verif_codageAT==0) 
						{
						coche_off(120,32);   // x  , y
						}							
					else
						{
						coche_on(120,32);   // x  , y
						}		

					if (Aff_Debit_tpa==0) 
						{
						coche_off(120,40);   // x  , y
						}							
					else
						{
						coche_on(120,40);   // x  , y
						}		




				while (touche_BP5 )
				{
				DELAY_MS (30);

				if (touche_BP3==0 && (pos_configAT<5))
					{
				i=1;
				pos_configAT++;

					}

				if (touche_BP0==0 && (pos_configAT>0))
					{
				i=1;
				pos_configAT--;
					}
//				DELAY_MS (30);

			if (i)
				{
					switch (pos_configAT)
					{
			
				case 0:  //R�glage horloge
				SetPos(0,16);
  				PutMessage(">"); 
				SetPos(0,24);
  				PutMessage(" ");	  				 
				SetPos(0,32);
  				PutMessage(" ");	  				 
				SetPos(0,40);
  				PutMessage(" ");	  			 
				SetPos(0,48);
  				PutMessage(" ");	  			 
				SetPos(0,56);
  				PutMessage(" ");	  			 
				break;
					
				case 1:	//Bloque infos central
				SetPos(0,16);
  				PutMessage(" "); 
				SetPos(0,24);
  				PutMessage(">");	  				 
				SetPos(0,32);
  				PutMessage(" ");	  				 
				SetPos(0,40);
  				PutMessage(" ");	  			 
				SetPos(0,48);
  				PutMessage(" ");	  			 
				SetPos(0,56);
  				PutMessage(" ");	  			 
				break;
					
				case 2:	// Bloque v�rif codage
				SetPos(0,16);
  				PutMessage(" "); 
				SetPos(0,24);
  				PutMessage(" ");	  				 
				SetPos(0,32);
  				PutMessage(">");	  				 
				SetPos(0,40);
  				PutMessage(" ");	  			 
				SetPos(0,48);
  				PutMessage(" ");	  			 
				SetPos(0,56);
  				PutMessage(" ");	  			 
				break;

				case 3:	// Bloque Affichage d�bit dans �crans TPA
				SetPos(0,16);
  				PutMessage(" "); 
				SetPos(0,24);
  				PutMessage(" ");	  				 
				SetPos(0,32);
  				PutMessage(" ");	  				 
				SetPos(0,40);
  				PutMessage(">");	  			 
				SetPos(0,48);
  				PutMessage(" ");	  			 
				SetPos(0,56);
  				PutMessage(" ");	  			 
				break;

				case 4:	// AJUSTEMENT DES GAINS DE CORRECTION I_Conso I_Repos I_Modul
				SetPos(0,16);
  				PutMessage(" "); 
				SetPos(0,24);
  				PutMessage(" ");	  				 
				SetPos(0,32);
  				PutMessage(" ");	  				 
				SetPos(0,40);
  				PutMessage(" ");	  			 
				SetPos(0,48);
  				PutMessage(">");	  			 
				SetPos(0,56);
  				PutMessage(" ");	  			 
				break;
				case 5:	// Modif Mot de passe
				SetPos(0,16);
  				PutMessage(" "); 
				SetPos(0,24);
  				PutMessage(" ");	  				 
				SetPos(0,32);
  				PutMessage(" ");	  				 
				SetPos(0,40);
  				PutMessage(" ");	  			 
				SetPos(0,48);
  				PutMessage(" ");	  			 
				SetPos(0,56);
  				PutMessage(">");	  			 
				break;
					}
				}
				i=0;
				while (touche_BP0==0 || touche_BP3==0); 

				 if (touche_BP4==0 || touche_BP6==0)
				{
				DELAY_MS (20);

					while (touche_BP4==0||touche_BP6==0);  // attente touche relach�e


					switch (pos_configAT)
					{
				case 0:  //Stop lect apres 1 TP:
					Set_Horloge(); // pour programmer l'horloge 
				break;
				case 1:  // bloque constitution  :
					if (!coche_constit)
					{
					coche_on(120,24);   // x  , y
					coche_constit=1;
					WriteEEPROM(0x00,0x01);    // m�morise 1 � l'adresse 0x00 de l'eeprom du PIC18
					}							
					else
					{
					coche_off(120,24);   // x  , y
					coche_constit=0;
					WriteEEPROM(0x00,0x00);    // m�morise 0 � l'adresse 0x00 de l'eeprom du PIC18
					}		
				break;
				case 2:  // Bloque v�rification du codage dans l"�cran codage TPA
					if (!Verif_codageAT)
					{
					coche_on(120,32);   // x  , y
					Verif_codageAT=1;
					WriteEEPROM(0x06,0x01);    // m�morise 1 � l'adresse 0x06 de l'eeprom du PIC18
					}							
					else
					{
					coche_off(120,32);   // x  , y
					Verif_codageAT=0;
					WriteEEPROM(0x06,0x00);    // m�morise 0 � l'adresse 0x06 de l'eeprom du PIC18
					}
				break;

				case 3:  //Aff_Debit_tpa pour bloquer l'affichage du d�bit dans les �crans TPA
					if (!Aff_Debit_tpa)  // Si � 1 bloque l'affichage D�bit
					{
					coche_on(120,40);   // x  , y
					Aff_Debit_tpa=1;
					WriteEEPROM(0x07,0x01);    // m�morise 1 � l'adresse 0x07 de l'eeprom du PIC18
					}							
					else
					{
					coche_off(120,40);   // x  , y
					Aff_Debit_tpa=0;
					WriteEEPROM(0x07,0x00);    // m�morise 0 � l'adresse 0x07 de l'eeprom du PIC18
					}
				break;
					
				case 4: // AJUSTEMENT DES GAINS DE CORRECTION I_Conso I_Repos I_Modul 
					Ajuste_Gains();

				break;
	
				case 5: //LIBRE
					Mot_de_passe();
				break;
					}	

				}

		}

}


void Ajuste_Gains()
{
char Pos_Conf_Gain;
char ii;
int  i;
unsigned char jj;  // stocke la valeur du coeff s�lectionn� pour modif
float tpFL;	// pour calcul et affichage en float ex:1.89 au lieu de 189
char bcl;
char memo; // si une seule modif memo = 1 pour sauvegarder
unsigned int x;
unsigned int y;
				x=80;
				memo=0;
				bcl=0;
				ClearScreen();		
				niveau_batt();
				Pos_Conf_Gain=0;
				ii = 0;  //=1 si changement de ligne
				SetPos(0,0);
  				PutMessage("CONFIG ADECEF ");		// Ecrire un message  nu_retour
				SetPos(0,8);
  				PutMessage("Ajustement des Gains"); 

				SetPos(10,24);
  				PutMessage("Modulation"); 
				SetPos(0,24);
  				PutMessage(">"); 
				SetPos(10,32);
  				PutMessage("Consommation"); 
				SetPos(10,40);
  				PutMessage("Repos"); 
				SetPos(10,48);
  				PutMessage("Resistifs"); 





		while (touche_BP5)
				{

				if (touche_BP6==1&&!touche_BP3 && Pos_Conf_Gain<4)
					{
				ii=1;
				Pos_Conf_Gain+=1;
				DELAY_MS (30);	//50
//				while (!touche_BP3);
					}

				if (touche_BP6==1&&!touche_BP0 && Pos_Conf_Gain>0)
					{
				ii=1;
				Pos_Conf_Gain-=1;
				DELAY_MS (30);	//50
//				while (!touche_BP0);
					}


			if (ii)
				{
				switch (Pos_Conf_Gain)
					{
			
				case 0:  //R�glage gain modulation
				SetPos(0,24);
  				PutMessage(">"); 
				SetPos(0,32);
  				PutMessage(" ");	  				 
				SetPos(0,40);
  				PutMessage(" ");
				SetPos(0,48);
  				PutMessage(" ");	  				 
				jj=Coeff_I_Modul;
				break;

				case 1:  //R�glage gain consommation
				SetPos(0,24);
  				PutMessage(" "); 
				SetPos(0,32);
  				PutMessage(">");	  				 
				SetPos(0,40);
  				PutMessage(" ");	  				 
				SetPos(0,48);
  				PutMessage(" ");	  				 
				jj=Coeff_I_Conso;
				break;
				case 2:  //R�glage gain repos
				SetPos(0,24);
  				PutMessage(" "); 
				SetPos(0,32);
  				PutMessage(" ");	  				 
				SetPos(0,40);
  				PutMessage(">");	  				 
				SetPos(0,48);
  				PutMessage(" ");	  				 
				jj=Coeff_I_Repos;
				break;
				case 3:  //R�glage gain r�sistifs
				SetPos(0,24);
  				PutMessage(" "); 
				SetPos(0,32);
  				PutMessage(" ");	  				 
				SetPos(0,40);
  				PutMessage(" ");	  				 
				SetPos(0,48);
  				PutMessage(">");	  				 
				jj=Coeff_V_TPR;
				break;
					}
				}
//				while (!touche_BP0&&!touche_BP3);
				ii=0;


		if (bcl==0)
		{
// Affiche les valeurs apr�s lecture eeprom du PIC dans la lecture des parametres => void LECTURE_PARAM(void) 
			for(i=0;i<4;i++)
			{
				switch (i){
				case 0:
				jj=Coeff_I_Modul;
				y=24;
				break;

				case 1:
				jj=Coeff_I_Conso;
				y=32;
				break;

				case 2:
				jj=Coeff_I_Repos;
				y=40;
				break;

				case 3:
				jj=Coeff_V_TPR;
				y=48;
				break;
						}		

				if(jj>=100)	{
				displaySmallNum(x,y,jj);	}
				else {
				SetPos(x,y);
				PutSmallChar(48);  //0
				displaySmallNum(x+6,y,jj);}		
				plot(x+5,y+6);				
				plot(x+4,y+7);				

			
			} // fin de while (i=0;i<3;i++)	

				bcl=1;
		}	// fin de if (bcl==0)	

			

      while(touche_BP6==0&&touche_BP0==0 && touche_BP4!=0 && (jj<200))	
		{ 	
 				memo=1; // si memo==1 donc une modif => on m�morise avant de quitter
   				switch (Pos_Conf_Gain)
					{			
				case 0:  //R�glage gain I_Modulation
				jj=Coeff_I_Modul+=1;
				y=24;
				break;
				case 1:  //R�glage gain I_Consommation
				jj=Coeff_I_Conso+=1;
				y=32;
				break;
				case 2:  //R�glage gain I_Repos
				jj=Coeff_I_Repos+=1;
				y=40;
				break;
				case 3:  //R�glage gain V_1K pour TPR (coeffx100) exemple 153 => coeff 1.53
				jj=Coeff_V_TPR+=1;
				y=48;
				break;
					}

				if(jj>=100)	{
				displaySmallNum(x,y,jj);	}
				else {
				SetPos(x,y);
				PutSmallChar(48);  //0
				displaySmallNum(x+6,y,jj);}		
				plot(x+5,y+6);				
				plot(x+4,y+7);				

				DELAY_MS (200);


		} // fin de  if(touche_BP6==0&&touche_BP0==0 && touche_BP4!=0 && (jj<200))	
			

       while(touche_BP6==0&&touche_BP3 ==0  && touche_BP4!=0 && (jj>80))	
		 { 							
 				memo=1; // si memo==1 donc une modif => on m�morise avant de quitter
				switch (Pos_Conf_Gain)
					{			
				case 0:  //R�glage gain I_Modulation
				jj=Coeff_I_Modul-=1;
				y=24;
				break;
				case 1:  //R�glage gain I_Consommation
				jj=Coeff_I_Conso-=1;
				y=32;
				break;
				case 2:  //R�glage gain I_Repos
				jj=Coeff_I_Repos-=1;
				y=40;
				break;
				case 3:  //R�glage gain V_1K pour TPR (coeffx100) exemple 153 => coeff 1.53
				jj=Coeff_V_TPR-=1;
				y=48;
				break;
					}

				if(jj>=100)
				{
				displaySmallNum(x,y,jj);
				}
				else {					
				SetPos(x,y);
				PutSmallChar(48);  //0
				displaySmallNum(x+6,y,jj);
				}		
				plot(x+5,y+6);				
				plot(x+4,y+7);				


				DELAY_MS (200);	

			}  // FIN DE while(touche_BP6==0&&touche_BP3 ==0  && touche_BP4!=0 && (jj>80))




			 
				if(memo) // si memo � 1, il y a eu une modif
				{
				SetPos(15,56);
  				PutMessage("MEMORISATION ? ");  
				}

				//#########################

		if (touche_BP4==0)  // pour programmer en EEPROM
			{
				  //gain modulation
				WriteEEPROM(0x20,Coeff_I_Modul); // Si valeur � 0 on met par d�faut le coeff 1 donc 100   O est impossible
		
				  //gain consommation
				WriteEEPROM(0x21,Coeff_I_Conso); // Si valeur � 0 on met par d�faut le coeff 1 donc 100   O est impossible

				  //R�glage gain repos
				WriteEEPROM(0x22,Coeff_I_Repos); // Si valeur � 0 on met par d�faut le coeff 1 donc 100   O est impossible

				  //R�glage gain V_1K pour TPR (coeffx100) exemple 153 => coeff 1.53
				WriteEEPROM(0x23,Coeff_V_TPR); // Si valeur � 0 on met par d�faut le coeff 1 donc 100   O est impossible

				SetPos(15,56);
  				PutMessage("MEMORISATION OK ");  
    			memo=0;
				DELAY_MS (750);
				SetPos(15,56);
  				PutMessage("                     ");  
				while (touche_BP4==0);  
			}


			DELAY_MS (250);




		}// Fin de while (touche_BP5)

}







			
  
