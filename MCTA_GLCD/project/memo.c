#include <delay.h>						/* Defs DELAY functions */
#include <GLCD.h>
#include <i2c.h>
#include <ftoa.h>						/* Adapt Chaine de caract�re */
#include <GLCD.h>						/* Gestion GLCD */
#include <memo.h>
#include <eeprom.h>




extern unsigned int adr_eeprom;
extern unsigned data_char;
extern unsigned int val_eeprom_i2c;

extern unsigned char Num_Ligne;		//num�ro ligne en cours

unsigned char 	Tab2_Horodatage[10];	// pour test � supprimer !!!!!
unsigned char 	Tab2_VoieRobTete[10];	// pour test � supprimer !!!!!
unsigned char 	Tab2_Central[10];		// pour test � supprimer !!!!!

extern unsigned int 	Tab_No_Enr[1];			//2 enregistre le num�ro d'enregistrement sur 2 octets
extern unsigned char 	Tab_Type_Enr[1];  		//1 octets  pour m�moriser le type d'enregistrement (TPA, TPR, D�bitm�tre, Groupe, Icra)
extern unsigned char 	Tab_Horodatage[10];		//10 octets	 //10 caract�res	  
extern unsigned char 	Tab_Constit[17][10];	//170 octets //constitution 10 caract�res
extern unsigned char 	Tab_VoieRobTete[10];	//10 octets 	VOIE 3 + ROB 2 + TETE 5	 MAX caract�res
extern unsigned char 	Tab_Central[14];		//14 octets	 //14 caract�res	  
extern unsigned int 	Tab_Pression[17];		//34 octets   en 0 valeur du d�bit  si ffff => pas de d�bitm�tre
extern unsigned int 	Tab_I_Repos[17];		//34 octets 17 caract�res	  1 d�bitmetre et 16 TP
extern unsigned char 	Tab_I_Modul[17];		//17 octets	17 caract�res	  1 d�bitmetre et 16 TP
extern unsigned int 	Tab_I_Conso[17];		//34 octets	17 caract�res	  1 d�bitmetre et 16 TP
extern unsigned char 	Tab_Etat[17];			//17 octets  
							  // total:  340+1+2 = 343 octets



#pragma romdata eeprom_data=0xF00000
rom const int 		Tab_No_Enr_eep[1];					//2 enregistre le num�ro d'enregistrement sur 2 octets
rom const char 		Tab_Type_Enr_eep[1];  				//1 octets  pour m�moriser le type d'enregistrement (TPA, TPR, D�bitm�tre, Groupe, Icra)
rom const char		Tab_Horodatage_eep[10];			//10 octets	 //horodatage
rom const char 		Tab_Constit_eep[17][10];		//224 octets //constitution 
rom const char	 	Tab_VoieRobTete_eep[10];		//10 octets 	VOIE 3 + ROB 2 + TETE 5	 (ROB 2 + TETE 5 devient Comment)
rom const char 		Tab_Central_eep[14];			//14 octets	 //central 
rom const  int 		Tab_Pression_eep[17];			//34 octets   en 0 valeur du d�bit  si ffff => pas de d�bitm�tre
rom const  int 		Tab_I_Repos_eep[17];			//34 octets   
rom const  char 	Tab_I_Modul_eep[17];			//17 octets
rom const  int	 	Tab_I_Conso_eep[17];			//17 octets
rom const  char 	Tab_Etat_eep[17];				//17 octets   total:   octets
#pragma romdata		/* Resume allocation of romdata into the default section */






unsigned int Offset_Enr; // adresse i2c de l'enregistrement en lecture

extern unsigned int ad_Der_Enreg; // adresse du dernier enregistrement en EEprom du pic
extern unsigned int Der_Enreg; // valeur de l'adresse m�moire du dernier enregistrement en EEprom du pic

extern unsigned int ad_No_der_enreg; // adresse o� est stock�e, dans le pic, le num�ro du dernier enregistrement 
extern unsigned int No_der_enreg ; // valeur du num�ro du dernier enregistrement   entre 0 et 80

unsigned int Offset_Lect;
extern unsigned char ConsAffTPA;    // valid conserve affichage TPA lorsqu'on quitte le mode mesure tpa
extern unsigned char F_sauve_mes_tpA; 		// flag pour m�mo sauvegarde valid�e des mesures des tp adressables
extern unsigned char F_sauve_mes_tpR; 		// flag pour m�mo sauvegarde valid�e des mesures des tp r�sistifs
extern unsigned char F_sauve_mes_Deb; 		// flag pour m�mo sauvegarde valid�e des mesures des d�bitm�tres

extern unsigned int Freq_R;			// valeur fr�quence ou pression d'un TPRunsigned int Debit;     		// D�bit mesur� en cours

#define adr_mesureTPA  7E00 	// valeur de l'adresse du stockage de la mesure tpa en cours  (7E00 correspond � l'enregistrement 84) 							
unsigned int Pression_TPR;	// Valeur de pression pour TPR
unsigned int Etat_TPR;	//	Etat TPR 1 = OK, 2 = absent, 3 = D�faut, 4 = TPA pr�sent
unsigned int Etat_Deb;	// Etat pour d�bitm�tre   2 = absent,  1 = OK

//#pragma		code	USER_ROM   // retour � la zone de code


void Save_tpa_i2c()
{
unsigned char tpa;
unsigned int i;
				

				Offset_Enr = (0X180 * No_der_enreg);   // ce qui donne la valeur de l'adresse de d�but de stockage en eeprom_i2c
			if (Offset_Enr >=0x7800)
			{
					SetPos(0,56);
				PutMessage("Memoire pleine !");		// 
				plot(8,57);  // pour �
				plot(9,56);  // pour �
 				DELAY_MS (100);
			}
			else 
			{   		// m�moire pleine donc on ne poursuit pas l'enregistrement

	 			No_der_enreg+=1;

				Der_Enreg = Offset_Enr;
				Wr_eeprom_int (ad_Der_Enreg,Der_Enreg);  // m�morise en eeprom du pic la derniere adresse stock�e en eeprom_i2c
				Wr_eeprom_int (ad_No_der_enreg,No_der_enreg);  // m�morise en eeprom du pic le num�ro de l'enregistrement stock� en eeprom_i2c

				Tab_No_Enr[0]= No_der_enreg; // met la valeur pour enregistrer en eeprom_i2c
				i=0;
				SetPos(0,0);								// Positionner le curseur
				PutMessage("ENREGISTREMENT MESURES");	
				SetPos(50,24);								// Positionner le curseur
				PutMessage("No:");	
				displayNum (70,24,No_der_enreg);
				//  ICI PREVOIR DE SAUVEGARDER LE NUMERO DE L'ENREGISTREMENT AINSI QUE SON ADRESSE DE DEBUT (en eeprom_i2c) ET LES PLACER EN EEPROM DU PIC  

				//***********N�d'enregistrement***********
				Wr_eeprom_i2c_int(&Tab_No_Enr_eep[0] + Offset_Enr, Tab_No_Enr[0]);     // �criture num�ro d'enregistrement

				//***********Type d'enregistrement*****
				Wr_eeprom_i2c_char(&Tab_Type_Enr_eep[0]+ Offset_Enr, Tab_Type_Enr[0]); // �criture type de capteur===>(TPA=0,Groupe=1,D�bitm�tre=2,TPR=3,Icra=4)

				//***********Horodatage***********
				for (i=0;i<=9;i++)
					{
				Wr_eeprom_i2c_char(&Tab_Horodatage_eep[i]+ Offset_Enr, Tab_Horodatage[i]);     // �criture int
					}

				//***********Central***********
				for (i=0;i<=13;i++)
					{
				Wr_eeprom_i2c_char(&Tab_Central_eep[i]+ Offset_Enr, Tab_Central[i]);     // �criture int
					}


	//*********** (Voie) Robinet T�te ***********!!!!!  voie n'est plus 3 caracteres mais un nombre de 1 a 100  dans Tab_VoieRobTete[0]
				for (i=0;i<=9;i++)
					{
				Wr_eeprom_i2c_char(&Tab_VoieRobTete_eep[i]+ Offset_Enr, Tab_VoieRobTete[i]);     // �criture int
					}



				//***********Constitution***********
				if (F_sauve_mes_tpA==1)				// bit � 1 sauvegarde des TPA
					{
				for (tpa=0;tpa<=16;tpa++)
						{
						for (i=0;i<=9;i++)
							{
				Wr_eeprom_i2c_char(&Tab_Constit_eep[tpa][i]+ Offset_Enr, Tab_Constit[tpa][i]);     // �criture int
							}


				//*********** Pression**I_Repos**I_Conso**I_Modul**Etat***********
				Wr_eeprom_i2c_int(&Tab_Pression_eep[tpa]+ Offset_Enr, Tab_Pression[tpa]);     // �criture int
				Wr_eeprom_i2c_int(&Tab_I_Repos_eep[tpa]+ Offset_Enr, Tab_I_Repos[tpa]);     // �criture int
				Wr_eeprom_i2c_char(&Tab_I_Modul_eep[tpa]+ Offset_Enr, Tab_I_Modul[tpa]);     // �criture int
				Wr_eeprom_i2c_int(&Tab_I_Conso_eep[tpa]+ Offset_Enr, Tab_I_Conso[tpa]);     // �criture int
				Wr_eeprom_i2c_char(&Tab_Etat_eep[tpa]+ Offset_Enr, Tab_Etat[tpa]);     // �criture int
						}
					} // fin de if (F_sauve_mes_tpA==1)


				// Modif du 21/02/2016
				
				else if(F_sauve_mes_tpR==1)	// si bit � 1 de sauvegarde des TPR   
						{
				Wr_eeprom_i2c_int(&Tab_Pression_eep[1]+ Offset_Enr, Freq_R);     // �criture pression TPR:(Freq_R) � la place d'un TPA1 en 1
				Wr_eeprom_i2c_char(&Tab_Etat_eep[1]+ Offset_Enr, Etat_TPR);     // �criture Etat: OK code=1  Absent code=2  D�faut code=3  TPA D�tect� code=4
						}
				else if(F_sauve_mes_Deb==1)	// si bit � 1 de sauvegarde des TPR   
						{
				Wr_eeprom_i2c_int(&Tab_Pression_eep[0]+ Offset_Enr,Tab_Pression[0]);     // �criture debit
				Wr_eeprom_i2c_char(&Tab_Etat_eep[0]+ Offset_Enr, Etat_Deb);     // �criture Etat: OK code=1  Absent code=2  D�faut code=3  TPA D�tect� code=4
						}
	
			}	
}


void Lect_tpa_i2c ()
{
unsigned char tpa;
unsigned int i;  
unsigned int tpa_max;


				tpa_max = 17;   // 16tpa + debit en 0

				//***********N�d'enregistrement***********
				Tab_No_Enr[0] = Rd_eeprom_i2c_int(&Tab_No_Enr_eep[0] + Offset_Lect );


				//***********Type d'enregistrement****** (TPA=0,Groupe=1,D�bitm�tre=2,TPR=3,Icra=4)
				Tab_Type_Enr[0] = Rd_eeprom_i2c_int(&Tab_Type_Enr_eep[0] + Offset_Lect );
				if (Tab_Type_Enr[0]==3)	//Type de capteur===>(TPA=0,Groupe=1,D�bitm�tre=2,TPR=3,Icra=4)
					{
					F_sauve_mes_tpR=1;
					F_sauve_mes_tpA=0;
					F_sauve_mes_Deb=0;
					}
				else if (Tab_Type_Enr[0]==0)
					{
					F_sauve_mes_tpR=0;
					F_sauve_mes_tpA=1;
					F_sauve_mes_Deb=0;
					}
				else if (Tab_Type_Enr[0]==2)
					{
					F_sauve_mes_tpR=0;
					F_sauve_mes_tpA=0;
					F_sauve_mes_Deb=1;
					}



				//***********Horodatage***************** (Voie) Robinet T�te ***********

	//*********** (Voie) Robinet T�te ***********!!!!!  voie n'est plus 3 caracteres mais un nombre de 1 a 100  dans Tab_VoieRobTete[0]
				for (i=0;i<=9;i++)
					{
				Tab_Horodatage[i] = Rd_eeprom_i2c_char( &Tab_Horodatage_eep[i] + Offset_Lect ); 
				Tab_VoieRobTete[i] = Rd_eeprom_i2c_char( &Tab_VoieRobTete_eep[i]+ Offset_Lect ); 
					}


				//***********Central****************************

				for (i=0;i<=13;i++)
					{
				Tab_Central[i] = Rd_eeprom_i2c_char( &Tab_Central_eep[i]+ Offset_Lect ); 
					}


/*
//				for (tpa=0;tpa<=tpa_max;tpa++)
				for (tpa=0;tpa<5;tpa++)
					{
				Tab_I_Repos[tpa] = Rd_eeprom_i2c_int( &Tab_I_Repos_eep[tpa] );
				if (Tab_I_Repos[tpa] == 0 ){tpa_max = tpa;}
					
					}
*/
				//***********Constitution + MESURES **********

				if (F_sauve_mes_tpA==1)				// bit � 1 LECTURE MESURES des TPA
					{
				for (tpa=0;tpa<=16;tpa++)
					{
						for (i=0;i<=9;i++)
							{
						Tab_Constit[tpa][i]=	Rd_eeprom_i2c_char( &Tab_Constit_eep[tpa][i]+ Offset_Lect );
							}


				Tab_Pression[tpa] = Rd_eeprom_i2c_int( &Tab_Pression_eep[tpa]+ Offset_Lect );
				Tab_I_Repos[tpa] = Rd_eeprom_i2c_int( &Tab_I_Repos_eep[tpa]+ Offset_Lect );
				Tab_I_Modul[tpa] = Rd_eeprom_i2c_char( &Tab_I_Modul_eep[tpa]+ Offset_Lect);
				Tab_I_Conso[tpa] = Rd_eeprom_i2c_int( &Tab_I_Conso_eep[tpa]+ Offset_Lect);
				Tab_Etat[tpa] = Rd_eeprom_i2c_char( &Tab_Etat_eep[tpa]+ Offset_Lect);
						}
					}  //FIN DE F_sauve_mes_tpA==1
					else if (F_sauve_mes_tpR==1)				// bit � 1 LECTURE MESURE des TPR
						{
				Freq_R = Rd_eeprom_i2c_int( &Tab_Pression_eep[1]+ Offset_Lect ); // Lecture de la valeur de pression en 1 de Ta__Pression
				Etat_TPR = Rd_eeprom_i2c_char( &Tab_Etat_eep[1]+ Offset_Lect);// Lecture Etat: OK code=1  Absent code=2  D�faut code=3  TPA D�tect� code=4
						}
					else if (F_sauve_mes_Deb==1)				// bit � 1 LECTURE MESURE des TPR
						{
				Tab_Pression[0] = Rd_eeprom_i2c_int( &Tab_Pression_eep[0]+ Offset_Lect ); // Lecture de la valeur du d�bit cable en 0 des TPA
//				Debit = Tab_Pression[0]; // Lecture de la valeur du d�bit cable en 0 des TPA
				Etat_Deb = Rd_eeprom_i2c_char( &Tab_Etat_eep[0]+ Offset_Lect);// Lecture Etat: OK code=1  Absent code=2  D�faut code=3  TPA D�tect� code=4

						}
}



void Save_MesEnCours_tpa_i2c(unsigned int INTValue)  // pour le r�affichage des mesures si on quitte on retourne � l'�cran mesure (UNIQUEMENT POUR LES TPA)
{
unsigned char tpa;
				
				Offset_Enr=INTValue;		// on charge dans l'Offset la valeur de l'adresse fixe de sauvergarde de la mesure en cours

				//***********Type d'enregistrement****** 
				Wr_eeprom_i2c_char(&Tab_Type_Enr_eep[0]+ Offset_Enr, Tab_Type_Enr[0]);  // (TPA=0,Groupe=1,D�bitm�tre=2,TPR=3,Icra=4)


				//***********Mesures***********
				for (tpa=0;tpa<=16;tpa++)
				{
				//*********** Pression**I_Repos**I_Conso**I_Modul**Etat***********
				Wr_eeprom_i2c_int(&Tab_Pression_eep[tpa]+ Offset_Enr, Tab_Pression[tpa]);     // �criture int
				Wr_eeprom_i2c_int(&Tab_I_Repos_eep[tpa]+ Offset_Enr, Tab_I_Repos[tpa]);     // �criture int
				Wr_eeprom_i2c_char(&Tab_I_Modul_eep[tpa]+ Offset_Enr, Tab_I_Modul[tpa]);     // �criture int
				Wr_eeprom_i2c_int(&Tab_I_Conso_eep[tpa]+ Offset_Enr, Tab_I_Conso[tpa]);     // �criture int
				Wr_eeprom_i2c_char(&Tab_Etat_eep[tpa]+ Offset_Enr, Tab_Etat[tpa]);     // �criture int
				}
			
}


void Lect_MesEnCours_tpa_i2c (unsigned int INTValue)  // pour le r�affichage des mesures si on quitte on retourne � l'�cran mesure (UNIQUEMENT POUR LES TPA) 
{
unsigned char tpa;

				Offset_Lect=INTValue;		// on charge dans l'Offset la valeur de l'adresse fixe de lecture de la mesure en cours
		

				//***********Type d'enregistrement****** (TPA=0,Groupe=1,D�bitm�tre=2,TPR=3,Icra=4)
				Tab_Type_Enr[0] = Rd_eeprom_i2c_int(&Tab_Type_Enr_eep[0] + Offset_Lect );

				//***********Mesures***********

				for (tpa=0;tpa<=16;tpa++)
					{

				Tab_Pression[tpa] = Rd_eeprom_i2c_int( &Tab_Pression_eep[tpa]+ Offset_Lect );
				Tab_I_Repos[tpa] = Rd_eeprom_i2c_int( &Tab_I_Repos_eep[tpa]+ Offset_Lect );
				Tab_I_Modul[tpa] = Rd_eeprom_i2c_char( &Tab_I_Modul_eep[tpa]+ Offset_Lect);
				Tab_I_Conso[tpa] = Rd_eeprom_i2c_int( &Tab_I_Conso_eep[tpa]+ Offset_Lect);
				Tab_Etat[tpa] = Rd_eeprom_i2c_char( &Tab_Etat_eep[tpa]+ Offset_Lect);
					}
}



/*



void Test_eeprom_i2c_int ()  // UNIQUEMENT pour tester le fonctionnement de l'eeprom
	{
	double val_double  ;
		char DATAtoLCD[30];
		// test eeprom ECRITURE LECTURE INT
				SetPos(10,0);								// Positionner le curseur
				PutMessage("TEST EEPROM");	
				SetPos(0,8);								// Positionner le curseur
				PutMessage("lect en 0=1234 et 2=7352");	
			
				val_eeprom_i2c= 1234 ;	//	
   				adr_eeprom = 0x00 ;
				Wr_eeprom_i2c_int(adr_eeprom, val_eeprom_i2c);     // �criture int
				DELAY_MS (10);
				val_eeprom_i2c= 7352 ;	//	
 				adr_eeprom = 0x02 ;
				Wr_eeprom_i2c_int(adr_eeprom, val_eeprom_i2c);		// �criture int
				DELAY_MS (10);

   				adr_eeprom = 0x00 ;
				val_eeprom_i2c = Rd_eeprom_i2c_int(adr_eeprom);	// lecture int
  				displayNum(10,20 , adr_eeprom);	
  				displayNum(10,26 , val_eeprom_i2c);	
				adr_eeprom += 0x02;
				DELAY_MS (250);
			
				//*
				adr_eeprom = 0x02;
				val_eeprom_i2c = Rd_eeprom_i2c_int(adr_eeprom);	// lecture int
  				displayNum(50,20 , adr_eeprom);	
  				displayNum(50,26 , val_eeprom_i2c);	
				DELAY_MS (300);
//				adr_eeprom++;
//				}
//				adr_eeprom= 0x0A;
//				Wr_eeprom_i2c_char( adr_eeprom,chaines1);

				adr_eeprom = 0x0A;
//				val_double  = 16524.456;
				val_double  = -16524.456;
				Wr_eeprom_i2c_double(adr_eeprom,&val_double);
				SetPos(0,40);								// Positionner le curseur
				PutMessage("lecture: -16524.45");	

				adr_eeprom = 0x0A;
				val_double = 0;
			    Rd_eeprom_i2c_double(adr_eeprom,&val_double);
 				SetPos(10,50);								// Positionner le curseur
				if ( val_double < 0)
				  	{
				  *DATAtoLCD = '-';   //pour positionner le - en premier caractere de DATAtoLCD
					
				  val_double = -val_double;
				ftoa (val_double,DATAtoLCD+1,2);				// Convertir un float en une chaine de caract�res
					 }
					else 
					{
				ftoa (val_double,DATAtoLCD,2);				// Convertir un float en une chaine de caract�res
					}
 				PutHFloat(DATAtoLCD);
//				PutFloat(DATAtoLCD);
					DELAY_MS (1000);
	}

*/







