#include <p18f8723.h>
#include <codage.h>
#include <inituc.h>
#include <delay.h>
#include <delays.h>
#include <GLCD.h>
#include <ecrans.h>


/*
Pour une lecture seule:
	TSTcc + LectCodeTP

Pour une programmation:
	ProgCodeTP + LectCodeTP (sans TSTcc)
*/


#define ccc	0x01  		// erreur= court-circuit du capteur
#define err	0x02 	 	// erreur= erreur dans lecture code ou comparaison codes
#define nor	0x04  		// erreur= non r�ponse du capteur
#define delay1 0x19		// valeur pour tempo correspond � delay1 (24mS) du bcta 


unsigned char Xcode;		// code pour emission
unsigned char Tcode;		// code pour r�ception
unsigned char Tcode1;		// code pour r�ception
unsigned char Zcode;		// code tampon pour contr�le
extern unsigned char Code_Lect;
extern unsigned char Code_Prog;
unsigned char erreur_codage;   //  erreurs sur bcta
//unsigned char memTcode;  // code tampon pour r�ception
//unsigned char memcode1;  // code tampon pour r�ception
//unsigned char memcode2;  // code tampon pour r�ception
extern unsigned char TPA_detecte;  // indique si TPA d�tect� dans le test pr�sence TPA dans la mesure des r�sistifs
//unsigned char del;
#pragma		code	USER_ROM   // retour � la zone de code

void LectCodeTP(void)
{
	
//DELAY_MS(100); //V1.88 
			Code_Lect = 0;

			DEMprog();   // D�but prog   d�but partie equ � DEMprog sur bcta

			Xcode = 0x11;	// code mode lecture codage = 0x11
			DELAY_MS(24);	// <== IMPORTANT (Delay1 (24,4ms)sur bcta)  VALEUR AUGMENT�e car lecture suite � prog ne fonctionnait plus!!!!
			Xmtr();			// envoi de la demande de lecture 
//			DELAY_MS(delay1);	// d�lai de (x)ms   (Delay1 (24,4ms)sur bcta)
//			DELAY_MS(24);	// <== IMPORTANT (Delay1 (24,4ms)sur bcta)  VALEUR AUGMENT�e car lecture suite � prog ne fonctionnait plus!!!!
							// mais lecture seule fonctionnait!!! MINI 40

			Tmtr(1);			// r�ception du 1er code de confirmation
			Tcode1=Tcode;
	//		DELAY_MS(25);	// <== !� ajuster
			if (erreur_codage ==0 ) //pas d'erreur
			{
			Tmtr(1);			// r�ception du 2�me code de confirmation
					if (Tcode!=0x22) //V1.88
					{//V1.88
					Tcode=Tcode&0x7F; //on masque le bit de poid fort sur certain capteurs on a 10001010 au lieu de 0001010 dans la 2ieme interrogation //V1.88					
					}//V1.88
			}
			DELAY_MS(1);	
			DELAY_MS(1);	
			DELAY_MS(1);	

			if (erreur_codage ==0) //si pas d'erreur  
			{
				if (Tcode1 == Tcode  && Tcode1!=0x22 && Tcode!=0x22) // 0x22 est la valeur retoun�e si le capteur ne r�pond pas bien
				{  
				Code_Lect = Tcode;
				Led_verte = 1;
				}
				else {				
					erreur_codage = err;
					}
			}
	//arret du convertisseur....
			if (erreur_codage!=0)
				{
			Led_rouge = 1;
			Buzzer_on();
				}
			HT_ON = 0;		// Arr�t du convertisseur
			CPT_ON = 0;		// d�valide ligne de mesure 
			UN_CPT = 0;		// set convertisseur pour 60V
			PONT_ON = 1;	// zener off
			Led_jaune =0;
			DELAY_MS(125);	// d�lai de (x)ms   (Delay1 (24,4ms)sur bcta)
		
}



void ProgCodeTP (void)
{
			Zcode = 0;
			Tcode = 0;
			erreur_codage = 0;
			TSTcc();    // impulsion de d�part  d�but partie correspondant � TSTcc sur bcta
			DEMprog();   // D�but prog   d�but partie equ � DEMprog sur bcta

			Xcode = 0x22;	// code mode programmation = 0x22
			Zcode =	Xcode;			
			Xmtr();			// envoi de la demande de programmation du code
			PONT_ON = 1;	// zener off    <========essai============================  
			UN_CPT = 1;		// set convertisseur pour 80V<=========essai========================== 

//			DELAY_MS(60);	//<<<############################ d�lai de (60)ms    permet de masquer une impulsion parasite du pcpg,  tres important!!!
//parait fonctionner sans cette tempo de 60mS

			Tmtr(1);			// r�ception du code de confirmation
//		if (erreur_codage == nor) 	// si code = nor => non r�ponse
//			{
//			return;
//			}

			if (erreur_codage == 0 )//si pas d'erreur 
				{
				if (Tcode != Zcode)  // test si les 2 codes sont diff�rents
					{
				erreur_codage = err;
 				Led_rouge = 1;
				Buzzer_on();
				DELAY_MS(50);	// d�lai de (x)ms   (Delay1 (24,4ms)sur bcta)
//				return;
					}
//				return;
				}
			//on peut continuer

///			PONT_ON = 0;	// zener on    <====================================  
///			UN_CPT = 1;		// set convertisseur pour 80V<==========1=ok========================= 
			DELAY_MS(50);	// attente <==========50=ok========================
if (erreur_codage == 0 )//si pas d'erreur
				{
			Xcode = Code_Prog;	// 1er envoi du nouveau code du TP
			Xmtr();			// envoi du code au TP
			DELAY_MS(50);	// attente 50ms
			Xcode = Code_Prog;	// 2�me envoi du nouveau code du TP
			Xmtr();			// envoi du code au TP
			DELAY_MS(50);	// attente 50ms<++++++++++++++++++++++++++
			UN_CPT = 0;		// set convertisseur pour 60V
			PONT_ON = 1;	// zener off   

			DELAY_MS(600);	// d�lai ec 		
//			while (REP_CPT == 0); // attent le bit de start
//			Control_Prog();

				}

			CPT_ON = 0;		// d�valide ligne de mesure 
			HT_ON = 0;		// arr�t convertisseur

//			DELAY_MS(200);	// d�lai ec 
			DECH_LIGN = 0;   // pas de d�charge ligne
//			DELAY_MS(200);	// d�lai ec 		

			Led_jaune = 0;          //1
//			Led_rouge = 0;
}



		// ajouter la comparaison des codes





 
void Xmtr (void)    // emission d'un code
{

int i;
						// R�glages convertisseur
		UN_CPT = 1;		// set convertisseur pour 80V
		PONT_ON = 0;	// zener on
		DELAY_MS(40);	// IMPORTANT d�lai rajout� avec batterie < 7.0V   voir si ok avec batterie bien charg�e 40
						// test� jusqu'� Vbatt = 6.67v
		PONT_ON = 1;	// zener off    envoi bit de start
//		DELAY_MS(delay1);	// d�lai de (x)ms   (Delay1 (24,4ms)sur bcta)
		DELAY_MS(30);	// IMPORTANT (Delay1 (24,4ms)sur bcta) SI BATTERIE FAIBLE IL FAUT AUGMENTER CETTE TEMPO  DE 24 � 30ms. 
						// ce d�lai de 30msec Semble fonctionner si batterie forte
		i=0;
		for (i=0;i<=7;i++)
			{
			if (Xcode&0x01== 1)
				{
			PONT_ON = 1;	// zener off    SI BIT = 1 
				}	
			else
				{
			PONT_ON = 0;	// zener on  SI BIT = 0
				}
//		DELAY_MS(delay1);	// d�lai de (x)ms   (Delay1 (24,4ms)sur bcta)
		DELAY_24_4mS(); // d�lai de 24,4mS
			Xcode >>=1;
			}
		PONT_ON = 1;	// zener off    envoi bit de stop

//		DELAY_MS(delay1);	// d�lai de (x)ms   (Delay1 (24,4ms)sur bcta)
		DELAY_24_4mS(); // d�lai de 24,4mS

		PONT_ON = 0;	// zener on 
		UN_CPT = 0;		// set convertisseur pour 60V<====================0==============================

//		DELAY_MS(delay1);	// d�lai de (x)ms   (Delay1 (24,4ms)sur bcta)
		DELAY_24_4mS(); // d�lai de 24,4mS

}
 




void Tmtr(char ett)	// r�ception
{
unsigned int Compteur;
		
		UN_CPT = 0;		// set convertisseur pour 60V
		PONT_ON = 1;	// zener off    
//		DELAY_MS(delay1);	// d�lai de (x)ms   (Delay1 (24,4ms)sur bcta)
		
			//	Attente r�ception du bit de start pendant 0,5sec
			// pour attente de ~255* 2ms d'apr�s soft BCTA soit ~0.5sec!!!

		Compteur = 50;   // pour attendre 500msec  le bit de start de la r�ponse   
		while (REP_CPT==0 && Compteur>0)    //   && avant modif -> ||  
			{
		DELAY_100US(100); // delai de 100x100�sec soit 10000�sec = 10msec x 50 soit 500ms 
			Compteur--;
			}
		if (REP_CPT==1 && Compteur > 0)
			{
		DELAY_MS(15);	// d�lai de (x)ms   (12ms)<=========================� voir=  sur BCTA 14.64mS
			
			//parasite ou le capteur n'est pas pass� en programmation //V1.88
			if ((REP_CPT==0)&& (ett==1))//V1.88 
				{ //V1.88
				
						Compteur = 50;   // pour attendre 500msec  le bit de start de la r�ponse   //V1.88
						while (REP_CPT==0 && Compteur>0)    //   && avant modif -> ||  //V1.88
							{
						DELAY_100US(100); // delai de 100x100�sec soit 10000�sec = 10msec x 50 soit 500ms //V1.88
							Compteur--;//V1.88
							}//V1.88
				DELAY_MS(15); //V1.88
				}//V1.88




			if (REP_CPT==1)  // confirmation si toujours pr�sent apr�s tempo
				{
//		DELAY_MS(12);	// d�lai de (x)ms   (12ms)  <=========================� voir==sur BCTA 24.4mS
		DELAY_24_4mS(); // d�lai de 24,4mS
			RcptCode();				//Confirmation du bit de start re�u on peut lire le cod
		
				}
			}
		else 
			{
			erreur_codage = nor;    //==> non r�ponse du capteur
			Led_rouge = 1;
			Buzzer_on();
			DELAY_MS (700);	//* attente stabilisation V48v		
			}
}


void RcptCode (void)

{
int j;
			j=0;
			Tcode=0;	
			for (j=0;j<=7;j++)
			{
			if (REP_CPT == 1)
				{
				Tcode |= 0x80;
				}
			if (j<7)
					{
				Tcode >>=1;
					}
		
//		DELAY_MS(delay1);	// d�lai de (x)ms   (Delay1 (24,4ms)sur bcta)
		DELAY_24_4mS(); // d�lai de 24,4mS
			}


//		DELAY_MS(delay1);	// d�lai de (x)ms   (Delay1 (24,4ms)sur bcta)
		DELAY_24_4mS(); // d�lai de 24,4mS
}




void TSTcc (void)
{
			// impulsion de d�part  d�but partie correspondant � TSTcc sur bcta
				Led_rouge = 0;          
				Led_jaune = 1;          
				Led_verte = 0;          

//			Code_Lect = 0;		//
			erreur_codage = 0;
			DECH_LIGN = 0;   // pas de d�charge ligne
				IMP_BCTA=1;    //*impulsion sur relais pour mode BCTA TRANS
				DELAY_MS (50);	//*
				IMP_BCTA=0;	//*
				DELAY_MS (20);	//*
			PONT_ON = 1;	// zener off (court circuit�e)
			UN_CPT = 0;		// set convertisseur pour 60V
			HT_ON = 1;		// mise en route du convertisseur
			DELAY_MS (20);	//* attente stabilisation V48v
			CPT_ON = 1;		// ligne de mesure valid�e
				Led_jaune = 1;          
			DELAY_MS (500);	// d�lai de (x)ms
			if (REP_CPT != 0)
				{
				erreur_codage = ccc;    //==> court circuit capteur
				Led_rouge = 1;          
   				Buzzer_on();
				}
			HT_ON = 0;		// Arr�t du convertisseur
			CPT_ON = 0;		// d�valide ligne de mesure 
			PONT_ON = 0;	// zener on
//			Led_jaune = 0;          //1
			DELAY_SEC (2);	// d�lai de (x)sec
			DELAY_MS (700);	// d�lai de (x)ms

		// Fin impulsion de d�part et tempo  fin partie equ � TSTcc sur bcta
}



void DEMprog (void)   // D�but prog   d�but partie equ � DEMprog sur bcta
{
		
			PONT_ON = 0;	// zener on pour avoir ~80v-Vzener	
			UN_CPT = 1;		// set convertisseur pour 80V
			HT_ON = 1;		// mise en route du convertisseur
			CPT_ON = 1;		// ligne de mesure valid�e
				Led_jaune = 1;          //1
			DELAY_MS (800);	// d�lai de (x)ms 
			PONT_ON = 1;	// zener off pour envoyer 80V
			DELAY_SEC (1);	// d�lai de (x)sec
			DELAY_MS (400);	// d�lai de (x)ms  
			PONT_ON = 0;	// zener ON pour redescendre en tension
			DELAY_MS (90);	// d�lai de (x)ms   
		//  fin de partie equ � DEMprog sur bcta




//		DELAY_MS (24);	// d�lai de (x)ms   (Delay1 (24,4ms)sur bcta)
//		DELAY_10US(40); // delai de x10�sec soit 400�sec

}


//***************************************************************************
//Nom   : void DELAY_24,4mS
//Role  : Delay de 1 milliseconde / Quartz 24Mhz (HS)
//utilise timer0
//*****************************************************************************/
void DELAY_24_4mS(void) // d�lai de 24,4mS

{
int t1;
int t2;
			t1=0x18;  //24mS
			t2=0x4;  //400�s
			DELAY_MS (t1);	// d�lai de 24ms +
			DELAY_100US(t2); // d�lai de 4x100�S => 24,400mS	
return;
}


void Test_ReponseCode(void)
{
unsigned int Compteur;

			TSTcc2();
			Code_Lect = 0; // mem lecture code

			DEMprog();   // D�but prog   d�but partie equ � DEMprog sur bcta

			Xcode = 0x11;	// code mode lecture codage = 0x11
			DELAY_MS(24);	// <== IMPORTANT (Delay1 (24,4ms)sur bcta)  VALEUR AUGMENT�e car lecture suite � prog ne fonctionnait plus!!!!
			Xmtr();			// envoi de la demande de lecture 
//			DELAY_MS(delay1);	// d�lai de (x)ms   (Delay1 (24,4ms)sur bcta)
//			DELAY_MS(24);	// <== IMPORTANT (Delay1 (24,4ms)sur bcta)  VALEUR AUGMENT�e car lecture suite � prog ne fonctionnait plus!!!!
							// mais lecture seule fonctionnait!!! MINI 40


// Suite si dessous est l'�quivalent de Tmtr(), mais modifi�
		
		UN_CPT = 0;		// set convertisseur pour 60V
		PONT_ON = 1;	// zener off    
//		DELAY_MS(delay1);	// d�lai de (x)ms   (Delay1 (24,4ms)sur bcta)
		
			//	Attente r�ception du bit de start pendant 0,5sec
			// pour attente de ~255* 2ms d'apr�s soft BCTA soit ~0.5sec!!!

		Compteur = 50;   // pour attendre 500msec  le bit de start de la r�ponse   
		while (REP_CPT==0 && Compteur>0)    //   && avant modif -> ||  
			{
		DELAY_100US(100); // delai de 100x100�sec soit 10000�sec = 10msec x 50 soit 500ms 
			Compteur--;
			}
	if (REP_CPT==1 && Compteur > 0)
		{
		DELAY_MS(15);	// d�lai de (x)ms   (12ms)<=========================� voir=  sur BCTA 14.64mS
			if (REP_CPT==1)  // confirmation si toujours pr�sent apr�s tempo
				{
		// TPA PRESENT!!!!!		
			TPA_detecte=1;
			Led_rouge = 1;  
				}
		}
		else 
		{
		// PAS DE REPONSE CODE DONC PAS DE TPA !!!!!		
			TPA_detecte=0;
			Led_rouge = 0;  
		}
			HT_ON = 0;		// Arr�t du convertisseur
			CPT_ON = 0;		// d�valide ligne de mesure 
			PONT_ON = 0;	// zener on
			Led_jaune = 0;          //1

			// Retour mode lecture
				IMP_TRANS=1;    //*impulsion sur relais pour mode transtest
				DELAY_MS (50);	//*
				IMP_TRANS=0;	//*
}


void TSTcc2(void)
{
			// impulsion de d�part  d�but partie correspondant � TSTcc sur bcta
				Led_rouge = 0;          
				Led_jaune = 0;          
				Led_verte = 0;          

			DECH_LIGN = 0;   // pas de d�charge ligne
				IMP_BCTA=1;    //*impulsion sur relais pour mode BCTA TRANS
				DELAY_MS (50);	//*
				IMP_BCTA=0;	//*
				DELAY_MS (20);	//*
			PONT_ON = 1;	// zener off (court circuit�e)
			UN_CPT = 0;		// set convertisseur pour 60V
			HT_ON = 1;		// mise en route du convertisseur
			DELAY_MS (20);	//* attente stabilisation V48v
			CPT_ON = 1;		// ligne de mesure valid�e
				Led_jaune = 1;          
			DELAY_MS (500);	// d�lai de (x)ms
			HT_ON = 0;		// Arr�t du convertisseur
			CPT_ON = 0;		// d�valide ligne de mesure 
			PONT_ON = 0;	// zener on
			DELAY_SEC (2);	// d�lai de (x)sec
			DELAY_MS (700);	// d�lai de (x)ms

		// Fin impulsion de d�part et tempo  
}








