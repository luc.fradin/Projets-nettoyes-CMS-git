/*
 * Project: Digital_Clock_V2.0
 * File Name: rtc.c
 * Author: Siddharth Chandrasekaran
 * Created on July 20, 2012, 6:12 PM
 * Visit http://embedjournal.com for more codes.
*/
#include <p18f8723.h>
#include "i2cds.h"
//#include <ecrans.h>
//#include "lcd.h"

void reset_time(unsigned int SetHeures,unsigned int SetMinutes,unsigned int SetSecondes,unsigned int Jour,unsigned int SetDate,unsigned int SetMois,unsigned int SetAnnee)       // resets the time-keeping register
{

    i2c_start();
    i2c_write(0b11010000);
    i2c_write(0x00);    // word adress
    i2c_write(SetSecondes);    // sec 		adr	0
    i2c_write(SetMinutes);    // min		"	1
    i2c_write(SetHeures);    // hrs		"	2
    i2c_write(Jour);    // day		"	3
    i2c_write(SetDate);    // date		"	4
    i2c_write(SetMois);    // month	"	5
    i2c_write(SetAnnee);    // year		"	6
    i2c_write(0x10);	// wave		"	7
    i2c_stop();
    return;
}

unsigned int get_time(unsigned int address)     // Gets time
{
    unsigned int data;
    i2c_start();
    i2c_write(0b11010000);
    i2c_write(address);
    i2c_restart();
    i2c_write(0b11010001);
    SSPCON2bits.ACKDT=1;
    data=i2c_read();
    i2c_stop();
    return (data);
}

void set_time(unsigned int address, unsigned int x)     // sets time
{
    i2c_start();
    i2c_write(0b11010000);
    i2c_write(address);
    i2c_write(x);
    i2c_stop();
    return;
}

