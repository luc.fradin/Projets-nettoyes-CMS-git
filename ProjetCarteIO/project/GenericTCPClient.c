/*********************************************************************
 * 
 *  Generic TCP Client Example Application
 *  Module for Microchip TCP/IP Stack
 *   -Implements an example HTTP client and should be used as a basis 
 *	  for creating new TCP client applications
 *	 -Reference: None.  Hopefully AN833 in the future.
 *
 *********************************************************************
 * FileName:        GenericTCPClient.c
 * Dependencies:    TCP, DNS, ARP, Tick
 * Processor:       PIC18, PIC24F, PIC24H, dsPIC30F, dsPIC33F, PIC32
 * Compiler:        Microchip C32 v1.05 or higher
 *					Microchip C30 v3.12 or higher
 *					Microchip C18 v3.30 or higher
 *					HI-TECH PICC-18 PRO 9.63PL2 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright (C) 2002-2009 Microchip Technology Inc.  All rights
 * reserved.
 *
 * Microchip licenses to you the right to use, modify, copy, and
 * distribute:
 * (i)  the Software when embedded on a Microchip microcontroller or
 *      digital signal controller product ("Device") which is
 *      integrated into Licensee's product; or
 * (ii) ONLY the Software driver source files ENC28J60.c, ENC28J60.h,
 *		ENCX24J600.c and ENCX24J600.h ported to a non-Microchip device
 *		used in conjunction with a Microchip ethernet controller for
 *		the sole purpose of interfacing with the ethernet controller.
 *
 * You should refer to the license agreement accompanying this
 * Software for additional information regarding your rights and
 * obligations.
 *
 * THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT
 * WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT
 * LIMITATION, ANY WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * MICROCHIP BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF
 * PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY CLAIMS
 * BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE
 * THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER
 * SIMILAR COSTS, WHETHER ASSERTED ON THE BASIS OF CONTRACT, TORT
 * (INCLUDING NEGLIGENCE), BREACH OF WARRANTY, OR OTHERWISE.
 *
 *
 * Author               Date    Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Howard Schlunder     8/01/06	Original
 ********************************************************************/
#define __GENERICTCPCLIENT_C

#include <TCPIPConfig.h>
//#include "TcpServer.h"
//#if defined(STACK_USE_GENERIC_TCP_CLIENT_EXAMPLE)

#include <TCPIPStack\TCPIP.h>

 
// Defines the server to be accessed for this application
#ifdef WIFI_NET_TEST
static BYTE ServerName[] =	"www" WIFI_NET_TEST_DOMAIN;
#else
static BYTE ServerName[] =	{192,168,0,11};  /////010.168.192.056    C58
#endif

// Defines the port to be accessed for this application
#if defined(STACK_USE_SSL_CLIENT)
    static WORD ServerPort = HTTPS_PORT;
	// Note that if HTTPS is used, the ServerName and URL 
	// must change to an SSL enabled server.
#else
    static WORD ServerPort = 80;  //PORT C58
#endif

// Defines the URL to be requested by this HTTP client
static ROM BYTE RemoteURL[] = "/search?as_q=Microchip&as_sitesearch=microchip.com";
void IntToChar1(unsigned int value, char *chaine,char Precision);
//unsigned int CRC116(char far *txt, unsigned  char lg_txt);
extern unsigned int CRC16(char far *txt, unsigned  int lg_txt);
/*****************************************************************************
  Function:
	void GenericTCPClient(void)

  Summary:
	Implements a simple HTTP client (over TCP).

  Description:
	This function implements a simple HTTP client, which operates over TCP.  
	The function is called periodically by the stack, and waits for BUTTON1 
	to be pressed.  When the button is pressed, the application opens a TCP
	connection to an Internet search engine, performs a search for the word
	"Microchip" on "microchip.com", and prints the resulting HTML page to
	the UART.
	
	This example can be used as a model for many TCP and HTTP client
	applications.

  Precondition:
	TCP is initialized.

  Parameters:
	None

  Returns:
  	None
  ***************************************************************************/

extern BYTE TRAMEOUT[512];
extern BYTE SERVEURIP[4];
extern int CRC;
extern WORD wMaxGet, wMaxPut, wCurrentChunk;
extern char ID_rack[30];
extern char TYPEORDREALARME;
static TCP_SOCKET	MySocket;
extern char nbrerecep,iboucle;
extern CHAR tableau[60];
//#pragma udata udata10
//BYTE TRAMEOUT[512];
extern char nbrerecep;
extern char TRAMEIN[125];
void GenericTCPClient(void)
{
 

	BYTE 				i;
	WORD				w , w2;
	BYTE				vBuffer[9];
	static DWORD		Timer;
	static TCP_SOCKET	MySocket = INVALID_SOCKET;
	static enum _GenericTCPExampleState
	{
		SM_HOME = 0,
		SM_SOCKET_OBTAINED,
		#if defined(STACK_USE_SSL_CLIENT)
    	SM_START_SSL,
    	#endif
		SM_PROCESS_RESPONSE,
		SM_DISCONNECT,
		SM_DONE
	} GenericTCPExampleState = SM_DONE;

	switch(GenericTCPExampleState)
	{
		case SM_HOME:
			// Connect a socket to the remote TCP server
			
			//MySocket = TCPOpen(*((DWORD*)&ServerName[0]), TCP_OPEN_IP_ADDRESS, ServerPort, TCP_PURPOSE_GENERIC_TCP_CLIENT);
			
			
			MySocket = TCPOpen(*((DWORD*)&ServerName[0]), TCP_OPEN_IP_ADDRESS, ServerPort, TCP_PURPOSE_GENERIC_TCP_CLIENT);
			
			// Abort operation if no TCP socket of type TCP_PURPOSE_GENERIC_TCP_CLIENT is available
			// If this ever happens, you need to go add one to TCPIPConfig.h
			if(MySocket == INVALID_SOCKET)
				break;
 
			#if defined(STACK_USE_UART)
			putrsUART((ROM char*)"\r\n\r\nConnecting using Microchip TCP API...\r\n");
			#endif

			GenericTCPExampleState++;
			Timer = TickGet();
			break;

		case SM_SOCKET_OBTAINED:
			// Wait for the remote server to accept our connection request
			if(!TCPIsConnected(MySocket))
			{
				// Time out if too much time is spent in this state
				if(TickGet()-Timer > 10*TICK_SECOND)
				{
					// Close the socket so it can be used by other modules
					TCPDisconnect(MySocket);
					MySocket = INVALID_SOCKET;
					GenericTCPExampleState--;
				}
				break;
			}
 
			Timer = TickGet();

etiq0:
		
	 	 		
	TRAMEOUT[0]='c'; //NUMERO SERVEUR TELGAT
	TRAMEOUT[1]='5';
	TRAMEOUT[2]='6';
 	TRAMEOUT[3]=0x0D;			
				
// Transfer the data out of our local processing buffer and into the TCP TX FIFO.
TCPPutArray(MySocket, TRAMEOUT, 4); // TRANSMETTRE LA TRAME
//putsUART("\n dldldld");
//TCPPut(MySocket, 'G'); // TRANSMETTRE LA TRAME


TRAMEOUT[12]='*';
TRAMEENVOIRS485DONNEES (); //NANCY
for (nbrerecep=0;nbrerecep<10;nbrerecep++) //NANCY
{
TRAMEIN[nbrerecep]='#';
}

iboucle=0;
nbrerecep=0;
etc1:
wMaxGet=0;
do 
{
if(!TCPIsConnected(MySocket))
return;

StackTask();     
StackApplications();
wMaxGet = TCPIsGetReady(MySocket);	// Get TCP RX FIFO byte count
wMaxPut = TCPIsPutReady(MySocket);	// Get TCP TX FIFO free space
}while (wMaxGet==0);

TCPGetArray(MySocket, TRAMEIN,2); //ATTENDRE ACK OU NOACK


TRAMEOUT[0]='#';
for (nbrerecep=1;nbrerecep<4;nbrerecep++) //NANCY
{
TRAMEOUT[nbrerecep]=TRAMEIN[nbrerecep-1];
}
TRAMEOUT[4]='*';
TRAMEENVOIRS485DONNEES (); //NANCY
iboucle++;
//if (iboucle<=3)
//goto et1;

//TEMPO(15); //BART
		nbrerecep=0;
		if (TRAMEIN[0]==0x05) //NO ACK
		{
		nbrerecep=100;
		//goto close;
		}
	    if (TRAMEIN[0]==0x06) //ACK
		{
		nbrerecep=50;	
		}
		if (TRAMEIN[0]!=0x06) //AUTRE
		{
		if (nbrerecep==0)
		goto etc1;
		//nbrerecep++;
		}








etiq: //ON ATTEND 0<CR> probable
//	if (TRAMEIN[0]==0x06){
				TRAMEOUT[0]=0x02;
				TRAMEOUT[1]='C';
				TRAMEOUT[2]=':';
				if (TYPEORDREALARME==1)
				TRAMEOUT[3]='A';  //A ENTREE
				if (TYPEORDREALARME==2)
				TRAMEOUT[3]='A';  //S SORTIE D'ALARME
				TRAMEOUT[4]=0x03;
				CRC=CRC16(TRAMEOUT, 5);   ///////// calcul de CRC
				IntToChar(CRC,tableau,5);   //////// conversion du CRC du int en char
				TRAMEOUT[5]=tableau[0];
				TRAMEOUT[6]=tableau[1];
				TRAMEOUT[7]=tableau[2];
				TRAMEOUT[8]=tableau[3];
				TRAMEOUT[9]=tableau[4];
				TRAMEOUT[10]=0x0C;
	TCPPutArray(MySocket, TRAMEOUT, 11);
//	}

			


TRAMEOUT[12]='*';
TRAMEENVOIRS485DONNEES (); //NANCY



for (nbrerecep=0;nbrerecep<10;nbrerecep++) //NANCY
{
TRAMEIN[nbrerecep]='#';
}

iboucle=0;
nbrerecep=0;
etc2:
wMaxGet=0;
do 
{
if(!TCPIsConnected(MySocket))
return;

StackTask();     
StackApplications();
wMaxGet = TCPIsGetReady(MySocket);	// Get TCP RX FIFO byte count
wMaxPut = TCPIsPutReady(MySocket);	// Get TCP TX FIFO free space
}while (wMaxGet==0);

TCPGetArray(MySocket, TRAMEIN,2); //ATTENDRE ACK OU NOACK
//TCPGetArray(MySocket, TRAMEIN, wCurrentChunk); //ATTENDRE ACK OU NOACK
 


TRAMEOUT[0]='#';
for (nbrerecep=1;nbrerecep<4;nbrerecep++) //NANCY
{
TRAMEOUT[nbrerecep]=TRAMEIN[nbrerecep-1];
}
TRAMEOUT[4]='*';
TRAMEENVOIRS485DONNEES (); //NANCY
iboucle++;
//if (iboucle<=3)
//goto et1;

//TEMPO(15); //BART
		nbrerecep=0;
		if (TRAMEIN[0]==0x05) //NO ACK
		{
		nbrerecep=100;
		//goto close;
		}
	    if (TRAMEIN[0]==0x06) //ACK
		{
		nbrerecep=50;	
		}
		if (TRAMEIN[0]!=0x06) //AUTRE
		{
		if (nbrerecep==0)
		goto etc2;
		//nbrerecep++;
		}





				
// Transfer the data out of our local processing buffer and into the TCP TX FIFO.



















//if (TRAMEIN[0]==0x06){
//////////////////////////////////////////////
nbrerecep=0;
etiq1:
TRAMEOUT[0]=0x02;
TRAMEOUT[1]='S';   
TRAMEOUT[2]=':';
 TRAMEOUT[3]=ID_rack[0];
                                                                    TRAMEOUT[4]=ID_rack[1];
                                                                    TRAMEOUT[5]=ID_rack[2];
                                                                    TRAMEOUT[6]=ID_rack[3];
                                                                    TRAMEOUT[7]=ID_rack[4];
                                                                    TRAMEOUT[8]=ID_rack[5];
                                                                    

                                                                    TRAMEOUT[9]=ID_rack[6];
                                                                    TRAMEOUT[10]=ID_rack[7];
                                                                    TRAMEOUT[11]=ID_rack[8];
                                                                    TRAMEOUT[12]=ID_rack[9];
                                                                    TRAMEOUT[13]=ID_rack[10];
																	TRAMEOUT[14]=ID_rack[11];
                                                                    TRAMEOUT[15]=ID_rack[12];
                                                                    TRAMEOUT[16]=ID_rack[13];
                                                                    TRAMEOUT[17]=ID_rack[14];
                                                                    TRAMEOUT[18]=ID_rack[15];
																	TRAMEOUT[19]=ID_rack[16];
                                                                    TRAMEOUT[20]=ID_rack[17];
                                                                    TRAMEOUT[21]=ID_rack[18];
                                                                    TRAMEOUT[22]=ID_rack[19];






TRAMEOUT[23]=0x03;
CRC=CRC16(TRAMEOUT, 24);   ///////// calcul de CRC
IntToChar(CRC,tableau,5);   //////// conversion du CRC du int en char
TRAMEOUT[24]=tableau[0];
TRAMEOUT[25]=tableau[1];
TRAMEOUT[26]=tableau[2];
TRAMEOUT[27]=tableau[3];
TRAMEOUT[28]=tableau[4];
TRAMEOUT[29]=0x0D;
TCPPutArray(MySocket, TRAMEOUT, 30); //JUSQUA LA LONGUEUR DU
//}




TRAMEOUT[31]='*';
TRAMEENVOIRS485DONNEES (); //NANCY

for (nbrerecep=0;nbrerecep<10;nbrerecep++) //NANCY
{
TRAMEIN[nbrerecep]='#';
}

iboucle=0;
nbrerecep=0;
etc3:
do
{
if(!TCPIsConnected(MySocket))
return;
StackTask();     
StackApplications();
wMaxGet = TCPIsGetReady(MySocket);	// Get TCP RX FIFO byte count
wMaxPut = TCPIsPutReady(MySocket);	// Get TCP TX FIFO free space
}while (wMaxGet==0);
//TCPGetArray(MySocket, TRAMEIN, wCurrentChunk); // ATENDRE ACK OU NOACK
TCPGetArray(MySocket, TRAMEIN, 2); //ATTENDRE ACK OU NOACK



TRAMEOUT[0]='#';
for (nbrerecep=1;nbrerecep<4;nbrerecep++) //NANCY
{
TRAMEOUT[nbrerecep]=TRAMEIN[nbrerecep-1];
}
TRAMEOUT[4]='*';
TRAMEENVOIRS485DONNEES (); //NANCY
iboucle++;
//if (iboucle<=3)
//goto et2;

//TEMPO(15); //BART
		nbrerecep=0;
		if (TRAMEIN[0]==0x05) //NO ACK
		{
		nbrerecep=100;
		//goto close;
		}
	    if (TRAMEIN[0]==0x06) //ACK
		{
		nbrerecep=50;	
		}
		if (TRAMEIN[0]!=0x06) //AUTRE
		{
		if (nbrerecep==0)
		goto etc3;
		//nbrerecep++;
		}





    echange(); 
	GenericTCPExampleState=SM_DISCONNECT;
			break;
close:	
		case SM_DISCONNECT:
			// Close the socket so it can be used by other modules
			// For this application, we wish to stay connected, but this state will still get entered if the remote server decides to disconnect
			TCPDisconnect(MySocket);
			MySocket = INVALID_SOCKET;
			GenericTCPExampleState = SM_DONE;
			break;
	
		case SM_DONE:
			// Do nothing unless the user pushes BUTTON1 and wants to restart the whole connection/download process
			//if(BUTTON1_IO == 0u)
				GenericTCPExampleState = SM_HOME;
			break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////// CONVERSION UNSIGNED INT TO CHAR///////////////////////////////////////////////////////////////////////////////////////// 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void IntToChar1(unsigned int value, char *chaine,char Precision)
{
unsigned int Mil, Cent, Diz, Unit, DMil;
int count = 0;

// initialisation des variables
Mil = 0; Cent = 0; Diz = 0; Unit = 0;DMil=0;
/*
if (value < 0) // si c'est un nombre n�gatif
    {
    value = value * (-1);
    *chaine = 45; // signe '-'
    count = 1;  // c'est un nombre n�gatif
    }
else // si c'est un nombre positif
    {
    count = 1;
    *chaine = 32; // code ASCII du signe espace ' '
    }
*/
// si la valeur n'est pas nulle
if (value != 0)
    {
if (Precision >= 5) // si l'utilisateur d�sire les milliers
         {
         // conversion des milliers
         DMil = value / 10000;
         if (DMil != 0)
               {
               *(chaine+count) = DMil + 48;
               if (*(chaine+count) < 48)
                     *(chaine+count) = 48;
               if (*(chaine+count) > 57)
                     *(chaine+count) = 57;
               }
         else
               *(chaine+count) = 48;
         count++;
         }
    if (Precision >= 4) // si l'utilisateur d�sire les milliers
         {
         // conversion des milliers
         Mil = value-(DMil*10000);
         Mil = Mil / 1000;
         if (Mil != 0)
               {
               *(chaine+count) = Mil + 48;
               if (*(chaine+count) < 48)
                     *(chaine+count) = 48;
               if (*(chaine+count) > 57)
                     *(chaine+count) = 57;
               }
         else
               *(chaine+count) = 48;
         count++;
         }

    if (Precision >= 3) // si l'utilisateur d�sire les centaines
        {
        // conversion des centaines
        Cent = value - (Mil * 1000)-(DMil*10000);
        Cent = Cent / 100;
        if (Cent != 0)
            {
            *(chaine+count) = Cent + 48;
            if (*(chaine+count) < 48)
                  *(chaine+count) = 48;
            if (*(chaine+count) > 57)
                  *(chaine+count) = 57;
            }
        else
            *(chaine+count) = 48;
            count++;
      }

    if (Precision >= 2) // si l'utilisateur d�sire les dizaines
         {
         // conversion des dizaines
         Diz = value - (Mil * 1000) - (Cent * 100)-(DMil*10000);
         Diz = Diz / 10;
         if (Diz != 0)
            {
            *(chaine+count) = Diz + 48;
            if (*(chaine+count) < 48)
                  *(chaine+count) = 48;
            if (*(chaine+count) > 57)
                  *(chaine+count) = 57;
            }
         else
            *(chaine+count) = 48;
         count++;
         }

    // conversion unit�s
    Unit = value - (Mil * 1000) - (Cent * 100) - (Diz * 10)-(DMil*10000);
    *(chaine+count) = Unit + 48;
    if (*(chaine+count) < 48)       // limites : 0 et 9
            *(chaine+count) = 48;
    if (*(chaine+count) > 57)
            *(chaine+count) = 57;
    }
else // if (value == 0)
    {
    //*(chaine) = 32; // ecrit un espace devant le nombre
    for (Mil=0;Mil<Precision;Mil++)   // inscription de '0' dans toute la chaine
          *(chaine+Mil) = 48;
    }

} // fin de la fonction de conversion INT => CHAR

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// calcul du CRC pour /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CRC_POLY 0x01021
#define CRC_START 0xFFFF
unsigned int CRC116(char far *txt, unsigned  char lg_txt)
{ 
	 unsigned char ii, nn;
	unsigned  int crc;
    
	 char far *p;
	crc = CRC_START;
	p=txt;

//essai[2]=*p;
ii=0;
nn=0;
	//for( ii=0; ii<lg_txt ; ii++, p++)
   do {
crc ^= (unsigned int)*p;
//for(; nn<8; nn++)
           do {
                  if (crc & 0x8000)
                     {
                         crc<<=1;
//UTIL;
                          crc^=CRC_POLY;
                      }
                      else
                      {crc<<=1;
                      }
                       nn++;
              }while (nn<8);
                      ii=ii+1;
                      p++;
if (nn==8){nn=0;}
     } while (ii<lg_txt);
ii=0;
return crc;
}

///////////////////////////////////////////////////////////////////////////////
/////////////////////tompo////////////////////////////////////////////////
void delay_qms1(char tempo)
{
unsigned char j,k,l;
 
for (j=0;j<tempo;j++)
  {
  for (k=0;k<10;k++)
    {
    for (l=0;l<40;l++)
      {
      //__no_operation();
      //__no_operation();
      //__no_operation();
      //__no_operation();
      //__no_operation();
      }
    }
  }  
j=0;
}
//#endif	//#if defined(STACK_USE_GENERIC_TCP_CLIENT_EXAMPLE)
