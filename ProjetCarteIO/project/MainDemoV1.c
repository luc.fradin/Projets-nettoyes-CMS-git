/*********************************************************************
 *
 *  Main Application Entry Point and TCP/IP Stack Demo
 *  Module for Microchip TCP/IP Stack
 *   -Demonstrates how to call and use the Microchip TCP/IP stack
 *   -Reference: Microchip TCP/IP Stack Help (TCPIP Stack Help.chm)1150

 *
 *********************************************************************
 * FileName:        MainDemo.c
 * Dependencies:    TCPIP.h
 * Processor:       PIC18, PIC24F, PIC24H, dsPIC30F, dsPIC33F, PIC32
 * Compiler:        Microchip C32 v1.11b or higher
 *                  Microchip C30 v3.24 or higher
 *                  Microchip C18 v3.36 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright (C) 2002-2010 Microchip Technology Inc.  All rights
 * reserved.
 *
 * Microchip licenses to you the right to use, modify, copy, and
 * distribute:
 * (i)  the Software when embedded on a Microchip microcontroller or
 *      digital signal controller product ("Device") which is
 *      integrated into Licensee's product; or
 * (ii) ONLY the Software driver source files ENC28J60.c, ENC28J60.h,
 *      ENCX24J600.c and ENCX24J600.h ported to a non-Microchip device
 *      used in conjunction with a Microchip ethernet controller for
 *      the sole purpose of interfacing with the ethernet controller.
 *
 * You should refer to the license agreement accompanying this
 * Software for additional information regarding your rights and
 * obligations.
 *
 * THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT
 * WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT
 * LIMITATION, ANY WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * MICROCHIP BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF
 * PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY CLAIMS
 * BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE
 * THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER
 * SIMILAR COSTS, WHETHER ASSERTED ON THE BASIS OF CONTRACT, TORT
 * (INCLUDING NEGLIGENCE), BREACH OF WARRANTY, OR OTHERWISE.
 *
 * File Description:
 * Change History:
 * Rev   Description
 * ----  -----------------------------------------
 * 1.0   Initial release
 * V5.36 ---- STACK_USE_MPFS support has been removed 
 ********************************************************************/
/*
 * This macro uniquely defines this file as the main entry point.
 * There should only be one such definition in the entire project,
 * and this file must define the AppConfig variable as described below.
 */

#define THIS_IS_STACK_APPLICATION
#include<i2c.h>
// Include all headers for any enabled TCPIP Stack functions
//#include "TCPIP Stack/ICMP.h"
#include <TCPIPStack\TCPIP.h>
//#include "TCPIPConfig.h"
#if defined(STACK_USE_ZEROCONF_LINK_LOCAL)
#include <ZeroconfLinkLocal.h>
#endif
#if defined(STACK_USE_ZEROCONF_MDNS_SD)
#include <ZeroconfMulticastDNS.h>
#endif

// Include functions specific to this stack application
#include <MainDemo.h>

// Used for Wi-Fi assertions
#define WF_MODULE_NUMBER   WF_MODULE_MAIN_DEMO

// Declare AppConfig structure and some other supporting stack variables
APP_CONFIG AppConfig;
static unsigned short wOriginalAppConfigChecksum;    // Checksum of the ROM defaults for AppConfig
BYTE AN0String[8];

// Use UART2 instead of UART1 for stdout (printf functions).  Explorer 16 
// serial port hardware is on PIC UART2 module.
#if defined(EXPLORER_16) || defined(PIC24FJ256DA210_DEV_BOARD)
    int __C30_UART = 2;
#endif
//BYTE AppBuffer[32];


#define I2CMEMO3 PORTDbits.RD3
#define I2CMEMO PORTDbits.RD0
#define I2CMEMO2 PORTDbits.RD2
#define I2CINTR PORTDbits.RD1
#define I2CHORLG PORTDbits.RD4
#define IO_EN_COM PORTDbits.RD5

#define DERE PORTAbits.RA4 // direction RX TX

#define RAZCMS	PORTBbits.RB3
#define LEDD PORTFbits.RF1
#define LEDC PORTFbits.RF0
#define LEDRJTEST1 PORTAbits.RA0
#define LEDRJTEST2 PORTAbits.RA1
//#define COUPMODEM PORTEbits.RE0 

extern int CRC;
extern WORD wMaxGet, wMaxPut, wCurrentChunk;
char nbrerecep,iboucle;
extern TCP_SOCKET MySocket;
//extern BYTE TRAMEOUT[512];
//extern char TRAMEIN[125];
//extern char tableau[60];
BOOL ICMPBeginUsage(void);
unsigned char npage, nchamps;
static enum _ETAT1 //QUELLE PAGE ON EST ?
	{
		I = 0,   /////page d'aceuille avec mots de passe 
		M,       ///// page menu
        A,       ///// page 1
		B,       ///// page2
		C,       ///// page3
		D,       ///// page4
		E,       ///// page5
		F,       ///// page6
		G,       ///// page6b
		H,       ///// page6c
        K,       ///// page8
		L,       ///// page 9 adecef reset
	} ETAT1 = I;


#pragma udata udata1
char ACTIONENCOURS;
char ETAT_CONNECT_MINITEL;
char CMouPAS;
char TERMINEACTION;
char POS[2];
char ii[2];
char CABLE[3];
char tmp,tmp2,u;
char tableau1[10];
char COD[2];
char VALEUR[4];
char CCON[4];
char CREP[4];
char CMOD[4];
char accordUSEROUADMI; //USER OU ADMI 
char capt_exist;
char JJ[2];
char MM[2];
char AA[4];
char HH[2];
char mm[2];
char ss[2];
char VOIECOD[5];
char CONSTITUTION[13];
char ETAT[4];
char DISTANCE[5];
char SEUIL[4];
char TYPE[1];
char identifiant;
char COMMENTAIRE[20];
char n_voie[2];
char nb_oct;
char SEUIL2[4];
char iiii[5];
char valeur[4];
char y[1];
char carac[1];
char testtimer;
//unsigned int SPEED;
char TRA;

//char MODERAPID;
//char NBCOM; //BUG ZIZ 

//char REINITMODEM;



#pragma udata udata2
BYTE TRAMEOUT[512];



#pragma udata udata4
char MODERAPID;
////// TRAMES IDENTIQUES
char START[1];
char ID1[2];
char ID2[2];
unsigned char FONCTION[3];
char CRC485[5];
char STOP[1];
int pointeur_out_in;
char DEMANDE_INFOS_CAPTEURS[10];
char TYPE_ERREUR[2];
char INFORMATION_ERREUR[20];
char NORECEPTION;
char TYPECARTE[5];
char NBCOM; //BUG ZIZ 

#pragma udata udata5
char TMP[125];  // PR SAV DES TRAMES
char P_RESERVOIR[15];
char P_SORTIE[15];
char P_SECOURS[15];
char POINT_ROSEE[11];
char DEBIT_GLOBAL[11];
char TEMPS_CYCLE_MOTEUR[11];
char TEMPS_MOTEUR[19];
char PRESENCE_220V[6];
char PRESENCE_48V[6];


#pragma udata udata6
unsigned char 	Tab_Horodatage[12];
char STOPMINITEL;
signed int decal=0;
int max_decalage=0;
char acc_u[4];
char acc_conf[4];
char nr_cog[15];
char nr_cms[15];
char passerelle[15];
char DNS[15];
char ID_rack[30];
char T_alarme[3];
char T_sortie[3];
char HORLOGET[12];
char DEMANDE_DE_LAUTO;
char REA_DESACTIVE;
BYTE SERVERIP[4];


#pragma udata udata8
char TRAMEIN[125];
char OUVERTUREIP;
char PASDEDEMANDECONNECTIONTCPIP;
char BOUCLEATTENTE;
unsigned long NUMIP4,NUMIP3,NUMIP2,NUMIP1;
unsigned long NUMDNS4,NUMDNS3,NUMDNS2,NUMDNS1;
unsigned long NUMPASSE4,NUMPASSE3,NUMPASSE2,NUMPASSE1;

#pragma udata udata9
char tableau[60];
//#pragma udata udata10
char CHG_CODAGE;
char NBRECAPTEURS_CABLE;
char type_capteur [13];
char ROB [2];
char COMMENTAIRESAV[20];
unsigned int kolop;


#pragma udata udata10
int repet=0;  //DIRE SI ON  REP. LA PAGE  
unsigned char max_champ=0;
char n_carte;
char  pos_init_cursur[4];
char kk;
char ET_TD[4],ET_GR[4],ET_TP[4],ET_TR[4],ET_I[4],ET_C[4],ET_F[4]; //ALARME,HG,NONREPONSE,INTER,
char groupe[11];
char ETAT_M1[23];
char desig[21];
char pos_capteur[5];
int accord, accordmp;
char REBOOTIP;
char ALARMEATRANS;
char ZPLENVOYEE;
int calculBLOC;
//unsigned char ALARME[102];
char CRC_ETAT;


#pragma udata udata11
unsigned char ALARME[102];
char TYPEORDREALARME;
char TRANSALARM;
char TRANSSORTIEALARM;
char MAJBLOC;
char CRC2021[5];


// Private helper functions.
// These may or may not be present in all applications.
static void InitAppConfig(void);
static void InitializeBoard(void);
static void ProcessIO(void);
#if defined(WF_CS_TRIS)
    void WF_Connect(void);
    #if !defined(MRF24WG)
    extern BOOL gRFModuleVer1209orLater;
    #endif

    #if defined(DERIVE_KEY_FROM_PASSPHRASE_IN_HOST)
    tPassphraseReady g_WpsPassphrase;
    #endif    /* defined(DERIVE_KEY_FROM_PASSPHRASE_IN_HOST) */
#endif

//
// PIC18 Interrupt Service Routines
// 
// NOTE: Several PICs, including the PIC18F4620 revision A3 have a RETFIE FAST/MOVFF bug
// The interruptlow keyword is used to work around the bug when using C18
#if defined(__18CXX)
    #if defined(HI_TECH_C)
    void interrupt low_priority LowISR(void)
    #else
    #pragma interruptlow LowISR
    void LowISR(void)
    #endif
    {
		
        TickUpdate();
    }
    
    #if defined(HI_TECH_C)
    void interrupt HighISR(void)
    #else
    #pragma interruptlow HighISR
    void HighISR(void)
    #endif
    {
        #if defined(STACK_USE_UART2TCP_BRIDGE)
            UART2TCPBridgeISR();
        #endif

        #if defined(WF_CS_TRIS)
            WFEintISR();
        #endif // WF_CS_TRIS
    }

    #if !defined(HI_TECH_C)
    #pragma code lowVector=0x18
    void LowVector(void){_asm goto LowISR _endasm}
    #pragma code highVector=0x8
    void HighVector(void){_asm goto HighISR _endasm}
    #pragma code // Return to default code section
    #endif

// C30 and C32 Exception Handlers
// If your code gets here, you either tried to read or write
// a NULL pointer, or your application overflowed the stack
// by having too many local variables or parameters declared.
#elif defined(__C30__)
    void _ISR __attribute__((__no_auto_psv__)) _AddressError(void)
    {
        Nop();
        Nop();
    }
    void _ISR __attribute__((__no_auto_psv__)) _StackError(void)
    {
        Nop();
        Nop();
    }
    
#elif defined(__C32__)
    void _general_exception_handler(unsigned cause, unsigned status)
    {
        Nop();
        Nop();
    }
#endif

#if defined(WF_CS_TRIS)
// Global variables
UINT8 ConnectionProfileID;
#endif

//
// Main application entry point.
//

#if defined(__18CXX)
void main(void)
#else
int main(void)
#endif
{


    static DWORD t = 0;
    static DWORD dwLastIP = 0;
REBOOTIP=0x00;
ZPLENVOYEE=0;//26102020
DEBUTIP:	
    // Initialize application specific hardware
    InitializeBoard();

    #if defined(USE_LCD)
    // Initialize and display the stack version on the LCD
    //LCDInit();
    DelayMs(100);
    strcpypgm2ram((char*)LCDText, "TCPStack " TCPIP_STACK_VERSION "  "
        "                "); 
    LCDUpdate();
    #endif

    // Initialize stack-related hardware components that may be 
    // required by the UART configuration routines
    TickInit();
    #if defined(STACK_USE_MPFS2)
    MPFSInit();
    #endif

    // Initialize Stack and application related NV variables into AppConfig.
    InitAppConfig();

    // Initiates board setup process if button is depressed 
    // on startup
    if(BUTTON0_IO == 0u)
    {
        #if defined(EEPROM_CS_TRIS) || defined(SPIFLASH_CS_TRIS)
        // Invalidate the EEPROM contents if BUTTON0 is held down for more than 4 seconds
        DWORD StartTime = TickGet();
        LED_PUT(0x00);
                
        while(BUTTON0_IO == 0u)
        {
            if(TickGet() - StartTime > 4*TICK_SECOND)
            {
                #if defined(EEPROM_CS_TRIS)
                XEEBeginWrite(0x0000);
                XEEWrite(0xFF);
                XEEWrite(0xFF);
                XEEEndWrite();
                #elif defined(SPIFLASH_CS_TRIS)
                SPIFlashBeginWrite(0x0000);
                SPIFlashWrite(0xFF);
                SPIFlashWrite(0xFF);
                #endif
                
                #if defined(STACK_USE_UART)
                putrsUART("\r\n\r\nBUTTON0 held for more than 4 seconds.  Default settings restored.\r\n\r\n");
                #endif

                LED_PUT(0x0F);
                while((LONG)(TickGet() - StartTime) <= (LONG)(9*TICK_SECOND/2));
                LED_PUT(0x00);
                while(BUTTON0_IO == 0u);
                Reset();
                break;
            }
        }
        #endif

        #if defined(STACK_USE_UART)
        DoUARTConfig();
        #endif
    }

    // Initialize core stack layers (MAC, ARP, TCP, UDP) and
    // application modules (HTTP, SNMP, etc.)
    StackInit();

    #if defined(WF_CS_TRIS)
    #if defined(DERIVE_KEY_FROM_PASSPHRASE_IN_HOST)
        g_WpsPassphrase.valid = FALSE;
    #endif    /* defined(DERIVE_KEY_FROM_PASSPHRASE_IN_HOST) */
    WF_Connect();
    #endif

    // Initialize any application-specific modules or functions/
    // For this demo application, this only includes the
    // UART 2 TCP Bridge
    #if defined(STACK_USE_UART2TCP_BRIDGE)
    UART2TCPBridgeInit();
    #endif

    #if defined(STACK_USE_ZEROCONF_LINK_LOCAL)
    ZeroconfLLInitialize();
    #endif

    #if defined(STACK_USE_ZEROCONF_MDNS_SD)
    mDNSInitialize(MY_DEFAULT_HOST_NAME);
    mDNSServiceRegister(
        (const char *) "DemoWebServer",    // base name of the service
        "_http._tcp.local",                // type of the service
        80,                                // TCP or UDP port, at which this service is available
        ((const BYTE *)"path=/index.htm"), // TXT info
        1,                                 // auto rename the service when if needed
        NULL,                              // no callback function
        NULL                               // no application context
        );

    mDNSMulticastFilterRegister();            
    #endif

    // Now that all items are initialized, begin the co-operative
    // multitasking loop.  This infinite loop will continuously 
    // execute all stack-related tasks, as well as your own
    // application's functions.  Custom functions should be added
    // at the end of this loop.
    // Note that this is a "co-operative mult-tasking" mechanism
    // where every task performs its tasks (whether all in one shot
    // or part of it) and returns so that other tasks can do their
    // job.
    // If a task needs very long time to do its job, it must be broken
    // down into smaller pieces so that other tasks can have CPU time.


	INITGESTIONCARTEIO ();
	OUVERTUREIP=0x00; //TCPIP NON OUVERT
	//mainio();
    while(1)
    {
        // Blink LED0 (right most one) every second.
        if(TickGet() - t >= TICK_SECOND/2ul)
        {
            t = TickGet();
            LED0_IO ^= 1;
        }

        // This task performs normal stack task including checking
        // for incoming packet, type of packet and calling
        // appropriate stack entity to process it.
        StackTask();
        
        #if defined(WF_CS_TRIS)
        #if !defined(MRF24WG)
        if (gRFModuleVer1209orLater)
        #endif
            WiFiTask();
        #endif

        // This tasks invokes each of the core stack application tasks
        StackApplications();

        #if defined(STACK_USE_ZEROCONF_LINK_LOCAL)
        ZeroconfLLProcess();
        #endif

        #if defined(STACK_USE_ZEROCONF_MDNS_SD)
        mDNSProcess();
        // Use this function to exercise service update function
        // HTTPUpdateRecord();
        #endif

        // Process application specific tasks here.
        // For this demo app, this will include the Generic TCP 
        // client and servers, and the SNMP, Ping, and SNMP Trap
        // demos.  Following that, we will process any IO from
        // the inputs on the board itself.
        // Any custom modules or processing you need to do should
        // go here.
        #if defined(STACK_USE_GENERIC_TCP_CLIENT_EXAMPLE)
       // GenericTCPClient(); //pour transmettre alarme � TELGAT
		client ();






	      #endif
        
        #if defined(STACK_USE_GENERIC_TCP_SERVER_EXAMPLE)
	//	A1707: 
	
		//	ATTENDRERECEPTIONIP==0xAA; // MODE DETECTION PUIS RECPETION
			//					    ecouteTCPIP();
//TEMPO(2);
	//							GenericTCPServer();
						 		//if (PASDEDEMANDECONNECTIONTCPIP==0xFF) // connection  detect�e
								//	{
//									TRAMEOUT[0]='#';
									//ATTENDRERECEPTIONIP==0xFF; // ON A EU UNE coonec donc on receptionne
									//FAIRE ARRETER LA CARTE MERE	
									//goto STOPPERZIZ;		
								//	}
//goto A1707;
        //GenericTCPServer(); //pour INTERROGATION TELGAT
//ATTENDRERECEPTIONIP=0xA8;
//TEMPO(3); //ATTENDRE 30 ICI CAR ON ATTEND 20 DANS LA CM DE NON REPONSE AVANT DE FAIRE LES DIC
//ecouteTCPIP();
//TEMPO(3); //ATTENDRE 30 ICI CAR ON ATTEND 20 DANS LA CM DE NON REPONSE AVANT DE FAIRE LES DIC
//TEMPO(3); //ATTENDRE 30 ICI CAR ON ATTEND 20 DANS LA CM DE NON REPONSE AVANT DE FAIRE LES DIC
//ecouteTCPIP();
//TEMPO(3); //ATTENDRE 30 ICI CAR ON ATTEND 20 DANS LA CM DE NON REPONSE AVANT DE FAIRE LES DIC
//ecouteTCPIP();
//TEMPO(3); //ATTENDRE 30 ICI CAR ON ATTEND 20 DANS LA CM DE NON REPONSE AVANT DE FAIRE LES DIC
//ecouteTCPIP();

        #endif
        
        #if defined(STACK_USE_SMTP_CLIENT)
        SMTPDemo();
        #endif
        
        #if defined(STACK_USE_ICMP_CLIENT)
        PingDemo();
        #endif

		#if defined(STACK_USE_TFTP_CLIENT) && defined(WF_CS_TRIS)	
		TFTPGetUploadStatus();
		#endif
        
        #if defined(STACK_USE_SNMP_SERVER) && !defined(SNMP_TRAP_DISABLED)
        //User should use one of the following SNMP demo
        // This routine demonstrates V1 or V2 trap formats with one variable binding.
        SNMPTrapDemo();
        
        #if defined(SNMP_STACK_USE_V2_TRAP) || defined(SNMP_V1_V2_TRAP_WITH_SNMPV3)
        //This routine provides V2 format notifications with multiple (3) variable bindings
        //User should modify this routine to send v2 trap format notifications with the required varbinds.
        //SNMPV2TrapDemo();
        #endif 
        if(gSendTrapFlag)
            SNMPSendTrap();
        #endif
        
        #if defined(STACK_USE_BERKELEY_API)
            BerkeleyTCPClientDemo();
            BerkeleyTCPServerDemo();
            BerkeleyUDPClientDemo();
            #endif

		ProcessIO();

        // If the local IP address has changed (ex: due to DHCP lease change)
        // write the new IP address to the LCD display, UART, and Announce 
        // service
        if(dwLastIP != AppConfig.MyIPAddr.Val)
        {
            dwLastIP = AppConfig.MyIPAddr.Val;
            
            #if defined(STACK_USE_UART)
                putrsUART((ROM char*)"\r\nNew IP Address: ");
            #endif

            DisplayIPValue(AppConfig.MyIPAddr);

            #if defined(STACK_USE_UART)
                putrsUART((ROM char*)"\r\n");
            #endif


            #if defined(STACK_USE_ANNOUNCE)
                AnnounceIP();
            #endif

            #if defined(STACK_USE_ZEROCONF_MDNS_SD)
                mDNSFillHostRecord();
            #endif

			#ifdef WIFI_NET_TEST	
			#ifdef STACK_USE_TFTP_CLIENT
			if(AppConfig.Flags.bIsDHCPEnabled && DHCPIsBound(0)) {	
				static UINT8  tftpInitDone = 0;
				static BYTE dummy_file[] = "TFTP test dummy contents";
				static ROM BYTE file_name[] = "dontcare";
				static ROM BYTE host_name[] = "tftp" WIFI_NET_TEST_DOMAIN;	
				if (!tftpInitDone) {
					TFTPUploadRAMFileToHost(host_name, file_name, dummy_file, sizeof(dummy_file));
					tftpInitDone = 1;
				}
			}
			#endif
			#endif
			
        }    
        #if defined(DERIVE_KEY_FROM_PASSPHRASE_IN_HOST) && defined (MRF24WG)
            if (g_WpsPassphrase.valid) {
                WF_ConvPassphrase2Key(g_WpsPassphrase.passphrase.keyLen, g_WpsPassphrase.passphrase.key,
                g_WpsPassphrase.passphrase.ssidLen, g_WpsPassphrase.passphrase.ssid);
                WF_SetPSK(g_WpsPassphrase.passphrase.key);
                g_WpsPassphrase.valid = FALSE;
            }
        #endif    /* defined(DERIVE_KEY_FROM_PASSPHRASE_IN_HOST) */
		#if defined(STACK_USE_AUTOUPDATE_HTTPSERVER) && defined(WF_CS_TRIS) && defined(MRF24WG)
		{
			static DWORD t_UpdateImage=0;
			extern UINT8 Flag_ImageUpdate_running;
			if(Flag_ImageUpdate_running == 1)
			{
				UINT8 buf_command[4];
				if( (TickGet() - t_UpdateImage) >= TICK_SECOND * 120ul)
				{
					putsUART((char *)"Update Firmware timeout, begin to restore..\r\n");
					buf_command[0]=UPDATE_CMD_ERASE0; //Erase bank0
					buf_command[1]=UPDATE_SERITY_KEY_1; 
					buf_command[2]=UPDATE_SERITY_KEY_2; 
					buf_command[3]=UPDATE_SERITY_KEY_3; 
					SendSetParamMsg(PARAM_FLASH_update, buf_command, 4);
					buf_command[0]=UPDATE_CMD_CPY_1TO0; //Copy bank1 to bank0
					buf_command[1]=UPDATE_SERITY_KEY_1; 
					buf_command[2]=UPDATE_SERITY_KEY_2; 
					buf_command[3]=UPDATE_SERITY_KEY_3; 
					SendSetParamMsg(PARAM_FLASH_update, buf_command, 4);
					putsUART((char *)"restore Done\r\n");
					Flag_ImageUpdate_running = 0;
				}
					
			}
			else
			{
				t_UpdateImage = TickGet();
			}
		}
		#endif
//do
//{
//GenericTCPServer();
//TEMPO(1);
//} while (1);	
//GenericTCPServer();
//ecouteTCPIP();
	mainio();
//	goto DEBUTIP;	


    }
}

#if defined(WF_CS_TRIS)

/*****************************************************************************
 * FUNCTION: WF_Connect
 *
 * RETURNS:  None
 *
 * PARAMS:   None
 *
 *  NOTES:   Connects to an 802.11 network.  Customize this function as needed 
 *           for your application.
 *****************************************************************************/
void WF_Connect(void)
{
    UINT8 channelList[] = MY_DEFAULT_CHANNEL_LIST;
 
    /* create a Connection Profile */
    WF_CPCreate(&ConnectionProfileID);
    
    WF_SetRegionalDomain(MY_DEFAULT_DOMAIN);  

    WF_CPSetSsid(ConnectionProfileID, 
                 AppConfig.MySSID, 
                 AppConfig.SsidLength);
    
    WF_CPSetNetworkType(ConnectionProfileID, MY_DEFAULT_NETWORK_TYPE);
    
    WF_CASetScanType(MY_DEFAULT_SCAN_TYPE);
    
    
    WF_CASetChannelList(channelList, sizeof(channelList));
    
    // The Retry Count parameter tells the WiFi Connection manager how many attempts to make when trying
    // to connect to an existing network.  In the Infrastructure case, the default is to retry forever so that
    // if the AP is turned off or out of range, the radio will continue to attempt a connection until the
    // AP is eventually back on or in range.  In the Adhoc case, the default is to retry 3 times since the 
    // purpose of attempting to establish a network in the Adhoc case is only to verify that one does not
    // initially exist.  If the retry count was set to WF_RETRY_FOREVER in the AdHoc mode, an AdHoc network
    // would never be established. 
    WF_CASetListRetryCount(MY_DEFAULT_LIST_RETRY_COUNT);

    WF_CASetEventNotificationAction(MY_DEFAULT_EVENT_NOTIFICATION_LIST);
    
    WF_CASetBeaconTimeout(MY_DEFAULT_BEACON_TIMEOUT);
    
    #if !defined(MRF24WG)
        if (gRFModuleVer1209orLater)
    #else
        {
            // If WEP security is used, set WEP Key Type.  The default WEP Key Type is Shared Key.
            if (AppConfig.SecurityMode == WF_SECURITY_WEP_40 || AppConfig.SecurityMode == WF_SECURITY_WEP_104)
            {
                WF_CPSetWepKeyType(ConnectionProfileID, MY_DEFAULT_WIFI_SECURITY_WEP_KEYTYPE);
            }
        }    
    #endif
            
    #if defined(MRF24WG)
        // Error check items specific to WPS Push Button mode 
        #if (MY_DEFAULT_WIFI_SECURITY_MODE==WF_SECURITY_WPS_PUSH_BUTTON)
            #if !defined(WF_P2P)
                WF_ASSERT(strlen(AppConfig.MySSID) == 0);  // SSID must be empty when using WPS
                WF_ASSERT(sizeof(channelList)==11);        // must scan all channels for WPS       
            #endif

             #if (MY_DEFAULT_NETWORK_TYPE == WF_P2P)
                WF_ASSERT(strcmp((char *)AppConfig.MySSID, "DIRECT-") == 0);
                WF_ASSERT(sizeof(channelList) == 3);
                WF_ASSERT(channelList[0] == 1);
                WF_ASSERT(channelList[1] == 6);
                WF_ASSERT(channelList[2] == 11);           
            #endif
        #endif    

    #endif /* MRF24WG */

    #if defined(DERIVE_KEY_FROM_PASSPHRASE_IN_HOST)
        if (AppConfig.SecurityMode == WF_SECURITY_WPA_WITH_PASS_PHRASE
            || AppConfig.SecurityMode == WF_SECURITY_WPA2_WITH_PASS_PHRASE
            || AppConfig.SecurityMode == WF_SECURITY_WPA_AUTO_WITH_PASS_PHRASE) {
            WF_ConvPassphrase2Key(AppConfig.SecurityKeyLength, AppConfig.SecurityKey,
                AppConfig.SsidLength, AppConfig.MySSID);
            AppConfig.SecurityMode--;
            AppConfig.SecurityKeyLength = 32;
        }
    #if defined (MRF24WG)
        else if (AppConfig.SecurityMode == WF_SECURITY_WPS_PUSH_BUTTON
                    || AppConfig.SecurityMode == WF_SECURITY_WPS_PIN) {
            WF_YieldPassphrase2Host();    
        }
    #endif    /* defined (MRF24WG) */
    #endif    /* defined(DERIVE_KEY_FROM_PASSPHRASE_IN_HOST) */
		#if !defined(MRF24WG)	
		Delay10us(10);  //give time to Roadrunner to clean message buffer, because Security message is a big data package
		#endif
    WF_CPSetSecurity(ConnectionProfileID,
                     AppConfig.SecurityMode,
                     AppConfig.WepKeyIndex,   /* only used if WEP enabled */
                     AppConfig.SecurityKey,
                     AppConfig.SecurityKeyLength);

    #if MY_DEFAULT_PS_POLL == WF_ENABLED
        WF_PsPollEnable(TRUE);
    #if !defined(MRF24WG) 
        if (gRFModuleVer1209orLater)
            WFEnableDeferredPowerSave();
    #endif    /* !defined(MRF24WG) */
    #else     /* MY_DEFAULT_PS_POLL != WF_ENABLED */
        WF_PsPollDisable();
    #endif    /* MY_DEFAULT_PS_POLL == WF_ENABLED */

    #ifdef WF_AGGRESSIVE_PS
    #if !defined(MRF24WG)
        if (gRFModuleVer1209orLater)
            WFEnableAggressivePowerSave();
    #endif
    #endif
    
    #if defined(STACK_USE_UART)  
        WF_OutputConnectionInfo(&AppConfig);
    #endif
    
    #if defined(DISABLE_MODULE_FW_CONNECT_MANAGER_IN_INFRASTRUCTURE)
        WF_DisableModuleConnectionManager();
    #endif
    
    #if defined(MRF24WG)
        WFEnableDebugPrint(ENABLE_WPS_PRINTS | ENABLE_P2P_PRINTS);
    #endif
    WF_CMConnect(ConnectionProfileID);
}   
#endif /* WF_CS_TRIS */

// Writes an IP address to the LCD display and the UART as available
void DisplayIPValue(IP_ADDR IPVal)
{
// printf("%u.%u.%u.%u", IPVal.v[0], IPVal.v[1], IPVal.v[2], IPVal.v[3]);
#if defined (__dsPIC33E__) || defined (__PIC24E__)
    static BYTE IPDigit[4];                        /* Needs to be declared as static to avoid the array getting optimized by C30 v3.30 compiler for dsPIC33E/PIC24E. 
                                                   Otherwise the LCD displays corrupted IP address on Explorer 16. To be fixed in the future compiler release*/
#else
    BYTE IPDigit[4];
#endif
    BYTE i;
#ifdef USE_LCD
    BYTE j;
    BYTE LCDPos=16;
#endif

    for(i = 0; i < sizeof(IP_ADDR); i++)
    {
        uitoa((WORD)IPVal.v[i], IPDigit);

        #if defined(STACK_USE_UART)
            putsUART((char *) IPDigit);
        #endif

        #ifdef USE_LCD
            for(j = 0; j < strlen((char*)IPDigit); j++)
            {
                LCDText[LCDPos++] = IPDigit[j];
            }
            if(i == sizeof(IP_ADDR)-1)
                break;
            LCDText[LCDPos++] = '.';
        #else
            if(i == sizeof(IP_ADDR)-1)
                break;
        #endif

        #if defined(STACK_USE_UART)
            while(BusyUART());
            WriteUART('.');
        #endif
    }

    #ifdef USE_LCD
        if(LCDPos < 32u)
            LCDText[LCDPos] = 0;
        LCDUpdate();
    #endif
}

// Processes A/D data from the potentiometer
static void ProcessIO(void)
{
#if defined(__C30__) || defined(__C32__)
    // Convert potentiometer result into ASCII string
    uitoa((WORD)ADC1BUF0, AN0String);
#else
    // AN0 should already be set up as an analog input
    ADCON0bits.GO = 1;

    // Wait until A/D conversion is done
    while(ADCON0bits.GO);

    // AD converter errata work around (ex: PIC18F87J10 A2)
    #if !defined(__18F87J50) && !defined(_18F87J50) && !defined(__18F87J11) && !defined(_18F87J11) 
    {
        BYTE temp = ADCON2;
        ADCON2 |= 0x7;    // Select Frc mode by setting ADCS0/ADCS1/ADCS2
        ADCON2 = temp;
    }
    #endif

    // Convert 10-bit value into ASCII string
    uitoa(*((WORD*)(&ADRESL)), AN0String);
#endif
}


/****************************************************************************
  Function:
    static void InitializeBoard(void)

  Description:
    This routine initializes the hardware.  It is a generic initialization
    routine for many of the Microchip development boards, using definitions
    in HardwareProfile.h to determine specific initialization.

  Precondition:
    None

  Parameters:
    None - None

  Returns:
    None

  Remarks:
    None
  ***************************************************************************/
static void InitializeBoard(void)
{    
    // LEDs
    LED0_TRIS = 0;
    LED1_TRIS = 0;
    LED2_TRIS = 0;
    LED3_TRIS = 0;
    LED4_TRIS = 0;
    LED5_TRIS = 0;
    LED6_TRIS = 0;
    LED7_TRIS = 0;
    LED_PUT(0x00);

#if defined(__18CXX)
    // Enable 4x/5x/96MHz PLL on PIC18F87J10, PIC18F97J60, PIC18F87J50, etc.
    OSCTUNE = 0x40;

    // Set up analog features of PORTA

    // PICDEM.net 2 board has POT on AN2, Temp Sensor on AN3
    #if defined(PICDEMNET2)
        ADCON0 = 0x09;        // ADON, Channel 2
        ADCON1 = 0x0B;        // Vdd/Vss is +/-REF, AN0, AN1, AN2, AN3 are analog
    #elif defined(PICDEMZ)
        ADCON0 = 0x81;        // ADON, Channel 0, Fosc/32
        ADCON1 = 0x0F;        // Vdd/Vss is +/-REF, AN0, AN1, AN2, AN3 are all digital
    #elif defined(__18F87J11) || defined(_18F87J11) || defined(__18F87J50) || defined(_18F87J50)
        ADCON0 = 0x01;        // ADON, Channel 0, Vdd/Vss is +/-REF
        WDTCONbits.ADSHR = 1;
        ANCON0 = 0xFC;        // AN0 (POT) and AN1 (temp sensor) are anlog
        ANCON1 = 0xFF;
        WDTCONbits.ADSHR = 0;        
    #else
        ADCON0 = 0x01;        // ADON, Channel 0
        ADCON1 = 0x0E;        // Vdd/Vss is +/-REF, AN0 is analog
    #endif
    ADCON2 = 0xBE;            // Right justify, 20TAD ACQ time, Fosc/64 (~21.0kHz)


    // Enable internal PORTB pull-ups
    INTCON2bits.RBPU = 0;

    // Configure USART
    TXSTA = 0x20;
    RCSTA = 0x90;

    // See if we can use the high baud rate setting
    #if ((GetPeripheralClock()+2*BAUD_RATE)/BAUD_RATE/4 - 1) <= 255
        SPBRG = (GetPeripheralClock()+2*BAUD_RATE)/BAUD_RATE/4 - 1;
        TXSTAbits.BRGH = 1;
    #else    // Use the low baud rate setting
        SPBRG = (GetPeripheralClock()+8*BAUD_RATE)/BAUD_RATE/16 - 1;
    #endif


    // Enable Interrupts
    RCONbits.IPEN = 1;        // Enable interrupt priorities
    INTCONbits.GIEH = 1;
    INTCONbits.GIEL = 1;

    // Do a calibration A/D conversion
    #if defined(__18F87J10) || defined(__18F86J15) || defined(__18F86J10) || defined(__18F85J15) || defined(__18F85J10) || defined(__18F67J10) || defined(__18F66J15) || defined(__18F66J10) || defined(__18F65J15) || defined(__18F65J10) || defined(__18F97J60) || defined(__18F96J65) || defined(__18F96J60) || defined(__18F87J60) || defined(__18F86J65) || defined(__18F86J60) || defined(__18F67J60) || defined(__18F66J65) || defined(__18F66J60) || \
         defined(_18F87J10) ||  defined(_18F86J15) || defined(_18F86J10)  ||  defined(_18F85J15) ||  defined(_18F85J10) ||  defined(_18F67J10) ||  defined(_18F66J15) ||  defined(_18F66J10) ||  defined(_18F65J15) ||  defined(_18F65J10) ||  defined(_18F97J60) ||  defined(_18F96J65) ||  defined(_18F96J60) ||  defined(_18F87J60) ||  defined(_18F86J65) ||  defined(_18F86J60) ||  defined(_18F67J60) ||  defined(_18F66J65) ||  defined(_18F66J60)
        ADCON0bits.ADCAL = 1;
        ADCON0bits.GO = 1;
        while(ADCON0bits.GO);
        ADCON0bits.ADCAL = 0;
    #elif defined(__18F87J11) || defined(__18F86J16) || defined(__18F86J11) || defined(__18F67J11) || defined(__18F66J16) || defined(__18F66J11) || \
           defined(_18F87J11) ||  defined(_18F86J16) ||  defined(_18F86J11) ||  defined(_18F67J11) ||  defined(_18F66J16) ||  defined(_18F66J11) || \
          defined(__18F87J50) || defined(__18F86J55) || defined(__18F86J50) || defined(__18F67J50) || defined(__18F66J55) || defined(__18F66J50) || \
           defined(_18F87J50) ||  defined(_18F86J55) ||  defined(_18F86J50) ||  defined(_18F67J50) ||  defined(_18F66J55) ||  defined(_18F66J50)
        ADCON1bits.ADCAL = 1;
        ADCON0bits.GO = 1;
        while(ADCON0bits.GO);
        ADCON1bits.ADCAL = 0;
    #endif

#else    // 16-bit C30 and and 32-bit C32
    #if defined(__PIC32MX__)
    {
        // Enable multi-vectored interrupts
        INTEnableSystemMultiVectoredInt();
        
        // Enable optimal performance
        SYSTEMConfigPerformance(GetSystemClock());
        mOSCSetPBDIV(OSC_PB_DIV_1);                // Use 1:1 CPU Core:Peripheral clocks
        
        // Disable JTAG port so we get our I/O pins back, but first
        // wait 50ms so if you want to reprogram the part with 
        // JTAG, you'll still have a tiny window before JTAG goes away.
        // The PIC32 Starter Kit debuggers use JTAG and therefore must not 
        // disable JTAG.
        DelayMs(50);
        #if !defined(__MPLAB_DEBUGGER_PIC32MXSK) && !defined(__MPLAB_DEBUGGER_FS2)
            DDPCONbits.JTAGEN = 0;
        #endif
        LED_PUT(0x00);                // Turn the LEDs off
        
        CNPUESET = 0x00098000;        // Turn on weak pull ups on CN15, CN16, CN19 (RD5, RD7, RD13), which is connected to buttons on PIC32 Starter Kit boards
    }
    #endif

    #if defined(__dsPIC33F__) || defined(__PIC24H__)
        // Crank up the core frequency
        PLLFBD = 38;                // Multiply by 40 for 160MHz VCO output (8MHz XT oscillator)
        CLKDIV = 0x0000;            // FRC: divide by 2, PLLPOST: divide by 2, PLLPRE: divide by 2
    
        // Port I/O
        AD1PCFGHbits.PCFG23 = 1;    // Make RA7 (BUTTON1) a digital input
        AD1PCFGHbits.PCFG20 = 1;    // Make RA12 (INT1) a digital input for MRF24W PICtail Plus interrupt

        // ADC
        AD1CHS0 = 0;                // Input to AN0 (potentiometer)
        AD1PCFGLbits.PCFG5 = 0;     // Disable digital input on AN5 (potentiometer)
        AD1PCFGLbits.PCFG4 = 0;     // Disable digital input on AN4 (TC1047A temp sensor)


    #elif defined(__dsPIC33E__)||defined(__PIC24E__)

        // Crank up the core frequency
        PLLFBD = 38;                /* M  = 30   */
        CLKDIVbits.PLLPOST = 0;     /* N1 = 2    */
        CLKDIVbits.PLLPRE = 0;      /* N2 = 2    */
        OSCTUN = 0;    
        
        /*    Initiate Clock Switch to Primary
         *    Oscillator with PLL (NOSC= 0x3)*/
        __builtin_write_OSCCONH(0x03);        
        __builtin_write_OSCCONL(0x01);
        // Disable Watch Dog Timer
        RCONbits.SWDTEN = 0;
        while (OSCCONbits.COSC != 0x3); 
        while (_LOCK == 0);            /* Wait for PLL lock at 60 MIPS */        
        // Port I/O
        ANSELAbits.ANSA7 = 0 ;   //Make RA7 (BUTTON1) a digital input
        #if defined ENC100_INTERFACE_MODE > 0
            ANSELEbits.ANSE0 = 0;      // Make these PMP pins as digital output when the interface is parallel.
            ANSELEbits.ANSE1 = 0;
            ANSELEbits.ANSE2 = 0;
            ANSELEbits.ANSE3 = 0;    
            ANSELEbits.ANSE4 = 0;
            ANSELEbits.ANSE5 = 0;
            ANSELEbits.ANSE6 = 0;
            ANSELEbits.ANSE7 = 0;    
            ANSELBbits.ANSB10 = 0;
            ANSELBbits.ANSB11 = 0;
            ANSELBbits.ANSB12 = 0;
            ANSELBbits.ANSB13 = 0;
            ANSELBbits.ANSB15 = 0;
        #endif

        ANSELEbits.ANSE8= 0 ;    // Make RE8(INT1) a digital input for ZeroG ZG2100M PICtail 
                    
        AD1CHS0 = 0;             // Input to AN0 (potentiometer)
        ANSELBbits.ANSB0= 1;     // Input to AN0 (potentiometer)
        ANSELBbits.ANSB5= 1;     // Disable digital input on AN5 (potentiometer)
        ANSELBbits.ANSB4= 1;     // Disable digital input on AN4 (TC1047A temp sensor)
                    
        ANSELDbits.ANSD7 =0;     //  Digital Pin Selection for S3(Pin 83) and S4(pin 84).
        ANSELDbits.ANSD6 =0;
            
        ANSELGbits.ANSG6 =0;     // Enable Digital input for RG6 (SCK2)
        ANSELGbits.ANSG7 =0;     // Enable Digital input for RG7 (SDI2)
        ANSELGbits.ANSG8 =0;     // Enable Digital input for RG8 (SDO2)
        ANSELGbits.ANSG9 =0;     // Enable Digital input for RG9 (CS)
                
        #if defined ENC100_INTERFACE_MODE == 0    // SPI Interface, UART can be used for debugging. Not allowed for other interfaces.
            RPOR9 = 0x0300;          //RP101= U2TX
            RPINR19 = 0X0064;        //RP100= U2RX
        #endif

        #if defined WF_CS_TRIS
            RPINR1bits.INT3R = 30;
            WF_CS_IO = 1;
            WF_CS_TRIS = 0;        

        #endif

    #else    //defined(__PIC24F__) || defined(__PIC32MX__)
        #if defined(__PIC24F__)
            CLKDIVbits.RCDIV = 0;        // Set 1:1 8MHz FRC postscalar
        #endif
        
        // ADC
        #if defined(__PIC24FJ256DA210__) || defined(__PIC24FJ256GB210__)
            // Disable analog on all pins
            ANSA = 0x0000;
            ANSB = 0x0000;
            ANSC = 0x0000;
            ANSD = 0x0000;
            ANSE = 0x0000;
            ANSF = 0x0000;
            ANSG = 0x0000;
        #else
            AD1CHS = 0;                   // Input to AN0 (potentiometer)
            AD1PCFGbits.PCFG4 = 0;        // Disable digital input on AN4 (TC1047A temp sensor)
            #if defined(__32MX460F512L__) || defined(__32MX795F512L__)    // PIC32MX460F512L and PIC32MX795F512L PIMs has different pinout to accomodate USB module
                AD1PCFGbits.PCFG2 = 0;    // Disable digital input on AN2 (potentiometer)
            #else
                AD1PCFGbits.PCFG5 = 0;    // Disable digital input on AN5 (potentiometer)
            #endif
        #endif
    #endif

    // ADC
    AD1CON1 = 0x84E4;            // Turn on, auto sample start, auto-convert, 12 bit mode (on parts with a 12bit A/D)
    AD1CON2 = 0x0404;            // AVdd, AVss, int every 2 conversions, MUXA only, scan
    AD1CON3 = 0x1003;            // 16 Tad auto-sample, Tad = 3*Tcy
    #if defined(__32MX460F512L__) || defined(__32MX795F512L__)    // PIC32MX460F512L and PIC32MX795F512L PIMs has different pinout to accomodate USB module
        AD1CSSL = 1<<2;                // Scan pot
    #else
        AD1CSSL = 1<<5;                // Scan pot
    #endif

    // UART
    #if defined(STACK_USE_UART)

        #if defined(__PIC24E__) || defined(__dsPIC33E__)
            #if defined (ENC_CS_IO) || defined (WF_CS_IO)   // UART to be used in case of ENC28J60 or MRF24W
                __builtin_write_OSCCONL(OSCCON & 0xbf);
                RPOR9bits.RP101R = 3; //Map U2TX to RF5
                RPINR19bits.U2RXR = 0;
                RPINR19bits.U2RXR = 0x64; //Map U2RX to RF4
                __builtin_write_OSCCONL(OSCCON | 0x40);
            #endif
            #if(ENC100_INTERFACE_MODE == 0)                 // UART to be used only in case of SPI interface with ENC624Jxxx
                    __builtin_write_OSCCONL(OSCCON & 0xbf);
                RPOR9bits.RP101R = 3; //Map U2TX to RF5
                RPINR19bits.U2RXR = 0;
                RPINR19bits.U2RXR = 0x64; //Map U2RX to RF4
                __builtin_write_OSCCONL(OSCCON | 0x40);

            #endif
        #endif

        UARTTX_TRIS = 0;
        UARTRX_TRIS = 1;
        UMODE = 0x8000;            // Set UARTEN.  Note: this must be done before setting UTXEN

        #if defined(__C30__)
            USTA = 0x0400;        // UTXEN set
            #define CLOSEST_UBRG_VALUE ((GetPeripheralClock()+8ul*BAUD_RATE)/16/BAUD_RATE-1)
            #define BAUD_ACTUAL (GetPeripheralClock()/16/(CLOSEST_UBRG_VALUE+1))
        #else    //defined(__C32__)
            USTA = 0x00001400;        // RXEN set, TXEN set
            #define CLOSEST_UBRG_VALUE ((GetPeripheralClock()+8ul*BAUD_RATE)/16/BAUD_RATE-1)
            #define BAUD_ACTUAL (GetPeripheralClock()/16/(CLOSEST_UBRG_VALUE+1))
        #endif
    
        #define BAUD_ERROR ((BAUD_ACTUAL > BAUD_RATE) ? BAUD_ACTUAL-BAUD_RATE : BAUD_RATE-BAUD_ACTUAL)
        #define BAUD_ERROR_PRECENT    ((BAUD_ERROR*100+BAUD_RATE/2)/BAUD_RATE)
        #if (BAUD_ERROR_PRECENT > 3)
            #warning UART frequency error is worse than 3%
        #elif (BAUD_ERROR_PRECENT > 2)
            #warning UART frequency error is worse than 2%
        #endif
    
        UBRG = CLOSEST_UBRG_VALUE;
    #endif

#endif

// Deassert all chip select lines so there isn't any problem with 
// initialization order.  Ex: When ENC28J60 is on SPI2 with Explorer 16, 
// MAX3232 ROUT2 pin will drive RF12/U2CTS ENC28J60 CS line asserted, 
// preventing proper 25LC256 EEPROM operation.
#if defined(ENC_CS_TRIS)
    ENC_CS_IO = 1;
    ENC_CS_TRIS = 0;
#endif
#if defined(ENC100_CS_TRIS)
    ENC100_CS_IO = (ENC100_INTERFACE_MODE == 0);
    ENC100_CS_TRIS = 0;
#endif
#if defined(EEPROM_CS_TRIS)
    EEPROM_CS_IO = 1;
    EEPROM_CS_TRIS = 0;
#endif
#if defined(SPIRAM_CS_TRIS)
    SPIRAM_CS_IO = 1;
    SPIRAM_CS_TRIS = 0;
#endif
#if defined(SPIFLASH_CS_TRIS)
    SPIFLASH_CS_IO = 1;
    SPIFLASH_CS_TRIS = 0;
#endif
#if defined(WF_CS_TRIS)
    WF_CS_IO = 1;
    WF_CS_TRIS = 0;
#endif

#if defined(PIC24FJ64GA004_PIM)
    __builtin_write_OSCCONL(OSCCON & 0xBF);  // Unlock PPS

    // Remove some LED outputs to regain other functions
    LED1_TRIS = 1;        // Multiplexed with BUTTON0
    LED5_TRIS = 1;        // Multiplexed with EEPROM CS
    LED7_TRIS = 1;        // Multiplexed with BUTTON1
    
    // Inputs
    RPINR19bits.U2RXR = 19;            //U2RX = RP19
    RPINR22bits.SDI2R = 20;            //SDI2 = RP20
    RPINR20bits.SDI1R = 17;            //SDI1 = RP17
    
    // Outputs
    RPOR12bits.RP25R = U2TX_IO;        //RP25 = U2TX  
    RPOR12bits.RP24R = SCK2OUT_IO;     //RP24 = SCK2
    RPOR10bits.RP21R = SDO2_IO;        //RP21 = SDO2
    RPOR7bits.RP15R = SCK1OUT_IO;      //RP15 = SCK1
    RPOR8bits.RP16R = SDO1_IO;         //RP16 = SDO1
    
    AD1PCFG = 0xFFFF;                  //All digital inputs - POT and Temp are on same pin as SDO1/SDI1, which is needed for ENC28J60 commnications

    __builtin_write_OSCCONL(OSCCON | 0x40); // Lock PPS
#endif

#if defined(__PIC24FJ256DA210__)
    __builtin_write_OSCCONL(OSCCON & 0xBF);  // Unlock PPS

    // Inputs
    RPINR19bits.U2RXR = 11;   // U2RX = RP11
    RPINR20bits.SDI1R = 0;    // SDI1 = RP0
    RPINR0bits.INT1R = 34;    // Assign RE9/RPI34 to INT1 (input) for MRF24W Wi-Fi PICtail Plus interrupt
    
    // Outputs
    RPOR8bits.RP16R = 5;    // RP16 = U2TX
    RPOR1bits.RP2R = 8;     // RP2 = SCK1
    RPOR0bits.RP1R = 7;     // RP1 = SDO1

    __builtin_write_OSCCONL(OSCCON | 0x40); // Lock PPS
#endif

#if defined(__PIC24FJ256GB110__) || defined(__PIC24FJ256GB210__)
    __builtin_write_OSCCONL(OSCCON & 0xBF);  // Unlock PPS
    
    // Configure SPI1 PPS pins (ENC28J60/ENCX24J600/MRF24W or other PICtail Plus cards)
    RPOR0bits.RP0R = 8;         // Assign RP0 to SCK1 (output)
    RPOR7bits.RP15R = 7;        // Assign RP15 to SDO1 (output)
    RPINR20bits.SDI1R = 23;     // Assign RP23 to SDI1 (input)

    // Configure SPI2 PPS pins (25LC256 EEPROM on Explorer 16)
    RPOR10bits.RP21R = 11;      // Assign RG6/RP21 to SCK2 (output)
    RPOR9bits.RP19R = 10;       // Assign RG8/RP19 to SDO2 (output)
    RPINR22bits.SDI2R = 26;     // Assign RG7/RP26 to SDI2 (input)
    
    // Configure UART2 PPS pins (MAX3232 on Explorer 16)
    #if !defined(ENC100_INTERFACE_MODE) || (ENC100_INTERFACE_MODE == 0) || defined(ENC100_PSP_USE_INDIRECT_RAM_ADDRESSING)
    RPINR19bits.U2RXR = 10;     // Assign RF4/RP10 to U2RX (input)
    RPOR8bits.RP17R = 5;        // Assign RF5/RP17 to U2TX (output)
    #endif
    
    // Configure INT1 PPS pin (MRF24W Wi-Fi PICtail Plus interrupt signal when in SPI slot 1)
    RPINR0bits.INT1R = 33;    // Assign RE8/RPI33 to INT1 (input)

    // Configure INT3 PPS pin (MRF24W Wi-Fi PICtail Plus interrupt signal when in SPI slot 2)
    RPINR1bits.INT3R = 40;    // Assign RC3/RPI40 to INT3 (input)

    __builtin_write_OSCCONL(OSCCON | 0x40); // Lock PPS
#endif

#if defined(__PIC24FJ256GA110__)
    __builtin_write_OSCCONL(OSCCON & 0xBF);  // Unlock PPS
    
    // Configure SPI2 PPS pins (25LC256 EEPROM on Explorer 16 and ENC28J60/ENCX24J600/MRF24W or other PICtail Plus cards)
    // Note that the ENC28J60/ENCX24J600/MRF24W PICtails SPI PICtails must be inserted into the middle SPI2 socket, not the topmost SPI1 slot as normal.  This is because PIC24FJ256GA110 A3 silicon has an input-only RPI PPS pin in the ordinary SCK1 location.  Silicon rev A5 has this fixed, but for simplicity all demos will assume we are using SPI2.
    RPOR10bits.RP21R = 11;      // Assign RG6/RP21 to SCK2 (output)
    RPOR9bits.RP19R = 10;       // Assign RG8/RP19 to SDO2 (output)
    RPINR22bits.SDI2R = 26;     // Assign RG7/RP26 to SDI2 (input)
    
    // Configure UART2 PPS pins (MAX3232 on Explorer 16)
    RPINR19bits.U2RXR = 10;     // Assign RF4/RP10 to U2RX (input)
    RPOR8bits.RP17R = 5;        // Assign RF5/RP17 to U2TX (output)
    
    // Configure INT3 PPS pin (MRF24W PICtail Plus interrupt signal)
    RPINR1bits.INT3R = 36;      // Assign RA14/RPI36 to INT3 (input)

    __builtin_write_OSCCONL(OSCCON | 0x40); // Lock PPS
#endif


#if defined(DSPICDEM11)
    // Deselect the LCD controller (PIC18F252 onboard) to ensure there is no SPI2 contention
    LCDCTRL_CS_TRIS = 0;
    LCDCTRL_CS_IO = 1;

    // Hold the codec in reset to ensure there is no SPI2 contention
    CODEC_RST_TRIS = 0;
    CODEC_RST_IO = 0;
#endif

#if defined(SPIRAM_CS_TRIS)
    SPIRAMInit();
#endif
#if defined(EEPROM_CS_TRIS)
    XEEInit();
#endif
#if defined(SPIFLASH_CS_TRIS)
    SPIFlashInit();
#endif
}

/*********************************************************************
 * Function:        void InitAppConfig(void)
 *
 * PreCondition:    MPFSInit() is already called.
 *
 * Input:           None
 *
 * Output:          Write/Read non-volatile config variables.
 *
 * Side Effects:    None
 *
 * Overview:        None
 *
 * Note:            None
 ********************************************************************/
// MAC Address Serialization using a MPLAB PM3 Programmer and 
// Serialized Quick Turn Programming (SQTP). 
// The advantage of using SQTP for programming the MAC Address is it
// allows you to auto-increment the MAC address without recompiling 
// the code for each unit.  To use SQTP, the MAC address must be fixed
// at a specific location in program memory.  Uncomment these two pragmas
// that locate the MAC address at 0x1FFF0.  Syntax below is for MPLAB C 
// Compiler for PIC18 MCUs. Syntax will vary for other compilers.
//#pragma romdata MACROM=0x1FFF0
static ROM BYTE SerializedMACAddress[6] = {MY_DEFAULT_MAC_BYTE1, MY_DEFAULT_MAC_BYTE2, MY_DEFAULT_MAC_BYTE3, MY_DEFAULT_MAC_BYTE4, MY_DEFAULT_MAC_BYTE5, MY_DEFAULT_MAC_BYTE6};
//#pragma romdata

static void InitAppConfig(void)
{
#if defined(EEPROM_CS_TRIS) || defined(SPIFLASH_CS_TRIS)
    unsigned char vNeedToSaveDefaults = 0;
#endif
    
    while(1)
    {
        // Start out zeroing all AppConfig bytes to ensure all fields are 
        // deterministic for checksum generation
        memset((void*)&AppConfig, 0x00, sizeof(AppConfig));
        
        AppConfig.Flags.bIsDHCPEnabled = TRUE;
        AppConfig.Flags.bInConfigMode = TRUE;
        memcpypgm2ram((void*)&AppConfig.MyMACAddr, (ROM void*)SerializedMACAddress, sizeof(AppConfig.MyMACAddr));
//        {
//            _prog_addressT MACAddressAddress;
//            MACAddressAddress.next = 0x157F8;
//            _memcpy_p2d24((char*)&AppConfig.MyMACAddr, MACAddressAddress, sizeof(AppConfig.MyMACAddr));
//        }

		//ADRESSE IP ET DNS ETC ............
        //AppConfig.MyIPAddr.Val = MY_DEFAULT_IP_ADDR_BYTE1 | MY_DEFAULT_IP_ADDR_BYTE2<<8ul | MY_DEFAULT_IP_ADDR_BYTE3<<16ul | MY_DEFAULT_IP_ADDR_BYTE4<<24ul;
        //AppConfig.DefaultIPAddr.Val = AppConfig.MyIPAddr.Val;
        //AppConfig.MyMask.Val = MY_DEFAULT_MASK_BYTE1 | MY_DEFAULT_MASK_BYTE2<<8ul | MY_DEFAULT_MASK_BYTE3<<16ul | MY_DEFAULT_MASK_BYTE4<<24ul;
        //AppConfig.DefaultMask.Val = AppConfig.MyMask.Val;
        //AppConfig.MyGateway.Val = MY_DEFAULT_GATE_BYTE1 | MY_DEFAULT_GATE_BYTE2<<8ul | MY_DEFAULT_GATE_BYTE3<<16ul | MY_DEFAULT_GATE_BYTE4<<24ul;
        //AppConfig.PrimaryDNSServer.Val = MY_DEFAULT_PRIMARY_DNS_BYTE1 | MY_DEFAULT_PRIMARY_DNS_BYTE2<<8ul  | MY_DEFAULT_PRIMARY_DNS_BYTE3<<16ul  | MY_DEFAULT_PRIMARY_DNS_BYTE4<<24ul;
        //AppConfig.SecondaryDNSServer.Val = MY_DEFAULT_SECONDARY_DNS_BYTE1 | MY_DEFAULT_SECONDARY_DNS_BYTE2<<8ul  | MY_DEFAULT_SECONDARY_DNS_BYTE3<<16ul  | MY_DEFAULT_SECONDARY_DNS_BYTE4<<24ul;
//nr_cms[15]; //192.168.000.244
//char passerelle[15]; //192.168.000.153
//char DNS[15]; //192.002.000.020

 

NUMIP1=100*(nr_cog[0]-48)+10*(nr_cog[1]-48)+(nr_cog[2]-48);
NUMIP2=100*(nr_cog[4]-48)+10*(nr_cog[5]-48)+(nr_cog[6]-48);
NUMIP3=100*(nr_cog[8]-48)+10*(nr_cog[9]-48)+(nr_cog[10]-48);
NUMIP4=100*(nr_cog[12]-48)+10*(nr_cog[13]-48)+(nr_cog[14]-48);

NUMDNS1=100*(DNS[0]-48)+10*(DNS[1]-48)+(DNS[2]-48);
NUMDNS2=100*(DNS[4]-48)+10*(DNS[5]-48)+(DNS[6]-48);
NUMDNS3=100*(DNS[8]-48)+10*(DNS[9]-48)+(DNS[10]-48);
NUMDNS4=100*(DNS[12]-48)+10*(DNS[13]-48)+(DNS[14]-48);

NUMPASSE1=100*(passerelle[0]-48)+10*(passerelle[1]-48)+(passerelle[2]-48);
NUMPASSE2=100*(passerelle[4]-48)+10*(passerelle[5]-48)+(passerelle[6]-48);
NUMPASSE3=100*(passerelle[8]-48)+10*(passerelle[9]-48)+(passerelle[10]-48);
NUMPASSE4=100*(passerelle[12]-48)+10*(passerelle[13]-48)+(passerelle[14]-48);


SERVERIP[0]=100*(nr_cog[0]-48)+10*(nr_cog[1]-48)+(nr_cog[2]-48); 
SERVERIP[1]=100*(nr_cog[4]-48)+10*(nr_cog[5]-48)+(nr_cog[6]-48);
SERVERIP[2]=100*(nr_cog[8]-48)+10*(nr_cog[9]-48)+(nr_cog[10]-48);
SERVERIP[3]=100*(nr_cog[12]-48)+10*(nr_cog[13]-48)+(nr_cog[14]-48); 

//FORCAGE ADRESSE POUR TEST
//NUMIP1=10;
//NUMIP2=142;
//NUMIP3=32;
//NUMIP4=26;

//NUMPASSE1=10;
//NUMPASSE2=142;
//NUMPASSE3=35;
//NUMPASSE4=254;


//NUMIP1=192;
//NUMIP2=168;
//NUMIP3=0;
//NUMIP4=244;

//NUMPASSE1=192;
//NUMPASSE2=168;
//NUMPASSE3=0;
//NUMPASSE4=153;




		AppConfig.MyIPAddr.Val = NUMIP1 | NUMIP2<<8ul | NUMIP3<<16ul | NUMIP4<<24ul;
        AppConfig.DefaultIPAddr.Val = AppConfig.MyIPAddr.Val;
        AppConfig.MyMask.Val = 255ul | 255ul<<8ul | 255ul<<16ul | 255ul<<24ul; //255ul | 255ul<<8ul | 255ul<<16ul | 192ul<<24ul;
        AppConfig.DefaultMask.Val = AppConfig.MyMask.Val;
        AppConfig.MyGateway.Val = NUMPASSE1 | NUMPASSE2<<8ul | NUMPASSE3<<16ul | NUMPASSE4<<24ul; 
        AppConfig.PrimaryDNSServer.Val = NUMDNS1 | NUMDNS2<<8ul  | NUMDNS3<<16ul  | NUMDNS4<<24ul;
        AppConfig.SecondaryDNSServer.Val = 192ul | 2ul<<8ul  | 0ul<<16ul  | 50ul<<24ul;



    
        // SNMP Community String configuration
        #if defined(STACK_USE_SNMP_SERVER)
        {
            BYTE i;
            static ROM char * ROM cReadCommunities[] = SNMP_READ_COMMUNITIES;
            static ROM char * ROM cWriteCommunities[] = SNMP_WRITE_COMMUNITIES;
            ROM char * strCommunity;
            
            for(i = 0; i < SNMP_MAX_COMMUNITY_SUPPORT; i++)
            {
                // Get a pointer to the next community string
                strCommunity = cReadCommunities[i];
                if(i >= sizeof(cReadCommunities)/sizeof(cReadCommunities[0]))
                    strCommunity = "";
    
                // Ensure we don't buffer overflow.  If your code gets stuck here, 
                // it means your SNMP_COMMUNITY_MAX_LEN definition in TCPIPConfig.h 
                // is either too small or one of your community string lengths 
                // (SNMP_READ_COMMUNITIES) are too large.  Fix either.
                if(strlenpgm(strCommunity) >= sizeof(AppConfig.readCommunity[0]))
                    while(1);
                
                // Copy string into AppConfig
                strcpypgm2ram((char*)AppConfig.readCommunity[i], strCommunity);
    
                // Get a pointer to the next community string
                strCommunity = cWriteCommunities[i];
                if(i >= sizeof(cWriteCommunities)/sizeof(cWriteCommunities[0]))
                    strCommunity = "";
    
                // Ensure we don't buffer overflow.  If your code gets stuck here, 
                // it means your SNMP_COMMUNITY_MAX_LEN definition in TCPIPConfig.h 
                // is either too small or one of your community string lengths 
                // (SNMP_WRITE_COMMUNITIES) are too large.  Fix either.
                if(strlenpgm(strCommunity) >= sizeof(AppConfig.writeCommunity[0]))
                    while(1);
    
                // Copy string into AppConfig
                strcpypgm2ram((char*)AppConfig.writeCommunity[i], strCommunity);
            }
        }
        #endif
    
        // Load the default NetBIOS Host Name
        memcpypgm2ram(AppConfig.NetBIOSName, (ROM void*)MY_DEFAULT_HOST_NAME, 16);
        FormatNetBIOSName(AppConfig.NetBIOSName);
    
        #if defined(WF_CS_TRIS)
            // Load the default SSID Name
            WF_ASSERT(sizeof(MY_DEFAULT_SSID_NAME)-1 <= sizeof(AppConfig.MySSID));
            memcpypgm2ram(AppConfig.MySSID, (ROM void*)MY_DEFAULT_SSID_NAME, sizeof(MY_DEFAULT_SSID_NAME));
            AppConfig.SsidLength = sizeof(MY_DEFAULT_SSID_NAME) - 1;
    
            AppConfig.SecurityMode = MY_DEFAULT_WIFI_SECURITY_MODE;
            
            #if (MY_DEFAULT_WIFI_SECURITY_MODE == WF_SECURITY_OPEN)
                memset(AppConfig.SecurityKey, 0x00, sizeof(AppConfig.SecurityKey));
                AppConfig.SecurityKeyLength = 0;
    
            #elif MY_DEFAULT_WIFI_SECURITY_MODE == WF_SECURITY_WEP_40
                AppConfig.WepKeyIndex  = MY_DEFAULT_WEP_KEY_INDEX;
                memcpypgm2ram(AppConfig.SecurityKey, (ROM void*)MY_DEFAULT_WEP_KEYS_40, sizeof(MY_DEFAULT_WEP_KEYS_40) - 1);
                AppConfig.SecurityKeyLength = sizeof(MY_DEFAULT_WEP_KEYS_40) - 1;
    
            #elif MY_DEFAULT_WIFI_SECURITY_MODE == WF_SECURITY_WEP_104
                AppConfig.WepKeyIndex  = MY_DEFAULT_WEP_KEY_INDEX;
                memcpypgm2ram(AppConfig.SecurityKey, (ROM void*)MY_DEFAULT_WEP_KEYS_104, sizeof(MY_DEFAULT_WEP_KEYS_104) - 1);
                AppConfig.SecurityKeyLength = sizeof(MY_DEFAULT_WEP_KEYS_104) - 1;
    
            #elif (MY_DEFAULT_WIFI_SECURITY_MODE == WF_SECURITY_WPA_WITH_KEY)       || \
                  (MY_DEFAULT_WIFI_SECURITY_MODE == WF_SECURITY_WPA2_WITH_KEY)      || \
                  (MY_DEFAULT_WIFI_SECURITY_MODE == WF_SECURITY_WPA_AUTO_WITH_KEY)
                memcpypgm2ram(AppConfig.SecurityKey, (ROM void*)MY_DEFAULT_PSK, sizeof(MY_DEFAULT_PSK) - 1);
                AppConfig.SecurityKeyLength = sizeof(MY_DEFAULT_PSK) - 1;
    
            #elif (MY_DEFAULT_WIFI_SECURITY_MODE == WF_SECURITY_WPA_WITH_PASS_PHRASE)     || \
                  (MY_DEFAULT_WIFI_SECURITY_MODE == WF_SECURITY_WPA2_WITH_PASS_PHRASE)    || \
                  (MY_DEFAULT_WIFI_SECURITY_MODE == WF_SECURITY_WPA_AUTO_WITH_PASS_PHRASE)
                memcpypgm2ram(AppConfig.SecurityKey, (ROM void*)MY_DEFAULT_PSK_PHRASE, sizeof(MY_DEFAULT_PSK_PHRASE) - 1);
                AppConfig.SecurityKeyLength = sizeof(MY_DEFAULT_PSK_PHRASE) - 1;
            #elif (MY_DEFAULT_WIFI_SECURITY_MODE == WF_SECURITY_WPS_PUSH_BUTTON)
                memset(AppConfig.SecurityKey, 0x00, sizeof(AppConfig.SecurityKey));
                AppConfig.SecurityKeyLength = 0;
            #elif (MY_DEFAULT_WIFI_SECURITY_MODE == WF_SECURITY_WPS_PIN)
                memcpypgm2ram(AppConfig.SecurityKey, (ROM void*)MY_DEFAULT_WPS_PIN, sizeof(MY_DEFAULT_WPS_PIN) - 1);
                AppConfig.SecurityKeyLength = sizeof(MY_DEFAULT_WPS_PIN) - 1;
            #else 
                #error "No security defined"
            #endif /* MY_DEFAULT_WIFI_SECURITY_MODE */
    
        #endif
 
        // Compute the checksum of the AppConfig defaults as loaded from ROM
        wOriginalAppConfigChecksum = CalcIPChecksum((BYTE*)&AppConfig, sizeof(AppConfig));

        #if defined(EEPROM_CS_TRIS) || defined(SPIFLASH_CS_TRIS)
        {
            NVM_VALIDATION_STRUCT NVMValidationStruct;

            // Check to see if we have a flag set indicating that we need to 
            // save the ROM default AppConfig values.
            if(vNeedToSaveDefaults)
                SaveAppConfig(&AppConfig);
        
            // Read the NVMValidation record and AppConfig struct out of EEPROM/Flash
            #if defined(EEPROM_CS_TRIS)
            {
                XEEReadArray(0x0000, (BYTE*)&NVMValidationStruct, sizeof(NVMValidationStruct));
                XEEReadArray(sizeof(NVMValidationStruct), (BYTE*)&AppConfig, sizeof(AppConfig));
            }
            #elif defined(SPIFLASH_CS_TRIS)
            {
                SPIFlashReadArray(0x0000, (BYTE*)&NVMValidationStruct, sizeof(NVMValidationStruct));
                SPIFlashReadArray(sizeof(NVMValidationStruct), (BYTE*)&AppConfig, sizeof(AppConfig));
            }
            #endif
    
            // Check EEPROM/Flash validitity.  If it isn't valid, set a flag so 
            // that we will save the ROM default values on the next loop 
            // iteration.
            if((NVMValidationStruct.wConfigurationLength != sizeof(AppConfig)) ||
               (NVMValidationStruct.wOriginalChecksum != wOriginalAppConfigChecksum) ||
               (NVMValidationStruct.wCurrentChecksum != CalcIPChecksum((BYTE*)&AppConfig, sizeof(AppConfig))))
            {
                // Check to ensure that the vNeedToSaveDefaults flag is zero, 
                // indicating that this is the first iteration through the do 
                // loop.  If we have already saved the defaults once and the 
                // EEPROM/Flash still doesn't pass the validity check, then it 
                // means we aren't successfully reading or writing to the 
                // EEPROM/Flash.  This means you have a hardware error and/or 
                // SPI configuration error.
                if(vNeedToSaveDefaults)
                {
                    while(1);
                }
                
                // Set flag and restart loop to load ROM defaults and save them
                vNeedToSaveDefaults = 1;
                continue;
            }
            
            // If we get down here, it means the EEPROM/Flash has valid contents 
            // and either matches the ROM defaults or previously matched and 
            // was run-time reconfigured by the user.  In this case, we shall 
            // use the contents loaded from EEPROM/Flash.
            break;
        }
        #endif
        break;
    }
}

#if defined(EEPROM_CS_TRIS) || defined(SPIFLASH_CS_TRIS)
void SaveAppConfig(const APP_CONFIG *ptrAppConfig)
{
    NVM_VALIDATION_STRUCT NVMValidationStruct;

    // Ensure adequate space has been reserved in non-volatile storage to 
    // store the entire AppConfig structure.  If you get stuck in this while(1) 
    // trap, it means you have a design time misconfiguration in TCPIPConfig.h.
    // You must increase MPFS_RESERVE_BLOCK to allocate more space.
    #if defined(STACK_USE_MPFS2)
        if(sizeof(NVMValidationStruct) + sizeof(AppConfig) > MPFS_RESERVE_BLOCK)
            while(1);
    #endif

    // Get proper values for the validation structure indicating that we can use 
    // these EEPROM/Flash contents on future boot ups
    NVMValidationStruct.wOriginalChecksum = wOriginalAppConfigChecksum;
    NVMValidationStruct.wCurrentChecksum = CalcIPChecksum((BYTE*)ptrAppConfig, sizeof(APP_CONFIG));
    NVMValidationStruct.wConfigurationLength = sizeof(APP_CONFIG);

    // Write the validation struct and current AppConfig contents to EEPROM/Flash
    #if defined(EEPROM_CS_TRIS)
        XEEBeginWrite(0x0000);
        XEEWriteArray((BYTE*)&NVMValidationStruct, sizeof(NVMValidationStruct));
        XEEWriteArray((BYTE*)ptrAppConfig, sizeof(APP_CONFIG));
    #else
        SPIFlashBeginWrite(0x0000);
        SPIFlashWriteArray((BYTE*)&NVMValidationStruct, sizeof(NVMValidationStruct));
        SPIFlashWriteArray((BYTE*)ptrAppConfig, sizeof(APP_CONFIG));
    #endif
}
#endif









void mainio(void)
{
unsigned char tabt;


MAJBLOC='Y';
DEBUTCOM:
TRISB=0xF7; //TT EN ENTREES SAUF RB3 RAZ CMS
RAZCMS=0;

CMouPAS=1;
testtimer=0;

ETAT_CONNECT_MINITEL=0x00;

TRISEbits.TRISE0=0;
TRISFbits.TRISF0=0; //EN SORTIE
TRISFbits.TRISF1=0;
TRISAbits.TRISA4=0;

TRISAbits.TRISA0=0;
TRISAbits.TRISA1=0;

LEDRJTEST1=0;
LEDRJTEST2=0;  

//TRISHbits.TRISH5=0;
TRISDbits.TRISD3=0;
TRISDbits.TRISD2=0; //LES I2C
TRISDbits.TRISD0=0;
TRISDbits.TRISD4=0;
TRISDbits.TRISD1=0;
TRISDbits.TRISD5=0;

IO_EN_COM=0; // IO N'EST PAS EN COM
TRISH=0b11001101;

NBCOM=0;
ALARMEATRANS=0;



//COUPMODEM=1;
TEMPO(2);
  
//COUPMODEM=0; // PAR DEFAUT REPOSE MODEM ALLUME
TEMPO(2); 
PORTHbits.RH5=0;//!!!!!!!!!!!!!!

ADCON0=0x00;
ADCON1=0x0F;

//COUPMODEM=1;

//int i=0; 
//int j=0;
//TRISB=0xF7; //TT EN ENTREES SAUF RB3 RAZ CMS
//TRISH=0b11001101;
TRISCbits.TRISC5=0; 
TRISCbits.TRISC7=1;
TRISCbits.TRISC6=0;
TRISAbits.TRISA4=0;

PORTHbits.RH3=0;

ACTIONENCOURS=0;

//init_RS232();	
	
TEMPO(1);// AVANT DE CONFIGURER MODEM !!!!!!!!!!!!!!!
STOPMINITEL=0x00;
//init_modem (0);	// A REMETTRE
//minitel ();											





Init_RS485(); // A REMETTRE
Init_I2C();

 


//INTCONbits.GIE=0; //AUTORISATION INT GENERALE
//RCONbits.IPEN = 1; // activation niveau de priorit� il y en pas mais il faut pr 0x08
//INTCONbits.RBIE=0;
//INTCON2bits.INTEDG1=0; //FRONT DESCENDANT RB1

//INTCON3bits.INT1IP=1; //?
//INTCON3bits.INT1IE=1; //?



I2CMEMO3=0;
I2CMEMO=1;
I2CMEMO2=1;
I2CINTR=1;
I2CHORLG=1;
DEMANDE_DE_LAUTO=0x00;

//GROUPE_EN_MEMOIRE(TRAMEIN,3) ;
							
Recuperer_param_cms();



			LIRE_HEURE_MINITEL(); // ON LIT L'HEURE AU DEMARRAGE
			if (AA[2]=='0' && AA[3]=='0') //ANNEE 2000 ON RELANCE LE TIMER DANS LES SECONDES ici on regles l'heure)
			{//hh mm ss JJ MM AA 
			I2CHORLG=0;  
			HORLOGET[0]='0';
			HORLOGET[1]='7';	 	
			HORLOGET[2]='1';
			HORLOGET[3]='5';	 	
			HORLOGET[4]='0';
			HORLOGET[5]='0';	
			HORLOGET[6]='2';
			HORLOGET[7]='9';	
			HORLOGET[8]='1';
			HORLOGET[9]='1';	 	
			HORLOGET[10]='0';
			HORLOGET[11]='1';	 	
			entrer_trame_horloge (); //ECRITURE 
			I2CHORLG=1;
			}

LEDC=0; 
LEDD=0; 
//INTCON3bits.INT1IF=0;
INTCONbits.GIE=1; //AUTORISATION INT GENERALE
PORTAbits.RA4=0; // EN RECEPTION
DERE=0; //AUTORISATION DE RECEVOIR
RCSTA2bits.CREN  = 0; 

TEMPO(5);
//-----------------------------------------------------------------zbr BLOC NOTES
if (MAJBLOC=='Y')
{
TEMPO(1);

// DEMANDE INFOS CAPTEURS ICI L'INTERRUPTION EST AUTORISEE
DEMANDE_INFOS_CAPTEURS[0]='9'; //26102020
DEMANDE_INFOS_CAPTEURS[1]='9';


//testINT2 ();

if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
{
ENVOIE_DES_TRAMES (0,328,&DEMANDE_INFOS_CAPTEURS) ;//ENVIRON 1.49mn UNE CARTE ET 8.50mn LES 100VOIES ET 17CAPTEURS
	MAJBLOC='N';
}
  PORTHbits.RH5=0;

}

//goto test2;
				if (CMouPAS==1)
									{
									
TEMPO(1);
//-------------------------------------------------------zpl parametre
										DEMANDE_INFOS_CAPTEURS[0]='x';
										DEMANDE_INFOS_CAPTEURS[1]='x';
										DEMANDE_INFOS_CAPTEURS[2]='x';
										DEMANDE_INFOS_CAPTEURS[3]='x';
										testINT2 ();
										if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
										{	
											ENVOIE_DES_TRAMES (0,336,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE PARAM 5 secondes ou plus
											if (TRAMEIN[8]=='D') //SI DD ON REFAIT LA DEMANDE
											{	
											TEMPO(10);
												testINT2 ();
												if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
												{	
												ENVOIE_DES_TRAMES (0,336,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE	
												}
											}

										Recuperer_param_cms(); //EST CE UTILE DE RECUPERER TT LE TEMPS
									
										}

//--------------------------------------------------------------------------
test2:
InitAppConfig();
//goto test3;
//		if (REBOOTIP==0x00)
	//	{
	//	TRA=0;
	//	REBOOTIP=0xFF;
	//	goto FINMAINIO;
//		return;
//	}


//LEDRJTEST1=0;
										PORTHbits.RH5=0;
										TEMPO(2);
//goto param;
 										//TEMPO(6);
//goto noziz;
 
//--------------------------------------------------------------------------groupe zig 
										//DEMANDEGROUPE ICI L'INTERRUPTION EST INTERDITE L'OPAERATION N'EST PAS LENTE EN THEORIE VALIDEE SEULE
										DEMANDE_INFOS_CAPTEURS[0]='x';
										DEMANDE_INFOS_CAPTEURS[1]='x';
										DEMANDE_INFOS_CAPTEURS[2]='x';
										DEMANDE_INFOS_CAPTEURS[3]='x'; 
										testINT2 ();
										if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
											{
											ENVOIE_DES_TRAMES (0,324,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE ENVIRON 7secondes ou plus
											if (TRAMEIN[8]=='D') //SI DD ON REFAIT LA DEMANDE
											{	
											TEMPO(10);
											testINT2 ();
												if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
												{
												ENVOIE_DES_TRAMES (0,324,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE	
												}
											}
											}

										//LEDRJTEST1=1;
										PORTHbits.RH5=0;
//--------------------------------------------------------------------------
										
										heure:
										//goto fintest;
										//TEMPO(6);

//--------------------------------------------------------------------------zor demande reglage heure
										TEMPO(2);
										DEMANDE_INFOS_CAPTEURS[0]='x';
										DEMANDE_INFOS_CAPTEURS[1]='x';
										DEMANDE_INFOS_CAPTEURS[2]='x';
										DEMANDE_INFOS_CAPTEURS[3]='x';
										testINT2 ();
										if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
										{
											ENVOIE_DES_TRAMES (0,341,&DEMANDE_INFOS_CAPTEURS) ; //ENVIRON 5secondes ou plus DEMANDE HEURE ICI L'INTERRUPTION EST INTERDITE L'OPAERATION N'EST PAS LENTE EN THEORIE VALIDEE SEULE
												if (TRAMEIN[8]=='D') //SI DD ON REFAIT LA DEMANDE
											{	
											TEMPO(10);
												testINT2 ();
																					if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
																					{
											ENVOIE_DES_TRAMES (0,341,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE	
																					}
												}
										
										}
										
										PORTHbits.RH5=0;
										//LEDRJTEST1=1;
										
										
										
										} //FIN DEMANDE GROUPE ET PARAM ET HEURE AU DEBUT SI CMS LAISSE LIO


//-------------------------------------------------------------------------------------

TEMPO(1);
test3:
//MOUCHARD('2');
//DEMANDE_INFOS_CAPTEURS[0]='#';
//DEMANDE_INFOS_CAPTEURS[1]='0';
//DEMANDE_INFOS_CAPTEURS[2]='1';
//DEMANDE_INFOS_CAPTEURS[3]='9';
//MOUCHARD2();

//INTCONbits.GIE=1; //AUTORISATION INT GENERALE
//INTCONbits.TMR0IF=0;
//TMR0L=0x92;
//T0CON=0x47;
//T0CONbits.TMR0ON=1;
//INTCONbits.TMR0IE=1; //RELANCE TIMER



//T2CONbits.TMR2ON=1; //ARRET TIMER
//TMR2=0;
//PR2=0xFF;
//PIR1bits.TMR2IF=0;
//PIE1bits.TMR2IE=1;

TRA=1;
		while (TRA==1) 
		{
	
//ENVOYER_GROUPE();
//RECEVOIR_RS232();	
//voir_qui_existe();
//CMS_TO_TELGAT ();  //pour test local
                    
//	voir_qui_existe(); //PREPARATION DES TRAMES CPATEURS A ENVOYER




 

		testINT2 ();
		if (REA_DESACTIVE==0xFF)
		{REA_DESACTIVE=0x00;
		goto REAOFF;
		
		}
//ENVOYER_GROUPE();

	




					if (testtimer==1) //ENVIRON 24s au lieu de 30seconde coeff environ 0.8
					{
									for (TRAMEIN[124]=1;TRAMEIN[124]<=10;TRAMEIN[124]++) //10SECONDES PAR PAS DE 100ms
									{	
										
									  //TEST TIMER 10SECONDES PAR PAS DE 1S EN REALITE ENVIRON 8,46S donc 1= environ 804ms
									LEDC=1;
									TEMPO(1);
									 LEDC=!LEDC;
									TEMPO(1);
									LEDC=!LEDC;
									TEMPO(1);
									LEDC=!LEDC;
									TEMPO(1);
									LEDC=!LEDC;
									TEMPO(1);
									LEDC=!LEDC;
									TEMPO(1);
									LEDC=!LEDC;
									TEMPO(1);
									LEDC=!LEDC;
									TEMPO(1);
									LEDC=!LEDC;
									TEMPO(1);
									LEDC=!LEDC;
									TEMPO(1);
									LEDC=1;
									
									TEMPO(5);
									
									for (testtimer=1;testtimer<=100;testtimer++) //10SECONDES PAR PAS DE 100ms ICI 7.93secondes donc 100= environ 80ms
									{
									delay_qms(100);
									LEDC=!LEDC;
									}
									
									LEDC=0;
									TEMPO(5);
									}// 10x
					TRAMEIN[124]='-';	
					}

//	tmp=ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEIN),calcul_addmemoire(&TRAMEIN),&TRAMEIN,1); //ECRITURE EN EEPROM


// 	tmp=LIRE_EEPROM(choix_memoire(&TRAMEIN),calcul_addmemoire(&TRAMEIN),&TRAMEIN,1); //LIRE DANS EEPROM







//LEDRJTEST1=1;
 

//LEDD=!LEDD;
delay_qms(200);
 // LEDD=!LEDD;
delay_qms(200);
//LEDD=!LEDD;
delay_qms(200);
//LEDD=!LEDD;
delay_qms(200);
//LEDD=!LEDD;
delay_qms(200);
//LEDD=!LEDD;
delay_qms(200);
//LEDD=!LEDD;
delay_qms(200);  



//LEDC=1;


//TEMPO(60);
//TEMPO(60);
//TEMPO(60);
//TEMPO(60);
//TEMPO(60);
//TEMPO(60);
//TEMPO(60);




//LEDC=0;

//LEDRJTEST1=0;

//LEDD=0;
 


DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='1';
DEMANDE_INFOS_CAPTEURS[2]='2';
//CHERCHER_DONNNEES_CABLES(&DEMANDE_INFOS_CAPTEURS);


//voir_qui_existe(); //PREPARATION DES TRAMES CPATEURS A ENVOYER


//ENVOIE_DES_TRAMES (0,329,&TRAMEIN) ; //CREATION CAPTEUR SUIVANT TRA. MEMOIRE SI ERRFF (TERMINEACTION=1) ALORS L LA CREATION/MODIF AIT EU LIEU

// pompe direction assist�e pour berlingo
//analyser_les_alarmes_en_cours();

 


//----------------------------

// EFFACEMENT CAPTEUR
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='0';
DEMANDE_INFOS_CAPTEURS[2]='1';
DEMANDE_INFOS_CAPTEURS[3]='0'; 
DEMANDE_INFOS_CAPTEURS[4]='1';
//ENVOIE_DES_TRAMES (0,320,&TRAMEIN) ; //CREATION CAPTEUR SUIVANT TRA. MEMOIRE



//kolop=0;
lk:



ANALYSE_SE_ALARME();
//delay_qms(100);
//LIRE_EEPROM(choix_memoirePRECEDENT(&TRAMEOUT),kolop,&TRAMEIN,1);
//delay_qms(10);
//kolop++;
//goto lk;

NBCOM=3;
 //TCPClose(MySocket); 
COM:
BOUCLEATTENTE=0;
IO_EN_COM=0; // IO EST PAS EN COM
test:
do
{
BOUCLEATTENTE++;
ecouteTCPIP();
if 	(PASDEDEMANDECONNECTIONTCPIP==0xFF)
GenericTCPServer();
TEMPO(1);
}while (BOUCLEATTENTE<30);				//while (BOUCLEATTENTE<30);
//goto test;






//GenericTCPClient(); //pour transmettre alarme � TELGAT



COM2:
ecouteTCPIP();
if 	(PASDEDEMANDECONNECTIONTCPIP==0xFF)
GenericTCPServer();
			if (DEMANDE_DE_LAUTO!=0x00) //ON BLOQUE TANT QUE 
			{
			if (IO_EN_COM==1)
			IO_EN_COM=0; // IO EST plus EN COM 


						testINT2 ();
			if (REA_DESACTIVE==0xFF)
			{
			REA_DESACTIVE=0x00;
			//goto REAOFF;
			}
			TEMPO(3);
			//if (NBCOM==1)
			
			goto COM2;
			//else			
			

			}




	if (CMouPAS==1)
	{

REAOFF:

if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
NBCOM=2;
goto COM;
}
//if (NBCOM<=2) //BUG
//{
//NBCOM=0;
//NBCOM=3;
//goto COM;
//}


TEMPO(1);
//LEDRJTEST1=0;
 PORTHbits.RH5=0;
TEMPO(1);
//goto noziz;
//goto fintest; 


MODERAPID=1;



ecouteTCPIP();
if 	(PASDEDEMANDECONNECTIONTCPIP==0xFF)
GenericTCPServer();


//------------------------------------------------------------------------------------------------------------
if (MAJBLOC=='Y')
{
TEMPO(1);

// DEMANDE INFOS CAPTEURS ICI L'INTERRUPTION EST AUTORISEE
DEMANDE_INFOS_CAPTEURS[0]='9'; //26102020
DEMANDE_INFOS_CAPTEURS[1]='9';


testINT2 ();

if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
{
ENVOIE_DES_TRAMES (0,328,&DEMANDE_INFOS_CAPTEURS) ;//ENVIRON 1.49mn UNE CARTE ET 8.50mn LES 100VOIES ET 17CAPTEURS
	MAJBLOC='N';
}
  PORTHbits.RH5=0;

}
//----------------------------------------------------------------------------------------------------------------





ecouteTCPIP();
if ((TYPECARTE[0]=='E')||(TYPECARTE[0]=='M'))
{
//---------------------------------------------------------------------------------------------------------------
TEMPO(1);

// DEMANDE INFOS CAPTEURS ICI L'INTERRUPTION EST AUTORISEE
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='0';
DEMANDE_INFOS_CAPTEURS[2]='1';
DEMANDE_INFOS_CAPTEURS[3]='0';
DEMANDE_INFOS_CAPTEURS[4]='0';
DEMANDE_INFOS_CAPTEURS[5]='0';
DEMANDE_INFOS_CAPTEURS[6]='2';
DEMANDE_INFOS_CAPTEURS[7]='0';
DEMANDE_INFOS_CAPTEURS[8]='1';
DEMANDE_INFOS_CAPTEURS[9]='6';

testINT2 ();

if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
ENVOIE_DES_TRAMES (0,343,&DEMANDE_INFOS_CAPTEURS) ;//ENVIRON 1.49mn UNE CARTE ET 8.50mn LES 100VOIES ET 17CAPTEURS
  PORTHbits.RH5=0;
 //LEDRJTEST1=1;
}

ecouteTCPIP();
if ((TYPECARTE[1]=='E')||(TYPECARTE[1]=='M'))
{

//---------------------------------------------------------------------------------------------------------------
TEMPO(6);

// DEMANDE INFOS CAPTEURS ICI L'INTERRUPTION EST AUTORISEE
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='2';
DEMANDE_INFOS_CAPTEURS[2]='1';
DEMANDE_INFOS_CAPTEURS[3]='0';
DEMANDE_INFOS_CAPTEURS[4]='0';
DEMANDE_INFOS_CAPTEURS[5]='0';
DEMANDE_INFOS_CAPTEURS[6]='4';
DEMANDE_INFOS_CAPTEURS[7]='0';
DEMANDE_INFOS_CAPTEURS[8]='1';
DEMANDE_INFOS_CAPTEURS[9]='6';

testINT2 ();


if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}		
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
ENVOIE_DES_TRAMES (0,343,&DEMANDE_INFOS_CAPTEURS) ;//ENVIRON 1.49mn UNE CARTE ET 8.50mn LES 100VOIES ET 17CAPTEURS
  PORTHbits.RH5=0;
 //LEDRJTEST1=1;
}

ecouteTCPIP();
if 	(PASDEDEMANDECONNECTIONTCPIP==0xFF)
GenericTCPServer();
if ((TYPECARTE[2]=='E')||(TYPECARTE[2]=='M'))
{
//---------------------------------------------------------------------------------------------------------------
TEMPO(6);

// DEMANDE INFOS CAPTEURS ICI L'INTERRUPTION EST AUTORISEE
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='4';
DEMANDE_INFOS_CAPTEURS[2]='1';
DEMANDE_INFOS_CAPTEURS[3]='0';
DEMANDE_INFOS_CAPTEURS[4]='0';
DEMANDE_INFOS_CAPTEURS[5]='0';
DEMANDE_INFOS_CAPTEURS[6]='6'; //6
DEMANDE_INFOS_CAPTEURS[7]='0';//0
DEMANDE_INFOS_CAPTEURS[8]='1';
DEMANDE_INFOS_CAPTEURS[9]='6';

testINT2 ();
if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
ENVOIE_DES_TRAMES (0,343,&DEMANDE_INFOS_CAPTEURS) ;//ENVIRON 1.49mn UNE CARTE ET 8.50mn LES 100VOIES ET 17CAPTEURS
  PORTHbits.RH5=0;
 //LEDRJTEST1=1;
}

ecouteTCPIP();
if 	(PASDEDEMANDECONNECTIONTCPIP==0xFF)
GenericTCPServer();
if ((TYPECARTE[3]=='E')||(TYPECARTE[3]=='M'))
{
//---------------------------------------------------------------------------------------------------------------
TEMPO(6);

// DEMANDE INFOS CAPTEURS ICI L'INTERRUPTION EST AUTORISEE
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='6';
DEMANDE_INFOS_CAPTEURS[2]='1';
DEMANDE_INFOS_CAPTEURS[3]='0';
DEMANDE_INFOS_CAPTEURS[4]='0';
DEMANDE_INFOS_CAPTEURS[5]='0';
DEMANDE_INFOS_CAPTEURS[6]='8';
DEMANDE_INFOS_CAPTEURS[7]='0';
DEMANDE_INFOS_CAPTEURS[8]='1';
DEMANDE_INFOS_CAPTEURS[9]='6';

testINT2 ();
if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
ENVOIE_DES_TRAMES (0,343,&DEMANDE_INFOS_CAPTEURS) ;//ENVIRON 1.49mn UNE CARTE ET 8.50mn LES 100VOIES ET 17CAPTEURS
  PORTHbits.RH5=0; 
 //LEDRJTEST1=1;
}

ecouteTCPIP();
if 	(PASDEDEMANDECONNECTIONTCPIP==0xFF)
GenericTCPServer();
if ((TYPECARTE[4]=='E')||(TYPECARTE[4]=='M'))
{

TEMPO(6); 

// DEMANDE INFOS CAPTEURS ICI L'INTERRUPTION EST AUTORISEE
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='8';
DEMANDE_INFOS_CAPTEURS[2]='1';
DEMANDE_INFOS_CAPTEURS[3]='0';
DEMANDE_INFOS_CAPTEURS[4]='0';
DEMANDE_INFOS_CAPTEURS[5]='1'; //1
DEMANDE_INFOS_CAPTEURS[6]='0';
DEMANDE_INFOS_CAPTEURS[7]='0';
DEMANDE_INFOS_CAPTEURS[8]='1';
DEMANDE_INFOS_CAPTEURS[9]='6';

testINT2 ();
if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
ENVOIE_DES_TRAMES (0,343,&DEMANDE_INFOS_CAPTEURS) ;//ENVIRON 1.49mn UNE CARTE ET 8.50mn LES 100VOIES ET 17CAPTEURS
  PORTHbits.RH5=0;
 //LEDRJTEST1=1;
}


//goto fintest;
TEMPO(6);

noziz:
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
{
ENVOIE_DES_TRAMES (0,341,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE HEURE
}
//----------------------------------------------------------------------------------------------------------------
ecouteTCPIP();
if 	(PASDEDEMANDECONNECTIONTCPIP==0xFF)
GenericTCPServer();
if ((TYPECARTE[0]=='E')||(TYPECARTE[0]=='M'))
{ 
  TEMPO(2);

 //DEMANDE TABLE EXISTENCE ICI L'INTERRUPTION EST INTERDITE L'OPAERATION N'EST PAS LENTE EN THEORIE
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='1';
//DEMANDE_INFOS_CAPTEURS[2]='3';
//DEMANDE_INFOS_CAPTEURS[3]='0';
//DEMANDE_INFOS_CAPTEURS[4]='0';
testINT2 ();	

if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
{
ENVOIE_DES_TRAMES (0,333,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE EXISTENCE A CM 8 SECONDES LA CARTE
	if (TRAMEIN[8]=='D') //SI DD ON REFAIT LA DEMANDE
	{	
	TEMPO(10); //RE ESSAYE AU BOUT DE 6secondes
											testINT2 ();
											if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
											{
											ENVOIE_DES_TRAMES (0,333,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE	
											}
	}
}
//LEDRJTEST1=0;
PORTHbits.RH5=0;
}
//----------------------------------------------------------------------------------------------------------------



//----------------------------------------------------------------------------------------------------------------
ecouteTCPIP();
if 	(PASDEDEMANDECONNECTIONTCPIP==0xFF)
GenericTCPServer();
if ((TYPECARTE[1]=='E')||(TYPECARTE[1]=='M'))
{ 
  TEMPO(2);

 //DEMANDE TABLE EXISTENCE ICI L'INTERRUPTION EST INTERDITE L'OPAERATION N'EST PAS LENTE EN THEORIE
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='2';
//DEMANDE_INFOS_CAPTEURS[2]='3';
//DEMANDE_INFOS_CAPTEURS[3]='0';
//DEMANDE_INFOS_CAPTEURS[4]='0';
testINT2 ();	
if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
{
ENVOIE_DES_TRAMES (0,333,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE EXISTENCE A CM
	if (TRAMEIN[8]=='D') //SI DD ON REFAIT LA DEMANDE
	{	
	TEMPO(10);
		testINT2 ();
											if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
											{
	ENVOIE_DES_TRAMES (0,333,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE	
		}
}
}
//LEDRJTEST1=0;
PORTHbits.RH5=0;
}
//----------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------
ecouteTCPIP();
if 	(PASDEDEMANDECONNECTIONTCPIP==0xFF)
GenericTCPServer();
if ((TYPECARTE[2]=='E')||(TYPECARTE[2]=='M'))
{ 
  TEMPO(2);

 //DEMANDE TABLE EXISTENCE ICI L'INTERRUPTION EST INTERDITE L'OPAERATION N'EST PAS LENTE EN THEORIE
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='3';
//DEMANDE_INFOS_CAPTEURS[2]='3';
//DEMANDE_INFOS_CAPTEURS[3]='0';
//DEMANDE_INFOS_CAPTEURS[4]='0';
testINT2 ();	
if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
{
ENVOIE_DES_TRAMES (0,333,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE EXISTENCE A CM
	if (TRAMEIN[8]=='D') //SI DD ON REFAIT LA DEMANDE
	{	
	TEMPO(10);
	testINT2 ();
											if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
											{
	ENVOIE_DES_TRAMES (0,333,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE	
	}
	}
}
//LEDRJTEST1=0;
PORTHbits.RH5=0;
}
//----------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------
ecouteTCPIP();
if ((TYPECARTE[3]=='E')||(TYPECARTE[3]=='M'))
{ 
  TEMPO(2);

 //DEMANDE TABLE EXISTENCE ICI L'INTERRUPTION EST INTERDITE L'OPAERATION N'EST PAS LENTE EN THEORIE
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='4';
//DEMANDE_INFOS_CAPTEURS[2]='3';
//DEMANDE_INFOS_CAPTEURS[3]='0';
//DEMANDE_INFOS_CAPTEURS[4]='0';
testINT2 ();	
if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
{
ENVOIE_DES_TRAMES (0,333,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE EXISTENCE A CM
	if (TRAMEIN[8]=='D') //SI DD ON REFAIT LA DEMANDE
	{	
	TEMPO(10);
		testINT2 ();
											if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
											{
	ENVOIE_DES_TRAMES (0,333,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE	
											}
	}
}
//LEDRJTEST1=0;
PORTHbits.RH5=0;
}
//----------------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------------
ecouteTCPIP();
if 	(PASDEDEMANDECONNECTIONTCPIP==0xFF)
GenericTCPServer();
if ((TYPECARTE[4]=='E')||(TYPECARTE[4]=='M'))
{ 
  TEMPO(2);

 //DEMANDE TABLE EXISTENCE ICI L'INTERRUPTION EST INTERDITE L'OPAERATION N'EST PAS LENTE EN THEORIE
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='5';
//DEMANDE_INFOS_CAPTEURS[2]='3';
//DEMANDE_INFOS_CAPTEURS[3]='0';
//DEMANDE_INFOS_CAPTEURS[4]='0';
testINT2 ();	
if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
{
ENVOIE_DES_TRAMES (0,333,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE EXISTENCE A CM
	if (TRAMEIN[8]=='D') //SI DD ON REFAIT LA DEMANDE
	{	
	TEMPO(10);
			testINT2 ();
											if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
											{
		ENVOIE_DES_TRAMES (0,333,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE	
		}
	}
}
//LEDRJTEST1=0;
PORTHbits.RH5=0;
}











 
//----------------------------------------------------------------------------------------------------------------

TEMPO(1); 
										//DEMANDEGROUPE ICI L'INTERRUPTION EST INTERDITE L'OPAERATION N'EST PAS LENTE EN THEORIE VALIDEE SEULE
										DEMANDE_INFOS_CAPTEURS[0]='x';
										DEMANDE_INFOS_CAPTEURS[1]='x';
										DEMANDE_INFOS_CAPTEURS[2]='x';
										DEMANDE_INFOS_CAPTEURS[3]='x'; 
										testINT2 ();
										if (IO_EN_COM==1)
										{
										IO_EN_COM=0; // IO EST plus EN COM 
										goto COM;
										}
										if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
											{
											ENVOIE_DES_TRAMES (0,324,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE ENVIRON 7secondes ou plus
											if (TRAMEIN[8]=='D') //SI DD ON REFAIT LA DEMANDE
											{	
											TEMPO(10);
											testINT2 ();
		 										if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
												{
												ENVOIE_DES_TRAMES (0,324,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE	
												}
											}
											}

										//LEDRJTEST1=1;
										PORTHbits.RH5=0;
										//--------------------------------


if (ZPLENVOYEE<2)  //26102020
{		
TEMPO(1);
										DEMANDE_INFOS_CAPTEURS[0]='x';
										DEMANDE_INFOS_CAPTEURS[1]='x';
										DEMANDE_INFOS_CAPTEURS[2]='x';
										DEMANDE_INFOS_CAPTEURS[3]='x'; 
										testINT2 ();
										if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
											{
											ENVOIE_DES_TRAMES (0,336,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE ENVIRON 7secondes ou plus
											if (TRAMEIN[8]=='D') //SI DD ON REFAIT LA DEMANDE
											{	
											TEMPO(10);
											testINT2 ();
												if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
												{
												ENVOIE_DES_TRAMES (0,336,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE	
												}
											}
											}

PORTHbits.RH5=0;
}

 //TEMPO(30); // POUR 30 ON A 25Secondes DONC 100ms DELAY_qms ENVIRON 83MS

NBCOM=3;
//TEMPO(20); //pour 100 1mn23s pour 10 8s
fintest:
//TEMPO(10);

PORTHbits.RH5=0;
goto COM;
 
//if ((TYPECARTE[0]=='E')||(TYPECARTE[0]=='M'))
//{
	//for (pas=0;pas<;pas++)
	//{
			testINT2 ();
			if (REA_DESACTIVE==0xFF)
			{
			REA_DESACTIVE=0x00;
			goto REAOFF;  
			
			}
			
//	}
//}








FINMAINIO:                                                            
//delay_qms(100);
//delay_qms(100);
//delay_qms(100);
if ((TRANSALARM=='O')||(TRANSSORTIEALARM=='O'))
ANALYSE_SE_ALARME ();

delay_qms(100);


//RS485('U');
//RS485('A');
//RS485('B');


//RECEVOIRST_RS485();
  
//--------------------------------


  

						

							}//FIN(CMouPAS==1)

	



}//FIN WHILE(1)







}


void echange(void)
{






//GROUPE
/////////////////////////////////////////////////////////

//nbrerecep=0;
etiq3:
ENVOYER_GROUPE ();
TCPPutArray(MySocket, TRAMEOUT, 235); //longueur 234 <CR> +1 
// TRAMEENVOIRS485DONNEES (); //DEBUG 485 
if (RECEPTTCPACK ()) // SI DECONNECT =1
goto sortir;

if (TYPEORDREALARME==2)
voir_qui_existe (2);
if (TYPEORDREALARME==1)
voir_qui_existe (1);
if (TYPEORDREALARME==0)
voir_qui_existe (0);





sortir:
TRAMEOUT[0]=0x02;


}

//#endif //#if defined(STACK_USE_GENERIC_TCP_SERVER_EXAMPLE)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////// CONVERSION UNSIGNED INT TO CHAR///////////////////////////////////////////////////////////////////////////////////////// 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void IntToChar(unsigned int value, char *chaine,char Precision)
{
unsigned int Mil, Cent, Diz, Unit, DMil;
int count = 0;

// initialisation des variables
Mil = 0; Cent = 0; Diz = 0; Unit = 0;DMil=0;
/*
if (value < 0) // si c'est un nombre n�gatif
    {
    value = value * (-1);
    *chaine = 45; // signe '-'
    count = 1;  // c'est un nombre n�gatif
    }
else // si c'est un nombre positif
    {
    count = 1;
    *chaine = 32; // code ASCII du signe espace ' '
    }
*/
// si la valeur n'est pas nulle
if (value != 0)
    {
if (Precision >= 5) // si l'utilisateur d�sire les milliers
         {
         // conversion des milliers
         DMil = value / 10000;
         if (DMil != 0)
               {
               *(chaine+count) = DMil + 48;
               if (*(chaine+count) < 48)
                     *(chaine+count) = 48;
               if (*(chaine+count) > 57)
                     *(chaine+count) = 57;
               }
         else
               *(chaine+count) = 48;
         count++;
         }
    if (Precision >= 4) // si l'utilisateur d�sire les milliers
         {
         // conversion des milliers
         Mil = value-(DMil*10000);
         Mil = Mil / 1000;
         if (Mil != 0)
               {
               *(chaine+count) = Mil + 48;
               if (*(chaine+count) < 48)
                     *(chaine+count) = 48;
               if (*(chaine+count) > 57)
                     *(chaine+count) = 57;
               }
         else
               *(chaine+count) = 48;
         count++;
         }

    if (Precision >= 3) // si l'utilisateur d�sire les centaines
        {
        // conversion des centaines
        Cent = value - (Mil * 1000)-(DMil*10000);
        Cent = Cent / 100;
        if (Cent != 0)
            {
            *(chaine+count) = Cent + 48;
            if (*(chaine+count) < 48)
                  *(chaine+count) = 48;
            if (*(chaine+count) > 57)
                  *(chaine+count) = 57;
            }
        else
            *(chaine+count) = 48;
            count++;
      }

    if (Precision >= 2) // si l'utilisateur d�sire les dizaines
         {
         // conversion des dizaines
         Diz = value - (Mil * 1000) - (Cent * 100)-(DMil*10000);
         Diz = Diz / 10;
         if (Diz != 0)
            {
            *(chaine+count) = Diz + 48;
            if (*(chaine+count) < 48)
                  *(chaine+count) = 48;
            if (*(chaine+count) > 57)
                  *(chaine+count) = 57;
            }
         else
            *(chaine+count) = 48;
         count++;
         }

    // conversion unit�s
    Unit = value - (Mil * 1000) - (Cent * 100) - (Diz * 10)-(DMil*10000);
    *(chaine+count) = Unit + 48;
    if (*(chaine+count) < 48)       // limites : 0 et 9
            *(chaine+count) = 48;
    if (*(chaine+count) > 57)
            *(chaine+count) = 57;
    }
else // if (value == 0)
    {
    //*(chaine) = 32; // ecrit un espace devant le nombre
    for (Mil=0;Mil<Precision;Mil++)   // inscription de '0' dans toute la chaine
          *(chaine+Mil) = 48;
    }

} // fin de la fonction de conversion INT => CHAR





/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// calcul du CRC pour /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CRC_POLY 0x01021 //0x01021
#define CRC_START 0xFFFF
unsigned int CRC16(char far *txt, unsigned  int lg_txt)
{ 
	 unsigned char nn;
	unsigned int ii;
	unsigned  int crc;
    
	 char far *p;
	crc = CRC_START;
	p=txt;

//essai[2]=*p;
ii=0;
nn=0;
	//for( ii=0; ii<lg_txt ; ii++, p++)
   do {
crc ^= (unsigned int)*p;
//for(; nn<8; nn++)
           do {
                  if (crc & 0x8000)
                     {
                         crc<<=1;
//UTIL;
                          crc^=CRC_POLY;
                      }
                      else
                      {crc<<=1;
                      }
                       nn++;
              }while (nn<8);
                      ii=ii+1;
                      p++;
if (nn==8){nn=0;}
     } while (ii<lg_txt);
ii=0;
return crc;
}



void delay_qms(char tempo)
{
delay_qmsip(tempo);
delay_qmsip(tempo);
}  

 
void delay_qmsip(char tempIP)
{
unsigned char j,k,l;
 
for (j=0;j<tempIP;j++)
  {
  for (k=0;k<10;k++)
    {
    for (l=0;l<40;l++)
      {
      //__no_operation();
      //__no_operation();
      //__no_operation();
      //__no_operation();
      //__no_operation();
      }
    }
  }  
j=0;
}



void ENVOYER_GROUPE (void)
{
unsigned char g;
LIRE_EEPROM(6,0,&TMP,3);
//"#CMIOZIG00018111752001071500000003000104200002070450000003000010550002070450000003000010550003071500000003000103000*222*�K�R~"
//#CMIOZIG000XXXXXXXX00107150000000300000250000207012300000300000040000207XXXXXXXXXXXXXXXXXXX003071500000003000003000*222**��"
//NE PAS PRENDRE LE 002 2x !!!!!!!!! prendre le premier

			//	if ((TMP[11]=='X')||((TMP[11]=='0')) //L'AUTOMATE N A PAS DONNE LHEURE 18032021
			//	{
					TMP[11]='2';
					TMP[12]='9';
					TMP[13]='1';
					TMP[14]='1';
					TMP[15]='0';
					TMP[16]='7';
					TMP[17]='1';
					TMP[18]='5';
			//	}


for (g=0;g<255;g++)
TRAMEOUT[g]='-';



TRAMEOUT[0]=0x02;
for (g=1;g<60;g++) 
{
TRAMEOUT[g]=TMP[g+7];
}



for (g=84;g<108;g++)
{
TRAMEOUT[g-24]=TMP[g+7];
}






LIRE_EEPROM(6,128,&TMP,3);

//"#CMIOZIGxxxxxxxxxxx004070000000000002400253005070000000000000003000006070000000000001430300008071000000000000024000*222*�K�R~"

for (g=84;g<204;g++) 
{
TRAMEOUT[g]=TMP[g-65];
}


 


//"#CMIOZIGxxxxxxxxxxx009070000000003000001300010070000000003000001300xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*222*�K�R~"

LIRE_EEPROM(6,256,&TMP,3);

for (g=180;g<228;g++) 
{
TRAMEOUT[g]=TMP[g-161];
}

 
//11092020
if((TRAMEOUT[196]=='1')&&(TRAMEOUT[197]=='6')&&(TRAMEOUT[198]=='0')&&(TRAMEOUT[199]=='0')){
	TRAMEOUT[196]='0';
	TRAMEOUT[197]='0';
	TRAMEOUT[199]='1';
}
else if((TRAMEOUT[196]=='0')&&(TRAMEOUT[197]=='9')&&(TRAMEOUT[198]=='0')&&(TRAMEOUT[199]=='0')){
	TRAMEOUT[197]='0';
}
else{
	TRAMEOUT[196]='?';
	TRAMEOUT[197]='?';
	TRAMEOUT[198]='?';
	TRAMEOUT[199]='?';	
}

if((TRAMEOUT[220]=='1')&&(TRAMEOUT[221]=='6')&&(TRAMEOUT[222]=='0')&&(TRAMEOUT[223]=='0')){
	TRAMEOUT[220]='0';
	TRAMEOUT[221]='0';
	TRAMEOUT[223]='1';
}
else if((TRAMEOUT[220]=='0')&&(TRAMEOUT[221]=='9')&&(TRAMEOUT[222]=='0')&&(TRAMEOUT[223]=='0')){
	TRAMEOUT[221]='0';
}
else{
	TRAMEOUT[220]='?';
	TRAMEOUT[221]='?';
	TRAMEOUT[223]='?';
	TRAMEOUT[224]='?';	
}


TRAMEOUT[228]=0x03; //ETX
CRC=CRC16(TRAMEOUT,229);
IntToChar(CRC,tableau,5);
TRAMEOUT[229]=tableau[0];
TRAMEOUT[230]=tableau[1];
TRAMEOUT[231]=tableau[2];
TRAMEOUT[232]=tableau[3];
TRAMEOUT[233]=tableau[4];
TRAMEOUT[234]=0x0D;






}




 
//Lecture dans l'EEPROM N�0 N�1 N�2 ou N�3 envoyer 000 ou 001......
unsigned char LIRE_EEPROM(unsigned char ADDE,unsigned int ADDHL,unsigned char *tab,char r)
{
char h;
unsigned char t;
unsigned char TMP2;
unsigned char TMP4;
unsigned char AddH,AddL;
 

//MAXERIE A2 

if (ADDE==8)
ADDE=16;
if (ADDE==7)
ADDE=14;
if (ADDE==6)
ADDE=12;
if (ADDE==5)
ADDE=10;
if (ADDE==4)
ADDE=8;
if (ADDE==3)
ADDE=6;
if (ADDE==2)
ADDE=4;
if (ADDE==1)
ADDE=2;


AddL=ADDHL;
AddH=ADDHL>>8;



TMP2=ADDE;
//TMP2=TMP2<<1;
TMP2=TMP2|0xA0;


   IdleI2C(); 
   StartI2C(); 
   //IdleI2C();	
   while ( SSPCON2bits.SEN ); 	

   WriteI2CC(TMP2); // 0 sur le bit RW pour indiquer une lecture
   IdleI2C();
  
        


    WriteI2CC(AddH); // 0 sur le bit RW pour indiquer une lecture
  IdleI2C();

  while ( SSPCON2bits.RSEN );   

	WriteI2CC(AddL); // 0 sur le bit RW pour indiquer une lecture
   IdleI2C();


	RestartI2C();
	while (SSPCON2bits.RSEN);
	

	//AckI2C();
	TMP2=TMP2|0x01;
 WriteI2CC(TMP2); // 1 sur le bit RW pour indiquer une lecture
   IdleI2C();
 
 
 
			//tab[85]='*';
			if (r==1) // pour trame memoire
			getsI2CC(tab,86);  
			if (r==0)	
			getsI2CC(tab,108);  
			if 	(r==3)	
			getsI2CC(tab,116); 
			if 	(r==4)	
			getsI2CC(tab,24); 
				if 	(r==5)	
			getsI2CC(tab,120); 
				if 	(r==6)	
			getsI2CC(tab,102); 
			  if (r == 7)
        getsI2CC(tab, 100);

   NotAckI2C(); 
   while(SSPCON2bits.ACKEN); 



   StopI2C();  
   while (SSPCON2bits.PEN);
   return (1); 
} 








unsigned char getsI2CC( unsigned char *rdptr, unsigned char length ) //COPIE 
{
    while ( length-- )           // perform getcI2C() for 'length' number of bytes
    {
      *rdptr++ = getcI2C();       // save byte received
      while ( SSPCON2bits.RCEN ); // check that receive sequence is over    

      if ( PIR2bits.BCLIF )       // test for bus collision
      {
        return ( -1 );            // return with Bus Collision error 
      }

	if( ((SSPCON1&0x0F)==0x08) || ((SSPCON1&0x0F)==0x0B) )	//master mode only
	{	
      if ( length )               // test if 'length' bytes have been read
      {
        SSPCON2bits.ACKDT = 0;    // set acknowledge bit state for ACK        
        SSPCON2bits.ACKEN = 1;    // initiate bus acknowledge sequence
        while ( SSPCON2bits.ACKEN ); // wait until ACK sequence is over 
      } 
	} 
	  
    }
    return ( 0 );                 // last byte received so don't send ACK      
}







unsigned char WriteI2CC( unsigned char data_out ) //COPIE
{
  SSPBUF = data_out;           // write single byte to SSPBUF
  if ( SSPCON1bits.WCOL )      // test if write collision occurred
   return ( -1 );              // if WCOL bit is set return negative #
  else
  {
	if( ((SSPCON1&0x0F)!=0x08) && ((SSPCON1&0x0F)!=0x0B) )	//Slave mode only
	{
	      SSPCON1bits.CKP = 1;        // release clock line 
	      while ( !PIR1bits.SSPIF );  // wait until ninth clock pulse received

	      if ( ( !SSPSTATbits.R_W ) && ( !SSPSTATbits.BF ) )// if R/W=0 and BF=0, NOT ACK was received
	      {
	        return ( -2 );           //return NACK
	      }
		  else
		  {
			return ( 0 );				//return ACK
		  }	
	}
	else if( ((SSPCON1&0x0F)==0x08) || ((SSPCON1&0x0F)==0x0B) )	//master mode only
	{ 
	    while( SSPSTATbits.BF );   // wait until write cycle is complete   
	    IdleI2C();                 // ensure module is idle
	    if ( SSPCON2bits.ACKSTAT ) // test for ACK condition received
	    	 return ( -2 );			// return NACK
		else return ( 0 );              //return ACK
	}
	
  }
}





void INITGESTIONCARTEIO (void)

{

unsigned char tabt;


TRISFbits.TRISF0=0; //EN SORTIE
TRISFbits.TRISF1=0;
TRISAbits.TRISA4=0;

//TRISHbits.TRISH5=0;
TRISDbits.TRISD3=0;
TRISDbits.TRISD2=0;


TRISH=0b11001101;
//ACCORDRS485=0;//!!!!!!!!!!!!!!

ADCON0=0x00;
ADCON1=0x0F; 

//int i=0;
//int j=0;
TRISB=0xFF;
//TRISH=0b11001101;
TRISCbits.TRISC5=0;
TRISCbits.TRISC7=1;
TRISCbits.TRISC6=0;
TRISAbits.TRISA4=0;
//RCONbits.IPEN = 1;
//INTCONbits.RBIE=1;
//INTCON3bits.INT3IE=1;
//INTCON3bits.INT1IE=1;
//INTCON2bits.INTEDG3=1;
PORTHbits.RH3=0;
//init_RS232();	

//init_modem ();	// A REMETTRE
														
Init_RS485();
Init_I2C();

I2CMEMO3=0;
I2CMEMO=1;
I2CMEMO2=1;
I2CINTR=1;
I2CHORLG=1;


//LIRE_EEPROM(6,0,&TMP,3);

}





void Init_I2C(void)
{
//	TRISCbits.TRISC3 = 1;
//	TRISCbits.TRISC4 = 1;

  //here is the I2C setup from the Seeval32 code.
	DDRCbits.RC3 = 1; //Configure SCL as Input
	DDRCbits.RC4 = 1; //Configure SDA as Input
	SSP1STAT = 0x00;   //Disable SMBus & Slew Rate Control 80
	SSP1CON1 = 0x28;  //Enable MSSP Master  28
	SSP1CON2 = 0x00;   //Clear MSSP Conr// 0x3B for 400kHz  (24mhz/((4*4OOkHz))-1 = 14 ou 3B en hexol Bits
	SSP1ADD  = 0x3B;    //0x0E
}

 
void Init_RS485(void)
{

unsigned int SPEED;

//TRISCbits.TRISC5 = 0;
TRISGbits.TRISG2 = 1;
//TRISCbits.TRISC6 = 0;
 TRISGbits.TRISG1 = 0; //RC5 en sortie	

TRISAbits.TRISA4 = 0; //RA4 RX_TX en sortie OU DERE

 

  RCSTA2bits.SPEN  = 1;       // Enable serial port
  TXSTA2bits.SYNC  = 0;       // Async mode	
  BAUDCON2bits.BRG16 = 1 ;
  
  TXSTA2bits.BRGH = 1;      //haute vitesse    
  SPEED = 1083;      	 		 // set 9600 bauds 650  // 207 28800bauds  1105
	
//  FOSC = 25 MHZ,
//41.6667 MHz internal
//(PRI_RUN HSPLL mode)
  SPBRG2 = SPEED ;     		// Write baudrate to SPBRG1
  SPBRGH2 = SPEED >> 8;		// For 16-bit baud rate generation


  IPR3bits.RC2IP  = 1;       // Set high priority
  PIR3bits.RC2IF = 0;        // met le drapeau d'IT � 0 (plus d'IT)
  PIE3bits.RC2IE = 0;        // ne g�n�re pas d'IT pour la liaison s�rie

  
//  IPR1bits.RCIP  = 1;       // Set high priority
//  PIR1bits.RCIF = 0;        // met le drapeau d'IT � 0 (plus d'IT)
//  PIE1bits.RCIE = 0;        // ne g�n�re pas d'IT pour la liaison s�rie
        
  TXSTA2bits.TXEN = 0;        // transmission inhib�e
  RCSTA2bits.RX9 = 0;         // r�ception sur 8 bits
  TXSTA2bits.TX9 = 0;           // transmission sur 8 bits
  RCSTA2bits.CREN  = 0;       // interdire reception
  RCONbits.IPEN  = 1;       // Enable priority levels 

}

 

void voir_qui_existe (char ES) //ES=2 trans E->S 1 S->E alarme ou ES=0 telgat normal 

{
char i,tpr,tpa,tda,j;
unsigned int l,longu;
char nb;
char ENVOYEROUPAS;
unsigned TMPTDA; 
tpr=0;
tpa=0;
tda=0;

//TYPECARTE[0]='M'; //NANCY
//TYPECARTE[1]='E';
//TYPECARTE[2]='E';




					for (i=1;i<=100;i+=1) //20 pour une carte de 1 � 99 	for (i=1;i<100;i+=1)
					{
pasdecarte:				
					//Tab_Voie_1[0]=i;
					//Tab_Voie_2[0]=i+1;
						if ((i>=1)&&(i<=20))
						{
							if ((TYPECARTE[0]!='E')&&(TYPECARTE[0]!='M')) //REAGARDE SI LA CARTE EXISTE ENCORE
							{
							i=21;
							goto pasdecarte;
							}
						}


						if ((i>=21)&&(i<=40))
						{
							if ((TYPECARTE[1]!='E')&&(TYPECARTE[1]!='M')) //REAGARDE SI LA CARTE EXISTE ENCORE
							{
							i=41;
							goto pasdecarte;
							}
						}
						if ((i>=41)&&(i<=60))
						{
							if ((TYPECARTE[2]!='E')&&(TYPECARTE[2]!='M')) //REAGARDE SI LA CARTE EXISTE ENCORE
							{
							i=61;
							goto pasdecarte;
							}
						}
						if ((i>=61)&&(i<=80))
						{
							if ((TYPECARTE[3]!='E')&&(TYPECARTE[3]!='M')) //REAGARDE SI LA CARTE EXISTE ENCORE
							{
							i=81;
							goto pasdecarte;
							}
						}
						if ((i>=81)&&(i<=100))
						{
							if ((TYPECARTE[4]!='E')&&(TYPECARTE[4]!='M')) //REAGARDE SI LA CARTE EXISTE ENCORE
							{
							//i=100;
							goto FINDETRANS;
							}
						}















					// VOIE IMPAIRE
					delay_qms(10);
					l=128*i+30000;
					LIRE_EEPROM(3,l,&TRAMEIN,4); //LIRE DANS EEPROM lexistance de toutes le voies



 
DEMANDE_INFOS_CAPTEURS[0]='#';
DEMANDE_INFOS_CAPTEURS[1]='0';
if (i>99)
{
DEMANDE_INFOS_CAPTEURS[1]='1';
DEMANDE_INFOS_CAPTEURS[2]='0';
DEMANDE_INFOS_CAPTEURS[3]='0';
}
else
{
DEMANDE_INFOS_CAPTEURS[2]=i/10+48;
DEMANDE_INFOS_CAPTEURS[3]=48+i-10*(DEMANDE_INFOS_CAPTEURS[2]-48);
}



///////////////// ENVOIE VOIE ADRESSABLE
		
ENVOYEROUPAS=0;	
		//if ((DEMANDE_INFOS_CAPTEURS[2]=='2')&&(DEMANDE_INFOS_CAPTEURS[3]=='9')) //TEST BUG 
		//TRAMEIN[2]='v';
	//	if ((DEMANDE_INFOS_CAPTEURS[2]=='2')&&(DEMANDE_INFOS_CAPTEURS[3]=='6')) //TEST BUG 
	//	j=0;
	//	if ((DEMANDE_INFOS_CAPTEURS[2]=='2')&&(DEMANDE_INFOS_CAPTEURS[3]=='1')) //TEST BUG 
	//	j=0;
		if (TRAMEIN[2]=='a') //voie meme carte adressable
		{
//		if (ES!=0)

		
			tpa=0;
			for (j=0;j<17;j++)
			{
		
			DEMANDE_INFOS_CAPTEURS[4]=j/10+48;
			DEMANDE_INFOS_CAPTEURS[5]=48+j-10*(DEMANDE_INFOS_CAPTEURS[4]-48);
			
	//		if (j==0)
	//		{	
	//		LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM
	//		TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(0,0); // 0 ADRESSABLE en ancien telgat 
			
	//		}			

 			
			if (TRAMEIN[3+j]=='A')
				{

				ENVOYEROUPAS=1;	
				tpa=tpa+1;
				LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM				
				TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(1,tpa); // 0 ADRESSABLE en ancien telgat dans la trameout
				//TRAMEOUT[20*j]=&TMP
				delay_qms(10);				
				}	
 		 
			} //TOUTE LA VOIE
			TMPTDA=tpa;

					  		TRAMEOUT[24*(TMPTDA-1)+36]=0x03; //ETX
			                CRC=CRC16(TRAMEOUT,24*(TMPTDA-1)+37); //taille totale
                                                                 	IntToChar(CRC,tableau,5);
                                                                    TRAMEOUT[24*(TMPTDA-1)+37]=tableau[0];
                                                                    TRAMEOUT[24*(TMPTDA-1)+38]=tableau[1];
                                                                    TRAMEOUT[24*(TMPTDA-1)+39]=tableau[2];
                                                                    TRAMEOUT[24*(TMPTDA-1)+40]=tableau[3];
                                                                    TRAMEOUT[24*(TMPTDA-1)+41]=tableau[4];
                                                                    TRAMEOUT[24*(TMPTDA-1)+42]=0x0D;
																	longu=24*(TMPTDA-1)+42;		
RECOMMENCER:


														if ((ALARMEATRANS==0xAA)||(ALARMEATRANS==0xBB)||(ALARMEATRANS==0xCC)||(ES==0)) //SI CHANGEMENT ALARME	
														{		
															if (ENVOYEROUPAS==1) 
																	{	
                                                                   TCPPutArray(MySocket, TRAMEOUT, longu+1); // ON ENVOIE LA VOIE LE CABLE max 396 16 TPA
																	//TRAMEENVOIRS485DONNEES (); //DEBUG RS485
                                                                    if (RECEPTTCPACK ())// ON RECOIT  A ACK OU PAS // ON RECOIT  A ACK OU PAS
																	goto FINDETRANS2;
																	}
														}

																  switch (TRAMEIN[0]) {
                                                                      case (0x06): // SI ACK ON PASSE AU CAPTEUR SUIVANT
                                                                     goto CONTINUER1;
                                                                      case (0x0f): // SI SI OU NOACK
                                                                     nb++;
                                                                     if (nb==2){ // ON ATTEND DEUX FOIS DAVOIR ACK SINON PROBLEME ON PASSE AU PROCHAIN CAPTEUR
                                                                     goto CONTINUER1;
                                                                     } 
                                                                      else
																	{
																	
																	goto RECOMMENCER;
																	} 
                                                                      default :
                                                                        break;
                                                                       }
                                                                     nb=0;
                                                                    /////////////////////////envoyer la valeur du deuxi�me capteur   







		} // FIN ENVOIE VOIE ADRESSABLE
		 

CONTINUER1:
		

 

				if (TRAMEIN[2]=='r') //voie meme carte resitive envoyer juste le premier capteur
				{
				tpa=1;
				DEMANDE_INFOS_CAPTEURS[4]='0';
				DEMANDE_INFOS_CAPTEURS[5]='1';
				LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM
				TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(1,tpa); // 0 ADRESSABLE en ancien telgat dans la trameout
					delay_qms(10);		
						TRAMEOUT[24*(tpa-1)+36]=0x03; //ETX
										
									                CRC=CRC16(TRAMEOUT,24*(tpa-1)+37); //taille totale
						                                                                 	IntToChar(CRC,tableau,5);
						                                                                    TRAMEOUT[24*(tpa-1)+37]=tableau[0];
						                                                                    TRAMEOUT[24*(tpa-1)+38]=tableau[1];
						                                                                    TRAMEOUT[24*(tpa-1)+39]=tableau[2];
						                                                                    TRAMEOUT[24*(tpa-1)+40]=tableau[3];
						                                                                    TRAMEOUT[24*(tpa-1)+41]=tableau[4];
						                                                                    TRAMEOUT[24*(tpa-1)+42]=0x0D;
																							longu=24*(tpa-1)+42;			
						
												if ((ALARMEATRANS==0xAA)||(ALARMEATRANS==0xBB)||(ALARMEATRANS==0xCC)||(ES==0)) //SI CHANGEMENT ALARME	
												{	
										                TCPPutArray(MySocket, TRAMEOUT, longu+1); // ON ENVOIE LA VOIE LE CABLE
													//	TRAMEENVOIRS485DONNEES (); //DEBUG 485
						      					}					                   
												        if (RECEPTTCPACK ())// ON RECOIT  A ACK OU PAS // ON RECOIT  A ACK OU PAS
														goto FINDETRANS2;
						
						
				
				}






if (TRAMEIN[2]=='v') //voie meme carte vide
{};
							if (TRAMEIN[2]=='c') //voie en court circuit
													{
						
							tpa=1;			
										DEMANDE_INFOS_CAPTEURS[4]='0';
										DEMANDE_INFOS_CAPTEURS[5]='1';
										//LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM
									//	TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(1,tpa); // 0 ADRESSABLE en ancien telgat dans la trameout	
							

										TRAMECAPTEURBIDON(&DEMANDE_INFOS_CAPTEURS);
										TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(1,tpa); // 0 ADRESSABLE en ancien telgat dans la trameout		
						
										delay_qms(10);		
											TRAMEOUT[26]='5';
											TRAMEOUT[27]='0';//S/R
											TRAMEOUT[28]='0';//0999mbar car codage 1 donc cc
											TRAMEOUT[29]='9';
											TRAMEOUT[30]='9';
											TRAMEOUT[31]='9';		
						
									  		TRAMEOUT[24*(tpa-1)+36]=0x03; //ETX
										
									                CRC=CRC16(TRAMEOUT,24*(tpa-1)+37); //taille totale
						                                                                 	IntToChar(CRC,tableau,5);
						                                                                    TRAMEOUT[24*(tpa-1)+37]=tableau[0];
						                                                                    TRAMEOUT[24*(tpa-1)+38]=tableau[1];
						                                                                    TRAMEOUT[24*(tpa-1)+39]=tableau[2];
						                                                                    TRAMEOUT[24*(tpa-1)+40]=tableau[3];
						                                                                    TRAMEOUT[24*(tpa-1)+41]=tableau[4];
						                                                                    TRAMEOUT[24*(tpa-1)+42]=0x0D;
																							longu=24*(tpa-1)+42;	
						
										if ((ALARMEATRANS==0xAA)||(ALARMEATRANS==0xBB)||(ALARMEATRANS==0xCC)||(ES==0)) //SI CHANGEMENT ALARME	
										{																		
										                TCPPutArray(MySocket, TRAMEOUT, longu+1); // ON ENVOIE LA VOIE LE CABLE
													//	TRAMEENVOIRS485DONNEES (); //DEBUG 485
										}
						                                if (RECEPTTCPACK ())// ON RECOIT  A ACK OU PAS // ON RECOIT  A ACK OU PAS
														goto FINDETRANS2;
						
						
						
						}






						if (TRAMEIN[2]=='h') //fusible HS voies P ou IMP
						{
						
							tpa=1;
										DEMANDE_INFOS_CAPTEURS[4]='0';
										DEMANDE_INFOS_CAPTEURS[5]='1';
									

										//LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM


										TRAMECAPTEURBIDON(&DEMANDE_INFOS_CAPTEURS);
										TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(1,tpa); // 0 ADRESSABLE en ancien telgat dans la trameout	
										
																
									
													delay_qms(10);		
											TRAMEOUT[26]='5';
											TRAMEOUT[27]='0';//S/R
											TRAMEOUT[28]='0';//0888mbar car codage 1 donc fusible hs
											TRAMEOUT[29]='8';
											TRAMEOUT[30]='8';
											TRAMEOUT[31]='8';		
						
									  		TRAMEOUT[24*(tpa-1)+36]=0x03; //ETX
										
									                CRC=CRC16(TRAMEOUT,24*(tpa-1)+37); //taille totale
						                                                                 	IntToChar(CRC,tableau,5);
						                                                                    TRAMEOUT[24*(tpa-1)+37]=tableau[0];
						                                                                    TRAMEOUT[24*(tpa-1)+38]=tableau[1];
						                                                                    TRAMEOUT[24*(tpa-1)+39]=tableau[2];
						                                                                    TRAMEOUT[24*(tpa-1)+40]=tableau[3];
						                                                                    TRAMEOUT[24*(tpa-1)+41]=tableau[4];
						                                                                    TRAMEOUT[24*(tpa-1)+42]=0x0D;
																							longu=24*(tpa-1)+42;			
						
												
											if ((ALARMEATRANS==0xAA)||(ALARMEATRANS==0xBB)||(ALARMEATRANS==0xCC)||(ES==0)) //SI CHANGEMENT ALARME	
											{		
										                TCPPutArray(MySocket, TRAMEOUT, longu+1); // ON ENVOIE LA VOIE LE CABLE
													//	TRAMEENVOIRS485DONNEES (); //DEBUG 485
						     				}                
										                if (RECEPTTCPACK ())// ON RECOIT  A ACK OU PAS // ON RECOIT  A ACK OU PAS
														goto FINDETRANS2;
						
						
						
						}






				if (TRAMEIN[2]=='i') //adressable sur carte resistive ou a voir
				{

				tpa=1;
				DEMANDE_INFOS_CAPTEURS[4]='0';
				DEMANDE_INFOS_CAPTEURS[5]='1';
			//	LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM
			//	TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(1,tpa); // 0 ADRESSABLE en ancien telgat dans la trameout	
			

										TRAMECAPTEURBIDON(&DEMANDE_INFOS_CAPTEURS);
										TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(1,tpa); // 0 ADRESSABLE en ancien telgat dans la trameout	

				delay_qms(10);		
					TRAMEOUT[26]='5';
					TRAMEOUT[27]='0';//S/R
					TRAMEOUT[28]='0';//0777mbar car codage 1 donc incident
					TRAMEOUT[29]='7';
					TRAMEOUT[30]='7';
					TRAMEOUT[31]='7';		

			  		TRAMEOUT[24*(tpa-1)+36]=0x03; //ETX
				
			                CRC=CRC16(TRAMEOUT,24*(tpa-1)+37); //taille totale
                                                                 	IntToChar(CRC,tableau,5);
                                                                    TRAMEOUT[24*(tpa-1)+37]=tableau[0];
                                                                    TRAMEOUT[24*(tpa-1)+38]=tableau[1];
                                                                    TRAMEOUT[24*(tpa-1)+39]=tableau[2];
                                                                    TRAMEOUT[24*(tpa-1)+40]=tableau[3];
                                                                    TRAMEOUT[24*(tpa-1)+41]=tableau[4];
                                                                    TRAMEOUT[24*(tpa-1)+42]=0x0D;
																	longu=24*(tpa-1)+42;	

								
					if ((ALARMEATRANS==0xAA)||(ALARMEATRANS==0xBB)||(ALARMEATRANS==0xCC)||(ES==0)) //SI CHANGEMENT ALARME	
					{
				                TCPPutArray(MySocket, TRAMEOUT, longu+1); // ON ENVOIE LA VOIE LE CABLE
								//TRAMEENVOIRS485DONNEES (); //DEBUG 485
     				}               
					            if (RECEPTTCPACK ())// ON RECOIT  A ACK OU PAS // ON RECOIT  A ACK OU PAS
								goto FINDETRANS2;

				}







//LIRE_EEPROM(choix_memoire(&TRAMEOUT),calcul_addmemoire(&TRAMEOUT),&TRAMEOUT); //LIRE DANS EEPROM


FINDETRANS:

delay_qms(10);
delay_qms(10);
 	
				
					} // FIN FOR
FINDETRANS2:

delay_qms(10);

}



void TRAMECAPTEURBIDON(char *taz)
{

TMP[0]='#';//#
TMP[1]=taz[1];
TMP[2]=taz[2];
TMP[3]=taz[3];
TMP[4]=taz[4];
TMP[5]=taz[5];
TMP[6]='A';
TMP[7]='0';
TMP[8]='0';
TMP[9]='0';
TMP[10]='0';
TMP[11]='0';
TMP[12]='0';
TMP[13]='0';
TMP[14]='0';
TMP[15]='0';
TMP[16]='0';
TMP[17]='0';
TMP[18]='0';
TMP[19]='P';
TMP[20]='R';
TMP[21]='O';
TMP[22]='B';
TMP[23]='L';
TMP[24]='E';
TMP[25]='M';
TMP[26]='E';
TMP[27]=' ';
TMP[28]=' ';
TMP[29]=' ';
TMP[30]=' ';
TMP[31]=' ';
TMP[32]=' ';
TMP[33]=' ';
TMP[34]=' ';
TMP[35]=' ';
TMP[36]=' ';
TMP[37]=' ';
TMP[38]=' ';
TMP[39]=' ';
TMP[40]=' ';
TMP[41]=' ';
TMP[42]=' ';
TMP[43]=' ';
TMP[44]=' ';
TMP[45]=' ';
TMP[46]=' ';
TMP[47]=' ';
TMP[48]=' ';
TMP[49]=' ';
TMP[50]=taz[1];
TMP[51]=taz[2];
TMP[52]=taz[3];
TMP[53]='2';
TMP[54]='9';
TMP[55]='1';
TMP[56]='1';
TMP[57]='0';
TMP[58]='7'; 
TMP[59]='1';
TMP[60]='5'; //18032021
TMP[61]='?';
TMP[62]='0';
TMP[63]='1';
TMP[64]='0';
TMP[65]='2';
TMP[66]='?';
TMP[67]='?';
TMP[68]='?';
TMP[69]='?';
TMP[70]='?';
TMP[71]='?';
TMP[72]='?';
TMP[73]='?';
TMP[74]='?';
TMP[75]='5';
TMP[76]='0';
TMP[77]='0';
TMP[78]='0';
TMP[79]='0';
TMP[80]='0';
TMP[81]='0';
TMP[82]='0';
TMP[83]='0';
TMP[84]='0';
TMP[85]='2';
TMP[86]='2';
TMP[87]='7';
TMP[88]='6';
TMP[89]='7';
TMP[90]='*';


}



char RECEPTTCPACK (void)

{




for (nbrerecep=0;nbrerecep<10;nbrerecep++) //NANCY
{
TRAMEIN[nbrerecep]='#';
}
iboucle=0;
nbrerecep=0;
ete1:
do
{
if(!TCPIsConnected(MySocket))
return(1); //DECONNECT
	StackTask();     
	StackApplications(); 
	wMaxGet = TCPIsGetReady(MySocket);	// Get TCP RX FIFO byte count
	wMaxPut = TCPIsPutReady(MySocket);	// Get TCP TX FIFO free space
}while (wMaxGet==0);
TCPGetArray(MySocket, TRAMEIN,2);



/* DEBUG 485
TRAMEOUT[0]='#';
for (nbrerecep=1;nbrerecep<4;nbrerecep++) //NANCY
{
TRAMEOUT[nbrerecep]=TRAMEIN[nbrerecep-1];
}
TRAMEOUT[4]='*';
TRAMEENVOIRS485DONNEES (); //NANCY
*/
iboucle++;
//if (iboucle<=3)
//goto ete1;




//TEMPO(15); //BART
if (TRAMEIN[0]!=0x06) //PAS ACK
{
nbrerecep++;
goto ete1;
}
if (TRAMEIN[0]==0x06) //NO ACK
{
nbrerecep=100;
//goto close;
}



// if (tuiyo<20) //09012019
//	{
//		goto ete1;
//	}
//}


return(0);



}



void TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(char c,char n) // que faire,numero cod
{

char k;
unsigned int temporaire;
// 0 trame ancien telgat debut N�cable J M H M ?(0)


							//if (c==0)
						
							TRAMEOUT[0]=0x02; //STX
							TRAMEOUT[1]=TMP[50]; //N�cable
							TRAMEOUT[2]=TMP[51];
							TRAMEOUT[3]=TMP[52];
							TRAMEOUT[4]=TMP[53]; //J
							TRAMEOUT[5]=TMP[54];
							TRAMEOUT[6]=TMP[55]; //M
							TRAMEOUT[7]=TMP[56];
							TRAMEOUT[8]=TMP[57]; //H
							TRAMEOUT[9]=TMP[58];		
							TRAMEOUT[10]=TMP[59]; //M
							TRAMEOUT[11]=TMP[60];	
							//TRAMEOUT[12]=TMP[61]; // ?	
	
				
//TRAMEOUT[4]='2'; //J							
//TRAMEOUT[6]='1'; //M


// 1 trame ancien telgat COD	POS	????	DISTANCE	ET	VALEUR	SEUIL


							if (c==1)
							{
						
							if ((TMP[19]=='v')&&(TMP[20]=='i')&&(TMP[21]=='r')&&(TMP[22]=='t')&&(TMP[23]=='u')&&(TMP[24]=='a')&&(TMP[25]=='l')) //capteur fictif demande de lopez
							TMP[65]='3'; //CAPTEUR TEMPERATURE
						

	
							for (k=0;k<24;k++) // ON RAJOUTE CAPTEUR 0 ,puis 1 ,puis 2
							{

							if (TMP[61+k]=='?')
							TMP[61+k]='0';
							if (n==0)
							temporaire=0;
							if (n==1)
							temporaire=0;
							if (n==2)
							temporaire=24;
							if (n==3)
							temporaire=48;
							if (n==4)
							temporaire=72;
							if (n==5)
							temporaire=96;
							if (n==6)
							temporaire=120;
								if (n==7)
							temporaire=144;
								if (n==8)
							temporaire=168;
								if (n==9)
							temporaire=192;
								if (n==10)
							temporaire=216;
								if (n==11)
							temporaire=240;
								if (n==12)
							temporaire=264;
								if (n==13)
							temporaire=288;
								if (n==14)
							temporaire=312;
								if (n==15)
							temporaire=336;
								if (n==16)
							temporaire=360;
								if (n==17)
							temporaire=384;
	
							//temporaire=(n-1)*24;
							temporaire=12+k+temporaire;
							TRAMEOUT[temporaire]=TMP[61+k];
//Modification / Rustine pour corriger le probl�me de distance 11092020
							if(((12+k)>=21)&&((12+k)<=25)){
								if ((TRAMEOUT[temporaire]<48)||(TRAMEOUT[temporaire]>57))
									TRAMEOUT[temporaire]='?';
							}

											
							//00101010000 ?0102????456783100000888 ?0203?????????3000000800 ?0405?????????3000000800
									//avec ?0102 01=codage et 02=Type TA (pos) rq:les ?=0

									//si pos =01 c'est un DEBIT avec codage=00 
							}
							

							
											


							}





 
 
 
 

}


// CALCUL EMPLACEMENT MEMOIRE
unsigned int calcul_addmemoire (unsigned char *tab)
{
unsigned int t;
unsigned int i;
unsigned int r;

 

t=0;
i=tab[3];
i=i-48;
t=t+1*i;
i=tab[2];
i=i-48;
t=t+10*i;
i=tab[1];
i=i-48;
t=t+100*i;


r=2176*(t);
if (t>29)
r=2176*(t-30);
if (t>59)
r=2176*(t-60);
if (t>89)
r=2176*(t-90);


t=0;
i=tab[5]-48;
t=1*i;
i=tab[4]-48;
t=t+10*i;
i=t;

t=r+128*i;



delay_qms(10);



return t;


}


// choix du numero memoire suivant N�voie
unsigned char choix_memoire(unsigned char *tab)
{
unsigned int i;
unsigned int t;
unsigned char r;




t=0;
i=tab[3]-48;
t=t+1*i;
i=tab[2]-48;
t=t+10*i;
i=tab[1]-48;
t=t+100*i;

r=0;
if (t>29)
r=1;
if (t>59)
r=2;
if (t>89)
r=3;




delay_qms(10);
return r;

}





///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////// 1 ERE PAGE DU MINITEL///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void page (char num)
{

int j,iii;
if (num == 'I')
{
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////page mot de passe///////////////////////////////////////////////////////////

TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='4';
TRAMEOUT[3]='a';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x39;
TRAMEOUT[6]=0x7F;
TRAMEOUT[7]=0x1B;
TRAMEOUT[8]=0x3A;
TRAMEOUT[9]=0x32;
TRAMEOUT[10]=0x7D;
TRAMEOUT[11]=0x1B;
TRAMEOUT[12]=0x5B;
TRAMEOUT[13]=0x32;
TRAMEOUT[14]=0x4A;
TRAMEOUT[15]=0x1B;
TRAMEOUT[16]=0x5B;
TRAMEOUT[17]=0x32;
TRAMEOUT[18]=0x6D;
TRAMEOUT[19]=0x1B;
TRAMEOUT[20]=0x5B;
TRAMEOUT[21]=0x35;
TRAMEOUT[22]=0x3B;
TRAMEOUT[23]=0x33;
TRAMEOUT[24]=0x33;
TRAMEOUT[25]=0x48;
TRAMEOUT[26]=0x54;
TRAMEOUT[27]=0x79;
TRAMEOUT[28]=0x70;
TRAMEOUT[29]=0x65;
TRAMEOUT[30]=0x20;
TRAMEOUT[31]=0x43;
TRAMEOUT[32]=0x6F;
TRAMEOUT[33]=0x6E;
TRAMEOUT[34]=0x6E;
TRAMEOUT[35]=0x65;
TRAMEOUT[36]=0x78;
TRAMEOUT[37]=0x69;
TRAMEOUT[38]=0x6F;
TRAMEOUT[39]=0x6E;
TRAMEOUT[40]=0x20;
TRAMEOUT[41]=0x3A;
TRAMEOUT[42]=0x1B;
TRAMEOUT[43]=0x5B;
TRAMEOUT[44]=0x30;
TRAMEOUT[45]=0x6D;
TRAMEOUT[46]=0x1B;
TRAMEOUT[47]=0x5B;
TRAMEOUT[48]=0x37;
TRAMEOUT[49]=0x3B;
TRAMEOUT[50]=0x33;
TRAMEOUT[51]=0x38;//05102020
TRAMEOUT[52]=0x48;
TRAMEOUT[53]=0x1B;
TRAMEOUT[54]=0x5B;
TRAMEOUT[55]=0x37;
TRAMEOUT[56]=0x6D;
TRAMEOUT[57]=0x20;
TRAMEOUT[58]=0x49; //RTC IP 14092020
TRAMEOUT[59]=0x2E;
TRAMEOUT[60]=0x50;
TRAMEOUT[61]=0x20;
TRAMEOUT[62]=0x1B;
TRAMEOUT[63]=0x5B;
TRAMEOUT[64]=0x30;
TRAMEOUT[65]=0x6D;
TRAMEOUT[66]=0x1B;
TRAMEOUT[67]=0x5B;
TRAMEOUT[68]=0x31;
TRAMEOUT[69]=0x30;
TRAMEOUT[70]=0x3B;
TRAMEOUT[71]=0x33;
TRAMEOUT[72]=0x38;//05102020
TRAMEOUT[73]=0x48;
TRAMEOUT[74]=0x53;
TRAMEOUT[75]=0x69;
TRAMEOUT[76]=0x74;
TRAMEOUT[77]=0x65;
TRAMEOUT[78]=0x20;
TRAMEOUT[79]=0x3A;
TRAMEOUT[80]=0x1B;
TRAMEOUT[81]=0x5B;
TRAMEOUT[82]=0x37;
TRAMEOUT[83]=0x6D;
TRAMEOUT[84]=0x1B;
TRAMEOUT[85]=0x5B;
TRAMEOUT[86]=0x30;///////////////////31
TRAMEOUT[87]=0x33;///////////////////32
TRAMEOUT[88]=0x3B;
TRAMEOUT[89]=0x32;
TRAMEOUT[90]=0x33;//05102020
TRAMEOUT[91]=0x48;
TRAMEOUT[92]=0x20;
TRAMEOUT[93]=0x41;  //NOM DU SITE
TRAMEOUT[94]=0x44;
TRAMEOUT[95]=0x45;
TRAMEOUT[96]=0x43;
TRAMEOUT[97]=0x45;
TRAMEOUT[98]=0x46;
TRAMEOUT[99]=0x20;
TRAMEOUT[100]=0x54;
TRAMEOUT[101]=0x45;
TRAMEOUT[102]=0x43;
TRAMEOUT[103]=0x48;
TRAMEOUT[104]=0x4E;
TRAMEOUT[105]=0x4F;
TRAMEOUT[106]=0x4C;
TRAMEOUT[107]=0x4F;
TRAMEOUT[108]=0x47;
TRAMEOUT[109]=0x59;
TRAMEOUT[110]=0x20;
TRAMEOUT[111]=0x1B;
TRAMEOUT[112]=0x5B;
TRAMEOUT[113]=0x30;//21092020
TRAMEOUT[114]=0x6D;
TRAMEOUT[115]=0x1B;
TRAMEOUT[116]=0x5B;
TRAMEOUT[117]=0x33;
TRAMEOUT[118]=0x3B;
TRAMEOUT[119]=0x34;//05102020
TRAMEOUT[120]=0x32;//21092020

TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<121;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='4';
TRAMEOUT[3]='b';
TRAMEOUT[4]=0x48;
TRAMEOUT[5]=0x20;
TRAMEOUT[6]=0x4E;
TRAMEOUT[7]=0x4F;
TRAMEOUT[8]=0x55;
TRAMEOUT[9]=0x56;
TRAMEOUT[10]=0x45;
TRAMEOUT[11]=0x41;
TRAMEOUT[12]=0x55;
TRAMEOUT[13]=0x20;
TRAMEOUT[14]=0x43;
TRAMEOUT[15]=0x4D;
TRAMEOUT[16]=0x53;
TRAMEOUT[17]=0x20;
TRAMEOUT[18]=0x32;
TRAMEOUT[19]=0x2E;
TRAMEOUT[20]=0x30;
TRAMEOUT[21]=0x20;
TRAMEOUT[22]=0x1B;
TRAMEOUT[23]=0x5B;
TRAMEOUT[24]=0x30;
TRAMEOUT[25]=0x6D;
TRAMEOUT[26]=0x20;
TRAMEOUT[27]=0x1B;
TRAMEOUT[28]=0x5B;
TRAMEOUT[29]=0x31;
TRAMEOUT[30]=0x38;
TRAMEOUT[31]=0x3B;
TRAMEOUT[32]=0x33;
TRAMEOUT[33]=0x34;
TRAMEOUT[34]=0x48;
TRAMEOUT[35]=0x20;
TRAMEOUT[36]=0x4D;
TRAMEOUT[37]=0x6F;
TRAMEOUT[38]=0x74;
TRAMEOUT[39]=0x20;
TRAMEOUT[40]=0x64;
TRAMEOUT[41]=0x65;
TRAMEOUT[42]=0x20;
TRAMEOUT[43]=0x70;
TRAMEOUT[44]=0x61;
TRAMEOUT[45]=0x73;
TRAMEOUT[46]=0x73;
TRAMEOUT[47]=0x65;
TRAMEOUT[48]=0x20;//05102020
TRAMEOUT[49]=0x3A;
TRAMEOUT[50]=0x1B;
TRAMEOUT[51]=0x5B;
TRAMEOUT[52]=0x37;
TRAMEOUT[53]=0x6D;
TRAMEOUT[54]=0x1B;
TRAMEOUT[55]=0x5B;
TRAMEOUT[56]=0x32;
TRAMEOUT[57]=0x30;
TRAMEOUT[58]=0x3B;
TRAMEOUT[59]=0x33;
TRAMEOUT[60]=0x39;//05102020
TRAMEOUT[61]=0x48;
TRAMEOUT[62]=0x20;
TRAMEOUT[63]=0x20;
TRAMEOUT[64]=0x20;
TRAMEOUT[65]=0x20;
TRAMEOUT[66]=0x1B;
TRAMEOUT[67]=0x5B;
TRAMEOUT[68]=0x31;
TRAMEOUT[69]=0x38;
TRAMEOUT[70]=0x3B;
TRAMEOUT[71]=0x35;
TRAMEOUT[72]=0x35;
TRAMEOUT[73]=0x48;
TRAMEOUT[74]=0x1B;
TRAMEOUT[75]=0x5B;
TRAMEOUT[76]=0x30;
TRAMEOUT[77]=0x6D;
TRAMEOUT[78]=0x1B;
TRAMEOUT[79]=0x5B;
TRAMEOUT[80]=0x31;
TRAMEOUT[81]=0x3B;
TRAMEOUT[82]=0x35;
TRAMEOUT[83]=0x39;
TRAMEOUT[84]=0x48;//DATE
TRAMEOUT[85]=0x20; //J
TRAMEOUT[86]=0x20; //J
TRAMEOUT[87]=0x20; // / 2F
TRAMEOUT[88]=0x20; //M
TRAMEOUT[89]=0x20; //M 
TRAMEOUT[90]=0x20; // /
TRAMEOUT[91]=0x20; //2
TRAMEOUT[92]=0x20; //0
TRAMEOUT[93]=0x20; //0
TRAMEOUT[94]=0x20; //0
TRAMEOUT[95]=0x20;
TRAMEOUT[96]=0x20;
TRAMEOUT[97]=0x20;  //H
TRAMEOUT[98]=0x20; //H
TRAMEOUT[99]=0x20; // :
TRAMEOUT[100]=0x20; //m
TRAMEOUT[101]=0x20; //m
TRAMEOUT[102]=0x1B;
TRAMEOUT[103]=0x5B;
TRAMEOUT[104]=0x32;
TRAMEOUT[105]=0x30;
TRAMEOUT[106]=0x3B;
TRAMEOUT[107]=0x33;
TRAMEOUT[108]=0x34;
TRAMEOUT[109]=0x48;
TRAMEOUT[110]='*';

 

j=0;
for (iii=4;iii<111;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 107); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (106);


}
else if (num == '1')
{
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////page 1 du minitel actuel ///////////////////////////////////////////////////////
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='1';
TRAMEOUT[3]='a';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x32;
TRAMEOUT[7]=0x4A;
TRAMEOUT[8]=0x1B;
TRAMEOUT[9]=0x5B;
TRAMEOUT[10]=0x31;
TRAMEOUT[11]=0x3B;
TRAMEOUT[12]=0x33;
TRAMEOUT[13]=0x38;//05102020
TRAMEOUT[14]=0x48;
TRAMEOUT[15]=0x53;
TRAMEOUT[16]=0x69;
TRAMEOUT[17]=0x74;
TRAMEOUT[18]=0x65;
TRAMEOUT[19]=0x20;//05102020
TRAMEOUT[20]=0x3A;//05102020
TRAMEOUT[21]='*';
j=0;
for (iii=4;iii<21;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 110); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (17);
}
else if (num == '2')
{
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////page 2 du minitel actuel ///////////////////////////////////////////////////////
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='2';
TRAMEOUT[3]='a';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x32;
TRAMEOUT[7]=0x4A;
TRAMEOUT[8]=0x1B;
TRAMEOUT[9]=0x5B;
TRAMEOUT[10]=0x31;
TRAMEOUT[11]=0x3B;
TRAMEOUT[12]=0x33;
TRAMEOUT[13]=0x38;
TRAMEOUT[14]=0x48;
TRAMEOUT[15]=0x53;
TRAMEOUT[16]=0x69;
TRAMEOUT[17]=0x74;
TRAMEOUT[18]=0x65;
TRAMEOUT[19]=0x20;//05102020
TRAMEOUT[20]=0x3A;//05102020
TRAMEOUT[21]=0x20;
TRAMEOUT[22]=0x20;
TRAMEOUT[23]=0x20;
TRAMEOUT[24]=0x1B;
TRAMEOUT[25]=0x5B;
TRAMEOUT[26]=0x31;
TRAMEOUT[27]=0x3B;
TRAMEOUT[28]=0x32;
TRAMEOUT[29]=0x48;
TRAMEOUT[30]=0x0A;
TRAMEOUT[31]=0x0A;
TRAMEOUT[32]=0x4C;
TRAMEOUT[33]=0x69;
TRAMEOUT[34]=0x73;
TRAMEOUT[35]=0x74;
TRAMEOUT[36]=0x65;
TRAMEOUT[37]=0x20;
TRAMEOUT[38]=0x64;
TRAMEOUT[39]=0x65;
TRAMEOUT[40]=0x73;
TRAMEOUT[41]=0x20;
TRAMEOUT[42]=0x61;
TRAMEOUT[43]=0x6C;
TRAMEOUT[44]=0x61;
TRAMEOUT[45]=0x72;
TRAMEOUT[46]=0x6D;
TRAMEOUT[47]=0x65;
TRAMEOUT[48]=0x73;
TRAMEOUT[49]=0x20;
TRAMEOUT[50]=0x65;
TRAMEOUT[51]=0x6E;
TRAMEOUT[52]=0x20;
TRAMEOUT[53]=0x63;
TRAMEOUT[54]=0x6F;
TRAMEOUT[55]=0x75;
TRAMEOUT[56]=0x72;
TRAMEOUT[57]=0x73;
TRAMEOUT[58]=0x3A;
TRAMEOUT[59]=0x1B;
TRAMEOUT[60]=0x5B;
TRAMEOUT[61]=0x30;
TRAMEOUT[62]=0x4B;
TRAMEOUT[63]=0x1B;
TRAMEOUT[64]=0x5B;
TRAMEOUT[65]=0x33;
TRAMEOUT[66]=0x3B;
TRAMEOUT[67]=0x32;
TRAMEOUT[68]=0x39;
TRAMEOUT[69]=0x48;
TRAMEOUT[70]=0x1B;
TRAMEOUT[71]=0x5B;
TRAMEOUT[72]=0x31;
TRAMEOUT[73]=0x3B;
TRAMEOUT[74]=0x32;
TRAMEOUT[75]=0x48;
TRAMEOUT[76]=0x0A;
TRAMEOUT[77]=0x0A;
TRAMEOUT[78]=0x0A;
TRAMEOUT[79]=0x0A;
TRAMEOUT[80]=0x2D;
TRAMEOUT[81]=0x2D;
TRAMEOUT[82]=0x2D;
TRAMEOUT[83]=0x2D;
TRAMEOUT[84]=0x2D;
TRAMEOUT[85]=0x2D;
TRAMEOUT[86]=0x2D;
TRAMEOUT[87]=0x2D;
TRAMEOUT[88]=0x2D;
TRAMEOUT[89]=0x2D;
TRAMEOUT[90]=0x2D;
TRAMEOUT[91]=0x2D;
TRAMEOUT[92]=0x2D;
TRAMEOUT[93]=0x2D;
TRAMEOUT[94]=0x2D;
TRAMEOUT[95]=0x2D;
TRAMEOUT[96]=0x2D;
TRAMEOUT[97]=0x2D;
TRAMEOUT[98]=0x2D;
TRAMEOUT[99]=0x2D;
TRAMEOUT[100]=0x2D;
TRAMEOUT[101]=0x2D;
TRAMEOUT[102]=0x2D;
TRAMEOUT[103]=0x2D;
TRAMEOUT[104]=0x2D;
TRAMEOUT[105]=0x2D;
TRAMEOUT[106]=0x2D;
TRAMEOUT[107]=0x2D;
TRAMEOUT[108]=0x2D;
TRAMEOUT[109]=0x2D;
TRAMEOUT[110]=0x2D;
TRAMEOUT[111]=0x2D;
TRAMEOUT[112]=0x2D;
TRAMEOUT[113]=0x2D;
TRAMEOUT[114]=0x2D;
TRAMEOUT[115]=0x2D;
TRAMEOUT[116]=0x2D;
TRAMEOUT[117]=0x2D;
TRAMEOUT[118]=0x2D;
TRAMEOUT[119]=0x2D;
TRAMEOUT[120]=0x2D;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='2';
TRAMEOUT[3]='b';
TRAMEOUT[4]=0x2D;
TRAMEOUT[5]=0x2D;
TRAMEOUT[6]=0x2D;
TRAMEOUT[7]=0x2D;
TRAMEOUT[8]=0x2D;
TRAMEOUT[9]=0x2D;
TRAMEOUT[10]=0x2D;
TRAMEOUT[11]=0x2D;
TRAMEOUT[12]=0x2D;
TRAMEOUT[13]=0x2D;
TRAMEOUT[14]=0x2D;
TRAMEOUT[15]=0x2D;
TRAMEOUT[16]=0x2D;
TRAMEOUT[17]=0x2D;
TRAMEOUT[18]=0x2D;
TRAMEOUT[19]=0x2D;
TRAMEOUT[20]=0x2D;
TRAMEOUT[21]=0x2D;
TRAMEOUT[22]=0x2D;
TRAMEOUT[23]=0x2D;
TRAMEOUT[24]=0x2D;
TRAMEOUT[25]=0x2D;
TRAMEOUT[26]=0x2D;
TRAMEOUT[27]=0x2D;
TRAMEOUT[28]=0x2D;
TRAMEOUT[29]=0x2D;
TRAMEOUT[30]=0x2D;
TRAMEOUT[31]=0x2D;
TRAMEOUT[32]=0x1B;
TRAMEOUT[33]=0x5B;
TRAMEOUT[34]=0x36;
TRAMEOUT[35]=0x3B;
TRAMEOUT[36]=0x32;
TRAMEOUT[37]=0x48;
TRAMEOUT[38]=0x21;
TRAMEOUT[39]=0x20;
TRAMEOUT[40]=0x54;
TRAMEOUT[41]=0x59;
TRAMEOUT[42]=0x50;
TRAMEOUT[43]=0x45;
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x44;
TRAMEOUT[46]=0x27;
TRAMEOUT[47]=0x41;
TRAMEOUT[48]=0x4C;
TRAMEOUT[49]=0x41;
TRAMEOUT[50]=0x52;
TRAMEOUT[51]=0x4D;
TRAMEOUT[52]=0x45;
TRAMEOUT[53]=0x20;
TRAMEOUT[54]=0x20;
TRAMEOUT[55]=0x20;
TRAMEOUT[56]=0x20;
TRAMEOUT[57]=0x20;
TRAMEOUT[58]=0x21;
TRAMEOUT[59]=0x20;
TRAMEOUT[60]=0x50;
TRAMEOUT[61]=0x2E;
TRAMEOUT[62]=0x47;
TRAMEOUT[63]=0x72;
TRAMEOUT[64]=0x6F;
TRAMEOUT[65]=0x75;
TRAMEOUT[66]=0x70;
TRAMEOUT[67]=0x65;
TRAMEOUT[68]=0x20;
TRAMEOUT[69]=0x21;
TRAMEOUT[70]=0x20;
TRAMEOUT[71]=0x54;
TRAMEOUT[72]=0x50;
TRAMEOUT[73]=0x2E;
TRAMEOUT[74]=0x41;
TRAMEOUT[75]=0x44;
TRAMEOUT[76]=0x52;
TRAMEOUT[77]=0x45;
TRAMEOUT[78]=0x53;
TRAMEOUT[79]=0x53;
TRAMEOUT[80]=0x41;
TRAMEOUT[81]=0x42;
TRAMEOUT[82]=0x4C;
TRAMEOUT[83]=0x45;
TRAMEOUT[84]=0x20;
TRAMEOUT[85]=0x21;
TRAMEOUT[86]=0x20;
TRAMEOUT[87]=0x54;
TRAMEOUT[88]=0x50;
TRAMEOUT[89]=0x2E;
TRAMEOUT[90]=0x52;
TRAMEOUT[91]=0x45;
TRAMEOUT[92]=0x53;
TRAMEOUT[93]=0x49;
TRAMEOUT[94]=0x53;
TRAMEOUT[95]=0x54;
TRAMEOUT[96]=0x49;
TRAMEOUT[97]=0x46;
TRAMEOUT[98]=0x20;
TRAMEOUT[99]=0x21;
TRAMEOUT[100]=0x20;
TRAMEOUT[101]=0x44;
TRAMEOUT[102]=0x45;
TRAMEOUT[103]=0x42;
TRAMEOUT[104]=0x49;
TRAMEOUT[105]=0x54;
TRAMEOUT[106]=0x21;
TRAMEOUT[107]=0x20;
TRAMEOUT[108]=0x1B;
TRAMEOUT[109]=0x5B;
TRAMEOUT[110]=0x37;
TRAMEOUT[111]=0x3B;
TRAMEOUT[112]=0x32;
TRAMEOUT[113]=0x48;
TRAMEOUT[114]=0x2D;
TRAMEOUT[115]=0x2D;
TRAMEOUT[116]=0x2D;
TRAMEOUT[117]=0x2D;
TRAMEOUT[118]=0x2D;
TRAMEOUT[119]=0x2D;
TRAMEOUT[120]=0x2D;
TRAMEOUT[121]='*';



j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='2';
TRAMEOUT[3]='c';
TRAMEOUT[4]=0x2D;
TRAMEOUT[5]=0x2D;
TRAMEOUT[6]=0x2D;
TRAMEOUT[7]=0x2D;
TRAMEOUT[8]=0x2D;
TRAMEOUT[9]=0x2D;
TRAMEOUT[10]=0x2D;
TRAMEOUT[11]=0x2D;
TRAMEOUT[12]=0x2D;
TRAMEOUT[13]=0x2D;
TRAMEOUT[14]=0x2D;
TRAMEOUT[15]=0x2D;
TRAMEOUT[16]=0x2D;
TRAMEOUT[17]=0x2D;
TRAMEOUT[18]=0x2D;
TRAMEOUT[19]=0x2D;
TRAMEOUT[20]=0x2D;
TRAMEOUT[21]=0x2D;
TRAMEOUT[22]=0x2D;
TRAMEOUT[23]=0x2D;
TRAMEOUT[24]=0x2D;
TRAMEOUT[25]=0x2D;
TRAMEOUT[26]=0x2D;
TRAMEOUT[27]=0x2D;
TRAMEOUT[28]=0x2D;
TRAMEOUT[29]=0x2D;
TRAMEOUT[30]=0x2D;
TRAMEOUT[31]=0x2D;
TRAMEOUT[32]=0x2D;
TRAMEOUT[33]=0x2D;
TRAMEOUT[34]=0x2D;
TRAMEOUT[35]=0x2D;
TRAMEOUT[36]=0x2D;
TRAMEOUT[37]=0x2D;
TRAMEOUT[38]=0x2D;
TRAMEOUT[39]=0x2D;
TRAMEOUT[40]=0x2D;
TRAMEOUT[41]=0x2D;
TRAMEOUT[42]=0x2D;
TRAMEOUT[43]=0x2D;
TRAMEOUT[44]=0x2D;
TRAMEOUT[45]=0x2D;
TRAMEOUT[46]=0x2D;
TRAMEOUT[47]=0x2D;
TRAMEOUT[48]=0x2D;
TRAMEOUT[49]=0x2D;
TRAMEOUT[50]=0x2D;
TRAMEOUT[51]=0x2D;
TRAMEOUT[52]=0x2D;
TRAMEOUT[53]=0x2D;
TRAMEOUT[54]=0x2D;
TRAMEOUT[55]=0x2D;
TRAMEOUT[56]=0x2D;
TRAMEOUT[57]=0x2D;
TRAMEOUT[58]=0x2D;
TRAMEOUT[59]=0x2D;
TRAMEOUT[60]=0x2D;
TRAMEOUT[61]=0x2D;
TRAMEOUT[62]=0x2D;
TRAMEOUT[63]=0x2D;
TRAMEOUT[64]=0x2D;
TRAMEOUT[65]=0x2D;
TRAMEOUT[66]=0x1B;
TRAMEOUT[67]=0x5B;
TRAMEOUT[68]=0x30;
TRAMEOUT[69]=0x38;
TRAMEOUT[70]=0x3B;
TRAMEOUT[71]=0x32;
TRAMEOUT[72]=0x48;
TRAMEOUT[73]=0x21;
TRAMEOUT[74]=0x20;
TRAMEOUT[75]=0x41;
TRAMEOUT[76]=0x4C;
TRAMEOUT[77]=0x41;
TRAMEOUT[78]=0x52;
TRAMEOUT[79]=0x4D;
TRAMEOUT[80]=0x45;
TRAMEOUT[81]=0x20;
TRAMEOUT[82]=0x45;
TRAMEOUT[83]=0x4E;
TRAMEOUT[84]=0x20;
TRAMEOUT[85]=0x43;
TRAMEOUT[86]=0x4F;
TRAMEOUT[87]=0x55;
TRAMEOUT[88]=0x52;
TRAMEOUT[89]=0x53;
TRAMEOUT[90]=0x20;
TRAMEOUT[91]=0x20;
TRAMEOUT[92]=0x20;
TRAMEOUT[93]=0x21;
TRAMEOUT[94]=0x1B;
TRAMEOUT[95]=0x5B;
TRAMEOUT[96]=0x30;
TRAMEOUT[97]=0x38;
TRAMEOUT[98]=0x3B;
TRAMEOUT[99]=0x33;
TRAMEOUT[100]=0x33;
TRAMEOUT[101]=0x48;
TRAMEOUT[102]=0x21;
TRAMEOUT[103]=0x1B;
TRAMEOUT[104]=0x5B;
TRAMEOUT[105]=0x30;
TRAMEOUT[106]=0x38;
TRAMEOUT[107]=0x3B;
TRAMEOUT[108]=0x34;
TRAMEOUT[109]=0x39;
TRAMEOUT[110]=0x48;
TRAMEOUT[111]=0x21;
TRAMEOUT[112]=0x1B;
TRAMEOUT[113]=0x5B;
TRAMEOUT[114]=0x30;
TRAMEOUT[115]=0x38;
TRAMEOUT[116]=0x3B;
TRAMEOUT[117]=0x36;
TRAMEOUT[118]=0x33;
TRAMEOUT[119]=0x48;
TRAMEOUT[120]=0x21;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='2';
TRAMEOUT[3]='d';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x30;
TRAMEOUT[7]=0x38;
TRAMEOUT[8]=0x3B;
TRAMEOUT[9]=0x37;
TRAMEOUT[10]=0x30;
TRAMEOUT[11]=0x48;
TRAMEOUT[12]=0x21;
TRAMEOUT[13]=0x1B;
TRAMEOUT[14]=0x5B;
TRAMEOUT[15]=0x39;
TRAMEOUT[16]=0x3B;
TRAMEOUT[17]=0x32;
TRAMEOUT[18]=0x48;
TRAMEOUT[19]=0x2D;
TRAMEOUT[20]=0x2D;
TRAMEOUT[21]=0x2D;
TRAMEOUT[22]=0x2D;
TRAMEOUT[23]=0x2D;
TRAMEOUT[24]=0x2D;
TRAMEOUT[25]=0x2D;
TRAMEOUT[26]=0x2D;
TRAMEOUT[27]=0x2D;
TRAMEOUT[28]=0x2D;
TRAMEOUT[29]=0x2D;
TRAMEOUT[30]=0x2D;
TRAMEOUT[31]=0x2D;
TRAMEOUT[32]=0x2D;
TRAMEOUT[33]=0x2D;
TRAMEOUT[34]=0x2D;
TRAMEOUT[35]=0x2D;
TRAMEOUT[36]=0x2D;
TRAMEOUT[37]=0x2D;
TRAMEOUT[38]=0x2D;
TRAMEOUT[39]=0x2D;
TRAMEOUT[40]=0x2D;
TRAMEOUT[41]=0x2D;
TRAMEOUT[42]=0x2D;
TRAMEOUT[43]=0x2D;
TRAMEOUT[44]=0x2D;
TRAMEOUT[45]=0x2D;
TRAMEOUT[46]=0x2D;
TRAMEOUT[47]=0x2D;
TRAMEOUT[48]=0x2D;
TRAMEOUT[49]=0x2D;
TRAMEOUT[50]=0x2D;
TRAMEOUT[51]=0x2D;
TRAMEOUT[52]=0x2D;
TRAMEOUT[53]=0x2D;
TRAMEOUT[54]=0x2D;
TRAMEOUT[55]=0x2D;
TRAMEOUT[56]=0x2D;
TRAMEOUT[57]=0x2D;
TRAMEOUT[58]=0x2D;
TRAMEOUT[59]=0x2D;
TRAMEOUT[60]=0x2D;
TRAMEOUT[61]=0x2D;
TRAMEOUT[62]=0x2D;
TRAMEOUT[63]=0x2D;
TRAMEOUT[64]=0x2D;
TRAMEOUT[65]=0x2D;
TRAMEOUT[66]=0x2D;
TRAMEOUT[67]=0x2D;
TRAMEOUT[68]=0x2D;
TRAMEOUT[69]=0x2D;
TRAMEOUT[70]=0x2D;
TRAMEOUT[71]=0x2D;
TRAMEOUT[72]=0x2D;
TRAMEOUT[73]=0x2D;
TRAMEOUT[74]=0x2D;
TRAMEOUT[75]=0x2D;
TRAMEOUT[76]=0x2D;
TRAMEOUT[77]=0x2D;
TRAMEOUT[78]=0x2D;
TRAMEOUT[79]=0x2D;
TRAMEOUT[80]=0x2D;
TRAMEOUT[81]=0x2D;
TRAMEOUT[82]=0x2D;
TRAMEOUT[83]=0x2D;
TRAMEOUT[84]=0x2D;
TRAMEOUT[85]=0x2D;
TRAMEOUT[86]=0x2D;
TRAMEOUT[87]=0x2D;
TRAMEOUT[88]=0x1B;
TRAMEOUT[89]=0x5B;
TRAMEOUT[90]=0x31;
TRAMEOUT[91]=0x30;
TRAMEOUT[92]=0x3B;
TRAMEOUT[93]=0x32;
TRAMEOUT[94]=0x48;
TRAMEOUT[95]=0x21;
TRAMEOUT[96]=0x20;
TRAMEOUT[97]=0x48;
TRAMEOUT[98]=0x2F;
TRAMEOUT[99]=0x47;
TRAMEOUT[100]=0x3A;
TRAMEOUT[101]=0x20;
TRAMEOUT[102]=0x48;
TRAMEOUT[103]=0x4F;
TRAMEOUT[104]=0x52;
TRAMEOUT[105]=0x53;
TRAMEOUT[106]=0x20;
TRAMEOUT[107]=0x47;
TRAMEOUT[108]=0x41;
TRAMEOUT[109]=0x4D;
TRAMEOUT[110]=0x4D;
TRAMEOUT[111]=0x45;
TRAMEOUT[112]=0x20;
TRAMEOUT[113]=0x20;
TRAMEOUT[114]=0x20;
TRAMEOUT[115]=0x21;
TRAMEOUT[116]=0x1B;
TRAMEOUT[117]=0x5B;
TRAMEOUT[118]=0x31;
TRAMEOUT[119]=0x30;
TRAMEOUT[120]=0x3B;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='2';
TRAMEOUT[3]='e';
TRAMEOUT[4]=0x33;
TRAMEOUT[5]=0x33;
TRAMEOUT[6]=0x48;
TRAMEOUT[7]=0x21;
TRAMEOUT[8]=0x1B;
TRAMEOUT[9]=0x5B;
TRAMEOUT[10]=0x31;
TRAMEOUT[11]=0x30;
TRAMEOUT[12]=0x3B;
TRAMEOUT[13]=0x34;
TRAMEOUT[14]=0x39;
TRAMEOUT[15]=0x48;
TRAMEOUT[16]=0x21;
TRAMEOUT[17]=0x1B;
TRAMEOUT[18]=0x5B;
TRAMEOUT[19]=0x31;
TRAMEOUT[20]=0x30;
TRAMEOUT[21]=0x3B;
TRAMEOUT[22]=0x36;
TRAMEOUT[23]=0x33;
TRAMEOUT[24]=0x48;
TRAMEOUT[25]=0x21;
TRAMEOUT[26]=0x1B;
TRAMEOUT[27]=0x5B;
TRAMEOUT[28]=0x31;
TRAMEOUT[29]=0x30;
TRAMEOUT[30]=0x3B;
TRAMEOUT[31]=0x37;
TRAMEOUT[32]=0x30;
TRAMEOUT[33]=0x48;
TRAMEOUT[34]=0x21;
TRAMEOUT[35]=0x1B;
TRAMEOUT[36]=0x5B;
TRAMEOUT[37]=0x31;
TRAMEOUT[38]=0x31;
TRAMEOUT[39]=0x3B;
TRAMEOUT[40]=0x32;
TRAMEOUT[41]=0x48;
TRAMEOUT[42]=0x2D;
TRAMEOUT[43]=0x2D;
TRAMEOUT[44]=0x2D;
TRAMEOUT[45]=0x2D;
TRAMEOUT[46]=0x2D;
TRAMEOUT[47]=0x2D;
TRAMEOUT[48]=0x2D;
TRAMEOUT[49]=0x2D;
TRAMEOUT[50]=0x2D;
TRAMEOUT[51]=0x2D;
TRAMEOUT[52]=0x2D;
TRAMEOUT[53]=0x2D;
TRAMEOUT[54]=0x2D;
TRAMEOUT[55]=0x2D;
TRAMEOUT[56]=0x2D;
TRAMEOUT[57]=0x2D;
TRAMEOUT[58]=0x2D;
TRAMEOUT[59]=0x2D;
TRAMEOUT[60]=0x2D;
TRAMEOUT[61]=0x2D;
TRAMEOUT[62]=0x2D;
TRAMEOUT[63]=0x2D;
TRAMEOUT[64]=0x2D;
TRAMEOUT[65]=0x2D;
TRAMEOUT[66]=0x2D;
TRAMEOUT[67]=0x2D;
TRAMEOUT[68]=0x2D;
TRAMEOUT[69]=0x2D;
TRAMEOUT[70]=0x2D;
TRAMEOUT[71]=0x2D;
TRAMEOUT[72]=0x2D;
TRAMEOUT[73]=0x2D;
TRAMEOUT[74]=0x2D;
TRAMEOUT[75]=0x2D;
TRAMEOUT[76]=0x2D;
TRAMEOUT[77]=0x2D;
TRAMEOUT[78]=0x2D;
TRAMEOUT[79]=0x2D;
TRAMEOUT[80]=0x2D;
TRAMEOUT[81]=0x2D;
TRAMEOUT[82]=0x2D;
TRAMEOUT[83]=0x2D;
TRAMEOUT[84]=0x2D;
TRAMEOUT[85]=0x2D;
TRAMEOUT[86]=0x2D;
TRAMEOUT[87]=0x2D;
TRAMEOUT[88]=0x2D;
TRAMEOUT[89]=0x2D;
TRAMEOUT[90]=0x2D;
TRAMEOUT[91]=0x2D;
TRAMEOUT[92]=0x2D;
TRAMEOUT[93]=0x2D;
TRAMEOUT[94]=0x2D;
TRAMEOUT[95]=0x2D;
TRAMEOUT[96]=0x2D;
TRAMEOUT[97]=0x2D;
TRAMEOUT[98]=0x2D;
TRAMEOUT[99]=0x2D;
TRAMEOUT[100]=0x2D;
TRAMEOUT[101]=0x2D;
TRAMEOUT[102]=0x2D;
TRAMEOUT[103]=0x2D;
TRAMEOUT[104]=0x2D;
TRAMEOUT[105]=0x2D;
TRAMEOUT[106]=0x2D;
TRAMEOUT[107]=0x2D;
TRAMEOUT[108]=0x2D;
TRAMEOUT[109]=0x2D;
TRAMEOUT[110]=0x2D;
TRAMEOUT[111]=0x1B;
TRAMEOUT[112]=0x5B;
TRAMEOUT[113]=0x31;
TRAMEOUT[114]=0x32;
TRAMEOUT[115]=0x3B;
TRAMEOUT[116]=0x32;
TRAMEOUT[117]=0x48;
TRAMEOUT[118]=0x21;
TRAMEOUT[119]=0x20;
TRAMEOUT[120]=0x4E;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='2';
TRAMEOUT[3]='f';
TRAMEOUT[4]=0x2F;
TRAMEOUT[5]=0x52;
TRAMEOUT[6]=0x3A;
TRAMEOUT[7]=0x20;
TRAMEOUT[8]=0x4E;
TRAMEOUT[9]=0x4F;
TRAMEOUT[10]=0x4E;
TRAMEOUT[11]=0x20;
TRAMEOUT[12]=0x52;
TRAMEOUT[13]=0x45;
TRAMEOUT[14]=0x50;
TRAMEOUT[15]=0x4F;
TRAMEOUT[16]=0x4E;
TRAMEOUT[17]=0x53;
TRAMEOUT[18]=0x45;
TRAMEOUT[19]=0x20;
TRAMEOUT[20]=0x20;
TRAMEOUT[21]=0x21;
TRAMEOUT[22]=0x1B;
TRAMEOUT[23]=0x5B;
TRAMEOUT[24]=0x31;
TRAMEOUT[25]=0x32;
TRAMEOUT[26]=0x3B;
TRAMEOUT[27]=0x33;
TRAMEOUT[28]=0x33;
TRAMEOUT[29]=0x48;
TRAMEOUT[30]=0x21;
TRAMEOUT[31]=0x1B;
TRAMEOUT[32]=0x5B;
TRAMEOUT[33]=0x31;
TRAMEOUT[34]=0x32;
TRAMEOUT[35]=0x3B;
TRAMEOUT[36]=0x34;
TRAMEOUT[37]=0x39;
TRAMEOUT[38]=0x48;
TRAMEOUT[39]=0x21;
TRAMEOUT[40]=0x1B;
TRAMEOUT[41]=0x5B;
TRAMEOUT[42]=0x31;
TRAMEOUT[43]=0x32;
TRAMEOUT[44]=0x3B;
TRAMEOUT[45]=0x36;
TRAMEOUT[46]=0x33;
TRAMEOUT[47]=0x48;
TRAMEOUT[48]=0x21;
TRAMEOUT[49]=0x1B;
TRAMEOUT[50]=0x5B;
TRAMEOUT[51]=0x31;
TRAMEOUT[52]=0x32;
TRAMEOUT[53]=0x3B;
TRAMEOUT[54]=0x37;
TRAMEOUT[55]=0x30;
TRAMEOUT[56]=0x48;
TRAMEOUT[57]=0x21;
TRAMEOUT[58]=0x1B;
TRAMEOUT[59]=0x5B;
TRAMEOUT[60]=0x31;
TRAMEOUT[61]=0x33;
TRAMEOUT[62]=0x3B;
TRAMEOUT[63]=0x32;
TRAMEOUT[64]=0x48;
TRAMEOUT[65]=0x2D;
TRAMEOUT[66]=0x2D;
TRAMEOUT[67]=0x2D;
TRAMEOUT[68]=0x2D;
TRAMEOUT[69]=0x2D;
TRAMEOUT[70]=0x2D;
TRAMEOUT[71]=0x2D;
TRAMEOUT[72]=0x2D;
TRAMEOUT[73]=0x2D;
TRAMEOUT[74]=0x2D;
TRAMEOUT[75]=0x2D;
TRAMEOUT[76]=0x2D;
TRAMEOUT[77]=0x2D;
TRAMEOUT[78]=0x2D;
TRAMEOUT[79]=0x2D;
TRAMEOUT[80]=0x2D;
TRAMEOUT[81]=0x2D;
TRAMEOUT[82]=0x2D;
TRAMEOUT[83]=0x2D;
TRAMEOUT[84]=0x2D;
TRAMEOUT[85]=0x2D;
TRAMEOUT[86]=0x2D;
TRAMEOUT[87]=0x2D;
TRAMEOUT[88]=0x2D;
TRAMEOUT[89]=0x2D;
TRAMEOUT[90]=0x2D;
TRAMEOUT[91]=0x2D;
TRAMEOUT[92]=0x2D;
TRAMEOUT[93]=0x2D;
TRAMEOUT[94]=0x2D;
TRAMEOUT[95]=0x2D;
TRAMEOUT[96]=0x2D;
TRAMEOUT[97]=0x2D;
TRAMEOUT[98]=0x2D;
TRAMEOUT[99]=0x2D;
TRAMEOUT[100]=0x2D;
TRAMEOUT[101]=0x2D;
TRAMEOUT[102]=0x2D;
TRAMEOUT[103]=0x2D;
TRAMEOUT[104]=0x2D;
TRAMEOUT[105]=0x2D;
TRAMEOUT[106]=0x2D;
TRAMEOUT[107]=0x2D;
TRAMEOUT[108]=0x2D;
TRAMEOUT[109]=0x2D;
TRAMEOUT[110]=0x2D;
TRAMEOUT[111]=0x2D;
TRAMEOUT[112]=0x2D;
TRAMEOUT[113]=0x2D;
TRAMEOUT[114]=0x2D;
TRAMEOUT[115]=0x2D;
TRAMEOUT[116]=0x2D;
TRAMEOUT[117]=0x2D;
TRAMEOUT[118]=0x2D;
TRAMEOUT[119]=0x2D;
TRAMEOUT[120]=0x2D;
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[121]='*';
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='2';
TRAMEOUT[3]='g';
TRAMEOUT[4]=0x2D;
TRAMEOUT[5]=0x2D;
TRAMEOUT[6]=0x2D;
TRAMEOUT[7]=0x2D;
TRAMEOUT[8]=0x2D;
TRAMEOUT[9]=0x2D;
TRAMEOUT[10]=0x2D;
TRAMEOUT[11]=0x2D;
TRAMEOUT[12]=0x2D;
TRAMEOUT[13]=0x2D;
TRAMEOUT[14]=0x2D;
TRAMEOUT[15]=0x2D;
TRAMEOUT[16]=0x2D;
TRAMEOUT[17]=0x1B;
TRAMEOUT[18]=0x5B;
TRAMEOUT[19]=0x31;
TRAMEOUT[20]=0x34;
TRAMEOUT[21]=0x3B;
TRAMEOUT[22]=0x32;
TRAMEOUT[23]=0x48;
TRAMEOUT[24]=0x21;
TRAMEOUT[25]=0x20;
TRAMEOUT[26]=0x49;
TRAMEOUT[27]=0x4E;
TRAMEOUT[28]=0x54;
TRAMEOUT[29]=0x3A;
TRAMEOUT[30]=0x20;
TRAMEOUT[31]=0x49;
TRAMEOUT[32]=0x4E;
TRAMEOUT[33]=0x54;
TRAMEOUT[34]=0x45;
TRAMEOUT[35]=0x52;
TRAMEOUT[36]=0x56;
TRAMEOUT[37]=0x45;
TRAMEOUT[38]=0x4E;
TRAMEOUT[39]=0x54;
TRAMEOUT[40]=0x49;
TRAMEOUT[41]=0x4F;
TRAMEOUT[42]=0x4E;
TRAMEOUT[43]=0x20;
TRAMEOUT[44]=0x21;
TRAMEOUT[45]=0x1B;
TRAMEOUT[46]=0x5B;
TRAMEOUT[47]=0x31;
TRAMEOUT[48]=0x34;
TRAMEOUT[49]=0x3B;
TRAMEOUT[50]=0x33;
TRAMEOUT[51]=0x33;
TRAMEOUT[52]=0x48;
TRAMEOUT[53]=0x21;
TRAMEOUT[54]=0x1B;
TRAMEOUT[55]=0x5B;
TRAMEOUT[56]=0x31;
TRAMEOUT[57]=0x34;
TRAMEOUT[58]=0x3B;
TRAMEOUT[59]=0x34;
TRAMEOUT[60]=0x39;
TRAMEOUT[61]=0x48;
TRAMEOUT[62]=0x21;
TRAMEOUT[63]=0x1B;
TRAMEOUT[64]=0x5B;
TRAMEOUT[65]=0x31;
TRAMEOUT[66]=0x34;
TRAMEOUT[67]=0x3B;
TRAMEOUT[68]=0x36;
TRAMEOUT[69]=0x33;
TRAMEOUT[70]=0x48;
TRAMEOUT[71]=0x21;
TRAMEOUT[72]=0x1B;
TRAMEOUT[73]=0x5B;
TRAMEOUT[74]=0x31;
TRAMEOUT[75]=0x34;
TRAMEOUT[76]=0x3B;
TRAMEOUT[77]=0x37;
TRAMEOUT[78]=0x30;
TRAMEOUT[79]=0x48;
TRAMEOUT[80]=0x21;
TRAMEOUT[81]=0x1B;
TRAMEOUT[82]=0x5B;
TRAMEOUT[83]=0x31;
TRAMEOUT[84]=0x35;
TRAMEOUT[85]=0x3B;
TRAMEOUT[86]=0x32;
TRAMEOUT[87]=0x48;
TRAMEOUT[88]=0x2D;
TRAMEOUT[89]=0x2D;
TRAMEOUT[90]=0x2D;
TRAMEOUT[91]=0x2D;
TRAMEOUT[92]=0x2D;
TRAMEOUT[93]=0x2D;
TRAMEOUT[94]=0x2D;
TRAMEOUT[95]=0x2D;
TRAMEOUT[96]=0x2D;
TRAMEOUT[97]=0x2D;
TRAMEOUT[98]=0x2D;
TRAMEOUT[99]=0x2D;
TRAMEOUT[100]=0x2D;
TRAMEOUT[101]=0x2D;
TRAMEOUT[102]=0x2D;
TRAMEOUT[103]=0x2D;
TRAMEOUT[104]=0x2D;
TRAMEOUT[105]=0x2D;
TRAMEOUT[106]=0x2D;
TRAMEOUT[107]=0x2D;
TRAMEOUT[108]=0x2D;
TRAMEOUT[109]=0x2D;
TRAMEOUT[110]=0x2D;
TRAMEOUT[111]=0x2D;
TRAMEOUT[112]=0x2D;
TRAMEOUT[113]=0x2D;
TRAMEOUT[114]=0x2D;
TRAMEOUT[115]=0x2D;
TRAMEOUT[116]=0x2D;
TRAMEOUT[117]=0x2D;
TRAMEOUT[118]=0x2D;
TRAMEOUT[119]=0x2D;
TRAMEOUT[120]=0x2D;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='2';
TRAMEOUT[3]='h';
TRAMEOUT[4]=0x2D;
TRAMEOUT[5]=0x2D;
TRAMEOUT[6]=0x2D;
TRAMEOUT[7]=0x2D;
TRAMEOUT[8]=0x2D;
TRAMEOUT[9]=0x2D;
TRAMEOUT[10]=0x2D;
TRAMEOUT[11]=0x2D;
TRAMEOUT[12]=0x2D;
TRAMEOUT[13]=0x2D;
TRAMEOUT[14]=0x2D;
TRAMEOUT[15]=0x2D;
TRAMEOUT[16]=0x2D;
TRAMEOUT[17]=0x2D;
TRAMEOUT[18]=0x2D;
TRAMEOUT[19]=0x2D;
TRAMEOUT[20]=0x2D;
TRAMEOUT[21]=0x2D;
TRAMEOUT[22]=0x2D;
TRAMEOUT[23]=0x2D;
TRAMEOUT[24]=0x2D;
TRAMEOUT[25]=0x2D;
TRAMEOUT[26]=0x2D;
TRAMEOUT[27]=0x2D;
TRAMEOUT[28]=0x2D;
TRAMEOUT[29]=0x2D;
TRAMEOUT[30]=0x2D;
TRAMEOUT[31]=0x2D;
TRAMEOUT[32]=0x2D;
TRAMEOUT[33]=0x2D;
TRAMEOUT[34]=0x2D;
TRAMEOUT[35]=0x2D;
TRAMEOUT[36]=0x2D;
TRAMEOUT[37]=0x2D;
TRAMEOUT[38]=0x2D;
TRAMEOUT[39]=0x2D;
TRAMEOUT[40]='*';

j=0;
for (iii=4;iii<40;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 36); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (36);
}
else if (num == '3')
{


TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='5';
TRAMEOUT[3]='a';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x32;
TRAMEOUT[7]=0x4A;
TRAMEOUT[8]=0x1B;
TRAMEOUT[9]=0x5B;
TRAMEOUT[10]=0x31;
TRAMEOUT[11]=0x3B;
TRAMEOUT[12]=0x36;
TRAMEOUT[13]=0x34;
TRAMEOUT[14]=0x48;
TRAMEOUT[15]=0x1B;
TRAMEOUT[16]=0x5B;
TRAMEOUT[17]=0x31;
TRAMEOUT[18]=0x3B;
TRAMEOUT[19]=0x33;
TRAMEOUT[20]=0x38;
TRAMEOUT[21]=0x48;
TRAMEOUT[22]=0x53;
TRAMEOUT[23]=0x69;
TRAMEOUT[24]=0x74;
TRAMEOUT[25]=0x65;
TRAMEOUT[26]=0x20;
TRAMEOUT[27]=0x3A;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]=0x1B;
TRAMEOUT[30]=0x5B;
TRAMEOUT[31]=0x31;
TRAMEOUT[32]=0x3B;
TRAMEOUT[33]=0x36;
TRAMEOUT[34]=0x34;
TRAMEOUT[35]=0x48;
TRAMEOUT[36]=0x20;
TRAMEOUT[37]=0x1B;
TRAMEOUT[38]=0x5B;
TRAMEOUT[39]=0x32;
TRAMEOUT[40]=0x3B;
TRAMEOUT[41]=0x33;
TRAMEOUT[42]=0x30;
TRAMEOUT[43]=0x48;
TRAMEOUT[44]=0x20;//19102020
TRAMEOUT[45]=0x20;
TRAMEOUT[46]=0x20;
TRAMEOUT[47]=0x20;
TRAMEOUT[48]=0x20;
TRAMEOUT[49]=0x20; //NOM DU SITE
TRAMEOUT[50]=0x20;
TRAMEOUT[51]=0x20;
TRAMEOUT[52]=0x20;
TRAMEOUT[53]=0x20;
TRAMEOUT[54]=0x20;
TRAMEOUT[55]=0x20;
TRAMEOUT[56]=0x20;
TRAMEOUT[57]=0x20;
TRAMEOUT[58]=0x20;
TRAMEOUT[59]=0x20;
TRAMEOUT[60]=0x20;
TRAMEOUT[61]=0x20;
TRAMEOUT[62]=0x20;
TRAMEOUT[63]=0x20;
TRAMEOUT[64]=0x20;
TRAMEOUT[65]=0x20;
TRAMEOUT[66]=0x20;
TRAMEOUT[67]=0x20;
TRAMEOUT[68]=0x20;
TRAMEOUT[69]=0x20;
TRAMEOUT[70]=0x20;
TRAMEOUT[71]=0x1B;
TRAMEOUT[72]=0x5B;
TRAMEOUT[73]=0x31;
TRAMEOUT[74]=0x3B;
TRAMEOUT[75]=0x32;
TRAMEOUT[76]=0x48;
TRAMEOUT[77]=0x1B;
TRAMEOUT[78]=0x5B;
TRAMEOUT[79]=0x33;
TRAMEOUT[80]=0x3B;
TRAMEOUT[81]=0x31;
TRAMEOUT[82]=0x36;
TRAMEOUT[83]=0x48;
TRAMEOUT[84]=0x1B;
TRAMEOUT[85]=0x5B;
TRAMEOUT[86]=0x32;
TRAMEOUT[87]=0x3B;
TRAMEOUT[88]=0x32;
TRAMEOUT[89]=0x48;
TRAMEOUT[90]=0x4E;
TRAMEOUT[91]=0x72;
TRAMEOUT[92]=0x2E;
TRAMEOUT[93]=0x20;
TRAMEOUT[94]=0x64;
TRAMEOUT[95]=0x65;
TRAMEOUT[96]=0x20;
TRAMEOUT[97]=0x63;
TRAMEOUT[98]=0x61;
TRAMEOUT[99]=0x62;
TRAMEOUT[100]=0x6C;
TRAMEOUT[101]=0x65;
TRAMEOUT[102]=0x3A;
TRAMEOUT[103]=0x20;
TRAMEOUT[104]=0x1B;
TRAMEOUT[105]=0x5B;
TRAMEOUT[106]=0x31;
TRAMEOUT[107]=0x3B;
TRAMEOUT[108]=0x32;
TRAMEOUT[109]=0x48;
TRAMEOUT[110]=0x1B;
TRAMEOUT[111]=0x5B;
TRAMEOUT[112]=0x33;
TRAMEOUT[113]=0x3B;
TRAMEOUT[114]=0x31;
TRAMEOUT[115]=0x34;
TRAMEOUT[116]=0x48;
TRAMEOUT[117]=0x1B;
TRAMEOUT[118]=0x5B;
TRAMEOUT[119]=0x37;
TRAMEOUT[120]=0x6D;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='5';
TRAMEOUT[3]='b';
TRAMEOUT[4]=0x20;
TRAMEOUT[5]=0x56;
TRAMEOUT[6]=0x49;
TRAMEOUT[7]=0x53;
TRAMEOUT[8]=0x55;
TRAMEOUT[9]=0x41;
TRAMEOUT[10]=0x4C;
TRAMEOUT[11]=0x49;
TRAMEOUT[12]=0x53;
TRAMEOUT[13]=0x41;
TRAMEOUT[14]=0x54;
TRAMEOUT[15]=0x49;
TRAMEOUT[16]=0x4F;
TRAMEOUT[17]=0x4E;
TRAMEOUT[18]=0x20;
TRAMEOUT[19]=0x44;
TRAMEOUT[20]=0x45;
TRAMEOUT[21]=0x53;
TRAMEOUT[22]=0x20;
TRAMEOUT[23]=0x44;
TRAMEOUT[24]=0x4F;
TRAMEOUT[25]=0x4E;
TRAMEOUT[26]=0x4E;
TRAMEOUT[27]=0x45;
TRAMEOUT[28]=0x45;
TRAMEOUT[29]=0x53;
TRAMEOUT[30]=0x20;
TRAMEOUT[31]=0x44;
TRAMEOUT[32]=0x27;
TRAMEOUT[33]=0x55;
TRAMEOUT[34]=0x4E;
TRAMEOUT[35]=0x20;
TRAMEOUT[36]=0x43;
TRAMEOUT[37]=0x41;
TRAMEOUT[38]=0x42;
TRAMEOUT[39]=0x4C;
TRAMEOUT[40]=0x45;
TRAMEOUT[41]=0x20;
TRAMEOUT[42]=0x45;
TRAMEOUT[43]=0x4E;
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x53;
TRAMEOUT[46]=0x45;
TRAMEOUT[47]=0x52;
TRAMEOUT[48]=0x56;
TRAMEOUT[49]=0x49;
TRAMEOUT[50]=0x43;
TRAMEOUT[51]=0x45;
TRAMEOUT[52]=0x20;
TRAMEOUT[53]=0x1B;
TRAMEOUT[54]=0x5B;
TRAMEOUT[55]=0x30;
TRAMEOUT[56]=0x6D;
TRAMEOUT[57]=0x1B;
TRAMEOUT[58]=0x5B;
TRAMEOUT[59]=0x34;
TRAMEOUT[60]=0x3B;
TRAMEOUT[61]=0x32;
TRAMEOUT[62]=0x48;
TRAMEOUT[63]=0x1B;
TRAMEOUT[64]=0x5B;
TRAMEOUT[65]=0x34;
TRAMEOUT[66]=0x3B;
TRAMEOUT[67]=0x30;
TRAMEOUT[68]=0x48;
TRAMEOUT[69]=0x2D;
TRAMEOUT[70]=0x2D;
TRAMEOUT[71]=0x2D;
TRAMEOUT[72]=0x2D;
TRAMEOUT[73]=0x2D;
TRAMEOUT[74]=0x2D;
TRAMEOUT[75]=0x2D;
TRAMEOUT[76]=0x2D;
TRAMEOUT[77]=0x2D;
TRAMEOUT[78]=0x2D;
TRAMEOUT[79]=0x2D;
TRAMEOUT[80]=0x2D;
TRAMEOUT[81]=0x2D;
TRAMEOUT[82]=0x2D;
TRAMEOUT[83]=0x2D;
TRAMEOUT[84]=0x2D;
TRAMEOUT[85]=0x2D;
TRAMEOUT[86]=0x2D;
TRAMEOUT[87]=0x2D;
TRAMEOUT[88]=0x2D;
TRAMEOUT[89]=0x2D;
TRAMEOUT[90]=0x2D;
TRAMEOUT[91]=0x2D;
TRAMEOUT[92]=0x2D;
TRAMEOUT[93]=0x2D;
TRAMEOUT[94]=0x2D;
TRAMEOUT[95]=0x2D;
TRAMEOUT[96]=0x2D;
TRAMEOUT[97]=0x2D;
TRAMEOUT[98]=0x2D;
TRAMEOUT[99]=0x2D;
TRAMEOUT[100]=0x2D;
TRAMEOUT[101]=0x2D;
TRAMEOUT[102]=0x2D;
TRAMEOUT[103]=0x2D;
TRAMEOUT[104]=0x2D;
TRAMEOUT[105]=0x2D;
TRAMEOUT[106]=0x2D;
TRAMEOUT[107]=0x2D;
TRAMEOUT[108]=0x2D;
TRAMEOUT[109]=0x2D;
TRAMEOUT[110]=0x2D;
TRAMEOUT[111]=0x2D;
TRAMEOUT[112]=0x2D;
TRAMEOUT[113]=0x2D;
TRAMEOUT[114]=0x2D;
TRAMEOUT[115]=0x2D;
TRAMEOUT[116]=0x2D;
TRAMEOUT[117]=0x2D;
TRAMEOUT[118]=0x2D;
TRAMEOUT[119]=0x2D;
TRAMEOUT[120]=0x2D;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='5';
TRAMEOUT[3]='c';
TRAMEOUT[4]=0x2D;
TRAMEOUT[5]=0x2D;
TRAMEOUT[6]=0x2D;
TRAMEOUT[7]=0x2D;
TRAMEOUT[8]=0x2D;
TRAMEOUT[9]=0x2D;
TRAMEOUT[10]=0x2D;
TRAMEOUT[11]=0x2D;
TRAMEOUT[12]=0x2D;
TRAMEOUT[13]=0x2D;
TRAMEOUT[14]=0x2D;
TRAMEOUT[15]=0x2D;
TRAMEOUT[16]=0x2D;
TRAMEOUT[17]=0x2D;
TRAMEOUT[18]=0x2D;
TRAMEOUT[19]=0x2D;
TRAMEOUT[20]=0x2D;
TRAMEOUT[21]=0x2D;
TRAMEOUT[22]=0x2D;
TRAMEOUT[23]=0x2D;
TRAMEOUT[24]=0x2D;
TRAMEOUT[25]=0x2D;
TRAMEOUT[26]=0x2D;
TRAMEOUT[27]=0x2D;
TRAMEOUT[28]=0x2D;
TRAMEOUT[29]=0x2D;
TRAMEOUT[30]=0x2D;
TRAMEOUT[31]=0x2D;
TRAMEOUT[32]=0x1B;
TRAMEOUT[33]=0x5B;
TRAMEOUT[34]=0x35;
TRAMEOUT[35]=0x3B;
TRAMEOUT[36]=0x32;
TRAMEOUT[37]=0x48;
TRAMEOUT[38]=0x4E;
TRAMEOUT[39]=0x2E;
TRAMEOUT[40]=0x20;
TRAMEOUT[41]=0x20;
TRAMEOUT[42]=0x21;
TRAMEOUT[43]=0x21;
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x20;
TRAMEOUT[46]=0x1B;
TRAMEOUT[47]=0x5B;
TRAMEOUT[48]=0x36;
TRAMEOUT[49]=0x3B;
TRAMEOUT[50]=0x32;
TRAMEOUT[51]=0x48;
TRAMEOUT[52]=0x54;
TRAMEOUT[53]=0x43;
TRAMEOUT[54]=0x41;
TRAMEOUT[55]=0x50;
TRAMEOUT[56]=0x21;
TRAMEOUT[57]=0x21;
TRAMEOUT[58]=0x1B;
TRAMEOUT[59]=0x5B;
TRAMEOUT[60]=0x35;
TRAMEOUT[61]=0x3B;
TRAMEOUT[62]=0x31;
TRAMEOUT[63]=0x30;
TRAMEOUT[64]=0x48;
TRAMEOUT[65]=0x43;
TRAMEOUT[66]=0x20;
TRAMEOUT[67]=0x20;
TRAMEOUT[68]=0x6D;
TRAMEOUT[69]=0x41;
TRAMEOUT[70]=0x20;
TRAMEOUT[71]=0x1B;
TRAMEOUT[72]=0x5B;
TRAMEOUT[73]=0x35;
TRAMEOUT[74]=0x3B;
TRAMEOUT[75]=0x31;
TRAMEOUT[76]=0x36;
TRAMEOUT[77]=0x48;
TRAMEOUT[78]=0x21;
TRAMEOUT[79]=0x20;
TRAMEOUT[80]=0x43;
TRAMEOUT[81]=0x20;
TRAMEOUT[82]=0x20;
TRAMEOUT[83]=0x20;
TRAMEOUT[84]=0x20;
TRAMEOUT[85]=0x6D;
TRAMEOUT[86]=0x41;
TRAMEOUT[87]=0x20;
TRAMEOUT[88]=0x21;
TRAMEOUT[89]=0x20;
TRAMEOUT[90]=0x43;
TRAMEOUT[91]=0x20;
TRAMEOUT[92]=0x20;
TRAMEOUT[93]='u';
TRAMEOUT[94]=0x41;
TRAMEOUT[95]=0x20;
TRAMEOUT[96]=0x21;
TRAMEOUT[97]=0x20;
TRAMEOUT[98]=0x20;
TRAMEOUT[99]=0x20;
TRAMEOUT[100]=0x20;
TRAMEOUT[101]=0x20;
TRAMEOUT[102]=0x20;
TRAMEOUT[103]=0x20;
TRAMEOUT[104]=0x20;
TRAMEOUT[105]=0x21;
TRAMEOUT[106]=0x20;
TRAMEOUT[107]=0x20;
TRAMEOUT[108]=0x20;
TRAMEOUT[109]=0x20;
TRAMEOUT[110]=0x20;
TRAMEOUT[111]=0x20;
TRAMEOUT[112]=0x20;
TRAMEOUT[113]=0x21;
TRAMEOUT[114]=0x20;
TRAMEOUT[115]=0x20;
TRAMEOUT[116]=0x20;
TRAMEOUT[117]=0x20;
TRAMEOUT[118]=0x20;
TRAMEOUT[119]=0x20;
TRAMEOUT[120]=0x21;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='5';
TRAMEOUT[3]='d';
TRAMEOUT[4]=0x20;
TRAMEOUT[5]=0x20;
TRAMEOUT[6]=0x20;
TRAMEOUT[7]=0x20;
TRAMEOUT[8]=0x20;
TRAMEOUT[9]=0x20;
TRAMEOUT[10]=0x21;
TRAMEOUT[11]=0x1B;
TRAMEOUT[12]=0x5B;
TRAMEOUT[13]=0x36;
TRAMEOUT[14]=0x3B;
TRAMEOUT[15]=0x31;
TRAMEOUT[16]=0x30;
TRAMEOUT[17]=0x48;
TRAMEOUT[18]=0x43;
TRAMEOUT[19]=0x4F;
TRAMEOUT[20]=0x4E;
TRAMEOUT[21]=0x53;
TRAMEOUT[22]=0x4F;
TRAMEOUT[23]=0x20;
TRAMEOUT[24]=0x1B;
TRAMEOUT[25]=0x5B;
TRAMEOUT[26]=0x36;
TRAMEOUT[27]=0x3B;
TRAMEOUT[28]=0x31;
TRAMEOUT[29]=0x36;
TRAMEOUT[30]=0x48;
TRAMEOUT[31]=0x21;
TRAMEOUT[32]=0x20;
TRAMEOUT[33]=0x4D;
TRAMEOUT[34]=0x4F;
TRAMEOUT[35]=0x44;
TRAMEOUT[36]=0x55;
TRAMEOUT[37]=0x4C;
TRAMEOUT[38]=0x41;
TRAMEOUT[39]=0x54;
TRAMEOUT[40]=0x20;
TRAMEOUT[41]=0x21;
TRAMEOUT[42]=0x20;
TRAMEOUT[43]=0x52;
TRAMEOUT[44]=0x45;
TRAMEOUT[45]=0x50;
TRAMEOUT[46]=0x4F;
TRAMEOUT[47]=0x53;
TRAMEOUT[48]=0x20;
TRAMEOUT[49]=0x21;
TRAMEOUT[50]=0x20;
TRAMEOUT[51]=0x4D;
TRAMEOUT[52]=0x45;
TRAMEOUT[53]=0x53;
TRAMEOUT[54]=0x55;
TRAMEOUT[55]=0x52;
TRAMEOUT[56]=0x45;
TRAMEOUT[57]=0x20;
TRAMEOUT[58]=0x21;
TRAMEOUT[59]=0x20;
TRAMEOUT[60]=0x53;
TRAMEOUT[61]=0x45;
TRAMEOUT[62]=0x55;
TRAMEOUT[63]=0x49;
TRAMEOUT[64]=0x4C;
TRAMEOUT[65]=0x20;
TRAMEOUT[66]=0x21;
TRAMEOUT[67]=0x20;
TRAMEOUT[68]=0x45;
TRAMEOUT[69]=0x54;
TRAMEOUT[70]=0x41;
TRAMEOUT[71]=0x54;
TRAMEOUT[72]=0x20;
TRAMEOUT[73]=0x21;
TRAMEOUT[74]=0x20;
TRAMEOUT[75]=0x44;
TRAMEOUT[76]=0x49;
TRAMEOUT[77]=0x53;
TRAMEOUT[78]=0x54;
TRAMEOUT[79]=0x20;
TRAMEOUT[80]=0x21;
TRAMEOUT[81]=0x20;
TRAMEOUT[82]=0x43;
TRAMEOUT[83]=0x4F;
TRAMEOUT[84]=0x4D;
TRAMEOUT[85]=0x4D;
TRAMEOUT[86]=0x45;
TRAMEOUT[87]=0x4E;
TRAMEOUT[88]=0x54;
TRAMEOUT[89]=0x41;
TRAMEOUT[90]=0x49;
TRAMEOUT[91]=0x52;
TRAMEOUT[92]=0x45;
TRAMEOUT[93]=0x1B;
TRAMEOUT[94]=0x5B;
TRAMEOUT[95]=0x37;
TRAMEOUT[96]=0x3B;
TRAMEOUT[97]=0x32;
TRAMEOUT[98]=0x48;
TRAMEOUT[99]=0x2D;
TRAMEOUT[100]=0x2D;
TRAMEOUT[101]=0x2D;
TRAMEOUT[102]=0x2D;
TRAMEOUT[103]=0x2D;
TRAMEOUT[104]=0x2D;
TRAMEOUT[105]=0x2D;
TRAMEOUT[106]=0x2D;
TRAMEOUT[107]=0x2D;
TRAMEOUT[108]=0x2D;
TRAMEOUT[109]=0x2D;
TRAMEOUT[110]=0x2D;
TRAMEOUT[111]=0x2D;
TRAMEOUT[112]=0x2D;
TRAMEOUT[113]=0x2D;
TRAMEOUT[114]=0x2D;
TRAMEOUT[115]=0x2D;
TRAMEOUT[116]=0x2D;
TRAMEOUT[117]=0x2D;
TRAMEOUT[118]=0x2D;
TRAMEOUT[119]=0x2D;
TRAMEOUT[120]=0x2D;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

//16092020
TRAMEENVOITCPDONNEES (95);

/*TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='5';
TRAMEOUT[3]='e';
TRAMEOUT[4]=0x2D;
TRAMEOUT[5]=0x2D;
TRAMEOUT[6]=0x2D;
TRAMEOUT[7]=0x2D;
TRAMEOUT[8]=0x2D;
TRAMEOUT[9]=0x2D;
TRAMEOUT[10]=0x2D;
TRAMEOUT[11]=0x2D;
TRAMEOUT[12]=0x2D;
TRAMEOUT[13]=0x2D;
TRAMEOUT[14]=0x2D;
TRAMEOUT[15]=0x2D;
TRAMEOUT[16]=0x2D;
TRAMEOUT[17]=0x2D;
TRAMEOUT[18]=0x2D;
TRAMEOUT[19]=0x2D;
TRAMEOUT[20]=0x2D;
TRAMEOUT[21]=0x2D;
TRAMEOUT[22]=0x2D;
TRAMEOUT[23]=0x2D;
TRAMEOUT[24]=0x2D;
TRAMEOUT[25]=0x2D;
TRAMEOUT[26]=0x2D;
TRAMEOUT[27]=0x2D;
TRAMEOUT[28]=0x2D;
TRAMEOUT[29]=0x2D;
TRAMEOUT[30]=0x2D;
TRAMEOUT[31]=0x2D;
TRAMEOUT[32]=0x2D;
TRAMEOUT[33]=0x2D;
TRAMEOUT[34]=0x2D;
TRAMEOUT[35]=0x2D;
TRAMEOUT[36]=0x2D;
TRAMEOUT[37]=0x2D;
TRAMEOUT[38]=0x2D;
TRAMEOUT[39]=0x2D;
TRAMEOUT[40]=0x2D;
TRAMEOUT[41]=0x2D;
TRAMEOUT[42]=0x2D;
TRAMEOUT[43]=0x2D;
TRAMEOUT[44]=0x2D;
TRAMEOUT[45]=0x2D;
TRAMEOUT[46]=0x2D;
TRAMEOUT[47]=0x2D;
TRAMEOUT[48]=0x2D;
TRAMEOUT[49]=0x2D;
TRAMEOUT[50]=0x2D;
TRAMEOUT[51]=0x2D;
TRAMEOUT[52]=0x2D;
TRAMEOUT[53]=0x2D;
TRAMEOUT[54]=0x2D;
TRAMEOUT[55]=0x2D;
TRAMEOUT[56]=0x2D;
TRAMEOUT[57]=0x2D;
TRAMEOUT[58]=0x2D;
TRAMEOUT[59]=0x2D;
TRAMEOUT[60]=0x2D;

TRAMEOUT[61]='*';



j=0;
for (iii=4;iii<61;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 57); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (56);*/
}
else if (num == '5')
{
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='5';
TRAMEOUT[3]='a';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x32;
TRAMEOUT[7]=0x4A;
TRAMEOUT[8]=0x1B;
TRAMEOUT[9]=0x5B;
TRAMEOUT[10]=0x31;
TRAMEOUT[11]=0x3B;
TRAMEOUT[12]=0x33;
TRAMEOUT[13]=0x38;
TRAMEOUT[14]=0x48;
TRAMEOUT[15]=0x53;
TRAMEOUT[16]=0x69;
TRAMEOUT[17]=0x74;
TRAMEOUT[18]=0x65;
TRAMEOUT[19]=0x20;
TRAMEOUT[20]=0x3A;
TRAMEOUT[21]=0x1B;
TRAMEOUT[22]=0x5B;
TRAMEOUT[23]=0x30;
TRAMEOUT[24]=0x6D;
TRAMEOUT[25]=0x1B;
TRAMEOUT[26]=0x5B;
TRAMEOUT[27]=0x35;
TRAMEOUT[28]=0x3B;
TRAMEOUT[29]=0x32;
TRAMEOUT[30]=0x31;
TRAMEOUT[31]=0x48;
TRAMEOUT[32]=0x56;
TRAMEOUT[33]=0x49;
TRAMEOUT[34]=0x53;
TRAMEOUT[35]=0x55;
TRAMEOUT[36]=0x41;
TRAMEOUT[37]=0x4C;
TRAMEOUT[38]=0x49;
TRAMEOUT[39]=0x53;
TRAMEOUT[40]=0x41;
TRAMEOUT[41]=0x54;
TRAMEOUT[42]=0x49;
TRAMEOUT[43]=0x4F;
TRAMEOUT[44]=0x4E;
TRAMEOUT[45]=0x20;
TRAMEOUT[46]=0x44;
TRAMEOUT[47]=0x45;
TRAMEOUT[48]=0x53;
TRAMEOUT[49]=0x20;
TRAMEOUT[50]=0x50;
TRAMEOUT[51]=0x41;
TRAMEOUT[52]=0x52;
TRAMEOUT[53]=0x41;
TRAMEOUT[54]=0x4D;
TRAMEOUT[55]=0x45;
TRAMEOUT[56]=0x54;
TRAMEOUT[57]=0x52;
TRAMEOUT[58]=0x45;
TRAMEOUT[59]=0x53;
TRAMEOUT[60]=0x20;
TRAMEOUT[61]=0x44;
TRAMEOUT[62]=0x55;
TRAMEOUT[63]=0x20;
TRAMEOUT[64]=0x47;
TRAMEOUT[65]=0x52;
TRAMEOUT[66]=0x4F;
TRAMEOUT[67]=0x55;
TRAMEOUT[68]=0x50;
TRAMEOUT[69]=0x45;
TRAMEOUT[70]=0x1B;
TRAMEOUT[71]=0x5B;
TRAMEOUT[72]=0x37;
TRAMEOUT[73]=0x3B;
TRAMEOUT[74]=0x37;
TRAMEOUT[75]=0x48;
TRAMEOUT[76]=0x08;
TRAMEOUT[77]=0x20;
TRAMEOUT[78]=0x20;
TRAMEOUT[79]=0x20;
TRAMEOUT[80]=0x20;
TRAMEOUT[81]=0x20;
TRAMEOUT[82]=0x20;
TRAMEOUT[83]=0x20;
TRAMEOUT[84]=0x50;
TRAMEOUT[85]=0x61;
TRAMEOUT[86]=0x72;
TRAMEOUT[87]=0x61;
TRAMEOUT[88]=0x6D;
TRAMEOUT[89]=0x65;
TRAMEOUT[90]=0x74;
TRAMEOUT[91]=0x72;
TRAMEOUT[92]=0x65;
TRAMEOUT[93]=0x73;
TRAMEOUT[94]=0x20;
TRAMEOUT[95]=0x20;
TRAMEOUT[96]=0x20;
TRAMEOUT[97]=0x20;
TRAMEOUT[98]=0x20;
TRAMEOUT[99]=0x20;
TRAMEOUT[100]=0x20;
TRAMEOUT[101]=0x20;
TRAMEOUT[102]=0x20;
TRAMEOUT[103]=0x20;
TRAMEOUT[104]=0x20;
TRAMEOUT[105]=0x20;
TRAMEOUT[106]=0x56;
TRAMEOUT[107]=0x61;
TRAMEOUT[108]=0x6C;
TRAMEOUT[109]=0x65;
TRAMEOUT[110]=0x75;
TRAMEOUT[111]=0x72;
TRAMEOUT[112]=0x20;
TRAMEOUT[113]=0x20;
TRAMEOUT[114]=0x20;
TRAMEOUT[115]=0x20;
TRAMEOUT[116]=0x45;
TRAMEOUT[117]=0x74;
TRAMEOUT[118]=0x61;
TRAMEOUT[119]=0x74;
TRAMEOUT[120]=0x20;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='5';
TRAMEOUT[3]='b';
TRAMEOUT[4]=0x20;
TRAMEOUT[5]=0x20;
TRAMEOUT[6]=0x20;
TRAMEOUT[7]=0x53;
TRAMEOUT[8]=0x65;
TRAMEOUT[9]=0x75;
TRAMEOUT[10]=0x69;
TRAMEOUT[11]=0x6C;
TRAMEOUT[12]=0x2E;
TRAMEOUT[13]=0x48;
TRAMEOUT[14]=0x20;
TRAMEOUT[15]=0x20;
TRAMEOUT[16]=0x20;
TRAMEOUT[17]=0x20;
TRAMEOUT[18]=0x53;
TRAMEOUT[19]=0x65;
TRAMEOUT[20]=0x75;
TRAMEOUT[21]=0x69;
TRAMEOUT[22]=0x6C;
TRAMEOUT[23]=0x2E;
TRAMEOUT[24]=0x42;
TRAMEOUT[25]=0x20;
TRAMEOUT[26]=0x20;
TRAMEOUT[27]=0x20;
TRAMEOUT[28]=0x49;
TRAMEOUT[29]=0x6E;
TRAMEOUT[30]=0x69;
TRAMEOUT[31]=0x74;
TRAMEOUT[32]=0x1B;
TRAMEOUT[33]=0x5B;
TRAMEOUT[34]=0x39;
TRAMEOUT[35]=0x3B;
TRAMEOUT[36]=0x37;
TRAMEOUT[37]=0x48;
TRAMEOUT[38]=0x08;
TRAMEOUT[39]=0x50;
TRAMEOUT[40]=0x2E;
TRAMEOUT[41]=0x72;
TRAMEOUT[42]=0x65;
TRAMEOUT[43]=0x73;
TRAMEOUT[44]=0x65;
TRAMEOUT[45]=0x72;
TRAMEOUT[46]=0x76;
TRAMEOUT[47]=0x6F;
TRAMEOUT[48]=0x69;
TRAMEOUT[49]=0x72;
TRAMEOUT[50]=0x5B;
TRAMEOUT[51]=0x53;
TRAMEOUT[52]=0x48;
TRAMEOUT[53]=0x5D;
TRAMEOUT[54]=0x5B;
TRAMEOUT[55]=0x53;
TRAMEOUT[56]=0x42;
TRAMEOUT[57]=0x5D;
TRAMEOUT[58]=0x28;
TRAMEOUT[59]=0x6D;
TRAMEOUT[60]=0x62;
TRAMEOUT[61]=0x61;
TRAMEOUT[62]=0x72;
TRAMEOUT[63]=0x29;
TRAMEOUT[64]=0x1B;
TRAMEOUT[65]=0x5B;
TRAMEOUT[66]=0x31;
TRAMEOUT[67]=0x30;
TRAMEOUT[68]=0x3B;
TRAMEOUT[69]=0x37;
TRAMEOUT[70]=0x48;
TRAMEOUT[71]=0x08;
TRAMEOUT[72]=0x50;
TRAMEOUT[73]=0x2E;
TRAMEOUT[74]=0x73;
TRAMEOUT[75]=0x6F;
TRAMEOUT[76]=0x72;
TRAMEOUT[77]=0x74;
TRAMEOUT[78]=0x69;
TRAMEOUT[79]=0x65;
TRAMEOUT[80]=0x20;
TRAMEOUT[81]=0x20;
TRAMEOUT[82]=0x20;
TRAMEOUT[83]=0x5B;
TRAMEOUT[84]=0x53;
TRAMEOUT[85]=0x48;
TRAMEOUT[86]=0x5D;
TRAMEOUT[87]=0x5B;
TRAMEOUT[88]=0x53;
TRAMEOUT[89]=0x42;
TRAMEOUT[90]=0x5D;
TRAMEOUT[91]=0x28;
TRAMEOUT[92]=0x6D;
TRAMEOUT[93]=0x62;
TRAMEOUT[94]=0x61;
TRAMEOUT[95]=0x72;
TRAMEOUT[96]=0x29;
TRAMEOUT[97]=0x1B;
TRAMEOUT[98]=0x5B;
TRAMEOUT[99]=0x31;
TRAMEOUT[100]=0x31;
TRAMEOUT[101]=0x3B;
TRAMEOUT[102]=0x37;
TRAMEOUT[103]=0x48;
TRAMEOUT[104]=0x08;
TRAMEOUT[105]=0x50;
TRAMEOUT[106]=0x2E;
TRAMEOUT[107]=0x73;
TRAMEOUT[108]=0x65;
TRAMEOUT[109]=0x63;
TRAMEOUT[110]=0x6F;
TRAMEOUT[111]=0x75;
TRAMEOUT[112]=0x72;
TRAMEOUT[113]=0x73;
TRAMEOUT[114]=0x20;
TRAMEOUT[115]=0x5B;
TRAMEOUT[116]=0x53;
TRAMEOUT[117]=0x42;
TRAMEOUT[118]=0x5D;
TRAMEOUT[119]=0x5B;
TRAMEOUT[120]=0x53;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='5';
TRAMEOUT[3]='c';
TRAMEOUT[4]=0x74;
TRAMEOUT[5]=0x42;
TRAMEOUT[6]=0x5D;
TRAMEOUT[7]=0x28;
TRAMEOUT[8]=0x6D;
TRAMEOUT[9]=0x62;
TRAMEOUT[10]=0x61;
TRAMEOUT[11]=0x72;
TRAMEOUT[12]=0x29;
TRAMEOUT[13]=0x1B;
TRAMEOUT[14]=0x5B;
TRAMEOUT[15]=0x31;
TRAMEOUT[16]=0x32;
TRAMEOUT[17]=0x3B;
TRAMEOUT[18]=0x37;
TRAMEOUT[19]=0x48;
TRAMEOUT[20]=0x08;
TRAMEOUT[21]=0x50;
TRAMEOUT[22]=0x6F;
TRAMEOUT[23]=0x69;
TRAMEOUT[24]=0x6E;
TRAMEOUT[25]=0x74;
TRAMEOUT[26]=0x20;
TRAMEOUT[27]=0x64;
TRAMEOUT[28]=0x65;
TRAMEOUT[29]=0x20;
TRAMEOUT[30]=0x52;
TRAMEOUT[31]=0x6F;
TRAMEOUT[32]=0x73;
TRAMEOUT[33]=0x65;
TRAMEOUT[34]=0x65;
TRAMEOUT[35]=0x20;
TRAMEOUT[36]=0x20;
TRAMEOUT[37]=0x20;
TRAMEOUT[38]=0x20;
TRAMEOUT[39]=0x20;
TRAMEOUT[40]=0x28;
TRAMEOUT[41]=0x6F;
TRAMEOUT[42]=0x4B;
TRAMEOUT[43]=0x29;
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x20;
TRAMEOUT[46]=0x1B;
TRAMEOUT[47]=0x5B;
TRAMEOUT[48]=0x31;
TRAMEOUT[49]=0x33;
TRAMEOUT[50]=0x3B;
TRAMEOUT[51]=0x37;
TRAMEOUT[52]=0x48;
TRAMEOUT[53]=0x08;
TRAMEOUT[54]=0x44;
TRAMEOUT[55]=0x65;
TRAMEOUT[56]=0x62;
TRAMEOUT[57]=0x69;
TRAMEOUT[58]=0x74;
TRAMEOUT[59]=0x20;
TRAMEOUT[60]=0x47;
TRAMEOUT[61]=0x6C;
TRAMEOUT[62]=0x6F;
TRAMEOUT[63]=0x62;
TRAMEOUT[64]=0x61;
TRAMEOUT[65]=0x6C;
TRAMEOUT[66]=0x20;
TRAMEOUT[67]=0x20;
TRAMEOUT[68]=0x20;
TRAMEOUT[69]=0x20;
TRAMEOUT[70]=0x20;
TRAMEOUT[71]=0x20;
TRAMEOUT[72]=0x20;
TRAMEOUT[73]=0x28;
TRAMEOUT[74]=0x6C;
TRAMEOUT[75]=0x2F;
TRAMEOUT[76]=0x68;
TRAMEOUT[77]=0x29;
TRAMEOUT[78]=0x20;
TRAMEOUT[79]=0x1B;
TRAMEOUT[80]=0x5B;
TRAMEOUT[81]=0x31;
TRAMEOUT[82]=0x34;
TRAMEOUT[83]=0x3B;
TRAMEOUT[84]=0x37;
TRAMEOUT[85]=0x48;
TRAMEOUT[86]=0x08;
TRAMEOUT[87]=0x54;
TRAMEOUT[88]=0x65;
TRAMEOUT[89]=0x6D;
TRAMEOUT[90]=0x70;
TRAMEOUT[91]=0x73;
TRAMEOUT[92]=0x20;
TRAMEOUT[93]=0x63;
TRAMEOUT[94]=0x79;
TRAMEOUT[95]=0x63;
TRAMEOUT[96]=0x6C;
TRAMEOUT[97]=0x65;
TRAMEOUT[98]=0x20;
TRAMEOUT[99]=0x6D;
TRAMEOUT[100]=0x6F;
TRAMEOUT[101]=0x74;
TRAMEOUT[102]=0x65;
TRAMEOUT[103]=0x75;
TRAMEOUT[104]=0x72;
TRAMEOUT[105]=0x20;
TRAMEOUT[106]=0x28;
TRAMEOUT[107]=0x73;
TRAMEOUT[108]=0x29;
TRAMEOUT[109]=0x20;
TRAMEOUT[110]=0x20;
TRAMEOUT[111]=0x20;
TRAMEOUT[112]=0x1B;
TRAMEOUT[113]=0x5B;
TRAMEOUT[114]=0x31;
TRAMEOUT[115]=0x35;
TRAMEOUT[116]=0x3B;
TRAMEOUT[117]=0x37;
TRAMEOUT[118]=0x48;
TRAMEOUT[119]=0x1B;
TRAMEOUT[120]=0x5B;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='5';
TRAMEOUT[3]='d';
TRAMEOUT[4]=0x31;
TRAMEOUT[5]=0x36;
TRAMEOUT[6]=0x3B;
TRAMEOUT[7]=0x37;
TRAMEOUT[8]=0x48;
TRAMEOUT[9]=0x08;
TRAMEOUT[10]=0x54;
TRAMEOUT[11]=0x65;
TRAMEOUT[12]=0x6D;
TRAMEOUT[13]=0x70;
TRAMEOUT[14]=0x73;
TRAMEOUT[15]=0x20;
TRAMEOUT[16]=0x6D;
TRAMEOUT[17]=0x6F;
TRAMEOUT[18]=0x74;
TRAMEOUT[19]=0x65;
TRAMEOUT[20]=0x75;
TRAMEOUT[21]=0x72;
TRAMEOUT[22]=0x20;
TRAMEOUT[23]=0x20;
TRAMEOUT[24]=0x20;
TRAMEOUT[25]=0x20;
TRAMEOUT[26]=0x20;
TRAMEOUT[27]=0x20;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]=0x28;
TRAMEOUT[30]=0x68;
TRAMEOUT[31]=0x29;
TRAMEOUT[32]=0x20;
TRAMEOUT[33]=0x1B;
TRAMEOUT[34]=0x5B;
TRAMEOUT[35]=0x31;
TRAMEOUT[36]=0x37;
TRAMEOUT[37]=0x3B;
TRAMEOUT[38]=0x37;
TRAMEOUT[39]=0x48;
TRAMEOUT[40]=0x08;
TRAMEOUT[41]=0x50;
TRAMEOUT[42]=0x72;
TRAMEOUT[43]=0x65;
TRAMEOUT[44]=0x73;
TRAMEOUT[45]=0x65;
TRAMEOUT[46]=0x6E;
TRAMEOUT[47]=0x63;
TRAMEOUT[48]=0x65;
TRAMEOUT[49]=0x20;
TRAMEOUT[50]=0x32;
TRAMEOUT[51]=0x32;
TRAMEOUT[52]=0x30;
TRAMEOUT[53]=0x56;
TRAMEOUT[54]=0x20;
TRAMEOUT[55]=0x20;
TRAMEOUT[56]=0x20;
TRAMEOUT[57]=0x20;
TRAMEOUT[58]=0x20;
TRAMEOUT[59]=0x20;
TRAMEOUT[60]=0x20;
TRAMEOUT[61]=0x20;
TRAMEOUT[62]=0x20;
TRAMEOUT[63]=0x20;
TRAMEOUT[64]=0x20;
TRAMEOUT[65]=0x20;
TRAMEOUT[66]=0x1B;
TRAMEOUT[67]=0x5B;
TRAMEOUT[68]=0x31;
TRAMEOUT[69]=0x38;
TRAMEOUT[70]=0x3B;
TRAMEOUT[71]=0x37;
TRAMEOUT[72]=0x48;
TRAMEOUT[73]=0x08;
TRAMEOUT[74]=0x50;
TRAMEOUT[75]=0x72;
TRAMEOUT[76]=0x65;
TRAMEOUT[77]=0x73;
TRAMEOUT[78]=0x65;
TRAMEOUT[79]=0x6E;
TRAMEOUT[80]=0x63;
TRAMEOUT[81]=0x65;
TRAMEOUT[82]=0x20;
TRAMEOUT[83]=0x20;
TRAMEOUT[84]=0x34;
TRAMEOUT[85]=0x38;
TRAMEOUT[86]=0x56;
//TRAMEOUT[87]=0x20;
//TRAMEOUT[88]=0x20;
//TRAMEOUT[89]=0x20;
//TRAMEOUT[90]=0x20;
//TRAMEOUT[91]=0x20;
//TRAMEOUT[92]=0x20;
//TRAMEOUT[93]=0x20;
//TRAMEOUT[94]=0x20;
//TRAMEOUT[95]=0x20;
//TRAMEOUT[96]=0x20;
//TRAMEOUT[97]=0x20;
//TRAMEOUT[98]=0x20;
//TRAMEOUT[99]=0x1B;
//TRAMEOUT[100]=0x5B;
//TRAMEOUT[101]=0x32;
//TRAMEOUT[102]=0x32;
//TRAMEOUT[103]=0x3B;
//TRAMEOUT[104]=0x32;
//TRAMEOUT[105]=0x33;
//TRAMEOUT[106]=0x48;
//TRAMEOUT[107]=0x1B;
//TRAMEOUT[108]=0x5B;
//TRAMEOUT[109]=0x37;
//TRAMEOUT[110]=0x6D;
//TRAMEOUT[111]=0x52;
//TRAMEOUT[112]=0x45;
//TRAMEOUT[113]=0x54;
//TRAMEOUT[114]=0x1B;
//TRAMEOUT[115]=0x5B;
//TRAMEOUT[116]=0x30;
//TRAMEOUT[117]=0x6D;
//TRAMEOUT[118]=0x20;
//TRAMEOUT[119]=0x3D;
//TRAMEOUT[120]=0x20;
//TRAMEOUT[121]='*';
//j=0;
//for (i=4;i<122;i++){
//TRAMEOUT[j]=TRAMEOUT[i];
//j++;
//}
//TRAMEENVOITCPDONNEES (117);
//TRAMEOUT[0]='#';
//TRAMEOUT[1]='0';
//TRAMEOUT[2]='5';
//TRAMEOUT[3]='e';
//TRAMEOUT[4]=0x76;
//TRAMEOUT[5]=0x61;
//TRAMEOUT[6]=0x6C;
//TRAMEOUT[7]=0x69;
//TRAMEOUT[8]=0x64;
//TRAMEOUT[9]=0x65;
//TRAMEOUT[10]=0x7A;
//TRAMEOUT[11]=0x20;
//TRAMEOUT[12]=0x6C;
//TRAMEOUT[13]=0x65;
//TRAMEOUT[14]=0x73;
//TRAMEOUT[15]=0x20;
//TRAMEOUT[16]=0x70;
//TRAMEOUT[17]=0x61;
//TRAMEOUT[18]=0x72;
//TRAMEOUT[19]=0x61;
//TRAMEOUT[20]=0x6D;
//TRAMEOUT[21]=0x65;
//TRAMEOUT[22]=0x74;
//TRAMEOUT[23]=0x72;
//TRAMEOUT[24]=0x65;
//TRAMEOUT[25]=0x73;
//TRAMEOUT[26]=0x20;
//TRAMEOUT[27]=0x64;
//TRAMEOUT[28]=0x75;
//TRAMEOUT[29]=0x20;
//TRAMEOUT[30]=0x67;
//TRAMEOUT[31]=0x72;
//TRAMEOUT[32]=0x6F;
//TRAMEOUT[33]=0x75;
//TRAMEOUT[34]=0x70;
//TRAMEOUT[35]=0x65;
//TRAMEOUT[36]='*';

j=0;
for (iii=4;iii<87;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 82); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (83);
}
else if (num == '6')
{
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='6';
TRAMEOUT[3]='a';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x32;
TRAMEOUT[7]=0x4A;
TRAMEOUT[8]=0x1B;
TRAMEOUT[9]=0x5B;
TRAMEOUT[10]=0x31;
TRAMEOUT[11]=0x3B;
TRAMEOUT[12]=0x33;
TRAMEOUT[13]=0x38;
TRAMEOUT[14]=0x48;
TRAMEOUT[15]=0x53;
TRAMEOUT[16]=0x69;
TRAMEOUT[17]=0x74;
TRAMEOUT[18]=0x65;
TRAMEOUT[19]=0x20;
TRAMEOUT[20]=0x3A;
TRAMEOUT[21]=0x20;
TRAMEOUT[22]=0x1B;
TRAMEOUT[23]=0x5B;
TRAMEOUT[24]=0x30;
TRAMEOUT[25]=0x6D;
TRAMEOUT[26]=0x1B;
TRAMEOUT[27]=0x5B;
TRAMEOUT[28]=0x33;
TRAMEOUT[29]=0x3B;
TRAMEOUT[30]=0x31;
TRAMEOUT[31]=0x30;
TRAMEOUT[32]=0x48;
TRAMEOUT[33]=0x21;
TRAMEOUT[34]=0x20;
TRAMEOUT[35]=0x41;
TRAMEOUT[36]=0x54;
TRAMEOUT[37]=0x54;
TRAMEOUT[38]=0x45;
TRAMEOUT[39]=0x4E;
TRAMEOUT[40]=0x54;
TRAMEOUT[41]=0x49;
TRAMEOUT[42]=0x4F;
TRAMEOUT[43]=0x4E;
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x4E;
TRAMEOUT[46]=0x45;
TRAMEOUT[47]=0x20;
TRAMEOUT[48]=0x44;
TRAMEOUT[49]=0x45;
TRAMEOUT[50]=0x43;
TRAMEOUT[51]=0x4C;
TRAMEOUT[52]=0x41;
TRAMEOUT[53]=0x52;
TRAMEOUT[54]=0x45;
TRAMEOUT[55]=0x52;
TRAMEOUT[56]=0x20;
TRAMEOUT[57]=0x51;
TRAMEOUT[58]=0x55;
TRAMEOUT[59]=0x27;
TRAMEOUT[60]=0x55;
TRAMEOUT[61]=0x4E;
TRAMEOUT[62]=0x20;
TRAMEOUT[63]=0x20;
TRAMEOUT[64]=0x53;
TRAMEOUT[65]=0x45;
TRAMEOUT[66]=0x55;
TRAMEOUT[67]=0x4C;
TRAMEOUT[68]=0x20;
TRAMEOUT[69]=0x54;
TRAMEOUT[70]=0x59;
TRAMEOUT[71]=0x50;
TRAMEOUT[72]=0x45;
TRAMEOUT[73]=0x20;
TRAMEOUT[74]=0x44;
TRAMEOUT[75]=0x45;
TRAMEOUT[76]=0x20;
TRAMEOUT[77]=0x43;
TRAMEOUT[78]=0x41;
TRAMEOUT[79]=0x50;
TRAMEOUT[80]=0x54;
TRAMEOUT[81]=0x45;
TRAMEOUT[82]=0x55;
TRAMEOUT[83]=0x52;
TRAMEOUT[84]=0x20;
TRAMEOUT[85]=0x50;
TRAMEOUT[86]=0x41;
TRAMEOUT[87]=0x52;
TRAMEOUT[88]=0x20;
TRAMEOUT[89]=0x43;
TRAMEOUT[90]=0x41;
TRAMEOUT[91]=0x52;
TRAMEOUT[92]=0x54;
TRAMEOUT[93]=0x45;
TRAMEOUT[94]=0x20;
TRAMEOUT[95]=0x21;
TRAMEOUT[96]=0x1B;
TRAMEOUT[97]=0x5B;
TRAMEOUT[98]=0x35;
TRAMEOUT[99]=0x3B;
TRAMEOUT[100]=0x32;
TRAMEOUT[101]=0x30;
TRAMEOUT[102]=0x48;
TRAMEOUT[103]=0x43;
TRAMEOUT[104]=0x41;
TRAMEOUT[105]=0x52;
TRAMEOUT[106]=0x54;
TRAMEOUT[107]=0x45;
TRAMEOUT[108]=0x20;
TRAMEOUT[109]=0x31;
TRAMEOUT[110]=0x20;
TRAMEOUT[111]=0x28;
TRAMEOUT[112]=0x56;
TRAMEOUT[113]=0x4F;
TRAMEOUT[114]=0x49;
TRAMEOUT[115]=0x45;
TRAMEOUT[116]=0x20;
TRAMEOUT[117]=0x31;
TRAMEOUT[118]=0x20;
TRAMEOUT[119]=0x41;
TRAMEOUT[120]=0x20;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='6';
TRAMEOUT[3]='b';
TRAMEOUT[4]=0x32;
TRAMEOUT[5]=0x30;
TRAMEOUT[6]=0x20;
TRAMEOUT[7]=0x29;
TRAMEOUT[8]=0x20;
TRAMEOUT[9]=0x20;
TRAMEOUT[10]=0x20;
TRAMEOUT[11]=0x3A;
TRAMEOUT[12]=0x20;
TRAMEOUT[13]=0x1B;
TRAMEOUT[14]=0x5B;
TRAMEOUT[15]=0x36;
TRAMEOUT[16]=0x3B;
TRAMEOUT[17]=0x31;
TRAMEOUT[18]=0x30;
TRAMEOUT[19]=0x48;
TRAMEOUT[20]=0x56;
TRAMEOUT[21]=0x4F;
TRAMEOUT[22]=0x49;
TRAMEOUT[23]=0x45;
TRAMEOUT[24]=0x53;
TRAMEOUT[25]=0x20;
TRAMEOUT[26]=0x44;
TRAMEOUT[27]=0x45;
TRAMEOUT[28]=0x43;
TRAMEOUT[29]=0x4C;
TRAMEOUT[30]=0x41;
TRAMEOUT[31]=0x52;
TRAMEOUT[32]=0x45;
TRAMEOUT[33]=0x45;
TRAMEOUT[34]=0x53;
TRAMEOUT[35]=0x20;
TRAMEOUT[36]=0x3A;
TRAMEOUT[37]=0x1B;
TRAMEOUT[38]=0x5B;
TRAMEOUT[39]=0x39;
TRAMEOUT[40]=0x3B;
TRAMEOUT[41]=0x32;
TRAMEOUT[42]=0x30;
TRAMEOUT[43]=0x48;
TRAMEOUT[44]=0x43;
TRAMEOUT[45]=0x41;
TRAMEOUT[46]=0x52;
TRAMEOUT[47]=0x54;
TRAMEOUT[48]=0x45;
TRAMEOUT[49]=0x20;
TRAMEOUT[50]=0x32;
TRAMEOUT[51]=0x20;
TRAMEOUT[52]=0x28;
TRAMEOUT[53]=0x56;
TRAMEOUT[54]=0x4F;
TRAMEOUT[55]=0x49;
TRAMEOUT[56]=0x45;
TRAMEOUT[57]=0x20;
TRAMEOUT[58]=0x32;
TRAMEOUT[59]=0x31;
TRAMEOUT[60]=0x20;
TRAMEOUT[61]=0x41;
TRAMEOUT[62]=0x20;
TRAMEOUT[63]=0x34;
TRAMEOUT[64]=0x30;
TRAMEOUT[65]=0x20;
TRAMEOUT[66]=0x29;
TRAMEOUT[67]=0x20;
TRAMEOUT[68]=0x20;
TRAMEOUT[69]=0x3A;
TRAMEOUT[70]=0x20;
TRAMEOUT[71]=0x1B;
TRAMEOUT[72]=0x5B;
TRAMEOUT[73]=0x31;
TRAMEOUT[74]=0x30;
TRAMEOUT[75]=0x3B;
TRAMEOUT[76]=0x31;
TRAMEOUT[77]=0x30;
TRAMEOUT[78]=0x48;
TRAMEOUT[79]=0x56;
TRAMEOUT[80]=0x4F;
TRAMEOUT[81]=0x49;
TRAMEOUT[82]=0x45;
TRAMEOUT[83]=0x53;
TRAMEOUT[84]=0x20;
TRAMEOUT[85]=0x44;
TRAMEOUT[86]=0x45;
TRAMEOUT[87]=0x43;
TRAMEOUT[88]=0x4C;
TRAMEOUT[89]=0x41;
TRAMEOUT[90]=0x52;
TRAMEOUT[91]=0x45;
TRAMEOUT[92]=0x45;
TRAMEOUT[93]=0x53;
TRAMEOUT[94]=0x20;
TRAMEOUT[95]=0x3A;
TRAMEOUT[96]=0x1B;
TRAMEOUT[97]=0x5B;
TRAMEOUT[98]=0x31;
TRAMEOUT[99]=0x33;
TRAMEOUT[100]=0x3B;
TRAMEOUT[101]=0x32;
TRAMEOUT[102]=0x30;
TRAMEOUT[103]=0x48;
TRAMEOUT[104]=0x43;
TRAMEOUT[105]=0x41;
TRAMEOUT[106]=0x52;
TRAMEOUT[107]=0x54;
TRAMEOUT[108]=0x45;
TRAMEOUT[109]=0x20;
TRAMEOUT[110]=0x33;
TRAMEOUT[111]=0x20;
TRAMEOUT[112]=0x28;
TRAMEOUT[113]=0x56;
TRAMEOUT[114]=0x4F;
TRAMEOUT[115]=0x49;
TRAMEOUT[116]=0x45;
TRAMEOUT[117]=0x20;
TRAMEOUT[118]=0x34;
TRAMEOUT[119]=0x31;
TRAMEOUT[120]=0x20;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='6';
TRAMEOUT[3]='c';
TRAMEOUT[4]=0x41;
TRAMEOUT[5]=0x20;
TRAMEOUT[6]=0x36;
TRAMEOUT[7]=0x30;
TRAMEOUT[8]=0x20;
TRAMEOUT[9]=0x29;
TRAMEOUT[10]=0x20;
TRAMEOUT[11]=0x20;
TRAMEOUT[12]=0x3A;
TRAMEOUT[13]=0x20;
TRAMEOUT[14]=0x1B;
TRAMEOUT[15]=0x5B;
TRAMEOUT[16]=0x31;
TRAMEOUT[17]=0x34;
TRAMEOUT[18]=0x3B;
TRAMEOUT[19]=0x31;
TRAMEOUT[20]=0x30;
TRAMEOUT[21]=0x48;
TRAMEOUT[22]=0x56;
TRAMEOUT[23]=0x4F;
TRAMEOUT[24]=0x49;
TRAMEOUT[25]=0x45;
TRAMEOUT[26]=0x53;
TRAMEOUT[27]=0x20;
TRAMEOUT[28]=0x44;
TRAMEOUT[29]=0x45;
TRAMEOUT[30]=0x43;
TRAMEOUT[31]=0x4C;
TRAMEOUT[32]=0x41;
TRAMEOUT[33]=0x52;
TRAMEOUT[34]=0x45;
TRAMEOUT[35]=0x45;
TRAMEOUT[36]=0x53;
TRAMEOUT[37]=0x20;
TRAMEOUT[38]=0x3A;
TRAMEOUT[39]=0x1B;
TRAMEOUT[40]=0x5B;
TRAMEOUT[41]=0x31;
TRAMEOUT[42]=0x37;
TRAMEOUT[43]=0x3B;
TRAMEOUT[44]=0x32;
TRAMEOUT[45]=0x30;
TRAMEOUT[46]=0x48;
TRAMEOUT[47]=0x43;
TRAMEOUT[48]=0x41;
TRAMEOUT[49]=0x52;
TRAMEOUT[50]=0x54;
TRAMEOUT[51]=0x45;
TRAMEOUT[52]=0x20;
TRAMEOUT[53]=0x34;
TRAMEOUT[54]=0x20;
TRAMEOUT[55]=0x28;
TRAMEOUT[56]=0x56;
TRAMEOUT[57]=0x4F;
TRAMEOUT[58]=0x49;
TRAMEOUT[59]=0x45;
TRAMEOUT[60]=0x20;
TRAMEOUT[61]=0x36;
TRAMEOUT[62]=0x31;
TRAMEOUT[63]=0x20;
TRAMEOUT[64]=0x41;
TRAMEOUT[65]=0x20;
TRAMEOUT[66]=0x38;
TRAMEOUT[67]=0x30;
TRAMEOUT[68]=0x20;
TRAMEOUT[69]=0x29;
TRAMEOUT[70]=0x20;
TRAMEOUT[71]=0x20;
TRAMEOUT[72]=0x3A;
TRAMEOUT[73]=0x20;
TRAMEOUT[74]=0x1B;
TRAMEOUT[75]=0x5B;
TRAMEOUT[76]=0x31;
TRAMEOUT[77]=0x38;
TRAMEOUT[78]=0x3B;
TRAMEOUT[79]=0x31;
TRAMEOUT[80]=0x30;
TRAMEOUT[81]=0x48;
TRAMEOUT[82]=0x56;
TRAMEOUT[83]=0x4F;
TRAMEOUT[84]=0x49;
TRAMEOUT[85]=0x45;
TRAMEOUT[86]=0x53;
TRAMEOUT[87]=0x20;
TRAMEOUT[88]=0x44;
TRAMEOUT[89]=0x45;
TRAMEOUT[90]=0x43;
TRAMEOUT[91]=0x4C;
TRAMEOUT[92]=0x41;
TRAMEOUT[93]=0x52;
TRAMEOUT[94]=0x45;
TRAMEOUT[95]=0x45;
TRAMEOUT[96]=0x53;
TRAMEOUT[97]=0x20;
TRAMEOUT[98]=0x3A;
TRAMEOUT[99]=0x1B;
TRAMEOUT[100]=0x5B;
TRAMEOUT[101]=0x32;
TRAMEOUT[102]=0x31;
TRAMEOUT[103]=0x3B;
TRAMEOUT[104]=0x32;
TRAMEOUT[105]=0x30;
TRAMEOUT[106]=0x48;
TRAMEOUT[107]=0x43;
TRAMEOUT[108]=0x41;
TRAMEOUT[109]=0x52;
TRAMEOUT[110]=0x54;
TRAMEOUT[111]=0x45;
TRAMEOUT[112]=0x20;
TRAMEOUT[113]=0x35;
TRAMEOUT[114]=0x20;
TRAMEOUT[115]=0x28;
TRAMEOUT[116]=0x56;
TRAMEOUT[117]=0x4F;
TRAMEOUT[118]=0x49;
TRAMEOUT[119]=0x45;
TRAMEOUT[120]=0x20;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='6';
TRAMEOUT[3]='d';
TRAMEOUT[4]=0x38;
TRAMEOUT[5]=0x31;
TRAMEOUT[6]=0x20;
TRAMEOUT[7]=0x41;
TRAMEOUT[8]=0x20;
TRAMEOUT[9]=0x31;
TRAMEOUT[10]=0x30;
TRAMEOUT[11]=0x30;
TRAMEOUT[12]=0x20;
TRAMEOUT[13]=0x29;
TRAMEOUT[14]=0x20;
TRAMEOUT[15]=0x3A;
TRAMEOUT[16]=0x20;
TRAMEOUT[17]=0x1B;
TRAMEOUT[18]=0x5B;
TRAMEOUT[19]=0x32;
TRAMEOUT[20]=0x32;
TRAMEOUT[21]=0x3B;
TRAMEOUT[22]=0x31;
TRAMEOUT[23]=0x30;
TRAMEOUT[24]=0x48;
TRAMEOUT[25]=0x56;
TRAMEOUT[26]=0x4F;
TRAMEOUT[27]=0x49;
TRAMEOUT[28]=0x45;
TRAMEOUT[29]=0x53;
TRAMEOUT[30]=0x20;
TRAMEOUT[31]=0x44;
TRAMEOUT[32]=0x45;
TRAMEOUT[33]=0x43;
TRAMEOUT[34]=0x4C;
TRAMEOUT[35]=0x41;
TRAMEOUT[36]=0x52;
TRAMEOUT[37]=0x45;
TRAMEOUT[38]=0x45;
TRAMEOUT[39]=0x53;
TRAMEOUT[40]=0x20;
TRAMEOUT[41]=0x3A;
TRAMEOUT[42]=0x20;
TRAMEOUT[43]=0x20;
TRAMEOUT[44]=0x1B;
TRAMEOUT[45]=0x5B;
TRAMEOUT[46]=0x32;
TRAMEOUT[47]=0x34;
TRAMEOUT[48]=0x3B;
TRAMEOUT[49]=0x32;
TRAMEOUT[50]=0x30;
TRAMEOUT[51]=0x48;
TRAMEOUT[52]=0x1B;
TRAMEOUT[53]=0x5B;
TRAMEOUT[54]=0x37;
TRAMEOUT[55]=0x6D;
TRAMEOUT[56]=0x45;
TRAMEOUT[57]=0x4E;
TRAMEOUT[58]=0x54;
TRAMEOUT[59]=0x52;
TRAMEOUT[60]=0x45;
TRAMEOUT[61]=0x45;
TRAMEOUT[62]=0x1B;
TRAMEOUT[63]=0x5B;
TRAMEOUT[64]=0x30;
TRAMEOUT[65]=0x6D;
TRAMEOUT[66]=0x20;
TRAMEOUT[67]=0x3A;
TRAMEOUT[68]=0x20;
TRAMEOUT[69]=0x50;
TRAMEOUT[70]=0x61;
TRAMEOUT[71]=0x67;
TRAMEOUT[72]=0x65;
TRAMEOUT[73]=0x73;
TRAMEOUT[74]=0x20;
TRAMEOUT[75]=0x73;
TRAMEOUT[76]=0x75;
TRAMEOUT[77]=0x69;
TRAMEOUT[78]=0x76;
TRAMEOUT[79]=0x61;
TRAMEOUT[80]=0x6E;
TRAMEOUT[81]=0x74;
TRAMEOUT[82]=0x65;
TRAMEOUT[83]=0x73;
TRAMEOUT[84]='*';

j=0;
for (iii=4;iii<84;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 80); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (80);
}
else if (num == 'a') /////////////////////////////////////////////6a
{
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='7';
TRAMEOUT[3]='a';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x32;
TRAMEOUT[7]=0x4A;
TRAMEOUT[8]=0x1B;
TRAMEOUT[9]=0x5B;
TRAMEOUT[10]=0x31;
TRAMEOUT[11]=0x3B;
TRAMEOUT[12]=0x33;
TRAMEOUT[13]=0x38;
TRAMEOUT[14]=0x48;
TRAMEOUT[15]=0x53;
TRAMEOUT[16]=0x69;
TRAMEOUT[17]=0x74;
TRAMEOUT[18]=0x65;
TRAMEOUT[19]=0x20;
TRAMEOUT[20]=0x3A;
TRAMEOUT[21]=0x1B;
TRAMEOUT[22]=0x5B;
TRAMEOUT[23]=0x34;
TRAMEOUT[24]=0x3B;
TRAMEOUT[25]=0x32;
TRAMEOUT[26]=0x38;
TRAMEOUT[27]=0x48;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]=0x43;
TRAMEOUT[30]=0x4F;
TRAMEOUT[31]=0x4E;
TRAMEOUT[32]=0x46;
TRAMEOUT[33]=0x49;
TRAMEOUT[34]=0x47;
TRAMEOUT[35]=0x55;
TRAMEOUT[36]=0x52;
TRAMEOUT[37]=0x41;
TRAMEOUT[38]=0x54;
TRAMEOUT[39]=0x49;
TRAMEOUT[40]=0x4F;
TRAMEOUT[41]=0x4E;
TRAMEOUT[42]=0x20;
TRAMEOUT[43]=0x44;
TRAMEOUT[44]=0x27;
TRAMEOUT[45]=0x55;
TRAMEOUT[46]=0x4E;
TRAMEOUT[47]=0x20;
TRAMEOUT[48]=0x43;
TRAMEOUT[49]=0x41;
TRAMEOUT[50]=0x42;
TRAMEOUT[51]=0x4C;
TRAMEOUT[52]=0x45;
TRAMEOUT[53]=0x20;
TRAMEOUT[54]=0x1B;
TRAMEOUT[55]=0x5B;
TRAMEOUT[56]=0x30;
TRAMEOUT[57]=0x6D;
TRAMEOUT[58]=0x1B;
TRAMEOUT[59]=0x5B;
TRAMEOUT[60]=0x35;
TRAMEOUT[61]=0x3B;
TRAMEOUT[62]=0x33;
TRAMEOUT[63]=0x38;
TRAMEOUT[64]=0x48;
TRAMEOUT[65]=0x1B;
TRAMEOUT[66]=0x5B;
TRAMEOUT[67]=0x36;
TRAMEOUT[68]=0x3B;
TRAMEOUT[69]=0x31;
TRAMEOUT[70]=0x35;
TRAMEOUT[71]=0x48;
TRAMEOUT[72]=0x4E;
TRAMEOUT[73]=0x6F;
TRAMEOUT[74]=0x20;
TRAMEOUT[75]=0x64;
TRAMEOUT[76]=0x65;
TRAMEOUT[77]=0x20;
TRAMEOUT[78]=0x76;
TRAMEOUT[79]=0x6F;
TRAMEOUT[80]=0x69;
TRAMEOUT[81]=0x65;
TRAMEOUT[82]=0x20;
TRAMEOUT[83]=0x3A;
TRAMEOUT[84]=0x20;
TRAMEOUT[85]=0x1B;
TRAMEOUT[86]=0x5B;
TRAMEOUT[87]=0x37;
TRAMEOUT[88]=0x3B;
TRAMEOUT[89]=0x31;
TRAMEOUT[90]=0x35;
TRAMEOUT[91]=0x48;
TRAMEOUT[92]=0x52;
TRAMEOUT[93]=0x4F;
TRAMEOUT[94]=0x42;
TRAMEOUT[95]=0x3A;
TRAMEOUT[96]=0x52;
TRAMEOUT[97]=0x6F;
TRAMEOUT[98]=0x20;
TRAMEOUT[99]=0x1B;
TRAMEOUT[100]=0x5B;
TRAMEOUT[101]=0x37;
TRAMEOUT[102]=0x3B;
TRAMEOUT[103]=0x33;
TRAMEOUT[104]=0x38;
TRAMEOUT[105]=0x48;
TRAMEOUT[106]=0x1B;
TRAMEOUT[107]=0x5B;
TRAMEOUT[108]=0x39;
TRAMEOUT[109]=0x3B;
TRAMEOUT[110]=0x31;
TRAMEOUT[111]=0x35;
TRAMEOUT[112]=0x48;
TRAMEOUT[113]=0x43;
TRAMEOUT[114]=0x6F;
TRAMEOUT[115]=0x6D;
TRAMEOUT[116]=0x6D;
TRAMEOUT[117]=0x65;
TRAMEOUT[118]=0x6E;
TRAMEOUT[119]=0x74;
TRAMEOUT[120]=0x61;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='7';
TRAMEOUT[3]='b';
TRAMEOUT[4]=0x69;
TRAMEOUT[5]=0x72;
TRAMEOUT[6]=0x65;
TRAMEOUT[7]=0x20;
TRAMEOUT[8]=0x3A;
TRAMEOUT[9]=0x20;
TRAMEOUT[10]=0x20;
TRAMEOUT[11]=0x20;
TRAMEOUT[12]=0x20;
TRAMEOUT[13]=0x0A;
TRAMEOUT[14]=0x08;
TRAMEOUT[15]=0x1B;
TRAMEOUT[16]=0x5B;
TRAMEOUT[17]=0x36;
TRAMEOUT[18]=0x3B;
TRAMEOUT[19]=0x34;
TRAMEOUT[20]=0x32;
TRAMEOUT[21]=0x48;
TRAMEOUT[22]=0x4E;
TRAMEOUT[23]=0x75;
TRAMEOUT[24]=0x6D;
TRAMEOUT[25]=0x65;
TRAMEOUT[26]=0x72;
TRAMEOUT[27]=0x6F;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]=0x64;
TRAMEOUT[30]=0x65;
TRAMEOUT[31]=0x20;
TRAMEOUT[32]=0x63;
TRAMEOUT[33]=0x61;
TRAMEOUT[34]=0x72;
TRAMEOUT[35]=0x74;
TRAMEOUT[36]=0x65;
TRAMEOUT[37]=0x3A;
TRAMEOUT[38]=0x20;
TRAMEOUT[39]=0x20;
TRAMEOUT[40]=0x20;
TRAMEOUT[41]=0x0A;
TRAMEOUT[42]=0x08;
TRAMEOUT[43]=0x08;
TRAMEOUT[44]=0x08;
TRAMEOUT[45]=0x08;
TRAMEOUT[46]=0x1B;
TRAMEOUT[47]=0x5B;
TRAMEOUT[48]=0x37;
TRAMEOUT[49]=0x3B;
TRAMEOUT[50]=0x34;
TRAMEOUT[51]=0x32;
TRAMEOUT[52]=0x48;
TRAMEOUT[53]=0x4E;
TRAMEOUT[54]=0x75;
TRAMEOUT[55]=0x6D;
TRAMEOUT[56]=0x65;
TRAMEOUT[57]=0x72;
TRAMEOUT[58]=0x6F;
TRAMEOUT[59]=0x20;
TRAMEOUT[60]=0x64;
TRAMEOUT[61]=0x65;
TRAMEOUT[62]=0x20;
TRAMEOUT[63]=0x6C;
TRAMEOUT[64]=0x69;
TRAMEOUT[65]=0x67;
TRAMEOUT[66]=0x6E;
TRAMEOUT[67]=0x65;
TRAMEOUT[68]=0x3A;
TRAMEOUT[69]=0x20;
TRAMEOUT[70]=0x1B;
TRAMEOUT[71]=0x5B;
TRAMEOUT[72]=0x38;
TRAMEOUT[73]=0x3B;
TRAMEOUT[74]=0x34;
TRAMEOUT[75]=0x31;
TRAMEOUT[76]=0x48;
TRAMEOUT[77]=0x20;
TRAMEOUT[78]=0x0A;
TRAMEOUT[79]=0x08;
TRAMEOUT[80]=0x08;
TRAMEOUT[81]=0x20;
TRAMEOUT[82]=0x20;
TRAMEOUT[83]=0x1B;
TRAMEOUT[84]=0x5B;
TRAMEOUT[85]=0x32;
TRAMEOUT[86]=0x32;
TRAMEOUT[87]=0x3B;
TRAMEOUT[88]=0x31;
TRAMEOUT[89]=0x48;
TRAMEOUT[90]=0x1B;
TRAMEOUT[91]=0x5B;
TRAMEOUT[92]=0x37;
TRAMEOUT[93]=0x6D;
TRAMEOUT[94]=0x53;
TRAMEOUT[95]=0x41;
TRAMEOUT[96]=0x49;
TRAMEOUT[97]=0x53;
TRAMEOUT[98]=0x49;
TRAMEOUT[99]=0x52;
TRAMEOUT[100]=0x1B;
TRAMEOUT[101]=0x5B;
TRAMEOUT[102]=0x30;
TRAMEOUT[103]=0x6D;//21092020
TRAMEOUT[104]=0x20;
TRAMEOUT[105]=0x20;
TRAMEOUT[106]=0x20;
TRAMEOUT[107]=0x20;
TRAMEOUT[108]=0x6C;
TRAMEOUT[109]=0x65;
TRAMEOUT[110]=0x20;
TRAMEOUT[111]=0x6E;
TRAMEOUT[112]=0x75;
TRAMEOUT[113]=0x6D;
TRAMEOUT[114]=0x65;
TRAMEOUT[115]=0x72;
TRAMEOUT[116]=0x6F;
TRAMEOUT[117]=0x20;
TRAMEOUT[118]=0x64;
TRAMEOUT[119]=0x65;
TRAMEOUT[120]=0x20;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='7';
TRAMEOUT[3]='c';
TRAMEOUT[4]=0x76;//63
TRAMEOUT[5]=0x6F;//61
TRAMEOUT[6]=0x69;//62
TRAMEOUT[7]=0x65;//6c
TRAMEOUT[8]=0x20;//65
TRAMEOUT[9]=0x61;
TRAMEOUT[10]=0x20;
TRAMEOUT[11]=0x65;
TRAMEOUT[12]=0x64;
TRAMEOUT[13]=0x69;
TRAMEOUT[14]=0x74;
TRAMEOUT[15]=0x65;
TRAMEOUT[16]=0x72;
TRAMEOUT[17]='*';
j=0;
for (iii=4;iii<18;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 14); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (13);

}
else if (num == 'b')////////////////////////////////////////////6c
{
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='8';
TRAMEOUT[3]='a';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x37;
TRAMEOUT[7]=0x6D;
TRAMEOUT[8]=0x1B;
TRAMEOUT[9]=0x5B;
TRAMEOUT[10]=0x34;
TRAMEOUT[11]=0x3B;
TRAMEOUT[12]=0x32;
TRAMEOUT[13]=0x32;
TRAMEOUT[14]=0x48;
TRAMEOUT[15]=0x20;
TRAMEOUT[16]=0x43;
TRAMEOUT[17]=0x4F;
TRAMEOUT[18]=0x4E;
TRAMEOUT[19]=0x46;
TRAMEOUT[20]=0x49;
TRAMEOUT[21]=0x47;
TRAMEOUT[22]=0x55;
TRAMEOUT[23]=0x52;
TRAMEOUT[24]=0x41;
TRAMEOUT[25]=0x54;
TRAMEOUT[26]=0x49;
TRAMEOUT[27]=0x4F;
TRAMEOUT[28]=0x4E;
TRAMEOUT[29]=0x20;
TRAMEOUT[30]=0x44;
TRAMEOUT[31]=0x45;
TRAMEOUT[32]=0x53;
TRAMEOUT[33]=0x20;
TRAMEOUT[34]=0x43;
TRAMEOUT[35]=0x41;
TRAMEOUT[36]=0x50;
TRAMEOUT[37]=0x54;
TRAMEOUT[38]=0x45;
TRAMEOUT[39]=0x55;
TRAMEOUT[40]=0x52;
TRAMEOUT[41]=0x53;
TRAMEOUT[42]=0x20;
TRAMEOUT[43]=0x44;
TRAMEOUT[44]=0x27;
TRAMEOUT[45]=0x55;
TRAMEOUT[46]=0x4E;
TRAMEOUT[47]=0x20;
TRAMEOUT[48]=0x43;
TRAMEOUT[49]=0x41;
TRAMEOUT[50]=0x42;
TRAMEOUT[51]=0x4C;
TRAMEOUT[52]=0x45;
TRAMEOUT[53]=0x20;
TRAMEOUT[54]=0x1B;
TRAMEOUT[55]=0x5B;
TRAMEOUT[56]=0x30;
TRAMEOUT[57]=0x6D;
TRAMEOUT[58]=0x1B;
TRAMEOUT[59]=0x5B;
TRAMEOUT[60]=0x31;
TRAMEOUT[61]=0x31;
TRAMEOUT[62]=0x3B;
TRAMEOUT[63]=0x32;
TRAMEOUT[64]=0x48;
TRAMEOUT[65]=0x2D;
TRAMEOUT[66]=0x2D;
TRAMEOUT[67]=0x2D;
TRAMEOUT[68]=0x2D;
TRAMEOUT[69]=0x2D;
TRAMEOUT[70]=0x2D;
TRAMEOUT[71]=0x2D;
TRAMEOUT[72]=0x2D;
TRAMEOUT[73]=0x2D;
TRAMEOUT[74]=0x2D;
TRAMEOUT[75]=0x2D;
TRAMEOUT[76]=0x2D;
TRAMEOUT[77]=0x2D;
TRAMEOUT[78]=0x2D;
TRAMEOUT[79]=0x2D;
TRAMEOUT[80]=0x2D;
TRAMEOUT[81]=0x2D;
TRAMEOUT[82]=0x2D;
TRAMEOUT[83]=0x2D;
TRAMEOUT[84]=0x2D;
TRAMEOUT[85]=0x2D;
TRAMEOUT[86]=0x2D;
TRAMEOUT[87]=0x2D;
TRAMEOUT[88]=0x2D;
TRAMEOUT[89]=0x2D;
TRAMEOUT[90]=0x2D;
TRAMEOUT[91]=0x2D;
TRAMEOUT[92]=0x2D;
TRAMEOUT[93]=0x2D;
TRAMEOUT[94]=0x2D;
TRAMEOUT[95]=0x2D;
TRAMEOUT[96]=0x2D;
TRAMEOUT[97]=0x2D;
TRAMEOUT[98]=0x2D;
TRAMEOUT[99]=0x2D;
TRAMEOUT[100]=0x2D;
TRAMEOUT[101]=0x2D;
TRAMEOUT[102]=0x2D;
TRAMEOUT[103]=0x2D;
TRAMEOUT[104]=0x2D;
TRAMEOUT[105]=0x2D;
TRAMEOUT[106]=0x2D;
TRAMEOUT[107]=0x2D;
TRAMEOUT[108]=0x2D;
TRAMEOUT[109]=0x2D;
TRAMEOUT[110]=0x2D;
TRAMEOUT[111]=0x2D;
TRAMEOUT[112]=0x2D;
TRAMEOUT[113]=0x2D;
TRAMEOUT[114]=0x2D;
TRAMEOUT[115]=0x2D;
TRAMEOUT[116]=0x2D;
TRAMEOUT[117]=0x2D;
TRAMEOUT[118]=0x2D;
TRAMEOUT[119]=0x2D;
TRAMEOUT[120]=0x2D;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='8';
TRAMEOUT[3]='b';
TRAMEOUT[4]=0x2D;
TRAMEOUT[5]=0x2D;
TRAMEOUT[6]=0x2D;
TRAMEOUT[7]=0x2D;
TRAMEOUT[8]=0x2D;
TRAMEOUT[9]=0x2D;
TRAMEOUT[10]=0x2D;
TRAMEOUT[11]=0x2D;
TRAMEOUT[12]=0x2D;
TRAMEOUT[13]=0x2D;
TRAMEOUT[14]=0x2D;
TRAMEOUT[15]=0x2D;
TRAMEOUT[16]=0x2D;
TRAMEOUT[17]=0x2D;
TRAMEOUT[18]=0x2D;
TRAMEOUT[19]=0x2D;
TRAMEOUT[20]=0x2D;
TRAMEOUT[21]=0x2D;
TRAMEOUT[22]=0x2D;
TRAMEOUT[23]=0x2D;
TRAMEOUT[24]=0x2D;
TRAMEOUT[25]=0x2D;
TRAMEOUT[26]=0x1B;
TRAMEOUT[27]=0x5B;
TRAMEOUT[28]=0x30;
TRAMEOUT[29]=0x4B;
TRAMEOUT[30]=0x1B;
TRAMEOUT[31]=0x5B;
TRAMEOUT[32]=0x31;
TRAMEOUT[33]=0x32;
TRAMEOUT[34]=0x3B;
TRAMEOUT[35]=0x31;
TRAMEOUT[36]=0x35;
TRAMEOUT[37]=0x48;
TRAMEOUT[38]=0x1B;
TRAMEOUT[39]=0x5B;
TRAMEOUT[40]=0x30;
TRAMEOUT[41]=0x4B;
TRAMEOUT[42]=0x43;
TRAMEOUT[43]=0x6F;
TRAMEOUT[44]=0x64;
TRAMEOUT[45]=0x61;
TRAMEOUT[46]=0x67;
TRAMEOUT[47]=0x65;
TRAMEOUT[48]=0x20;
TRAMEOUT[49]=0x20;
TRAMEOUT[50]=0x20;
TRAMEOUT[51]=0x20;
TRAMEOUT[52]=0x20;
TRAMEOUT[53]=0x20;
TRAMEOUT[54]=0x3A;
TRAMEOUT[55]=0x1B;
TRAMEOUT[56]=0x5B;
TRAMEOUT[57]=0x31;
TRAMEOUT[58]=0x33;
TRAMEOUT[59]=0x3B;
TRAMEOUT[60]=0x31;
TRAMEOUT[61]=0x32;
TRAMEOUT[62]=0x48;
TRAMEOUT[63]=0x54;
TRAMEOUT[64]=0x79;
TRAMEOUT[65]=0x70;
TRAMEOUT[66]=0x65;
TRAMEOUT[67]=0x20;
TRAMEOUT[68]=0x64;
TRAMEOUT[69]=0x65;
TRAMEOUT[70]=0x20;
TRAMEOUT[71]=0x63;
TRAMEOUT[72]=0x61;
TRAMEOUT[73]=0x70;
TRAMEOUT[74]=0x74;
TRAMEOUT[75]=0x65;
TRAMEOUT[76]=0x75;
TRAMEOUT[77]=0x72;
TRAMEOUT[78]=0x3A;
TRAMEOUT[79]=0x0A;
TRAMEOUT[80]=0x08;
TRAMEOUT[81]=0x1B;
TRAMEOUT[82]=0x5B;
TRAMEOUT[83]=0x31;
TRAMEOUT[84]=0x34;
TRAMEOUT[85]=0x3B;
TRAMEOUT[86]=0x31;
TRAMEOUT[87]=0x35;
TRAMEOUT[88]=0x48;
TRAMEOUT[89]=0x1B;
TRAMEOUT[90]=0x5B;
TRAMEOUT[91]=0x30;
TRAMEOUT[92]=0x4B;
TRAMEOUT[93]=0x43;
TRAMEOUT[94]=0x6F;
TRAMEOUT[95]=0x6E;
TRAMEOUT[96]=0x73;
TRAMEOUT[97]=0x74;
TRAMEOUT[98]=0x69;
TRAMEOUT[99]=0x74;
TRAMEOUT[100]=0x75;
TRAMEOUT[101]=0x74;
TRAMEOUT[102]=0x69;
TRAMEOUT[103]=0x6F;
TRAMEOUT[104]=0x6E;
TRAMEOUT[105]=0x3A;
TRAMEOUT[106]=0x20;
TRAMEOUT[107]=0x1B;
TRAMEOUT[108]=0x5B;
TRAMEOUT[109]=0x31;
TRAMEOUT[110]=0x35;
TRAMEOUT[111]=0x3B;
TRAMEOUT[112]=0x31;
TRAMEOUT[113]=0x35;
TRAMEOUT[114]=0x48;
TRAMEOUT[115]=0x1B;
TRAMEOUT[116]=0x5B;
TRAMEOUT[117]=0x30;
TRAMEOUT[118]=0x4B;
TRAMEOUT[119]=0x53;
TRAMEOUT[120]=0x65;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='8';
TRAMEOUT[3]='c';
TRAMEOUT[4]=0x75;
TRAMEOUT[5]=0x69;
TRAMEOUT[6]=0x6C;
TRAMEOUT[7]=0x20;
TRAMEOUT[8]=0x20;
TRAMEOUT[9]=0x20;
TRAMEOUT[10]=0x20;
TRAMEOUT[11]=0x20;
TRAMEOUT[12]=0x20;
TRAMEOUT[13]=0x20;
TRAMEOUT[14]=0x3A;
TRAMEOUT[15]=0x0A;
TRAMEOUT[16]=0x08;
TRAMEOUT[17]=0x1B;
TRAMEOUT[18]=0x5B;
TRAMEOUT[19]=0x31;
TRAMEOUT[20]=0x36;
TRAMEOUT[21]=0x3B;
TRAMEOUT[22]=0x31;
TRAMEOUT[23]=0x35;
TRAMEOUT[24]=0x48;
TRAMEOUT[25]=0x1B;
TRAMEOUT[26]=0x5B;
TRAMEOUT[27]=0x30;
TRAMEOUT[28]=0x4B;
TRAMEOUT[29]=0x44;
TRAMEOUT[30]=0x69;
TRAMEOUT[31]=0x73;
TRAMEOUT[32]=0x74;
TRAMEOUT[33]=0x61;
TRAMEOUT[34]=0x6E;
TRAMEOUT[35]=0x63;
TRAMEOUT[36]=0x65;
TRAMEOUT[37]=0x20;
TRAMEOUT[38]=0x20;
TRAMEOUT[39]=0x20;
TRAMEOUT[40]=0x20;
TRAMEOUT[41]=0x3A;
TRAMEOUT[42]=0x1B;
TRAMEOUT[43]=0x5B;
TRAMEOUT[44]=0x31;
TRAMEOUT[45]=0x36;
TRAMEOUT[46]=0x3B;
TRAMEOUT[47]=0x35;
TRAMEOUT[48]=0x32;
TRAMEOUT[49]=0x48;
TRAMEOUT[50]=0x20;
TRAMEOUT[51]=0x49;
TRAMEOUT[52]=0x6E;
TRAMEOUT[53]=0x74;
TRAMEOUT[54]=0x65;
TRAMEOUT[55]=0x72;
TRAMEOUT[56]=0x76;
TRAMEOUT[57]=0x65;
TRAMEOUT[58]=0x6E;
TRAMEOUT[59]=0x74;
TRAMEOUT[60]=0x69;
TRAMEOUT[61]=0x6F;
TRAMEOUT[62]=0x6E;
TRAMEOUT[63]=0x20;
TRAMEOUT[64]=0x3A;
TRAMEOUT[65]=0x20;
TRAMEOUT[66]=0x20;
TRAMEOUT[67]=0x1B;
TRAMEOUT[68]=0x5B;
TRAMEOUT[69]=0x31;
TRAMEOUT[70]=0x35;
TRAMEOUT[71]=0x3B;
TRAMEOUT[72]=0x33;
TRAMEOUT[73]=0x35;
TRAMEOUT[74]=0x48;
TRAMEOUT[75]=0x6D;
TRAMEOUT[76]=0x42;
TRAMEOUT[77]=0x61;
TRAMEOUT[78]=0x72;
TRAMEOUT[79]=0x1B;
TRAMEOUT[80]=0x5B;
TRAMEOUT[81]=0x31;
TRAMEOUT[82]=0x36;
TRAMEOUT[83]=0x3B;
TRAMEOUT[84]=0x33;
TRAMEOUT[85]=0x35;
TRAMEOUT[86]=0x48;
TRAMEOUT[87]=0x4D;
TRAMEOUT[88]=0x65;
TRAMEOUT[89]=0x74;
TRAMEOUT[90]=0x72;
TRAMEOUT[91]=0x65;
TRAMEOUT[92]=0x73;
TRAMEOUT[93]=0x1B;
TRAMEOUT[94]=0x5B;
TRAMEOUT[95]=0x31;
TRAMEOUT[96]=0x36;
TRAMEOUT[97]=0x3B;
TRAMEOUT[98]=0x36;
TRAMEOUT[99]=0x38;
TRAMEOUT[100]=0x48;
TRAMEOUT[101]=0x4E;
TRAMEOUT[102]=0x4F;
TRAMEOUT[103]=0x4E;
TRAMEOUT[104]=0x1B;
TRAMEOUT[105]=0x5B;
TRAMEOUT[106]=0x37;
TRAMEOUT[107]=0x6D;
TRAMEOUT[108]=0x1B;
TRAMEOUT[109]=0x5B;
TRAMEOUT[110]=0x30;
TRAMEOUT[111]=0x6D;
TRAMEOUT[112]=0x1B;
TRAMEOUT[113]=0x5B;
TRAMEOUT[114]=0x36;
TRAMEOUT[115]=0x3B;
TRAMEOUT[116]=0x32;
TRAMEOUT[117]=0x39;
TRAMEOUT[118]=0x48;
TRAMEOUT[119]=0x1B;
TRAMEOUT[120]=0x5B;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 115); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (115);
TRAMEOUT[0]='#'; 
TRAMEOUT[1]='0';
TRAMEOUT[2]='8';
TRAMEOUT[3]='d';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x32;
TRAMEOUT[7]=0x33;
TRAMEOUT[8]=0x3B;
TRAMEOUT[9]=0x31;
TRAMEOUT[10]=0x48;
TRAMEOUT[11]=0x1B;
TRAMEOUT[12]=0x5B;
TRAMEOUT[13]=0x37;
TRAMEOUT[14]=0x6D;
TRAMEOUT[15]=0x41;
TRAMEOUT[16]=0x50;
TRAMEOUT[17]=0x50;
TRAMEOUT[18]=0x55;
TRAMEOUT[19]=0x59;
TRAMEOUT[20]=0x45;
TRAMEOUT[21]=0x52;
TRAMEOUT[22]=0x1B;
TRAMEOUT[23]=0x5B;
TRAMEOUT[24]=0x30;
TRAMEOUT[25]=0x6D;
TRAMEOUT[26]=0x20;
TRAMEOUT[27]=0x20;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]=0x4F;
TRAMEOUT[30]=0x2F;
TRAMEOUT[31]=0x4E;
TRAMEOUT[32]=0x20;
TRAMEOUT[33]=0x70;
TRAMEOUT[34]=0x6F;
TRAMEOUT[35]=0x75;
TRAMEOUT[36]=0x72;
TRAMEOUT[37]=0x20;
TRAMEOUT[38]=0x61;
TRAMEOUT[39]=0x66;
TRAMEOUT[40]=0x66;
TRAMEOUT[41]=0x69;
TRAMEOUT[42]=0x63;
TRAMEOUT[43]=0x68;
TRAMEOUT[44]=0x65;
TRAMEOUT[45]=0x72;
TRAMEOUT[46]=0x20;
TRAMEOUT[47]=0x6F;
TRAMEOUT[48]=0x75;
TRAMEOUT[49]=0x69;
TRAMEOUT[50]=0x2F;
TRAMEOUT[51]=0x6E;
TRAMEOUT[52]=0x6F;
TRAMEOUT[53]=0x6E;
TRAMEOUT[54]=0x1B;
TRAMEOUT[55]=0x5B;
TRAMEOUT[56]=0x32;
TRAMEOUT[57]=0x34;
TRAMEOUT[58]=0x3B;
TRAMEOUT[59]=0x31;
TRAMEOUT[60]=0x48;
TRAMEOUT[61]=0x1B;
TRAMEOUT[62]=0x5B;
TRAMEOUT[63]=0x37;
TRAMEOUT[64]=0x6D;
TRAMEOUT[65]=0x41;
TRAMEOUT[66]=0x50;
TRAMEOUT[67]=0x50;
TRAMEOUT[68]=0x55;
TRAMEOUT[69]=0x59;
TRAMEOUT[70]=0x45;
TRAMEOUT[71]=0x52;
TRAMEOUT[72]=0x1B;
TRAMEOUT[73]=0x5B;
TRAMEOUT[74]=0x30;
TRAMEOUT[75]=0x6D;
TRAMEOUT[76]=0x20;
TRAMEOUT[77]=0x20;
TRAMEOUT[78]=0x20;
TRAMEOUT[79]=0x41;
TRAMEOUT[80]=0x20;
TRAMEOUT[81]=0x70;
TRAMEOUT[82]=0x6F;
TRAMEOUT[83]=0x75;
TRAMEOUT[84]=0x72;
TRAMEOUT[85]=0x20;
TRAMEOUT[86]=0x75;
TRAMEOUT[87]=0x6E;
TRAMEOUT[88]=0x20;
TRAMEOUT[89]=0x61;
TRAMEOUT[90]=0x64;
TRAMEOUT[91]=0x72;
TRAMEOUT[92]=0x65;
TRAMEOUT[93]=0x73;
TRAMEOUT[94]=0x73;
TRAMEOUT[95]=0x61;
TRAMEOUT[96]=0x62;
TRAMEOUT[97]=0x6C;
TRAMEOUT[98]=0x65;
TRAMEOUT[99]=0x20;
TRAMEOUT[100]=0x65;
TRAMEOUT[101]=0x74;
TRAMEOUT[102]=0x20;
TRAMEOUT[103]=0x52;
TRAMEOUT[104]=0x20;
TRAMEOUT[105]=0x70;
TRAMEOUT[106]=0x6F;
TRAMEOUT[107]=0x75;
TRAMEOUT[108]=0x72;
TRAMEOUT[109]=0x20;
TRAMEOUT[110]=0x75;
TRAMEOUT[111]=0x6E;
TRAMEOUT[112]=0x20;
TRAMEOUT[113]=0x72;
TRAMEOUT[114]=0x65;
TRAMEOUT[115]=0x73;
TRAMEOUT[116]=0x69;
TRAMEOUT[117]=0x73;
TRAMEOUT[118]=0x74;
TRAMEOUT[119]=0x69;
TRAMEOUT[120]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 116); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (116);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='8';
TRAMEOUT[3]='e';
TRAMEOUT[4]=0x66;
TRAMEOUT[5]=0x1B;
TRAMEOUT[6]=0x5B;
TRAMEOUT[7]=0x30;
TRAMEOUT[8]=0x4B;
//TRAMEOUT[9]=0x1B;
//TRAMEOUT[10]=0x5B;
//TRAMEOUT[11]=0x39;
//TRAMEOUT[12]=0x3B;
//TRAMEOUT[13]=0x34;
//TRAMEOUT[14]=0x32;
//TRAMEOUT[15]=0x48;
//TRAMEOUT[16]=0x1B;
//TRAMEOUT[17]=0x5B;
//TRAMEOUT[18]=0x36;
//TRAMEOUT[19]=0x3B;
//TRAMEOUT[20]=0x33;
//TRAMEOUT[21]=0x32;
//TRAMEOUT[22]=0x48;
//TRAMEOUT[23]=0x1B;
//TRAMEOUT[24]='*';
j=0;
for (iii=4;iii<9;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 5); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (5);

TRAMEOUT[0]='#'; //21092020
TRAMEOUT[1]='0';
TRAMEOUT[2]='8';
TRAMEOUT[3]='f';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]='[';
TRAMEOUT[6]='2';
TRAMEOUT[7]='0';
TRAMEOUT[8]=';';
TRAMEOUT[9]='1';
TRAMEOUT[10]='H';
TRAMEOUT[11]=0x1B;
TRAMEOUT[12]='[';
TRAMEOUT[13]='7';
TRAMEOUT[14]='m';
TRAMEOUT[15]='S';
TRAMEOUT[16]='A';
TRAMEOUT[17]='I';
TRAMEOUT[18]='S';
TRAMEOUT[19]='I';
TRAMEOUT[20]='R';
TRAMEOUT[21]=0x1B;
TRAMEOUT[22]='[';
TRAMEOUT[23]='0';
TRAMEOUT[24]='m';
TRAMEOUT[25]=' ';
TRAMEOUT[26]=' ';
TRAMEOUT[27]=' ';
TRAMEOUT[28]=' ';
TRAMEOUT[29]='l';
TRAMEOUT[30]='e';
TRAMEOUT[31]=' ';
TRAMEOUT[32]='c';
TRAMEOUT[33]='o';
TRAMEOUT[34]='d';
TRAMEOUT[35]='a';
TRAMEOUT[36]='g';
TRAMEOUT[37]='e';
TRAMEOUT[38]=' ';
TRAMEOUT[39]='a';
TRAMEOUT[40]=' ';
TRAMEOUT[41]='e';
TRAMEOUT[42]='d';
TRAMEOUT[43]='i';
TRAMEOUT[44]='t';
TRAMEOUT[45]='e';
TRAMEOUT[46]='r';
TRAMEOUT[47]=' ';
TRAMEOUT[48]=' ';
TRAMEOUT[49]=' ';
TRAMEOUT[50]=' ';
TRAMEOUT[51]=' ';
TRAMEOUT[52]=' ';
TRAMEOUT[53]=' ';
TRAMEOUT[54]=' ';
TRAMEOUT[55]=0x1B;
TRAMEOUT[56]='[';
TRAMEOUT[57]='2';
TRAMEOUT[58]='1';
TRAMEOUT[59]=';';
TRAMEOUT[60]='1';
TRAMEOUT[61]='H';
TRAMEOUT[62]=0x1B;
TRAMEOUT[63]='[';
TRAMEOUT[64]='7';
TRAMEOUT[65]='m';
TRAMEOUT[66]='A';
TRAMEOUT[67]='P';
TRAMEOUT[68]='P';
TRAMEOUT[69]='U';
TRAMEOUT[70]='Y';
TRAMEOUT[71]='E';
TRAMEOUT[72]='R';
TRAMEOUT[73]=0x1B;
TRAMEOUT[74]='[';
TRAMEOUT[75]='0';
TRAMEOUT[76]='m';
TRAMEOUT[77]=' ';
TRAMEOUT[78]=' ';
TRAMEOUT[79]=' ';
TRAMEOUT[80]='s';
TRAMEOUT[81]='u';
TRAMEOUT[82]='r';
TRAMEOUT[83]=' ';
TRAMEOUT[84]='E';
TRAMEOUT[85]='N';
TRAMEOUT[86]='T';
TRAMEOUT[87]='E';
TRAMEOUT[88]='R';
TRAMEOUT[89]=' ';
TRAMEOUT[90]='a';
TRAMEOUT[91]='p';
TRAMEOUT[92]='r';
TRAMEOUT[93]='e';
TRAMEOUT[94]='s';
TRAMEOUT[95]=' ';
TRAMEOUT[96]='l';
TRAMEOUT[97]='e';
TRAMEOUT[98]=' ';
TRAMEOUT[99]='p';
TRAMEOUT[100]='a';
TRAMEOUT[101]='r';
TRAMEOUT[102]='a';
TRAMEOUT[103]='m';
TRAMEOUT[104]='e';
TRAMEOUT[105]='t';
TRAMEOUT[106]='r';
TRAMEOUT[107]='a';
TRAMEOUT[108]='g';
TRAMEOUT[109]='e';
TRAMEOUT[110]=' ';
TRAMEOUT[111]='p';
TRAMEOUT[112]='o';
TRAMEOUT[113]='u';
TRAMEOUT[114]='r';
TRAMEOUT[115]=' ';
TRAMEOUT[116]='c';
TRAMEOUT[117]='r';
TRAMEOUT[118]='e';
TRAMEOUT[119]='e';
TRAMEOUT[120]='r';
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}

TRAMEENVOITCPDONNEES (117);

TRAMEOUT[0]='#'; //21092020
TRAMEOUT[1]='0';
TRAMEOUT[2]='8';
TRAMEOUT[3]='g';
TRAMEOUT[4]=' ';
TRAMEOUT[5]='l';
TRAMEOUT[6]='e';
TRAMEOUT[7]=' ';
TRAMEOUT[8]='c';
TRAMEOUT[9]='a';
TRAMEOUT[10]='p';
TRAMEOUT[11]='t';
TRAMEOUT[12]='e';
TRAMEOUT[13]='u';
TRAMEOUT[14]='r';
TRAMEOUT[15]=0x1B;
TRAMEOUT[16]='[';
TRAMEOUT[17]='2';
TRAMEOUT[18]='2';
TRAMEOUT[19]=';';
TRAMEOUT[20]='1';
TRAMEOUT[21]='H';
TRAMEOUT[22]=0x1B;
TRAMEOUT[23]='[';
TRAMEOUT[24]='7';
TRAMEOUT[25]='m';
TRAMEOUT[26]='A';
TRAMEOUT[27]='P';
TRAMEOUT[28]='P';
TRAMEOUT[29]='U';
TRAMEOUT[30]='Y';
TRAMEOUT[31]='E';
TRAMEOUT[32]='R';
TRAMEOUT[33]=0x1B;
TRAMEOUT[34]='[';
TRAMEOUT[35]='0';
TRAMEOUT[36]='m';
TRAMEOUT[37]=' ';
TRAMEOUT[38]=' ';
TRAMEOUT[39]=' ';
TRAMEOUT[40]='s';
TRAMEOUT[41]='u';
TRAMEOUT[42]='r';
TRAMEOUT[43]=' ';
TRAMEOUT[44]='"';
TRAMEOUT[45]='-';
TRAMEOUT[46]='"';
TRAMEOUT[47]=' ';
TRAMEOUT[48]='a';
TRAMEOUT[49]='u';
TRAMEOUT[50]=' ';
TRAMEOUT[51]='n';
TRAMEOUT[52]='i';
TRAMEOUT[53]='v';
TRAMEOUT[54]='e';
TRAMEOUT[55]='a';
TRAMEOUT[56]='u';
TRAMEOUT[57]=' ';
TRAMEOUT[58]='d';
TRAMEOUT[59]='u';
TRAMEOUT[60]=' ';
TRAMEOUT[61]='c';
TRAMEOUT[62]='o';
TRAMEOUT[63]='d';
TRAMEOUT[64]='a';
TRAMEOUT[65]='g';
TRAMEOUT[66]='e';
TRAMEOUT[67]=' ';
TRAMEOUT[68]='p';
TRAMEOUT[69]='o';
TRAMEOUT[70]='u';
TRAMEOUT[71]='r';
TRAMEOUT[72]=' ';
TRAMEOUT[73]='e';
TRAMEOUT[74]='f';
TRAMEOUT[75]='f';
TRAMEOUT[76]='a';
TRAMEOUT[77]='c';
TRAMEOUT[78]='e';
TRAMEOUT[79]='r';
TRAMEOUT[80]=' ';
TRAMEOUT[81]='l';
TRAMEOUT[82]='e';
TRAMEOUT[83]=' ';
TRAMEOUT[84]='c';
TRAMEOUT[85]='a';
TRAMEOUT[86]='p';
TRAMEOUT[87]='t';
TRAMEOUT[88]='e';
TRAMEOUT[89]='u';
TRAMEOUT[90]='r';
TRAMEOUT[91]='*';
j=0;
for (iii=4;iii<92;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}

TRAMEENVOITCPDONNEES (87);
}
else if (num == '8')
{
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='9';
TRAMEOUT[3]='a';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x32;
TRAMEOUT[7]=0x4A;
TRAMEOUT[8]=0x1B;
TRAMEOUT[9]=0x5B;
TRAMEOUT[10]=0x31;
TRAMEOUT[11]=0x3B;
TRAMEOUT[12]=0x36;
TRAMEOUT[13]=0x34;
TRAMEOUT[14]=0x48;
TRAMEOUT[15]=0x1B;
TRAMEOUT[16]=0x5B;
TRAMEOUT[17]=0x31;
TRAMEOUT[18]=0x3B;
TRAMEOUT[19]=0x33;
TRAMEOUT[20]=0x38;
TRAMEOUT[21]=0x48;
TRAMEOUT[22]=0x53;
TRAMEOUT[23]=0x69;
TRAMEOUT[24]=0x74;
TRAMEOUT[25]=0x65;
TRAMEOUT[26]=0x20;
TRAMEOUT[27]=0x3A;
TRAMEOUT[28]=0x20;//19102020
TRAMEOUT[29]=0x20;
TRAMEOUT[30]=0x20;
TRAMEOUT[31]=0x20;
TRAMEOUT[32]=0x20;
TRAMEOUT[33]=0x1B;
TRAMEOUT[34]=0x5B;
TRAMEOUT[35]=0x32;
TRAMEOUT[36]=0x3B;
TRAMEOUT[37]=0x33;
TRAMEOUT[38]=0x30;
TRAMEOUT[39]=0x48;
TRAMEOUT[40]=0x20;
TRAMEOUT[41]=0x20;
TRAMEOUT[42]=0x20;
TRAMEOUT[43]=0x20;
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x20;
TRAMEOUT[46]=0x20;
TRAMEOUT[47]=0x20;
TRAMEOUT[48]=0x20;
TRAMEOUT[49]=0x20;
TRAMEOUT[50]=0x20;
TRAMEOUT[51]=0x20;
TRAMEOUT[52]=0x20;
TRAMEOUT[53]=0x20;
TRAMEOUT[54]=0x20;
TRAMEOUT[55]=0x20;
TRAMEOUT[56]=0x20;
TRAMEOUT[57]=0x20;
TRAMEOUT[58]=0x20;
TRAMEOUT[59]=0x1B;
TRAMEOUT[60]=0x5B;
TRAMEOUT[61]=0x30;
TRAMEOUT[62]=0x6D;
TRAMEOUT[63]=0x1B;
TRAMEOUT[64]=0x5B;
TRAMEOUT[65]=0x31;
TRAMEOUT[66]=0x3B;
TRAMEOUT[67]=0x35;
TRAMEOUT[68]=0x39;
TRAMEOUT[69]=0x48;//DATE
TRAMEOUT[70]=0x20; //J
TRAMEOUT[71]=0x20; //J
TRAMEOUT[72]=0x20; // /2F
TRAMEOUT[73]=0x20; //M
TRAMEOUT[74]=0x20; //M
TRAMEOUT[75]=0x20; // /
TRAMEOUT[76]=0x20; //A
TRAMEOUT[77]=0x20;//A
TRAMEOUT[78]=0x20;//A
TRAMEOUT[79]=0x20;//A
TRAMEOUT[80]=0x20;
TRAMEOUT[81]=0x20;
TRAMEOUT[82]=0x20; //H
TRAMEOUT[83]=0x20; //H
TRAMEOUT[84]=0x20; // :3A
TRAMEOUT[85]=0x20; //m
TRAMEOUT[86]=0x20; //m
TRAMEOUT[87]=0x1B;
TRAMEOUT[88]=0x5B;
TRAMEOUT[89]=0x34;
TRAMEOUT[90]=0x3B;
TRAMEOUT[91]=0x32;
TRAMEOUT[92]=0x38;
TRAMEOUT[93]=0x48;
TRAMEOUT[94]=0x1B;
TRAMEOUT[95]=0x5B;
TRAMEOUT[96]=0x37;
TRAMEOUT[97]=0x6D;
TRAMEOUT[98]=0x50;
TRAMEOUT[99]=0x41;
TRAMEOUT[100]=0x52;
TRAMEOUT[101]=0x41;
TRAMEOUT[102]=0x4D;
TRAMEOUT[103]=0x45;
TRAMEOUT[104]=0x54;
TRAMEOUT[105]=0x52;
TRAMEOUT[106]=0x45;
TRAMEOUT[107]=0x53;
TRAMEOUT[108]=0x20;
TRAMEOUT[109]=0x44;
TRAMEOUT[110]=0x27;
TRAMEOUT[111]=0x45;
TRAMEOUT[112]=0x58;
TRAMEOUT[113]=0x50;
TRAMEOUT[114]=0x4C;
TRAMEOUT[115]=0x4F;
TRAMEOUT[116]=0x49;
TRAMEOUT[117]=0x54;
TRAMEOUT[118]=0x41;
TRAMEOUT[119]=0x54;
TRAMEOUT[120]=0x49;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='9';
TRAMEOUT[3]='b';
TRAMEOUT[4]=0x4F;
TRAMEOUT[5]=0x4E;
TRAMEOUT[6]=0x1B;
TRAMEOUT[7]=0x5B;
TRAMEOUT[8]=0x30;
TRAMEOUT[9]=0x6D;
TRAMEOUT[10]=0x1B;
TRAMEOUT[11]=0x5B;
TRAMEOUT[12]=0x34;
TRAMEOUT[13]=0x3B;
TRAMEOUT[14]=0x32;
TRAMEOUT[15]=0x36;
TRAMEOUT[16]=0x48;
TRAMEOUT[17]=0x1B;
TRAMEOUT[18]=0x5B;
TRAMEOUT[19]=0x36;
TRAMEOUT[20]=0x3B;
TRAMEOUT[21]=0x31;
TRAMEOUT[22]=0x31;
TRAMEOUT[23]=0x48;
TRAMEOUT[24]=0x49;
TRAMEOUT[25]=0x64;
TRAMEOUT[26]=0x65;
TRAMEOUT[27]=0x6E;
TRAMEOUT[28]=0x74;
TRAMEOUT[29]=0x69;
TRAMEOUT[30]=0x66;
TRAMEOUT[31]=0x69;
TRAMEOUT[32]=0x63;
TRAMEOUT[33]=0x61;
TRAMEOUT[34]=0x74;
TRAMEOUT[35]=0x69;
TRAMEOUT[36]=0x6F;
TRAMEOUT[37]=0x6E;
TRAMEOUT[38]=0x20;
TRAMEOUT[39]=0x52;
TRAMEOUT[40]=0x61;
TRAMEOUT[41]=0x63;
TRAMEOUT[42]=0x6B;
TRAMEOUT[43]=0x20;
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x20;
TRAMEOUT[46]=0x20;
TRAMEOUT[47]=0x20;
TRAMEOUT[48]=0x20;
TRAMEOUT[49]=0x3D;
TRAMEOUT[50]=0x20;
TRAMEOUT[51]=0x20;
TRAMEOUT[52]=0x1B;
TRAMEOUT[53]=0x5B;
TRAMEOUT[54]=0x31;
TRAMEOUT[55]=0x30;
TRAMEOUT[56]=0x3B;
TRAMEOUT[57]=0x31;
TRAMEOUT[58]=0x31;
TRAMEOUT[59]=0x48;
TRAMEOUT[60]=0x54;
TRAMEOUT[61]=0x72;
TRAMEOUT[62]=0x61;
TRAMEOUT[63]=0x6E;
TRAMEOUT[64]=0x73;
TRAMEOUT[65]=0x6D;
TRAMEOUT[66]=0x69;
TRAMEOUT[67]=0x73;
TRAMEOUT[68]=0x73;
TRAMEOUT[69]=0x69;
TRAMEOUT[70]=0x6F;
TRAMEOUT[71]=0x6E;
TRAMEOUT[72]=0x20;
TRAMEOUT[73]=0x64;
TRAMEOUT[74]=0x65;
TRAMEOUT[75]=0x73;
TRAMEOUT[76]=0x20;
TRAMEOUT[77]=0x61;
TRAMEOUT[78]=0x6C;
TRAMEOUT[79]=0x61;
TRAMEOUT[80]=0x72;
TRAMEOUT[81]=0x6D;
TRAMEOUT[82]=0x65;
TRAMEOUT[83]=0x73;
TRAMEOUT[84]=0x20;
TRAMEOUT[85]=0x3D;
TRAMEOUT[86]=0x20;
TRAMEOUT[87]=0x20;
TRAMEOUT[88]=0x1B;
TRAMEOUT[89]=0x5B;
TRAMEOUT[90]=0x31;
TRAMEOUT[91]=0x32;
TRAMEOUT[92]=0x3B;
TRAMEOUT[93]=0x32;
TRAMEOUT[94]=0x36;
TRAMEOUT[95]=0x48;
TRAMEOUT[96]=0x1B;
TRAMEOUT[97]=0x5B;
TRAMEOUT[98]=0x31;
TRAMEOUT[99]=0x32;
TRAMEOUT[100]=0x3B;
TRAMEOUT[101]=0x31;
TRAMEOUT[102]=0x31;
TRAMEOUT[103]=0x48;
TRAMEOUT[104]=0x54;
TRAMEOUT[105]=0x72;
TRAMEOUT[106]=0x61;
TRAMEOUT[107]=0x6E;
TRAMEOUT[108]=0x73;
TRAMEOUT[109]=0x6D;
TRAMEOUT[110]=0x69;
TRAMEOUT[111]=0x73;
TRAMEOUT[112]=0x73;
TRAMEOUT[113]=0x69;
TRAMEOUT[114]=0x6F;
TRAMEOUT[115]=0x6E;
TRAMEOUT[116]=0x20;
TRAMEOUT[117]=0x64;
TRAMEOUT[118]=0x65;
TRAMEOUT[119]=0x73;
TRAMEOUT[120]=0x20;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='9';
TRAMEOUT[3]='c';
TRAMEOUT[4]=0x73;
TRAMEOUT[5]=0x6F;
TRAMEOUT[6]=0x72;
TRAMEOUT[7]=0x74;
TRAMEOUT[8]=0x69;
TRAMEOUT[9]=0x65;
TRAMEOUT[10]=0x73;
TRAMEOUT[11]=0x20;
TRAMEOUT[12]=0x3D;
TRAMEOUT[13]=0x20;
TRAMEOUT[14]=0x20;
TRAMEOUT[15]=0x1B;
TRAMEOUT[16]=0x5B;
TRAMEOUT[17]=0x31;
TRAMEOUT[18]=0x34;
TRAMEOUT[19]=0x3B;
TRAMEOUT[20]=0x31;
TRAMEOUT[21]=0x32;
TRAMEOUT[22]=0x48;
TRAMEOUT[23]=' ';
TRAMEOUT[24]=' ';
TRAMEOUT[25]=' ';
TRAMEOUT[26]=0x20;
TRAMEOUT[27]=0x20; // IP
TRAMEOUT[28]='I';
TRAMEOUT[29]='P';
TRAMEOUT[30]=0x20;
TRAMEOUT[31]=0x3D;
TRAMEOUT[32]=0x20;
TRAMEOUT[33]=0x20;
TRAMEOUT[34]=0x1B;
TRAMEOUT[35]=0x5B;
TRAMEOUT[36]=0x31;
TRAMEOUT[37]=0x34;
TRAMEOUT[38]=0x3B;
TRAMEOUT[39]=0x34;
TRAMEOUT[40]=0x36;
TRAMEOUT[41]=0x48;
TRAMEOUT[42]=' '; //N
TRAMEOUT[43]=' '; //r
TRAMEOUT[44]=' '; //.
TRAMEOUT[45]=0x20;
TRAMEOUT[46]=' ';
TRAMEOUT[47]=' ';
TRAMEOUT[48]=' ';
TRAMEOUT[49]=0x20;
TRAMEOUT[50]=0x20;
TRAMEOUT[51]=0x20;
TRAMEOUT[52]=0x20;
TRAMEOUT[53]=0x20;
TRAMEOUT[54]=0x20; //COG version IP
TRAMEOUT[55]=0x20;
TRAMEOUT[56]=0x20;
TRAMEOUT[57]=0x20;
TRAMEOUT[58]=' ';//=
TRAMEOUT[59]=0x20;
TRAMEOUT[60]=0x20;
TRAMEOUT[61]=0x1B;
TRAMEOUT[62]=0x5B;
TRAMEOUT[63]=0x31;
TRAMEOUT[64]=0x38;
TRAMEOUT[65]=0x3B;
TRAMEOUT[66]=0x32;
TRAMEOUT[67]=0x31;
TRAMEOUT[68]=0x48;
TRAMEOUT[69]=' '; //heure
TRAMEOUT[70]=' ';
TRAMEOUT[71]=' ';
TRAMEOUT[72]=' ';
TRAMEOUT[73]=0x20;
TRAMEOUT[74]=' ';
TRAMEOUT[75]=0x20;
TRAMEOUT[76]=' ';
TRAMEOUT[77]=' ';
TRAMEOUT[78]=0x68;
TRAMEOUT[79]=0x65;
TRAMEOUT[80]=0x75;
TRAMEOUT[81]=0x72;
TRAMEOUT[82]=0x65;
TRAMEOUT[83]=0x20;
TRAMEOUT[84]=0x3D;
TRAMEOUT[85]=0x20;
TRAMEOUT[86]=0x20;
TRAMEOUT[87]=0x20;
TRAMEOUT[88]=0x20;
TRAMEOUT[89]=0x20;
TRAMEOUT[90]=0x3A;
TRAMEOUT[91]=0x20;
TRAMEOUT[92]=0x20;
TRAMEOUT[93]=0x1B;
TRAMEOUT[94]=0x5B;
TRAMEOUT[95]=0x31;
TRAMEOUT[96]=0x39;
TRAMEOUT[97]=0x3B;
TRAMEOUT[98]=0x32;
TRAMEOUT[99]=0x31;
TRAMEOUT[100]=0x48;
TRAMEOUT[101]=' '; //DATE
TRAMEOUT[102]=' ';
TRAMEOUT[103]=' ';
TRAMEOUT[104]=' ';
TRAMEOUT[105]=0x20;
TRAMEOUT[106]=' ';
TRAMEOUT[107]=0x20;
TRAMEOUT[108]=' ';
TRAMEOUT[109]=' ';
TRAMEOUT[110]=0x20;
TRAMEOUT[111]=0x64;
TRAMEOUT[112]=0x61;
TRAMEOUT[113]=0x74;
TRAMEOUT[114]=0x65;
TRAMEOUT[115]=0x20;
TRAMEOUT[116]=0x3D;
TRAMEOUT[117]=0x20;
TRAMEOUT[118]=0x20;
TRAMEOUT[119]=0x20;
TRAMEOUT[120]=0x20;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='9';
TRAMEOUT[3]='d';
TRAMEOUT[4]=0x20;
TRAMEOUT[5]=0x2D;
TRAMEOUT[6]=0x20;
TRAMEOUT[7]=0x20;
TRAMEOUT[8]=0x20;
TRAMEOUT[9]=0x20;
TRAMEOUT[10]=0x20;
TRAMEOUT[11]=0x2D;
TRAMEOUT[12]=0x20;
TRAMEOUT[13]=0x20;
TRAMEOUT[14]=0x20;
TRAMEOUT[15]=0x20;
TRAMEOUT[16]=0x20;
TRAMEOUT[17]=0x20;
TRAMEOUT[18]=0x1B;
TRAMEOUT[19]=0x5B;
TRAMEOUT[20]=0x32;
TRAMEOUT[21]=0x31;
TRAMEOUT[22]=0x3B;
TRAMEOUT[23]=0x32;
TRAMEOUT[24]=0x32;
TRAMEOUT[25]=0x48;
TRAMEOUT[26]=' '; //ACCES
TRAMEOUT[27]=' ';
TRAMEOUT[28]=' ';
TRAMEOUT[29]=' ';
TRAMEOUT[30]=' ';
TRAMEOUT[31]=' ';
TRAMEOUT[32]=' '; 
TRAMEOUT[33]=' ';
TRAMEOUT[34]=' ';
TRAMEOUT[35]=' ';
TRAMEOUT[36]=' ';
TRAMEOUT[37]=' ';
TRAMEOUT[38]=' ';
TRAMEOUT[39]=' ';
TRAMEOUT[40]=' ';
TRAMEOUT[41]=' ';
TRAMEOUT[42]=' ';
TRAMEOUT[43]=0x20;
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x20;
TRAMEOUT[46]=0x20;
TRAMEOUT[47]=0x20;
TRAMEOUT[48]=' ';
TRAMEOUT[49]=0x20;
TRAMEOUT[50]=0x20;
TRAMEOUT[51]=0x1B;
TRAMEOUT[52]=0x5B;
TRAMEOUT[53]=0x32;
TRAMEOUT[54]=0x32;
TRAMEOUT[55]=0x3B;
TRAMEOUT[56]=0x32;
TRAMEOUT[57]=0x32;
TRAMEOUT[58]=0x48;
TRAMEOUT[59]=0x41; //ACCES
TRAMEOUT[60]=0x63;
TRAMEOUT[61]=0x63;
TRAMEOUT[62]=0x65;
TRAMEOUT[63]=0x73;
TRAMEOUT[64]=0x20;
TRAMEOUT[65]=0x63;
TRAMEOUT[66]=0x6F;
TRAMEOUT[67]=0x6E;
TRAMEOUT[68]=0x66;
TRAMEOUT[69]=0x69;
TRAMEOUT[70]=0x67;
TRAMEOUT[71]=0x75;
TRAMEOUT[72]=0x72;
TRAMEOUT[73]=0x61;
TRAMEOUT[74]=0x74;
TRAMEOUT[75]=0x65;
TRAMEOUT[76]=0x75;
TRAMEOUT[77]=0x72;
TRAMEOUT[78]=0x20;
TRAMEOUT[79]=0x20;
TRAMEOUT[80]=0x20;
TRAMEOUT[81]=0x3D;
TRAMEOUT[82]=0x20;
TRAMEOUT[83]=0x20;
TRAMEOUT[84]=0x1B;
TRAMEOUT[85]=0x5B;
TRAMEOUT[86]=0x31;
TRAMEOUT[87]=0x36;
TRAMEOUT[88]=0x3B;
TRAMEOUT[89]=0x31;
TRAMEOUT[90]=0x31;
TRAMEOUT[91]=0x48;
TRAMEOUT[92]=0x20;
TRAMEOUT[93]=0x70; 
TRAMEOUT[94]=0x61;
TRAMEOUT[95]=0x73;
TRAMEOUT[96]=0x73;
TRAMEOUT[97]=0x65;
TRAMEOUT[98]=0x72;
TRAMEOUT[99]=0x65;
TRAMEOUT[100]=0x6C;
TRAMEOUT[101]=0x6C;
TRAMEOUT[102]=0x65;
TRAMEOUT[103]=0x20;
TRAMEOUT[104]=0x3D;
TRAMEOUT[105]=0x20;
TRAMEOUT[106]=0x20;
TRAMEOUT[107]=0x1B;
TRAMEOUT[108]=0x5B;
TRAMEOUT[109]=0x31;
TRAMEOUT[110]=0x36;
TRAMEOUT[111]=0x3B;
TRAMEOUT[112]=0x34;
TRAMEOUT[113]=0x36;
TRAMEOUT[114]=0x48;
TRAMEOUT[115]=' '; //DNS
TRAMEOUT[116]=' ';
TRAMEOUT[117]=' ';
TRAMEOUT[118]=' ';
TRAMEOUT[119]=' ';
TRAMEOUT[120]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 116); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (116);
}
else if (num == 'M')
{
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////// page menu //////////////////////////////////////////////////////////////////////////////////
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='0';
TRAMEOUT[3]='a';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x32;
TRAMEOUT[7]=0x4A;
TRAMEOUT[8]=0x20;//19102020
TRAMEOUT[9]=0x20;
TRAMEOUT[10]=0x20;
TRAMEOUT[11]=0x20;
TRAMEOUT[12]=0x20;
TRAMEOUT[13]=0x20;
TRAMEOUT[14]=0x20;
TRAMEOUT[15]=0x20;
TRAMEOUT[16]=0x20;
TRAMEOUT[17]=0x20;
TRAMEOUT[18]=0x20;
TRAMEOUT[19]=0x20;
TRAMEOUT[20]=0x20;
TRAMEOUT[21]=0x20; //NOM DU SITE
TRAMEOUT[22]=0x20;
TRAMEOUT[23]=0x20;
TRAMEOUT[24]=0x20;
TRAMEOUT[25]=0x20;
TRAMEOUT[26]=0x20;
TRAMEOUT[27]=0x20;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]=0x20;
TRAMEOUT[30]=0x20;
TRAMEOUT[31]=0x20;
TRAMEOUT[32]=0x20;
TRAMEOUT[33]=0x20;
TRAMEOUT[34]=0x20;
TRAMEOUT[35]=0x20;
TRAMEOUT[36]=0x20;
TRAMEOUT[37]=0x20;
TRAMEOUT[38]=0x20;
TRAMEOUT[39]=0x20;
TRAMEOUT[40]=0x20;
TRAMEOUT[41]=0x20;
TRAMEOUT[42]=0x20;
TRAMEOUT[43]=0x1B;
TRAMEOUT[44]=0x5B;
TRAMEOUT[45]=0x31;
TRAMEOUT[46]=0x3B;
TRAMEOUT[47]=0x33;
TRAMEOUT[48]=0x38;
TRAMEOUT[49]=0x48;
TRAMEOUT[50]=0x53;
TRAMEOUT[51]=0x69;
TRAMEOUT[52]=0x74;
TRAMEOUT[53]=0x65;
TRAMEOUT[54]=0x20;
TRAMEOUT[55]=0x3A;
TRAMEOUT[56]=0x20;
TRAMEOUT[57]=0x1B;
TRAMEOUT[58]=0x5B;
TRAMEOUT[59]=0x36;
TRAMEOUT[60]=0x3B;
TRAMEOUT[61]=0x31;
TRAMEOUT[62]=0x36;
TRAMEOUT[63]=0x48;
TRAMEOUT[64]='1'; //1
TRAMEOUT[65]=':'; // :
TRAMEOUT[66]=' ';// 
TRAMEOUT[67]='B'; // LISTE EVENEN..
TRAMEOUT[68]='l';
TRAMEOUT[69]='o';
TRAMEOUT[70]='c';
TRAMEOUT[71]=0x20;
TRAMEOUT[72]='n'; //0211202
TRAMEOUT[73]='o';
TRAMEOUT[74]='t';
TRAMEOUT[75]='e';
TRAMEOUT[76]='s';
TRAMEOUT[77]=0x20;
TRAMEOUT[78]=0x20;
TRAMEOUT[79]=0x20;
TRAMEOUT[80]=0x20;
TRAMEOUT[81]=0x20;
TRAMEOUT[82]=0x20;
TRAMEOUT[83]=0x20;
TRAMEOUT[84]=0x20;
TRAMEOUT[85]=0x20;
TRAMEOUT[86]=0x20;
TRAMEOUT[87]=0x20;
TRAMEOUT[88]=0x20;
TRAMEOUT[89]=0x20;
TRAMEOUT[90]=0x20;
TRAMEOUT[91]=0x20;
TRAMEOUT[92]=0x20;
TRAMEOUT[93]=0x20;
TRAMEOUT[94]=0x20;
TRAMEOUT[95]=0x20;
TRAMEOUT[96]=0x20;
TRAMEOUT[97]=0x20;
TRAMEOUT[98]=0x20;
TRAMEOUT[99]=0x20;
TRAMEOUT[100]=0x20;
TRAMEOUT[101]=0x20;
TRAMEOUT[102]=0x20;
TRAMEOUT[103]=0x20;
TRAMEOUT[104]=0x1B;
TRAMEOUT[105]=0x5B;
TRAMEOUT[106]=0x35;
TRAMEOUT[107]=0x3B;
TRAMEOUT[108]=0x32;
TRAMEOUT[109]=0x48;
TRAMEOUT[110]=0x1B;
TRAMEOUT[111]=0x5B;
TRAMEOUT[112]=0x38;
TRAMEOUT[113]=0x3B;
TRAMEOUT[114]=0x31;
TRAMEOUT[115]=0x36;
TRAMEOUT[116]=0x48;
TRAMEOUT[117]=0x32;
TRAMEOUT[118]=0x3A;
TRAMEOUT[119]=0x20;
TRAMEOUT[120]=0x56;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='0';
TRAMEOUT[3]='b';
TRAMEOUT[4]=0x69;
TRAMEOUT[5]=0x73;
TRAMEOUT[6]=0x75;
TRAMEOUT[7]=0x61;
TRAMEOUT[8]=0x6C;
TRAMEOUT[9]=0x69;
TRAMEOUT[10]=0x73;
TRAMEOUT[11]=0x61;
TRAMEOUT[12]=0x74;
TRAMEOUT[13]=0x69;
TRAMEOUT[14]=0x6F;
TRAMEOUT[15]=0x6E;
TRAMEOUT[16]=0x20;
TRAMEOUT[17]=0x64;
TRAMEOUT[18]=0x65;
TRAMEOUT[19]=0x73;
TRAMEOUT[20]=0x20;
TRAMEOUT[21]=0x61;
TRAMEOUT[22]=0x6C;
TRAMEOUT[23]=0x61;
TRAMEOUT[24]=0x72;
TRAMEOUT[25]=0x6D;
TRAMEOUT[26]=0x65;
TRAMEOUT[27]=0x73;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]=0x65;
TRAMEOUT[30]=0x6E;
TRAMEOUT[31]=0x20;
TRAMEOUT[32]=0x63;
TRAMEOUT[33]=0x6F;
TRAMEOUT[34]=0x75;
TRAMEOUT[35]=0x72;
TRAMEOUT[36]=0x73;
TRAMEOUT[37]=0x1B;
TRAMEOUT[38]=0x5B;
TRAMEOUT[39]=0x37;
TRAMEOUT[40]=0x3B;
TRAMEOUT[41]=0x32;
TRAMEOUT[42]=0x48;
TRAMEOUT[43]=0x1B;
TRAMEOUT[44]=0x5B;
TRAMEOUT[45]=0x31;
TRAMEOUT[46]=0x30;
TRAMEOUT[47]=0x3B;
TRAMEOUT[48]=0x31;
TRAMEOUT[49]=0x36;
TRAMEOUT[50]=0x48;
TRAMEOUT[51]=0x33;
TRAMEOUT[52]=0x3A;
TRAMEOUT[53]=0x20;
TRAMEOUT[54]=0x56;
TRAMEOUT[55]=0x69;
TRAMEOUT[56]=0x73;
TRAMEOUT[57]=0x75;
TRAMEOUT[58]=0x61;
TRAMEOUT[59]=0x6C;
TRAMEOUT[60]=0x69;
TRAMEOUT[61]=0x73;
TRAMEOUT[62]=0x61;
TRAMEOUT[63]=0x74;
TRAMEOUT[64]=0x69;
TRAMEOUT[65]=0x6F;
TRAMEOUT[66]=0x6E;
TRAMEOUT[67]=0x20;
TRAMEOUT[68]=0x64;
TRAMEOUT[69]=0x65;
TRAMEOUT[70]=0x73;
TRAMEOUT[71]=0x20;
TRAMEOUT[72]=0x64;
TRAMEOUT[73]=0x6F;
TRAMEOUT[74]=0x6E;
TRAMEOUT[75]=0x6E;
TRAMEOUT[76]=0x65;
TRAMEOUT[77]=0x65;
TRAMEOUT[78]=0x73;
TRAMEOUT[79]=0x1B;
TRAMEOUT[80]=0x5B;
TRAMEOUT[81]=0x31;
TRAMEOUT[82]=0x31;
TRAMEOUT[83]=0x3B;
TRAMEOUT[84]=0x32;
TRAMEOUT[85]=0x48;
TRAMEOUT[86]=0x1B;
TRAMEOUT[87]=0x5B;
TRAMEOUT[88]=0x31;
TRAMEOUT[89]=0x32;
TRAMEOUT[90]=0x3B;
TRAMEOUT[91]=0x31;
TRAMEOUT[92]=0x36;
TRAMEOUT[93]=0x48;
TRAMEOUT[94]=0x35;
TRAMEOUT[95]=0x3A;
TRAMEOUT[96]=0x20;
TRAMEOUT[97]=0x47;
TRAMEOUT[98]=0x72;
TRAMEOUT[99]=0x6F;
TRAMEOUT[100]=0x75;
TRAMEOUT[101]=0x70;
TRAMEOUT[102]=0x65;
TRAMEOUT[103]=0x20;
TRAMEOUT[104]=0x64;
TRAMEOUT[105]=0x65;
TRAMEOUT[106]=0x20;
TRAMEOUT[107]=0x70;
TRAMEOUT[108]=0x72;
TRAMEOUT[109]=0x65;
TRAMEOUT[110]=0x73;
TRAMEOUT[111]=0x73;
TRAMEOUT[112]=0x75;
TRAMEOUT[113]=0x72;
TRAMEOUT[114]=0x69;
TRAMEOUT[115]=0x73;
TRAMEOUT[116]=0x61;
TRAMEOUT[117]=0x74;
TRAMEOUT[118]=0x69;
TRAMEOUT[119]=0x6F;
TRAMEOUT[120]=0x6E;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='0';
TRAMEOUT[3]='c';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x31;
TRAMEOUT[7]=0x33;
TRAMEOUT[8]=0x3B;
TRAMEOUT[9]=0x32;
TRAMEOUT[10]=0x48;
TRAMEOUT[11]=0x1B;
TRAMEOUT[12]=0x5B;
TRAMEOUT[13]=0x31;
TRAMEOUT[14]=0x37;
TRAMEOUT[15]=0x3B;
TRAMEOUT[16]=0x32;
TRAMEOUT[17]=0x48;
TRAMEOUT[18]=0x1B;
TRAMEOUT[19]=0x5B;
TRAMEOUT[20]=0x31;
TRAMEOUT[21]=0x38;
TRAMEOUT[22]=0x3B;
TRAMEOUT[23]=0x31;
TRAMEOUT[24]=0x36;
TRAMEOUT[25]=0x48;
TRAMEOUT[26]=0x39;
TRAMEOUT[27]=0x3A;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]=0x41; //ADECEF
TRAMEOUT[30]=0x44;
TRAMEOUT[31]=0x45;
TRAMEOUT[32]=0x43;
TRAMEOUT[33]=0x45;
TRAMEOUT[34]=0x46;
TRAMEOUT[35]=0x20;
TRAMEOUT[36]=0x20;
TRAMEOUT[37]=0x20;
TRAMEOUT[38]=0x20;
TRAMEOUT[39]=0x20;
TRAMEOUT[40]=0x20;
TRAMEOUT[41]=0x20;
TRAMEOUT[42]=0x20;
TRAMEOUT[43]=0x20;
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x20;
TRAMEOUT[46]=0x20;
TRAMEOUT[47]=0x20;
TRAMEOUT[48]=0x20;
TRAMEOUT[49]=0x20;
TRAMEOUT[50]=0x20;
TRAMEOUT[51]=0x20;
TRAMEOUT[52]=0x20;
TRAMEOUT[53]=0x20;
TRAMEOUT[54]=0x20;
TRAMEOUT[55]=0x20;
TRAMEOUT[56]=0x20;
TRAMEOUT[57]=0x20; 
TRAMEOUT[58]=0x1B;
TRAMEOUT[59]=0x5B;
TRAMEOUT[60]=0x32;
TRAMEOUT[61]=0x31;
TRAMEOUT[62]=0x3B;
TRAMEOUT[63]=0x32;
TRAMEOUT[64]=0x48;
TRAMEOUT[65]=0x1B;
TRAMEOUT[66]=0x5B;
TRAMEOUT[67]=0x31;
TRAMEOUT[68]=0x34;
TRAMEOUT[69]=0x3B;
TRAMEOUT[70]=0x31;
TRAMEOUT[71]=0x36;
TRAMEOUT[72]=0x48;
TRAMEOUT[73]=0x36;
TRAMEOUT[74]=0x3A;
TRAMEOUT[75]=0x20;
TRAMEOUT[76]=0x43;
TRAMEOUT[77]=0x6F;
TRAMEOUT[78]=0x6E;
TRAMEOUT[79]=0x66;
TRAMEOUT[80]=0x69;
TRAMEOUT[81]=0x67;
TRAMEOUT[82]=0x75;
TRAMEOUT[83]=0x72;
TRAMEOUT[84]=0x61;
TRAMEOUT[85]=0x74;
TRAMEOUT[86]=0x69;
TRAMEOUT[87]=0x6F;
TRAMEOUT[88]=0x6E;
TRAMEOUT[89]=0x20;
TRAMEOUT[90]=0x64;
TRAMEOUT[91]=0x27;
TRAMEOUT[92]=0x75;
TRAMEOUT[93]=0x6E;
TRAMEOUT[94]=0x20;
TRAMEOUT[95]=0x63;
TRAMEOUT[96]=0x61;
TRAMEOUT[97]=0x62;
TRAMEOUT[98]=0x6C;
TRAMEOUT[99]=0x65;
TRAMEOUT[100]=0x1B;
TRAMEOUT[101]=0x5B;
TRAMEOUT[102]=0x31;
TRAMEOUT[103]=0x35;
TRAMEOUT[104]=0x3B;
TRAMEOUT[105]=0x32;
TRAMEOUT[106]=0x48;
TRAMEOUT[107]=0x1B;
TRAMEOUT[108]=0x5B;
TRAMEOUT[109]=0x31;
TRAMEOUT[110]=0x36;
TRAMEOUT[111]=0x3B;
TRAMEOUT[112]=0x31;
TRAMEOUT[113]=0x36;
TRAMEOUT[114]=0x48;
TRAMEOUT[115]=0x38;
TRAMEOUT[116]=0x3A;
TRAMEOUT[117]=0x20;
TRAMEOUT[118]=0x50;
TRAMEOUT[119]=0x61;
TRAMEOUT[120]=0x72;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='0';
TRAMEOUT[3]='d';
TRAMEOUT[4]=0x61;
TRAMEOUT[5]=0x6D;
TRAMEOUT[6]=0x65;
TRAMEOUT[7]=0x74;
TRAMEOUT[8]=0x72;
TRAMEOUT[9]=0x65;
TRAMEOUT[10]=0x73;
TRAMEOUT[11]=0x20;
TRAMEOUT[12]=0x64;
TRAMEOUT[13]=0x27;
TRAMEOUT[14]=0x65;
TRAMEOUT[15]=0x78;
TRAMEOUT[16]=0x70;
TRAMEOUT[17]=0x6C;
TRAMEOUT[18]=0x6F;
TRAMEOUT[19]=0x69;
TRAMEOUT[20]=0x74;
TRAMEOUT[21]=0x61;
TRAMEOUT[22]=0x74;
TRAMEOUT[23]=0x69;
TRAMEOUT[24]=0x6F;
TRAMEOUT[25]=0x6E;
TRAMEOUT[26]=0x20;
TRAMEOUT[27]=0x64;
TRAMEOUT[28]=0x75;
TRAMEOUT[29]=0x20;
TRAMEOUT[30]=0x73;
TRAMEOUT[31]=0x79;
TRAMEOUT[32]=0x73;
TRAMEOUT[33]=0x74;
TRAMEOUT[34]=0x65;
TRAMEOUT[35]=0x6D;
TRAMEOUT[36]=0x65;
TRAMEOUT[37]=0x1B;
TRAMEOUT[38]=0x5B;
TRAMEOUT[39]=0x32;
TRAMEOUT[40]=0x33;
TRAMEOUT[41]=0x3B;
TRAMEOUT[42]=0x31;
TRAMEOUT[43]=0x36;
TRAMEOUT[44]=0x48;
TRAMEOUT[45]=0x20;
TRAMEOUT[46]=0x1B;
TRAMEOUT[47]=0x5B;
TRAMEOUT[48]=0x37;
TRAMEOUT[49]=0x6D;
TRAMEOUT[50]=0x43;
TRAMEOUT[51]=0x68;
TRAMEOUT[52]=0x69;
TRAMEOUT[53]=0x66;
TRAMEOUT[54]=0x66;
TRAMEOUT[55]=0x72;
TRAMEOUT[56]=0x65;
TRAMEOUT[57]=0x73;
TRAMEOUT[58]=0x1B;
TRAMEOUT[59]=0x5B;
TRAMEOUT[60]=0x30;
TRAMEOUT[61]=0x6D;
TRAMEOUT[62]=0x20;
TRAMEOUT[63]=0x3D;
TRAMEOUT[64]=0x20;
TRAMEOUT[65]=0x63;
TRAMEOUT[66]=0x68;
TRAMEOUT[67]=0x6F;
TRAMEOUT[68]=0x69;
TRAMEOUT[69]=0x78;
TRAMEOUT[70]=0x20;
TRAMEOUT[71]=0x6D;
TRAMEOUT[72]=0x65;
TRAMEOUT[73]=0x6E;
TRAMEOUT[74]=0x75;
TRAMEOUT[75]=0x20;
TRAMEOUT[76]=0x20;
TRAMEOUT[77]=0x20;
TRAMEOUT[78]=0x20;
TRAMEOUT[79]=0x20;
TRAMEOUT[80]=0x20;
TRAMEOUT[81]=0x1B;
TRAMEOUT[82]=0x5B;
TRAMEOUT[83]=0x37;
TRAMEOUT[84]=0x6D;
TRAMEOUT[85]=0x45;
TRAMEOUT[86]=0x53;
TRAMEOUT[87]=0x43;
TRAMEOUT[88]=0x1B;
TRAMEOUT[89]=0x5B;
TRAMEOUT[90]=0x30;
TRAMEOUT[91]=0x6D;
TRAMEOUT[92]=0x20;
TRAMEOUT[93]=0x3D;
TRAMEOUT[94]=0x20;
TRAMEOUT[95]=0x73;
TRAMEOUT[96]=0x6F;
TRAMEOUT[97]=0x72;
TRAMEOUT[98]=0x74;
TRAMEOUT[99]=0x69;
TRAMEOUT[100]=0x65;
TRAMEOUT[101]=0x1B;
TRAMEOUT[102]=0x5B;
TRAMEOUT[103]=0x30;
TRAMEOUT[104]=0x4B;
TRAMEOUT[105]=0x1B;
TRAMEOUT[106]=0x5B;
TRAMEOUT[107]=0x31;
TRAMEOUT[108]=0x38;
TRAMEOUT[109]=0x3B;
TRAMEOUT[110]=0x35;
TRAMEOUT[111]=0x35;
TRAMEOUT[112]=0x48;
TRAMEOUT[113]=0x1B;
TRAMEOUT[114]=0x5B;
TRAMEOUT[115]=0x31;
TRAMEOUT[116]=0x3B;
TRAMEOUT[117]=0x35;
TRAMEOUT[118]=0x39;
TRAMEOUT[119]=0x48; //DATE 
TRAMEOUT[120]=0x20; //J
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 117); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (117); 

//  HEURE EVENTUELLE

TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='0';
TRAMEOUT[3]='e';
TRAMEOUT[4]=0x20; //J
TRAMEOUT[5]=0x20; // /
TRAMEOUT[6]=0x20;//M
TRAMEOUT[7]=0x20;//M
TRAMEOUT[8]=0x20; // /  2F
TRAMEOUT[9]=0x20;// A
TRAMEOUT[10]=0x20; //A
TRAMEOUT[11]=0x20; //A
TRAMEOUT[12]=0x20; //A
TRAMEOUT[13]=0x20;  
TRAMEOUT[14]=0x20;
TRAMEOUT[15]=0x20; //H
TRAMEOUT[16]=0x20; //H
TRAMEOUT[17]=0x20;//3A :
TRAMEOUT[18]=0x20;//m
TRAMEOUT[19]=0x20;//m
TRAMEOUT[20]='*';

j=0;
for (iii=4;iii<21;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
//TCPPutArray(MySocket, TRAMEOUT, 16); //JUSQUA LA LONGUEUR DU

TRAMEENVOITCPDONNEES (16);
}

}




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////// FONCTION MINITEL///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void minitel (void)           /////////////////fonction principale du minitel
{
//BYTE ETAT;
repet =0;                    
decal=0;
while (STOPMINITEL!=0xFF){
	
switch(ETAT1)
		{
	


			case I:  /////page mots de passe

                          if (repet ==0)     ///la boucle permit d'envoyer la page une fois seulement 
		{					max_decalage=0;
 							//acc_u[0]='c';
 							//acc_u[1]='c';
 							//acc_u[2]='c';
 							//acc_u[3]='c';

             				page ('I');       /// ENVOIE DE LA PAGE 
                            nom_site();
                            repet++;
		}
							accordUSEROUADMI=verifMP();	
     						if ((accordUSEROUADMI==1) ||(accordUSEROUADMI==2))      ///// verification du mots de passe ADMIN OU USER
                            {
                            ETAT1 = M; ///aller vers la page menu

                             }
                            else
                             {
                            repet=0;
                              ETAT1 = I; ////        rester sur la meme page 
                              
                              }    
                             break;
            case M:

               				page ('M');  ///////////envoie de la page "menu principal"
                            nom_site();
							TRAMERECEPTIONTCPDONNEES (1);
							//TRAMERECEPTIONRS232DONNEES (1);
							if (STOPMINITEL==0xFF)
							goto fin_minitel;
                            repet=0;
                            capt_exist=1;
							if (TRAMEIN[0]=='1')  // TOUCHE 1
								{
								ETAT1 = A;    ///////////entrer dans le menu 1 02112020
								}
							else if (TRAMEIN[0]=='2') {           //////////entrer dans la page menu 2
								ETAT1 = B;}
							else if (TRAMEIN[0]=='3')  {ETAT1 = C;} //////////entrer dans la page menu 3
							else if (TRAMEIN[0]=='5') {ETAT1 = D;} //////////entrer dans la page menu 5
                            else if (TRAMEIN[0]=='6')  {ETAT1 = E;} //////////entrer dans la page menu 6.1
						
                            
							else  if (TRAMEIN[0]=='8') {ETAT1 = K;} //////////entrer dans la page menu 8
							else  if (TRAMEIN[0]=='9') {ETAT1 = L;} //////////entrer dans la page menu 8
						break;
            case A:                         ////////////menu 1
                   	if (repet ==0){
							page ('1');      //// la boucle permit d'envoyer la page une fois seulement 
							nom_site();
							npage='1';       ///// num�ro de la page
 							nchamps=1;		 ////num�ro de champs d'�criture 
							max_champ=10;    //// le nombre maximal de champs d'�criture dans une page
							
                            }
                            repet++;
					 gestion_menu1();

                          break;      
            case B:                      /////////////////menu2
                     	if (repet ==0){
							page ('2');
							//rajout pour ip
							TRAMEOUT[0]=0x1B; 
							TRAMEOUT[1]=0x5B ;
							TRAMEOUT[2]=0x30;
							TRAMEOUT[3]=0x32;
							TRAMEOUT[4]=0x3B;
							TRAMEOUT[5]=0x33 ;
							TRAMEOUT[6]=0x30 ;
							TRAMEOUT[7]=0x48 ;
							TRAMEENVOITCPDONNEES (8);	
							//nom_site(); //sup pour ip
							npage='2';
							nchamps=1;
                            }
                            repet++;
					 gestion_menu2();
break;
            case C:						//////////////menu3
	if (repet ==0){
							page ('3');
							nom_site();
							npage='3';
							nchamps=0;
                            }
                            repet++;
                          gestion_menu3();
					 
break;

            case D:						///////////menu 5
	if (repet ==0){
							page ('5');
							nom_site();
							npage='5';
							nchamps=0;
                            }
                            repet++;
					 gestion_menu5();
             break;
      case E:							////////menu6.1
	if (repet ==0){
							page ('6');
							nom_site();
							npage='6';
							nchamps=0;
                            }
                            repet++;
							gestion_menu6 ();                           ///////////////gestion de la premi�re page du menu6 
					 		TRAMERECEPTIONminitel();
							if (STOPMINITEL==0xFF)
							goto fin_minitel;	
                            if (TRAMEIN[0]==0x17 && TRAMEIN[1]==0x41 ) ////////////////validation et retour vers le menu principal
                             {
                             
                             ETAT1 = F;        //////////si c'est on a appui sur "entrer" on passe a la deuxi�me page du menu 6. 
         					repet =0;
                         
                             }
           				   if ( TRAMEIN[0]==0x17 && TRAMEIN[1] == 0x46 )   ////////////////echape et  revenir au menu principale 
                              {
                              repet =0;
                              ETAT1=M;
                              }
             break;
      case F:							//////menu6.2
	if (repet ==0){
							page ('a');
							npage='a';
							nom_site();
							max_champ=5;
							nchamps=1;
							decal=0;
                            position_init(npage, nchamps);
							envoi_cursur();
                            }
                                             
                            repet++;
					 gestion_menu6a ();                        ////////gestion de la deuxi�me page du menu 6
					
				                                 /////// on passe a l'�tat G "la troisi�me page du menu 6
             break;
      case G: 
	if (repet ==0){						//////menu6.3
						page ('b');
						//nom_site(); 
						npage='b';
						decal=0;
						max_champ=6;
						nchamps=1;
						position_init(npage, nchamps);
						envoi_cursur();
					}
                    repet++;
					gestion_menu6b ();
             break;
      case K:
	if (repet ==0){						/////menu8
							page ('8');
							max_champ=11;
							nom_site();
							npage='8';
							nchamps=0;

                            }
                            repet++;
					gestion_menu8(); 
                      
             break;
		case L: //RAZ CMS
		ETAT1 = M;
		//TEMPO(2);
			TRAMEOUT[0]=0x1B;
				TRAMEOUT[1]=0x5B;
				TRAMEOUT[2]=0x32;
				TRAMEOUT[3]=0x4A;
				TRAMEOUT[4]=0x1B;
				TRAMEOUT[5]=0x5B;
				TRAMEOUT[6]=0x31;
				TRAMEOUT[7]=0x32;
				TRAMEOUT[8]=0x3B;
				TRAMEOUT[9]=0x31;
				TRAMEOUT[10]=0x33;
				TRAMEOUT[11]=0x48;
				TRAMEOUT[12]=0x20;
				TRAMEOUT[13]='C';
				TRAMEOUT[14]='O';
				TRAMEOUT[15]='D';
				TRAMEOUT[16]='E';
				TRAMEOUT[17]='?';
				TRAMEOUT[18]=' ';
				TRAMEOUT[19]=' ';
				TRAMEOUT[20]=' ';
				TRAMEOUT[21]=' ';
				TRAMEOUT[22]=' ';
				//TCPPutArray(MySocket, TRAMEOUT, 23); 
	
				TRAMEENVOITCPDONNEES (23);
	TRAMERECEPTIONminitel();
		if (TRAMEIN[0]=='1')
		{	
		TRAMERECEPTIONminitel();
		}
		else
		{
		goto dontraz;
		}
		if (TRAMEIN[0]=='9')
		{
			TRAMERECEPTIONminitel();
		}
		else
		{
		goto dontraz;
		}
		if (TRAMEIN[0]=='9')
		{
			TRAMERECEPTIONminitel();
		}
		else
		{
		goto dontraz;
		}

		if (STOPMINITEL==0xFF)
		goto fin_minitel;

		if (TRAMEIN[0]=='8')
		{

		if (STOPMINITEL==0xFF)
		goto fin_minitel;
				TRAMEOUT[0]=0x1B;
				TRAMEOUT[1]=0x5B;
				TRAMEOUT[2]=0x32;
				TRAMEOUT[3]=0x4A;
				TRAMEOUT[4]=0x1B;
				TRAMEOUT[5]=0x5B;
				TRAMEOUT[6]=0x31;
				TRAMEOUT[7]=0x32;
				TRAMEOUT[8]=0x3B;
				TRAMEOUT[9]=0x31;
				TRAMEOUT[10]=0x33;
				TRAMEOUT[11]=0x48;
				TRAMEOUT[12]=0x20;
				TRAMEOUT[13]='C';
				TRAMEOUT[14]='M';
				TRAMEOUT[15]='S';
				TRAMEOUT[16]=' ';
				TRAMEOUT[17]='R';
				TRAMEOUT[18]='E';
				TRAMEOUT[19]='B';
				TRAMEOUT[20]='O';
				TRAMEOUT[21]='O';
				TRAMEOUT[22]='T';
				
				TRAMEOUT[23]=' ';

				TRAMEOUT[24]='V';
				TRAMEOUT[25]='E';
				TRAMEOUT[26]='U';
				TRAMEOUT[27]='I';
				TRAMEOUT[28]='L';
				TRAMEOUT[29]='L';
				TRAMEOUT[30]='E';
				TRAMEOUT[31]='Z';
				TRAMEOUT[32]=' ';
				TRAMEOUT[33]='C';
				TRAMEOUT[34]='O';
				TRAMEOUT[35]='U';
				TRAMEOUT[36]='P'; 
				TRAMEOUT[37]='E';
				TRAMEOUT[38]='R';
				//TCPPutArray(MySocket, TRAMEOUT, 39); 
				TRAMEENVOITCPDONNEES (39);	
				TEMPO(2);
				TRAMERECEPTIONminitel(); //RECEPTION FICTIVE pour faire apparaitre message->IP ON ATTEND LA DECONNECT !!!!!!
				RAZCMS=1; //RAZ CMS
		}


	if (TRAMEIN[0]=='3') //MODE ESPION EEPROM
		{

		
mouchard3next:
		//TRAMERECEPTIONminitel();	
		//if (TRAMEIN[0]=='q')
		//goto dontraz;	
	
	
		MOUCHARDREGLAHEURE();
		if (STOPMINITEL==0xFF)
		goto fin_minitel;
	

		}










	

	if (TRAMEIN[0]=='9') //MODE ESPION EEPROM
		{
		
mouchardnext:
		TRAMERECEPTIONminitel();	
		if (TRAMEIN[0]=='q')
		goto dontraz;	
		if ((TRAMEIN[0]<'1')||(TRAMEIN[0]>'5'))
		TRAMEIN[0]='1';
	
		MOUCHARD(TRAMEIN[0]);
		if (STOPMINITEL==0xFF)
		goto fin_minitel;
		//TRAMERECEPTIONminitel();
		//if (TRAMEIN[0]=='c') //ON CONTINU
		goto mouchardnext;
		//if (STOPMINITEL==0xFF)
		//goto fin_minitel;

		}

		if (TRAMEIN[0]=='7') //MODE ESPION EEPROM
		{
mouchard2next:		
		DEMANDE_INFOS_CAPTEURS[0]='#';
		TRAMERECEPTIONminitel();
	
		if (TRAMEIN[0]=='q')
		goto dontraz;
		if ((TRAMEIN[0]>'1')||(TRAMEIN[0]<'0'))
		TRAMEIN[0]='0';
		
			

		DEMANDE_INFOS_CAPTEURS[1]=TRAMEIN[0];
		if (STOPMINITEL==0xFF)
		goto fin_minitel;

		TRAMERECEPTIONminitel();
		if ((TRAMEIN[0]<'0')||(TRAMEIN[0]>'9'))
		TRAMEIN[0]='1';

		DEMANDE_INFOS_CAPTEURS[2]=TRAMEIN[0];
		if (STOPMINITEL==0xFF)
		goto fin_minitel;

		TRAMERECEPTIONminitel();
		if ((TRAMEIN[0]<'0')||(TRAMEIN[0]>'9'))
		TRAMEIN[0]='1';

		DEMANDE_INFOS_CAPTEURS[3]=TRAMEIN[0];
		if (STOPMINITEL==0xFF)
		goto fin_minitel;

		MOUCHARD2();
		

		//TRAMERECEPTIONminitel();
		//if (TRAMEIN[0]=='c') //ON CONTINU
		goto mouchard2next;
		//if (STOPMINITEL==0xFF)
		//goto fin_minitel;
		}




dontraz:
		TEMPO(1);
		break;
		




}
fin_minitel:
		if (STOPMINITEL==0xFF) //ARRET
				{
				repet=0;
				ETAT1=I;
				break;
				
				}




}

}

void MOUCHARDREGLAHEURE(void)
{

char iop;
LIRE_HEURE_MINITEL();

	TRAMEOUT[0]=0x1B;
				TRAMEOUT[1]=0x5B;
				TRAMEOUT[2]=0x32;
				TRAMEOUT[3]=0x4A;
				TRAMEOUT[4]=0x1B;
				TRAMEOUT[5]=0x5B;
				TRAMEOUT[6]=0x31;
				TRAMEOUT[7]=0x32;
				TRAMEOUT[8]=0x3B;
				TRAMEOUT[9]=0x31;
				TRAMEOUT[10]=0x33;
				TRAMEOUT[11]=0x48;
				TRAMEOUT[12]=0x20;
				TRAMEOUT[13]='H';
				TRAMEOUT[14]='O';
				TRAMEOUT[15]='R';
				TRAMEOUT[16]='L';
				TRAMEOUT[17]='O';
				TRAMEOUT[18]='G';
				TRAMEOUT[19]='E';
				TRAMEOUT[20]='(';
				TRAMEOUT[21]='h';
				TRAMEOUT[22]='h';
				TRAMEOUT[23]='m';
				TRAMEOUT[24]='m';
				TRAMEOUT[25]='s';
				TRAMEOUT[26]='s';
				TRAMEOUT[27]='J';
				TRAMEOUT[28]='J';
				TRAMEOUT[29]='M';
				TRAMEOUT[30]='M';
				TRAMEOUT[31]='A';
				TRAMEOUT[32]='A';
				TRAMEOUT[33]=')';
				TRAMEOUT[34]=' ';
				TRAMEOUT[35]=Tab_Horodatage[6];
				TRAMEOUT[36]=Tab_Horodatage[7];
				TRAMEOUT[37]=Tab_Horodatage[8];
				TRAMEOUT[38]=Tab_Horodatage[9];
				TRAMEOUT[39]=Tab_Horodatage[4];
				TRAMEOUT[40]=Tab_Horodatage[5];
				TRAMEOUT[41]=Tab_Horodatage[0];
				TRAMEOUT[42]=Tab_Horodatage[1];
				TRAMEOUT[43]=Tab_Horodatage[2];
				TRAMEOUT[44]=Tab_Horodatage[3];
				TRAMEOUT[45]=Tab_Horodatage[10];	
				TRAMEOUT[46]=Tab_Horodatage[11];


				TRAMEOUT[47]=' ';
				TRAMEOUT[48]='q';
				TRAMEOUT[49]=' ';
				TRAMEOUT[50]='o';
				TRAMEOUT[51]='u';
				TRAMEOUT[52]=' ';
				TRAMEOUT[53]='v';
				TRAMEOUT[54]='?';
	



				



		



				//TCPPutArray(MySocket, TRAMEOUT, 39); 
				TRAMEENVOITCPDONNEES (55);		                
				
				for (iop=0;iop<12;iop++)
				{
				
               	TMP[iop]=RECEVOIR_TCP();//hhmmssJJMMAA
				//TRAMEOUT[7]++;
				TRAMEOUT[0]=TMP[iop];
				TRAMEENVOITCPDONNEES (1);
				}

					
	//	TRAMERECEPTIONminitel();	


repeterconfir:

		TRAMEIN[0]=RECEVOIR_TCP();//q ou v
		if (STOPMINITEL==0xFF)
		TRAMEIN[0]='q'; // ON QUITTE
						  
		if (TRAMEIN[0]=='v')
		{
			
		ENVOIE_DES_TRAMES (0,334,&TMP) ;//HEURES
		if (TERMINEACTION==1) // LE REGLAGE A ETE FAIT
		{

				TRAMEOUT[0]=0x1B;
				TRAMEOUT[1]=0x5B;
				TRAMEOUT[2]=0x32;
				TRAMEOUT[3]=0x4A;
				TRAMEOUT[4]=0x1B;
				TRAMEOUT[5]=0x5B;
				TRAMEOUT[6]=0x31;
				TRAMEOUT[7]=0x32;
				TRAMEOUT[8]=0x3B;
				TRAMEOUT[9]=0x31;
				TRAMEOUT[10]=0x33;
				TRAMEOUT[11]=0x48;
				TRAMEOUT[12]=0x20;
				TRAMEOUT[13]='C';
				TRAMEOUT[14]='M';
				TRAMEOUT[15]='S';
				TRAMEOUT[16]=' ';
				TRAMEOUT[17]='R';
				TRAMEOUT[18]='E';
				TRAMEOUT[19]='B';
				TRAMEOUT[20]='O';
				TRAMEOUT[21]='O';
				TRAMEOUT[22]='T';
				
				TRAMEOUT[23]=' ';

				TRAMEOUT[24]='V';
				TRAMEOUT[25]='E';
				TRAMEOUT[26]='U';
				TRAMEOUT[27]='I';
				TRAMEOUT[28]='L';
				TRAMEOUT[29]='L';
				TRAMEOUT[30]='E';
				TRAMEOUT[31]='Z';
				TRAMEOUT[32]=' ';
				TRAMEOUT[33]='C';
				TRAMEOUT[34]='O';
				TRAMEOUT[35]='U';
				TRAMEOUT[36]='P'; 
				TRAMEOUT[37]='E';
				TRAMEOUT[38]='R';
				//TCPPutArray(MySocket, TRAMEOUT, 39); 
				TRAMEENVOITCPDONNEES (39);		
			TRAMERECEPTIONminitel(); //RECEPTION FICTIVE pour faire apparaitre message->IP ON ATTEND LA DECONNECT !!!!!!
				
		//RAZCMS=1;
		TRAMEIN[0]='q'; // ON QUITTE si RAZ PAS FAIT
		

		}
		}

		if (TRAMEIN[0]!='q')
		goto repeterconfir;



}

char MOUCHARD2(void)
{
unsigned int iop,ghd,jkl,tgy;



				//	iop=128*ghd+30000;

				//	LIRE_EEPROM(3,iop,&TRAMEIN,4); //LIRE DANS EEPROM lexistance de toutes le voies

DEMANDE_INFOS_CAPTEURS[4]='0';
DEMANDE_INFOS_CAPTEURS[5]='0';	
tgy=0;		
iop=1;
			for (ghd=0;ghd<=16;ghd++)
			{
			LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM				
			//TEMPO(1);
			delay_qms(10);
			TRAMEOUT[0]=0x1B;
			TRAMEOUT[1]=0x5B;
			TRAMEOUT[2]=0x30;
			TRAMEOUT[3]=0x6D;
	
			TRAMEOUT[4]=0x1B;
			TRAMEOUT[5]=0x5B;
	
			TRAMEOUT[8]=0X3B;
			TRAMEOUT[9]=0x30;
			TRAMEOUT[10]=0x31; //COL 1
			TRAMEOUT[11]=0x48;
			for (jkl=0;jkl<120;jkl++)
			{
			TRAMEOUT[jkl+12]=TMP[jkl+6];	//ON PLACE LA TRAME MAIS DECALEE DE 12 LIGNE COL
			}
			if (iop==10)
			{
			tgy=1;
			//iop=0;
			iop=0;	
			}
			TRAMEOUT[6]=0x30+tgy; // 0
			TRAMEOUT[7]=0x30+iop; // 1 ligne
			//TRAMEOUT[38]=iop+0x30;
			//TRAMEOUT[39]=tgy+0x30;
			//TRAMEOUT[13]=TRAMEOUT[13]+0x30;	
			//TCPPutArray(MySocket, TRAMEOUT, 128); 	
			TRAMEENVOITCPDONNEES (128);
			
			 		
			if (DEMANDE_INFOS_CAPTEURS[5]=='9')
			{
			DEMANDE_INFOS_CAPTEURS[5]=47;
			DEMANDE_INFOS_CAPTEURS[4]='1';
			} 				
			iop++;
			DEMANDE_INFOS_CAPTEURS[5]=DEMANDE_INFOS_CAPTEURS[5]+1;			
			
			}








}



char MOUCHARD(char carte)


{
int iop,ghd,jlm,jkl,tgy;
jlm=carte;
if (carte=='1')
carte=1;
if (carte=='2')
carte=21;
if (carte=='3')
carte=41;
if (carte=='4')
carte=61;
if (carte=='5')
carte=81;

tgy=0;
iop=0;
	for (ghd=carte;ghd<=(carte+19);ghd++) //ON ENV. LES TABLES D'EXISTANCES
			{
			LIRE_EEPROM(3,128*ghd+30000,&TRAMEIN,4); //LIRE DANS EEPROM	
			
	
			
			for (jkl=0;jkl<24;jkl++)
			{
			TRAMEOUT[jkl+12]=TRAMEIN[jkl];	//ON PLACE LA TRAME MAIS DECALEE DE 12 LIGNE COL
			}

			TRAMEOUT[0]=0x1B;
			TRAMEOUT[1]=0x5B;
			TRAMEOUT[2]=0x30;
			TRAMEOUT[3]=0x6D;
	
			TRAMEOUT[4]=0x1B;
			TRAMEOUT[5]=0x5B;
	
			TRAMEOUT[8]=0X3B;
			TRAMEOUT[9]=0x30;
			TRAMEOUT[10]=0x31; //COL 1
			TRAMEOUT[11]=0x48;
	
			TRAMEOUT[36]='c';
			TRAMEOUT[37]=jlm;
			
		
			TRAMEOUT[40]='/';
			
			//TRAMEIN[0]=0x31;
			tgy++;
			if (tgy==10)
			{
			iop++;
			tgy=0;
			//TRAMEIN[0]=TRAMEIN[0]+1;
			}
			TRAMEOUT[6]=0x30+iop; // 0
			TRAMEOUT[7]=0x30+tgy; // 1 ligne
			TRAMEOUT[38]=iop+0x30;
			TRAMEOUT[39]=tgy+0x30;
			TRAMEOUT[13]='x';	


			//TCPPutArray(MySocket, TRAMEOUT, 41); 	
			TRAMEENVOITCPDONNEES (41);
			delay_qms(10);
			}

//	TEMPO(5);

}

void TEMPO(char seconde)
{
char i;


for (i=0;i<seconde;i++)
{
delay_qms(100);
delay_qms(100);
delay_qms(100);
delay_qms(100);
delay_qms(100);
delay_qms(100);
delay_qms(100);
delay_qms(100);
delay_qms(100);
delay_qms(100);
}

}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////gestion de la page 8 du menu //////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gestion_menu8 (void)
{
char tc;

Recuperer_param_cms();

LIRE_HEURE_MINITEL();




nchamps=1;
position_init(npage, nchamps);
envoi_cursur();

//gestion_page ();

for (tc=0;tc<30;tc++)
{
TRAMEOUT[tc]=ID_rack[tc];
};














TRAMEENVOITCPDONNEES (30);
nchamps++;
position_init(npage, nchamps);
envoi_cursur();

TRAMEOUT[0]=T_alarme[0];
TRAMEOUT[1]=T_alarme[1];
TRAMEOUT[2]=T_alarme[2];
TRAMEENVOITCPDONNEES (3);
nchamps++;
position_init(npage, nchamps);
envoi_cursur();

TRAMEOUT[0]=T_sortie[0];
TRAMEOUT[1]=T_sortie[1];
TRAMEOUT[2]=T_sortie[2];
TRAMEENVOITCPDONNEES (3);
nchamps++;
position_init(npage, nchamps);
envoi_cursur();








TRAMEOUT[0]=HH[0];
TRAMEOUT[1]=HH[1];
TRAMEOUT[2]=0x3A;
TRAMEOUT[3]=mm[0];
TRAMEOUT[4]=mm[1];
TRAMEENVOITCPDONNEES (5);
nchamps++;
position_init(npage, nchamps);
envoi_cursur();

TRAMEOUT[0]=JJ[0];
TRAMEOUT[1]=JJ[1];
TRAMEOUT[2]=0x2D;
TRAMEOUT[3]=MM[0];
TRAMEOUT[4]=MM[1];
TRAMEOUT[5]=0x2D;
TRAMEOUT[6]=AA[0];
TRAMEOUT[7]=AA[1];
TRAMEOUT[8]=AA[2];
TRAMEOUT[9]=AA[3];
TRAMEENVOITCPDONNEES (10);

nchamps++;
position_init(npage, nchamps);
envoi_cursur();


//TRAMEOUT[0]=acc_u[0];
//TRAMEOUT[1]=acc_u[1];
//TRAMEOUT[2]=acc_u[2];
//TRAMEOUT[3]=acc_u[3];
//TRAMEENVOITCPDONNEES (4);
nchamps++;
position_init(npage, nchamps);
envoi_cursur();


TRAMEOUT[0]=acc_conf[0];
TRAMEOUT[1]=acc_conf[1];
TRAMEOUT[2]=acc_conf[2];
TRAMEOUT[3]=acc_conf[3];
TRAMEENVOITCPDONNEES (4);
nchamps++;
position_init(npage, nchamps);
envoi_cursur();


for (tc=0;tc<15;tc++)
{
TRAMEOUT[tc]=nr_cog[tc];
};
TRAMEENVOITCPDONNEES (15);
nchamps++;
position_init(npage, nchamps);
envoi_cursur();


//for (tc=0;tc<15;tc++) /////010.168.192.056 telgat
//{
//TRAMEOUT[tc]=nr_cms[tc];
//};

//TRAMEENVOITCPDONNEES (15);
nchamps++;
position_init(npage, nchamps);
envoi_cursur();

TRAMEOUT[0]=passerelle[0];
TRAMEOUT[1]=passerelle[1];
TRAMEOUT[2]=passerelle[2];
TRAMEOUT[3]=passerelle[3];
TRAMEOUT[4]=passerelle[4];
TRAMEOUT[5]=passerelle[5];
TRAMEOUT[6]=passerelle[6];
TRAMEOUT[7]=passerelle[7];
TRAMEOUT[8]=passerelle[8];
TRAMEOUT[9]=passerelle[9];
TRAMEOUT[10]=passerelle[10];
TRAMEOUT[11]=passerelle[11];
TRAMEOUT[12]=passerelle[12];
TRAMEOUT[13]=passerelle[13];
TRAMEOUT[14]=passerelle[14];

TRAMEENVOITCPDONNEES (15);
nchamps++;
position_init(npage, nchamps);
envoi_cursur();


//TRAMEOUT[0]=DNS[0];
//TRAMEOUT[1]=DNS[1];
//TRAMEOUT[2]=DNS[2];
//TRAMEOUT[3]=DNS[3];
//TRAMEOUT[4]=DNS[4];
//TRAMEOUT[5]=DNS[5];
//TRAMEOUT[6]=DNS[6];
//TRAMEOUT[7]=DNS[7];
//TRAMEOUT[8]=DNS[8];
//TRAMEOUT[9]=DNS[9];
//TRAMEOUT[10]=DNS[10];
//TRAMEOUT[11]=DNS[11];
//TRAMEOUT[12]=DNS[12];
//TRAMEOUT[13]=DNS[13];
//TRAMEOUT[14]=DNS[14];

//TRAMEENVOITCPDONNEES (15);

nchamps=1;
position_init(npage, nchamps);
envoi_cursur();
 											do
										    {
											
											gestion_page();
                                         if (STOPMINITEL==0xFF)
											break;
											}while (ETAT1==K);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////gestion de la page 6B du menu //////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gestion_menu6b (void)
{

char o;
CHG_CODAGE=0;

VOIECOD[3]='0';     
VOIECOD[4]='1';
position_init(npage, 1); //LISTE
envoi_cursur();
TRAMEOUT[0]=VOIECOD[3];
TRAMEOUT[1]=VOIECOD[4];
TRAMEENVOITCPDONNEES (2);

//voie_select[0]=tableau[0];

//REMPLIR LISTE CAPTEURS EXISTANTS

NBRECAPTEURS_CABLE=0;

for (o=0;o<17;o++)
{
		CHERCHER_DONNNEES_CABLES(&VOIECOD,o); //CAPTEURS UN A UN
		if (capt_exist==1)
		{
		IntToChar(o,&TMP,2);
		TRAMEOUT[8+3*o]='-';
		TRAMEOUT[9+3*o]=TMP[0];
		TRAMEOUT[10+3*o]=TMP[1];
		//TRAMEOUT[3+4*o]=TMP[2];
		NBRECAPTEURS_CABLE=NBRECAPTEURS_CABLE+1;
		}

		else
		{
		IntToChar(o,&TMP,2);
		TRAMEOUT[8+3*o]='-';
		TRAMEOUT[9+3*o]=' ';
		TRAMEOUT[10+3*o]=' ';
		//TRAMEOUT[3+4*o]=' ';
		}

}

TRAMEOUT[59]='-'; //FIN LISTE 
position_init(npage, 7); //LISTE
envoi_cursur();
TRAMEENVOITCPDONNEES (60);

//capt_exist==1;
//CHERCHER_DONNNEES_CABLES(&voie_select,0); //CAPTEUR DEBIT

RE_6B:

o=10*(VOIECOD[3]-48)+(VOIECOD[4]-48);
CHERCHER_DONNNEES_CABLES(&VOIECOD,o); // SI LE CAPTEUR EXISTE 
 

//-----------------------------TYPE DE CAPTEUR




								if  ((TYPE[0]=='A')&&(TYPEDECARTE()=='E'))
								{
									type_capteur[0]='T';
									type_capteur[1]='P';
									type_capteur[2]=0x20;
									type_capteur[3]='A';
									type_capteur[4]='D';
									type_capteur[5]='R';
									type_capteur[6]='E';
									type_capteur[7]='S';
									type_capteur[8]='S';
									type_capteur[9]='A';
									type_capteur[10]='B';
									type_capteur[11]='L';
									type_capteur[12]='E';
								}


								if  ((TYPE[0]=='R')&&(TYPEDECARTE()=='M'))
								{
									type_capteur[0]='T';
									type_capteur[1]='P';
									type_capteur[2]=0x20;
									type_capteur[3]='R';
									type_capteur[4]='E';
									type_capteur[5]='S';
									type_capteur[6]='I';
									type_capteur[7]='S';
									type_capteur[8]='I';
									type_capteur[9]='F';
									type_capteur[10]=0X20;
									type_capteur[11]=0X20;
									type_capteur[12]=0X20;
								}
 
								 								if (TYPE[0]=='0'){ //23092020
									type_capteur[0]='T';
									type_capteur[1]='P';
									type_capteur[2]=0x20;
									type_capteur[3]='I';
									type_capteur[4]='N';
									type_capteur[5]='C';
									type_capteur[6]='O';
									type_capteur[7]='N';
									type_capteur[8]='N';
									type_capteur[9]='U';
									type_capteur[10]=0X20;
									type_capteur[11]=0X20;
									type_capteur[12]=0X20;
								}

								if((VOIECOD[3]=='0' &&	VOIECOD[4]=='0')&&(TYPEDECARTE()=='E')) //SI CODAGE = '00' et carte pas resistive
								{
									type_capteur[0]='D';
									type_capteur[1]='E';
									type_capteur[2]='B';
									type_capteur[3]='I';
									type_capteur[4]='M';
									type_capteur[5]='E';
									type_capteur[6]='T';
									type_capteur[7]='R';
									type_capteur[8]='E';
									type_capteur[9]=0x20;
									type_capteur[10]=0x20;
									type_capteur[11]=0x20;
									type_capteur[12]=0x20;
									identifiant=3; // LE TYPE DE CAPTEUR  1 2 3 adress...resis.....debit..
									enregistrer_donnees();
								
								}




//TYPE DE CAPTEUR 
nchamps=2;
position_init(npage, nchamps);
envoi_cursur();
TRAMEOUT[0]=type_capteur[0];
TRAMEOUT[1]=type_capteur[1];
TRAMEOUT[2]=type_capteur[2];
TRAMEOUT[3]=type_capteur[3];
TRAMEOUT[4]=type_capteur[4];
TRAMEOUT[5]=type_capteur[5];
TRAMEOUT[6]=type_capteur[6];
TRAMEOUT[7]=type_capteur[7];
TRAMEOUT[8]=type_capteur[8];
TRAMEOUT[9]=type_capteur[9];
TRAMEOUT[10]=type_capteur[10];
TRAMEOUT[11]=type_capteur[11];
TRAMEOUT[12]=type_capteur[12];
TRAMEENVOITCPDONNEES (13);


//CONSTITUTION
nchamps=3;
position_init(npage, nchamps);
envoi_cursur();



TRAMEOUT[0]=CONSTITUTION[0];
TRAMEOUT[1]=CONSTITUTION[1];
TRAMEOUT[2]=CONSTITUTION[2];
TRAMEOUT[3]=CONSTITUTION[3];
TRAMEOUT[4]=CONSTITUTION[4];
TRAMEOUT[5]=CONSTITUTION[5];
TRAMEOUT[6]=CONSTITUTION[6];
TRAMEOUT[7]=CONSTITUTION[7];
TRAMEOUT[8]=CONSTITUTION[8];
TRAMEOUT[9]=CONSTITUTION[9];
TRAMEOUT[10]=CONSTITUTION[10];
TRAMEOUT[11]=CONSTITUTION[11];
TRAMEOUT[12]=CONSTITUTION[12];
TRAMEENVOITCPDONNEES (13);


nchamps=4;
position_init(npage, nchamps);
envoi_cursur();
TRAMEOUT[0]=SEUIL[0];
TRAMEOUT[1]=SEUIL[1];
TRAMEOUT[2]=SEUIL[2];
TRAMEOUT[3]=SEUIL[3];
TRAMEENVOITCPDONNEES (4);
//------------------
if(VOIECOD[3]=='0' &&	VOIECOD[4]=='0') //////////////ACCE
{
TRAMEOUT[0]=0x20; //TD DEBIT
TRAMEOUT[1]=0x20;
TRAMEOUT[2]='L';
TRAMEOUT[3]='/';
TRAMEOUT[4]='H';
TRAMEOUT[5]=0x20;
}
else
{
TRAMEOUT[0]=0x20; //TA TR PRESSION
TRAMEOUT[1]='m';
TRAMEOUT[2]='b';
TRAMEOUT[3]='a';
TRAMEOUT[4]='r';
TRAMEOUT[5]=0x20;
}
TRAMEENVOITCPDONNEES (6);

//----------------

nchamps=5;
position_init(npage, nchamps);
envoi_cursur();

TRAMEOUT[0]=DISTANCE[0];
TRAMEOUT[1]=DISTANCE[1];
TRAMEOUT[2]=DISTANCE[2];
TRAMEOUT[3]=DISTANCE[3];
TRAMEOUT[4]=DISTANCE[4];
TRAMEENVOITCPDONNEES (5);


nchamps=6;
position_init(npage, nchamps);
envoi_cursur();
if (ETAT[1]=='1')
{
TRAMEOUT[0]='O';
TRAMEOUT[1]='U';
TRAMEOUT[2]='I';
}
else
{
TRAMEOUT[0]='N';
TRAMEOUT[1]='O';
TRAMEOUT[2]='N';
}

TRAMEENVOITCPDONNEES (3);


if (CHG_CODAGE==0)
{
//NUMERO DE CODAGE
	nchamps=1;
	position_init(npage, nchamps);
	envoi_cursur();
}
else
{
	CHG_CODAGE=0;
	nchamps=2;
	position_init(npage, nchamps);
	envoi_cursur();
}

 	do {
		gestion_page();
		if ((CHG_CODAGE==1))
		{
			if (((VOIECOD[3]=='1')&& (VOIECOD[4]>'6'))||((VOIECOD[3]>'1')&& (VOIECOD[4]>='0'))) //EMPECHER CODAGE >16 
			{
				VOIECOD[3]='1';
				VOIECOD[4]='6';
				nchamps=1;
				position_init(npage, nchamps);
				envoi_cursur();
				TRAMEOUT[0]=VOIECOD[3];
				TRAMEOUT[1]=VOIECOD[4];	
				TRAMEENVOITCPDONNEES (2);		
			}
			goto RE_6B;	
		}
	}while ((ETAT1==G) && (repet!=0)&& (STOPMINITEL!=0xFF)); // SI ON SORT DE LA PAGE AEF
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////gestion de la 6eme page du menu //////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gestion_menu6a (void)
{

char t;
char fin; ////MPL

etiq21:

nb_oct=0;
fin =0;   ////MPL




do {
TRAMERECEPTIONminitel();
						if (STOPMINITEL==0xFF)
						goto fin_menu6a;	

                      if ((TRAMEIN[0]>0x2F && TRAMEIN[0]<0x3A) && (decal<max_decalage) ){
                             				saisie();
											if (nb_oct==decal-1 )
											{
											nb_oct++;
											}
											}

//				if (TRAMEIN[0]==0x17)	
//                            {
//					goto etiquet;
//							}
 				else if (TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x43 && decal<max_decalage)//// d�calage a droite 
                             {
							 
                             droite ();              
                             }
				else if (TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x44)//// d�calage a gauche
                             {
							gauche();
							nb_oct--;   ////////22022017
                             }
                else if ((TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x42 ) && (nchamps==2)) ///// MPL debut
				{
					fin = 0xFF;
					tabulation ();
				}           
                if (TRAMEIN[0]==0x17 || (TRAMEIN[0]==0x1B && TRAMEIN[1]!=0x5B ))
                  fin =0xFF;           ///// MPL fin
  
//				else {
//                     goto etiq21;}

			}while ( fin !=0xFF); /////(decal<max_decalage);
										//	enregistrer_donnees();
if (( TRAMEIN[0]==0x1B && TRAMEIN[1]!=0x5B) || (TRAMEIN[0]==0x17 && TRAMEIN[1]==0x46))  ///////21012017
												{
													repet =0;   // TOUCHE ECHAP h1746 recue precedemment  par gestion page
                              						ETAT1=M; 
													goto  fin_menu6a;
												} // RETOUR MENU PRINCIPAL (ETAT1M)	

if ((nb_oct==0 )&&(nchamps==1)) //si aucune saisie alors on reste dans le champ
goto etiq21;



if (nchamps==1){         																	/////aghi
if ((VOIECOD[0]>='1' && VOIECOD[1]!='0') || (VOIECOD[0]=='1' && VOIECOD[2]!='0'))      
{
VOIECOD[0]='1';
VOIECOD[1]='0';
VOIECOD[2]='0';
position_init(npage, nchamps);
envoi_cursur();
TRAMEOUT[0]=VOIECOD[0];
TRAMEOUT[1]=VOIECOD[1];
TRAMEOUT[2]=VOIECOD[2];
TRAMEENVOITCPDONNEES (3);
}
}       																			/////aghi
if (nchamps == 1 && (TRAMEIN[1]!=0x46 || TRAMEIN[1]!=0x5B )) ///// MPL  debut
{





											VOIECOD[3]='0';
											VOIECOD[4]='0';
											CHERCHER_DONNNEES_CABLES(&VOIECOD,111); //CABLE 

											if ((TYPEDECARTE()!='E') && (TYPEDECARTE()!='M')) //
											{

												nchamps=6;
											position_init(npage, nchamps);
											envoi_cursur();						
											TRAMEOUT[0]='A';				
											TRAMEOUT[1]='B';
											TRAMEOUT[2]='S';
											TRAMEOUT[3]='E';
											TRAMEOUT[4]='N';
											TRAMEOUT[5]='T';
											TRAMEOUT[6]='E';
											TRAMEENVOITCPDONNEES (7);
											TEMPO(5);
											goto fin_menu6a;

 




											}	


											if (capt_exist==1) //CABLE EXISTANT
											{	

											nchamps=6;
											position_init(npage, nchamps);
											envoi_cursur();						
											TRAMEOUT[0]='E';				
											TRAMEOUT[1]='X';
											TRAMEOUT[2]='I';
											TRAMEOUT[3]='S';
											TRAMEOUT[4]='T';
											TRAMEOUT[5]='E';
											TRAMEOUT[6]=' ';
											TRAMEENVOITCPDONNEES (7);


											//ROBINET DEJA ENTRE
											ROB[0]=COMMENTAIRE[2];
											ROB[1]=COMMENTAIRE[3];
											nchamps=2;
															position_init(npage, nchamps);	
															envoi_cursur();
															TRAMEOUT[0]=ROB[0];
															TRAMEOUT[1]=ROB[1];
															TRAMEENVOITCPDONNEES (2); //NUMERO DE LA CARTE


											}
											else
											{

											nchamps=6;
											position_init(npage, nchamps);
											envoi_cursur();						
											TRAMEOUT[0]='N';				
											TRAMEOUT[1]='O';
											TRAMEOUT[2]='U';
											TRAMEOUT[3]='V';
											TRAMEOUT[4]='E';
											TRAMEOUT[5]='A';
											TRAMEOUT[6]='U';
											TRAMEENVOITCPDONNEES (7);	
											}

dejaassoc:

nchamps=2;
position_init(npage, nchamps);
envoi_cursur();
fin=0;

goto etiq21;
}             ///// MPL  fin

if (nchamps ==2 && nb_oct==0 &&capt_exist==0)  ///////////////////////////// lundi 20/20/2017
{goto etiq21; }/////////////////////////////////////////////////////////////
etiquet:






//afficher_donnees_si_existe(&tableau);

                                            if ((TRAMEIN[0]==0x17 && TRAMEIN[1]==0x41)||(TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x42 )) ////MPL
										{  ///////////ON REGARDE SI ENTREE=VALIDATION h1741 ET PAS ECHAP BACK ETC
										//VOIECOD[0]=tableau[0];
										//VOIECOD[1]=tableau[1]; //ON SAV LE CABLE
										//VOIECOD[2]=tableau[2];	


															nchamps=2;
															position_init(npage, nchamps);	
															envoi_cursur();
															TRAMEOUT[0]=ROB[0];
															TRAMEOUT[1]=ROB[1];
															TRAMEENVOITCPDONNEES (2); //NUMERO DE LA CARTE
																
															if (gestion_rob()==1) //deja associ�
															goto dejaassoc;	
                                      

															calcul_c_l(); //NUMERO DE LA CARTE ET LIGNE
															nchamps=3;  ///// MPL
															position_init(npage, nchamps);
															envoi_cursur();
															TRAMEOUT[0]=n_carte ;
															TRAMEENVOITCPDONNEES (1); //NUMERO DE LA CARTE
															nchamps=4;   ///// MPL
												            position_init(npage, nchamps);
													        envoi_cursur();
												            TRAMEOUT[0]=n_voie[0];
												            TRAMEOUT[1]=n_voie[1];
															TRAMEENVOITCPDONNEES (2); //NUMERO DE LA VOIE



     
                                            if (capt_exist==1) //CABLE EXISTANT
											{



															//voie_select[0]=tableau[0]; // POUR LA LE NUMERO DU CABLE SAV POUR 6B
															//voie_select[1]=tableau[1];
															//voie_select[2]=tableau[2];
														
															//interv[0]='N';
															//interv[1]='O';
															//interv[2]='N';
														

														
															nchamps=5;  ///// MPL
															position_init(npage, nchamps);
															envoi_cursur();
															for (t=4;t<18;t++) //commencer a 4 rbxx on naffiche pas le robinet
															{
															TRAMEOUT[t-4]=COMMENTAIRE[t];
															}
				
															TRAMEENVOITCPDONNEES (14); //le reste 
											
										




											}
											else //NOUVEAU CABLE
											{

								




											}	




									
											nchamps=5;   ///// MPL
     								        position_init(npage, nchamps);
       										envoi_cursur();
                                          
													




																do { //SI AUTRE ATTENDRE VALIDATION
					         									  gestion_page();
					  									         }while ((ETAT1==F)&& (STOPMINITEL!=0xFF)); //SI VALIDATION ON VA A 6.3 ETAT1 G AEF
					                    }// FIN VALIDATION
                                            else if (( TRAMEIN[0]==0x1B && TRAMEIN[1]!=0x5B) || (TRAMEIN[0]==0x17 && TRAMEIN[1]==0x46))
												{
													repet =0;   // TOUCHE ECHAP h1746 recue precedemment  par gestion page
                              						ETAT1=M; 
												} // RETOUR MENU PRINCIPAL (ETAT1M)
                                     


//Rajouter commentaire et numero Rob au debut du commentaire si modifi� ou pas peut importe

COMMENTAIRE[0]='R';
COMMENTAIRE[1]='o';
COMMENTAIRE[2]=ROB[0];
COMMENTAIRE[3]=ROB[1];




for (t=0;t<20;t++)
{
COMMENTAIRESAV[t]=COMMENTAIRE[t];   //SAV DU CABLE COMMENTAIRE     EN xxx00 SUR LE DEBITMETRE   	
}






fin_menu6a:
delay_qms(10);


}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////envoie des coordonn�es du cursur //////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void envoi_cursur (void)
{

TRAMEOUT[0]=0x1B;
                     TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur avec d�calage a droite, EXEMPLE  1B 5B 34 3B 34 30 48 DEVIEN 1B 5B 34 3B 34 31 48
                     TRAMEOUT[2]=pos_init_cursur [3];
                     TRAMEOUT[3]=pos_init_cursur [2];
                     TRAMEOUT[4]=0x3B;
                     TRAMEOUT[5]=pos_init_cursur [1];
                     TRAMEOUT[6]=pos_init_cursur [0];
                     TRAMEOUT[7]=0x48;

                    TRAMEENVOITCPDONNEES (8);

}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////renvoyer les positions initials du cursur suivant la page//////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



void position_init (char Npage, char Nchamps)
{
decal=0;

               switch (Npage)
{
                           case 'I':
                                     if (Nchamps == 1)
                                     {   
                                        pos_init_cursur [0]='9';////////////// ligne1, colomne 59 chmaps heure et date 
										pos_init_cursur [1]='5';
										pos_init_cursur [2]='1';
                                        pos_init_cursur [3]='0';
 										max_decalage=9;
                                     }
                           
                                     else if  (Nchamps == 2) 
                                     {
                                        pos_init_cursur [0]='4';//////// ligne 20 colomne 34 :mots de passe 
										pos_init_cursur [1]='3';
										pos_init_cursur [2]='0';
                                        pos_init_cursur [3]='2';
 										max_decalage=5;
                                     }             

                           break;
                           case 'M':
                                     if (Nchamps == 1)
                                     {   
                                        pos_init_cursur [0]='9';////////////// ligne1, colomne 59 chmaps heure et date 
										pos_init_cursur [1]='5';
										pos_init_cursur [2]='1';
                                        pos_init_cursur [3]='0';
 										max_decalage=9;
                                     }
                                     else if  (Nchamps == 2) 
                                     {
                                        pos_init_cursur [0]='0';/////ligne 7 colomne 30 nom de site
										pos_init_cursur [1]='3';
										pos_init_cursur [2]='7';
                                        pos_init_cursur [3]='0';
 										max_decalage=40;
                                     } 
                           break;
                           case '1':
									 
                                     if (Nchamps == 1)
                                     {   
                                        pos_init_cursur [0]='2';////////////ligne 4 colonne 2 
										pos_init_cursur [1]='0';
										pos_init_cursur [2]='3';
                                        pos_init_cursur [3]='0';
 										max_decalage=40;
                                     }
                                   
                                        
                           break;

                           case '2':
                                     if (Nchamps == 1)
                                     {   
                                        pos_init_cursur [0]='8';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='8';
                                        pos_init_cursur [3]='0';
 										max_decalage=20;
                                     }
               

                           

                           break;
                           case '3':
                       

                                      if  (Nchamps == 1) ///////num�ro de cable
                                     {
                                        pos_init_cursur [0]='6';
										pos_init_cursur [1]='1';
										pos_init_cursur [2]='2';
                                        pos_init_cursur [3]='0';
 										max_decalage=3;
                                     }                                 
                                     
                                     else if  (Nchamps == 2)  /////////ligne 11 colonne 18 
                                     {
                                        pos_init_cursur [0]='2';
										pos_init_cursur [1]='0';
										pos_init_cursur [2]='7';
                                        pos_init_cursur [3]='0';
 										max_decalage=20;
                                     }                                 
                                  
                             
                           break;








                           case '5':
                                    if (Nchamps == 1)
                                     { 
                                        pos_init_cursur [0]='6';
										pos_init_cursur [1]='3';
										pos_init_cursur [2]='9';
                                        pos_init_cursur [3]='0';
 										max_decalage=20;
                                     }
                                    
                                     
                           break;
                           case '6':

                                    if (Nchamps == 1)
                                     { 
   										pos_init_cursur [0]='9';//1*X
										pos_init_cursur [1]='2';//10*X
										pos_init_cursur [2]='6';// 1*Y
                                        pos_init_cursur [3]='0'; //10*Y
 										max_decalage=20;  
                                     }
                                     else if  (Nchamps == 2) 
                                     {
   										pos_init_cursur [0]='9';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='0';
                                        pos_init_cursur [3]='1';
 										max_decalage=20;
                                     }                                 
                                     else if  (Nchamps == 3) 
                                     {
   										pos_init_cursur [0]='9';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='4';
                                        pos_init_cursur [3]='1';
 										max_decalage=20;
                                     }  
                                     else if  (Nchamps == 4 ) 
                                     {
   										pos_init_cursur [0]='9';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='8';
                                        pos_init_cursur [3]='1';
 										max_decalage=20;
                                     }                                 
                                     else if  (Nchamps == 5) 
                                     {
   										pos_init_cursur [0]='9';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='2';
                                        pos_init_cursur [3]='2';
 										max_decalage=20;
                                     }  

									      else if  (Nchamps == 11) 
                                     {
   										pos_init_cursur [0]='9';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='7';
                                        pos_init_cursur [3]='0';
 										max_decalage=20;
                                     }  


									else if  (Nchamps == 12) 
                                     {
   										pos_init_cursur [0]='9';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='1';
                                        pos_init_cursur [3]='1';
 										max_decalage=20;
                                     }  

									else if  (Nchamps == 13) 
                                     {
   										pos_init_cursur [0]='9';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='5';
                                        pos_init_cursur [3]='1';
 										max_decalage=20;
                                     }  

									else if  (Nchamps == 14) 
                                     {
   										pos_init_cursur [0]='9';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='9';
                                        pos_init_cursur [3]='1';
 										max_decalage=20;
                                     }  
		
									else if  (Nchamps == 15) 
                                     {
   										pos_init_cursur [0]='9';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='3';
                                        pos_init_cursur [3]='2';
 										max_decalage=20;
                                     }  
			


                                    
                           break;
                           case 'a':
 
                                    if (Nchamps == 1)
                                     { 
                                        pos_init_cursur [0]='9';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='6';
                                        pos_init_cursur [3]='0';
 										max_decalage=3;  
                                     }
                                     else if  (Nchamps == 2) 
                                     {
                                        pos_init_cursur [0]='1';  ////MPL
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='7';
                                        pos_init_cursur [3]='0';
 										max_decalage=2; 
                                     }  
                                     else if  (Nchamps == 3) 
                                     {
                                        pos_init_cursur [0]='0';
										pos_init_cursur [1]='6';
										pos_init_cursur [2]='6';
                                        pos_init_cursur [3]='0';
 										max_decalage=1; 
                                     }                                 
                                     else if  (Nchamps == 4) 
                                     {
                                        pos_init_cursur [0]='9';
										pos_init_cursur [1]='5';
										pos_init_cursur [2]='7';
                                        pos_init_cursur [3]='0';
 										max_decalage=2;
                                     }  
                                   else if (Nchamps == 5)
                                     { 
                                        pos_init_cursur [0]='0';
										pos_init_cursur [1]='3';
										pos_init_cursur [2]='9';
                                        pos_init_cursur [3]='0';
 										max_decalage=13;  //COMMENTAIRE 18CARAC
                                     }
                                  //   else if  (Nchamps == 5) 
                                    // {
                                      //  pos_init_cursur [0]='0';
										//pos_init_cursur [1]='3';
										//pos_init_cursur [2]='0';
                                        //pos_init_cursur [3]='1';
 										//max_decalage=3;
                                     //}                                 
                                  
									 else if  (Nchamps == 6) //champ out QUI INDIQUE SI NOUVEAU OU PAS !!!
                                     {
                                        pos_init_cursur [0]='0';
										pos_init_cursur [1]='3';
										pos_init_cursur [2]='8';
                                        pos_init_cursur [3]='0';
 										//max_decalage=3;
                                     }  	









                                    
                           break;
                           case 'b':
if (Nchamps ==1)
                                     { 
                                        pos_init_cursur [0]='9'; //CODAGE
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='2';
                                        pos_init_cursur [3]='1';
 										max_decalage=1;  
                                     }
                                     else if  (Nchamps == 2) 
                                     {
                                        pos_init_cursur [0]='9'; //TYPE DE CAPTEURS
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='3';
                                        pos_init_cursur [3]='1';
 										max_decalage=1;
                                     }                                 
                                     else if  (Nchamps == 3) 
                                     {
                                        pos_init_cursur [0]='9'; //CONSTITUTION
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='4';
                                        pos_init_cursur [3]='1';
 										max_decalage=12;
                                     }  
                                   else if (Nchamps == 4)
                                     { 
                                        pos_init_cursur [0]='9'; //SEUIL
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='5';
                                        pos_init_cursur [3]='1';
 										max_decalage=4;  
                                     }
                                     else if  (Nchamps == 5) 
                                     {
                                        pos_init_cursur [0]='9'; //DISTANCE
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='6';
                                        pos_init_cursur [3]='1';
 										max_decalage=4;
                                     }                                 
                                     else if  (Nchamps == 6) 
                                     {
                                        pos_init_cursur [0]='8'; //INTERVENTION
										pos_init_cursur [1]='6';
										pos_init_cursur [2]='6';
                                        pos_init_cursur [3]='1';
 										max_decalage=2;
                                     }  
   
									  else if  (Nchamps == 7)  //LISTE CAPTEURS EXISTANTS 
                                     {
                                        pos_init_cursur [0]='5';
										pos_init_cursur [1]='1';
										pos_init_cursur [2]='7';
                                        pos_init_cursur [3]='1';
 										//max_decalage=2;
                                     }  			
								
										else if  (Nchamps == 8)  //PATIENTEZ
                                     {
                                        pos_init_cursur [0]='5';
										pos_init_cursur [1]='1';
										pos_init_cursur [2]='8';
                                        pos_init_cursur [3]='1';
 										//max_decalage=2;
                                     }  



                           break;
                              case '8':
                                    
                                      if  (Nchamps == 1) 
                                     {
                                        pos_init_cursur [0]='8';
										pos_init_cursur [1]='3';
										pos_init_cursur [2]='6';
                                        pos_init_cursur [3]='0';
 										max_decalage=30;
                                     }                                 
                                     else if  (Nchamps == 2) 
                                     {
                                        pos_init_cursur [0]='8';
										pos_init_cursur [1]='3';
										pos_init_cursur [2]='0';
                                        pos_init_cursur [3]='1';
 										max_decalage=3;
                                     }  
                                     else if  (Nchamps == 3) 
                                     {
                                        pos_init_cursur [0]='8';
										pos_init_cursur [1]='3';
										pos_init_cursur [2]='2';
                                        pos_init_cursur [3]='1';
 										max_decalage=3;
                                     }                                 
                                   
                                     else if  (Nchamps == 4) 
                                     {
                                        pos_init_cursur [0]='0';
										pos_init_cursur [1]='4';
										pos_init_cursur [2]='8';
                                        pos_init_cursur [3]='1';
 										max_decalage=4;
                                     }                                 
                                     else if  (Nchamps == 5) 
                                     {
                                        pos_init_cursur [0]='0';
										pos_init_cursur [1]='4';
										pos_init_cursur [2]='9';
                                        pos_init_cursur [3]='1';
 										max_decalage=8;
                                     }  
                                     else if  (Nchamps == 6) 
                                     {
                                        pos_init_cursur [0]='6';
										pos_init_cursur [1]='4';
										pos_init_cursur [2]='1';
                                        pos_init_cursur [3]='2';
 										max_decalage=4;
                                     }                                 
                                     else if  (Nchamps == 7) 
                                     {
                                        pos_init_cursur [0]='6';
										pos_init_cursur [1]='4';
										pos_init_cursur [2]='2';
                                        pos_init_cursur [3]='2';
 										max_decalage=4;
                                     }  
                                     else if  (Nchamps == 8) 
                                     {
                                        pos_init_cursur [0]='2';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='4';
                                        pos_init_cursur [3]='1';
 										max_decalage=10;
                                     }     
  									else if  (Nchamps == 9) 
                                     {
                                        pos_init_cursur [0]='4';
										pos_init_cursur [1]='6';
										pos_init_cursur [2]='4';
                                        pos_init_cursur [3]='1';
 										max_decalage=14;
                                     }        
                                     else if  (Nchamps == 10) 
                                     {
                                        pos_init_cursur [0]='5';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='6';
                                        pos_init_cursur [3]='1';
 										max_decalage=12;
                                     }     
  									else if  (Nchamps == 11) 
                                     {
                                        pos_init_cursur [0]='8';
										pos_init_cursur [1]='5';
										pos_init_cursur [2]='6';
                                        pos_init_cursur [3]='1';
 										max_decalage=12;
                                     } 
                                    

                           break;
}
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////reception minitel////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void TRAMERECEPTIONminitel (void)  //GESTION DES TOUCHES 1B, ... 1B 15 7F  NOMBRE D'OCTETS 1 OU 2 OU 3 ??????? 
{
unsigned char u;
unsigned char tmp;
tmp=0;


		u=RECEVOIR_TCP();
        TRAMEIN[tmp]=u;
	                     	 if (u==0x17) //ON CHERCHE A DETECTER ENTREE(h1741)  autres h17xx ???
                            {
           
                             u=RECEVOIR_TCP(); // Si h41 on sort
		                       tmp=tmp+1;
                              TRAMEIN[tmp]=u;
                            }
                      
                           else if (TRAMEIN[0]==0x1B) // h1Bxx  //pour les fl�ches h1B5B 41 ou 42 43 44 fleches haut bas droite gauche
                            {                             
                             u=RECEVOIR_TCP(); //ATTENTE 2IEME TOUCHE 
		                       tmp=tmp+1;
                              TRAMEIN[tmp]=u;
             				if (TRAMEIN[tmp]!=0x5B ) /// AEF SI 2xECHAP (1B)
							{
							goto fin_recep;	
							}
                             u=RECEVOIR_TCP();
		                       tmp=tmp+1;
                              TRAMEIN[tmp]=u;
                            }
                           else if (TRAMEIN[0]==0x0D) // h1Bxx  //pour les fl�ches h1B5B 41 ou 42 43 44 fleches haut bas droite gauche
                            {
                            u=RECEVOIR_TCP();
							if (u=='3') // AEF
							STOPMINITEL=0xFF;

							
                            }
                           else  //les autres cas ????
                            {
                      	   tmp=tmp;
                            }
fin_recep:
		TRAMEIN[tmp]=u;
		tmp=tmp+1;



 
//delay_qms(100);
 
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////gestion de la 6eme page du menu CONFIG (LISTE) //////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gestion_menu6 (void)
{
//kk=0;
nchamps=1;
position_init(npage, nchamps);




 

//do {



TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
TRAMEOUT[2]=pos_init_cursur [3];
TRAMEOUT[3]=pos_init_cursur [2];
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=pos_init_cursur [1];
TRAMEOUT[6]=pos_init_cursur [0];
TRAMEOUT[7]=0x48;


TRAMEENVOITCPDONNEES (resume_voies(1,8));  //ENVOIE CARTE 1

////////////////////ICI SANS DOUTE !!!!!!!!!!!!!!!!!!

nchamps++;



position_init(npage, nchamps);
TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
TRAMEOUT[2]=pos_init_cursur [3];
TRAMEOUT[3]=pos_init_cursur [2];
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=pos_init_cursur [1];
TRAMEOUT[6]=pos_init_cursur [0];
TRAMEOUT[7]=0x48;
TRAMEENVOITCPDONNEES (resume_voies(2,8));  //ENVOIE CARTE 2

nchamps++;



position_init(npage, nchamps);
TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
TRAMEOUT[2]=pos_init_cursur [3];
TRAMEOUT[3]=pos_init_cursur [2];
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=pos_init_cursur [1];
TRAMEOUT[6]=pos_init_cursur [0];
TRAMEOUT[7]=0x48;
TRAMEENVOITCPDONNEES (resume_voies(3,8));  //ENVOIE CARTE 3


nchamps++;


position_init(npage, nchamps);
TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
TRAMEOUT[2]=pos_init_cursur [3];
TRAMEOUT[3]=pos_init_cursur [2];
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=pos_init_cursur [1];
TRAMEOUT[6]=pos_init_cursur [0];
TRAMEOUT[7]=0x48;
TRAMEENVOITCPDONNEES (resume_voies(4,8));  //ENVOIE CARTE 4
nchamps++;


position_init(npage, nchamps);
TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
TRAMEOUT[2]=pos_init_cursur [3];
TRAMEOUT[3]=pos_init_cursur [2];
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=pos_init_cursur [1];
TRAMEOUT[6]=pos_init_cursur [0];
TRAMEOUT[7]=0x48;
TRAMEENVOITCPDONNEES (resume_voies(5,8));  //ENVOIE CARTE 5



 pos_init_cursur [2]= pos_init_cursur [2]+ 1;
                if (pos_init_cursur [2]==0x3A){             ////////mettre a jour la ligne 
                       pos_init_cursur [2]= 0x30;
                       pos_init_cursur [3]=pos_init_cursur [3] + 1;
		
                     } 
//kk++;
//}while (kk<1);
nchamps=1;
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////gestion de la 5eme page du menu GROUPE//////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gestion_menu5 (void)
{

kk=0;
nchamps=1;
position_init(npage, nchamps);


CHERCHER_DONNEES_GROUPE();

do {
 
variables_groupe(kk+1);





TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
TRAMEOUT[2]=pos_init_cursur [3];
TRAMEOUT[3]=pos_init_cursur [2];
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=pos_init_cursur [1];
TRAMEOUT[6]=pos_init_cursur [0];
TRAMEOUT[7]=0x48;



TRAMEOUT[8]=valeur[0];
TRAMEOUT[9]=valeur[1];
TRAMEOUT[10]=valeur[2];
TRAMEOUT[11]=valeur[3];

if (kk==8) //14092020
{
	if (valeur[0]=='1') //1600
	{
		TRAMEOUT[8]=' ';		
		TRAMEOUT[9]='O';
		TRAMEOUT[10]='U';
		TRAMEOUT[11]='I';
	}
	else if (valeur[1]=='9') //900
	{
		TRAMEOUT[8]=' ';		
		TRAMEOUT[9]='N';
		TRAMEOUT[10]='O';
		TRAMEOUT[11]='N';
	}
	else
	{
		TRAMEOUT[8]='?';		
		TRAMEOUT[9]='?';
		TRAMEOUT[10]='?';
		TRAMEOUT[11]='?';
	}
}

if (kk==9)
{
	if (valeur[0]=='1') //1600
	{
		TRAMEOUT[8]=' ';		
		TRAMEOUT[9]='O';
		TRAMEOUT[10]='U';
		TRAMEOUT[11]='I';
	}
	else if (valeur[1]=='9') //900
	{
		TRAMEOUT[8]=' ';		
		TRAMEOUT[9]='N';
		TRAMEOUT[10]='O';
		TRAMEOUT[11]='N';
	}
	else
	{
		TRAMEOUT[8]='?';		
		TRAMEOUT[9]='?';
		TRAMEOUT[10]='?';
		TRAMEOUT[11]='?';
	}
}

TRAMEOUT[12]=0x20;
TRAMEOUT[13]=0x20;
TRAMEOUT[14]=0x20;
TRAMEOUT[15]=0x20;
TRAMEOUT[16]=0x20;



if (((ETAT[0]=='3')||(ETAT[0]=='1')||(ETAT[0]=='2'))&&(ETAT[1]=='0'))
{
ETAT[0]='A';
ETAT[1]='L';
ETAT[2]='.';
ETAT[3]=' ';
}

if (ETAT[1]=='1')
{
ETAT[0]='I';
ETAT[1]='N';
ETAT[2]='T';
ETAT[3]='.';
}

if ((ETAT[0]=='0')&&(ETAT[1]=='0'))
{
ETAT[0]='O';
ETAT[1]='K';
ETAT[2]=' ';
ETAT[3]=' ';
}

if ((ETAT[0]=='4')&&(ETAT[1]=='0'))
{
ETAT[0]='H';
ETAT[1]='.';
ETAT[2]='G';
ETAT[3]='.';
}


if ((ETAT[0]=='5')&&(ETAT[1]=='0'))
{
ETAT[0]='S';
ETAT[1]='.';
ETAT[2]='R';
ETAT[3]='.';
}




TRAMEOUT[17]=ETAT[0];
TRAMEOUT[18]=ETAT[1];
TRAMEOUT[19]=ETAT[2];
TRAMEOUT[20]=ETAT[3];

TRAMEOUT[21]=0x20;
TRAMEOUT[22]=0x20;
TRAMEOUT[23]=0x20;
TRAMEOUT[24]=0x20;
TRAMEOUT[25]=0x20;
TRAMEOUT[26]=0x20;

TRAMEOUT[27]=SEUIL[0];
TRAMEOUT[28]=SEUIL[1];
TRAMEOUT[29]=SEUIL[2];
TRAMEOUT[30]=SEUIL[3];

TRAMEOUT[31]=0x20;
TRAMEOUT[32]=0x20;
TRAMEOUT[33]=0x20;
TRAMEOUT[34]=0x20;
TRAMEOUT[35]=0x20;
TRAMEOUT[36]=0x20;
TRAMEOUT[37]=0x20;

TRAMEOUT[38]=SEUIL2[0];
TRAMEOUT[39]=SEUIL2[1];
TRAMEOUT[40]=SEUIL2[2];
TRAMEOUT[41]=SEUIL2[3];

TRAMEOUT[42]=0x20;
TRAMEOUT[43]=0x20;
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x20;

TRAMEOUT[46]=iiii[0];
TRAMEOUT[47]=iiii[1];
TRAMEOUT[48]=iiii[2];
TRAMEOUT[49]=iiii[1];
TRAMEOUT[50]=iiii[2];


TRAMEENVOITCPDONNEES (51);
 pos_init_cursur [2]= pos_init_cursur [2]+ 1;
                if (pos_init_cursur [2]==0x3A){             ////////mettre a jour la ligne 
                       pos_init_cursur [2]= 0x30;
                       pos_init_cursur [3]=pos_init_cursur [3] + 1;
		
                     } 
kk++;
}while (kk<10);
gestion_page();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////gestion de la 3eme page du menu VISUALTION DES DONNEES D'UN CAPTEUR //////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gestion_menu3 (void)
{
	char nb_oct;
	
	retour_menu3:
	  
	kk=0;
	nchamps=1;
	position_init(npage, nchamps); //16092020
	envoi_cursur();
	TRAMEOUT[0]='?';
	TRAMEOUT[1]='?';
	TRAMEOUT[2]='?';
	TRAMEENVOITCPDONNEES(3);
	envoi_cursur();
	//do {
	//gestion_page();
	//}while (TRAMEIN[0]!=0x17 && TRAMEIN[1]!=0x41 && STOPMINITEL!=0xFF);
	//tableau on range le numero de cable
	//CHERCHER_DONNNEES_CABLES(&tableau,0); //DEBIT
	
	nb_oct=0;

	do {                         /////////ACCE
		TRAMERECEPTIONminitel();
	    if (STOPMINITEL==0xFF)
	      goto fin_menu3; 
		if ( (TRAMEIN[0]==0x17 && TRAMEIN[1]==0x46) || (TRAMEIN[0]==0x1B && TRAMEIN[1]!=0x5B))   /////aghi
			goto fin_menu3; 
	    if ((TRAMEIN[0]>0x2F && TRAMEIN[0]<0x3A) && (decal<max_decalage) ){ // 0 et 9 chiffres
	    	saisie();
	    	if (nb_oct==decal-1)
	    	{
	           nb_oct++;
	        }
	    }
	    else if (TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x43 && decal<max_decalage)//// d�calage a droite 
	    {
	           droite ();              
	    }
	    else if (TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x44)//// d�calage a gauche
	    {
	       gauche();     
	       nb_oct--;    /////////22022017
	    }
		else if (TRAMEIN[0]==0x1B && TRAMEIN[1]!=0x5B) //Si echap voir ancien telgat
		{
			ETAT1=M;
			repet=0;
			goto fin_menu3;
		}	
   }while ((TRAMEIN[0]!=0x17) &&(STOPMINITEL!=0xFF)); //15092020
	//while (TRAMEIN[0]!=0x17 && STOPMINITEL!=0xFF);

	if (STOPMINITEL==0xFF )
		goto fin_menu3;

	if (nb_oct==1){           /////aghi
		tableau[2]=tableau[0];
		tableau[1]='0';
		tableau[0]='0';
	}

	if (nb_oct==2){
		tableau[2]=tableau[1];
		tableau[1]=tableau[0];
		tableau[0]='0';
	}
	
	if (nb_oct==3){
		tableau[0]=tableau[0];
		tableau[1]=tableau[1];
		tableau[2]=tableau[2];
	}                    /////aghi
	
	
	//16092020
	if (((tableau[0]>='1')&&((tableau[1]>'0')||(tableau[2]>'0'))||(tableau[0]>'1'))) //16092020
	{
		tableau[0]='1';
		tableau[1]='0';
		tableau[2]='0';
	}
	//nchamps=2;
	//position_init(npage, nchamps);
	//debit[0]= 'D'; //CODAGE 0
	//debit[1]= 'D';
	//debit[2]= 'D';
	//debit[3]= 'D';
	//TRAMEOUT[0]=0x1B;
	//TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
	//TRAMEOUT[2]=pos_init_cursur [3];
	//TRAMEOUT[3]=pos_init_cursur [2];
	//TRAMEOUT[4]=0x3B;
	//TRAMEOUT[5]=pos_init_cursur [1];
	//TRAMEOUT[6]=pos_init_cursur [0];
	//TRAMEOUT[7]=0x48;
	//TRAMEOUT[8]=debit[0];
	//TRAMEOUT[9]=debit[1];
	//TRAMEOUT[10]=debit[2];
	//TRAMEOUT[11]=debit[3];
	//TRAMEENVOITCPDONNEES (12);
	if (TRAMEIN[0]==0x17 && TRAMEIN[1]==0x41 )
	{
	
		pos_init_cursur [0]='2'; //16092020
		pos_init_cursur [1]='0';
		pos_init_cursur [2]='3';
		pos_init_cursur [3]='0';
		envoi_cursur();
		TRAMEOUT[0]='C';
		TRAMEOUT[1]='A';
		TRAMEOUT[2]='B';
		TRAMEOUT[3]='L';
		TRAMEOUT[4]='E';
		TRAMEOUT[5]=':';
		TRAMEOUT[6]=' ';
		TRAMEOUT[7]=tableau[0];
		TRAMEOUT[8]=tableau[1];
		TRAMEOUT[9]=tableau[2];
		TRAMEENVOITCPDONNEES(10);
	
		nchamps=2;
		position_init(npage, nchamps);
		
		do {
			CHERCHER_DONNNEES_CABLES(&tableau,kk); //DEBIT
			if (((ETAT[0]=='3')||(ETAT[0]=='1')||(ETAT[0]=='2'))&&(ETAT[1]=='0')){ //22092020
				ETAT[0]='A';
				ETAT[1]='L';
				ETAT[2]='.';
				ETAT[3]=' ';
			}
			if (ETAT[1]=='1'){
				ETAT[0]='I';
				ETAT[1]='N';
				ETAT[2]='T';
				ETAT[3]='.';
			}
			if ((ETAT[0]=='0')&&(ETAT[1]=='0')){
				ETAT[0]='O';
				ETAT[1]='K';
				ETAT[2]=' ';
				ETAT[3]=' ';
			}
			if ((ETAT[0]=='4')&&(ETAT[1]=='0')){
				ETAT[0]='H';
				ETAT[1]='.';
				ETAT[2]='G';
				ETAT[3]='.';
			}
			if ((ETAT[0]=='5')&&(ETAT[1]=='0')){
				ETAT[0]='S';
				ETAT[1]='.';
				ETAT[2]='R';
				ETAT[3]='.';
			}
			TRAMEOUT[0]=0x1B;
			TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
			TRAMEOUT[2]=pos_init_cursur [3];
			TRAMEOUT[3]=pos_init_cursur [2];
			TRAMEOUT[4]=0x3B;
			TRAMEOUT[5]=pos_init_cursur [1];
			TRAMEOUT[6]=pos_init_cursur [0];
			TRAMEOUT[7]=0x48;
			//16092020					
			if (capt_exist==1)
			{					
				TRAMEOUT[8]=0x20;				
				TRAMEOUT[9]=COD[0];
				TRAMEOUT[10]=COD[1];	
				TRAMEOUT[11]=0x20;
				TRAMEOUT[12]=0x20;
				TRAMEOUT[13]=0x20;
				TRAMEOUT[14]=0x20;
				TRAMEOUT[15]=CCON[0];
				TRAMEOUT[16]=CCON[1];
				TRAMEOUT[17]=CCON[2];
				TRAMEOUT[18]=',';
				TRAMEOUT[19]=CCON[3];
				TRAMEOUT[20]=0x20;
				TRAMEOUT[21]=0x20;
				TRAMEOUT[22]=0x20;
				TRAMEOUT[23]=0x20;
				TRAMEOUT[24]=0x20;
				TRAMEOUT[25]=CMOD[0];
				TRAMEOUT[26]=CMOD[1];
				TRAMEOUT[27]=CMOD[2];
				TRAMEOUT[28]=',';
				TRAMEOUT[29]=CMOD[3];
				TRAMEOUT[30]=0x20;
				TRAMEOUT[31]=0x20;
				TRAMEOUT[32]=0x20;
				TRAMEOUT[33]=0x20;
				TRAMEOUT[34]=0x20;
				TRAMEOUT[35]=CREP[0];
				TRAMEOUT[36]=CREP[1];
				TRAMEOUT[37]=CREP[2];
				TRAMEOUT[38]=CREP[3];
				TRAMEOUT[39]=0x20;
				TRAMEOUT[40]=0x20;
				TRAMEOUT[41]=0x20;
				TRAMEOUT[42]=0x20;
				TRAMEOUT[43]=VALEUR[0];
				TRAMEOUT[44]=VALEUR[1];
				TRAMEOUT[45]=VALEUR[2];
				TRAMEOUT[46]=VALEUR[3];
				TRAMEOUT[47]=0x20;
				TRAMEOUT[48]=0x20;
				TRAMEOUT[49]=0x20;
				TRAMEOUT[50]=0x20;
				TRAMEOUT[51]=0x20;
				TRAMEOUT[52]=SEUIL[0];
				TRAMEOUT[53]=SEUIL[1];
				TRAMEOUT[54]=SEUIL[2];
				TRAMEOUT[55]=SEUIL[3];
				TRAMEOUT[56]=0x20;
				TRAMEOUT[57]=0x20;
				TRAMEOUT[58]=0x20;
				TRAMEOUT[59]=ETAT[0]; //22092020
				TRAMEOUT[60]=ETAT[1];
				TRAMEOUT[61]=ETAT[2];
				TRAMEOUT[62]=ETAT[3];
				TRAMEOUT[63]=0x20;
				TRAMEOUT[64]=0x20;
				TRAMEOUT[65]=0x20;
				TRAMEOUT[66]=DISTANCE[0];
				TRAMEOUT[67]=DISTANCE[1];
				TRAMEOUT[68]=DISTANCE[2];
				TRAMEOUT[69]=DISTANCE[3];
				TRAMEOUT[70]=DISTANCE[4];
				TRAMEOUT[71]=0x20;
				TRAMEOUT[72]=0x20;
				TRAMEOUT[73]=CONSTITUTION[0];
				TRAMEOUT[74]=CONSTITUTION[1];
				TRAMEOUT[75]=CONSTITUTION[2];
				TRAMEOUT[76]=CONSTITUTION[3];
				TRAMEOUT[77]=CONSTITUTION[4];
				TRAMEOUT[78]=CONSTITUTION[5];
				TRAMEOUT[79]=CONSTITUTION[6];
				TRAMEOUT[80]=CONSTITUTION[7];
				TRAMEOUT[81]=CONSTITUTION[8];
				TRAMEOUT[82]=CONSTITUTION[9];
				TRAMEOUT[83]=CONSTITUTION[10];
				TRAMEOUT[84]=CONSTITUTION[11];
				TRAMEOUT[85]=CONSTITUTION[12];
				TRAMEOUT[86]=0x20;
			}
			else
			{
				for (nb_oct=8;nb_oct<=86;nb_oct++)
					TRAMEOUT[nb_oct]=' ';
			}
			TRAMEOUT[87]=0x20;
			TRAMEOUT[88]=0x20;
			TRAMEENVOITCPDONNEES (89);
			pos_init_cursur [2]= pos_init_cursur [2]+ 1;
			if (pos_init_cursur [2]==0x3A)
			{             ////////mettre a jour la ligne 
				pos_init_cursur [2]= 0x30;
				pos_init_cursur [3]=pos_init_cursur [3] + 1;
			} 
			kk++; 
		}while (kk<=16);  //tous les capteurs 
		//gestion_page();
		goto retour_menu3;
	}

	fin_menu3:
	delay_qms(10);
	repet =0;   // TOUCHE ECHAP h1746 recue precedemment  par gestion page
	ETAT1=M;

}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////gestion de la 2eme page du menu ALARMES EN COURS//////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gestion_menu2 (void)
{
kk=0;
position_init(npage, nchamps);
analyser_les_alarmes_en_cours(); //ANALYSE LES CABLES ET LES ETATS POPUR L'INSTANT


nom_site (); //CORRECTION POUR IP


TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
pos_init_cursur [3]='1';
pos_init_cursur [2]='6';

TRAMEOUT[2]=pos_init_cursur [3];
TRAMEOUT[3]=pos_init_cursur [2];
TRAMEOUT[4]=0x3B;
pos_init_cursur [1]='0';
pos_init_cursur [0]='2';
TRAMEOUT[5]=pos_init_cursur [1];
TRAMEOUT[6]=pos_init_cursur [0];
TRAMEOUT[7]=0x48;




TRAMEOUT[8]='I';
TRAMEOUT[9]='N';
TRAMEOUT[10]='C';
TRAMEOUT[11]=':';
TRAMEOUT[12]=' ';
IntToChar(ET_I[0],TMP,2);
TRAMEOUT[13]=TMP[0];
TRAMEOUT[14]=TMP[1];
TRAMEOUT[15]=' '; 
TRAMEOUT[16]='C';
TRAMEOUT[17]='C';
TRAMEOUT[18]=':';
TRAMEOUT[19]=' ';
IntToChar(ET_C[0],TMP,2);
TRAMEOUT[20]=TMP[0];
TRAMEOUT[21]=TMP[1];
TRAMEOUT[22]=' ';
TRAMEOUT[23]='v';
TRAMEOUT[24]='F';
TRAMEOUT[25]='U';
TRAMEOUT[26]='S';
TRAMEOUT[27]=':';
TRAMEOUT[28]=' ';
IntToChar(ET_F[0],TMP,2);
TRAMEOUT[29]=TMP[0];
TRAMEOUT[30]=TMP[1];

LIRE_EEPROM(6,256,&TMP,3); // POUR RECUPERER INFOS AUTORESETRI
TRAMEOUT[31]=0x20;
TRAMEOUT[32]=0x20;
TRAMEOUT[33]=0x20;
TRAMEOUT[34]=0x20;
TRAMEOUT[35]=0x20;
TRAMEOUT[36]=0x20;
TRAMEOUT[37]=0x20;
TRAMEOUT[38]=0x20;
TRAMEOUT[39]=0x20;
TRAMEOUT[40]=0x20;
TRAMEOUT[41]=0x20;
TRAMEOUT[42]=0x20;
TRAMEOUT[43]=0x20;
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x20;
TRAMEOUT[46]=0x20;
TRAMEOUT[47]=0x20;
TRAMEOUT[48]=0x20;
TRAMEOUT[49]=0x20;
TRAMEOUT[50]=0x20;
TRAMEOUT[51]=0x20;
TRAMEOUT[52]=0x20;
TRAMEOUT[53]=0x20;
TRAMEOUT[54]=0x20;
TRAMEOUT[55]=0x20;
TRAMEOUT[56]=0x20;
TRAMEOUT[57]=0x20;
TRAMEOUT[58]=0x20;
TRAMEOUT[59]=0x20;
TRAMEOUT[60]=0x20;
TRAMEOUT[61]=TMP[97];// de 0 a 99
TRAMEOUT[62]=TMP[98];
TRAMEOUT[63]=TMP[99];// de 0 a 99
TRAMEOUT[64]=TMP[100];
TRAMEOUT[65]=TMP[101];// de 0 a 99
TRAMEOUT[66]=TMP[102];
TRAMEOUT[67]=TMP[103]; // de 0 a 99 
TRAMEOUT[68]=TMP[104];
TRAMEOUT[69]=TMP[105];//de 0 a 99 
TRAMEOUT[70]=TMP[106];
TRAMEOUT[71]=TMP[107];//de 0 a 99 
TRAMEOUT[72]=TMP[108];
TRAMEENVOITCPDONNEES (73);



nom_site (); //CORRECTION POUR IP

pos_init_cursur[3]='8';
pos_init_cursur[2]='2';
pos_init_cursur[1]='8';
pos_init_cursur[0]='0';

do
{



//LIMITE A 99 ALARMES

//aadress[0]='x';
//aadress[1]='x';
//agroup[0]='x';
//agroup[1]='x';
//aresis[0]='x';
//aresis[1]='x';
TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
TRAMEOUT[2]=pos_init_cursur [0];
TRAMEOUT[3]=pos_init_cursur [1];
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=pos_init_cursur [2];
TRAMEOUT[6]=pos_init_cursur [3];
TRAMEOUT[7]=0x48;

IntToChar(ET_GR[kk],TMP,2);
TRAMEOUT[8]=TMP[0]; //GROUPE
TRAMEOUT[9]=TMP[1];
TRAMEOUT[10]=0x20;
TRAMEOUT[11]=0x20;
TRAMEOUT[12]=0x20;
TRAMEOUT[13]='!';
TRAMEOUT[14]=0x20;
TRAMEOUT[15]=0x20;
TRAMEOUT[16]=0x20;
TRAMEOUT[17]=0x20;
TRAMEOUT[18]=0x20;
//TRAMEOUT[19]=0x20;
//TRAMEOUT[20]=0x20;
//TRAMEOUT[21]=0x20;
//TRAMEOUT[22]=0x20;
//TRAMEOUT[23]=0x20;


IntToChar(ET_TP[kk],TMP,2);
TRAMEOUT[19]=TMP[0]; //TA
TRAMEOUT[20]=TMP[1];
TRAMEOUT[21]=0x20;
TRAMEOUT[22]=0x20;
TRAMEOUT[23]=0x20;
TRAMEOUT[24]=0x20;
TRAMEOUT[25]=0x20;
TRAMEOUT[26]=0x20;
TRAMEOUT[27]=0x20;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]='!';
TRAMEOUT[30]=0x20;
TRAMEOUT[31]=0x20;
TRAMEOUT[32]=0x20;
TRAMEOUT[33]=0x20;
TRAMEOUT[34]=0x20;
TRAMEOUT[35]=0x20;




IntToChar(ET_TR[kk],TMP,2);
TRAMEOUT[36]=TMP[0]; //TR
TRAMEOUT[37]=TMP[1];


TRAMEOUT[38]=0x20;
TRAMEOUT[39]=0x20;
TRAMEOUT[40]=0x20;
TRAMEOUT[41]=0x20;
TRAMEOUT[42]=0x20;
TRAMEOUT[43]='!';
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x20;
IntToChar(ET_TD[kk],TMP,2);
TRAMEOUT[46]=TMP[0];
TRAMEOUT[47]=TMP[1];
TRAMEOUT[48]=0x20;
TRAMEOUT[49]=0x20;
TRAMEOUT[50]='!';


TRAMEENVOITCPDONNEES (51);


 pos_init_cursur [1]= pos_init_cursur [1]+ 2; //ligne suivante
                if (pos_init_cursur [1]==0x3A){             ////////mettre a jour la ligne 
                       pos_init_cursur [0]= 0x31;
                       pos_init_cursur [1]=0x30;
		
                     }  



kk++;                                
}while (kk<4); //LES 4 x 3 champs



TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
TRAMEOUT[2]='1';
TRAMEOUT[3]='5';
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]='0';
TRAMEOUT[6]='2';
TRAMEOUT[7]=0x48;
for (kk=8;kk<121;kk++)
{
TRAMEOUT[kk]=0x2D;
}
TRAMEOUT[121]='*';
TRAMEENVOITCPDONNEES (77);

gestion_page();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////gestion de la 1ere page du menu //////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gestion_menu1 (void)
{
char tutu;
kk=1;
position_init(npage, nchamps);

do
{


						calculBLOC=7168+(kk-1)*128;
					
LIRE_EEPROM(6,calculBLOC, &TRAMEOUT, 7); //LIRE 85caract
 

TRAMEOUT[8]='-';
TRAMEOUT[9]='-';

TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
TRAMEOUT[2]=pos_init_cursur [3];
TRAMEOUT[3]=pos_init_cursur [2];
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=pos_init_cursur [1];
TRAMEOUT[6]=pos_init_cursur [0];
TRAMEOUT[7]=0x48;
delay_qms(300);
TRAMEENVOITCPDONNEES (82); //ON ENVOIT
 pos_init_cursur [2]= pos_init_cursur [2]+ 1;
                if (pos_init_cursur [2]==0x3A){             ////////mettre a jour la ligne 
                       pos_init_cursur [2]= 0x30;
                       pos_init_cursur [3]=pos_init_cursur [3] + 1;
		
                     }  




kk++;                                
}while (kk<22);


gestion_page();
}


void TRAMERECEPTIONTCPDONNEES (int val2) 
{
unsigned char u;
unsigned char tmp;
tmp=0;
TXSTA1bits.TXEN =0;
RCSTA1bits.CREN =1;

do
{
ret1:
		u=RECEVOIR_TCP();
		//if ((u!='=') && (tmp==0))
		//goto ret1;
	
		TRAMEIN[tmp]=u;
		tmp=tmp+1;
}
while ((STOPMINITEL!=0xFF)&&(u!=0x0D) && (u!='N') && (u!=0x06) && (u!=0x15) && (u!=0x0F)&&(tmp<val2)); // <CR> ou 'N' (ICOG:N ou IMIN) ou <ACK> ou <NOTACK> ou <NOTACK autre> ou nombre de car. maaxi


TXSTA1bits.TXEN =1;
RCSTA1bits.CREN =0;

if (STOPMINITEL==0xFF)
delay_qms(100);



//delay_qms(100);
 
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////// VERIFICATION DU MOT DE PASSE///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int verifMP( void )    ///////v�rification du mots de passe
{
	int k;
	char erreurMPP;
	char bValid;
	bValid=0;
	erreurMPP=0;
	k=0;//22092020
DEBMP:
	
	//TRAMERECEPTIONRS232DONNEES (2);
	do{		
		TRAMERECEPTIONTCPDONNEES (1); //ON ATTEND LE CARACTERE
		if ((TRAMEIN[0]>=0x80)||(TRAMEIN[0]==0x00)){} //Suppression de l'ASCII �tendu
		else if (TRAMEIN[0]==0x17){
			erreurMPP=1;
		}
		else if ((TRAMEIN[0]==0x41) && (erreurMPP==1)){
			erreurMPP=0;
		}
		else if (TRAMEIN[0]== 0x0D) {}//On ignore les "<CR>"
		else
		{	
			tableau1[k] = TRAMEIN[0];
			TRAMEOUT[0]='*';
			TRAMEENVOITCPDONNEES (1); //ON ENVOIT '*'
			k++;
		}
	}while ( k<4); //17062019 <4
/*	bValid=0;
	TRAMERECEPTIONTCPDONNEES (1); //VALIDATION
	if (TRAMEIN[0]==0x17){
		TRAMERECEPTIONTCPDONNEES (1); //VALIDATION
		if(TRAMEIN[0]==0x41)
			bValid=1;
	}*/
	accord = 0; //FAUX
	if (tableau1[1]== acc_u[1] && tableau1[2]== acc_u[2] && tableau1[3]== acc_u[3] && tableau1[0]== acc_u[0]/* && ((bValid==1)||(TRAMEIN[0]== 0x0D))*/) 
	{
		accord =2; //UTILISATEUR
	} 

	if (tableau1[1]== acc_conf[1] && tableau1[2]== acc_conf[2] && tableau1[3]== acc_conf[3] && tableau1[0]== acc_conf[0]/* && ((bValid==1)||(TRAMEIN[0]== 0x0D))*/)  
	{
		accord =1; //ADMINISTRATEUR
	} 

	return accord ; 
}



















 
 


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////// nom de site ///////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void nom_site ( void)
{
	char i;
	char k;
	char u;
	for(i=29;i>0;i--){
		if(ID_rack[i]!=0x20)
			break;
	}

	k = 39 - i/2; //40 (milieu de minitel) - 1 car affichage du site avec un blank
	u = k/10;
	k = k - u*10;

if (ETAT1==I)                 ////////////////// emplacement du nom de site sur la page mots de passe 
{
TRAMEOUT[0]=0x1B; 
TRAMEOUT[1]=0x5B;
TRAMEOUT[2]=0x31;
TRAMEOUT[3]=0x32;
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=u+48;
TRAMEOUT[6]=k+48;
TRAMEOUT[7]=0x48;
}
else                             ////////////////// emplacement du nom de site sur les autres pages 
{
                                               
TRAMEOUT[0]=0x1B; 
TRAMEOUT[1]=0x5B;
TRAMEOUT[2]=0x30;
TRAMEOUT[3]=0x32;
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=u+48;
TRAMEOUT[6]=k+48;
TRAMEOUT[7]=0x48;
}


//NOM DU SITE 
TRAMEOUT[8]=0x1B ;
TRAMEOUT[9]=0x5B ;
TRAMEOUT[10]=0x37 ; //SURLIGNER EN BLANC
TRAMEOUT[11]=0x6D ;
TRAMEOUT[12]=0x20 ;
for(k=0;k<=i;k++)
	TRAMEOUT[k+13]=ID_rack[k];

TRAMEOUT[i+14]=0x20;
TRAMEOUT[i+15]=0x1B ;
TRAMEOUT[i+16]=0x5B ;
TRAMEOUT[i+17]=0x30 ; //NON SURLIGNER EN BLANC
TRAMEOUT[i+18]=0x6D ;
TRAMEOUT[i+19]=0x20 ;

					TRAMEENVOITCPDONNEES (i+20);
if (ETAT1==I)                ////// mettre le cursur dans la case mot de passe 
{
TRAMEOUT[0]=0x1B; 
TRAMEOUT[1]=0x5B ;
TRAMEOUT[2]=0x32;
TRAMEOUT[3]=0x30;
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=0x33 ;
TRAMEOUT[6]=0x39 ;//05102020
TRAMEOUT[7]=0x48 ;
TRAMEENVOITCPDONNEES (8);
}

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////gestion de cursur /////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gestion_page (void)
{
/////////////// reception

                           TRAMERECEPTIONminitel();
						if (STOPMINITEL==0xFF)
						goto fin_gestion;	

						//	if ((nchamps==2) && (npage=='b'))
						//	CHG_CODAGE=0; //REINIT POUR 6B 






						    if (npage=='b' && nchamps==2 &&((TRAMEIN[0]!='A' || TRAMEIN[0]!='a') || (TRAMEIN[0]=='R' || TRAMEIN[0]=='r')))
							{
								if (VOIECOD[3]=='0' && VOIECOD[4]=='0' )       ///////////////////////////// lundi 20/20/2017
                                {
               						goto capt;
								}
								else {
								 if ((TRAMEIN[0]=='A' || TRAMEIN[0]=='a') && (TYPEDECARTE()=='E'))
								{
									type_capteur[0]='T';
									type_capteur[1]='P';
									type_capteur[2]=0X20;
									type_capteur[3]='A';
									type_capteur[4]='D';
									type_capteur[5]='R';
									type_capteur[6]='E';
									type_capteur[7]='S';
									type_capteur[8]='S';
									type_capteur[9]='A';
									type_capteur[10]='B';
									type_capteur[11]='L';
									type_capteur[12]='E';
									identifiant=1;	
									enregistrer_donnees();
																
								}
								else if ((TRAMEIN[0]=='R' || TRAMEIN[0]=='r')&&(TYPEDECARTE()=='M')) //TEST DU FLAG TYPEDECARTE
								{
									type_capteur[0]='T';
									type_capteur[1]='P';
									type_capteur[2]=0X20;
									type_capteur[3]='R';
									type_capteur[4]='E';
									type_capteur[5]='S';
									type_capteur[6]='I';
									type_capteur[7]='S';
									type_capteur[8]='T';
									type_capteur[9]='I';
									type_capteur[10]='F';
									type_capteur[11]=0X20;
									type_capteur[12]=0X20;
									identifiant=2;		
									enregistrer_donnees();
							}
								}
capt:
									position_init(npage, nchamps);
									envoi_cursur();
									TRAMEOUT[0]=type_capteur[0];
									TRAMEOUT[1]=type_capteur[1];
									TRAMEOUT[2]=type_capteur[2];
									TRAMEOUT[3]=type_capteur[3];
									TRAMEOUT[4]=type_capteur[4];
									TRAMEOUT[5]=type_capteur[5];
									TRAMEOUT[6]=type_capteur[6];
									TRAMEOUT[7]=type_capteur[7];
									TRAMEOUT[8]=type_capteur[8];
									TRAMEOUT[9]=type_capteur[9];
									TRAMEOUT[10]=type_capteur[10];
									TRAMEOUT[11]=type_capteur[11];
									TRAMEOUT[12]=type_capteur[12];
									TRAMEENVOITCPDONNEES (13);
									position_init(npage, nchamps);
									envoi_cursur();
							}		

						 //AUTORISATION MODIF PARAM if (npage=='8' && (nchamps==2 || nchamps==3) || (npage=='b' && nchamps==6  )) //Si page 8 et champ 2 ou 3 Identitification du Rack et transmission des alarmes ou page 6b champ 6 
						   if  (npage=='b' && nchamps==6  ) //page 6b champ 6 // SANS AUTORISATION


								{
                         		 if ( TRAMEIN[0]=='O' || TRAMEIN[0]=='o' )   // OUI
                              			{
                            
										position_init(npage, nchamps);
										envoi_cursur();
                                       
                                        TRAMEOUT[0]='O';
                                        TRAMEOUT[1]='U';
                                        TRAMEOUT[2]='I';
										identifiant=1; // POUR DIRE OUI OU NON
										enregistrer_donnees();
                                        TRAMEENVOITCPDONNEES (3);
                            			 }  
		    						if ( TRAMEIN[0]=='N' || TRAMEIN[0]=='n' ) //NON  
										{
                                		position_init(npage, nchamps);
										envoi_cursur();
                                        TRAMEOUT[0]='N';
                                        TRAMEOUT[1]='O';
                                        TRAMEOUT[2]='N';
										identifiant=2;
										enregistrer_donnees();
                                       	TRAMEENVOITCPDONNEES (3);
										}	
                                       
							}


           				   if (( TRAMEIN[0]==0x1B && TRAMEIN[1]!=0x5B) || (TRAMEIN[0]==0x17 && TRAMEIN[1]==0x46))   ////////////////echape et  revenir au menu principale de nimporte ou  ////// s
                              {
                              repet =0;
                              ETAT1=M;
                              }
 
							if (TRAMEIN[0]==0x2D) //EFFACEMENT CAPTEUR (-)
							{
								if (npage=='b' && nchamps==1)
								{
											sup_capteur();		///////////// supression capteur 
										//	tmp=EFFACER_CAPTEUR();
											

		
							}
							}



		                   else if (( (TRAMEIN[0]>0x40 && TRAMEIN[0]<0x5B) || (TRAMEIN[0]>0x60 && TRAMEIN[0]<0x7B) || (TRAMEIN[0]>0x2F && TRAMEIN[0]<0x3A) || TRAMEIN [0]==0x20) ) // si lettres MAJUS. ou Lettres MINUSC. ou LES LES CHIFFRES ou ESPACE
                             {
								//interdire ecriture menu 8	if ((npage=='a' && nchamps==4) || (npage=='b' && ((nchamps<6 && nchamps>2)||(nchamps==1))) ||(npage=='8' && nchamps!=2 && nchamps!=3  )) // page 6b et pas le champ intervention ou page8 et pas alarme (OUI/NON) et page6b et pas le champ type de capteur
								if ((npage=='a' && nchamps==5) || (npage=='b' && nchamps==3 )) ///MPL //// 01022017 // interdire ecriture menu8 page 6b et pas le champ intervention et page6b et pas le champ type de capteur  ////ACCE ///////////////////////////// lundi 20/20/2017
             					
									{
										 if (npage == '8' && (nchamps == 4 || nchamps == 5) && (decal ==1 || decal ==3) ) //Si heure et date de la page 8
							 			{
								 
								 			pos_init_cursur [0]= pos_init_cursur [0]+ 1;
                							if (pos_init_cursur [0]==0x3A) // incrementation position a cause des : / : / dans date et heure page 8
											{
                       							pos_init_cursur [0]= 0x30;
                     							pos_init_cursur [1]=pos_init_cursur [1] + 1;
												envoi_cursur(); 
							            	}
										}
									    // if ((TRAMEIN[0]>0x2F && TRAMEIN[0]<0x3A) && (decal<max_decalage) )
							 			if (decal<=max_decalage) // tant que on a pas termin� le champ
										{
                             				saisie();
										}

									}
                       			 	if ( (TRAMEIN[0]>0x2F && TRAMEIN[0]<0x3A )&& ((npage=='3' )|| (npage=='b' && (nchamps == 1 || nchamps == 5 || nchamps == 4 )))) // 01022017//si uniquement champ avec des chiffres (page 3)
									{
                                   		saisie();
                                	}
                             }
                     



   
          				 // interdire ecriture menu 8 else if ((TRAMEIN[0]==0x09 ) || (TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x42 )) // si tabulation ou en bas
                           	else if (((TRAMEIN[0]==0x09 ) || (TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x42 )) && (npage!='a'))  //interdire ecriture menu8 si tabulation ou en bas ///ACCE ///page 6a interdir ld�placement d'un champ a un autre
                       
							{



             //  INTERDIRE ECRITURE MENU8            		if ((npage=='b')||(npage=='8'))
                               if (npage=='b')   //  INTERDIRE ECRITURE MENU8
								{
              // 			     		tabulation:
                //                       if (npage == '8' && nchamps ==4  ) //CHAMP HEURE
        		//						{
				//						verif_heure_date(); //VERIFICATION DE lA DATE
				//						}
                  //                     if (npage == '8' && nchamps ==5) //CHAMP DATE
        			//					{
					//					verif_heure_date(); //VERIFICATION DE LA DATE
					//					}
                           		 tabulation ();
                             	}

								if ((npage=='b') && (nchamps ==2)) 	//ON A FORCEMENT INCREMENTE AU DESSUS	
								{
								CHG_CODAGE=1;
								}

							TRAMEIN[0]==0x00; TRAMEIN[1]==0x00; TRAMEIN[2]==0x00;   //////AEF


							}

							else if (	TRAMEIN[0]==0x08)     //////////////22022017

              				{
								TRAMEOUT[0]=0x08;
								TRAMEENVOITCPDONNEES (1);
								decal--;
							}


																													
							else if (((TRAMEIN[0]==0x17) && (TRAMEIN[1]==0x41))) ////////////////validation et retour vers le menu principal
							{
								if (npage=='b' && nchamps==1) // PAS SUR LE CODAGE
								{}
								else{	
									if (npage == 'a') //PASSER DE 6a a 6b
									{
										ETAT1=G;	
										repet =0;
									}
									else
									{			
										if ((npage=='b') && (nchamps!=1)) //SI  on valide un capteur sauf pour le champ codage
										{
											tmp=VALIDER_CAPTEUR();
											repet=0;
											//ETAT1 = M;
										}
										else{                   		
											ETAT1 = M;
										}									//VALIDATION		
									}
								}	
							}

                           else if (TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x43 )//// d�calage a droite 
                             {
									//autoriser ecriture menu 8 if ((npage=='a') || (npage=='b')||(npage=='8'))
										if ((npage=='a') || (npage=='b'))//interdire ecriture menu 8
										
										{ //Uniquement pr ces pages on valide laction de la touche DROITE
																	 //decal ++;
										                             droite ();
																	
										} 
								TRAMEIN[0]==0x00; TRAMEIN[1]==0x00; TRAMEIN[2]==0x00;         ////// AEF               
                             }
                           else if (TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x41 )//// d�calage a haut 
                             {
							 //autoriser ecriture menu 8 if ((npage=='a') || ((npage=='b')&& (nchamps!=1))||(npage=='8'))
							 if  ((npage=='b')&& (nchamps!=1))////interdire ecriture menu 8
							{
                             haut ();
      						  }  
						
							
								TRAMEIN[0]==0x00; TRAMEIN[1]==0x00; TRAMEIN[2]==0x00;   ////// AEF
              
                             }
                            
                             
                             else if (TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x44)//// d�calage a gauche
                             {
							//autoriser ecriture menu 8 if ((npage=='a') || (npage=='b')||(npage=='8'))
								if ((npage=='a') || (npage=='b'))//autoriser ecriture menu 8
								  	{
										gauche();
							
                           			  }
								TRAMEIN[0]==0x00; TRAMEIN[1]==0x00; TRAMEIN[2]==0x00;    ////// AEF
							}



fin_gestion:
delay_qms(1);
                             
} 

void TRAMEENVOITCPDONNEES (int val)
{

//delay_qms(100);
//TRAMEENVOIRS485DONNEES();
StackTask();
TCPPutArray(MySocket, TRAMEOUT, val); //JUSQUA LA LONGUEUR DU
wMaxGet = TCPIsGetReady(MySocket);	// Get TCP RX FIFO byte count
wMaxPut = TCPIsPutReady(MySocket);
}


void LIRE_HEURE_MINITEL(void)
{
 

I2CHORLG=0;  	 	
creer_trame_horloge ();  //VERIFICATION JJMMsshhmmAA
I2CHORLG=1;

//FORCAGE DATE DERNIER CYCLE
Tab_Horodatage[6]=HORLOGET[0];
Tab_Horodatage[7]=HORLOGET[1];
Tab_Horodatage[8]=HORLOGET[2];
Tab_Horodatage[9]=HORLOGET[3];
Tab_Horodatage[4]=HORLOGET[4];
Tab_Horodatage[5]=HORLOGET[5];
Tab_Horodatage[0]=HORLOGET[6];
Tab_Horodatage[1]=HORLOGET[7];
Tab_Horodatage[2]=HORLOGET[8];
Tab_Horodatage[3]=HORLOGET[9];
Tab_Horodatage[10]=HORLOGET[10];
Tab_Horodatage[11]=HORLOGET[11];
//FORCAGE DATE DERNIER CYCLE

HH[0]=Tab_Horodatage[6];
HH[1]=Tab_Horodatage[7];
mm[0]=Tab_Horodatage[8];
mm[1]=Tab_Horodatage[9];
JJ[0]=Tab_Horodatage[0];
JJ[1]=Tab_Horodatage[1];
MM[0]=Tab_Horodatage[2];
MM[1]=Tab_Horodatage[3];

AA[0]='2';
AA[1]='0';
AA[2]=Tab_Horodatage[10];
AA[3]=Tab_Horodatage[11];
ss[0]=Tab_Horodatage[4];
ss[1]=Tab_Horodatage[5];

}



void Recuperer_param_cms(void)


{
char p;
LIRE_EEPROM(6,1024,&TMP,5);


for (p=0;p<30;p++) //IDENTIFIANT RACK
{
ID_rack[p]=TMP[8+p];
}


T_alarme[0]=TMP[38];
T_alarme[1]=TMP[39];
T_alarme[2]=TMP[40];

T_sortie[0]=TMP[41];
T_sortie[1]=TMP[42];
T_sortie[2]=TMP[43];

if ((T_alarme[0]=='O')||(T_alarme[0]=='o'))
TRANSALARM='O';
if ((T_alarme[0]=='N')||(T_alarme[0]=='n'))
TRANSALARM='N';
if ((T_sortie[0]=='O')||(T_sortie[0]=='o'))
TRANSSORTIEALARM='O';
if ((T_sortie[0]=='N')||(T_sortie[0]=='n'))
TRANSSORTIEALARM='N';



HH[0]='1';
HH[1]='1';
mm[0]='2';
mm[1]='2';

JJ[0]='0';
JJ[1]='1';
MM[0]='0';
MM[1]='1';
AA[0]='2';
AA[1]='0';
AA[2]='1';
AA[3]='7';

 
for (p=0;p<15;p++)
{
nr_cog[p]=TMP[p+44];
}
 
 

acc_u[0]=TMP[104];
acc_u[1]=TMP[105];
acc_u[2]=TMP[106];
acc_u[3]=TMP[107];

acc_conf[0]=TMP[108];
acc_conf[1]=TMP[109];
acc_conf[2]=TMP[110];
acc_conf[3]=TMP[111];


for (p=0;p<15;p++)
{
nr_cms[p]=TMP[p+59];
}


for (p=0;p<15;p++)
{
passerelle[p]=TMP[p+74];
}

 
 


for (p=0;p<15;p++)
{
DNS[p]=TMP[p+89];
}
 

}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////// enregistrement des donn�es  ///////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void enregistrer_donnees (void)
{
char i;
	if ( npage== 'a' && nchamps ==1)        //////////page 6a //ON SAV LE CABLE
	{
			if (nb_oct==0) //NBRE DE CARAC MENU 6.A
			{
			VOIECOD[2]=tableau[0]; //2 =002
			VOIECOD[1]='0'; 
			VOIECOD[0]='0';	
   			}         
            if (nb_oct==1)
			{
			VOIECOD[2]=tableau[1]; //21 = 021
			VOIECOD[1]=tableau[0]; 
			VOIECOD[0]='0';	
   			}
            if (nb_oct==2)
			{
			VOIECOD[0]=tableau[0]; //213 = 213
			VOIECOD[1]=tableau[1]; 
			VOIECOD[2]=tableau[2];
   			} 

	} 
    if ( npage== 'a' && nchamps ==2)       ///////////p	ge 6a  //////MPL
	{
        
        
		if (nb_oct==0) //NBRE DE CARAC MENU 6.A
			{
			ROB[1]=tableau[0]; //2 
			ROB[0]='0'; 
			
   			}         
            if (nb_oct==1)
			{
			ROB[1]=tableau[1]; //21 
			ROB[0]=tableau[0]; 
   			}
		if( ROB [0] >= '6')
		 {
			ROB[1]='0'; //21 
			ROB[0]='6'; 	
  		 }

		if(( ROB [0] == '0')&&(ROB[1]=='0'))
		 {
			ROB[1]='1'; //21 
			ROB[0]='0'; 	
  		 }

      
	}                                                     /////// MPL
    if ( npage== 'a' && nchamps ==5)       ///////////p	ge 6a
	{
        for (i=0;i<=decal;i++)
        
		COMMENTAIRE[decal+4]=tableau[decal]; //+4 car les premiers octets contiennent le Roxx robinet
      
	}
    if ( npage== 'b' && nchamps ==2)           /////////////page 6b
	{
		if (identifiant==1)                   ///////variable qui indique si 1 debimetre, si 2 resistif si 3 adressable
        {
           TYPE[0]='A';
        }
		else if (identifiant==2)
        {
           TYPE[0]='R';
        }
		else if (identifiant==3)
        {
           TYPE[0]='A';
        }
	}
     
    if ( npage== 'b' && nchamps ==1)                //////////page6b      champ 2
	{
	     	VOIECOD[decal+3]=tableau[decal];        //////////en modifie directement le champ saisie (codage capteur)
			
	}
     
    if ( npage== 'b' && nchamps ==3)               //////////page6b      champ 3
	{
    
	 CONSTITUTION[decal]=tableau[decal];	     //////////en modifie directement le champ saisie (commentaire)
		
	}
     
    if ( npage== 'b' && nchamps ==4)               //////////page6b      champ 4
	{
    	
		SEUIL[decal]=tableau[decal];               //////////en modifie directement le champ saisie (seuil)
		
	}
     
    if ( npage== 'b' && nchamps ==5)              //////////page6b      champ 5
	{
    
		DISTANCE[decal]=tableau[decal];           //////////en modifie directement le champ saisie (distance)
		
	}
     
    if ( npage== 'b' && nchamps ==6)              //////////page6b      champ 6
	{
		if (identifiant==1)                        //////// variable qui identifie  si le capteur est en intervention ou pas 1 oui et 0 non
        {
        //   	ETAT[0]='0'; //on  ne modifie pas letat
			ETAT[1]='1';                          
        }
		else if (identifiant==2)
        {
           	//ETAT[0]='0';//on  ne modifie pas letat
			ETAT[1]='0';
        }
	} 
      
    if ( npage== '8' && nchamps ==1)                   /////////////////////page 8 champ 1
	{
    
		 	ID_rack[decal]=tableau[decal];             ///////////////////// identification du rack
	
	}
    if ( npage== '8' && nchamps ==2)                    /////////////////////page 8 champ 2   
	{
		if (identifiant==1)
        {
           	T_alarme[0]=1;
			T_alarme[1]=0;                                //////////////////////identifiant =1  alarme =oui ou identifiant =0  alarme =non
        }
		else if (identifiant==2)
        {
           	T_alarme[0]=0;
			T_alarme[1]=0;
        }
		
	}
    if ( npage== '8' && nchamps ==3)                    /////////////////////page 8 champ 3 
	{
		if (identifiant==1)
        {
           	T_sortie[0]=1;
			T_sortie[1]=0;                                    //////////////////////identifiant =1  transmission sortie =oui ou identifiant =0  transmission sortie =non
        }
		else if (identifiant==2)
        {
           	T_sortie[0]=0;
			T_sortie[1]=0;
        }		
	} 
    if ( npage== '8' && nchamps ==4)                    /////////////////////page 8 champ 4
	{
		if (decal==0)                                    //////////champs heure suivant la position du curseur (decal)
		{
		HH[0]=tableau[0];
		}
		if (decal==1)
		{
		HH[1]=tableau[1];
		}
		if (decal==2)
		{
		mm[0]=tableau[2];
		}		
		if (decal==3)
		{
		mm[0]=tableau[3];
		}					
	}
    if ( npage== '8' && nchamps ==5)                    /////////////////////page 8 champ 5
	{
		if (decal==0)
		{
		JJ[0]=tableau[0];                                   //////////champs de la date suivant la position du curseur (decal)
		}
		if (decal==0)
		{
		JJ[1]=tableau[1];
		}		
		if (decal==0)
		{
		MM[0]=tableau[2];
		}		
		if (decal==0)
		{
		MM[1]=tableau[3];
		}		
		if (decal==0)
		{
		AA[0]=tableau[4];
		}		
		if (decal==0)
		{
		AA[1]=tableau[5];
		}		
		if (decal==0)
		{
		AA[2]=tableau[6];
		}		
		if (decal==0)
		{
		AA[3]=tableau[7];
		}		
		 
	} 
    if ( npage== '8' && nchamps ==6)                     /////////////////////page 8 champ 6
	{
		acc_u[decal]=tableau[decal];                     /////////////////////// mots de passe utilisateur 

	}
    if ( npage== '8' && nchamps ==7)                     /////////////////////page 8 champ 7
	{
		acc_conf[decal]=tableau[decal];                  /////////////////////// mots de passe configuration 
	
	}
    if ( npage== '8' && nchamps ==8)                     /////////////////////page 8 champ 8
	{
		
		nr_cog[decal]=tableau[decal];	                 /////////////////////// numero COG
		
	}
    if ( npage== '8' && nchamps ==9)                     /////////////////////page 8 champ 9
	{
	
		nr_cms[decal]=tableau[decal];	                 /////////////////////// numero CMS
	
	}
     
     
}
char TYPEDECARTE(void)

{
char tampon; 

tampon=TYPECARTE[4]; //POUR LA VOIE 0x81 a 100 
if (VOIECOD[0]=='0')
{
				if ((VOIECOD[1]>='6')&&(VOIECOD[1]<'8')) //ENTRE 06x et x79
				tampon=TYPECARTE[3];
				if ((VOIECOD[1]>='4')&&(VOIECOD[1]<'6')) //ENTRE 04x et x59
				tampon=TYPECARTE[2];
				if ((VOIECOD[1]>='2')&&(VOIECOD[1]<'4')) //ENTRE 02x et x39
				tampon=TYPECARTE[1];
				if ((VOIECOD[1]>='0') && (VOIECOD[1]<'2')) //ENTRE 00x et x19
				tampon=TYPECARTE[0];


if ((VOIECOD[2]=='0')&&(VOIECOD[1]=='8')) //x80
tampon=TYPECARTE[3];
if ((VOIECOD[2]=='0')&&(VOIECOD[1]=='6')) //x60
tampon=TYPECARTE[2];
if ((VOIECOD[2]=='0')&&(VOIECOD[1]=='4')) //x40
tampon=TYPECARTE[1];
if ((VOIECOD[2]=='0')&&(VOIECOD[1]=='2')) //x20
tampon=TYPECARTE[0];



 


}

return tampon;
}

void CHERCHER_DONNNEES_CABLES(char *g,char j) //051 cable et codage j //DONNE LES DONNEES DU CABLE ET DU CODAGE CHOISSI ET MET UNE VARIABLE EXISTENCE A UN SI CAPTEUR EXISTE

{
char i;
char tati[6];
//tmp=LIRE_EEPROM(choix_memoire(&g),calcul_addmemoire(&g),&TMP,1); //LIRE DANS EEPROM le cable
unsigned int l;

											if (j==111) // INIT COMMENTAIRE SI MENU 6a
											{		
											for (i=0;i<19;i++)
											{
											COMMENTAIRE[i]=0x20;
											}			
											}




tati[5]='0';
tati[4]='0';
tati[3]=g[2];
tati[2]=g[1];
tati[1]=g[0];
tati[0]='#';





if (j!=111)	
{												
tati[4]=j/10+48; //CODAGE
tati[5]=48+j-10*(tati[4]-48);
}


											l=100*(tati[1]-48)+10*(tati[2]-48)+1*(tati[3]-48);
											l=128*l+30000;


											
											LIRE_EEPROM(3,l,&TMP,4); //LIRE DANS EEPROM lexistance du capteur




											capt_exist=0;
											if (j==111) // ON veut savoir s'il cable
													{
													j=2; //CABLE											
													if (TMP[j]=='a') //VOIR i etc....
													capt_exist=1;
													if (TMP[j]=='r')	
													capt_exist=1;
													}
													else
													{
													j=3+j; ///CAPTEUR
													if (TMP[j]=='A')
													capt_exist=1;
													j=2 ; //Car pour resistif seul  la ligne est r
													if ((TMP[j]=='r')&&(tati[4]=='0')&&(tati[5]=='1')) //SI r uniquement pour le codage 1
													capt_exist=1;
													}

//25092020
if ((tati[1]!='1')&& (TYPECARTE[0]=='V')&&((tati[2]=='0')||(tati[2]=='1')))
capt_exist=0;
if ((TYPECARTE[0]=='V')&&(tati[2]=='2')&&(tati[3]=='0'))
capt_exist=0;

if ((TYPECARTE[1]=='V')&&((tati[2]=='2')||(tati[2]=='3')))
capt_exist=0;
if ((TYPECARTE[1]=='V')&&(tati[2]=='4')&&(tati[3]=='0'))
capt_exist=0;

if ((TYPECARTE[2]=='V')&&((tati[2]=='4')||(tati[2]=='5')))
capt_exist=0;
if ((TYPECARTE[2]=='V')&&(tati[2]=='6')&&(tati[3]=='0'))
capt_exist=0;

if ((TYPECARTE[3]=='V')&&((tati[2]=='6')||(tati[2]=='7')))
capt_exist=0;
if ((TYPECARTE[3]=='V')&&(tati[2]=='8')&&(tati[3]=='0'))
capt_exist=0;

if ((TYPECARTE[4]=='V')&&((tati[2]=='8')||(tati[2]=='9')))
capt_exist=0;
if ((TYPECARTE[4]=='V')&&(tati[1]=='1')&&(tati[2]=='0'))
capt_exist=0;

 

if (capt_exist==1)
{																										
LIRE_EEPROM(choix_memoire(&tati),calcul_addmemoire(&tati),&TMP,1); //LIRE DANS EEPROM voir si TRAMEIN DISPO




											

											//recuperer_trame_memoire (0); //voir utiliser cela pour recuperations des variables VOIR STOCKAGE TRAME IN	
 

											


											 TYPE[0]=TMP[6];
											
											if (TMP[7]!=0xFF) // Si memoire pas vide
											{	
											CMOD[0]=TMP[7];
											CMOD[1]=TMP[8];
											CMOD[2]=TMP[9];
											CMOD[3]=TMP[10];
											}
											else
											{
											CMOD[0]='0';
											CMOD[1]='0';
											CMOD[2]='0';
											CMOD[3]='0';
											}
											

											if (TMP[11]!=0xFF) // Si memoire pas vide
											{	
											CCON[0]=TMP[11];
											CCON[1]=TMP[12];
											CCON[2]=TMP[13];
											CCON[3]=TMP[14];
											}
											else
											{
											CCON[0]='0';
											CCON[1]='0';
											CCON[2]='0';
											CCON[3]='0';
											}	

											if (TMP[15]!=0xFF) // Si memoire pas vide
											{	
											CREP[0]=TMP[15];
											CREP[1]=TMP[16];
											CREP[2]=TMP[17];
											CREP[3]=TMP[18];
											}
											else
											{
											CREP[0]='0';
											CREP[1]='0';
											CREP[2]='0';
											CREP[3]='0';
											}





											CONSTITUTION[0]=TMP[19];
											CONSTITUTION[1]=TMP[20];
											CONSTITUTION[2]=TMP[21];
											CONSTITUTION[3]=TMP[22];
											CONSTITUTION[4]=TMP[23];
											CONSTITUTION[5]=TMP[24];
											CONSTITUTION[6]=TMP[25];
											CONSTITUTION[7]=TMP[26];
											CONSTITUTION[8]=TMP[27];
											CONSTITUTION[9]=TMP[28];
											CONSTITUTION[10]=TMP[29];
											CONSTITUTION[11]=TMP[30];			
											CONSTITUTION[12]=TMP[31];	

											//VOIR POUR COMMENTAIRE 
											//dans afficher_donnees_si_existe
 											for (i=0;i<19;i++)
											{
											COMMENTAIRE[i]=TMP[32+i];
											}		

										
											 if (TMP[50]!=0x0FF)
											{																			
											 CABLE[0]=TMP[50];
											 CABLE[1]=TMP[51];
											 CABLE[2]=TMP[52];
											}
											else
											{
											CABLE[0]=VOIECOD[0];
											CABLE[1]=VOIECOD[1];
											CABLE[2]=VOIECOD[2];	
											}



											if (TMP[53]!=0xFF) // Si memoire pas vide
										{	
										JJ[0]=TMP[53];	
										JJ[1]=TMP[54];	
										MM[0]=TMP[55];
										MM[1]=TMP[56];
										HH[0]=TMP[57];
										HH[1]=TMP[58];	
										mm[0]=TMP[59];	
										mm[1]=TMP[60];
										}
										else
										{
										JJ[0]='2';	
										JJ[1]='9';	
										MM[0]='1';
										MM[1]='1';
										HH[0]='0';
										HH[1]='7';	
										mm[0]='1';	
										mm[1]='5';

										}			
													



										if (TMP[61]!=0xFF) // Si memoire pas vide
										ii[0]=TMP[61];
										else
										ii[0]='0';		


									if (TMP[61]!=0xFF) // Si memoire pas vide	
									{
									COD[0]=TMP[62];
									COD[1]=TMP[63];
									}
									else
									{
									COD[0]=VOIECOD[3];
									COD[1]=VOIECOD[4];	

									}


										POS[0]=TMP[64];
										POS[1]=TMP[65];
	
										if (COD[0]=='0' && COD[1]=='0') // ON FORCE LA POS A 01 CODAGE 00
										{ 
										POS[0]='0';
										POS[1]='1'; //DEBIT 
										}
										else
										{
										POS[0]='0';
										POS[1]='2'; //TPA OU TPR
										}




										if (TMP[66]!=0xFF) // Si memoire pas vide	
									{	
										iiii[0]=TMP[66];
										iiii[1]=TMP[67];
										iiii[2]=TMP[68];
										iiii[3]=TMP[69];
									}
									else
									{
										iiii[0]='0';
										iiii[1]='0';
										iiii[2]='0';
										iiii[3]='0';

									}				




											 DISTANCE[0]=TMP[70];
											 DISTANCE[1]=TMP[71];
											 DISTANCE[2]=TMP[72];
											 DISTANCE[3]=TMP[73];
											DISTANCE[4]=TMP[74];



											if (TMP[75]!=0xFF) // Si memoire pas vide		
											{		
											ETAT[0]=TMP[75];
											ETAT[1]=TMP[76];
											}
											else		
											{
											ETAT[0]='0';
											ETAT[1]='0';	
											}	


											if (TMP[77]!=0xFF) // Si memoire pas vide		
											{
											VALEUR[0]=TMP[77];
											VALEUR[1]=TMP[78];
											VALEUR[2]=TMP[79];
											VALEUR[3]=TMP[80];
											}
											else
											{
											VALEUR[0]='0';
											VALEUR[1]='0';
											VALEUR[2]='0';
											VALEUR[3]='0';
											}



 





											 SEUIL[0]=TMP[81];
											SEUIL[1]=TMP[82];
											SEUIL[2]=TMP[83];
											SEUIL[3]=TMP[84];



			 						

 		 							 
 										
}																					
else

{
	if(TYPEDECARTE()=='E')
		TYPE[0]='A';
	else if (TYPEDECARTE()=='M')
		TYPE[0]='R';
	else
		TYPE[0]='0';




											CMOD[0]='0';
											CMOD[1]='0';
											CMOD[2]='0';
											CMOD[3]='0';

											 CCON[0]='0';
											CCON[1]='0';
											CCON[2]='0';
											CCON[3]='0';

											 CREP[0]='0';
											CREP[1]='0';
											CREP[2]='0';
											CREP[3]='0';

											
											for (i=0;i<13;i++)
											{
											CONSTITUTION[i]=0x20;
											}	


													
											
											CABLE[0]=VOIECOD[0];
											CABLE[1]=VOIECOD[1];
											CABLE[2]=VOIECOD[2];


													JJ[0]='0';	
												JJ[1]='0';	
												MM[0]='0';
												MM[1]='0';
												HH[0]='0';
												HH[1]='0';	
												mm[0]='0';
												mm[1]='0';
											
	
											ii[0]='?';

											
								 	
										COD[0]=VOIECOD[3];
										COD[1]=VOIECOD[4];

										
											


									

									//	l=10*(COD[0]-48)+(COD[1]-48)+1; //INCREMENTE LE CODAGE POUR ETRE POS
									
								
									//	IntToChar(l,&POS,2); /// convertir le POS en CHAR pour l'envoyer 
										
								
										if (COD[0]=='0' && COD[1]=='0') // ON FORCE LA POS A 01 CODAGE 00
										{ 
										POS[0]='0';
										POS[1]='1'; //DEBIT 
										}
										else
										{
										POS[0]='0';
										POS[1]='2'; //TPA OU TPR
										}

										
										
										iiii[0]='?';
										iiii[1]='?';
										iiii[2]='?';
										iiii[3]='?';



											VALEUR[0]='?';
											VALEUR[1]='?';
											VALEUR[2]='?';
											VALEUR[3]='?';




											SEUIL[0]='0';
											SEUIL[1]='8';
											SEUIL[2]='0';
											SEUIL[3]='0';
				
											for (i=0;i<6;i++)
											{
											DISTANCE[i]='0';
											}
											
										//	ETAT[0]='0';
										//	ETAT[1]='0';
											ETAT[0]='5'; //CREATION DE CAPTEUR PAR DEFAUT S/R
											ETAT[1]='0';
											


}

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////// calculer le num�ro de la carte et le num�ro de la ligne   /////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void calcul_c_l (void)     
{
if ((VOIECOD[1]=='0' || VOIECOD[1]=='1'  || (VOIECOD[1]=='2' && VOIECOD[2]=='0'))&& VOIECOD[0]=='0')     //// carte num�ro 1 vois de 1 a 20
{
 																											
n_carte='1';
n_voie[0]=VOIECOD[1];
n_voie[1]=VOIECOD[2];
}
else if (((VOIECOD[1]=='2' && VOIECOD[2]!='0') || VOIECOD[1]=='3'  || (VOIECOD[1]=='4' && VOIECOD[2]=='0'))&& VOIECOD[0]=='0')  //// carte num�ro 2 vois de 1 a 20
{
 																											
n_carte='2';
n_voie[0]=VOIECOD[1]-2;
n_voie[1]=VOIECOD[2];
}
else if (((VOIECOD[1]=='4' && VOIECOD[2]!='0') || VOIECOD[1]=='5'  || (VOIECOD[1]=='6' && VOIECOD[2]=='0'))&& VOIECOD[0]=='0')   //// carte num�ro 3 vois de 1 a 20
{
 																											
n_carte='3';
n_voie[0]=VOIECOD[1]-4;
n_voie[1]=VOIECOD[2];
}
else if (((VOIECOD[1]=='6' && VOIECOD[2]!='0') || VOIECOD[1]=='7'  || (VOIECOD[1]=='8' && VOIECOD[2]=='0'))&& VOIECOD[0]=='0')   //// carte num�ro 4 vois de 1 a 20
{
 																										
n_carte='4';
n_voie[0]=VOIECOD[1]-6;
n_voie[1]=VOIECOD[2];
}
else if ((VOIECOD[1]=='8' && VOIECOD[2]!='0') || VOIECOD[1]=='9'  || (VOIECOD[0]=='1' && VOIECOD[2]=='0' && VOIECOD[1]=='0'  ))  //// carte num�ro 5 vois de 1 a 20
{
 																											
n_carte='5';
n_voie[0]=VOIECOD[1]-8;
n_voie[1]=VOIECOD[2];
if (VOIECOD[0]=='1')
{
n_voie[0]='2';
n_voie[1]='0';
}
}
else 
{
                ///////////////voie n'existe pas
}
}

char gestion_rob(void)
{
char exist;

if (capt_exist=1) //si le cable existe avec un capteur reel






exist=0;
return(exist);
}

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////fonction tabulation/////////////////////////////////////////////////////////////

void tabulation (void)
{

if (nchamps < max_champ){
nchamps++;
position_init(npage, nchamps);

envoi_cursur(); 
}


}


  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////d�calage a gauche ///////////////////////////////////////////////////////////// 
 void gauche (void)

{		  
        if (decal>0  ){    //////////modif
		  pos_init_cursur [0]= pos_init_cursur [0]- 1;

                    if (pos_init_cursur [0]==0x2F){
                       pos_init_cursur [0]= 0x39;
                       pos_init_cursur [1]=pos_init_cursur [1] - 1;
                     }
                      
					envoi_cursur(); 
                     
                    decal--;
                  	 if ((npage == '8' && (nchamps == 4 || nchamps == 5) && (decal ==1 || decal ==3) ) || (npage == '8' && (nchamps == 10 || nchamps == 11) && (decal ==2 || decal ==5 || decal ==8) ))
							 			{
								 
								 			pos_init_cursur [0]= pos_init_cursur [0]- 1;
  											if (pos_init_cursur [0]==0x2F)
											{
                       						pos_init_cursur [0]= 0x39;
                      						pos_init_cursur [1]=pos_init_cursur [1] - 1;
                                            }
                      						envoi_cursur(); 
										}

                     
                          } 
         
} 

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////fonction d�calage a droit/////////////////////////////////////////////////////////////

void droite (void)
{
				if (decal < max_decalage)
                {
                	 if ((npage == '8' && (nchamps == 4 || nchamps == 5) && (decal ==1 || decal ==3) ) || (npage == '8' && (nchamps == 10 || nchamps == 11) && (decal ==2 || decal ==5 || decal ==8) ))
							 			{
								 
								 			pos_init_cursur [0]= pos_init_cursur [0]+ 1;       ////////////// pour �viter de modifier le trait d'union de la date et les de point dans la date et le point dans la passerelle et le dns
                							if (pos_init_cursur [0]==0x3A){
                       							pos_init_cursur [0]= 0x30;
                     							pos_init_cursur [1]=pos_init_cursur [1] + 1;
												envoi_cursur(); 
							            	}
										}
            decal++;
		  		pos_init_cursur [0]= pos_init_cursur [0]+ 1;

                    if (pos_init_cursur [0]==0x3A){
                       pos_init_cursur [0]= 0x30;
                       pos_init_cursur [1]=pos_init_cursur [1] + 1;
                     }
                     
          		envoi_cursur();
 
                }    
                    
                    if (decal >= max_decalage )
                         {
                        
                     if (npage != 'a' && npage != '3' && nchamps< max_champ)
				     {
                     decal =0;  ///////////modif
                     tabulation ();
}
                      } // on passe au champs d'�criture suivant (champs1, champs2, champs3, champs4 sauf pour le menu 6.2 (a)et le menu3


                                                 }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////validation de la saisie ///////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void saisie (void)
{

TRAMEOUT[0]=TRAMEIN[0];


     if (decal <= max_decalage)
         {
  
 			 pos_init_cursur [0]= pos_init_cursur [0]+ 1;
                if (pos_init_cursur [0]==0x3A){
                       pos_init_cursur [0]= 0x30;
                       pos_init_cursur [1]=pos_init_cursur [1] + 1;
		
                     }
    				
					tableau[decal] =TRAMEIN [0]; //sauveguarder les modification du champs de donn�es ensuite il faut appel� une retine qui enregistre les modification dans l'eeprom
       				enregistrer_donnees();
                    TRAMEENVOITCPDONNEES (1);
					envoi_cursur(); 
                    decal++;
         }           
  if (decal >= max_decalage)
		{
				pos_init_cursur [1]= pos_init_cursur [1];
				pos_init_cursur [0]= pos_init_cursur [0];
                    envoi_cursur(); 

                    TRAMEENVOITCPDONNEES (8);
		}



		}


 char RECEVOIR_TCP(void)

{

//{


nbrerecep=0;
ete2:
do
{
if(!TCPIsConnected(MySocket))
{
STOPMINITEL=0xFF;
return(1); //DECONNECT

}
	StackTask();     
	StackApplications();
	wMaxGet = TCPIsGetReady(MySocket);	// Get TCP RX FIFO byte count
	wMaxPut = TCPIsPutReady(MySocket);	// Get TCP TX FIFO free space
}while (wMaxGet==0);
TCPGetArray(MySocket,carac, 1);
//if (carac[0]!=0xFF) //silence
//{
nbrerecep++;
//TEMPO(2);
//}
//if (TRAMEIN[0]==0x15) //NO ACK
//{
//nbrerecep=100;
//goto close;
//}
  if (nbrerecep<1)
	{
		goto ete2;
	}
//}
 

return(carac[0]);



}


//} 


unsigned char resume_voies(char y,char deb) //ANALYSE L'ETAT VOIES EXIST. y numero carte et deb mettre dans TRAMEOUT A PARTIR DE LA pour menu 2 alarme

{


char j,i,offset,nepasprendre;
unsigned int l;

nepasprendre=0;
//TEST PRESENCE CARTE
if((TYPECARTE[0]=='V')&&(y==1))
nepasprendre=1;
if((TYPECARTE[1]=='V')&&(y==2))
nepasprendre=1;
if((TYPECARTE[2]=='V')&&(y==3))
nepasprendre=1;
if((TYPECARTE[3]=='V')&&(y==4))
nepasprendre=1;
if((TYPECARTE[4]=='V')&&(y==5))
nepasprendre=1;





j=1+(y-1)*20;



					for (i=j;i<(j+20);i+=1) //20 pour une carte de 1 � 99
					{
				
										//Tab_Voie_1[0]=i;
										//Tab_Voie_2[0]=i+1;
					
 										// VOIE IMPAIRE
										delay_qms(10);
										l=128*i+30000;
										LIRE_EEPROM(3,l,&TMP,4); //LIRE DANS EEPROM lexistance de toutes le voies
					
											TRAMEIN[i-(y-1)*20-1]='|';	
					
										
					
											if ((TMP[2]=='a')&&(nepasprendre==0)) //voie adressable declar��
												{
												
												TRAMEIN[i-(y-1)*20-1]=TMP[1];
					
					
					
					
												
												}
												
										
										
												
					
					
					
												if ((TMP[2]=='r')&&(nepasprendre==0)) //voie resistive declar��
												{
											
												TRAMEIN[i-(y-1)*20-1]=TMP[1];
					
											
											
												}
					





 






					}	//FIN DE TOUTES LES VOIES	

			
 

			offset=0;
			for (i=0;i<20;i++)
			{
			
								if (((4+i*+deb)>84)&&(offset==0)) // MAX DE LA LIGNE pas forcement 80 caracteres
								{
								position_init(6, 11+y); //PAGE 6 champ 1 
								pos_init_cursur [2]++;	
								if ((pos_init_cursur [2]-48)>9)
								pos_init_cursur [3]++;
								TRAMEOUT[4*i+deb]=0x1B;
								TRAMEOUT[4*i+deb+1]=0x5B;////////////// envoyer les coordonn�es du cursur
								TRAMEOUT[4*i+deb+2]=pos_init_cursur [3];
								TRAMEOUT[4*i+deb+3]=pos_init_cursur [2];
								TRAMEOUT[4*i+deb+4]=0x3B;
								TRAMEOUT[4*i+deb+5]=pos_init_cursur [1];
								TRAMEOUT[4*i+deb+6]=pos_init_cursur [0];
								TRAMEOUT[4*i+deb+7]=0x48;		
								offset=8; //on decale tt de 8 mnt dans trameout
	
								}	
				
			IntToChar(TRAMEIN[i],&TMP,3); /// convertir le CRC en CHAR pour l'envoyer 
			if (TRAMEIN[i]=='|')
			{
			TMP[0]=' ';
			TMP[1]=' ';
			TMP[2]=' ';
			}	
			
			TRAMEOUT[4*i+deb+offset]='-';
			TRAMEOUT[4*i+deb+1+offset]=TMP[0];
			TRAMEOUT[4*i+deb+2+offset]=TMP[1];
			TRAMEOUT[4*i+deb+3+offset]=TMP[2];
	
			
			

			} //le tableau fini a 84+deb

deb=offset+deb+4*20;
    
return deb; 

 

}

void variables_groupe(char t)

{

				valeur[0]=0x20;
				valeur[1]=0x20;
				valeur[2]=0x20;
				valeur[3]=0x20;
				
				
				ETAT[0]=0x20;
				ETAT[1]=0x20;
				ETAT[2]=0x20;
				ETAT[3]=0x20;
				
				
				
				SEUIL[0]=0x20;
				SEUIL[1]=0x20;
				SEUIL[2]=0x20;
				SEUIL[3]=0x20;
				
				SEUIL2[0]=0x20;
				SEUIL2[1]=0x20;
				SEUIL2[2]=0x20;
				SEUIL2[3]=0x20;

				iiii[0]=0x20;
				iiii[1]=0x20;
				iiii[2]=0x20;
				iiii[3]=0x20;
				iiii[4]=0x20;



if (t==1)
{
				valeur[0]=P_RESERVOIR[0];
				valeur[1]=P_RESERVOIR[1];
				valeur[2]=P_RESERVOIR[2];
				valeur[3]=P_RESERVOIR[3];
				
				
				ETAT[0]=P_RESERVOIR[4];
				ETAT[1]=P_RESERVOIR[5];
				ETAT[2]=' ';
				ETAT[3]=' ';
				
				
				
				SEUIL[0]=P_RESERVOIR[6];
				SEUIL[1]=P_RESERVOIR[7];
				SEUIL[2]=P_RESERVOIR[8];
				SEUIL[3]=P_RESERVOIR[9];
				
				SEUIL2[0]=P_RESERVOIR[10];
				SEUIL2[1]=P_RESERVOIR[11];
				SEUIL2[2]=P_RESERVOIR[12];
				SEUIL2[3]=P_RESERVOIR[13];
}



if (t==2)
{
				valeur[0]=P_SORTIE[0];
				valeur[1]=P_SORTIE[1];
				valeur[2]=P_SORTIE[2];
				valeur[3]=P_SORTIE[3];
				
				
				ETAT[0]=P_SORTIE[4];
				ETAT[1]=P_SORTIE[5];
				ETAT[2]=' ';
				ETAT[3]=' ';
				
				
				
				SEUIL[0]=P_SORTIE[6];
				SEUIL[1]=P_SORTIE[7];
				SEUIL[2]=P_SORTIE[8];
				SEUIL[3]=P_SORTIE[9];
				
				SEUIL2[0]=P_SORTIE[10];
				SEUIL2[1]=P_SORTIE[11];
				SEUIL2[2]=P_SORTIE[12];
				SEUIL2[3]=P_SORTIE[13];
}



if (t==3)
{
				valeur[0]=P_SECOURS[0];
				valeur[1]=P_SECOURS[1];
				valeur[2]=P_SECOURS[2];
				valeur[3]=P_SECOURS[3];
				
				
				ETAT[0]=P_SECOURS[4];
				ETAT[1]=P_SECOURS[5];
				ETAT[2]=' ';
				ETAT[3]=' ';
				
				
				
				SEUIL[0]=P_SECOURS[6];
				SEUIL[1]=P_SECOURS[7];
				SEUIL[2]=P_SECOURS[8];
				SEUIL[3]=P_SECOURS[9];
				
				SEUIL2[0]=P_SECOURS[10];
				SEUIL2[1]=P_SECOURS[11];
				SEUIL2[2]=P_SECOURS[12];
				SEUIL2[3]=P_SECOURS[13];
}


if (t==4)
{
				valeur[0]=POINT_ROSEE[0];
				valeur[1]=POINT_ROSEE[1];
				valeur[2]=POINT_ROSEE[2];
				valeur[3]=POINT_ROSEE[3];
				
				
				ETAT[0]=POINT_ROSEE[4];
				ETAT[1]=POINT_ROSEE[5];
				ETAT[2]=' ';
				ETAT[3]=' ';
				
				
				
				SEUIL[0]=POINT_ROSEE[6];
				SEUIL[1]=POINT_ROSEE[7];
				SEUIL[2]=POINT_ROSEE[8];
				SEUIL[3]=POINT_ROSEE[9];
				
			
}


if (t==5)
{
				valeur[0]=DEBIT_GLOBAL[0];
				valeur[1]=DEBIT_GLOBAL[1];
				valeur[2]=DEBIT_GLOBAL[2];
				valeur[3]=DEBIT_GLOBAL[3];
				
				
				ETAT[0]=DEBIT_GLOBAL[4];
				ETAT[1]=DEBIT_GLOBAL[5];
				ETAT[2]=' ';
				ETAT[3]=' ';
				
				
				
				SEUIL[0]=DEBIT_GLOBAL[6];
				SEUIL[1]=DEBIT_GLOBAL[7];
				SEUIL[2]=DEBIT_GLOBAL[8];
				SEUIL[3]=DEBIT_GLOBAL[9];
				
	
}


if (t==6)
{
				valeur[0]=TEMPS_CYCLE_MOTEUR[0];
				valeur[1]=TEMPS_CYCLE_MOTEUR[1];
				valeur[2]=TEMPS_CYCLE_MOTEUR[2];
				valeur[3]=TEMPS_CYCLE_MOTEUR[3];
				
				
				ETAT[0]=TEMPS_CYCLE_MOTEUR[4];
				ETAT[1]=TEMPS_CYCLE_MOTEUR[5];
				ETAT[2]=' ';
				ETAT[3]=' ';
				
				
				
				SEUIL[0]=TEMPS_CYCLE_MOTEUR[6];
				SEUIL[1]=TEMPS_CYCLE_MOTEUR[7];
				SEUIL[2]=TEMPS_CYCLE_MOTEUR[8];
				SEUIL[3]=TEMPS_CYCLE_MOTEUR[9];

}



if (t==8)
{
				valeur[0]=TEMPS_MOTEUR[0];
				valeur[1]=TEMPS_MOTEUR[1];
				valeur[2]=TEMPS_MOTEUR[2];
				valeur[3]=TEMPS_MOTEUR[3];
				
				
				ETAT[0]=TEMPS_MOTEUR[4];
				ETAT[1]=TEMPS_MOTEUR[5];
				ETAT[2]=' ';
				ETAT[3]=' ';
				
				
				
				SEUIL[0]=TEMPS_MOTEUR[6];
				SEUIL[1]=TEMPS_MOTEUR[7];
				SEUIL[2]=TEMPS_MOTEUR[8];
				SEUIL[3]=TEMPS_MOTEUR[9];
				
				SEUIL2[0]=TEMPS_MOTEUR[10];
				SEUIL2[1]=TEMPS_MOTEUR[11];
				SEUIL2[2]=TEMPS_MOTEUR[12];
				SEUIL2[3]=TEMPS_MOTEUR[13];
				iiii[0]=TEMPS_MOTEUR[14];
				iiii[1]=TEMPS_MOTEUR[15];
				iiii[2]=TEMPS_MOTEUR[16];
				iiii[3]=TEMPS_MOTEUR[17];
				iiii[4]=TEMPS_MOTEUR[18];


}


if (t==9)
{
				valeur[0]=PRESENCE_220V[0];
				valeur[1]=PRESENCE_220V[1];
				valeur[2]=PRESENCE_220V[2];
				valeur[3]=PRESENCE_220V[3];
				
				
				ETAT[0]=PRESENCE_220V[4];
				ETAT[1]=PRESENCE_220V[5];
				ETAT[2]=' ';
				ETAT[3]=' ';
				
				
	
}





if (t==10)
{
				valeur[0]=PRESENCE_48V[0];
				valeur[1]=PRESENCE_48V[1];
				valeur[2]=PRESENCE_48V[2];
				valeur[3]=PRESENCE_48V[3];
				
				
				ETAT[0]=PRESENCE_48V[4];
				ETAT[1]=PRESENCE_48V[5];
				ETAT[2]=' ';
				ETAT[3]=' ';
				
				
				
	
}







}

char CHERCHER_DONNEES_GROUPE(void)

{

char o;




//---------------------------------1
LIRE_EEPROM(6,0,&TMP,3);
delay_qms(100);
//VALEUR



o=35;
P_RESERVOIR[0]=TMP[o];
P_RESERVOIR[1]=TMP[o+1];
P_RESERVOIR[2]=TMP[o+2];
P_RESERVOIR[3]=TMP[o+3];


//ETAT
P_RESERVOIR[4]=TMP[o-2];
P_RESERVOIR[5]=TMP[o-1];


//SEUILH
P_RESERVOIR[6]=TMP[o+4];
P_RESERVOIR[7]=TMP[o+5];
P_RESERVOIR[8]=TMP[o+6];
P_RESERVOIR[9]=TMP[o+7];


//SEUILB
P_RESERVOIR[10]=TMP[o-11];
P_RESERVOIR[11]=TMP[o-10];
P_RESERVOIR[12]=TMP[o-9];
P_RESERVOIR[13]=TMP[o-8];

//---------------------------------------------------2

o=o+24;

//VALEUR
P_SORTIE[0]=TMP[o];
P_SORTIE[1]=TMP[o+1];
P_SORTIE[2]=TMP[o+2];
P_SORTIE[3]=TMP[o+3];

//ETAT
P_SORTIE[4]=TMP[o-2];
P_SORTIE[5]=TMP[o-1];

//SEUILH
P_SORTIE[6]=TMP[o+4];
P_SORTIE[7]=TMP[o+5];
P_SORTIE[8]=TMP[o+6];
P_SORTIE[9]=TMP[o+7];



//SEUILB
P_SORTIE[10]=TMP[o-11];
P_SORTIE[11]=TMP[o-10];
P_SORTIE[12]=TMP[o-9];
P_SORTIE[13]=TMP[o-8];


//---------------------------------------3 attention  xxxxxxx entre 2 et 3 donc deux fois
o=o+24;
o=o+24;




//VALEUR
P_SECOURS[0]=TMP[o];
P_SECOURS[1]=TMP[o+1];
P_SECOURS[2]=TMP[o+2];
P_SECOURS[3]=TMP[o+3];

//ETAT
P_SECOURS[4]=TMP[o-2];
P_SECOURS[5]=TMP[o-1];

//SEUILH
P_SECOURS[6]=TMP[o+4];
P_SECOURS[7]=TMP[o+5];
P_SECOURS[8]=TMP[o+6];
P_SECOURS[9]=TMP[o+7];

 

//SEUILB
P_SECOURS[10]=TMP[o-11];
P_SECOURS[11]=TMP[o-10];
P_SECOURS[12]=TMP[o-9];
P_SECOURS[13]=TMP[o-8];





//--------------------------------------------------------------------4




LIRE_EEPROM(6,128,&TMP,3);
delay_qms(100);


o=35;
POINT_ROSEE[0]=TMP[o];
POINT_ROSEE[1]=TMP[o+1];
POINT_ROSEE[2]=TMP[o+2];
POINT_ROSEE[3]=TMP[o+3];


//ETAT
POINT_ROSEE[4]=TMP[o-2];
POINT_ROSEE[5]=TMP[o-1];


//SEUILH
POINT_ROSEE[6]=TMP[o+4];
POINT_ROSEE[7]=TMP[o+5];
POINT_ROSEE[8]=TMP[o+6];
POINT_ROSEE[9]=TMP[o+7];



//---------------------------------------------------5

o=o+24;

//VALEUR
DEBIT_GLOBAL[0]=TMP[o];
DEBIT_GLOBAL[1]=TMP[o+1];
DEBIT_GLOBAL[2]=TMP[o+2];
DEBIT_GLOBAL[3]=TMP[o+3];

//ETAT
DEBIT_GLOBAL[4]=TMP[o-2];
DEBIT_GLOBAL[5]=TMP[o-1];

//SEUILH
DEBIT_GLOBAL[6]=TMP[o+4];
DEBIT_GLOBAL[7]=TMP[o+5];
DEBIT_GLOBAL[8]=TMP[o+6];
DEBIT_GLOBAL[9]=TMP[o+7];



o=o+24;

//-------------------------------------------------6


//VALEUR
TEMPS_CYCLE_MOTEUR[0]=TMP[o];
TEMPS_CYCLE_MOTEUR[1]=TMP[o+1];
TEMPS_CYCLE_MOTEUR[2]=TMP[o+2];
TEMPS_CYCLE_MOTEUR[3]=TMP[o+3];

//ETAT
TEMPS_CYCLE_MOTEUR[4]=TMP[o-2];
TEMPS_CYCLE_MOTEUR[5]=TMP[o-1];

//SEUILH
TEMPS_CYCLE_MOTEUR[6]=TMP[o+4];
TEMPS_CYCLE_MOTEUR[7]=TMP[o+5];
TEMPS_CYCLE_MOTEUR[8]=TMP[o+6];
TEMPS_CYCLE_MOTEUR[9]=TMP[o+7];

//PAS DE 7

//-------------------------------------8





o=o+24;
TEMPS_MOTEUR[0]=TMP[o];
TEMPS_MOTEUR[1]=TMP[o+1];
TEMPS_MOTEUR[2]=TMP[o+2];
TEMPS_MOTEUR[3]=TMP[o+3];


//ETAT
TEMPS_MOTEUR[4]=TMP[o-2];
TEMPS_MOTEUR[5]=TMP[o-1];


//SEUILH
TEMPS_MOTEUR[6]=TMP[o+4];
TEMPS_MOTEUR[7]=TMP[o+5];
TEMPS_MOTEUR[8]=TMP[o+6];
TEMPS_MOTEUR[9]=TMP[o+7];



//SEUILB
TEMPS_MOTEUR[10]=TMP[o-11];
TEMPS_MOTEUR[11]=TMP[o-10];
TEMPS_MOTEUR[12]=TMP[o-9];
TEMPS_MOTEUR[13]=TMP[o-8];



TEMPS_MOTEUR[14]=TMP[o-7];
TEMPS_MOTEUR[15]=TMP[o-6];
TEMPS_MOTEUR[16]=TMP[o-5];
TEMPS_MOTEUR[17]=TMP[o-4];
TEMPS_MOTEUR[18]=TMP[o-3];
//INITY...????



LIRE_EEPROM(6,256,&TMP,3);
delay_qms(100);

//---------------------------------------------------9

o=35;

//VALEUR
PRESENCE_220V[0]=TMP[o];
PRESENCE_220V[1]=TMP[o+1];
PRESENCE_220V[2]=TMP[o+2];
PRESENCE_220V[3]=TMP[o+3];

//ETAT
PRESENCE_220V[4]=TMP[o-2];
PRESENCE_220V[5]=TMP[o-1];



o=o+24;

//------------------------------------------------------10


//VALEUR
PRESENCE_48V[0]=TMP[o];
PRESENCE_48V[1]=TMP[o+1];
PRESENCE_48V[2]=TMP[o+2];
PRESENCE_48V[3]=TMP[o+3];

//ETAT
PRESENCE_48V[4]=TMP[o-2];
PRESENCE_48V[5]=TMP[o-1];




}


void analyser_les_alarmes_en_cours(void) //ANALYSE LES CABLES ET LES ETATS POPUR L'INSTANT

{


char j,tpa,i;
unsigned int l;





for (i=0;i<5;i++)
{
ET_TD[i]=0;
ET_GR[i]=0;
ET_TP[i]=0;
ET_TR[i]=0;
ET_I[i]=0;
ET_C[i]=0;
ET_F[i]=0;

}
CHERCHER_DONNEES_GROUPE();



//ETAT
TMP[0]=P_RESERVOIR[4];
TMP[1]=P_RESERVOIR[5];
infos_etats(&TMP,4); //CAPTEURS GROUPE


//ETAT
TMP[0]=P_SORTIE[4];
TMP[1]=P_SORTIE[5];
infos_etats(&TMP,4); //CAPTEURS GROUPE



//ETAT
TMP[0]=P_SECOURS[4];
TMP[1]=P_SECOURS[5];
infos_etats(&TMP,4); //CAPTEURS GROUPE


//ETAT
TMP[0]=POINT_ROSEE[4];
TMP[1]=POINT_ROSEE[5];
infos_etats(&TMP,4); //CAPTEURS GROUPE

//ETAT
TMP[0]=DEBIT_GLOBAL[4];
TMP[1]=DEBIT_GLOBAL[5];
infos_etats(&TMP,4); //CAPTEURS GROUPE

//ETAT
TMP[0]=TEMPS_CYCLE_MOTEUR[4];
TMP[1]=TEMPS_CYCLE_MOTEUR[5];
infos_etats(&TMP,4); //CAPTEURS GROUPE



//ETAT
TMP[0]=TEMPS_MOTEUR[4];
TMP[1]=TEMPS_MOTEUR[5];
infos_etats(&TMP,4); //CAPTEURS GROUPE

//ETAT
TMP[0]=PRESENCE_220V[4];
TMP[1]=PRESENCE_220V[5];
infos_etats(&TMP,4); //CAPTEURS GROUPE


//ETAT
TMP[0]=PRESENCE_48V[4];
TMP[1]=PRESENCE_48V[5];
infos_etats(&TMP,4); //CAPTEURS GROUPE





							DEMANDE_INFOS_CAPTEURS[0]='0';
							DEMANDE_INFOS_CAPTEURS[1]='0';
							DEMANDE_INFOS_CAPTEURS[2]='0';
							DEMANDE_INFOS_CAPTEURS[3]='0';
							DEMANDE_INFOS_CAPTEURS[4]='0';


	for (i=1;i<=100;i+=1) //20 pour une carte de 1 � 99
					{

					if ((i>=1)&&(i<21)&&(TYPECARTE[0]=='V'))
					i=21;	
					if ((i>=21)&&(i<41)&&(TYPECARTE[1]=='V'))
					i=41;
					if ((i>=41)&&(i<61)&&(TYPECARTE[2]=='V'))
					i=61;
					if ((i>=61)&&(i<81)&&(TYPECARTE[3]=='V'))
					i=81;
					if ((i>=81)&&(i<=100)&&(TYPECARTE[4]=='V'))
					goto finetude;


					//Tab_Voie_1[0]=i;
					//Tab_Voie_2[0]=i+1;

					// VOIE IMPAIRE
					delay_qms(10);
					l=128*i+30000;
					LIRE_EEPROM(3,l,&TRAMEIN,4); //LIRE DANS EEPROM lexistance de toutes le voies


DEMANDE_INFOS_CAPTEURS[0]='#';
DEMANDE_INFOS_CAPTEURS[1]='0';
if (i>99)
DEMANDE_INFOS_CAPTEURS[1]='1';


DEMANDE_INFOS_CAPTEURS[2]=i/10+48;
DEMANDE_INFOS_CAPTEURS[3]=48+i-10*(DEMANDE_INFOS_CAPTEURS[2]-48);
						
						if (TRAMEIN[2]=='i') //voie EN INCIDENT
						ET_I[0]++;
						if (TRAMEIN[2]=='h') //voies FUSIBLE HS
						ET_F[0]++;
						if (TRAMEIN[2]=='c') //voie en CC
						ET_C[0]++;


						if (TRAMEIN[2]=='a') //voie meme carte adressable
							{
								tpa=0;
														for (j=0;j<17;j++)
														{
													
														DEMANDE_INFOS_CAPTEURS[4]=j/10+48;
														DEMANDE_INFOS_CAPTEURS[5]=48+j-10*(DEMANDE_INFOS_CAPTEURS[4]-48);
														
														if ((j==0)&&(TRAMEIN[3]=='A'))
														{	
														LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM
													//	TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(0,0); // 0 ADRESSABLE en ancien telgat
														infos_etats(&TMP,0); // DEBITMETRES
 
														}			
														if (j>0)
														{
											
														if (TRAMEIN[3+j]=='A')
															{
															tpa=tpa+1;
															LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM
															
															//TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(1,tpa); // 0 ADRESSABLE en ancien telgat dans la trameout
															infos_etats(&TMP,1); //CAPTEURS PRESSION



															//TRAMEOUT[20*j]=&TMP
															delay_qms(10);
											
															}	
													 
														}
														} //TOUTE LA VOIE
					
					
					
					
					
					
					
					
					
					
								}



					if (TRAMEIN[2]=='r') //voie meme carte resitive envoyer juste le premier capteur
					{
					DEMANDE_INFOS_CAPTEURS[4]='0';			
					DEMANDE_INFOS_CAPTEURS[5]='1';

					LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM
					infos_etats(&TMP,3); //CAPTEURS PRESSION RESISTIFS
					delay_qms(10);		
				
				
					}


finetude:
delay_qms(10);		









					}	//FIN DE TOUTES LES VOIES	

			
 


//	tmp=LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM
    



 

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////// fonction haut pour aller sur le champs d'�criture pr�cedent//////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void haut (void)
{
if (nchamps > 1){
nchamps--;
position_init(npage, nchamps);

      envoi_cursur();                

}

}

char VALIDER_CAPTEUR(void)
{
char t,u;


TERMINEACTION=0; //L'ACTION A FAIRE 

if (CMouPAS==0)
TERMINEACTION=1; // AEF A SUPPRIMER
 



u=0;
	position_init(npage, 8);
	envoi_cursur();
	TRAMEOUT[0]='P';
	TRAMEOUT[1]='A';
	TRAMEOUT[2]='T';
	TRAMEOUT[3]='I';
	TRAMEOUT[4]='E';
	TRAMEOUT[5]='N';
	TRAMEOUT[6]='T';
	TRAMEOUT[7]='E';
	TRAMEOUT[8]='Z';
	TRAMEENVOITCPDONNEES(9);
	TCPFlush(MySocket);
	position_init(npage, nchamps);
	envoi_cursur();

do
{
TEMPO(5);
CREER_TRAMEIN_CAPTEUR_DU_MINITEL();//RECUPERE LES VARIABLES ET CREATION D'UNE TRAME EEPROM	

if (CMouPAS==1)
ENVOIE_DES_TRAMES (0,329,&TRAMEIN) ; //CREATION CAPTEUR SUIVANT TRA. MEMOIRE SI ERRFF (TERMINEACTION=1) ALORS L LA CREATION/MODIF AIT EU LIEU AEF

if (TERMINEACTION==3)// on a recu un 0xDD o, retente //ATTENTION TRAMEIN PLUS CORRECTE!!!!!!!!!!!!!!!!!!!!!!!!
{
TRAMERECEPTIONRS485DONNEES (18);  //ON ATTEND TRUC DU GENRE CMR1 0xAA de la carte
IO_EN_COM=1; //2020
CREER_TRAMEIN_CAPTEUR_DU_MINITEL();//RECUPERE LES VARIABLES ET CREATION D'UNE TRAME EEPROM	
TEMPO(2);
ENVOIE_DES_TRAMES (0,329,&TRAMEIN) ; //CREATION CAPTEUR SUIVANT TRA. MEMOIRE SI ERRFF (TERMINEACTION=1) ALORS L LA CREATION/MODIF AIT EU LIEU AEF

}


		if (TERMINEACTION==1) 
		{
					 if (CMouPAS==1)
					 {						

								//VERSION AVEC CM
							//-----------------------------
							      
							TRAMEIN[0]='#'; //ICI ON REM. TOUTE LA TRAME OUT COM 485 DU CAPTEUR EN IN FORMAT MEMOIRE #00102.....
							for (t=1;t<91;t++)
							{
							TRAMEIN[t]=TRAMEOUT[t+7];
							}
							//-----------------------------

		
					}





		MODIFICATION_TABLE_DEXISTENCE (&TRAMEIN,0,0); //MODIFICATION DE LA TABLE 

		ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEIN),calcul_addmemoire(&TRAMEIN),&TRAMEIN,1); //ECRITURE EN EEPROM LE CABLE
		
		LIRE_EEPROM(choix_memoire(&TRAMEIN),calcul_addmemoire(&TRAMEIN),&TRAMEOUT,1); //ESSAI A EFFACER


		//TRAMEIN[4]='0';
		//TRAMEIN[5]='0'; //CODAGE 0
		//LIRE_EEPROM(choix_memoire(&TRAMEOUT),calcul_addmemoire(&TRAMEOUT),&TRAMEIN,1); //LIRE DANS EEPROM LIRE LE CAPTEUR 0 C ICI QUON IRA LIRE LE COMMENTAIRE CABLE
		
		
		//TRAMEOUT[0]='#';
		//TRAMEOUT[1]=VOIECOD[0];
		//TRAMEOUT[2]=VOIECOD[1];
		//TRAMEOUT[3]=VOIECOD[2];
		//TRAMEOUT[4]='0';
		//TRAMEOUT[5]='0';
		
		//for (t=0;t<18;t++) 
		//{
		//TRAMEOUT[32+t]=COMMENTAIRE[t];
		
		//}
		//ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEOUT),calcul_addmemoire(&TRAMEOUT),&TRAMEOUT,1); //ECRITURE EN EEPROM

		u=99;

//ENVOIE_DES_TRAMES (0,329,&TRAMEIN) ; //CREATION CAPTEUR SUIVANT TRA. MEMOIRE SI ERRFF (TERMINEACTION=1) ALORS L LA CREATION/MODIF AIT EU LIEU
 

		// ICI ON ENREGISTRE EGALEMENT SUR LE DEBIT PRESENT OU PAS EN CODAGE 00 POUR LE COMMENTAIRE ET ROBINET  !!!!!!
		VOIECOD[3]='0';
		VOIECOD[4]='0';
		CHERCHER_DONNNEES_CABLES(&VOIECOD,0); //CABLE 
		CREER_TRAMEIN_CAPTEUR_DU_MINITEL();
		ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEIN),calcul_addmemoire(&TRAMEIN),&TRAMEIN,1); //ECRITURE EN EEPROM

		//SAV MODIF


		}
u=u+1;
}
while (u<2); //si pas reuissi on refait une fois


nchamps=8;
npage='b';
 				        
position_init(npage, nchamps);
       										envoi_cursur();
if (TERMINEACTION!=1)
{
											
											TRAMEOUT[0]='V';				
											TRAMEOUT[1]='E';
											TRAMEOUT[2]='R';
											TRAMEOUT[3]='I';
											TRAMEOUT[4]='F';
											TRAMEOUT[5]='I';
											TRAMEOUT[6]='E';
											TRAMEOUT[7]='R';
											TRAMEOUT[8]=' ';


}

else
{

											TRAMEOUT[0]=' ';				
											TRAMEOUT[1]=' ';
											TRAMEOUT[2]=' ';
											TRAMEOUT[3]=' ';
											TRAMEOUT[4]=' ';
											TRAMEOUT[5]=' ';
											TRAMEOUT[6]=' ';
											TRAMEOUT[7]=' ';
											TRAMEOUT[8]=' ';



}


												


											TRAMEENVOITCPDONNEES (9);





TERMINEACTION=2;

//delay_qms(100);

}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////supression d'un capteur////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void sup_capteur (void)   /////
{
TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;///////////////////////////////envoi du texte etes vous sur de vouloir supprimer le capteur?  oui ou non
TRAMEOUT[2]=0x32;
TRAMEOUT[3]=0x4A;
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x31;
TRAMEOUT[7]=0x32;
TRAMEOUT[8]=0x3B;
TRAMEOUT[9]=0x31;
TRAMEOUT[10]=0x32;
TRAMEOUT[11]=0x48;
TRAMEOUT[12]=0x45;
TRAMEOUT[13]=0x74;
TRAMEOUT[14]=0x65;
TRAMEOUT[15]=0x73;
TRAMEOUT[16]=0x2D;
TRAMEOUT[17]=0x76;
TRAMEOUT[18]=0x6F;
TRAMEOUT[19]=0x75;
TRAMEOUT[20]=0x73;
TRAMEOUT[21]=0x20;
TRAMEOUT[22]=0x73;
TRAMEOUT[23]=0x75;
TRAMEOUT[24]=0x72;
TRAMEOUT[25]=0x20;
TRAMEOUT[26]=0x64;
TRAMEOUT[27]=0x65;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]=0x76;
TRAMEOUT[30]=0x6F;
TRAMEOUT[31]=0x75;
TRAMEOUT[32]=0x6C;
TRAMEOUT[33]=0x6F;
TRAMEOUT[34]=0x69;
TRAMEOUT[35]=0x72;
TRAMEOUT[36]=0x20;
TRAMEOUT[37]=0x73;
TRAMEOUT[38]=0x75;
TRAMEOUT[39]=0x70;
TRAMEOUT[40]=0x70;
TRAMEOUT[41]=0x72;
TRAMEOUT[42]=0x69;
TRAMEOUT[43]=0x6D;
TRAMEOUT[44]=0x65;
TRAMEOUT[45]=0x72;
TRAMEOUT[46]=0x20;
TRAMEOUT[47]=0x6C;
TRAMEOUT[48]=0x65;
TRAMEOUT[49]=0x20;
TRAMEOUT[50]=0x63;
TRAMEOUT[51]=0x61;
TRAMEOUT[52]=0x70;
TRAMEOUT[53]=0x74;
TRAMEOUT[54]=0x65;
TRAMEOUT[55]=0x75;
TRAMEOUT[56]=0x72;
TRAMEOUT[57]=0x3F;
TRAMEOUT[58]=0x1B; //21092020
TRAMEOUT[59]='[';
TRAMEOUT[60]='2';
TRAMEOUT[61]='1';
TRAMEOUT[62]=';';
TRAMEOUT[63]='1';
TRAMEOUT[64]='H';
TRAMEOUT[65]=0x1B;
TRAMEOUT[66]='[';
TRAMEOUT[67]='7';
TRAMEOUT[68]='m';
TRAMEOUT[69]='A';
TRAMEOUT[70]='P';
TRAMEOUT[71]='P';
TRAMEOUT[72]='U';
TRAMEOUT[73]='Y';
TRAMEOUT[74]='E';
TRAMEOUT[75]='R';
TRAMEOUT[76]=0x1B;
TRAMEOUT[77]='[';
TRAMEOUT[78]='0';
TRAMEOUT[79]='m';
TRAMEOUT[80]=' ';
TRAMEOUT[81]='s';
TRAMEOUT[82]='u';
TRAMEOUT[83]='r';
TRAMEOUT[84]=' ';
TRAMEOUT[85]='O';
TRAMEOUT[86]='/';
TRAMEOUT[87]='o';
TRAMEOUT[88]=' ';
TRAMEOUT[89]='p';
TRAMEOUT[90]='o';
TRAMEOUT[91]='u';
TRAMEOUT[92]='r';
TRAMEOUT[93]=' ';
TRAMEOUT[94]='c';
TRAMEOUT[95]='o';
TRAMEOUT[96]='n';
TRAMEOUT[97]='f';
TRAMEOUT[98]='i';
TRAMEOUT[99]='r';
TRAMEOUT[100]='m';
TRAMEOUT[101]='e';
TRAMEOUT[102]='r';
TRAMEOUT[103]=' ';
TRAMEOUT[104]='o';
TRAMEOUT[105]='u';

TRAMEENVOITCPDONNEES(106);

TRAMEOUT[0]=' ';
TRAMEOUT[1]='u';
TRAMEOUT[2]='n';
TRAMEOUT[3]='e';
TRAMEOUT[4]=' ';
TRAMEOUT[5]='a';
TRAMEOUT[6]='u';
TRAMEOUT[7]='t';
TRAMEOUT[8]='r';
TRAMEOUT[9]='e';
TRAMEOUT[10]=' ';
TRAMEOUT[11]='t';
TRAMEOUT[12]='o';
TRAMEOUT[13]='u';
TRAMEOUT[14]='c';
TRAMEOUT[15]='h';
TRAMEOUT[16]='e';
TRAMEOUT[17]=' ';
TRAMEOUT[18]='p';
TRAMEOUT[19]='o';
TRAMEOUT[20]='u';
TRAMEOUT[21]='r';
TRAMEOUT[22]=' ';
TRAMEOUT[23]='a';
TRAMEOUT[24]='n';
TRAMEOUT[25]='n';
TRAMEOUT[26]='u';
TRAMEOUT[27]='l';
TRAMEOUT[28]='e';
TRAMEOUT[29]='r';
TRAMEENVOITCPDONNEES (30);
TCPFlush(MySocket);

TRAMERECEPTIONminitel();
if (STOPMINITEL==0xFF)
goto fin_sup;



if (TRAMEIN[0]=='o' || TRAMEIN[0]=='O')  
{
TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;
TRAMEOUT[2]=0x32;
TRAMEOUT[3]=0x4A;
TRAMEOUT[4]=0x1B;///////////////////////////////envoi du texte "veuillez patienter" en attendant la supression du capteur 
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x31;
TRAMEOUT[7]=0x32;
TRAMEOUT[8]=0x3B;
TRAMEOUT[9]=0x31;
TRAMEOUT[10]=0x32;
TRAMEOUT[11]=0x48;
TRAMEOUT[12]=0x20;
TRAMEOUT[13]=0x56;
TRAMEOUT[14]=0x65;
TRAMEOUT[15]=0x75;
TRAMEOUT[16]=0x69;
TRAMEOUT[17]=0x6C;
TRAMEOUT[18]=0x6C;
TRAMEOUT[19]=0x65;
TRAMEOUT[20]=0x7A;
TRAMEOUT[21]=0x20;
TRAMEOUT[22]=0x70;
TRAMEOUT[23]=0x61;
TRAMEOUT[24]=0x74;
TRAMEOUT[25]=0x69;
TRAMEOUT[26]=0x65;
TRAMEOUT[27]=0x6E;
TRAMEOUT[28]=0x74;
TRAMEOUT[29]=0x65;
TRAMEOUT[30]=0x72;
TRAMEOUT[31]=0x20;
TRAMEOUT[32]=0x20;
TRAMEENVOITCPDONNEES (33);
TCPFlush(MySocket);
   tmp=EFFACER_CAPTEUR(); ///210232017                             /////////////////////// sortir de la boucle quand le capteur sera supprimer //a ajouter 
}
else//21092020
{
ETAT1= F;
repet=0;
}
fin_sup:
delay_qms(10);



}


void creer_trame_horloge (void)  //CREATION D'UNR TRAME AVEC LES DONNEES DE L'HORLOGE JJMMsshhmmAA
{

char HEURE,MINUTE,SECONDE,MOIS,JOUR,ANNEE,DATE;


ANNEE=LIRE_HORLOGE(6);
delay_qms(10);
MOIS=LIRE_HORLOGE(5);
delay_qms(10);
DATE=LIRE_HORLOGE(4);
delay_qms(10);
JOUR=LIRE_HORLOGE(3);
delay_qms(10);
HEURE=LIRE_HORLOGE(2);
delay_qms(10);
MINUTE=LIRE_HORLOGE(1);
delay_qms(10);
SECONDE=LIRE_HORLOGE(0);

Tab_Horodatage[1] = (DATE & 0x0F) + 48;
   Tab_Horodatage[0] = ((DATE & 0x30) >> 4) + 48;      // conversion BCD => ASCII
 
   Tab_Horodatage[3] = (MOIS & 0x0F) + 48;  
  Tab_Horodatage[2] = ((MOIS & 0x10) >> 4) + 48;

 Tab_Horodatage[5] = (SECONDE & 0x0F) + 48;
     Tab_Horodatage[4] = ((SECONDE & 0x70) >> 4) + 48;
 
 Tab_Horodatage[7] = (HEURE & 0x0F) + 48; 
 Tab_Horodatage[6] = ((HEURE & 0x30) >> 4) + 48;      // conversion BCD => ASCII
  

Tab_Horodatage[9] = (MINUTE& 0x0F) + 48;
   Tab_Horodatage[8] = ((MINUTE & 0x70) >> 4) + 48;

   

 Tab_Horodatage[11] = (ANNEE & 0x0F) + 48;
   Tab_Horodatage[10] = ((ANNEE & 0xF0) >> 4) + 48;
  











//HORLOGE[12];




}




void infos_etats(char *y,char t) //ADDITIONNE LE TYPE D'ETAT

{
//x1 INTERVENTION 75-76
//ET_DEBITS=0;
//ET_GROUPE=0;
//ET_TP=0;
//ET_TR=0;
//if (t==0) //DEBIT

		if (t==1) // capteurs TP
		{
															if ((y[75]=='3')&&(y[76]=='0')) //ALARME A3	
															{
															 ET_TP[0]=ET_TP[0]+1; // ON INCREMENTE LE NOMBRE D'ALARME TP
															}
		
		
															if ((y[75]=='4')&&(y[76]=='0')) //H/G
															{
															 ET_TP[1]=ET_TP[1]+1; // ON INCREMENTE LE NOMBRE H/G TP
															}
		
		
				 											if ((y[75]=='5')&&(y[76]=='0')) //SANS REPONSE
															{
															 ET_TP[2]=ET_TP[2]+1; // ON INCREMENTE LE NOMBRE SR TP
															}													
		
		
															if (y[76]=='1') //INTERVENTION
															{
															 ET_TP[3]=ET_TP[3]+1; // ON INCREMENTE LE NOMBRE INTERVENTION TP
															}													
		
		
		
		
			}



		if (t==0) // capteurs TD
		{
															if ((y[75]=='3')&&(y[76]=='0'))  //ALARME A3	
															{
															 ET_TD[0]=ET_TD[0]+1; // ON INCREMENTE LE NOMBRE D'ALARME TD
															}
		
		
															if ((y[75]=='4')&&(y[76]=='0'))  //H/G
															{
															 ET_TD[1]=ET_TD[1]+1; // ON INCREMENTE LE NOMBRE H/G TP
															}
		
		
															if ((y[75]=='5')&&(y[76]=='0'))  //SANS REPONSE
															{
															 ET_TD[2]=ET_TD[2]+1; // ON INCREMENTE LE NOMBRE SR TP
															}													
		
		
															if (y[76]=='1') //INTERVENTION
															{
															 ET_TD[3]=ET_TD[3]+1; // ON INCREMENTE LE NOMBRE INTERVENTION TP
															}													
		
		
		
		
			}




				if (t==3) // capteurs TP resisitifs
		{
															if ((y[75]=='3')&&(y[76]=='0'))  //ALARME A3	
															{
															 ET_TR[0]=ET_TR[0]+1; // ON INCREMENTE LE NOMBRE D'ALARME TD
															}
		
		
															if ((y[75]=='4')&&(y[76]=='0'))  //H/G
															{
															 ET_TR[1]=ET_TR[1]+1; // ON INCREMENTE LE NOMBRE H/G TP
															}
		
		
															if ((y[75]=='5')&&(y[76]=='0'))  //SANS REPONSE
															{
															 ET_TR[2]=ET_TR[2]+1; // ON INCREMENTE LE NOMBRE SR TP
															}													
		
		
															if (y[76]=='1') //INTERVENTION
															{
															 ET_TR[3]=ET_TR[3]+1; // ON INCREMENTE LE NOMBRE INTERVENTION TP
															}													
		
		
		
		
			}

 
				if (t==4) //GROUPE
			{

		
															if ((y[0]=='3')&&(y[1]=='0'))  //ALARME A3	
															{
															 ET_GR[0]=ET_GR[0]+1; // ON INCREMENTE LE NOMBRE D'ALARME TD
															}
		
		
															if ((y[0]=='4')&&(y[1]=='0'))  //H/G
															{
															 ET_GR[1]=ET_GR[1]+1; // ON INCREMENTE LE NOMBRE H/G TP
															}
		
		
															if ((y[0]=='5')&&(y[1]=='0'))  //SANS REPONSE
															{
															 ET_GR[2]=ET_GR[2]+1; // ON INCREMENTE LE NOMBRE SR TP
															}													
		
		
															if (y[0]=='1') //INTERVENTION
															{
															 ET_GR[3]=ET_GR[3]+1; // ON INCREMENTE LE NOMBRE INTERVENTION TP
															}													
		
		
		



			}		





///////////////////////MANQUE LE GROUPE





}
 

//Ecriture dans l'EEPROM N�0 N�1 N�2 ou N�3 envoyer 000 ou 001......adresse haute et basse et une donn�e sur 8bits

unsigned char ECRIRE_EEPROMBYTE(unsigned char ADDE,unsigned int ADDHL,unsigned char *tab,char g)
{
unsigned char TMP2,TMP;
unsigned int h;
unsigned char t;
unsigned char AddH,AddL;

//MAXERIE A2 
if (ADDE==8)
ADDE=16;
if (ADDE==7)
ADDE=14;
if (ADDE==6)
ADDE=12;
if (ADDE==5)
ADDE=10;
if (ADDE==4)
ADDE=8;
if (ADDE==3)
ADDE=6;
if (ADDE==2)
ADDE=4;
if (ADDE==1)
ADDE=2;




TMP2=ADDE;
//			TMP2=TMP2<<1; 
			TMP2=0XA0|TMP2;

			
AddL=(ADDHL);
AddH=(ADDHL)>>8;			
			
if 	(g==1) // POUR FONCTIONNEMENT TRAME MEMOIRE		
tab[85]='*';   //FIN DE TRAME MEMOIRE			
if 	(g==3) // POUR FONCTIONNEMENT TRAME MEMOIRE		
tab[115]='*';   //FIN DE TRAME GROUPE
if 	(g==4) // POUR FONCTIONNEMENT TRAME MEMOIRE		
tab[23]='*';   //FIN DE TRAME EXISTANCE
if 	(g==5) // POUR FONCTIONNEMENT TRAME MEMOIRE		
tab[119]='*';   //FIN DE TRAME P�RAM1
if 	(g==6) // POUR FONCTIONNEMENT ALARME	
tab[101]='*';   //FIN DE TRAME ALARME
if (g == 7) // POUR FONCTIONNEMENT TRAME MEMOIRE
tab[99] = '*'; //FIN DE TRAME BLOC NOTES			
			 
  IdleI2C();                      // ensure module is idle
  StartI2C();                     // initiate START condition
  while ( SSPCON2bits.SEN );      // wait until start condition is over 
  WriteI2CC(TMP2);        // write 1 byte - R/W bit should be 0
  IdleI2C();                      // ensure module is idle
  WriteI2CC(AddH);            // write HighAdd byte to EEPROM 
  IdleI2C();                      // ensure module is idle
  WriteI2CC(AddL);             // write LowAdd byte to EEPROM
  IdleI2C();                      // ensure module is idle
  putstringI2C (tab);         // pointer to data for page write
  IdleI2C();                      // ensure module is idle
  StopI2C();                      // send STOP condition
  while ( SSPCON2bits.PEN );      // wait until stop condition is over 
  return ( 0 );                   // return with no error	
		
 				


    
		return 0;	// FIN DE BOUCLE a *

                     
        
        

 


}

void MODIFICATION_TABLE_DEXISTENCE(char *v,char eff,char etat) //eff pour effacer (eff=2 alors recopie) alors et etat ligne pour fusible HS ou CC ou surtension 
{

unsigned int y,z;
char fa;




v[0]='#';
y=100*(v[1]-48)+10*(v[2]-48)+(v[3]-48); //VOIE

y=128*y+30000;

LIRE_EEPROM(3,y,&TMP,4); //LIRE DANS EEPROM



 





z=10*(v[4]-48)+(v[5]-48); //CODAGE

if (eff==2) //RECOPIE
{
goto recop;
}



if (eff==0) //CREATION attention ici on cr�e le type capteur et le type de cable ic on a pas encore test� si le cable avant etait resistif ou r codage en 2 3 ....il faut juste 
{
TMP[z+3]=v[6]; //RECOPIE SI R OU A #00101R
fa=TMP[z+3];



}




// VOIR voie a 'v' qd il y a que des 'F' 
//ecrire_tab_presence_TPA;
// ATTENTION SI 
//if (TMP[z+3]=='v')
// OK ON PEUT
//if (TMP[z+3]=='a')
// OK ON PEUT SI v[6]=='A'
//if(TMP[z+3]=='r')
// OK ON PEUT SI v[6]=='v'



if (fa=='A')
{
TMP[2]='a';

}
if (fa=='R') //L'ADRESSABLE NE PEUT QUE EXISTER EN CODAGE1 !!!!!!!!!!!!!!!!!!!!!!
{
TMP[2]='r';
}



if (eff==1) //EFFACEMENT
{
TMP[z+3]='F'; //CAPTEUR EFFACEE DE LA TABLE DEXISTENCE
// VOIR voie a 'v' qd il y a que des 'F' 

if ((NBRECAPTEURS_CABLE==1)&&(TMP[z+3]=='F')) //SI IL N'Y A QUE UN CAPTEUR VARIABLES INIT DANS MENU6b et que on efface pas un autre cable if ((NBRECAPTEURS_CABLE==1)&&(TMP[z+3]!='F')) //SI IL N'Y A QUE UN CAPTEUR VARIABLES INIT DANS MENU6b et que on efface pas un autre cable
TMP[2]='v';  // ON EFFACE LE CABLE
 

 
}


recop:



if (etat==1)
{
TMP[2]='h';
}

if (etat==2)
{
TMP[2]='h';
}

if ((etat==4)&&(TMP[2]!='c'))
{
TMP[2]=TMP[21];
}

if ((etat==5)&&(TMP[2]!='c'))
{
TMP[2]=TMP[21];

}

if (etat==6)
{
TMP[2]='c';

}


if (etat==7)
{
TMP[2]='c';
}


if (etat==8)  // SCRUTATION POUR INHIBER DEFAUT CC
{
TMP[2]=TMP[21];
}




ecrire:
// ON RE ECRIT LE NUMERO DE VOIE DANS LA TABLE  //��a�AFF����������������* qd memoire vide
if (TMP[1]==0xFF) //VIDE
{
y=(y-10000)/128; //FORCEMENT UN ENTIER
TMP[0]='#';
TMP[1]=y;
y=128*y+30000;
}


//ecrire_tab_presence_TPR;
ECRIRE_EEPROMBYTE(3,y,&TMP,4);

delay_qms(10);
LIRE_EEPROM(3,y,&TMP,4); //LIRE DANS EEPROM pour verifier



}

//////////////////////////////////////////////////////////////////
// RECEPTION D'UNE TRAME COMPLETE RS485          		   		//
//////////////////////////////////////////////////////////////////

//exemple #R1CMDIC03300R111122223333BRA/EEEE/22/1COMMENTAIRESICIROB0332611152500102000045678110865088822222*

// STOCKAGE DU RESULTAT DANS LA TRAMEIN[]


void TRAMERECEPTIONRS485DONNEES (char tout) 
{
unsigned char u;
unsigned char tmp;


tmp=0;

do
{
ret1:
LEDD=!LEDD;
		u=RECEVOIR_RS485(tout);

		if (tmp==0) // erreur trame 1 et 2 existence on recoit '.' ou autre d'abord puis '#' sur le premier caract
		{
		if (u=='$')
		goto tramedecal; 	
		

		if((u!='#')&&(NORECEPTION!=0xff))				
		goto ret1;
		}
		
tramedecal:
		TRAMEIN[tmp]=u;
			
		tmp=tmp+1;
}
while ((u!='$')&&(u!='*') && (tmp<200) && (NORECEPTION!=0xff));

LEDD=0; 
delay_qms(10);
 
}
  
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UNIQUEMENT POUR CARTE MERE ET A LA DEMANDE DE LA CARTE MERE (LORS d'une PREMIERE TRAME CM-->Rx  et  erreurs)  //
//ECRIRE 1 pour carte R1 ,2 pour R2 etc..................                           
//puis le numero de la fonction exemple 

// pour DIC=68*2+73+67=276 (avec code decimal des carac ascii) DEMANDE INFOS CAPTEUR A LA CARTE RELAIS	//	
//puis le tableau contenant les donn�es a envoyer suivant le code de la fonction																								                //
// le resultat est stock� dans la TRAMEOUT[]
//////////////////////////////////////////////////////////////////////////////////////////////////////


 

void ENVOIE_DES_TRAMES (char R,int fonctionRS,char *tab) 


 


{

int t;
unsigned int funct;
int y;
char u;
float h;

ID1[0]='I';
ID1[1]='O';
START[0]='#';
STOP[0]='*'; 
	switch(R)  //analyse de la fonction
		
		{
		 

			case 0 :  // R1
			ID2[0]='C';
			ID2[1]='M';
			break;


			case 1 :  // R1
			ID2[0]='R';
			ID2[1]='1';
			break;
				case 2 :  // R2
			ID2[0]='R';
			ID2[1]='2';
			break;
				case 3 :  // R3
			ID2[0]='R';
			ID2[1]='3';
			break;
				case 4 :  // R4
			ID2[0]='R';
			ID2[1]='4';
			break; 
				case 5 :  // R5
			ID2[0]='R';
			ID2[1]='5';
			break; 	

						case 6 :  // R5
			ID2[0]='A';
			ID2[1]='U';
			break; 	

			case 7 :  // IO
			ID2[0]='I';
			ID2[1]='O';
			break; 	
			
			default :
	 	
						
			break;
		
		}

 

	switch(fonctionRS)
	{

			case 319 : //RAZ CMS PA COM RS485
			

			break;

		
			case 302 :  // ERREUR


			FONCTION[0]='E';
			FONCTION[1]='R';
			FONCTION[2]='R';
			TRAMEOUT[0]=START[0];
			TRAMEOUT[1]=ID1[0];
			TRAMEOUT[2]=ID1[1];
			TRAMEOUT[3]=ID2[0];
			TRAMEOUT[4]=ID2[1];
			TRAMEOUT[5]=FONCTION[0];
			TRAMEOUT[6]=FONCTION[1];
			TRAMEOUT[7]=FONCTION[2];
			//TRAMEOUT[8]=tab[0]; 
			//TRAMEOUT[9]=tab[1];
				
								for (t=8;t<28;t++)
								{		
								TRAMEOUT[t]=tab[t-8]; 
								}
							
			pointeur_out_in=28;
			calcul_CRC(); //A COMPLETER
			TRAMEOUT[pointeur_out_in+5]=STOP[0];
			//PORTHbits.RH5=1; // demande d'interrogation carte mere
			TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE 
			//REPOSAQUIENVOYER (&ID2);
			//PORTHbits.RH5=0; // demande d'interrogation carte mere  22/04/2018
			funct = 0;	
			
			break;

			case 328:
			ACTIONENCOURS=0XDD; // ACTION ZBR
			INTCONbits.GIE= 0; // INTERRUPTIONS GENERALES
			FONCTION[0]='Z';
			FONCTION[1]='B';
			FONCTION[2]='R';
			TRAMEOUT[0]=START[0];
			TRAMEOUT[1]=ID1[0];
			TRAMEOUT[2]=ID1[1];
			TRAMEOUT[3]=ID2[0];
			TRAMEOUT[4]=ID2[1];
			TRAMEOUT[5]=FONCTION[0];
			TRAMEOUT[6]=FONCTION[1];
			TRAMEOUT[7]=FONCTION[2];
			TRAMEOUT[8]=tab[0];
			TRAMEOUT[9]=tab[1];
			TRAMEOUT[10]='x';
			TRAMEOUT[11]='x';
			TRAMEOUT[12]='x';
			TRAMEOUT[13]='x';
			TRAMEOUT[14]='*';
			
			PORTHbits.RH5=1; // demande d'interrogation carte mere
			TRAMERECEPTIONRS485DONNEES (18);  // RECOIT 
			if ((TRAMEIN[3]!='I')&&(TRAMEIN[4]!='O'))
			goto finBZ;

			delay_qms(100);
			TRAMEENVOIRS485DONNEES (); // ENVOI L'ORDRE ZBR LA CARTE MERE
			PORTHbits.RH5=0; // demande d'interrogation carte mere
			if ((TRAMEOUT[8]=='9')&&	(TRAMEOUT[8]=='9'))
			{
			
			for (tmp2=1;tmp2<22;tmp2++)
			{
						
						TRAMERECEPTIONRS485DONNEES (18);  // SE MET ENRECPETION POUR ATTENDRE LA REPONSE TRAME 1
					
						TRAMEIN[100]=10*(TRAMEIN[8]-48);
						TRAMEIN[101]=(TRAMEIN[9]-48);
      			    	  calculBLOC=TRAMEIN[100]+TRAMEIN[101];
						if ((calculBLOC>7)&&(calculBLOC<=17))
						calculBLOC=calculBLOC-3;
						if ((calculBLOC>17)&&(calculBLOC<=27))	
						calculBLOC=calculBLOC-6;

						calculBLOC=7168+(calculBLOC-1)*128;
						ECRIRE_EEPROMBYTE(6,calculBLOC, &TRAMEIN, 7); //ECRITURE EN EEPROM LIGNE 1 85 caract

				
						TRAMEOUT[0]!='$'; 

						delay_qms(100);					
						LIRE_EEPROM(6,calculBLOC, &TRAMEOUT, 7); //LIRE 85caract									
								if ((TRAMEOUT[0]!='#') || (TRAMEOUT[8]<'0') || (TRAMEOUT[8]>'2') || (TRAMEOUT[9]<'0') || (TRAMEOUT[9]>'9')) 
								{		
										TRAMEOUT[0]=START[0];
										TRAMEOUT[1]=ID1[0];
										TRAMEOUT[2]=ID1[1];
										TRAMEOUT[3]=ID2[0];
										TRAMEOUT[4]=ID2[1];
										TRAMEOUT[5]='E';
										TRAMEOUT[6]='R';
										TRAMEOUT[7]='R';
										TRAMEOUT[8]='F'; 
										TRAMEOUT[9]='F'; 
								}
								else
								{
										TRAMEOUT[0]=START[0];
										TRAMEOUT[1]=ID1[0];
										TRAMEOUT[2]=ID1[1];
										TRAMEOUT[3]=ID2[0];
										TRAMEOUT[4]=ID2[1];
										TRAMEOUT[5]='E';
										TRAMEOUT[6]='R';
										TRAMEOUT[7]='R';
										TRAMEOUT[8]='0'; 
										TRAMEOUT[9]='0'; 


								}

			 
															for (t=10;t<28;t++)
															{		
															TRAMEOUT[t]='x'; 
															}
														
										pointeur_out_in=28;
										calcul_CRC(); //A COMPLETER
										TRAMEOUT[pointeur_out_in+5]=STOP[0];
										//PORTHbits.RH5=1; // demande d'interrogation carte mere
										TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE 
												
			}
			
			}
			else
			{
			//PORTHbits.RH5=1; // demande d'interrogation carte mere
	
			//TEMPO(1);
			TRAMERECEPTIONRS485DONNEES (18);  // SE MET ENRECPETION POUR ATTENDRE LA REPONSE TRAME 1
			controle_CRC();	
			//PORTHbits.RH5=0; // demande d'interrogation carte mere
			}
finBZ:
			PORTHbits.RH5=0;
			ACTIONENCOURS=0X00; // ACTION BCZ
			INTCONbits.GIE= 1; //AUTORISER LES INTERRUPTIONS GENERALES


			break;


			case 324:
			INTCONbits.GIE= 0; //AUTORISER LES INTERRUPTIONS GENERALES

					ACTIONENCOURS=0XBB; // ACTION ZIG 
			FONCTION[0]='Z';
			FONCTION[1]='I';
			FONCTION[2]='G';
			TRAMEOUT[0]=START[0];
			TRAMEOUT[1]=ID1[0];
			TRAMEOUT[2]=ID1[1];
			TRAMEOUT[3]=ID2[0];
			TRAMEOUT[4]=ID2[1];
			TRAMEOUT[5]=FONCTION[0];
			TRAMEOUT[6]=FONCTION[1];
			TRAMEOUT[7]=FONCTION[2];
			
			TRAMEOUT[8]=tab[0];
			TRAMEOUT[9]=tab[1];
			TRAMEOUT[10]=tab[2];
			TRAMEOUT[11]=tab[3];
			
			pointeur_out_in=12;
			calcul_CRC(); //A COMPLETER
			TRAMEOUT[pointeur_out_in+5]=STOP[0];


							tmp=0;
			//goto dieze;

		tmp2=0;
TRAMEIN[0]='x'; //REINIT
TRAMEIN[1]='x'; //REINIT
TRAMEIN[2]='x'; 
TRAMEIN[3]='x'; 
TRAMEIN[4]='x'; 



		PORTHbits.RH5=1; // demande d'interrogation carte mere
	
repeterZIG:

			//TEMPO(1);
			 
								
					TRAMERECEPTIONRS485DONNEES (18);  // SI ERR DD ON ARRETE
supertrame:
					
					
									if (IO_EN_COM==1) 
										goto STOPPERZIG;
	
									if ((TRAMEIN[3]=='I')&&(TRAMEIN[4]=='O')) 
								{
										if 	((TRAMEIN[8]=='D')&&(TRAMEIN[9]=='D')) //CM OCCUPEE 
										{
										PORTHbits.RH5=0;
										TEMPO(5); // ON ATTEND QUE CARTE ri RECOIT AA	
										goto STOPPERZIG; //SI LA CARTE MERE EST OCCUPEE ALORS ON ARRETE 
										}
										if 	((TRAMEIN[8]=='0')&&(TRAMEIN[9]=='0')) //CM OK 
										{
										// OK	//SI OK ON CONTINUE
											delay_qms(100); //attend un peu avant d'envoyer

										}
									
									
								}

					else
								{
								tmp2++;	
								
										if (tmp2==2) //20 si CM->Rx et Rx->CM table existence ou autre  
										goto STOPPERZIG; //sinon ON ARRETE
									
					

								//PORTHbits.RH5=0;
								goto repeterZIG;
									
								}
											


					
					delay_qms(100); //attend un peu avant d'envoyer

					TRAMEENVOIRS485DONNEES (); // ENVOI L'ORDRE ZIG A LA CARTE MERE
	
						funct=0;	
						do		

						{	
								funct =funct+1;
								TRAMERECEPTIONRS485DONNEES (9);  // SE MET ENRECPETION POUR ATTENDRE LA REPONSE TRAME 1
							//	controle_CRC();	
								pointeur_out_in=115;
								controle_CRC2021();
								if (IO_EN_COM==1) 
								goto STOPPERZIG;

							
						
								if (ETAT_CONNECT_MINITEL==0xFF) // SI IL Y A EU UNE INTERRUPTION AVANT ON QUITTE AVANT DE REPRENDRE
								goto STOPPERZIG;
						if ((TRAMEIN[1] == 'C')&&(TRAMEIN[2] == 'M')&&(TRAMEIN[3] == 'I')&&(TRAMEIN[4] == 'O')) //UNIQUEMENT LE GROUPE
         				{		

								if ((funct == 1)&&(TRAMEIN[19] == '0')&&(TRAMEIN[20] == '0')&&(TRAMEIN[21] == '1')&&(TRAMEIN[120] == '*'))
                    			{
								GROUPE_EN_MEMOIRE(TRAMEIN,0) ;
								}

								if ((funct == 2)&&(TRAMEIN[19] == '0')&&(TRAMEIN[20] == '0')&&(TRAMEIN[21] == '4')&&(TRAMEIN[120] == '*'))
                    			{
								GROUPE_EN_MEMOIRE(TRAMEIN,1) ;
								}

								if ((funct == 3)&&(TRAMEIN[19] == '0')&&(TRAMEIN[20] == '0')&&(TRAMEIN[21] == '9')&&(TRAMEIN[120] == '*'))
                    			{
								GROUPE_EN_MEMOIRE(TRAMEIN,2) ;
								}



							if (funct==3) //3ieme TRAME CONTIENT LE TYPE DE CARTE
								{
								MAJBLOC=TRAMEIN[70];
								TYPECARTE[0]=TRAMEIN[110];
								TYPECARTE[1]=TRAMEIN[111];
								TYPECARTE[2]=TRAMEIN[112];	
								TYPECARTE[3]=TRAMEIN[113];
								TYPECARTE[4]=TRAMEIN[114];
								}								

  						}
									//delay_qms(100); //attend un peu avant d'envoyer	
									INFORMATION_ERREUR[0]='0'; 
									INFORMATION_ERREUR[1]='0';			
								
								
									for (t=2;t<20;t++)
									{
									INFORMATION_ERREUR[t]='x';
									}
//1107									ENVOIE_DES_TRAMES (0,302,&INFORMATION_ERREUR); // ON ENVOIE A CARTE 1 ICI voir les autres suivant demande
										FONCTION[0]='E';
										FONCTION[1]='R';
										FONCTION[2]='R';
										TRAMEOUT[0]=START[0];
										TRAMEOUT[1]=ID1[0];
										TRAMEOUT[2]=ID1[1];
										TRAMEOUT[3]=ID2[0];
										TRAMEOUT[4]=ID2[1];
										TRAMEOUT[5]=FONCTION[0];
										TRAMEOUT[6]=FONCTION[1];
										TRAMEOUT[7]=FONCTION[2];
										TRAMEOUT[8]='0'; 
										TRAMEOUT[9]='0'; 
											 
															for (t=10;t<28;t++)
															{		
															TRAMEOUT[t]='x'; 
															}
														
										pointeur_out_in=28;
										TRAMEOUT[pointeur_out_in]=CRC2021[0];
										TRAMEOUT[pointeur_out_in+1]=CRC2021[1];
										TRAMEOUT[pointeur_out_in+2]=CRC2021[2];
										TRAMEOUT[pointeur_out_in+3]=CRC2021[3];
										TRAMEOUT[pointeur_out_in+4]=CRC2021[4];	


										//calcul_CRC(); //A COMPLETER
										TRAMEOUT[pointeur_out_in+5]=STOP[0];
										//PORTHbits.RH5=1; // demande d'interrogation carte mere
										TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE 

						}
						while(funct<3);
				
		
						










 

			STOPPERZIG:
		ETAT_CONNECT_MINITEL==0x00; 
		
				ACTIONENCOURS=0X00; // ACTION ZTE
			INTCONbits.GIE= 1; //AUTORISER LES INTERRUPTIONS GENERALES
			PORTHbits.RH5=0; // demande d'interrogation carte mere
			funct = 0;	






			break;


			case 341: //ZOR DEMANDE DE REGLAGE DE L'HEURE A LA CM

			ACTIONENCOURS=0X77; // ACTION ZIG 
			FONCTION[0]='Z';
			FONCTION[1]='O';
			FONCTION[2]='R';
			TRAMEOUT[0]=START[0];
			TRAMEOUT[1]=ID1[0];
			TRAMEOUT[2]=ID1[1];
			TRAMEOUT[3]=ID2[0];
			TRAMEOUT[4]=ID2[1];
			TRAMEOUT[5]=FONCTION[0];
			TRAMEOUT[6]=FONCTION[1];
			TRAMEOUT[7]=FONCTION[2];
			
			TRAMEOUT[8]=tab[0];
			TRAMEOUT[9]=tab[1];
			TRAMEOUT[10]=tab[2];
			TRAMEOUT[11]=tab[3];
			
			pointeur_out_in=12;
			calcul_CRC(); //A COMPLETER
			TRAMEOUT[pointeur_out_in+5]=STOP[0];

 
		 	tmp2=0;
			PORTHbits.RH5=1; // demande d'interrogation carte mere
			//TEMPO(1);
			repeterZOR: 
								
					TRAMERECEPTIONRS485DONNEES (9);  // SI ERR DD ON ARRETE
					controle_CRC();	
									if ((TRAMEIN[3]=='I')&&(TRAMEIN[4]=='O')) 
								{
										if 	((TRAMEIN[8]=='D')&&(TRAMEIN[9]=='D')) //CM OCCUPEE 
										{
										PORTHbits.RH5=0;
										goto STOPPERZOR; //SI LA CARTE MERE EST OCCUPEE ALORS ON ARRETE 
										}
										if 	((TRAMEIN[8]=='0')&&(TRAMEIN[9]=='0')) //CM OCCUPEE 
										{
										// OK	//SI OK ON CONTINUE
										}
									


								} 

					else
								{
								tmp2++;	

										if (tmp2==30)
										goto STOPPERZOR; //SI ON CAPTE UNE TRAME NON DESTINEE 5 fois ALORS ON ARRETE LE BUS EST OCCUPE
										if ((TRAMEIN[1]=='C') && (TRAMEIN[3]=='A')) //TRAME #CMAUERR00xxxxxx DETECTEE
										goto STOPPERZOR;
										if ((TRAMEIN[0]=='*')) //on recoit juste *
										goto STOPPERZOR;
	
								goto repeterZOR;
									
								}
											


					
					delay_qms(100); //attend un peu avant d'envoyer

					TRAMEENVOIRS485DONNEES (); // ENVOI L'ORDRE ZOR LA CARTE MERE
	
						
								TRAMERECEPTIONRS485DONNEES (9);  // SE MET ENRECPETION POUR ATTENDRE LA REPONSE TRAME PARAMETRE
								controle_CRC();	
								
								
								if (ETAT_CONNECT_MINITEL==0xFF) // SI IL Y A EU UNE INTERRUPTION AVANT ON QUITTE AVANT DE REPRENDRE
								goto STOPPERZOR;
								I2CHORLG=0;  
								
								for (funct=0;funct<12;funct++)
								{	
								HORLOGET[funct]=TRAMEIN[8+funct];//hh mm ss JJ MM AA 								 	
								}	
								entrer_trame_horloge (); //ECRITURE 
								I2CHORLG=1;

	 						
					
									delay_qms(300); //attend un peu avant d'envoyer
					
									INFORMATION_ERREUR[0]='0'; //
									INFORMATION_ERREUR[1]='0';			
								
								
									for (t=2;t<20;t++)
									{
									INFORMATION_ERREUR[t]='x';
									}
//1107									ENVOIE_DES_TRAMES (0,302,&INFORMATION_ERREUR); // ON ENVOIE A CARTE M ICI voir les autres suivant demande
										FONCTION[0]='E';
										FONCTION[1]='R';
										FONCTION[2]='R';
										TRAMEOUT[0]=START[0];
										TRAMEOUT[1]=ID1[0];
										TRAMEOUT[2]=ID1[1];
										TRAMEOUT[3]=ID2[0];
										TRAMEOUT[4]=ID2[1];
										TRAMEOUT[5]=FONCTION[0];
										TRAMEOUT[6]=FONCTION[1];
										TRAMEOUT[7]=FONCTION[2];
										TRAMEOUT[8]='0'; 
										TRAMEOUT[9]='0'; 
											 
															for (t=10;t<28;t++)
															{		
															TRAMEOUT[t]='x'; 
															}
														
										pointeur_out_in=28;
										calcul_CRC(); //A COMPLETER
										TRAMEOUT[pointeur_out_in+5]=STOP[0];
										//PORTHbits.RH5=1; // demande d'interrogation carte mere
										TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE 
			
					
			
 

			LIRE_HEURE_MINITEL();	
				
		
						

 
 







 

			STOPPERZOR:
			ETAT_CONNECT_MINITEL==0xFF;
		
				ACTIONENCOURS=0X00; // ACTION ZTE
			INTCONbits.GIE= 1; //AUTORISER LES INTERRUPTIONS GENERALES
			PORTHbits.RH5=0; // demande d'interrogation carte mere
			funct = 0;	





			break;







			case 336: //ZPL 
			INTCONbits.GIE= 0;			

							ACTIONENCOURS=0XBB; // ACTION ZIG 
			FONCTION[0]='Z';
			FONCTION[1]='P';
			FONCTION[2]='L';
			TRAMEOUT[0]=START[0];
			TRAMEOUT[1]=ID1[0];
			TRAMEOUT[2]=ID1[1];
			TRAMEOUT[3]=ID2[0];
			TRAMEOUT[4]=ID2[1];
			TRAMEOUT[5]=FONCTION[0];
			TRAMEOUT[6]=FONCTION[1];
			TRAMEOUT[7]=FONCTION[2];
			
			TRAMEOUT[8]=tab[0];
			TRAMEOUT[9]=tab[1];
			TRAMEOUT[10]=tab[2];
			TRAMEOUT[11]=tab[3];
			
			pointeur_out_in=12;
			calcul_CRC(); //A COMPLETER
			TRAMEOUT[pointeur_out_in+5]=STOP[0];

			tmp2=0;
			tmp=0;
			//goto dieze;


  


			PORTHbits.RH5=1; // demande d'interrogation carte mere
			//TEMPO(1);
			repeterZPL: 
							
					TRAMERECEPTIONRS485DONNEES (9);  // SI ERR DD ON ARRETE
					controle_CRC();	



									if ((TRAMEIN[3]=='I')&&(TRAMEIN[4]=='O')) 
								{
										if 	((TRAMEIN[8]=='D')&&(TRAMEIN[9]=='D')) //CM OCCUPEE 
										{
										PORTHbits.RH5=0;
										TEMPO(5); // ON ATTEND QUE CARTE ri RECOIT AA
										goto STOPPERZPL; //SI LA CARTE MERE EST OCCUPEE ALORS ON ARRETE 
										}
											
										if 	((TRAMEIN[8]=='0')&&(TRAMEIN[9]=='0')) //CM OCCUPEE 
										{
										// OK	//SI OK ON CONTINUE
										}
									


								}

					else
								{
								tmp2++;	
								
										if (tmp2==30)
										goto STOPPERZPL; //SI ON CAPTE UNE TRAME NON DESTINEE 5 fois ALORS ON ARRETE LE BUS EST OCCUPE
										if ((TRAMEIN[1]=='C') && (TRAMEIN[3]=='A')) //TRAME #CMAUERR00xxxxxx DETECTEE
										goto STOPPERZPL;
										if ((TRAMEIN[0]=='*')) //on recoit juste *
										goto STOPPERZOR;	
								goto repeterZPL;
									
								}
											
  

					
					delay_qms(100); //attend un peu avant d'envoyer

					TRAMEENVOIRS485DONNEES (); // ENVOI L'ORDRE ZIG A LA CARTE MERE
	
						
								TRAMERECEPTIONRS485DONNEES (18);  // SE MET ENRECPETION POUR ATTENDRE LA REPONSE TRAME PARAMETRE
								controle_CRC();
								
								
								if (ETAT_CONNECT_MINITEL==0xFF) // SI IL Y A EU UNE INTERRUPTION AVANT ON QUITTE AVANT DE REPRENDRE
								goto STOPPERZPL;	

								if ((TRAMEIN[5]=='E')&&(TRAMEIN[6]=='R')) //SI ERR SOIT INTERRUPTION AUTOMATE CAPTEE
								goto STOPPERZPL;	
								ECRIRE_EEPROMBYTE(6,1024,&TRAMEIN,5); //ENREGISTRER PARAM. EEPROM		
								ZPLENVOYEE=ZPLENVOYEE+1; //26102020
									delay_qms(300); //attend un peu avant d'envoyer

									LIRE_EEPROM(6,1024,&TRAMEOUT,5);
					
				
		
						










 

			STOPPERZPL:
			ETAT_CONNECT_MINITEL==0x00;
		
				ACTIONENCOURS=0X00; // ACTION ZTE
			INTCONbits.GIE= 1; //AUTORISER LES INTERRUPTIONS GENERALES
			PORTHbits.RH5=0; // demande d'interrogation carte mere
			funct = 0;	

						




			break;






			case 333 :  // ZTE
			INTCONbits.GIE= 1;
				ACTIONENCOURS=0XEE; // ACTION ZIZ
			FONCTION[0]='Z';
			FONCTION[1]='T';
			FONCTION[2]='E';
			TRAMEOUT[0]=START[0];
			TRAMEOUT[1]=ID1[0];
			TRAMEOUT[2]=ID1[1];
			TRAMEOUT[3]=ID2[0];
			TRAMEOUT[4]=ID2[1];
			TRAMEOUT[5]=FONCTION[0];
			TRAMEOUT[6]=FONCTION[1];
			TRAMEOUT[7]=FONCTION[2];
			
			TRAMEOUT[8]=tab[0];
			TRAMEOUT[9]=tab[1];





			pointeur_out_in=10;
			calcul_CRC(); //A COMPLETER
			TRAMEOUT[pointeur_out_in+5]=STOP[0];
		//	PORTHbits.RH5=1; // demande d'interrogation carte mere

			//TEMPO(1); //attend un peu avant d'envoyer

	//		TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE 
			//REPOSAQUIENVOYER (&ID2);
		//	PORTHbits.RH5=0;

									tmp2=0; //SYNCHRONISATION ON ATTEND "%" debut supertrame
			tmp=0;
			//goto dieze;





PORTHbits.RH5=1; // demande d'interrogation carte mere
//TEMPO(1);
repeterZTE:
				
					
				
					
					TRAMERECEPTIONRS485DONNEES (9);  // SI ERR DD ON ARRETE
					controle_CRC();	
					
											
					if ((TRAMEIN[3]=='I')&&(TRAMEIN[4]=='O')) 
								{
										if 	((TRAMEIN[8]=='D')&&(TRAMEIN[9]=='D')) //CM OCCUPEE 
										{
										PORTHbits.RH5=0;
										goto STOPPERZTE; //SI LA CARTE MERE EST OCCUPEE ALORS ON ARRETE 
										}
										if 	((TRAMEIN[8]=='0')&&(TRAMEIN[9]=='0')) //CM OCCUPEE 
										{
										// OK	//SI OK ON CONTINUE
										}
									


								}

					else
								{
								tmp2++;	

										if (tmp2==30)
										goto STOPPERZTE; //SI ON CAPTE UNE TRAME NON DESTINEE 5 fois ALORS ON ARRETE LE BUS EST OCCUPE

								goto repeterZTE;
									
								} 
						
								

					//if (NORECEPTION==0xFF) // RIEN REcU
					//			{
					//			tmp2=tmp2+1;
					//			
					//			PORTHbits.RH5=0; // demande d'interrogation carte mere
								//delay_qms(300); //attend un peu avant d'envoyer
					//			goto repeterZTE;
					//			}
							


					delay_qms(100); //attend un peu avant d'envoyer

					TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE MERE
	





tmp2=0;


rep3:
			if (tmp2<=3) //3 ESSAIS pour le % de la super trame
				
					{
					//PORTHbits.RH5=1; // demande d'interrogation carte mere
					//TEMPO(1);
	
		//			PORTHbits.RH5=0; // demande d'interrogation carte mere
					//TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE 
dieze:
					tmp=RECEVOIR_RS485(9); // ON ATTEND '%'
					
								if (NORECEPTION==0xFF) // RIEN REcU
								{
								tmp2=tmp2+1;
								
								//PORTHbits.RH5=0; // demande d'interrogation carte mere
								//delay_qms(300); //attend un peu avant d'envoyer
								goto rep3;
								}
					}
					else			
					{
					if (tmp=='%')
					break;
					}
					


 			
			
//RECEPTION DES TRAMES EXISTANCES 

			funct = 0;

								do		
					
								{	
								
								TRAMERECEPTIONRS485DONNEES (9);  // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
									
							
							if (ETAT_CONNECT_MINITEL==0xFF) // SI IL Y A EU UNE INTERRUPTION AVANT ON QUITTE AVANT DE REPRENDRE
							goto STOPPERZTE;

								controle_CRC();	
							
								funct =funct+1;
								if (TRAMEIN[0]!='$')
								SAV_EXISTENCE(&TRAMEIN); 
					
							//	recuperer_trame_memoire (1); //STOCKAGE EN MMOIRE DE L'INFORMATION DU CAPTEUR 
					
								if (funct==25) //BLOQUE
								{
					
							
								TRAMEIN[0]='$';
					
								}
								
					 
					 
								}
					 
								while (TRAMEIN[0]!='$'); //FIN ATTENTION SI ON BLOQUE IL FAUT ATTENDRE 25 TRAMES D'AILLEURS

			


			funct = 0;
//			 LECTURE DES TABLES
			for (funct=0;funct<100;funct++)
			{
			LIRE_EEPROM(3,128*funct+30000,&TRAMEOUT,4); //LIRE DANS EEPROM
			TRAMEIN[0]!='$';
			}


STOPPERZTE:
				ETAT_CONNECT_MINITEL==0xFF;
		
				ACTIONENCOURS=0X00; // ACTION ZTE
			INTCONbits.GIE= 1; //AUTORISER LES INTERRUPTIONS GENERALES
			PORTHbits.RH5=0; // demande d'interrogation carte mere
			funct = 0;	



	



















 



			break;

				case 334: //ZRH	REGLER HORLOGE

					FONCTION[0]='Z';
			FONCTION[1]='R';
			FONCTION[2]='H';
			TRAMEOUT[0]=START[0];
			TRAMEOUT[1]=ID1[0];
			TRAMEOUT[2]=ID1[1];
			TRAMEOUT[3]=ID2[0];
			TRAMEOUT[4]=ID2[1];
			TRAMEOUT[5]=FONCTION[0]; 
			TRAMEOUT[6]=FONCTION[1];
			TRAMEOUT[7]=FONCTION[2];
			TRAMEOUT[8]=tab[0];
			TRAMEOUT[9]=tab[1];
			TRAMEOUT[10]=tab[2];
			TRAMEOUT[11]=tab[3];		
			TRAMEOUT[12]=tab[4];
			TRAMEOUT[13]=tab[5];
			TRAMEOUT[14]=tab[6];
			TRAMEOUT[15]=tab[7];
			TRAMEOUT[16]=tab[8];
			TRAMEOUT[17]=tab[9];
			TRAMEOUT[18]=tab[10];
			TRAMEOUT[19]=tab[11];

			pointeur_out_in=20;
			calcul_CRC(); //A COMPLETER
			TRAMEOUT[pointeur_out_in+5]=STOP[0];


			//pointeur_out_in=10;
			//calcul_CRC(); //A COMPLETER
			//TRAMEOUT[pointeur_out_in+5]=STOP[0];

			//PORTHbits.RH5=1; // demande d'interrogation carte mere
			//TEMPO(1); //attend un peu avant d'envoyer
tmp2=0;
TERMINEACTION=0;
repeterZRH: 
		PORTHbits.RH5=1; // demande d'interrogation carte mere			
					
			//	LEDC=1;
					
					TRAMERECEPTIONRS485DONNEES (18);  // on attend 
					controle_CRC();	
				//	LEDC=0;						
					if ((TRAMEIN[3]=='I')&&(TRAMEIN[4]=='O')) 
								{
										if 	((TRAMEIN[8]=='D')&&(TRAMEIN[9]=='D')) //CM OCCUPEE 
										{
										TERMINEACTION=3; //PROBLEME
										//PORTHbits.RH5=0;
										goto STOPPERZRH; //SI LA CARTE MERE EST OCCUPEE ALORS ON ARRETE 
										}
										if 	((TRAMEIN[8]=='0')&&(TRAMEIN[9]=='0')) //CM NON OCCUPEE 
										{
										// OK	//SI OK ON CONTINUE
										}
								


								}

					else // rien ou en r�alit� rien
								{
								tmp2++;	

										if (tmp2==5)
										{
										goto STOPPERZRH; //SI ON CAPTE UNE TRAME NON DESTINEE 5 fois ALORS ON ARRETE LE BUS EST OCCUPE
										TERMINEACTION=2;//PROBLEME
										}
								PORTHbits.RH5=0; // demande d'interrogation carte mere
								goto repeterZRH;
									
								}








			TRAMEIN[0]='-';

			
			TEMPO(1); // ON ATTEND 1S AVANT D'ENVOYER
			TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE 

					



encoreZRH:
					TRAMERECEPTIONRS485DONNEES (20);  // SI ERR FF TT EST OK
					controle_CRC();
					if 	(NORECEPTION==0xFF) // RIEN RECU	EN 12S
					{

					}
					else
					{
									if ((TRAMEIN[3]=='I')&&(TRAMEIN[4]=='O')) //SI TRAME PAS RECU PROBLEME
									{
									TERMINEACTION=1; //L'ACTION A ETE FAITE

													//OK
									}
									else
									{
									
									goto encoreZRH;
									}
					}

					



				
STOPPERZRH:
		
			
			funct = 0;
			ACTIONENCOURS=0X00; // ACTION REPOS
			//INTCONbits.GIE= 1; //AUTORISER LES INTERRUPTIONS GENERALES
			
			PORTHbits.RH5=0; // demande d'interrogation carte mere
				


				break;




				case 343 :  //ZIZ 
			
		
						//DEMANDE INFOS CAPTEUR A LA CARTE RELAIS exemple : 
						//	DEMANDE_INFOS_CAPTEURS[0]='0';
						//																					DEMANDE_INFOS_CAPTEURS[1]='0';
						//																					DEMANDE_INFOS_CAPTEURS[2]='1';
						//																					DEMANDE_INFOS_CAPTEURS[3]='0';
						//																					DEMANDE_INFOS_CAPTEURS[4]='0';
						//																					DEMANDE_INFOS_CAPTEURS[5]='0';
						//																					DEMANDE_INFOS_CAPTEURS[6]='0';
						//																					DEMANDE_INFOS_CAPTEURS[7]='3';
						//																					DEMANDE_INFOS_CAPTEURS[8]='0';
						//																					DEMANDE_INFOS_CAPTEURS[9]='0';


						//																					ENVOIE_DES_TRAMES (1,276,&DEMANDE_INFOS_CAPTEURS);


 
		INTCONbits.GIE= 1; //AUTORISER LES INTERRUPTIONS GENERALES
		ACTIONENCOURS=0XDD; // ACTION ZIZ


	

			FONCTION[0]='Z';
			FONCTION[1]='I';
			FONCTION[2]='Z';

			TRAMEOUT[0]=START[0];
			TRAMEOUT[1]=ID1[0];
			TRAMEOUT[2]=ID1[1];
			TRAMEOUT[3]=ID2[0];
			TRAMEOUT[4]=ID2[1];
			TRAMEOUT[5]=FONCTION[0];
			TRAMEOUT[6]=FONCTION[1];
			TRAMEOUT[7]=FONCTION[2];
			TRAMEOUT[8]=tab[0]; 
			TRAMEOUT[9]=tab[1];
			TRAMEOUT[10]=tab[2];
			TRAMEOUT[11]=tab[3];
			TRAMEOUT[12]=tab[4];
			if (MODERAPID==0)
			TRAMEOUT[13]='-';
 			else
			TRAMEOUT[13]='/';
			TRAMEOUT[14]=tab[5];
			TRAMEOUT[15]=tab[6];
			TRAMEOUT[16]=tab[7];
			TRAMEOUT[17]=tab[8];
			TRAMEOUT[18]=tab[9];


		  
		 //	INTCONbits.GIE= 0;
  
			pointeur_out_in=19;          // SAUVEGARDE POINTEUR DU TABLEAU
			calcul_CRC();     // CALCUL LE CRC ET LE RAJOUTE AU TABLEAU TRAMEOUT[]
			TRAMEOUT[pointeur_out_in+5]=STOP[0]; // RAJOUT LE BYTE DE STOP 
		



PORTHbits.RH5=1; // demande d'interrogation carte mere
//TEMPO(1);
//PORTHbits.RH5=0; 
repeterZIZ:
				
					
				
					
					TRAMERECEPTIONRS485DONNEES (18);  // SI ERR DD ON ARRETE
				
					
						if (IO_EN_COM==1)
						 {

						goto erreurcomIOCM;

						}				


					if ((TRAMEIN[3]=='I')&&(TRAMEIN[4]=='O')) 
								{
										if 	((TRAMEIN[8]=='D')&&(TRAMEIN[9]=='D')) //CM OCCUPEE 
										{
										//PORTHbits.RH5=0;
										TEMPO(5); // ON ATTEND QUE CARTE ri RECOIT AA



										goto STOPPERZIZ; //SI LA CARTE MERE EST OCCUPEE ALORS ON ARRETE 
										}
										


										if 	((TRAMEIN[8]=='0')&&(TRAMEIN[9]=='0')) //CM OCCUPEE 
										{

										// OK	//SI OK ON CONTINUE
										}
									


								}

					else
								{
								//tmp2++;	

								//		if (tmp2==5)
										goto STOPPERZIZ; //SI ON CAPTE UNE TRAME NON DESTINEE 5 fois ALORS ON ARRETE LE BUS EST OCCUPE

								//goto repeterZIZ;
									
								}
						
					 			

					//if (NORECEPTION==0xFF) // RIEN REcU
					//			{
					//			tmp2=tmp2+1;
					//			
					//			PORTHbits.RH5=0; // demande d'interrogation carte mere
								//delay_qms(300); //attend un peu avant d'envoyer
					//			goto repeterZTE;
					//			}
							


					delay_qms(100); //attend un peu avant d'envoyer AVANT IL Y  AVAIT 300 MAIS ??? PB TROP TARD

					TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE MERE





	
 
tmp2=0; //SYNCHRONISATION ON ATTEND "%" debut supertrame
tmp=0;
rep2:
			if (tmp2<=10) //10 ESSAIS
				
					{

					//TEMPO(1); //attend un peu avant d'envoyer
					//TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE 
					//PORTHbits.RH5=0; // 
					//TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE 
					tmp=RECEVOIR_RS485(9); // ON ATTEND '%'
					
						if (IO_EN_COM==1) 
						goto STOPPERZIZ;
	
								if (tmp!='%')
								{
								tmp2=tmp2+1;
									
								
								delay_qms(300); //attend un peu avant d'envoyer		
								RS485('?');
								goto rep2;
								}
					}
					else			
					{
					if (tmp=='%')
					break;
					}
					
	 				


			
 

				funct = 0;

//PORTHbits.RH5=0;

			do		

			{
			funct =funct+1;
			if (INTCON3bits.INT2IF) //L'AUTOMATE VEUT PARLER
			goto STOPPERZIZ;	
	 
			
			TRAMERECEPTIONRS485DONNEES (9);  // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
			pointeur_out_in=92;
			controle_CRC2021();	

			//controle_CRC();	
			ecouteTCPIP();
						if (IO_EN_COM==1) 
						goto erreurcomIOCM;
	 
			
			if ((TRAMEIN[0]=='#')||(TRAMEIN[0]=='$')) //PAS DESTINEE A LA CARTE IO R1CM
			funct=0;
			else
			goto STOPPERZIZ;

			if (TRAMEIN[3]!='I') //PAS DESTINEE A LA CARTE IO R1CM
			goto STOPPERZIZ;

						
			//GenericTCPServer();					

								
			if (ETAT_CONNECT_MINITEL==0xFF) // SI IL Y A EU UNE INTERRUPTION AVANT ON QUITTE AVANT DE REPRENDRE
			goto STOPPERZIZ;

			if (INTCON3bits.INT2IF) //L'AUTOMATE VEUT PARLER
			goto STOPPERZIZ;
			if (TRAMEIN[0]!='$') 
			{
			recuperer_trame_memoire (1); //STOCKAGE EN MMOIRE DE L'INFORMATION DU CAPTEUR A REMETTRE

				if (INTCON3bits.INT2IF) //L'AUTOMATE VEUT PARLER
				goto STOPPERZIZ;	
				INFORMATION_ERREUR[0]='0'; //
				INFORMATION_ERREUR[1]='0';			
			
			
				for (t=2;t<20;t++)
				{
				INFORMATION_ERREUR[t]='x';
				}
				if (INTCON3bits.INT2IF) //L'AUTOMATE VEUT PARLER
				goto STOPPERZIZ;
//1107				ENVOIE_DES_TRAMES (0,302,&INFORMATION_ERREUR); // ON ENVOIE A CARTE 1 ICI voir les autres suivant demande
					FONCTION[0]='E';
										FONCTION[1]='R';
										FONCTION[2]='R';
										TRAMEOUT[0]=START[0];
										TRAMEOUT[1]=ID1[0];
										TRAMEOUT[2]=ID1[1];
										TRAMEOUT[3]=ID2[0];
										TRAMEOUT[4]=ID2[1];
										TRAMEOUT[5]=FONCTION[0];
										TRAMEOUT[6]=FONCTION[1];
										TRAMEOUT[7]=FONCTION[2];
										TRAMEOUT[8]='0'; 
										TRAMEOUT[9]='0'; 
											 
															for (t=10;t<28;t++)
															{		
															TRAMEOUT[t]='x'; 
															}
							 							
										pointeur_out_in=28;
										TRAMEOUT[pointeur_out_in]=CRC2021[0];
										TRAMEOUT[pointeur_out_in+1]=CRC2021[1];
										TRAMEOUT[pointeur_out_in+2]=CRC2021[2];
										TRAMEOUT[pointeur_out_in+3]=CRC2021[3];
										TRAMEOUT[pointeur_out_in+4]=CRC2021[4];
										TRAMEOUT[pointeur_out_in+5]=STOP[0];
										//PORTHbits.RH5=1; // demande d'interrogation carte mere
										TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE 
			}

				
			//if (funct==2)
			//TRAMEOUT[0]='#';
			//DEMANDE_INFOS_CAPTEURS[0]='#';
			//DEMANDE_INFOS_CAPTEURS[1]='0';
			//DEMANDE_INFOS_CAPTEURS[2]='0';
			//DEMANDE_INFOS_CAPTEURS[3]='1';
			//DEMANDE_INFOS_CAPTEURS[4]='0';
			//DEMANDE_INFOS_CAPTEURS[5]='1';

			//LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM
			//TRAMEOUT[0]='#';
			SAV_CHG_ALARME(&TRAMEIN);	
			}


					
			

			while ((TRAMEIN[1]!='$')&&(TRAMEIN[0]!='$'));




erreurcomIOCM:
		//	if (IO_EN_COM==1)
		//	{
		//	RS485('$');
		//	RS485('$');
		//	RS485('$');
		//	}


STOPPERZIZ:



			
				
		
			PORTHbits.RH5=0; // demande d'interrogation carte mere
			INTCON3bits.INT2IF=0;
			funct = 0;
			ACTIONENCOURS=0XCC; // ACTION CREATION MODIFICATION
			ETAT_CONNECT_MINITEL=0x00;
			INTCONbits.GIE= 1; //AUTORISER LES INTERRUPTIONS GENERALES





			break; 



			case 329:

			FONCTION[0]='Z';
			FONCTION[1]='C';
			FONCTION[2]='R';
			TRAMEOUT[0]=START[0];
			TRAMEOUT[1]=ID1[0];
			TRAMEOUT[2]=ID1[1];
			TRAMEOUT[3]=ID2[0];
			TRAMEOUT[4]=ID2[1];
			TRAMEOUT[5]=FONCTION[0]; 
			TRAMEOUT[6]=FONCTION[1];
			TRAMEOUT[7]=FONCTION[2];
			
			//TRAMEOUT[8]=tab[0];
			//TRAMEOUT[9]=tab[1];

			//recuperer_trame_memoire (0); //TRANSFORME LA TRAMEIN EN FORMAT MEMOIRE SUR TRAMEOUT
			//intervertirOUT_IN();
			pointeur_out_in=8;
			TRANSFORMER_TRAME_MEM_EN_COM (); // TRANSFORME LA TRAMEIN MEMOIRE en TRAMEOUUT COM
			calcul_CRC(); //A COMPLETER
			TRAMEOUT[pointeur_out_in+5]=STOP[0];


			//pointeur_out_in=10;
			//calcul_CRC(); //A COMPLETER
			//TRAMEOUT[pointeur_out_in+5]=STOP[0];

			//PORTHbits.RH5=1; // demande d'interrogation carte mere
			//TEMPO(1); //attend un peu avant d'envoyer
tmp2=0;
TERMINEACTION=0;
repeterZCR: 
		PORTHbits.RH5=1; // demande d'interrogation carte mere			
					
			//	LEDC=1;
					
					TRAMERECEPTIONRS485DONNEES (18);  // on attend 
					controle_CRC();	
				//	LEDC=0;						
					if ((TRAMEIN[3]=='I')&&(TRAMEIN[4]=='O')) 
								{
										if 	((TRAMEIN[8]=='D')&&(TRAMEIN[9]=='D')) //CM OCCUPEE 
										{
										TERMINEACTION=3; //PROBLEME
										//PORTHbits.RH5=0;
										goto STOPPERZCR; //SI LA CARTE MERE EST OCCUPEE ALORS ON ARRETE 
										}
										if 	((TRAMEIN[8]=='0')&&(TRAMEIN[9]=='0')) //CM NON OCCUPEE 
										{
										// OK	//SI OK ON CONTINUE
										}
								


								}

					else // rien ou en r�alit� rien
								{
								tmp2++;	

										if (tmp2==5)
										{
										goto STOPPERZCR; //SI ON CAPTE UNE TRAME NON DESTINEE 5 fois ALORS ON ARRETE LE BUS EST OCCUPE
										TERMINEACTION=2;//PROBLEME
										}
								PORTHbits.RH5=0; // demande d'interrogation carte mere
								goto repeterZCR;
									
								}








			TRAMEIN[0]='-';

			
			TEMPO(1); // ON ATTEND 1S AVANT D'ENVOYER
			TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE 

					
					TRAMERECEPTIONRS485DONNEES (9);  // SI ERR 00 LA CARTE M A BIEN RECU LA TRAME


encoreZCR:
					TRAMERECEPTIONRS485DONNEES (20);  // SI ERR FF TT EST OK
					controle_CRC();
					if 	(NORECEPTION==0xFF) // RIEN RECU	EN 12S
					{

					}
					else
					{
									if ((TRAMEIN[3]=='I')&&(TRAMEIN[4]=='O')) //SI TRAME PAS RECU PROBLEME
									{
									TERMINEACTION=1; //L'ACTION A ETE FAITE

													//OK
									}
									else
									{
									
									goto encoreZCR;
									}
					}

					



				
STOPPERZCR:
		
			
			funct = 0;
			ACTIONENCOURS=0X00; // ACTION REPOS
			//INTCONbits.GIE= 1; //AUTORISER LES INTERRUPTIONS GENERALES
			
			PORTHbits.RH5=0; // demande d'interrogation carte mere
			//LEDC=1;
			//LEDD=1;



			break;

 

			case 320:

			FONCTION[0]='Z';
			FONCTION[1]='F';
			FONCTION[2]='F';
			TRAMEOUT[0]=START[0];
			TRAMEOUT[1]=ID1[0];
			TRAMEOUT[2]=ID1[1];
			TRAMEOUT[3]=ID2[0];
			TRAMEOUT[4]=ID2[1];
			TRAMEOUT[5]=FONCTION[0];
			TRAMEOUT[6]=FONCTION[1];
			TRAMEOUT[7]=FONCTION[2];
			
			TRAMEOUT[8]=DEMANDE_INFOS_CAPTEURS[0];
			
			TRAMEOUT[9]=DEMANDE_INFOS_CAPTEURS[1];

			TRAMEOUT[10]=DEMANDE_INFOS_CAPTEURS[2];

			TRAMEOUT[11]=DEMANDE_INFOS_CAPTEURS[3];

			TRAMEOUT[12]=DEMANDE_INFOS_CAPTEURS[4];

			pointeur_out_in=13;

			calcul_CRC(); //A COMPLETER
			TRAMEOUT[pointeur_out_in+5]=STOP[0];


			//pointeur_out_in=10;
			//calcul_CRC(); //A COMPLETER
			//TRAMEOUT[pointeur_out_in+5]=STOP[0];

			
tmp2=0;
TERMINEACTION=0;
PORTHbits.RH5=1; // demande d'interrogation carte mere
			
repeterZFF:	
		
			TRAMERECEPTIONRS485DONNEES (9);  // on attend ERR00
					controle_CRC();	
				//	LEDC=0;						
					if ((TRAMEIN[3]=='I')&&(TRAMEIN[4]=='O')) 
								{
										if 	((TRAMEIN[8]=='D')&&(TRAMEIN[9]=='D')) //CM OCCUPEE 
										{
										//PORTHbits.RH5=0;
										TERMINEACTION=3; //PROBLEME
										goto STOPPERZFF; //SI LA CARTE MERE EST OCCUPEE ALORS ON ARRETE 
										}
										if 	((TRAMEIN[8]=='0')&&(TRAMEIN[9]=='0')) //CM NON OCCUPEE 
										{
										// OK	//SI OK ON CONTINUE
										}			
								}

								else // rien ou en r�alit� rien
								{
								tmp2++;	

										if (tmp2==5)
										{
										goto STOPPERZFF; //SI ON CAPTE UNE TRAME NON DESTINEE 5 fois ALORS ON ARRETE LE BUS EST OCCUPE
										TERMINEACTION=2; //PROBLEME	
										}	
								//PORTHbits.RH5=0; // demande d'interrogation carte mere
								goto repeterZFF;
									
								}



			TRAMEIN[0]='-';

			
			TEMPO(1); // ON ATTEND 1S AVANT D'ENVOYER
			TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE 

					
					TRAMERECEPTIONRS485DONNEES (9);  // SI ERR 00 LA CARTE M A BIEN RECU LA TRAME
			



encoreZFF:
					TRAMERECEPTIONRS485DONNEES (20);  // SI ERR FF TT EST OK
					controle_CRC();
					if 	(NORECEPTION==0xFF) // RIEN RECU	EN 12S
					{

					}
					else
					{
									if ((TRAMEIN[3]=='I')&&(TRAMEIN[4]=='O')) //SI TRAME PAS RECU PROBLEME
									{


									TERMINEACTION=1; //L'ACTION A ETE FAITE

							





									//OK
									}
									else
									{
									
									goto encoreZFF;
									}
					}





STOPPERZFF:



			PORTHbits.RH5=0; // demande d'interrogation carte mere




			break;





































			default :
	 	
						
			break;

 



	}


}

void CREER_TRAMEIN_CAPTEUR_DU_MINITEL(void) //CREER LA TRAME QUE L'ON VA SAV DANS LEEPROM

{

											TRAMEIN[0]='#';
											
											
											TRAMEIN[1]=VOIECOD[0];
											TRAMEIN[2]=VOIECOD[1];
											TRAMEIN[3]=VOIECOD[2];
											TRAMEIN[4]=VOIECOD[3];
											TRAMEIN[5]=VOIECOD[4];
											 
											
											//ecrire dans I2C
											
											TRAMEIN[6]=TYPE[0];
											
											TRAMEIN[7]=CMOD[0];
											TRAMEIN[8]=CMOD[1];
											TRAMEIN[9]=CMOD[2];
											TRAMEIN[10]=CMOD[3];
											
											
											TRAMEIN[11]=CCON[0];
											TRAMEIN[12]=CCON[1];
											TRAMEIN[13]=CCON[2];
											TRAMEIN[14]=CCON[3];
											
											
											TRAMEIN[15]=CREP[0];
											TRAMEIN[16]=CREP[1];
											TRAMEIN[17]=CREP[2];
											TRAMEIN[18]=CREP[3];
											
											
											TRAMEIN[19]=CONSTITUTION[0];//info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEIN,1,i);
											TRAMEIN[20]=CONSTITUTION[1];
											TRAMEIN[21]=CONSTITUTION[2];
										TRAMEIN[22]=CONSTITUTION[3];
										TRAMEIN[23]=CONSTITUTION[4];
										TRAMEIN[24]=CONSTITUTION[5];
										TRAMEIN[25]=CONSTITUTION[6];
										TRAMEIN[26]=CONSTITUTION[7];
										TRAMEIN[27]=CONSTITUTION[8];
										TRAMEIN[28]=CONSTITUTION[9];
										TRAMEIN[29]=CONSTITUTION[10];
										TRAMEIN[30]=CONSTITUTION[11];
										TRAMEIN[31]=CONSTITUTION[12];



										TRAMEIN[32]=COMMENTAIRESAV[0];//info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEIN,1,i);
										TRAMEIN[33]=COMMENTAIRESAV[1];
										TRAMEIN[34]=COMMENTAIRESAV[2];
										TRAMEIN[35]=COMMENTAIRESAV[3];
										TRAMEIN[36]=COMMENTAIRESAV[4];
										TRAMEIN[37]=COMMENTAIRESAV[5];
										TRAMEIN[38]=COMMENTAIRESAV[6];
										TRAMEIN[39]=COMMENTAIRESAV[7];
										TRAMEIN[40]=COMMENTAIRESAV[8];
										TRAMEIN[41]=COMMENTAIRESAV[9];
										TRAMEIN[42]=COMMENTAIRESAV[10];
										TRAMEIN[43]=COMMENTAIRESAV[11];
										TRAMEIN[44]=COMMENTAIRESAV[12];		
										TRAMEIN[45]=COMMENTAIRESAV[13];	
										TRAMEIN[46]=COMMENTAIRESAV[14];	
										TRAMEIN[47]=COMMENTAIRESAV[15];	
										TRAMEIN[48]=COMMENTAIRESAV[16];	
										TRAMEIN[49]=COMMENTAIRESAV[17];	


	//CODAGE

										TRAMEIN[50]=CABLE[0];
										TRAMEIN[51]=CABLE[1];		
										TRAMEIN[52]=CABLE[2];	
										TRAMEIN[53]=JJ[0];	
										TRAMEIN[54]=JJ[1];	
										TRAMEIN[55]=MM[0];	
										TRAMEIN[56]=MM[1];	
										TRAMEIN[57]=HH[0];	
										TRAMEIN[58]=HH[1];	
										TRAMEIN[59]=mm[0];	
										TRAMEIN[60]=mm[1];	

										TRAMEIN[61]=ii[0];		









									
										TRAMEIN[62]=COD[0];
										TRAMEIN[63]=COD[1];
										
										//POS
										
										TRAMEIN[64]=POS[0]; //POS
										TRAMEIN[65]=POS[1]; //POS
										
										
										TRAMEIN[66]=iiii[0];
										TRAMEIN[67]=iiii[1];
										TRAMEIN[68]=iiii[2];
										TRAMEIN[69]=iiii[3];

	//DISTANCE
										TRAMEIN[70]=DISTANCE[0];  //info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEIN,1,i);
										TRAMEIN[71]=DISTANCE[1];
										TRAMEIN[72]=DISTANCE[2];
										TRAMEIN[73]=DISTANCE[3];
										TRAMEIN[74]=DISTANCE[4];


//ETAT

					
										TRAMEIN[75]=ETAT[0];
										TRAMEIN[76]=ETAT[1];
										
										//FREQUENCE
							
										TRAMEIN[77]=VALEUR[0];
										TRAMEIN[78]=VALEUR[1];
										TRAMEIN[79]=VALEUR[2];
										TRAMEIN[80]=VALEUR[3];
										
							
										TRAMEIN[81]=SEUIL[0];
										TRAMEIN[82]=SEUIL[1];
										TRAMEIN[83]=SEUIL[2];
										TRAMEIN[84]=SEUIL[3];

											























}

char EFFACER_CAPTEUR(void)

{
char t,u;
TERMINEACTION=0; //L'ACTION A FAIRE

if (CMouPAS==0)
TERMINEACTION=1; //AEF A SUPPRIMER


u=0;
do
{
TEMPO(5);
CREER_TRAMEIN_CAPTEUR_DU_MINITEL();//RECUPERE LES VARIABLES ET CREATION D'UNE TRAME EEPROM	
DEMANDE_INFOS_CAPTEURS[0]=TRAMEIN[1];
DEMANDE_INFOS_CAPTEURS[1]=TRAMEIN[2];
DEMANDE_INFOS_CAPTEURS[2]=TRAMEIN[3];
DEMANDE_INFOS_CAPTEURS[3]=TRAMEIN[4];
DEMANDE_INFOS_CAPTEURS[4]=TRAMEIN[5];





if (CMouPAS==1)
ENVOIE_DES_TRAMES (0,320,&TRAMEIN) ; //CREATION CAPTEUR SUIVANT TRA. MEMOIRE AEF

				if (TERMINEACTION==1)
				
				{
					 if (CMouPAS==1)
					 {	
				//VERSION AVEC CM AEF
				//----------------------------
				TRAMEIN[0]='#'; //ICI ON REM. TOUTE LA TRAME OUT COM 485 DU CAPTEUR EN IN FORMAT MEMOIRE #00102.....
				for (t=1;t<91;t++)
				{
				TRAMEIN[t]=TRAMEOUT[t+7];
				}
	 			//----------------------------
					}			
								


				MODIFICATION_TABLE_DEXISTENCE (&TRAMEIN,1,0); // ON EFFACE LE CAPTEUR DANS LA MEMOIRE TABLE EXIST DE L'IO
				u=99;
				ETAT1=F; //Pour gestion menu et retours menu 21092020
				repet=0; // idem

				}

if (TERMINEACTION==3)
u=u+1;

 
}
while (u<2); //si pas reuissi on refait une fois



if (TERMINEACTION!=1)
{
TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;
TRAMEOUT[2]=0x32;
TRAMEOUT[3]=0x4A;
TRAMEOUT[4]=0x1B;///////////////////////////////envoi du texte "A VERIFIER" � la fin de la suppression du capteur 
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x31;
TRAMEOUT[7]=0x32;
TRAMEOUT[8]=0x3B;
TRAMEOUT[9]=0x31;
TRAMEOUT[10]=0x33;
TRAMEOUT[11]=0x48;
TRAMEOUT[12]=0x20;
TRAMEOUT[13]='A';
TRAMEOUT[14]=0x20;
TRAMEOUT[15]='V';
TRAMEOUT[16]='E';
TRAMEOUT[17]='R';
TRAMEOUT[18]='I';
TRAMEOUT[19]='F';
TRAMEOUT[20]='I';
TRAMEOUT[21]='E';
TRAMEOUT[22]='R';

TRAMEENVOITCPDONNEES (23);	
										


}

else
{
TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;
TRAMEOUT[2]=0x32;
TRAMEOUT[3]=0x4A;
TRAMEOUT[4]=0x1B;///////////////////////////////envoi du texte "OK" � la fin de la supression du capteur 
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x31;
TRAMEOUT[7]=0x32;
TRAMEOUT[8]=0x3B;
TRAMEOUT[9]=0x31;
TRAMEOUT[10]=0x33;
TRAMEOUT[11]=0x48;
TRAMEOUT[12]=0x20;
TRAMEOUT[13]='O';
TRAMEOUT[14]='K';
TRAMEOUT[15]=' ';
TRAMEOUT[16]=' ';
TRAMEOUT[17]=' ';
TRAMEOUT[18]=' ';
TRAMEOUT[19]=' ';
TRAMEOUT[20]=' ';

IO_EN_COM=1; //2020
TRAMEENVOITCPDONNEES (21);	

															


}


TEMPO(2);												


									


TERMINEACTION=2;


}

unsigned char LIRE_HORLOGE(unsigned char zone) //LIRE LES DONNEES HORLOGE AUX CHOIX
{
unsigned char TMP;
TMP=litHORLOGE_i2c(0xD1,zone); 


if (zone==0)
TMP=TMP&0x7F; // supprimle le bit 7 seconde
if (zone==1)
TMP=TMP&0x7F; //laisse le bit7 a zero minutes
if (zone==2)
TMP=TMP&0x3F;//met a zero les bits 6 et 7 pour le mode 24h  
if (zone==3)
TMP=TMP&0x07; // laisse ts les bits sauf 0 1 2 a 0 
if (zone==4)
TMP=TMP&0x3F; // met a 0 les bits 7 et 6 date 
if (zone==5)
TMP=TMP&0x1F; // met a 0 les bits 7 et 6 et 5 mois
if (zone==6)
TMP=TMP;










return (TMP);
}




//////////////////////////////////////////////////////////////////
// reception d'un caract�re sur le port s�rie	avec tempo max	//
//////////////////////////////////////////////////////////////////
  
   
char RECEVOIR_RS485(char ttout)
{   
unsigned char Carac;
unsigned int TEMPS1,TEMPS2; 

ttout=2*ttout; //version IP
ERREURRS:
DERE=0; //AUTORISATION DE RECEVOIR
 // PORTCbits.RC5=1; //FIN DE TRANSMISSION     		 
PORTAbits.RA4=0;
RCSTA2bits.CREN  = 1; 
TEMPS1=0;
TEMPS2=0;  
NORECEPTION=0x00;

while ((PIR3bits.RC2IF == 0)&&(TEMPS2<(ttout+1))) // max 6s pour 10
//while ((TEMPS2<(ttout+1))) 
{
TEMPS1=TEMPS1+1;
if (TEMPS1>=50000) //ENVIRON 500ms
{ 
TEMPS2++; 
TEMPS1=0;
}
};       // attend une r�ception de caract�re sur RS232
//    __no_operation();

Carac = RCREG2;    // caract�re re�u
//if ((Carac==0x00)) // EXCEPTION NE PAS PRENDRE EN COMPTE LE CARACTERE <NUL> voir si origine croisement TX RX CM
//{
//goto ERREURRS; //24052017
//}
if (TEMPS2>=ttout)
{
NORECEPTION=0xff;
}
PIR3bits.RC2IF = 0;
RCSTA2bits.CREN  = 0; 
return(Carac);    // retourne le caract�re re�u sur la liaison RS232


}



void TRANSFORMER_TRAME_MEM_EN_COM (void)

{

int t;
int u;
u=8;
TRAMEOUT[0]='#';


VOIECOD[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
VOIECOD[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
VOIECOD[2]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
VOIECOD[3]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
VOIECOD[4]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;


//ecrire dans I2C

TYPE[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;


CMOD[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CMOD[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CMOD[2]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CMOD[3]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;


CCON[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CCON[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CCON[2]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CCON[3]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;



CREP[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CREP[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CREP[2]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CREP[3]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;

 
   
 

t=0;
for (t=0;t<13;t++) 
{
CONSTITUTION[t]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;

}
 
t=0;
for (t=0;t<18;t++) 
{
COMMENTAIRE[t]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
}
 
        

CABLE[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CABLE[1]==TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CABLE[2]==TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;



JJ[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
JJ[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;


MM[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
MM[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;


HH[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
HH[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;


mm[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
mm[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;



iiii[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;



COD[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
COD[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;




POS[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
POS[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;



  




iiii[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
iiii[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
iiii[2]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
iiii[3]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;




DISTANCE[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
DISTANCE[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
DISTANCE[2]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
DISTANCE[3]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
DISTANCE[3]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;

  

ETAT[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
ETAT[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;


VALEUR[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
VALEUR[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
VALEUR[2]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
VALEUR[3]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;


SEUIL[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
SEUIL[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
SEUIL[2]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
SEUIL[3]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;

TRAMEOUT[u]='*';

//ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEIN),calcul_addmemoire(&TRAMEIN),&TRAMEIN,1); //ECRITURE EN EEPROM
//LIRE_EEPROM(choix_memoire(&TRAMEOUT),calcul_addmemoire(&TRAMEOUT),&TRAMEIN); //LIRE DANS EEPROM
pointeur_out_in=u;


}


void recuperer_trame_memoire (char w)  //RECUPERE TRAME MET DANS LES VARIABLES PUIS ECRIT DANS L'EPROOM


{

int t;
int u;

u=8; // pr un TRAMOUT MEMOIRE 

TRAMEOUT[0]='#';


VOIECOD[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
VOIECOD[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
VOIECOD[2]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
VOIECOD[3]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
VOIECOD[4]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;


//ecrire dans I2C

TYPE[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
 

CMOD[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CMOD[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CMOD[2]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CMOD[3]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;


CCON[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CCON[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CCON[2]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CCON[3]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;



CREP[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CREP[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CREP[2]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CREP[3]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;

 
   
 

t=0;
for (t=0;t<13;t++) 
{
CONSTITUTION[t]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;

}
 
t=0;
for (t=0;t<18;t++) 
{
COMMENTAIRE[t]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
}
 
        

CABLE[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CABLE[1]==TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CABLE[2]==TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;



JJ[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
JJ[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
 

MM[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
MM[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;


HH[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
HH[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;


mm[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
mm[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;



y[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;



COD[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
COD[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;




POS[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
POS[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;



  




iiii[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
iiii[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
iiii[2]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
iiii[3]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;




DISTANCE[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
DISTANCE[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
DISTANCE[2]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
DISTANCE[3]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
DISTANCE[3]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;

  

ETAT[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
ETAT[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;


VALEUR[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
VALEUR[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
VALEUR[2]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
VALEUR[3]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;


SEUIL[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
SEUIL[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
SEUIL[2]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
SEUIL[3]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;

TRAMEOUT[u-7]='*';



//ecriture ou pas
if (w==1)
ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEOUT),calcul_addmemoire(&TRAMEOUT),&TRAMEOUT,1); //ECRITURE EN EEPROM


LIRE_EEPROM(choix_memoire(&TRAMEOUT),calcul_addmemoire(&TRAMEOUT),&TRAMEIN,1); //LIRE DANS EEPROM
pointeur_out_in=u;

 

}


void SAV_EXISTENCE(char *tt)
{

//#CMAUZTE02aFAFAFFFFFFFFFFFFF0ax22222* on recoit pour azutomate ou CARTE IO pas sur carte mere (#R1CM02aFAFAFFFFFFFFFFFFF0ax99999*)



		unsigned int l;
		char i;
		
     
	  			
		
		l=tt[8]-48;
		l=l*10;
		l=l+tt[9]-48;
		i=l;	

		l=128*l+30000;
	    if ((tt[8]==':')&&(tt[9]=='0')) //voie 100 25092020	
		l=42800;		

		TMP[0]='#';
		TMP[1]=i;
		for (i=5;i<37;i++)
		{
		TMP[i-3]=tt[5+i];
		}


ECRIRE_EEPROMBYTE(3,l,&TMP,4);

//delay_qms(100);

//LIRE_EEPROM(3,l,&TRAMEIN,4); //LIRE DANS EEPROM

//delay_qms(100);

} 

void entrer_trame_horloge (void) //PREPARE TRAME POUR ECRIRE L'HORLOGE EN FORMAT 

//hh mm ss JJ MM AA 
{
char HEURE,MINUTE,SECONDE,MOIS,JOUR,ANNEE,DATE;
//ASCII->BCD
HEURE=(HORLOGET[0]-48)<<4; 
HEURE=HEURE+ (HORLOGET[1]-48) ;
MINUTE=(HORLOGET[2]-48)<<4; 
MINUTE=MINUTE+ (HORLOGET[3]-48) ;
SECONDE=(HORLOGET[4]-48)<<4; 
SECONDE=SECONDE+ (HORLOGET[5]-48) ;
DATE=(HORLOGET[6]-48)<<4;
DATE=DATE+ (HORLOGET[7]-48) ;
MOIS=(HORLOGET[8]-48)<<4; 
MOIS=MOIS+ (HORLOGET[9]-48) ;
ANNEE=(HORLOGET[10]-48)<<4;
ANNEE=ANNEE + (HORLOGET[11]-48);





ECRIRE_HORLOGE(2,HEURE);
ECRIRE_HORLOGE(1,MINUTE);
ECRIRE_HORLOGE(0,SECONDE);
ECRIRE_HORLOGE(4,DATE);
ECRIRE_HORLOGE(5,MOIS);
ECRIRE_HORLOGE(6,ANNEE);

//HORLOGE[12];

 


}

void GROUPE_EN_MEMOIRE(unsigned char *tab,char lui) 

{

int ici;

for (ici=0;ici<129;ici++)
{



}



if (lui==0)
ici=0; 
if (lui==1)
ici=128;
if (lui==2)
ici=256; 





ECRIRE_EEPROMBYTE(6,ici,tab,3);
delay_qms(100);
LIRE_EEPROM(6,ici,&TRAMEOUT,3);
delay_qms(100);

}

void controle_CRC(void)
{

 
CRC485[0]=TRAMEIN[pointeur_out_in];
CRC485[1]=TRAMEIN[pointeur_out_in+1];
CRC485[2]=TRAMEIN[pointeur_out_in+2];
CRC485[3]=TRAMEIN[pointeur_out_in+3];
CRC485[4]=TRAMEIN[pointeur_out_in+4];



}

//////////////////////////////////////////////////////////////////
// ENVOI D'UNE TRAME COMPLETE RS485          		   	        //
//////////////////////////////////////////////////////////////////

//exemple #CMR1DIC03300R111122223333BRA/EEEE/22/1COMMENTAIRESICIROB0332611152500102000045678110865088822222*

// STOCKAGE DU RESULTAT DANS LA TRAMEOUT[]

void TRAMEENVOIRS485DONNEES (void)
{
unsigned char u;
unsigned char tmp;
tmp=0;

do
{
ret1:
LEDC=!LEDC;
		u=TRAMEOUT[tmp];
		RS485(u);
		//if ((u!='=') && (tmp==0))
		//goto ret1;
	
		
		tmp=tmp+1;
}
while ((u!='*') && (tmp<200));

LEDC=0;
//delay_qms(100);
 
}

void calcul_CRC(void)
{

 
CRC485[0]='2';
CRC485[1]='2';
CRC485[2]='2';
CRC485[3]='2';
CRC485[4]='2';


TRAMEOUT[pointeur_out_in]=CRC485[0];
TRAMEOUT[pointeur_out_in+1]=CRC485[1];
TRAMEOUT[pointeur_out_in+2]=CRC485[2];
TRAMEOUT[pointeur_out_in+3]=CRC485[3];
TRAMEOUT[pointeur_out_in+4]=CRC485[4];
TRAMEOUT[pointeur_out_in+5]='*';

}


signed char litHORLOGE_i2c(unsigned char NumeroH,unsigned char AddH) 
{     


  char Data;
  
  StartI2C();
  WriteI2CC(NumeroH&0xFE);        // adresse de la DS1307
  WriteI2CC(AddH);     // suivant le choix utilisateur
  RestartI2C();
  WriteI2CC(NumeroH);        // on veut lire
  Data = ReadI2C();     // lit la valeur 
  StopI2C();
  return(Data);



} 

unsigned char ECRIRE_HORLOGE(unsigned char zone,unsigned char Time)
{
unsigned char TMP;

if (zone==0)
Time=Time&0x7F; //laisse l'horloge CHbits a 1 secondes tres important si ce bit n'est pas a 1 lhorloge ne marche pas
if (zone==1)
Time=Time&0x7F; //laisse le bit7 a zero minutes
if (zone==2)
Time=Time&0x3F;//met a zero les bits 6 et 7 pour le mode 24h  
if (zone==3)
Time=Time&0x07; // laisse ts les bits sauf 0 1 2 a 0 
if (zone==4)
Time=Time&0x3F; // met a 0 les bits 7 et 6 date 
if (zone==5)
Time=Time&0x1F; // met a 0 les bits 7 et 6 et 5 mois
if (zone==6)
Time=Time;
if (zone==7)
Time=Time; //registre control ex: 00010000 =) oscillation de 1hz out

 



TMP=envoyerHORLOGE_i2c(0xD0,zone,Time);
return(TMP);
}

//////////////////////////////////////////////////////////////////
// �mission d'un caract�re sur le port s�rie					//
//////////////////////////////////////////////////////////////////

void RS485(unsigned char carac)
{
//carac = code(carac);


DERE=1; //AUTORISATION TRANSMETTRE
	//	LEDC=!LEDC;
PORTAbits.RA4=1;
TXSTA2bits.TXEN = 0;

 RCSTA2bits.CREN  = 0;       // interdire reception

	TXSTA2bits.TXEN = 1;  // autoriser transmission
	

TXREG2 = carac;
	
  //__no_operation();
  //__no_operation();
  //__no_operation();
  //__no_operation();
 while (TXSTA2bits.TRMT == 0) ;    // attend la fin de l'�mission
//     __no_operation();
TXSTA2bits.TXEN = 0; 
	
DERE=0; //INTERDICTION TRANSMETTRE
//delayms(100);


}

unsigned char envoyerHORLOGE_i2c(unsigned char NumeroH,unsigned char ADDH, unsigned char dataH0)
{
	
 StartI2C();
   WriteI2CC(NumeroH);     // adresse de l'horloge temps r�el
   WriteI2CC(ADDH);
   WriteI2CC(dataH0);
   StopI2C();
   delay_qms(11);
 				
}


//********************************************************************/

unsigned char putstringI2C(unsigned char *wrptr)
{



unsigned char x;

unsigned int PageSize;
PageSize=128;


  for (x = 0; x < PageSize; x++ ) // transmit data until PageSize  
  {
    if ( SSPCON1bits.SSPM3 )      // if Master transmitter then execute the following
    { // 
	
	 	
	//	      if ( putcI2CC( *wrptr ) )   // write 1 byte
		if (WriteI2CC( *wrptr ) )   // write 1 byte
		{
		        return ( -3 );            // return with write collision error
		}
	

      IdleI2C();                  // test for idle condition
      if ( SSPCON2bits.ACKSTAT )  // test received ack bit state
      {
        return ( -2 );            // bus device responded with  NOT ACK
      }                           // terminateputstringI2C() function
    }
    else                          // else Slave transmitter
    {
      PIR1bits.SSPIF = 0;         // reset SSPIF bit

	
      SSPBUF = *wrptr;            // load SSPBUF with new data
		    

	  SSPCON1bits.CKP = 1;        // release clock line 
      while ( !PIR1bits.SSPIF );  // wait until ninth clock pulse received

      if ( ( !SSPSTATbits.R_W ) && ( !SSPSTATbits.BF ) )// if R/W=0 and BF=0, NOT ACK was received
      {
        return ( -2 );            // terminateputstringI2C() function
      }
    }

 	

  wrptr ++;                       // increment pointer 
 
  
  	 
 }                               // continue data writes until null character
  return ( 0 );
}






void testINT2 (void)

{

if (INTCON3bits.INT2IF==1)
{
if (DEMANDE_DE_LAUTO==0xFF)
{

DEMANDE_DE_LAUTO=0x00;
REA_DESACTIVE=0xFF;
LEDRJTEST2=0;
LEDRJTEST1=0;
}
else
{
DEMANDE_DE_LAUTO=0xFF;
LEDRJTEST2=1;
LEDRJTEST1=1;
}

INTCON3bits.INT2IF=0;
}

}



void ecouteTCPIP(void)
{

char tyuo;

//if (PASDEDEMANDECONNECTIONTCPIP==0xFF)
//goto finecoute;
for (tyuo=1;tyuo<100;tyuo++)
{




      // Blink LED0 (right most one) every second.
    //    if(TickGet() - t >= TICK_SECOND/2ul)
      //  {
        //    t = TickGet();
          //  LED0_IO ^= 1;
       // }

        // This task performs normal stack task including checking
        // for incoming packet, type of packet and calling
        // appropriate stack entity to process it.
        StackTask();
        
        #if defined(WF_CS_TRIS)
        #if !defined(MRF24WG)
        if (gRFModuleVer1209orLater)
        #endif
            WiFiTask();
        #endif

        // This tasks invokes each of the core stack application tasks
        StackApplications();

        #if defined(STACK_USE_ZEROCONF_LINK_LOCAL)
        ZeroconfLLProcess();
        #endif

        #if defined(STACK_USE_ZEROCONF_MDNS_SD)
        mDNSProcess();
        // Use this function to exercise service update function
        // HTTPUpdateRecord();
        #endif
 
        // Process application specific tasks here.
        // For this demo app, this will include the Generic TCP 
        // client and servers, and the SNMP, Ping, and SNMP Trap
        // demos.  Following that, we will process any IO from
        // the inputs on the board itself.
        // Any custom modules or processing you need to do should
        // go here.
	
        #if defined(STACK_USE_GENERIC_TCP_CLIENT_EXAMPLE)		
//        GenericTCPClient(); //pour transmettre alarme � TELGAT		
        #endif
        
        #if defined(STACK_USE_GENERIC_TCP_SERVER_EXAMPLE)
	// Allocate a socket for this server to listen and accept connections on
if (ICMPBeginUsage())
{
//ETAT_CONNECT_MINITEL=0;
}
else
{


}
if (OUVERTUREIP!=0xFF)
{
			MySocket = TCPOpen(0, TCP_OPEN_SERVER,1111, TCP_PURPOSE_GENERIC_TCP_SERVER);
			
			//delay_qms(150);	
			if(MySocket == INVALID_SOCKET)
			{
			PASDEDEMANDECONNECTIONTCPIP=0; //PAS DE CONNECTION AU PREMIER TEST IL DETECTE UNE CONNECTION????
			ETAT_CONNECT_MINITEL=0;
    	    IO_EN_COM=0; // IO EST PAS EN COM
			
			goto noncorrect;
			}
			OUVERTUREIP=0xFF; // MODE DETECTION PUIS RECPETION
}
							if(!TCPIsConnected(MySocket))	
							{
							}
							else
							{
							PASDEDEMANDECONNECTIONTCPIP=0xFF; //CONNECTION	
							}	
							if 	(PASDEDEMANDECONNECTIONTCPIP==0xFF)
							{
							
							ETAT_CONNECT_MINITEL=0xFF;
					        IO_EN_COM=1; // IO EST EN COM
							goto finecoute;
							}

		
noncorrect:
		#endif
        
        #if defined(STACK_USE_SMTP_CLIENT)
        SMTPDemo();
        #endif
        
        #if defined(STACK_USE_ICMP_CLIENT)
        PingDemo();
        #endif

		#if defined(STACK_USE_TFTP_CLIENT) && defined(WF_CS_TRIS)	
		TFTPGetUploadStatus();
		#endif
        
        #if defined(STACK_USE_SNMP_SERVER) && !defined(SNMP_TRAP_DISABLED)
        //User should use one of the following SNMP demo
        // This routine demonstrates V1 or V2 trap formats with one variable binding.
        SNMPTrapDemo();
        
        #if defined(SNMP_STACK_USE_V2_TRAP) || defined(SNMP_V1_V2_TRAP_WITH_SNMPV3)
        //This routine provides V2 format notifications with multiple (3) variable bindings
        //User should modify this routine to send v2 trap format notifications with the required varbinds.
        //SNMPV2TrapDemo();
        #endif 
        if(gSendTrapFlag)
            SNMPSendTrap();
        #endif
        
        #if defined(STACK_USE_BERKELEY_API)
        BerkeleyTCPClientDemo();
        BerkeleyTCPServerDemo();
        BerkeleyUDPClientDemo();
        #endif

        ProcessIO();

        
        #if defined(DERIVE_KEY_FROM_PASSPHRASE_IN_HOST) && defined (MRF24WG)
            if (g_WpsPassphrase.valid) {
                WF_ConvPassphrase2Key(g_WpsPassphrase.passphrase.keyLen, g_WpsPassphrase.passphrase.key,
                g_WpsPassphrase.passphrase.ssidLen, g_WpsPassphrase.passphrase.ssid);
                WF_SetPSK(g_WpsPassphrase.passphrase.key);
                g_WpsPassphrase.valid = FALSE;
            }
        #endif    /* defined(DERIVE_KEY_FROM_PASSPHRASE_IN_HOST) */
		#if defined(STACK_USE_AUTOUPDATE_HTTPSERVER) && defined(WF_CS_TRIS) && defined(MRF24WG)
		{
			static DWORD t_UpdateImage=0;
			extern UINT8 Flag_ImageUpdate_running;
			if(Flag_ImageUpdate_running == 1)
			{
				UINT8 buf_command[4];
				if( (TickGet() - t_UpdateImage) >= TICK_SECOND * 120ul)
				{
		 			putsUART((char *)"Update Firmware timeout, begin to restore..\r\n");
					buf_command[0]=UPDATE_CMD_ERASE0; //Erase bank0
					buf_command[1]=UPDATE_SERITY_KEY_1; 
					buf_command[2]=UPDATE_SERITY_KEY_2; 
					buf_command[3]=UPDATE_SERITY_KEY_3; 
					SendSetParamMsg(PARAM_FLASH_update, buf_command, 4);
					buf_command[0]=UPDATE_CMD_CPY_1TO0; //Copy bank1 to bank0
					buf_command[1]=UPDATE_SERITY_KEY_1; 
					buf_command[2]=UPDATE_SERITY_KEY_2; 
					buf_command[3]=UPDATE_SERITY_KEY_3; 
					SendSetParamMsg(PARAM_FLASH_update, buf_command, 4);
					putsUART((char *)"restore Done\r\n");
					Flag_ImageUpdate_running = 0;
				}
					
			}
			else
			{
				t_UpdateImage = TickGet();
			}
		}
		#endif


}

finecoute:
delay_qms(1);

}

 

 

void SAV_CHG_ALARME(unsigned char *tab) 

{

 


ECRIRE_EEPROMBYTE(choix_memoirePRECEDENT(&TRAMEIN),calcul_addmemoirePRECEDENT(&TRAMEIN),&TRAMEIN,1); //ECRITURE EN EEPROM
// LIRE_EEPROM(choix_memoire(&tati),calcul_addmemoire(&tati),&TMP,1);
//ECRIRE_EEPROMBYTE(7,ici,tab,6);
//delay_qms(100);
//LIRE_EEPROM(choix_memoirePRECEDENT(&TRAMEIN),calcul_addmemoirePRECEDENT(&TRAMEIN),&TRAMEOUT,1);
//delay_qms(10);

}

 

// choix du numero memoire suivant N�voie
unsigned char choix_memoirePRECEDENT(unsigned char *tab)
{
unsigned int i;
unsigned int t;
unsigned char r;




t=0;
i=tab[3]-48;
t=t+1*i;
i=tab[2]-48;
t=t+10*i;
i=tab[1]-48;
t=t+100*i;

r=7;
if (t>50)
r=0;





delay_qms(10);
return r;

}

// CALCUL EMPLACEMENT MEMOIRE
unsigned int calcul_addmemoirePRECEDENT (unsigned char *tab)
{
unsigned int t;
unsigned int i;
unsigned int r;

 

t=0;
i=tab[3];
i=i-48;
t=t+1*i;
i=tab[2];
i=i-48;
t=t+10*i;
i=tab[1];
i=i-48;
t=t+100*i;


r=2176*(t);
if (t>50)
r=2176*(t-50);



t=0;
i=tab[5]-48;
t=1*i;
i=tab[4]-48;
t=t+10*i;
i=t;

t=r+128*i;



delay_qms(10);



return t;


}

void ANALYSE_SE_ALARME (void)

{
unsigned char esd,changement;
unsigned char tuto[6];
unsigned char SALARME[102];
unsigned char EALARME[102];
tuto[0]='#';

tuto[1]='0';
tuto[2]='0';
tuto[3]='1';
tuto[4]='0';
tuto[5]='0';


for (esd=0;esd<101;esd++)
{


LIRE_EEPROM(choix_memoirePRECEDENT(&tuto),calcul_addmemoirePRECEDENT(&tuto),&TRAMEOUT,1);
LIRE_EEPROM(choix_memoire(&tuto),calcul_addmemoire(&tuto),&TRAMEIN,1);



//ANALYSE ET ENREGISTREMENT
changement=0x00;
if (TRAMEIN[76]!=1) //CAD A DIRE PAS EN INTERVENTION
{

			if ((TRAMEOUT[75]=='0')&&(TRAMEIN[75]=='4')) //ENTREE EN ALARME 
			{
			changement=0xAA; //ENTREE D'ALARME		
			}
	
 
			if ((TRAMEOUT[75]=='4')&&(TRAMEIN[75]=='0')) //SORTIE D'ALARME
			{
			changement=0xBB; //SORTIE D'ALARME
			}


}

if (changement==0xAA)
{
EALARME[esd]=changement;
ALARMEATRANS=0xAA; //ALARME E
SALARME[esd]=0x00;
}
if (changement==0xBB)
{

if (ALARMEATRANS==0xAA)
ALARMEATRANS=0xCC; //ALARME E/S
else
ALARMEATRANS=0xBB; //ALARME S

SALARME[esd]=changement;
EALARME[esd]=0x00;
}

if (changement==0x00)
{
EALARME[esd]=0x00;
SALARME[esd]=0x00;
ALARMEATRANS=0x00;
}

//	ECRIRE_EEPROMBYTE(6,1000+esd,changement,3); //TRAME DE 115 LISTE DES ENTREES

//	ECRIRE_EEPROMBYTE(6,4000+esd,changement,3); //TRAME DE 115 LISTE DES SORTIES
//





tuto[5]++;

				if (tuto[5]==58) //SUP A '9'
				{
				tuto[4]='1';
				tuto[5]='0';
				}
				if ((tuto[4]=='1')&&(tuto[5]=='7')) //FIN CODAGE 
				{
				tuto[4]='0';
				tuto[5]='0';
				tuto[3]++;
						if (tuto[3]==58) 	//SUP A '9'
						{
						tuto[3]='0';
						tuto[2]++;	
								if ((tuto[2]==58)&&(tuto[3]=='9')) //FIN CABLE
	    						{
								tuto[3]='0';
								tuto[2]='0';	
								tuto[1]='1';
								}    
						}
				}



}
//ALARMEATRANS=0;

ECRIRE_EEPROMBYTE(6,2560,&EALARME,6); //TRAME DE 115 LISTE DES ENTREES
delay_qms(10);
ECRIRE_EEPROMBYTE(6,2688,&SALARME,6); //TRAME DE 115 LISTE DES SORTIES

delay_qms(10);
//LISTE DES CHANGEMENTS ALARMES E/S

ALARMEATRANS=0xBB; //a enlever
if (TRANSSORTIEALARM=='O')
{
			if ((ALARMEATRANS==0xBB)||(ALARMEATRANS==0xCC)) //SORTIE D'ALARME
			{
			LIRE_EEPROM(6,2688,&ALARME,6); //SORTIES D'ALAMRE
			TYPEORDREALARME=2;
			GenericTCPClient(); //pour transmettre alarme � TELGAT			
			//voir_qui_existe(2); 
			}
			TYPEORDREALARME=0;
			ALARMEATRANS==0x00;
			delay_qms(10);
}

if (TRANSALARM=='O')
{
			if ((ALARMEATRANS==0xAA)||(ALARMEATRANS==0xCC)) //ENTREE ALARME
			{
			LIRE_EEPROM(6,2560,&ALARME,6); //ENTREES D'ALARME
			TYPEORDREALARME=1;
			GenericTCPClient(); //pour transmettre alarme � TELGAT
			
			//voir_qui_existe(1);
			}
			TYPEORDREALARME=0;
			ALARMEATRANS==0x00;
			delay_qms(10);
}

			TYPEORDREALARME=0;
			ALARMEATRANS==0x00;

}




void client (void)
{
char openip;
int ouioui;
static DWORD		Timering;
static BYTE ServerNameio[] =	{192,168,0,11};  /////010.168.192.056 telgat
static TCP_SOCKET	MySocket;

			// Connect a socket to the remote TCP server
openip=0;			
			//MySocket = TCPOpen(*((DWORD*)&ServerName[0]), TCP_OPEN_IP_ADDRESS, ServerPort, TCP_PURPOSE_GENERIC_TCP_CLIENT);
for (ouioui=0;ouioui<3000;ouioui++)
{
			
debutclient:			
if (openip==0)
{

			//MySocket = TCPOpen(*((DWORD*)&ServerName[0]), TCP_OPEN_IP_ADDRESS, ServerPort, TCP_PURPOSE_GENERIC_TCP_CLIENT);
			MySocket = TCPOpen(*((DWORD*)&ServerNameio[0]), TCP_OPEN_IP_ADDRESS, 80, TCP_PURPOSE_GENERIC_TCP_CLIENT);
			
			// Abort operation if no TCP socket of type TCP_PURPOSE_GENERIC_TCP_CLIENT is available
			// If this ever happens, you need to go add one to TCPIPConfig.h
			if(MySocket == INVALID_SOCKET)
			goto finclient;
 
			#if defined(STACK_USE_UART)
			putrsUART((ROM char*)"\r\n\r\nConnecting using Microchip TCP API...\r\n");
			#endif
			openip=1;
		//	GenericTCPExampleState++;
			Timering = TickGet();
			goto finclient;
}
 
if (openip==1)
{

obtenu:
			// Wait for the remote server to accept our connection request
			if(!TCPIsConnected(MySocket))
			{
				// Time out if too much time is spent in this state
				if(TickGet()-Timering > 10*TICK_SECOND)
				{
					// Close the socket so it can be used by other modules
					TCPDisconnect(MySocket);
					MySocket = INVALID_SOCKET;
					goto debutclient;
				//	GenericTCPExampleState--;
				}
			goto finclient;
			}
 
			Timering = TickGet();

			TRAMEOUT[0]='c'; //NUMERO SERVEUR TELGAT


finclient:
		
	 	 		
	TRAMEOUT[0]='c'; //NUMERO SERVEUR TELGAT
}


}

}


BOOL ICMPBeginUsage(void)



{

static struct
{
	unsigned char bICMPInUse:1;         // Indicates that the ICMP Client is in use
	unsigned char bReplyValid:1;        // Indicates that a correct Ping response to one of our pings was received
	unsigned char bRemoteHostIsROM:1;   // Indicates that a remote host name was passed as a ROM pointer argument
} ICMPFlags = {0x00};


	if(ICMPFlags.bICMPInUse)
		return FALSE;

	ICMPFlags.bICMPInUse = TRUE;
	return TRUE;

}



void controle_CRC2021(void) {

    char CRCcal[5];
    unsigned int fft;
    //ON RECHERCHE LE CRC
    CRC2021[0] = TRAMEIN[pointeur_out_in];
    CRC2021[1] = TRAMEIN[pointeur_out_in + 1];
    CRC2021[2] = TRAMEIN[pointeur_out_in + 2];
    CRC2021[3] = TRAMEIN[pointeur_out_in + 3];
    CRC2021[4] = TRAMEIN[pointeur_out_in + 4];





    fft = CRC16(TRAMEIN, pointeur_out_in - 1);

    IntToChar5(fft, CRCcal, 5);

    if ((CRC2021[0] == CRCcal[0]) && (CRC2021[1] == CRCcal[1]) && (CRC2021[2] == CRCcal[2]) && (CRC2021[3] == CRCcal[3]) && (CRC2021[4] == CRCcal[4]))
        CRC_ETAT = 0xFF; //CRC CORRECT
    else
        CRC_ETAT = 0x00; //CRC INCORRECT




}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////// CONVERSION UNSIGNED INT TO CHAR/////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void IntToChar5(unsigned int value, char *chaine, char Precision) {
    unsigned int Mil, Cent, Diz, Unit, DMil;
    int count = 0;

    // initialisation des variables
    Mil = 0;
    Cent = 0;
    Diz = 0;
    Unit = 0;
    DMil = 0;
    /*
    if (value < 0) // si c'est un nombre n�gatif
        {
        value = value * (-1);
     *chaine = 45; // signe '-'
        count = 1;  // c'est un nombre n�gatif
        }
    else // si c'est un nombre positif
        {
        count = 1;
     *chaine = 32; // code ASCII du signe espace ' '
        }
     */
    // si la valeur n'est pas nulle
    if (value != 0) {
        if (Precision >= 5) // si l'utilisateur d�sire les milliers
        {
            // conversion des milliers
            DMil = value / 10000;
            if (DMil != 0) {
                *(chaine + count) = DMil + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }
        if (Precision >= 4) // si l'utilisateur d�sire les milliers
        {
            // conversion des milliers
            Mil = value - (DMil * 10000);
            Mil = Mil / 1000;
            if (Mil != 0) {
                *(chaine + count) = Mil + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        if (Precision >= 3) // si l'utilisateur d�sire les centaines
        {
            // conversion des centaines
            Cent = value - (Mil * 1000)-(DMil * 10000);
            Cent = Cent / 100;
            if (Cent != 0) {
                *(chaine + count) = Cent + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        if (Precision >= 2) // si l'utilisateur d�sire les dizaines
        {
            // conversion des dizaines
            Diz = value - (Mil * 1000) - (Cent * 100)-(DMil * 10000);
            Diz = Diz / 10;
            if (Diz != 0) {
                *(chaine + count) = Diz + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        // conversion unit�s
        Unit = value - (Mil * 1000) - (Cent * 100) - (Diz * 10)-(DMil * 10000);
        *(chaine + count) = Unit + 48;
        if (*(chaine + count) < 48) // limites : 0 et 9
            *(chaine + count) = 48;
        if (*(chaine + count) > 57)
            *(chaine + count) = 57;
    } else // if (value == 0)
    {
        //*(chaine) = 32; // ecrit un espace devant le nombre
        for (Mil = 0; Mil < Precision; Mil++) // inscription de '0' dans toute la chaine
            *(chaine + Mil) = 48;
    }

} // fin de la fonction de conversion INT => CHAR



