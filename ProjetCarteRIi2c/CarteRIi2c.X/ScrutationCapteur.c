#define _ADIF	PIR1bits.ADIF	// INTERRUPTION FIN CONVERSION ANALOGIQUE
#define _ADON	ADCON0bits.ADON	// VALIDE LE CAN
#define _ADGO	ADCON0bits.GO	// LANCE ACQUISITION ET CONVERSION
#define _ADDONE	ADCON0bits.DONE	// TEST SI ACQUISITION ET CONVERSION EN COURS
// PIC18F8723 Configuration Bit Settings

// 'C' source line config statements
#include <p18f8723.h>
#include <stdio.h>
#include <stdlib.h>
#include "RecupCapteur.h"
#include <i2c.h>

// CONFIG1H
#pragma config OSC = HS         // Oscillator Selection bits (HS oscillator)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Two-Speed Start-up disabled)

// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = SBORDIS  // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
#pragma config BORV = 3         // Brown-out Voltage bits (Minimum setting)

// CONFIG2H
#pragma config WDT = OFF        // Watchdog Timer (WDT disabled (control is placed on the SWDTEN bit))
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3L
#pragma config MODE = MC        // Processor Data Memory Mode Select bits (Microcontroller mode)
#pragma config ADDRBW = ADDR20BIT// Address Bus Width Select bits (20-bit Address Bus)
#pragma config DATABW = DATA16BIT// Data Bus Width Select bit (16-bit External Bus mode)
#pragma config WAIT = OFF       // External Bus Data Wait Enable bit (Wait selections are unavailable for table reads and table writes)

// CONFIG3H
#pragma config CCP2MX = PORTC   // CCP2 MUX bit (ECCP2 input/output is multiplexed with RC1)
#pragma config ECCPMX = PORTE   // ECCP MUX bit (ECCP1/3 (P1B/P1C/P3B/P3C) are multiplexed onto RE6, RE5, RE4 and RE3 respectively)
#pragma config LPT1OSC = OFF    // Low-Power Timer1 Oscillator Enable bit (Timer1 configured for higher power operation)
#pragma config MCLRE = ON       // MCLR Pin Enable bit (MCLR pin enabled; RG5 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
#pragma config LVP = OFF        // Single-Supply ICSP Enable bit (Single-Supply ICSP disabled)
#pragma config BBSIZ = BB2K     // Boot Block Size Select bits (1K word (2 Kbytes) Boot Block size)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled (Legacy mode))

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not code-protected)
#pragma config CP1 = OFF        // Code Protection bit Block 1 (Block 1 (004000-007FFFh) not code-protected)
#pragma config CP2 = OFF        // Code Protection bit Block 2 (Block 2 (008000-00BFFFh) not code-protected)
#pragma config CP3 = OFF        // Code Protection bit Block 3 (Block 3 (00C000-00FFFFh) not code-protected)
#pragma config CP4 = OFF        // Code Protection bit Block 4 (Block 4 (010000-013FFFh) not code-protected)
#pragma config CP5 = OFF        // Code Protection bit Block 5 (Block 5 (014000-017FFFh) not code-protected)
#pragma config CP6 = OFF        // Code Protection bit Block 6 (Block 6 (01BFFF-018000h) not code-protected)
#pragma config CP7 = OFF        // Code Protection bit Block 7 (Block 7 (01C000-01FFFFh) not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Boot Block Code Protection bit (Boot Block (000000-0007FFh) not code-protected)
#pragma config CPD = OFF        // Data EEPROM Code Protection bit (Data EEPROM not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not write-protected)
#pragma config WRT1 = OFF       // Write Protection bit Block 1 (Block 1 (004000-007FFFh) not write-protected)
#pragma config WRT2 = OFF       // Write Protection bit Block 2 (Block 2 (008000-00BFFFh) not write-protected)
#pragma config WRT3 = OFF       // Write Protection bit Block 3 (Block 3 (00C000-00FFFFh) not write-protected)
#pragma config WRT4 = OFF       // Write Protection bit Block 4 (Block 4 (010000-013FFFh) not write-protected)
#pragma config WRT5 = OFF       // Write Protection bit Block 5 (Block 5 (014000-017FFFh) not write-protected)
#pragma config WRT6 = OFF       // Write Protection bit Block 6 (Block 6 (01BFFF-018000h) not write-protected)
#pragma config WRT7 = OFF       // Write Protection bit Block 7 (Block 7 (01C000-01FFFFh) not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) not write-protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block (000000-007FFF, 000FFF or 001FFFh) not write-protected)
#pragma config WRTD = OFF       // Data EEPROM Write Protection bit (Data EEPROM not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection bit Block 1 (Block 1 (004000-007FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR2 = OFF      // Table Read Protection bit Block 2 (Block 2 (008000-00BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR3 = OFF      // Table Read Protection bit Block 3 (Block 3 (00C000-00FFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR4 = OFF      // Table Read Protection bit Block 4 (Block 4 (010000-013FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR5 = OFF      // Table Read Protection bit Block 5 (Block 5 (014000-017FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR6 = OFF      // Table Read Protection bit Block 6 (Block 6 (018000-01BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR7 = OFF      // Table Read Protection bit Block 7 (Block 7 (01C000-01FFFFh) not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Boot Block Table Read Protection bit (Boot Block (000000-007FFF, 000FFF or 001FFFh) not protected from table reads executed in other blocks)





#pragma udata udata1
unsigned char TRAMEIN[53];
unsigned char TRAMEOUT[53];
unsigned char dernier_TPA_1; // contient le num�ro du dernier capteur Adressable (de 0�16), de la voie impaire, � mesurer
extern unsigned int Tab_Pression_1[17];
extern unsigned int Tab_Pression_2[17];

#pragma udata udata2

extern unsigned int Tab_I_Repos_1[17];
extern unsigned int Tab_I_Repos_2[17];
extern unsigned int Tab_I_Modul_1[17];
extern unsigned int Tab_I_Modul_2[17];
extern unsigned int Tab_I_Conso_1[17];
extern unsigned int Tab_I_Conso_2[17];
extern unsigned char Tab_Etat_1[17];
extern unsigned char Tab_Etat_2[17];

#pragma udata udata3
unsigned char Tab_Voie_1; //1 octet VOIE IMPAIRES 1,3,5....99 	  ASCII OU HEX??  => HEXA OU DECIMAL ET CONVERSION EN ASCII JUSTE POUR ECRITURE EN EEPROM
unsigned char Tab_Voie_2; //1 octet VOIE PAIRES   2,4,6...100 	 					IDEM POUR TOUTES LES VALEURS
extern unsigned int Timer1L; // pour sauvegarder les valeurs � l'interruption de INT2 (utilis� dans la mesure de fr�quence)
extern unsigned int Timer1H;
extern unsigned int t1ov_cnt; //	compteur de d�bordement du timer1  (de 65535 � 0000)
extern unsigned int cpt_int2;
unsigned char No_voie; // voie � sauvegarder
unsigned int Tab_Seuil_1[17]; //34 octets	17 caract�res	  seuil 16+1 tp + deb
unsigned int Tab_Seuil_2[17]; //34 octets	17 caract�res	  seuil 16+1 tp + deb


void Interruption1(void);

#pragma code highVector=0x08			//valeur 0x08 pour interruptions prioritaite 

void highVector(void) {
    _asm GOTO Interruption1 _endasm // on doit �xecuter le code de la fonction interruption
}

#pragma interrupt Interruption1 

void Interruption1(void) {
    unsigned char sauv1;
    unsigned char sauv2;

    sauv1 = PRODL; // sauvegarde le contenu des registres de calcul
    sauv2 = PRODH;

    //===========TIMER1
    if (PIR1bits.TMR1IF) { // v�rifie que l'IT est Timer1
        t1ov_cnt++; // incr�mente le compteur de d�bordement
        PIR1bits.TMR1IF = 0; // efface le flag d'IT du Timer1
    }
    //===========INT0 sur RB0  

    if (INTCONbits.INT0IF == 1) { // v�rifie que l'IT est INT0, origine RB0=0
        INTCONbits.INT0IF = 0; //efface le flag d'IT extern sur RB0
        Reset(); //  <================================= !!!!!!! RESET SOFT DU PIC !!!!!!! 
    }
    if (INTCON3bits.INT2IF == 1) {// v�rifie que l'IT est INT2, origine RB2=2
        if (cpt_int2 == 0) {
            TMR1H = 0;
            TMR1L = 0;
            t1ov_cnt = 0;
            T1CONbits.TMR1ON = 1; // timer 1 ON
            PIR1bits.TMR1IF = 0; // 0 = TMR1 register overflowed reset
        }
        if (cpt_int2 >= 20) {
            T1CONbits.TMR1ON = 0; // timer 1 OFF
            Timer1H = TMR1H;
            Timer1L = TMR1L;
            INTCON3bits.INT2IE = 0; // arret interruption INT2
        }
        cpt_int2++;
        INTCON3bits.INT2IF = 0; //efface le drapeau d'IT sur RB2
    }
    PRODL = sauv1; // on restaure les registres de calcul
    PRODH = sauv2;
}

void main(void) {

    unsigned char cTabExistence[20];
    char cBcl;
    char cNumCapteur;

    PORTAbits.RA4 = 1;

    RECEPTION = 0;
    ENVOIE = 0; // transmission inhib�e

    Init_I2C();
    Init_RS485();
    init_uc();

    envoyerHORLOGE_i2c(0xD0, 0x07, 0b00010000);


    while (1) {
        INTCON3bits.INT1IE = 0; // devalide l'interruption pour RS485 1 pr reactiver
        for (cBcl = 0; cBcl < 17; cBcl++) { //Initialisation des seuils des capteurs pour test
            Tab_Seuil_1[cBcl] = 1000;
            Tab_Seuil_2[cBcl] = 1000;
        }


        TRAMEIN[0] = '#'; //Initialisation trame envoy�e par la carte m�re ( voie 20 capteur 1 pour essai)
        TRAMEIN[1] = 'C';
        TRAMEIN[2] = 'M';
        TRAMEIN[3] = 'R';
        TRAMEIN[4] = '4';
        TRAMEIN[5] = 'O';
        TRAMEIN[6] = 'E';
        TRAMEIN[7] = 'I';
        TRAMEIN[8] = '0';
        TRAMEIN[9] = '2';
        TRAMEIN[10] = '0';
        TRAMEIN[11] = '0';
        TRAMEIN[12] = '1';
        TRAMEIN[13] = '2';
        TRAMEIN[14] = '2';
        TRAMEIN[15] = '2';
        TRAMEIN[16] = '2';
        TRAMEIN[17] = '2';
        TRAMEIN[18] = '*';
        No_voie = (TRAMEIN[8] - 48)*100 + (TRAMEIN[9] - 48)*10 + (TRAMEIN[10] - 48);
        cNumCapteur = (TRAMEIN[12] - 48) + 10 * (TRAMEIN[11] - 48);

        if (No_voie % 2 == 1) { // IMPAIRE
            Tab_Voie_1 = No_voie;
            Tab_Voie_2 = 0;
        } else {
            Tab_Voie_1 = 0;
            Tab_Voie_2 = No_voie;
        }

        for (cBcl = 2; cBcl <= 17; cBcl++) { //Modification de la boucle pour �tre plus clair sur le calcul de dernier_TPA_1. Remarque : D�calage de la boucle entre 2 et 17 pour prendre [4]->[19] dans la table d'existence (on ignore le d�bitm�tre plac� en [3])
            if (cTabExistence[cBcl + 2] == 'A') { //si il y a un A
                dernier_TPA_1 = cBcl;
            }
        }

        do {
            Mesure_DEB_TPA(cNumCapteur, Tab_Voie_1, Tab_Voie_2, 0, 0, TRAMEOUT); //R��crire la fonction pour passer les voies paires/impaires en param�tre, ainsi que le capteur max
            if (No_voie % 2 == 1) {
                EcritureTrameCapteurOeil(No_voie, cNumCapteur, Tab_I_Modul_1, Tab_I_Conso_1, Tab_I_Repos_1, Tab_Etat_1, Tab_Pression_1, Tab_Seuil_1, TRAMEOUT);
            } else {
                EcritureTrameCapteurOeil(No_voie, cNumCapteur, Tab_I_Modul_2, Tab_I_Conso_2, Tab_I_Repos_2, Tab_Etat_2, Tab_Pression_2, Tab_Seuil_2, TRAMEOUT);
            }
            EnvoieTrame(TRAMEOUT, 36);
        } while (PORTBbits.RB1);

    }

}
