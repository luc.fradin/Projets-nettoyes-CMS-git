#define _ADIF	PIR1bits.ADIF	// INTERRUPTION FIN CONVERSION ANALOGIQUE
#define _ADON	ADCON0bits.ADON	// VALIDE LE CAN
#define _ADGO	ADCON0bits.GO	// LANCE ACQUISITION ET CONVERSION
#define _ADDONE	ADCON0bits.DONE	// TEST SI ACQUISITION ET CONVERSION EN COURS
// PIC18F8723 Configuration Bit Settings

// 'C' source line config statements
#include <p18f8723.h>


#include <stdio.h>
#include <stdlib.h>
#include "RecupCapteur.h"
#include <i2c.h>
#define abs(x) ((x) > 0 ? (x) : (-x))
// CONFIG1H
#pragma config OSC = HS         // Oscillator Selection bits (HS oscillator)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Two-Speed Start-up disabled)

// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = SBORDIS  // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
#pragma config BORV = 3         // Brown-out Voltage bits (Minimum setting)

// CONFIG2H
#pragma config WDT = OFF        // Watchdog Timer (WDT disabled (control is placed on the SWDTEN bit))
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3L
#pragma config MODE = MC        // Processor Data Memory Mode Select bits (Microcontroller mode)
#pragma config ADDRBW = ADDR20BIT// Address Bus Width Select bits (20-bit Address Bus)
#pragma config DATABW = DATA16BIT// Data Bus Width Select bit (16-bit External Bus mode)
#pragma config WAIT = OFF       // External Bus Data Wait Enable bit (Wait selections are unavailable for table reads and table writes)

// CONFIG3H
#pragma config CCP2MX = PORTC   // CCP2 MUX bit (ECCP2 input/output is multiplexed with RC1)
#pragma config ECCPMX = PORTE   // ECCP MUX bit (ECCP1/3 (P1B/P1C/P3B/P3C) are multiplexed onto RE6, RE5, RE4 and RE3 respectively)
#pragma config LPT1OSC = OFF    // Low-Power Timer1 Oscillator Enable bit (Timer1 configured for higher power operation)
#pragma config MCLRE = ON       // MCLR Pin Enable bit (MCLR pin enabled; RG5 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
#pragma config LVP = OFF        // Single-Supply ICSP Enable bit (Single-Supply ICSP disabled)
#pragma config BBSIZ = BB2K     // Boot Block Size Select bits (1K word (2 Kbytes) Boot Block size)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled (Legacy mode))

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not code-protected)
#pragma config CP1 = OFF        // Code Protection bit Block 1 (Block 1 (004000-007FFFh) not code-protected)
#pragma config CP2 = OFF        // Code Protection bit Block 2 (Block 2 (008000-00BFFFh) not code-protected)
#pragma config CP3 = OFF        // Code Protection bit Block 3 (Block 3 (00C000-00FFFFh) not code-protected)
#pragma config CP4 = OFF        // Code Protection bit Block 4 (Block 4 (010000-013FFFh) not code-protected)
#pragma config CP5 = OFF        // Code Protection bit Block 5 (Block 5 (014000-017FFFh) not code-protected)
#pragma config CP6 = OFF        // Code Protection bit Block 6 (Block 6 (01BFFF-018000h) not code-protected)
#pragma config CP7 = OFF        // Code Protection bit Block 7 (Block 7 (01C000-01FFFFh) not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Boot Block Code Protection bit (Boot Block (000000-0007FFh) not code-protected)
#pragma config CPD = OFF        // Data EEPROM Code Protection bit (Data EEPROM not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not write-protected)
#pragma config WRT1 = OFF       // Write Protection bit Block 1 (Block 1 (004000-007FFFh) not write-protected)
#pragma config WRT2 = OFF       // Write Protection bit Block 2 (Block 2 (008000-00BFFFh) not write-protected)
#pragma config WRT3 = OFF       // Write Protection bit Block 3 (Block 3 (00C000-00FFFFh) not write-protected)
#pragma config WRT4 = OFF       // Write Protection bit Block 4 (Block 4 (010000-013FFFh) not write-protected)
#pragma config WRT5 = OFF       // Write Protection bit Block 5 (Block 5 (014000-017FFFh) not write-protected)
#pragma config WRT6 = OFF       // Write Protection bit Block 6 (Block 6 (01BFFF-018000h) not write-protected)
#pragma config WRT7 = OFF       // Write Protection bit Block 7 (Block 7 (01C000-01FFFFh) not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) not write-protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block (000000-007FFF, 000FFF or 001FFFh) not write-protected)
#pragma config WRTD = OFF       // Data EEPROM Write Protection bit (Data EEPROM not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection bit Block 1 (Block 1 (004000-007FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR2 = OFF      // Table Read Protection bit Block 2 (Block 2 (008000-00BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR3 = OFF      // Table Read Protection bit Block 3 (Block 3 (00C000-00FFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR4 = OFF      // Table Read Protection bit Block 4 (Block 4 (010000-013FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR5 = OFF      // Table Read Protection bit Block 5 (Block 5 (014000-017FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR6 = OFF      // Table Read Protection bit Block 6 (Block 6 (018000-01BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR7 = OFF      // Table Read Protection bit Block 7 (Block 7 (01C000-01FFFFh) not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Boot Block Table Read Protection bit (Boot Block (000000-007FFF, 000FFF or 001FFFh) not protected from table reads executed in other blocks)




#define SPEED 624 // 624 set 9600 bauds 311 pour 19230bauds  208 pour 28800 Bauds 38 pour 38400 vitesse moyenne 51 115200 grande vitesse   156 38400
#define ENVOIE TXSTA1bits.TXEN 
#define RECEPTION RCSTA1bits.CREN
#define FINTRANS TXSTA1bits.TRMT
#define FINRECEPTION PIR1bits.RCIF

#pragma udata udata1

unsigned char Nb_TPA_ok;
unsigned char fin_mesures_1; // si 1 indique que l'interrogation est termin�e sur voie impaire
unsigned char fin_mesures_2; // si 1 indique que l'interrogation est termin�e sur voie impaire
unsigned char Nb_Capteurs_max; //16 � voir 
unsigned int Freq_TPA_1;
unsigned int Freq_TPA_2;

signed int Freq1_1;
signed int Freq2_1;
signed int Freq3_1;
signed int Freq4_1;
signed int Freq1_2;
signed int Freq2_2;
signed int Freq3_2;
signed int Freq4_2;
char COURTCIRCUIT;
unsigned char Memo_autorisee_1; // autorisation de sauvegarde voie impaire
unsigned char Memo_autorisee_2; // autorisation de sauvegarde voie paire
unsigned int Freq_A;
float Cumul_Freq_1; //r�sultat de mesure de fr�quence du TPA voie impaire
float Cumul_Freq_2; //r�sultat de mesure de fr�quence du TPA voie paire
unsigned char Tab_code_1[17]; //17 octets 	code 0,1,2.....16 	
unsigned char Tab_code_2[17]; //17 octets 	code 0,1,2.....16 
unsigned char dernier_TPA_1; // contient le num�ro du dernier capteur Adressable (de 0�16), de la voie impaire, � mesurer
unsigned char dernier_TPA_2;
unsigned char voie_valide_1;
unsigned char voie_valide_2;

unsigned int Tab_Pression_1[17];
unsigned int Tab_Pression_2[17];
unsigned int debit_cable; // D�bit mesur� en cours

unsigned int I_Conso_1; //courant conso capteurs voies impaires
unsigned int I_Conso_2; //courant conso capteurs voies paires
unsigned int I_Modul_1; //courant modul capteurs voies impaires
unsigned int I_Modul_2; //courant modul capteurs voies paires
unsigned int I_Repos_1;
unsigned int I_Repos_2;
#pragma udata udata2

int pointeur_out_in;
unsigned int Tab_I_Repos_1[17];
unsigned int Tab_I_Repos_2[17];
unsigned int Tab_I_Modul_1[17];
unsigned int Tab_I_Modul_2[17];
unsigned int Tab_I_Conso_1[17];
unsigned int Tab_I_Conso_2[17];
unsigned char Tab_Etat_1[17];
unsigned char Tab_Etat_2[17];
unsigned char BREAKSCRUTATION;

#pragma udata udata3
unsigned char Tab_Voie_1[1]; //1 octet VOIE IMPAIRES 1,3,5....99 	  ASCII OU HEX??  => HEXA OU DECIMAL ET CONVERSION EN ASCII JUSTE POUR ECRITURE EN EEPROM
unsigned char Tab_Voie_2[1]; //1 octet VOIE PAIRES   2,4,6...100 	 					IDEM POUR TOUTES LES VALEURS

unsigned char Tab_Horodatage[10]; //10 octets	 //10 caract�res	

unsigned char alarme_caM;


float tempF1;
float tempF2;
float tempF3;
float tempF4;

unsigned int t1ov_cnt;
unsigned int cpt_int2;

unsigned int Timer1L;
unsigned int Timer1H;

unsigned char No_voie; // voie � sauvegarder
unsigned char No_codage; // n� de capteur � sauvegarder
unsigned char No_eeprom; // N� de m�moire pour la sauvegarde

unsigned char TRAMEOUT[125];
unsigned char recherche_TPA;

#pragma udata udata4
unsigned char TMP[125];

unsigned char Tab_Constitution[13];
unsigned char Tab_Commentaire_cable[18];
unsigned char Tab_Distance[10];
unsigned int TMPINT;

unsigned int tmp_I, tmp_I2;
#pragma udata udata5
unsigned char TRAMEIN[125];

unsigned int Tab_Seuil_1[17]; //34 octets	17 caract�res	  seuil 16+1 tp + deb

unsigned int Tab_Seuil_2[17]; //34 octets	17 caract�res	  seuil 16+1 tp + deb

unsigned char Sauvegarde_en_cours;
#pragma udata udata6
char NOSAVEOEILSCRUT;
char FUSIBLEHS, ALARMECABLE;

unsigned char Tab_existance1[24]; //EXISTANCE VOIE IMPAIRE VOIR CARTOGRAPHIE FIN MEMOIRE 4 
unsigned char Tab_existance2[24]; //EXISTANCE VOIE PAIRE
char typedecarte, numdecarte;
unsigned char recherche_TPR;
unsigned char SAVADDE;

char SAVMEMOEIL;
unsigned int SAVADOEIL;
char FUNCTIONUSE;
unsigned int SAVADDHL;
unsigned int SAVtab;
unsigned char SAVr;

char DEMANDE_INFOS_CAPTEURS[11];

char SAVcaracOEIL;

unsigned int Info_reset;

void EnvoieTrame(unsigned char *cTableau, char cLongueur);
unsigned char RecoitTrame(unsigned char *cTableau, char cLongueur);
unsigned int acqui_ana(unsigned char adcon);
void Init_Tab_tpa(void);
void Delay10KTCYx(PARAM_SCLASS unsigned char);
void DELAY_125MS(int ms);
void DELAY_SW_250MS();
unsigned int Mesure_I_Conso(char cMesure, unsigned int Coeff);

unsigned int Mesure_I_Modul(char cMesure, unsigned int Coeff);

unsigned int Mesure_I_Repos(char cMesure, unsigned int Coeff);
void Prog_HC595(unsigned char rel_voies_impaires, unsigned char rel_voies_paires, unsigned char alarme_ca, unsigned char test_fus);
void DELAY_MS(int ms);
void Mesure_Freq();
unsigned char Instable(signed int iFreq1, signed int iFreq2, signed int iFreq3, signed int iFreq4);
void Calcul_Deb(unsigned int* deb_temp);
void delay_qms(char tempo);
void mesure_test_cc(unsigned char n, unsigned char i);
void InitTimer0();
unsigned int acqui_ana_16_plus(unsigned char adcon);
void IntToChar(signed int value, char *chaine, int Precision);
void MODIFICATION_TABLE_DEXISTENCE(char *v, char eff, char etat);
unsigned char LIRE_HORLOGE(unsigned char zone);
void remplir_tab_presence(char v, char pouimp, char co);
void creer_trame_horloge(void);
void ecriture_trame_i2c(char vv);
void RECUPERE_TRAME_ET_CAL_ETAT(char *hk, char t, char y);
unsigned char ECRIRE_EEPROMBYTE(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char g);
unsigned char LIRE_EEPROM(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char r);
unsigned char putstringI2C(unsigned char *wrptr);
unsigned int calcul_addmemoire(unsigned char *tab);
unsigned char choix_memoire(unsigned char *tab);
void ecrire_tab_presence_TPA(char p);
signed char litHORLOGE_i2c(unsigned char NumeroH, unsigned char AddH);
char VOIR_TABLE_EXISTENCE_POUR_OEIL(char *v);
unsigned int calcul_CRC(char far *txt, unsigned char lg_txt);
void IntToChar5(unsigned int value, char *chaine, char Precision);
void Mesure_DEB_TPA(void);
void TRANSFORMER_TRAME_MEM_EN_COM(void);
void TRAMEENVOIRS485DONNEES(void);
void RS485(unsigned char carac);
void Init_I2C(void);
void Init_RS485(void);
unsigned char ECRIRE_HORLOGE(unsigned char zone, unsigned char Time);
unsigned char envoyerHORLOGE_i2c(unsigned char NumeroH, unsigned char ADDH, unsigned char dataH0);
void init_uc(void);


void Interruption1(void);

#pragma code highVector=0x08			//valeur 0x08 pour interruptions prioritaite 

void highVector(void) {
    _asm GOTO Interruption1 _endasm // on doit �xecuter le code de la fonction interruption
}

#pragma interrupt Interruption1 

void Interruption1(void) {
    unsigned char sauv1;
    unsigned char sauv2;
    char dummy;
    char buffer_test[2];

    sauv1 = PRODL; // sauvegarde le contenu des registres de calcul
    sauv2 = PRODH;

    //===========TIMER1
    if (PIR1bits.TMR1IF) // v�rifie que l'IT est Timer1
    {
        t1ov_cnt++; // incr�mente le compteur de d�bordement
        PIR1bits.TMR1IF = 0; // efface le flag d'IT du Timer1
    }

    //===========INT0 sur RB0  

    if (INTCONbits.INT0IF == 1) // v�rifie que l'IT est INT0, origine RB0=0		
    {
        // =====> ALIM OFF
        // peut-�tre faire comme pour un reset

        Info_reset = 5555; // pour indiquer qu'il faut reprendre les mesures interrompues par l'int0
        //	Sauvegarde en eeprom ou RAM? pour red�marrage apr�s reset
        INTCONbits.INT0IF = 0; //efface le flag d'IT extern sur RB0

        Reset(); //  <================================= !!!!!!! RESET SOFT DU PIC !!!!!!! 

    }



    //===========INT1 sur RB1   POUR DETECTION DEMANDE RS485

    //  IL FAUDRA CONFIGURER: INTCON2bits.INTEDG1 = 0 si d�tection sur front descendant,  ou = 1 si d�tection sur front montant
    //   et INTCON3bits.INT1IE = 1


    //===========INT2 sur RB2  pour mesure fr�quence du MCTA
    if (INTCON3bits.INT2IF == 1) // v�rifie que l'IT est INT2, origine RB2=2		
    {
        if (cpt_int2 == 0) {
            TMR1H = 0;
            TMR1L = 0;
            t1ov_cnt = 0;
            T1CONbits.TMR1ON = 1; // timer 1 ON
            PIR1bits.TMR1IF = 0; // 0 = TMR1 register overflowed reset
        }

        ////			INTCON3bits.INT2IE = 0;	// arret interruption INT2
        if (cpt_int2 >= 20) {
            T1CONbits.TMR1ON = 0; // timer 1 OFF
            Timer1H = TMR1H;
            Timer1L = TMR1L;
            INTCON3bits.INT2IE = 0; // arret interruption INT2
        }
        cpt_int2++;
        INTCON3bits.INT2IF = 0; //efface le drapeau d'IT sur RB2
        ////			INTCON3bits.INT2IE=1;	// valide interruption INT2
    }

    PRODL = sauv1; // on restaure les registres de calcul
    PRODH = sauv2;
    //	INTCONbits.GIE = 1; 
    //	INTCONbits.PEIE = 1;

}

void main(void) {

    unsigned char cTrame[10];
    unsigned char cTestEnvoie;
    unsigned char cCara;
    unsigned char cFlag_OK;
    unsigned char cWatchDog;
    char g, k[20];
    int t;
    float h;
    unsigned int iii;
    unsigned int ttt;
    unsigned int rrr;


    TRISAbits.TRISA0 = 1; // AN0   V_ligne_AM1 	mesure tension Amont du capteur voies impaires
    TRISAbits.TRISA1 = 1; // AN1	 V_ligne_AM2 	mesure tension Amont du capteur voies paires
    TRISAbits.TRISA2 = 1; // AN2	 V_ligne_AV1_A 	mesure tension Aval du capteur voies impaires  (apr�s fusible)
    TRISAbits.TRISA3 = 1; // AN3	 V_ligne_AV2_A 	mesure tension Aval du capteur voies paires  (apr�s fusible)
    TRISAbits.TRISA5 = 1; // AN4	 I_conso_1		mesure tension(courant de consommation) du capteur voies impaires
    TRISAbits.TRISA6 = 0; //	OSC
    TRISAbits.TRISA7 = 0; //	OSC

    TRISBbits.TRISB0 = 1; //	ALIM_OFF (INT)
    TRISBbits.TRISB1 = 1; //	VAL_RS485_EXT
    TRISBbits.TRISB2 = 1; //	FREQ_TPA
    TRISBbits.TRISB3 = 1; //	HORLOGE
    TRISBbits.TRISB4 = 1; //	VAL_I2C_EXT
    TRISBbits.TRISB5 = 0; //	PGM	}
    TRISBbits.TRISB6 = 0; //	PGC	]> ICSP
    TRISBbits.TRISB7 = 0; //	PGD	}

    TRISCbits.TRISC0 = 0; //	DECH_L1			==> modif piste sur CI
    TRISCbits.TRISC1 = 0; //	OK_I2C_EXT
    TRISCbits.TRISC2 = 0; //	I2C_INTERNE
    TRISCbits.TRISC3 = 1; //	SCL_I2C
    TRISCbits.TRISC4 = 1; //	SDA_I2C

    TRISDbits.TRISD0 = 0; //	D0	
    TRISDbits.TRISD1 = 0; //	D1
    TRISDbits.TRISD2 = 0; //	D2
    TRISDbits.TRISD3 = 0; //	D3
    TRISDbits.TRISD4 = 0; //	D4
    TRISDbits.TRISD5 = 0; //	D5
    TRISDbits.TRISD6 = 0; //	D6
    TRISDbits.TRISD7 = 0; //	D7
    // 1=INPUT    0=OUTPUT
    TRISEbits.TRISE0 = 0; //	RS_DI	
    TRISEbits.TRISE1 = 0; //	R/W	
    TRISEbits.TRISE2 = 0; //	E			
    TRISEbits.TRISE3 = 0; //	Data_Rel
    TRISEbits.TRISE4 = 0; //	Clk_Rel
    TRISEbits.TRISE5 = 0; //	Rck_Rel
    TRISEbits.TRISE6 = 0; //	Rst_Rel
    TRISEbits.TRISE7 = 0; //	Test_Fus


    TRISFbits.TRISF0 = 1; // AN5   I_conso_2		mesure tension(courant de consommation) du capteur voies paires 
    TRISFbits.TRISF1 = 1; // AN6	 I_modul_1		mesure tension(courant de modulation) du capteur voies impaires
    TRISFbits.TRISF2 = 1; // AN7	 I_modul_2		mesure tension(courant de modulation) du capteur voies paires
    TRISFbits.TRISF3 = 1; // AN8   Sel A0 Lecture en entr�e Analogique!!!
    TRISFbits.TRISF4 = 1; // AN9	 Sel A1 Lecture en entr�e Analogique!!!
    TRISFbits.TRISF5 = 1; // AN10  Sel A2 Lecture en entr�e Analogique!!!
    TRISFbits.TRISF6 = 0; // DECH_L1 AN11	 <===  entr�e analogique LIBRE   ==> modif piste sur CI
    TRISFbits.TRISF7 = 0; // DECH_L2

    TRISGbits.TRISG0 = 0; //	Sel_Freq	(0 voie impaire)(1 voie paire)
    TRISGbits.TRISG1 = 0; //	CPT_ON_1
    TRISGbits.TRISG2 = 0; //	UN_CPT_1			
    TRISGbits.TRISG3 = 0; //	CPT_ON_2
    TRISGbits.TRISG4 = 0; //	UN_CPT_2
    //	(TRISGbits.TRISG5 = 1;	//	/MCLR)

    TRISHbits.TRISH0 = 0; //  AL_CABLE_ON + LED ALARME
    TRISHbits.TRISH1 = 0; //  /RST
    TRISHbits.TRISH2 = 0; //	LED_TX
    TRISHbits.TRISH3 = 0; //	LED_RX
    TRISHbits.TRISH4 = 1; // AN12  V_48V_1 mesure tension 48V voies impaires  
    TRISHbits.TRISH5 = 1; // AN13	 V_48V_2 mesure tension 48V voies paires 
    TRISHbits.TRISH6 = 1; // AN14  V_ligne_AV1_B 	mesure gain+ tension Aval du capteur voies impaires  (apr�s fusible)
    TRISHbits.TRISH7 = 1; // AN15  V_ligne_AV2_B 	mesure gain+ tension Aval du capteur voies paires  (apr�s fusible)

    TRISJbits.TRISJ0 = 0; //	CS1
    TRISJbits.TRISJ1 = 0; //	CS2
    TRISJbits.TRISJ2 = 0; //	Pt2
    TRISJbits.TRISJ3 = 1; // 	Jumper (entr�e pour TPR)
    TRISJbits.TRISJ4 = 1; //	MODE TEST	
    TRISJbits.TRISJ5 = 0; //	Val_R_I2C
    TRISJbits.TRISJ6 = 0; //	V_SUP_1
    TRISJbits.TRISJ7 = 0; //	V_SUP_2

    PORTAbits.RA4 = 1;

    TRISCbits.TRISC5 = 0;
    BAUDCON1bits.BRG16 = 1;
    TXSTA1bits.BRGH = 1; //haute vitesse //Passer � 0 a vitesse moyenne
    SPBRG1 = SPEED & 0xFF; // Write baudrate to SPBRG1
    SPBRGH1 = SPEED >> 8; // For 16-bit baud rate generation
    TXSTA1bits.SYNC = 0; // Async mode
    RCSTA1bits.SPEN = 1; // Enable serial port

    IPR1bits.RCIP = 1; // Set high priority
    PIE1bits.RCIE = 0; // ne g�n�re pas d'IT pour la liaison s�rie

    RCSTA1bits.RX9 = 0; // r�ception sur 8 bits
    TXSTA1bits.TX9 = 0; // transmission sur 8 bits
    RCONbits.IPEN = 1; // Enable priority levels
    RECEPTION = 0;
    ENVOIE = 0; // transmission inhib�e



    Init_I2C();
    Init_RS485();
    init_uc();
    Val_R_I2C = 0; // ON VALIDE LES RESISTANCES DE TIRAGES SCL ET SDA
    I2C_INTERNE = 0; //ON LAISSE LE BUS I2C A L'INTERIEUR DE LA CARTE RI



    INTCONbits.GIE = 0;
    INTCONbits.PEIE = 0;
    INTCON2bits.RBIP = 0; // interrupt Hight priorit� sur port B
    INTCONbits.RBIE = 0; //valide  interrupt sur port B
    // pour d�tecter coupure alim
    INTCONbits.TMR0IE = 0; // 0 = Disables the TMR0 overflow interrupt    
    INTCONbits.INT0IE = 1; // INT0 External Interrupt Enable bit 1 = Enables the INT0 external interrupt 
    INTCON2bits.INTEDG0 = 0; //External Interrupt 0 Edge Select bit 1 = Interrupt on rising edge 0 = Interrupt on falling edge bit 
    //
    INTCON2bits.INTEDG1 = 1; //pour interruption d�tection demande RS485 sur front montant  (RB1/INT1)

    INTCON3bits.INT1IF = 0; // reinit flag RS485 pour le premier demmarage on autorise pas la com

    INTCON3bits.INT1IE = 1; // valide l'interruption pour RS485 1 pr reactiver



    INTCONbits.GIE = 1;

    TMP[2] = 'a';
    MODIFICATION_TABLE_DEXISTENCE(&TRAMEOUT, 2, 4);


    ECRIRE_HORLOGE(0x07, 0b00010000); // ECRIRE AVANT PR MARCHE

    while (1) {




        //        cWatchDog = 0;
        //        do {
        //            cCara = RecoitTrame(&cTestEnvoie, 1);
        //            if (cTestEnvoie == '#' && cCara == 'a') {
        //                cTestEnvoie = 6;
        //                cFlag_OK = 1;
        //            } else {
        //                cTestEnvoie = 21;
        //                cFlag_OK = 0;
        //            }
        //            EnvoieTrame(&cTestEnvoie, 1);
        //        } while (cFlag_OK == 0);
        //
        //        do {
        //            cCara = RecoitTrame(cTrame, 10);
        //            if (cTrame[0] == 'T' && cTrame[1] == 'e' && cTrame[2] == 's' && cTrame[3] == 't' && cTrame[4] == 'E' && cTrame[5] == 'n' && cTrame[6] == 'v' && cTrame[7] == 'o' &&
        //                    cTrame[8] == 'i' && cTrame[9] == 'e' && cCara == 'a') {
        //                cTestEnvoie = 6;
        //                cFlag_OK = 1;
        //
        //            } else if (cTrame[0] == 'T' && cTrame[1] == 'e' && cTrame[2] == 's' && cTrame[3] == 't' && cTrame[4] == 'I' && cTrame[5] == 'n' && cTrame[6] == 't' && cTrame[7] == 'e' &&
        //                    cTrame[8] == 'r' && cTrame[9] == 'r' && cCara == 'a') {
        //                cTestEnvoie = 6;
        //                cFlag_OK = 1;
        //            } else {
        //                cTestEnvoie = 21;
        //                cFlag_OK = 0;
        //            }
        //            EnvoieTrame(&cTestEnvoie, 1);
        //            cWatchDog++;

        //	Prog_HC595(1,2,0,FUSIBLEHS); //COMMANDE RELAIS !


        //Voir si on active la memorisation


        INTCON3bits.INT1IE = 0; // devalide l'interruption pour RS485 1 pr reactiver


        TRAMEIN[0] = '#';
        TRAMEIN[1] = 'C';
        TRAMEIN[2] = 'M';
        TRAMEIN[3] = 'R';
        TRAMEIN[4] = '4';
        TRAMEIN[5] = 'O';
        TRAMEIN[6] = 'E';
        TRAMEIN[7] = 'I';
        TRAMEIN[8] = '0';
        TRAMEIN[9] = '2';
        TRAMEIN[10] = '0';
        TRAMEIN[11] = '0';
        TRAMEIN[12] = '1';
        TRAMEIN[13] = '2';
        TRAMEIN[14] = '2';
        TRAMEIN[15] = '2';
        TRAMEIN[16] = '2';
        TRAMEIN[17] = '2';
        TRAMEIN[18] = '*';


        DEMANDE_INFOS_CAPTEURS[1] = TRAMEIN[8];
        DEMANDE_INFOS_CAPTEURS[2] = TRAMEIN[9];
        DEMANDE_INFOS_CAPTEURS[3] = TRAMEIN[10];
        DEMANDE_INFOS_CAPTEURS[4] = TRAMEIN[11];
        DEMANDE_INFOS_CAPTEURS[5] = TRAMEIN[12]; // QUEL CAPTEUR

        g = VOIR_TABLE_EXISTENCE_POUR_OEIL(&DEMANDE_INFOS_CAPTEURS); //TMP variable globale contient lexistence de la voie


        for (t = 0; t < 21; t++) {
            k[t] = TMP[t];
        }

        if (TMP[1] % 2 == 1) // IMPAIRE
        {
            NOSAVEOEILSCRUT = 1; //CONTRE ORDRE DE SAV POUR VOIE PAIRE CAR DIF DE 2 
            voie_valide_2 = 0;
            voie_valide_1 = 1;
            Tab_Voie_1[0] = TMP[1];
            Tab_Voie_2[0] = TMP[1] + 1;
        }
        if (TMP[1] % 2 == 0) // PAIRE
        {
            NOSAVEOEILSCRUT = 2; ////CONTRE ORDRE DE SAV POUR VOIE IMPAIRE CAR DIF DE 1
            voie_valide_1 = 0;
            voie_valide_2 = 1;
            Tab_Voie_2[0] = TMP[1];
            Tab_Voie_1[0] = TMP[1] - 1;
        }

        //	



        DEMANDE_INFOS_CAPTEURS[0] = DEMANDE_INFOS_CAPTEURS[1];
        DEMANDE_INFOS_CAPTEURS[1] = DEMANDE_INFOS_CAPTEURS[2];
        DEMANDE_INFOS_CAPTEURS[2] = DEMANDE_INFOS_CAPTEURS[3];
        DEMANDE_INFOS_CAPTEURS[3] = DEMANDE_INFOS_CAPTEURS[4];
        DEMANDE_INFOS_CAPTEURS[4] = DEMANDE_INFOS_CAPTEURS[5];


        //PAIRES
        if (DEMANDE_INFOS_CAPTEURS[2] == '2')
            Tab_Voie_1[0] = 0;
        if (DEMANDE_INFOS_CAPTEURS[2] == '4')
            Tab_Voie_1[0] = 0;
        if (DEMANDE_INFOS_CAPTEURS[2] == '6')
            Tab_Voie_1[0] = 0;
        if (DEMANDE_INFOS_CAPTEURS[2] == '8')
            Tab_Voie_1[0] = 0;
        if (DEMANDE_INFOS_CAPTEURS[2] == '0')
            Tab_Voie_1[0] = 0;

        //IMPAIRES
        if (DEMANDE_INFOS_CAPTEURS[2] == '1')
            Tab_Voie_2[0] = 0;
        if (DEMANDE_INFOS_CAPTEURS[2] == '3')
            Tab_Voie_2[0] = 0;
        if (DEMANDE_INFOS_CAPTEURS[2] == '5')
            Tab_Voie_2[0] = 0;
        if (DEMANDE_INFOS_CAPTEURS[2] == '7')
            Tab_Voie_2[0] = 0;
        if (DEMANDE_INFOS_CAPTEURS[2] == '9')
            Tab_Voie_2[0] = 0;

        do {
            Prog_HC595(Tab_Voie_1[0], Tab_Voie_2[0], alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !		
            //Voir si on active la memorisation
            for (t = 3; t <= 19; t++) {
                if (k[t] == 'A') //si il y a un A
                {
                    dernier_TPA_2 = t - 2;
                    dernier_TPA_1 = t - 2;
                }
            }

            recherche_TPA = 0;
            Mesure_DEB_TPA();
            LIRE_EEPROM(2, 128, &TRAMEIN, 1);
            TMP[0] = '#';
            TMP[1] = DEMANDE_INFOS_CAPTEURS[0];
            TMP[2] = DEMANDE_INFOS_CAPTEURS[1];
            TMP[3] = DEMANDE_INFOS_CAPTEURS[2];
            TMP[4] = DEMANDE_INFOS_CAPTEURS[3];
            TMP[5] = DEMANDE_INFOS_CAPTEURS[4];
            //	DELAY_SEC(10);
            TMP[6] = 0; //TOUJOURS MEMOIRE 1 soit 0
            //CALCUL ZONE MEMOIRE UNIQUEMENT POUR OEIL PB fonction calcul_addmemoire ???????,,,,
            ttt = 0;
            iii = TMP[3];
            iii = iii - 48;
            ttt = ttt + 1 * iii;
            iii = TMP[2];
            iii = iii - 48;
            ttt = ttt + 10 * iii;
            iii = TMP[1];
            iii = iii - 48;
            ttt = ttt + 100 * iii;

            rrr = 2176 * ttt;

            ttt = 0;
            iii = TMP[5] - 48;
            ttt = 1 * iii;
            iii = TMP[4] - 48;
            ttt = ttt + 10 * iii;
            iii = ttt;
            ttt = rrr + 128 * iii;


            SAVMEMOEIL = 0; //SAV BUG

            SAVADOEIL = ttt;
            LIRE_EEPROM(2, 128, &TRAMEIN, 1); //LIRE DANS EEPROM
            FUNCTIONUSE = 'O';

            delay_qms(10);

            //FIN


            //	y=calcul_addmemoire(&TMP);

            //DELAY_SEC(2);
            //	INTCONbits.GIE = 0;
            //LIRE_EEPROM(2, 128, &TRAMEIN, 1); //LIRE DANS EEPROM
            //	INTCONbits.GIE = 1;

            FUNCTIONUSE = 'N';

            TRAMEOUT[0] = '#';
            TRAMEOUT[1] = 'R';
            TRAMEOUT[2] = '4';
            TRAMEOUT[3] = 'C';
            TRAMEOUT[4] = 'M';
            TRAMEOUT[5] = 'O';
            TRAMEOUT[6] = 'E';
            TRAMEOUT[7] = 'I';

            TRANSFORMER_TRAME_MEM_EN_COM();
            pointeur_out_in = 92; // SAUVEGARDE POINTEUR DU TABLEAU
            calcul_CRC(TRAMEOUT, pointeur_out_in - 1); // CALCUL LE CRC ET LE RAJOUTE AU TABLEAU TRAMEOUT[]
            TRAMEOUT[pointeur_out_in + 5] = '*'; // RAJOUT LE BYTE DE STOP 

            if (TRAMEIN[0] == 0xFF) //BUG INCONNU
            {
                TRAMEOUT[8] = '*';
            }
            FUNCTIONUSE = 'O';
            TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE 
            FUNCTIONUSE = 'N';


        } while (PORTBbits.RB1);
        //        } while ((cFlag_OK == 0) && (cWatchDog < 5));

        NOSAVEOEILSCRUT = 0;

        cTrame[0] = 0;
        cTrame[1] = 0;
        cTrame[2] = 0;
        cTrame[3] = 0;
        cTrame[4] = 0;
        cTrame[5] = 0;
        cTrame[6] = 0;
        cTrame[7] = 0;
        cTrame[8] = 0;
        cTrame[9] = 0;
    }

}

void EnvoieTrame(unsigned char *cTableau, char cLongueur) {
    int iBcl;
    ENVOIE = 1; // autoriser transmission
    for (iBcl = 0; iBcl < cLongueur; iBcl++) {
        TXREG1 = *(cTableau + iBcl);
        while (FINTRANS == 0); //Attente fin transmission  
    }
    ENVOIE = 0;
}

unsigned char RecoitTrame(unsigned char *cTableau, char cLongueur) {
    int iBcl;
    unsigned char cErreur;
    unsigned int iWatchDog;
    iWatchDog = 0;
    cErreur = 'a';
    RECEPTION = 1; // reception
    for (iBcl = 0; iBcl < cLongueur; iBcl++) {
        if (iWatchDog < 20000) {
            iWatchDog = 0;
            while ((FINRECEPTION == 0) && (iWatchDog < 20000)) {
                //            while (FINRECEPTION == 0);
                iWatchDog++;
            }
            *(cTableau + iBcl) = RCREG1;
            if (RCSTAbits.OERR || iWatchDog == 20000) {
                //            if (RCSTAbits.OERR) {
                RECEPTION = 0;
                RECEPTION = 1;
                cErreur = 'o';
                *(cTableau + iBcl) = 0; //dummy value
            } else if (RCSTAbits.FERR) {
                cErreur = 'f';
            }
        } else {
            RCREG1;
            *(cTableau + iBcl) = 0;
        }
    }
    RECEPTION = 0;
    return cErreur;
}

unsigned int acqui_ana(unsigned char adcon) {


    //LED1_ALIM_VL = ~PORTJbits.RJ1 ;		// test
    ADCON0 = adcon; // adress + abort la mesure en cours
    _ADON = 1;
    _ADGO = 1;

    while (_ADDONE) {
        //	Nop();
    }

    return ADRES;
} // fin acqui_ana ()

unsigned int acqui_ana_16_plus(unsigned char adcon) {



    unsigned long II;
    int i; // Variable usage general



    II = 0;

    for (i = 0; i < 0x10; i++) // 16 MESURES POUR LA MOYENNE
    {
        II = II + acqui_ana(adcon);
        Delay10KTCYx(1);
    }
    II >>= 4; // Moyenne pour 16 mesures

    return ((int) II); // sauvegarde de Long en Int
}

void Mesure_DEB_TPA(void) {
    unsigned int TEST; // pour tester le calcul debit cable

    int tpa;
    int scrut_TP; //indicateur r�ponse d'un tpa, seulement dans le cas ou le courant de repos est < 150�A (pour stopper la sructation d�s que mesur�, si un seul tpa sur la ligne)
    int nb; // variable compteur

    unsigned int I_Conso1_1; // pour faire moyenne sur 2 mesures, voies impaires
    unsigned int I_Conso2_1; //
    unsigned int I_Conso1_2; // pour faire moyenne sur 2 mesures, voies paires
    unsigned int I_Conso2_2; //

    unsigned int I_Repos1_1; // pour faire moyenne sur 2 mesures, voies impaires
    unsigned int I_Repos2_1; //
    unsigned int I_Repos1_2; // pour faire moyenne sur 2 mesures, voies paires
    unsigned int I_Repos2_2; //




    // POUR ERIC
    //voie_valide_1=1;
    //voie_valide_2=1; //ON REGARDE LES VOIES PAIRES ET IMPAIRES
    //recherche_TPA=1;

    LEDRX = 0;
    Memo_autorisee_1 = 1;
    Memo_autorisee_2 = 1;
    //
    COURTCIRCUIT = 0x00;
    Freq_A = 0x0000;
    Freq1_1 = 0x0000;
    Freq2_1 = 0x0000;
    Freq3_1 = 0x0000;
    Freq4_1 = 0x0000;
    Freq1_2 = 0x0000;
    Freq2_2 = 0x0000;
    Freq3_2 = 0x0000;
    Freq4_2 = 0x0000;

    Cumul_Freq_1 = 0x0000; // pour moyenne
    Cumul_Freq_2 = 0x0000; // pour moyenne

    I_Conso1_1 = 0x0000;
    I_Conso2_1 = 0x0000;
    I_Conso1_2 = 0x0000;
    I_Conso2_2 = 0x0000;
    I_Conso_1 = 0;
    I_Conso_2 = 0;
    Init_Tab_tpa();

    Nb_TPA_ok = 0; // indique le nb de capteurs trouv�s en interrogation
    fin_mesures_1 = 0; // si 1 indique que l'interrogation est termin�e  
    fin_mesures_2 = 0; // si 1 indique que l'interrogation est termin�e  



    if (recherche_TPA) //si mode recherche automatique des tp 
    {
        Nb_Capteurs_max = 17; //16 tp + 1 d�bitm�tre en 0 mettre 17
        for (nb = 0; nb <= 16; nb++) //TOUS LES CODAGES
        {
            Tab_code_1[nb] = 1;
            Tab_code_2[nb] = 1;
        }
    } else {
        if (dernier_TPA_1 >= dernier_TPA_2) {
            Nb_Capteurs_max = dernier_TPA_1; //VOIR AVEC TABLEAU EXISTANCE
        } else {
            Nb_Capteurs_max = dernier_TPA_2;
        }
    }


    tempF1 = 0x0000;
    tempF2 = 0x0000;
    tempF3 = 0x0000;
    tempF4 = 0x0000;







    //DEPART INIT
    DECH_L1 = 0;
    V_SUP1 = 0;
    UN_CPT_1 = 0;
    CPT_ON_1 = 0;
    DECH_L2 = 0;
    V_SUP2 = 0;
    UN_CPT_2 = 0;
    CPT_ON_2 = 0;





    //config pour 56V VOIE IMPAIRE
    UN_CPT_1 = 1; //VALIDE
    V_SUP1 = 0; //DEVALIDE 



    CPT_ON_1 = 0; //   <======== d�valide 56V SUR VOIE   par s�curit�
    DECH_L1 = 0; // valide d�charge VOIE 

    //config pour 56V VOIE PAIRE
    UN_CPT_2 = 1;
    V_SUP2 = 0;

    CPT_ON_2 = 0; //   <======== d�valide 56V SUR VOIE   par s�curit�
    DECH_L2 = 0; // valide d�charge VOIE 




    tpa = 0;
    scrut_TP = 1; // tant que  = 1 => scrutation


    DECH_L1 = 1;
    DECH_L2 = 1;



    creer_trame_horloge();
    envoyerHORLOGE_i2c(0xD0, 0, 0b00000000);

    while (HORLOGE == 0); //attend passage � 0 de RB3, boucle tant que la condition n'est pas arriv�e HORLOGE=PORTBbits.RB3


    while (HORLOGE == 1); //attend passage � 1 de RB3, boucle tant que la condition n'est pas arriv�e HORLOGE=PORTBbits.RB3


    LEDRX = !LEDRX;
    //===>	0Ms
    if (voie_valide_1 == 1) { // si voie impaire valid�e
        CPT_ON_1 = 1; //   <======== valide voie impaire de mesure � 56v
    }
    if (voie_valide_2 == 1) { //	si voie paire valid�e
        CPT_ON_2 = 1; //   <======== valide voie paire de mesure � 56v
    }



    DECH_L1 = 0; // lib�re la d�charge ligne impaire
    DECH_L2 = 0; // lib�re la d�charge ligne  paire

    Delay10KTCYx(10); //peut-�tre � ajuster


    for (tpa = 0; tpa < Nb_Capteurs_max; tpa++) //tpa=0 POUR LECTURE EN ZERO (DEBITMETRE)
    {

        if (recherche_TPA)
            LATHbits.LATH0 = !LATHbits.LATH0; //SIGNALISATION EXTERNE



        LEDRX = !LEDRX;
        Freq_A = 0;
        Freq1_1 = 0x0000;
        Freq2_1 = 0x0000;
        Freq3_1 = 0x0000;
        Freq4_1 = 0x0000;
        Freq1_2 = 0x0000;
        Freq2_2 = 0x0000;
        Freq3_2 = 0x0000;
        Freq4_2 = 0x0000;
        tempF1 = 0;
        tempF2 = 0;
        tempF3 = 0;
        tempF4 = 0;
        Cumul_Freq_1 = 0;
        Cumul_Freq_2 = 0;


        DELAY_125MS(2); //Tempo1  250ms!!!!!!!!!!!!!!!!!!!! on stop a 250ms (AAA)

        DELAY_SW_250MS(); // TEMPO2  la Tempo2 se termine � 	while(!(INTCONbits.TMR0IF)); on lance la tempo 250ms au niveau du timer0

        Delay10KTCYx(10); //<== voir s'il faut plus de tempo  pour mesurer un peu plus tard la conso  


        if (voie_valide_1 == 1) { // si voie impaire valid�e
            I_Conso_1 = Mesure_I_Conso(IConso1_AN4, Coeff_I_Conso1);
        }
        if (voie_valide_2 == 1) { // si voie paire valid�e
            I_Conso_2 = Mesure_I_Conso(IConso2_AN5, Coeff_I_Conso2);
        }

        Delay10KTCYx(10);

        if (voie_valide_1 == 1) { // si voie impaire valid�e
            I_Conso_1 = (I_Conso_1 + Mesure_I_Conso(IConso1_AN4, Coeff_I_Conso1)) / 2; // moyenne 
            I_Modul_1 = Mesure_I_Modul(IModul1_AN6, Coeff_I_Modul1);
        }
        if (voie_valide_2 == 1) { // si voie impaire valid�e
            I_Conso_2 = (I_Conso_2 + Mesure_I_Conso(IConso2_AN5, Coeff_I_Conso2)) / 2; // moyenne 
            I_Modul_2 = Mesure_I_Modul(IModul2_AN7, Coeff_I_Modul2);
        }

        //attente fin de tempo
        // TEST CC
        if (I_Conso_1 >= Icons_cc)// AU DESSU DE 15mA (4000ohms) sur la ligne resistif >100k (0.6mA)
        {
            COURTCIRCUIT = 0xF1;
            Prog_HC595(0, Tab_Voie_2[0], alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !
            if (I_Conso_2 >= Icons_cc)// AU DESSU DE 15mA (4000ohms) sur la ligne resistif >100k (0.6mA)
            {
                Prog_HC595(0, 0, alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !
                COURTCIRCUIT = 0xFF;
            }
        }
        if (I_Conso_2 >= Icons_cc)// AU DESSU DE 15mA (4000ohms) sur la ligne resistif >100k (0.6mA)
        {
            COURTCIRCUIT = 0xF2;
            Prog_HC595(Tab_Voie_1[0], 0, alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !
            if (I_Conso_1 >= Icons_cc)// AU DESSU DE 15mA (4000ohms) sur la ligne resistif >100k (0.6mA)
            {
                Prog_HC595(0, 0, alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !
                COURTCIRCUIT = 0xFF;
            }
        }


        while (!(INTCONbits.TMR0IF)); // FIN de TEMPO2      wait until TMR0 rolls over  FIN (BBB)

        // ====> 500mS
        if (recherche_TPA)
            LATHbits.LATH0 = !LATHbits.LATH0; //SIGNALISATION EXTERNE

        LEDRX = !LEDRX;

        DELAY_MS(60); // 125ms -5 ms ajustement pour avoir 625ms pour lancer la premiere mesure de frequence (CCC)

        LEDRX = !LEDRX;

        //voie impaire
        if (voie_valide_1 == 1) { // si voie impaire valid�e
            Sel_Freq = 0; //commutateur de fr�quence voie impaire
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); // mesure de la fr�quence 	
                Freq1_1 += Freq_A; //m�morise 1�re valeur de fr�quence (pour tester instabilit�)

            }
        }
        //voie paire
        if (voie_valide_2 == 1) { // si voie paire valid�e
            Sel_Freq = 1; //commutateur de fr�quence voie paire 
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); // mesure de la fr�quence 	
                Freq1_2 += Freq_A; //m�morise 1�re valeur de fr�quence (pour tester instabilit�)
            }
        }


        LEDRX = !LEDRX;
        DELAY_MS(60); // 125ms
        LEDRX = !LEDRX;
        //voie impaire
        if (voie_valide_1 == 1) { // si voie impaire valid�e
            Sel_Freq = 0; //commutateur de fr�quence voie impaire
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); // mesure de la fr�quence 	
                Freq2_1 += Freq_A; //m�morise 1�re valeur de fr�quence (pour tester instabilit�)
            }
        }
        //voie paire
        if (voie_valide_2 == 1) { // si voie paire valid�e
            Sel_Freq = 1; //commutateur de fr�quence voie paire 
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); // mesure de la fr�quence 	
                Freq2_2 += Freq_A; //m�morise 1�re valeur de fr�quence (pour tester instabilit�)
            }
        }

        LEDRX = !LEDRX;
        DELAY_MS(60); // 125ms
        LEDRX = !LEDRX;
        //voie impaire
        if (voie_valide_1 == 1) { // si voie impaire valid�e
            Sel_Freq = 0; //commutateur de fr�quence voie impaire
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); // mesure de la fr�quence 	
                Freq3_1 += Freq_A; //m�morise 1�re valeur de fr�quence (pour tester instabilit�)
            }
        }
        //voie paire
        if (voie_valide_2 == 1) { // si voie paire valid�e
            Sel_Freq = 1; //commutateur de fr�quence voie paire 
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); // mesure de la fr�quence 	
                Freq3_2 += Freq_A; //m�morise 1�re valeur de fr�quence (pour tester instabilit�)
            }
        }

        LEDRX = !LEDRX;
        DELAY_MS(60); // 125ms   
        LEDRX = !LEDRX;
        //voie impaire
        if (voie_valide_1 == 1) { // si voie impaire valid�e
            Sel_Freq = 0; //commutateur de fr�quence voie impaire
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); // mesure de la fr�quence 	
                Freq4_1 += Freq_A; //m�morise 1�re valeur de fr�quence (pour tester instabilit�)
            }
        }
        //voie paire
        if (voie_valide_2 == 1) { // si voie paire valid�e
            Sel_Freq = 1; //commutateur de fr�quence voie paire 
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); // mesure de la fr�quence 	
                Freq4_2 += Freq_A; //m�morise 1�re valeur de fr�quence (pour tester instabilit�)
            }
        }

        LEDRX = !LEDRX;

        if (tpa == 2) {

            LEDRX = !LEDRX;

        }

        if (I_Conso_1 > i_conso_min) // soit I_Conso mini >= 2.0mA pour led verte => pr�sence capteur   (i_conso est x10)
        {

            if (voie_valide_1 == 1) { // si voie impaire valid�e
                Tab_Etat_1[tpa] |= Instable(Freq1_1, Freq2_1, Freq3_1, Freq4_1); // v�rification de l'instabilit�
                Freq_TPA_1 = (int) (Cumul_Freq_1 / 8); // 8 => 4 x 2 mesures
            }


        } else {
            Freq_TPA_1 = 0;
        }

        if (I_Conso_2 > i_conso_min) // soit I_Conso mini >= 2.0mA pour led verte => pr�sence capteur   (i_conso est x10)
        {

            if (voie_valide_2 == 1) { // si voie paire valid�e
                Tab_Etat_2[tpa + 1] |= Instable(Freq1_2, Freq2_2, Freq3_2, Freq4_2);
                Freq_TPA_2 = (int) (Cumul_Freq_2 / 8); // 8 => 4 x 2 mesures
            }
        } else {
            Freq_TPA_2 = 0;
        }


        if (tpa > 0) {
            Tab_Pression_1[tpa] = Freq_TPA_1;
            Tab_Pression_2[tpa] = Freq_TPA_2; //1 = OFFSET POUR VOIE PAIRE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        }
        //CALCUL DEBIT
        if (!tpa) {
            Calcul_Deb(Tab_Pression_1[0]);
            TEST = debit_cable;
            Tab_Pression_1[0] = debit_cable;

            Calcul_Deb(Tab_Pression_2[0]);
            TEST = debit_cable;
            Tab_Pression_2[0] = debit_cable;
        }
        // ====> 500mS + 375mS + 375mS ==> 1250mS 
        //Attente de la chute du signal d'horloge pour synchro avec 1.5sec

        if (HORLOGE == 1) {
            while (HORLOGE == 1);
            while (HORLOGE == 0); //attend passage � 0 de RB3, boucle tant que la condition n'est pas arriv�e Synchro1s=PORTBbits.RB3
        }

        while (HORLOGE == 0); //attend passage � 0 de RB3, boucle tant que la condition n'est pas arriv�e Synchro1s=PORTBbits.RB3

        LEDRX = !LEDRX;


        //<=================ICI 1.5SEC================>
        DELAY_MS(150); // 1 x 150ms  peut-�tre � ajuster (LLL)

        //#####################			// MESURE le courant de repos VOIE IMPAIRE
        if (voie_valide_1) // si voie paire valid�e
        {
            I_Repos_1 = Mesure_I_Repos(IRepos1_AN2, Coeff_I_Repos1);
            DELAY_MS(5);
            I_Repos_1 = (I_Repos_1 + Mesure_I_Repos(IRepos1_AN2, Coeff_I_Repos1)) / 2;
            if (I_Repos_1 <= 15) {
                I_Repos_1 = 0;
            } // seuil pour �viter d'afficher un courant de repos si rien de branch� 
            // car sinon il y a un minimum de 12�A d� � l'offset de l'ampli

            Tab_I_Repos_1[tpa] = I_Repos_1; //I_Repos est en (�A)	  
            Tab_I_Modul_1[tpa] = I_Modul_1; //I_Modul est en (mA x10)

            //######  Retranche le courant de repos du courant de modulation
            I_Conso_1 *= 100; // met � la m�me �chelle que i_repos   (i_conso est x10)
            if (I_Conso_1 > I_Repos_1) { // pour ne pas prendre en compte si pas de capteur
                I_Conso_1 -= I_Repos_1; // retranche I_Repos de I_Conso  (i_conso est x10)
            } else {
                I_Conso_1 = 0;
            }
            I_Conso_1 /= 100; // remet � l'�chelle pour i_conso
            //######
            I_Conso_1 += (I_Modul_1 / 2); // TEST  ON RAJOUTE LA MOITI� DE LA MODULATION
            Tab_I_Conso_1[tpa] = I_Conso_1; //I_Conso en (mA x10) 
        } //FIN de if(voie_valide_1)

        //#####################			// MESURE le courant de repos VOIE PAIRE



        if (voie_valide_2 == 1) // si voie paire valid�e 1
        {
            I_Repos_2 = Mesure_I_Repos(IRepos2_AN3, Coeff_I_Repos2);
            DELAY_MS(5);
            I_Repos_2 = (I_Repos_2 + Mesure_I_Repos(IRepos2_AN3, Coeff_I_Repos2)) / 2;
            if (I_Repos_2 <= 15) {
                I_Repos_2 = 0;
            } // seuil pour �viter d'afficher un courant de repos si rien de branch� 
            // car sinon il y a un minimum de 12�A d� � l'offset de l'ampli

            Tab_I_Repos_2[tpa] = I_Repos_2; //I_Repos est en (�A)	  
            Tab_I_Modul_2[tpa] = I_Modul_2; //I_Modul est en (mA x10)


            //######  Retranche le courant de repos du courant de modulation
            I_Conso_2 *= 100; // met � la m�me �chelle que i_repos   (i_conso est x10) 2
            if (I_Conso_2 > I_Repos_2) { // pour ne pas prendre en compte si pas de capteur
                I_Conso_2 -= I_Repos_2; // retranche I_Repos de I_Conso  (i_conso est x10)
            } else {
                I_Conso_2 = 0;
            }
            I_Conso_2 = I_Conso_2 / 100; // remet � l'�chelle pour i_conso
            //######
            I_Conso_2 += (I_Modul_2 / 2); // TEST  ON RAJOUTE LA MOITI� DE LA MODULATION
            Tab_I_Conso_2[tpa] = I_Conso_2; //I_Conso en (mA x10)   1 OFFSET POUR VOIE PAIRE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        } //FIN de if(voie_valide_1==1)


        //Attente de la remont�e du signal d'horloge pour synchro avec 2sec
        while (HORLOGE == 1); //attend passage � 1 de RB3, boucle tant que la condition n'est pas arriv�e HORLOGE=PORTBbits.RB3






        //FIN DE BOUCLE MESURE CAPTEURS PCPG  ou groupe
        if (scrut_TP == 0) // A REVOIR!!!!!!!!!!!!!!!!!!!!!!!!!!
        {
            Nb_Capteurs_max = tpa; // utilis� en mode scrolling  -1
            fin_mesures_1 = 1; // A REVOIR!!!!!!!!!!!!!!!!!!!!!!!!!!
            fin_mesures_2 = 1; // A REVOIR!!!!!!!!!!!!!!!!!!!!!!!!!!
            break; //<======!!!!!
        }
    } // FIN DE :	for(tpa=0; tpa < Nb_Capteurs_max ;tpa++)


    CPT_ON_1 = 0; //   <======== d�valide 56V SUR VOIE  IMPAIRE
    CPT_ON_2 = 0; //   <======== d�valide 56V SUR VOIE  PAIRE

    delay_qms(100);

    DECH_L1 = 1; // d�charge la ligne impaire
    DECH_L2 = 1; // d�charge la ligne	paire

    delay_qms(100);
    DECH_L1 = 0; // d�charge la ligne impaire
    DECH_L2 = 0; // d�charge la ligne	paire

    V_SUP1 = 0;
    UN_CPT_1 = 0;
    V_SUP2 = 0;
    UN_CPT_2 = 0;

    Prog_HC595(0, 0, alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !


    //	ICI SAUVEGARDES EN EEPROM DU PIC ou 24LC256 A VOIR!!!!!!!!!!!!!!!!!!!


    //	ICI SAUVEGARDES EN EEPROM 24LC256  A VOIR!!!!!!!!!!!!!!!!!!!

    if (voie_valide_1 == 1) // si voie impaire valid�e
    {
        Memo_autorisee_1 = 1;
        Memo_autorisee_2 = 0;
        ecriture_trame_i2c(1);
    }

    if (BREAKSCRUTATION == 0xFF)
        goto BREAK;

    if (voie_valide_2 == 1) // si voie paire valid�e
    {
        Memo_autorisee_1 = 0;
        Memo_autorisee_2 = 1;
        ecriture_trame_i2c(2);
    }

    if (BREAKSCRUTATION == 0xFF)
        goto BREAK;

BREAK:
    mesure_test_cc(Tab_Voie_1[0], Tab_Voie_2[0]); //ecris CC ou pas 

    LIRE_EEPROM(2, 128, &TRAMEIN, 1);
}

void Init_Tab_tpa(void) //reset tableau de mesures des tpa
{
    int i, j;
    for (i = 0; i <= 16; i++) {
        //			Tab_Code[i]=0;			//17 caract�res	  1 d�bitmetre et 16 TP
        Tab_Pression_1[i] = 0; //17 caract�res	
        Tab_I_Repos_1[i] = 0; //17 caract�res	  
        Tab_I_Modul_1[i] = 0; //17 caract�res		
        Tab_I_Conso_1[i] = 0; //34 caract�res	
        Tab_Etat_1[i] = 0; //17 caract�res	

        Tab_Pression_2[i] = 0; //17 caract�res	
        Tab_I_Repos_2[i] = 0; //17 caract�res	  
        Tab_I_Modul_2[i] = 0; //17 caract�res		
        Tab_I_Conso_2[i] = 0; //34 caract�res	
        Tab_Etat_2[i] = 0; //17 caract�res	

    }

    for (j = 0; j <= 9; j++) //10 caract�res
    {
        Tab_Horodatage[j] = 32; // 32 = espace
    }

}

void creer_trame_horloge(void) {

    char HEURE, MINUTE, SECONDE, MOIS, JOUR, ANNEE, DATE;


    ANNEE = LIRE_HORLOGE(6);
    delay_qms(10);
    MOIS = LIRE_HORLOGE(5);
    delay_qms(10);
    DATE = LIRE_HORLOGE(4);
    delay_qms(10);
    JOUR = LIRE_HORLOGE(3);
    delay_qms(10);
    HEURE = LIRE_HORLOGE(2);
    delay_qms(10);
    MINUTE = LIRE_HORLOGE(1);
    delay_qms(10);
    SECONDE = LIRE_HORLOGE(0);

    Tab_Horodatage[1] = (DATE & 0x0F) + 48;
    Tab_Horodatage[0] = ((DATE & 0x30) >> 4) + 48; // conversion BCD => ASCII

    Tab_Horodatage[3] = (MOIS & 0x0F) + 48;
    Tab_Horodatage[2] = ((MOIS & 0x10) >> 4) + 48;

    Tab_Horodatage[5] = (SECONDE & 0x0F) + 48;
    Tab_Horodatage[4] = ((SECONDE & 0x70) >> 4) + 48;

    Tab_Horodatage[7] = (HEURE & 0x0F) + 48;
    Tab_Horodatage[6] = ((HEURE & 0x30) >> 4) + 48; // conversion BCD => ASCII


    Tab_Horodatage[9] = (MINUTE & 0x0F) + 48;
    Tab_Horodatage[8] = ((MINUTE & 0x70) >> 4) + 48;



    Tab_Horodatage[11] = (ANNEE & 0x0F) + 48;
    Tab_Horodatage[10] = ((ANNEE & 0xF0) >> 4) + 48;
}

void DELAY_125MS(int ms) {
    int i;
    if (ms >= 1000) //PROBLEME BUG EN SCRUTATION AUTO
        ms = 2;

    T0CON = 0x83; /* enable TMR0, select instruction clock, prescaler set to 1.16 */
    for (i = 0; i < ms; i++) {
        InitTimer0();
    }
    return;
}

void DELAY_SW_250MS() {

    T0CON = 0x84; /* enable TMR0, select instruction clock, prescaler set to 1.16 */
    TMR0H = 0x48; //Preload = 18661
    TMR0L = 0xE5;
    INTCONbits.TMR0IE = 0;
    INTCONbits.TMR0IF = 0;
    // while(!(INTCONbits.TMR0IF)); /* wait until TMR0 rolls over */=> dans le programme apr�s l'instruction delay
    return;
}

unsigned int Mesure_I_Conso(char cMesure, unsigned int Coeff) // Acquisition de la valeur CAN du courant de consommation en MAx10
{
    float Val_Temp_Conso2;
    Val_Temp_Conso2 = 10 * q * acqui_ana_16_plus(cMesure) * Coeff / 100; // AJUSTEMENT GAIN
    return ((char) Val_Temp_Conso2);
}


// --------- Mesure courant de modulation du capteur ----------------

unsigned int Mesure_I_Modul(char cMesure, unsigned int Coeff) // Acquisition de la valeur CAN du courant de modulation MA*10
{
    float Val_Temp_Modul2;
    Val_Temp_Modul2 = 10 * q * acqui_ana_16_plus(cMesure) * Coeff / 100; // AJUSTEMENT GAIN
    return ((char) Val_Temp_Modul2);
}

void Prog_HC595(unsigned char rel_voies_impaires, unsigned char rel_voies_paires, unsigned char alarme_ca, unsigned char test_fus) {
    unsigned char i;
    unsigned char j;
    unsigned char val;
    unsigned char registre1; //registres des HC595 pour commutation des relais voies et alarme et test fusible
    unsigned char registre2;
    unsigned char registre3;
    registre1 = 0b00000000; // relais 1 � 8
    registre2 = 0b00000000; // relais 9 � 16
    registre3 = 0b00000000; // relais 17 � 20 + alarme c�ble + test fusibles voies

    if (rel_voies_impaires > 0) {
        switch (rel_voies_impaires) {
            case 1:
                registre1 |= 0b00000001; // relais 1
                break;
            case 3:
                registre1 |= 0b00000100; // relais 3
                break;
            case 5:
                registre1 |= 0b00010000; // relais 5
                break;
            case 7:
                registre1 |= 0b01000000; // relais 7
                break;
            case 9:
                registre2 |= 0b00000001; // relais 9
                break;
            case 11:
                registre2 |= 0b00000100; // relais 11
                break;
            case 13:
                registre2 |= 0b00010000; // relais 13
                break;
            case 15:
                registre2 |= 0b01000000; // relais 15
                break;
            case 17:
                registre3 |= 0b00000001; // relais 17
                break;
            case 19:
                registre3 |= 0b00000100; // relais 19
                break;
        }
    }
    // Choix Relais		
    if (rel_voies_paires > 0) {
        switch (rel_voies_paires) {
            case 2:
                registre1 |= 0b00000010; // relais 2
                break;
            case 4:
                registre1 |= 0b00001000; // relais 4
                break;
            case 6:
                registre1 |= 0b00100000; // relais 6
                break;
            case 8:
                registre1 |= 0b10000000; // relais 8
                break;
            case 10:
                registre2 |= 0b00000010; // relais 10
                break;
            case 12:
                registre2 |= 0b00001000; // relais 12
                break;
            case 14:
                registre2 |= 0b00100000; // relais 14
                break;
            case 16:
                registre2 |= 0b10000000; // relais 16
                break;
            case 18:
                registre3 |= 0b00000010; // relais 18
                break;
            case 20:
                registre3 |= 0b00001000; // relais 20
                break;
        }
    }
    //test si alarme c�ble . si oui mofification du registre 3
    if (alarme_ca) {
        registre3 |= 0b00010000; //"ou logique"  pour si alarme c�ble
    }
    // pour TEST si fusion fusible voies . si oui mofification du registre 3
    if (test_fus) {
        registre3 |= 0b00100000; //"ou logique"  pour si test fusion fusible
    }

    j = 24; // correspond aux nombres de pins ou coups d'horloge


    Rst_HC595 = 0; // reset les "shift regitrers"

    Rst_HC595 = 1; // reset les "shift regitrers"

    Rck_HC595 = 0;
    delay_qms(1);
    Rck_HC595 = 1;


    for (i = j; i > 0; i--) // 23= Nb de pins-1
    {
        if (i > 16) {
            val = registre3 & 0x80;
            registre3 <<= 1; //relais de 17 � 24
        }

        if (i > 8 && i < 17) {
            val = registre2 & 0x80;
            registre2 <<= 1; //relais de 9 � 16
        }
        if (i < 9) {
            val = registre1 & 0x80;
            registre1 <<= 1; //relais de 1 � 8
        }

        if (val == 0x80)
            val = 0xFF;
        Data_HC595 = val;
        Clk_HC595 = 1;
        Clk_HC595 = 0;

    }

    Rck_HC595 = 0;
    Rck_HC595 = 1;
    Rst_HC595 = 1; // reset les "shift regitrers" 

}

void DELAY_MS(int ms) {
    int bb;

    //PORTHbits.RH3=0;
    if (ms >= 1000) //PROBLEME BUG EN SCRUTATION AUTO
        ms = 2;
    T0CON = 0x88; /* enable TMR0, select instruction clock, prescaler set to 1.1 */
    for (bb = 0; bb < ms; bb++) {

        TMR0H = 0xE8; //Preload = 59536
        TMR0L = 0x90;
        INTCONbits.TMR0IE = 0;
        INTCONbits.TMR0IF = 0;
        while (!(INTCONbits.TMR0IF)); /* wait until TMR0 rolls over */

    }
    //PORTHbits.RH3=1;
    return;

}

void Mesure_Freq() // ne pas modifier critique!!!
{
    unsigned int cpt_to; //compteur time out

    // mesure sur 1 p�riode du signal fr�quence capteur par int2(RB2) d�clench�e sur front montant et par le nbre de boucles du timer1 sur 16bits
    // l'horloge du timer1 est celle du quarts de 24Mhz/4= 6Mhz, donc une boucle timer = 1 / (6Mhz) = 0.000000166sec = 0.166�s
    // la valeur de la fr�quence capteur = 1/(0.166�s * val_timer1)
    // valeur d'1 p�riode =   [(t1ov_cnt * 65536 + TempH * 256 + TempL) * 0.166]  normalement   t1ov_cnt sera �gal � 0 si >0 => erreur

    t1ov_cnt = 0x00; // compteur de d�bordement timer1
    cpt_int2 = 0x00;
    Freq_A = 0x0000; // fr�quence de capteur
    tempF1 = 0x0000;
    tempF2 = 0x0000;
    tempF3 = 0x0000;
    tempF4 = 0x0000;
    TMR1H = 0x0000; // registre H 8bit du timer 1
    TMR1L = 0x0000; // registre L 16bit du timer 1
    Timer1L = 0x0000;
    Timer1H = 0x0000;


    PIR1bits.TMR1IF = 0; // efface le drapeau d'IT
    T1CONbits.T1CKPS0 = 0; // pr�diviseur  1:1
    T1CONbits.T1CKPS1 = 0; // pr�diviseur  1:1
    T1CONbits.RD16 = 0; // timer sur 8bits SINON TROP DE PB
    T1CONbits.TMR1CS = 0; // TIMER1 sur horloge interne (Fosc/4)
    T1CONbits.T1RUN = 0; // source autre que oscillateur du timer 1
    T1CONbits.T1OSCEN = 0; // turn off resistor feeback 
    IPR1 = 0x01; // set Timer1 to high priority	
    PIE1 = 0x01; // enable Timer1 roll-over interrupt
    cpt_int2 = 0;
    INTCONbits.RBIE = 0; //d�valide  interrupt sur port B  
    INTCON2bits.RBPU = 1; // pull-up port B         � voir  <<<< provoque une erreur sur l'entre de 2V 
    INTCON2bits.INTEDG2 = 0; //INT2 sur front montant   <<<<<
    INTCON2bits.RBIP = 0; // interrupt Hight priorit� sur port B   <<<<<
    T1CONbits.TMR1ON = 0; // stop timer 1 

    INTCONbits.GIEH = 1; // Toutes les IT d�masqu�es autoris�es
    INTCONbits.PEIE = 1; // valide ttes les int haute priorit�
    INTCON3bits.INT2IP = 1; // INT2 en haute priorit�

    INTCON3bits.INT2IF = 0; // flag d'interruption int2 � reseter 
    INTCON3bits.INT2IE = 1; // valide interruption INT2

    cpt_to = 0x0000; // compteur time-out

    while (cpt_to < 10000 && cpt_int2 < 21) {
        cpt_to++;
    }

    INTCONbits.GIE = 0; //disable global interrupt  
    INTCONbits.PEIE = 0; //d�valide ttes les int haute priorit�
    INTCON3bits.INT2IE = 0; // arret interruption INT2

    tempF1 = (Timer1H * 256 + Timer1L);
    tempF1 = (tempF1 + t1ov_cnt * 65535) / 20;
    if (tempF1 > 0) {
        tempF1 += 0x0015;
        tempF2 = tempF1 * 0xA6; //0xA6 = * 166
        tempF3 = 1000000 / tempF2;
        tempF4 = tempF3 * 1000; //x1000
        Freq_A = (int) tempF4;

        if (!Sel_Freq) { //commutateur de fr�quence voie impaire
            Cumul_Freq_1 += tempF4;
        }
        if (Sel_Freq) { //commutateur de fr�quence voie paire
            Cumul_Freq_2 += tempF4;
        }
    } else {
        Freq_A = 0x0000;
    }

    T1CONbits.RD16 = 0; // timer sur 8bits
    TMR1H = 0x0000; // registre H 8bit du timer 1
    TMR1L = 0x0000; // registre L 16bit du timer 1

}

unsigned char Instable(signed int iFreq1, signed int iFreq2, signed int iFreq3, signed int iFreq4) // test instabilit�
{
    float fDifF12, fDifF23, fDifF34;
    unsigned char cSortie;

    fDifF12 = iFreq1 - iFreq2;
    fDifF12 = abs(fDifF12);

    fDifF23 = iFreq2 - iFreq3;
    fDifF23 = abs(fDifF23);

    fDifF34 = iFreq3 - iFreq4;
    fDifF34 = abs(fDifF34);

    if (fDifF12 > max_instable || fDifF23 > max_instable || fDifF34 > max_instable) {
        cSortie = 0x80; // instable
    } else {
        cSortie = 0;
    }
    return (cSortie);

}

void Calcul_Deb(unsigned int* deb_temp) {


    // Echelle d�bitm�tre 1000 � 1500Hz pour 0L/H � 1000 et 500L/H � 1500Hz

    if (*deb_temp >= 0x03E5 && *deb_temp <= 0x03E8) // 3Hz de tol�rance � voir !!
    {
        *deb_temp = 0;
    } else if ((*deb_temp < 0x03E5) || (*deb_temp >= 0x05DC)) //997 inferieur a 1000 alors on met 9999
    {
        *deb_temp = 0x270F; //9999
    } else {
        *deb_temp = (*deb_temp - 0x03E8); // -1000
    }
}

unsigned int Mesure_I_Repos(char cMesure, unsigned int Coeff) // Acquisition de la valeur CAN de la tension ligne AVal capteur �A
{
    float Val_Temp_Repos2;
    Val_Temp_Repos2 = (q * acqui_ana_16_plus(cMesure) * Coeff / 1);
    return ((int) Val_Temp_Repos2);
} // fin

void delay_qms(char tempo) {
    char j, k, l;

    for (j = 0; j < tempo; j++) {
        for (k = 0; k < 10; k++) {
            for (l = 0; l < 40; l++) {
                //__no_operation();
                //__no_operation();
                //__no_operation();
                //__no_operation();
                //__no_operation();
            }
        }
    }
}

void ecriture_trame_i2c(char vv) //choisir voie a traiter ATTENTION CELA CALCUL LETAT AUSSI !!!!!!!!!!!!!!!!!!!!! EN FONCTION DU SEUIL
{// ET RECUPERE LES INFOS FIXE


    unsigned char tpa;
    unsigned char i;
    unsigned char c; //	pour constitution			
    //unsigned int tmpI;
    float tmpf;
    unsigned int iAdresse;

    // VOIES IMPAIRES############################################################
    //	*********** CodeVoie  ***********
    //	CODE  de 0 � 16  sachant sue le 0 est le d�bitm�tre 



    DELAY_MS(100); // CAR DES FOIS UNE CARTE PARTAIT EN LIVE !!!!!!!!
    for (i = 0; i <= 16; i++) // il faut faire autant d'enregistrement que de capteurs  A VOIR POUR LES DEBITMETRES !!!!!!!!!!!!!!!
    {
        BREAKSCRUTATION = 0x00;
        if ((vv != 1)&&(vv != 2)) //PROBLEME ON RESET !!!! SCRUTATION AUTOMATIQUE
        {




            BREAKSCRUTATION = 0xFF;
            goto finscrut;

        }

        if (vv == 1) // si autorisation de sauvegarde voie impaire valid�e
        {
            tpa = Tab_code_1[i]; //CODAGE A SAV
            if (tpa > 0) {
                // Sauvegarde 
                No_voie = Tab_Voie_1[0];
                No_codage = Tab_code_1[i]; //A FAIRE 16 SAUVEGARDES	MAX + DEBITMETRE A VOIR !!!!!!!!!!!!!!!!!!	//au sens existence
            }




            //VOIE PAIRE 
            TRAMEOUT[0] = '#';
            IntToChar(No_voie, TMP, 3);
            TRAMEOUT[1] = TMP[0];
            TRAMEOUT[2] = TMP[1];
            TRAMEOUT[3] = TMP[2];
            IntToChar(i, TMP, 2);
            TRAMEOUT[4] = TMP[0];
            TRAMEOUT[5] = TMP[1];

            //	if (No_voie==11)
            //	{			
            //	TRAMEOUT[50]=TMP[0]; //TEST
            //	}
            RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT, 1, i); //RECUPERE LES ELEMENTS FIXE EN MOIRE CONSTIT....SEUIL......ETC //PRECEDENT CYCLE


            TRAMEOUT[6] = 'A'; // ADRESSABLE OU RESITIF 

            IntToChar(Tab_I_Modul_1[i], TMP, 4);
            TRAMEOUT[7] = TMP[0];
            TRAMEOUT[8] = TMP[1];
            TRAMEOUT[9] = TMP[2];
            TRAMEOUT[10] = TMP[3];

            IntToChar(Tab_I_Conso_1[i], TMP, 4);
            TRAMEOUT[11] = TMP[0];
            TRAMEOUT[12] = TMP[1];
            TRAMEOUT[13] = TMP[2];
            TRAMEOUT[14] = TMP[3];


            IntToChar(Tab_I_Repos_1[i], TMP, 4);
            TRAMEOUT[15] = TMP[0];
            TRAMEOUT[16] = TMP[1];
            TRAMEOUT[17] = TMP[2];
            TRAMEOUT[18] = TMP[3];


            //CONSTITUTION 

            TRAMEOUT[19] = Tab_Constitution[0]; //info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT,1,i);
            TRAMEOUT[20] = Tab_Constitution[1];
            TRAMEOUT[21] = Tab_Constitution[2];
            TRAMEOUT[22] = Tab_Constitution[3];
            TRAMEOUT[23] = Tab_Constitution[4];
            TRAMEOUT[24] = Tab_Constitution[5];
            TRAMEOUT[25] = Tab_Constitution[6];
            TRAMEOUT[26] = Tab_Constitution[7];
            TRAMEOUT[27] = Tab_Constitution[8];
            TRAMEOUT[28] = Tab_Constitution[9];
            TRAMEOUT[29] = Tab_Constitution[10];
            TRAMEOUT[30] = Tab_Constitution[11];
            TRAMEOUT[31] = Tab_Constitution[12];

            if (recherche_TPA) {
                TRAMEOUT[19] = 'x';
                TRAMEOUT[20] = 'x';
                TRAMEOUT[21] = 'x';
                TRAMEOUT[22] = 'x';
                TRAMEOUT[23] = 'x';
                TRAMEOUT[24] = 'x';
                TRAMEOUT[25] = 'x';
                TRAMEOUT[26] = 'x';
                TRAMEOUT[27] = 'x';
                TRAMEOUT[28] = 'x';
                TRAMEOUT[29] = 'x';
                TRAMEOUT[30] = 'x';
                TRAMEOUT[31] = 'x';
            }

            //COMMENTAIRE CABLE


            TRAMEOUT[32] = Tab_Commentaire_cable[0]; //info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT,1,i);
            TRAMEOUT[33] = Tab_Commentaire_cable[1];
            TRAMEOUT[34] = Tab_Commentaire_cable[2];
            TRAMEOUT[35] = Tab_Commentaire_cable[3];
            TRAMEOUT[36] = Tab_Commentaire_cable[4];
            TRAMEOUT[37] = Tab_Commentaire_cable[5];
            TRAMEOUT[38] = Tab_Commentaire_cable[6];
            TRAMEOUT[39] = Tab_Commentaire_cable[7];
            TRAMEOUT[40] = Tab_Commentaire_cable[8];
            TRAMEOUT[41] = Tab_Commentaire_cable[9];
            TRAMEOUT[42] = Tab_Commentaire_cable[10];
            TRAMEOUT[43] = Tab_Commentaire_cable[11];
            TRAMEOUT[44] = Tab_Commentaire_cable[12];
            TRAMEOUT[45] = Tab_Commentaire_cable[13];
            TRAMEOUT[46] = Tab_Commentaire_cable[14];
            TRAMEOUT[47] = Tab_Commentaire_cable[15];
            TRAMEOUT[48] = Tab_Commentaire_cable[16];
            TRAMEOUT[49] = Tab_Commentaire_cable[17];

            if (recherche_TPA) {
                TRAMEOUT[32] = 'x';
                TRAMEOUT[33] = 'x';
                TRAMEOUT[34] = 'x';
                TRAMEOUT[35] = 'x';
                TRAMEOUT[36] = 'x';
                TRAMEOUT[37] = 'x';
                TRAMEOUT[38] = 'x';
                TRAMEOUT[39] = 'x';
                TRAMEOUT[40] = 'x';
                TRAMEOUT[41] = 'x';
                TRAMEOUT[42] = 'x';
                TRAMEOUT[43] = 'x';
                TRAMEOUT[44] = 'x';
                TRAMEOUT[45] = 'x';
                TRAMEOUT[46] = 'x';
                TRAMEOUT[47] = 'x';
                TRAMEOUT[48] = 'x';
                TRAMEOUT[49] = 'x';
            }






            IntToChar(No_voie, TMP, 3);
            TRAMEOUT[50] = TMP[0];
            TRAMEOUT[51] = TMP[1];
            TRAMEOUT[52] = TMP[2];



            TRAMEOUT[53] = Tab_Horodatage[0]; //Jour
            TRAMEOUT[54] = Tab_Horodatage[1]; //Jour
            TRAMEOUT[55] = Tab_Horodatage[2]; // MOIS
            TRAMEOUT[56] = Tab_Horodatage[3]; //MOIS
            TRAMEOUT[57] = Tab_Horodatage[6]; //HEURE
            TRAMEOUT[58] = Tab_Horodatage[7]; //HEURE
            TRAMEOUT[59] = Tab_Horodatage[8]; //MIN
            TRAMEOUT[60] = Tab_Horodatage[9]; //MIN



            TRAMEOUT[61] = '?';

            //CODAGE
            IntToChar(i, TMP, 2);
            TRAMEOUT[62] = TMP[0];
            TRAMEOUT[63] = TMP[1];

            //POS
            if (i == 0) {
                TMP[0] = '0';
                TMP[1] = '1';
            } else {
                TMP[0] = '0';
                TMP[1] = '2';
            }
            //IntToChar(i+1, TMP,2);
            TRAMEOUT[64] = TMP[0]; //POS
            TRAMEOUT[65] = TMP[1]; //POS




            TRAMEOUT[66] = '?';
            TRAMEOUT[67] = '?';
            TRAMEOUT[68] = '?';
            TRAMEOUT[69] = '?';







            //DISTANCE
            TRAMEOUT[70] = Tab_Distance[0]; //info recuperer par	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT,1,i);
            TRAMEOUT[71] = Tab_Distance[1];
            TRAMEOUT[72] = Tab_Distance[2];
            TRAMEOUT[73] = Tab_Distance[3];
            TRAMEOUT[74] = Tab_Distance[4];

            if (recherche_TPA) {
                TRAMEOUT[70] = '?';
                TRAMEOUT[71] = '?';
                TRAMEOUT[72] = '?';
                TRAMEOUT[73] = '?';
                TRAMEOUT[74] = '?';
            }

            //ETAT

            IntToChar(Tab_Etat_1[i], TMP, 2);
            TRAMEOUT[75] = TMP[0];
            TRAMEOUT[76] = TMP[1];



            //FREQUENCE
            // TEST L'INSTABILITE QD SCRUTATION test la valeur
            //if ((TRAMEOUT[81]=='0')&&(TRAMEOUT[82]=='8')&&(TRAMEOUT[83]=='0')&&(TRAMEOUT[84]<'5')&&(TRAMEOUT[84]>'1'))
            //{
            tmp_I = 0;

            tmp_I2 = TRAMEIN[77];
            tmp_I = tmp_I + 1000 * (tmp_I2 - 48);
            tmp_I2 = TRAMEIN[78];
            tmp_I = tmp_I + 100 * (tmp_I2 - 48);
            tmp_I2 = TRAMEIN[79];
            tmp_I = tmp_I + 10 * (tmp_I2 - 48);
            tmp_I2 = TRAMEIN[80];
            tmp_I = tmp_I + (tmp_I2 - 48);
            if (Tab_Pression_1[i] != 0) {
                if ((tmp_I < (Tab_Pression_1[i] - 20)) || (tmp_I > (Tab_Pression_1[i] + 20)))// il est instable car variation sur 20mbar
                {
                    //TRAMEOUT[75]='5'; //ON MET L'ETAT S/R avec la valeur donc instable  08/03/2018 SUP 
                    //TRAMEOUT[76]='0'; ON LAISSE SI ETAT PRECEDENT = INT 
                } // voir si qd plus le cas letat redevient normal											
            }
            //}// FIN INSTABILITE


            IntToChar(Tab_Pression_1[i], TMP, 4);
            TRAMEOUT[77] = TMP[0];
            TRAMEOUT[78] = TMP[1];
            TRAMEOUT[79] = TMP[2];
            TRAMEOUT[80] = TMP[3];

            IntToChar(Tab_Seuil_1[i], TMP, 4);
            TRAMEOUT[81] = TMP[0];
            TRAMEOUT[82] = TMP[1];
            TRAMEOUT[83] = TMP[2];
            TRAMEOUT[84] = TMP[3];



            if (recherche_TPA) //SEUIL AUTO LORS DE LA SCRUTATION
            {

                Tab_Seuil_1[i] = Tab_Pression_1[i] - 60;
                if (Tab_Seuil_1[i] <= 900)
                    Tab_Seuil_1[i] = 900; //SI LA VALEUR EST INFERIEUR A 900mbar

                IntToChar(Tab_Seuil_1[i], TMP, 4);
                TRAMEOUT[81] = TMP[0];
                TRAMEOUT[82] = TMP[1];
                TRAMEOUT[83] = TMP[2];
                TRAMEOUT[84] = TMP[3];

            }






            //CRC
            TRAMEOUT[85] = '2';
            TRAMEOUT[86] = '2';
            TRAMEOUT[87] = '2';
            TRAMEOUT[88] = '2';
            TRAMEOUT[89] = '2';
            TRAMEOUT[90] = '*';





            remplir_tab_presence(No_voie, 1, i);

        }

        if (vv == 2) // si autorisation de sauvegarde voie paire valid�e
        {

            tpa = Tab_code_2[i];
            if (tpa > 0) {
                // Sauvegarde 
                No_voie = Tab_Voie_2[0];
                No_codage = Tab_code_2[i]; //A FAIRE 16 SAUVEGARDES	MAX 
            }
            //VOIE IMPAIRE 
            TRAMEOUT[0] = '#';
            IntToChar(No_voie, TMP, 3);
            TRAMEOUT[1] = TMP[0];
            TRAMEOUT[2] = TMP[1];
            TRAMEOUT[3] = TMP[2];
            IntToChar(i, TMP, 2);
            TRAMEOUT[4] = TMP[0];
            TRAMEOUT[5] = TMP[1];


            RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT, 2, i); //RECUPERE LES ELEMENTS FIXE EN MOIRE CONSTIT....SEUIL......ETC						

            TRAMEOUT[6] = 'A'; // ADRESSABLE OU RESITIF 

            IntToChar(Tab_I_Modul_2[i], TMP, 4);
            TRAMEOUT[7] = TMP[0];
            TRAMEOUT[8] = TMP[1];
            TRAMEOUT[9] = TMP[2];
            TRAMEOUT[10] = TMP[3];

            IntToChar(Tab_I_Conso_2[i], TMP, 4);
            TRAMEOUT[11] = TMP[0];
            TRAMEOUT[12] = TMP[1];
            TRAMEOUT[13] = TMP[2];
            TRAMEOUT[14] = TMP[3];


            IntToChar(Tab_I_Repos_2[i], TMP, 4);
            TRAMEOUT[15] = TMP[0];
            TRAMEOUT[16] = TMP[1];
            TRAMEOUT[17] = TMP[2];
            TRAMEOUT[18] = TMP[3];



            TRAMEOUT[19] = Tab_Constitution[0]; //info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT,1,i);
            TRAMEOUT[20] = Tab_Constitution[1];
            TRAMEOUT[21] = Tab_Constitution[2];
            TRAMEOUT[22] = Tab_Constitution[3];
            TRAMEOUT[23] = Tab_Constitution[4];
            TRAMEOUT[24] = Tab_Constitution[5];
            TRAMEOUT[25] = Tab_Constitution[6];
            TRAMEOUT[26] = Tab_Constitution[7];
            TRAMEOUT[27] = Tab_Constitution[8];
            TRAMEOUT[28] = Tab_Constitution[9];
            TRAMEOUT[29] = Tab_Constitution[10];
            TRAMEOUT[30] = Tab_Constitution[11];
            TRAMEOUT[31] = Tab_Constitution[12];


            if (recherche_TPA) {
                TRAMEOUT[19] = 'x';
                TRAMEOUT[20] = 'x';
                TRAMEOUT[21] = 'x';
                TRAMEOUT[22] = 'x';
                TRAMEOUT[23] = 'x';
                TRAMEOUT[24] = 'x';
                TRAMEOUT[25] = 'x';
                TRAMEOUT[26] = 'x';
                TRAMEOUT[27] = 'x';
                TRAMEOUT[28] = 'x';
                TRAMEOUT[29] = 'x';
                TRAMEOUT[30] = 'x';
                TRAMEOUT[31] = 'x';
            }


            TRAMEOUT[32] = Tab_Commentaire_cable[0]; //info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT,1,i);
            TRAMEOUT[33] = Tab_Commentaire_cable[1];
            TRAMEOUT[34] = Tab_Commentaire_cable[2];
            TRAMEOUT[35] = Tab_Commentaire_cable[3];
            TRAMEOUT[36] = Tab_Commentaire_cable[4];
            TRAMEOUT[37] = Tab_Commentaire_cable[5];
            TRAMEOUT[38] = Tab_Commentaire_cable[6];
            TRAMEOUT[39] = Tab_Commentaire_cable[7];
            TRAMEOUT[40] = Tab_Commentaire_cable[8];
            TRAMEOUT[41] = Tab_Commentaire_cable[9];
            TRAMEOUT[42] = Tab_Commentaire_cable[10];
            TRAMEOUT[43] = Tab_Commentaire_cable[11];
            TRAMEOUT[44] = Tab_Commentaire_cable[12];
            TRAMEOUT[45] = Tab_Commentaire_cable[13];
            TRAMEOUT[46] = Tab_Commentaire_cable[14];
            TRAMEOUT[47] = Tab_Commentaire_cable[15];
            TRAMEOUT[48] = Tab_Commentaire_cable[16];
            TRAMEOUT[49] = Tab_Commentaire_cable[17];


            if (recherche_TPA) {
                TRAMEOUT[32] = 'x';
                TRAMEOUT[33] = 'x';
                TRAMEOUT[34] = 'x';
                TRAMEOUT[35] = 'x';
                TRAMEOUT[36] = 'x';
                TRAMEOUT[37] = 'x';
                TRAMEOUT[38] = 'x';
                TRAMEOUT[39] = 'x';
                TRAMEOUT[40] = 'x';
                TRAMEOUT[41] = 'x';
                TRAMEOUT[42] = 'x';
                TRAMEOUT[43] = 'x';
                TRAMEOUT[44] = 'x';
                TRAMEOUT[45] = 'x';
                TRAMEOUT[46] = 'x';
                TRAMEOUT[47] = 'x';
                TRAMEOUT[48] = 'x';
                TRAMEOUT[49] = 'x';
            }



            IntToChar(No_voie, TMP, 3);
            TRAMEOUT[50] = TMP[0];
            TRAMEOUT[51] = TMP[1];
            TRAMEOUT[52] = TMP[2];



            TRAMEOUT[53] = Tab_Horodatage[0]; //Jour
            TRAMEOUT[54] = Tab_Horodatage[1]; //Jour
            TRAMEOUT[55] = Tab_Horodatage[2]; // MOIS
            TRAMEOUT[56] = Tab_Horodatage[3]; //MOIS
            TRAMEOUT[57] = Tab_Horodatage[6]; //HEURE
            TRAMEOUT[58] = Tab_Horodatage[7]; //HEURE
            TRAMEOUT[59] = Tab_Horodatage[8]; //MIN
            TRAMEOUT[60] = Tab_Horodatage[9]; //MIN


            TRAMEOUT[61] = '?';
            IntToChar(i, TMP, 2);
            TRAMEOUT[62] = TMP[0];
            TRAMEOUT[63] = TMP[1];

            //POS
            if (i == 0) {
                TMP[0] = '0';
                TMP[1] = '1';
            } else {
                TMP[0] = '0';
                TMP[1] = '2';
            }
            //IntToChar(i+1, TMP,2);
            TRAMEOUT[64] = TMP[0]; //POS
            TRAMEOUT[65] = TMP[1]; //POS



            TRAMEOUT[66] = '?';
            TRAMEOUT[67] = '?';
            TRAMEOUT[68] = '?';
            TRAMEOUT[69] = '?';

            //DISTANCE
            TRAMEOUT[70] = Tab_Distance[0]; //info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT,1,i);
            TRAMEOUT[71] = Tab_Distance[1];
            TRAMEOUT[72] = Tab_Distance[2];
            TRAMEOUT[73] = Tab_Distance[3];
            TRAMEOUT[74] = Tab_Distance[4];


            if (recherche_TPA) {
                TRAMEOUT[70] = '?';
                TRAMEOUT[71] = '?';
                TRAMEOUT[72] = '?';
                TRAMEOUT[73] = '?';
                TRAMEOUT[74] = '?';
            }

            //ETAT
            IntToChar(Tab_Etat_2[i], TMP, 2); // voir si etat pr resistif
            TRAMEOUT[75] = TMP[0];
            TRAMEOUT[76] = TMP[1];

            //FREQUENCE


            tmp_I = 0;

            tmp_I2 = TRAMEIN[77];
            tmp_I = tmp_I + 1000 * (tmp_I2 - 48);
            tmp_I2 = TRAMEIN[78];
            tmp_I = tmp_I + 100 * (tmp_I2 - 48);
            tmp_I2 = TRAMEIN[79];
            tmp_I = tmp_I + 10 * (tmp_I2 - 48);
            tmp_I2 = TRAMEIN[80];
            tmp_I = tmp_I + (tmp_I2 - 48);
            if (Tab_Pression_2[i] != 0) {
                if ((tmp_I < (Tab_Pression_2[i] - 20)) || (tmp_I > (Tab_Pression_2[i] + 20)))// il est instable car variation sur 20mbar
                {
                    //	TRAMEOUT[75]='5'; //ON MET L'ETAT S/R avec la valeur donc instable  08/03/2018 SUP
                    //TRAMEOUT[76]='0'; // ON LAISSE LETAT PRECEDENT SI INT
                } // voir si qd plus le cas letat redevient normal											
            }
            //}// FIN INSTABILITE


            IntToChar(Tab_Pression_2[i], TMP, 4);
            TRAMEOUT[77] = TMP[0];
            TRAMEOUT[78] = TMP[1];
            TRAMEOUT[79] = TMP[2];
            TRAMEOUT[80] = TMP[3];

            IntToChar(Tab_Seuil_2[i], TMP, 4);
            TRAMEOUT[81] = TMP[0];
            TRAMEOUT[82] = TMP[1];
            TRAMEOUT[83] = TMP[2];
            TRAMEOUT[84] = TMP[3];

            if (recherche_TPA) {


                Tab_Seuil_2[i] = Tab_Pression_2[i] - 60;
                if (Tab_Seuil_2[i] <= 900)
                    Tab_Seuil_2[i] = 900; //SI VALEUR <=
                IntToChar(Tab_Seuil_2[i], TMP, 4);
                TRAMEOUT[81] = TMP[0];
                TRAMEOUT[82] = TMP[1];
                TRAMEOUT[83] = TMP[2];
                TRAMEOUT[84] = TMP[3];

            }
            //CRC
            TRAMEOUT[85] = '2';
            TRAMEOUT[86] = '2';
            TRAMEOUT[87] = '2';
            TRAMEOUT[88] = '2';
            TRAMEOUT[89] = '2';
            TRAMEOUT[90] = '*';

            remplir_tab_presence(No_voie, 2, i);

        }
        Sauvegarde_en_cours = 1;

        INTCON3bits.INT1IE = 0; // devalide l'interruption pour RS485 1 pr reactiver

        //CODE V
        if (Memo_autorisee_1 || Memo_autorisee_2) //  si on memorise
        {
            if ((NOSAVEOEILSCRUT == 0) || (NOSAVEOEILSCRUT == 2)) //CONTRE ORDRE PAR OEIL OU SCRUTATION VOIE UNIQUE
            {
                if (vv == 2) //Si voie paire
                    iAdresse = 128 * i;
                ECRIRE_EEPROMBYTE(2, iAdresse, &TRAMEOUT, 1); //ECRITURE EN EEPROM seulement codage1
            } else {
                DELAY_MS(100);
            }
            if ((NOSAVEOEILSCRUT == 0) || (NOSAVEOEILSCRUT == 1)) //CONTRE ORDRE PAR OEIL OU SCRUTATION VOIE UNIQUE
            {
                if (vv == 1) //Si voie impaire
                    ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEOUT), calcul_addmemoire(&TRAMEOUT), &TRAMEOUT, 1); //ECRITURE EN EEPROM seulement codage1
            } else {
                DELAY_MS(100);
            }
        }
        DELAY_MS(10);
        LIRE_EEPROM(2, iAdresse, &TRAMEIN, 1); //LIRE DANS EEPROM
        DELAY_MS(10);
    } // FIN DU FOR

    //CODE V
    if (vv == 1 && recherche_TPA == 1) {
        if ((NOSAVEOEILSCRUT == 0) || (NOSAVEOEILSCRUT == 1)) //CONTRE ORDRE PAR OEIL OU SCRUTATION VOIE UNIQUE
        {
            ecrire_tab_presence_TPA(1);
        } else {
            DELAY_MS(100);
        }
    }
    if (vv == 2 && recherche_TPA == 1) {
        if ((NOSAVEOEILSCRUT == 0) || (NOSAVEOEILSCRUT == 2)) //CONTRE ORDRE PAR OEIL OU SCRUTATION VOIE UNIQUE
        {
            ecrire_tab_presence_TPA(2);
        } else {
            DELAY_MS(100);
        }

    }

finscrut:


    LIRE_EEPROM(2, 128, &TRAMEIN, 1);
    Sauvegarde_en_cours = 0; // Sauvegarde termin�e
    INTCON3bits.INT1IE = 1; // valide l'interruption pour RS485 1 pr reactiver

}

void mesure_test_cc(unsigned char n, unsigned char i) // test si la voie est en CC
{
    if ((COURTCIRCUIT == 0xF1) || (COURTCIRCUIT == 0xFF)) {
        //VOIE IMPAIRE CC OK
        TRAMEOUT[1] = '0';
        TRAMEOUT[2] = '0';
        TRAMEOUT[3] = n + 48;
        if (n >= 10) {
            TRAMEOUT[2] = '1';
            TRAMEOUT[3] = (n - 10) + 48;
        }
        if (n >= 20) {
            TRAMEOUT[2] = '2';
            TRAMEOUT[3] = '0';
        }

        TRAMEOUT[4] = '0';
        TRAMEOUT[5] = '0';

        MODIFICATION_TABLE_DEXISTENCE(&TRAMEOUT, 2, 6); //mettre 'c' sur la table existence
    }


    if ((COURTCIRCUIT == 0xF2) || (COURTCIRCUIT == 0xFF)) {



        TRAMEOUT[1] = '0';
        TRAMEOUT[2] = '0';
        TRAMEOUT[3] = i + 48;
        if (i >= 10) {
            TRAMEOUT[2] = '1';
            TRAMEOUT[3] = (i - 10) + 48;
        }
        if (i >= 20) {
            TRAMEOUT[2] = '2';
            TRAMEOUT[3] = '0';
        }
        TRAMEOUT[4] = '0';
        TRAMEOUT[5] = '0';

        MODIFICATION_TABLE_DEXISTENCE(&TRAMEOUT, 2, 7); //mettre 'c' sur la table existence
    }
}

void InitTimer0() {
    TMR0H = 0x48; //Preload = 18661
    TMR0L = 0xE5;
    INTCONbits.TMR0IE = 0;
    INTCONbits.TMR0IF = 0;
    while (!(INTCONbits.TMR0IF)); /* wait until TMR0 rolls over */
}

void IntToChar(signed int value, char *chaine, int Precision) {
    signed int Mil, Cent, Diz, Unit;
    int count = 0;

    // initialisation des variables
    Mil = 0;
    Cent = 0;
    Diz = 0;
    Unit = 0;
    // si la valeur n'est pas nulle
    if (value != 0) {
        if (Precision >= 4) // si l'utilisateur d�sire les milliers
        {
            // conversion des milliers
            Mil = value / 1000;
            if (Mil != 0) {
                *(chaine + count) = Mil + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        if (Precision >= 3) // si l'utilisateur d�sire les centaines
        {
            // conversion des centaines
            Cent = value - (Mil * 1000);
            Cent = Cent / 100;
            if (Cent != 0) {
                *(chaine + count) = Cent + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        if (Precision >= 2) // si l'utilisateur d�sire les dizaines
        {
            // conversion des dizaines
            Diz = value - (Mil * 1000) - (Cent * 100);
            Diz = Diz / 10;
            if (Diz != 0) {
                *(chaine + count) = Diz + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        // conversion unit�s
        Unit = value - (Mil * 1000) - (Cent * 100) - (Diz * 10);
        *(chaine + count) = Unit + 48;
        if (*(chaine + count) < 48) // limites : 0 et 9
            *(chaine + count) = 48;
        if (*(chaine + count) > 57)
            *(chaine + count) = 57;
    } else // if (value == 0)
    {
        //*(chaine) = 32; // ecrit un espace devant le nombre
        for (Mil = 0; Mil < Precision; Mil++) // inscription de '0' dans toute la chaine
            *(chaine + Mil) = 48;
    }

} // fin de la fonction de conversion INT => CHAR

void RECUPERE_TRAME_ET_CAL_ETAT(char *hk, char t, char y) //RECUPERE LES ELEMENTS FIXE EN MOIRE CONSTIT....SEUIL......ETC
{
    unsigned int jp[4];
    char j;


    LIRE_EEPROM(choix_memoire(hk), calcul_addmemoire(hk), &TRAMEIN, 1); //LIRE DANS EEPROM



    ALARMECABLE = 0; //AUCUNE ALARME

    //jp0;
    TMPINT = 0;

    //RECUPERATION DU SEUIL
    if (t == 1) {

        if (y == 2) //TEST
        {
            jp[0] = (TRAMEIN[81] - 48); //TEST 
        }

        //calcul
        jp[0] = (TRAMEIN[81] - 48);
        jp[1] = (TRAMEIN[82] - 48);
        jp[2] = (TRAMEIN[83] - 48);
        jp[3] = (TRAMEIN[84] - 48);

        TMPINT = jp[3] + 10 * jp[2] + 100 * jp[1] + 1000 * jp[0];

        Tab_Seuil_1[y] = TMPINT;

        if (Tab_Etat_1[y] == 0x80)//INSTABLE	 
        {
            Tab_Etat_1[y] = 50; //devient SR 
        }

        //calcul etat
        if (Tab_Etat_1[y] != 0x80)//INSTABLE
        {
            if (Tab_Pression_1[y] >= Tab_Seuil_1[y]) //superieure
                Tab_Etat_1[y] = 0x00; //OK 0 en decimal

            if (Tab_Pression_1[y] < Tab_Seuil_1[y]) //inferieur
            {
                Tab_Etat_1[y] = 0x1E; //ALARME 30 en decimal
                //nbreALARMES=nbreALARMES+1;
                //AL_CABLE=0; // SORTIE ALARME CABLE
            }


            if (Tab_Pression_1[y] == 0) //si Resistif ou Adressable donne 0 l'etat devient S/R
                Tab_Etat_1[y] = 50; //H/G 40 en decimal	




            if ((Tab_Pression_1[y] < 900)&&(Tab_Pression_1[y] > 5)) //entre 5mbar et 800
                Tab_Etat_1[y] = 40; //H/G 40 en decimal	





            if (Tab_Pression_1[y] > 1700) //1555 avant
            {
                Tab_Etat_1[y] = 40; //H/G 40 en decimal	
                if (typedecarte == 'M') //SI 1999 resistif qui a exist�
                {
                    Tab_Etat_1[y] = 50; //H/G 50 en decimal
                }

            }

            if ((y == 0)&&(Tab_Pression_1[0] >= 9000)) //debit absent 
                Tab_Etat_1[y] = 0;



        }


        if ((!recherche_TPA)&&(!recherche_TPR)) {
            if (TRAMEIN[76] == '1') //SI EN INTERVENTION
                Tab_Etat_1[y] = Tab_Etat_1[y] + 1;
        } else {
            if (TRAMEIN[76] == '1') //SI EN INTERVENTION ON ENLEVE INTERVENTION PENDANT LA SCRUTATION
                Tab_Etat_1[y] = Tab_Etat_1[y] - 1;
        }



    }

    if (t == 2) {

        if (y == 10) {

        }



        jp[0] = (TRAMEIN[81] - 48);
        jp[1] = (TRAMEIN[82] - 48);
        jp[2] = (TRAMEIN[83] - 48);
        jp[3] = (TRAMEIN[84] - 48);

        TMPINT = jp[3] + 10 * jp[2] + 100 * jp[1] + 1000 * jp[0];



        Tab_Seuil_2[y] = TMPINT;

        if (Tab_Etat_2[y] == 0x80)//INSTABLE	 
        {
            Tab_Etat_2[y] == 50; //devient SR 
        }


        if (Tab_Etat_2[y] != 0x80)//SI PAS INSTABLE
        {



            if (Tab_Pression_2[y] >= Tab_Seuil_2[y]) //superieure
                Tab_Etat_2[y] = 0x00; //OK

            if (Tab_Pression_2[y] < Tab_Seuil_2[y]) //inferieur
            {
                Tab_Etat_2[y] = 0x1E; //ALARME 30 en decimal 
            }

            if (Tab_Pression_2[y] == 0) //si Resistif ou Adressable donne 0 l'etat devient S/R
                Tab_Etat_2[y] = 50; //S/R 50 en decimal	



            if ((Tab_Pression_2[y] < 900)&&(Tab_Pression_2[y] > 5)) //entre 5mbar et 800	{
                Tab_Etat_2[y] = 40; //H/G 40 en decimal	




            if (Tab_Pression_2[y] > 1700) {
                Tab_Etat_2[y] = 40; //H/G 40 en decimal	
                if (typedecarte == 'M') //SI 1999 resistif qui a exist�
                {
                    Tab_Etat_2[y] = 50; //H/G 50 en decimal
                }
            }




            if ((y == 0)&&(Tab_Pression_2[0] >= 9000)) //debit absent 
                Tab_Etat_2[y] = 0;


        }



        if ((!recherche_TPA)&&(!recherche_TPR)) {
            if (TRAMEIN[76] == '1') //SI EN INTERVENTION
                Tab_Etat_2[y] = Tab_Etat_2[y] + 1;
        } else {
            if (TRAMEIN[76] == '1') //SI EN INTERVENTION ON ENLEVE INTERVENTION PENDANT LA SCRUTATION
                Tab_Etat_2[y] = Tab_Etat_2[y] - 1;
        }




    }



    for (j = 0; j < 13; j++) {
        Tab_Constitution[j] = TRAMEIN[19 + j];
    }

    for (j = 0; j < 18; j++) {
        Tab_Commentaire_cable[j] = TRAMEIN[32 + j];
    }

    for (j = 0; j < 5; j++) {
        Tab_Distance[j] = TRAMEIN[70 + j];
    }
}

void remplir_tab_presence(char v, char pouimp, char co) //remplie
{


    Tab_existance2[0] = '#';
    Tab_existance1[0] = '#';

    if (pouimp == 1) {
        Tab_existance1[1] = v;
        Tab_existance1[2] = 'a'; //POUR L'INSTANT VOIE ADRESSABLE

        if (v == 5) {




            v = 5;
        }

        if (Tab_Pression_1[co] == 0) // pas de capteur pression=0
            Tab_existance1[co + 3] = 'F';
        if (Tab_Pression_1[co] != 0) // Il y a une reponse adressable (modulation)
            Tab_existance1[co + 3] = 'A';
        if (Tab_Pression_1[0] == 9999) // pas de debimetre debit =9999
            Tab_existance1[3] = 'F';

        if (Tab_existance1[3] == 'A') //TEST
        {


            Tab_existance2[0] = '#';
        }




        if (co == 16) {
            Tab_existance1[20] = numdecarte + 48;
            Tab_existance1[21] = Tab_existance1[2];
            Tab_existance1[22] = 'x';
            Tab_existance1[23] = '*';



        }


    }

    if (pouimp == 2) {

        Tab_existance2[1] = v;
        Tab_existance2[2] = 'a'; //POUR L'INSTANT VOIE ADRESSABLE



        if (Tab_Pression_2[co] == 0) // pas de capteur pression=0
            Tab_existance2[co + 3] = 'F';
        if (Tab_Pression_2[co] != 0) // Il y a une rfeponse adressable (modulation)
            Tab_existance2[co + 3] = 'A';
        if (Tab_Pression_2[0] == 9999) // pas de debimetre debit =9999
            Tab_existance2[3] = 'F';
        if (co == 16) {
            Tab_existance2[20] = numdecarte + 48;
            Tab_existance2[21] = Tab_existance2[2];
            Tab_existance2[22] = typedecarte; //le type de carte
            Tab_existance2[23] = '*';
        }
    }
}

unsigned char ECRIRE_EEPROMBYTE(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char g) {
    unsigned char TMP2;
    unsigned char AddH, AddL;



    if ((ADDE != SAVADDE)&&(FUNCTIONUSE == 'R'))
        ADDE = SAVADDE;
    if ((ADDHL != SAVADDHL)&&(FUNCTIONUSE == 'R')) {
        g = SAVr;
        ADDHL = SAVADDHL;
        tab = TMP;
    }


    //MAXERIE A2 
    if (ADDE == 3)
        ADDE = 6;
    if (ADDE == 2)
        ADDE = 4;
    if (ADDE == 1)
        ADDE = 2;




    TMP2 = ADDE;
    TMP2 = TMP2 << 1;
    TMP2 = 0XA0 | TMP2;


    AddL = (ADDHL);
    AddH = (ADDHL) >> 8;

    if (g == 1) // POUR FONCTIONNEMENT TRAME MEMOIRE		
        tab[85] = '*'; //FIN DE TRAME MEMOIRE			
    if (g == 3) // POUR FONCTIONNEMENT TRAME MEMOIRE		
        tab[115] = '*'; //FIN DE TRAME GROUPE
    if (g == 4) // POUR FONCTIONNEMENT TRAME MEMOIRE		
        tab[23] = '*'; //FIN DE TRAME EXISTANCE




    IdleI2C(); // ensure module is idle
    StartI2C(); // initiate START condition
    while (SSPCON2bits.SEN); // wait until start condition is over 
    WriteI2C(TMP2); // write 1 byte - R/W bit should be 0
    IdleI2C(); // ensure module is idle
    WriteI2C(AddH); // write HighAdd byte to EEPROM 
    IdleI2C(); // ensure module is idle
    WriteI2C(AddL); // write LowAdd byte to EEPROM
    IdleI2C(); // ensure module is idle
    putstringI2C(tab); // pointer to data for page write
    IdleI2C(); // ensure module is idle
    StopI2C(); // send STOP condition
    while (SSPCON2bits.PEN); // wait until stop condition is over 
    return ( 0); // return with no error	

}

unsigned char putstringI2C(unsigned char *wrptr) {



    unsigned char x;

    unsigned int PageSize;
    PageSize = 128;


    for (x = 0; x < PageSize; x++) // transmit data until PageSize  
    {
        if (SSPCON1bits.SSPM3) // if Master transmitter then execute the following
        { // 


            if (putcI2C(*wrptr)) // write 1 byte
            {
                return ( -3); // return with write collision error
            }


            IdleI2C(); // test for idle condition
            if (SSPCON2bits.ACKSTAT) // test received ack bit state
            {
                return ( -2); // bus device responded with  NOT ACK
            } // terminateputstringI2C() function
        } else // else Slave transmitter
        {
            PIR1bits.SSPIF = 0; // reset SSPIF bit


            SSPBUF = *wrptr; // load SSPBUF with new data


            SSPCON1bits.CKP = 1; // release clock line 
            while (!PIR1bits.SSPIF); // wait until ninth clock pulse received

            if ((!SSPSTATbits.R_W) && (!SSPSTATbits.BF))// if R/W=0 and BF=0, NOT ACK was received
            {
                return ( -2); // terminateputstringI2C() function
            }
        }



        wrptr++; // increment pointer 



    } // continue data writes until null character
    return ( 0);
}

unsigned int calcul_addmemoire(unsigned char *tab) {
    unsigned int t;
    unsigned int i;
    unsigned int r;



    t = 0;
    i = tab[3];
    i = i - 48;
    t = t + 1 * i;
    i = tab[2];
    i = i - 48;
    t = t + 10 * i;
    i = tab[1];
    i = i - 48;
    t = t + 100 * i;


    r = 2176 * (t);
    if (t > 29)
        r = 2176 * (t - 30);
    if (t > 59)
        r = 2176 * (t - 60);
    if (t > 89)
        r = 2176 * (t - 90);


    t = 0;
    i = tab[5] - 48;
    t = 1 * i;
    i = tab[4] - 48;
    t = t + 10 * i;
    i = t;

    t = r + 128 * i;



    delay_qms(10);



    return t;


}

// choix du numero memoire suivant N�voie

unsigned char choix_memoire(unsigned char *tab) {
    unsigned int i;
    unsigned int t;
    unsigned char r;




    t = 0;
    i = tab[3] - 48;
    t = t + 1 * i;
    i = tab[2] - 48;
    t = t + 10 * i;
    i = tab[1] - 48;
    t = t + 100 * i;

    r = 0;
    if (t > 29)
        r = 1;
    if (t > 59)
        r = 2;
    if (t > 89)
        r = 3;




    delay_qms(10);
    return r;

}

unsigned char LIRE_EEPROM(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char r) {
    char h;
    unsigned char t;
    unsigned char TMP2;
    unsigned char TMP4;
    unsigned char AddH, AddL;

    if ((ADDE != SAVMEMOEIL)&&(FUNCTIONUSE == 'O'))
        ADDE = SAVMEMOEIL;

    if ((ADDHL != SAVADOEIL)&&(FUNCTIONUSE == 'O')) {
        ADDHL = SAVADOEIL;
        tab = TRAMEIN;
    }


    //if (r==2)
    //r=2;


    if ((ADDE != SAVADDE)&&(FUNCTIONUSE == 'R'))
        ADDE = SAVADDE;
    if ((ADDHL != SAVADDHL)&&(FUNCTIONUSE == 'R')) {
        r = SAVr;
        ADDHL = SAVADDHL;
        tab = TMP;
    }

    if ((ADDE != SAVADDE)&&(FUNCTIONUSE == 'r')) {
        ADDE = SAVADDE;
        r = SAVr;
        ADDHL = SAVADDHL;
        tab = TRAMEIN;
    }

    if ((ADDHL != SAVADDHL)&&(FUNCTIONUSE == 'r')) {
        r = SAVr;
        ADDHL = SAVADDHL;
        tab = TRAMEIN;
    }

    if ((ADDE != SAVADDE)&&(FUNCTIONUSE == 'T'))
        ADDE = SAVADDE;
    if ((ADDHL != SAVADDHL)&&(FUNCTIONUSE == 'T')) {
        r = SAVr;
        ADDHL = SAVADDHL;
        tab = TRAMEIN;
    }




    //MAXERIE A2 
    if (ADDE == 3)
        ADDE = 6;
    if (ADDE == 2)
        ADDE = 4;
    if (ADDE == 1)
        ADDE = 2;





    AddL = ADDHL;
    AddH = ADDHL >> 8;



    TMP2 = ADDE;
    TMP2 = TMP2 << 1;
    TMP2 = TMP2 | 0xA0;


    IdleI2C();
    StartI2C();
    //IdleI2C();	
    while (SSPCON2bits.SEN);

    WriteI2C(TMP2); // 0 sur le bit RW pour indiquer une lecture
    IdleI2C();




    WriteI2C(AddH); // 0 sur le bit RW pour indiquer une lecture
    IdleI2C();

    while (SSPCON2bits.RSEN);

    WriteI2C(AddL); // 0 sur le bit RW pour indiquer une lecture
    IdleI2C();


    RestartI2C();
    while (SSPCON2bits.RSEN);


    //AckI2C();
    TMP2 = TMP2 | 0x01;
    WriteI2C(TMP2); // 1 sur le bit RW pour indiquer une lecture
    IdleI2C();



    //tab[85]='*';
    if (r == 1) // pour trame memoire
        getsI2C(tab, 86);
    if (r == 0)
        getsI2C(tab, 108);
    if (r == 3)
        getsI2C(tab, 116);
    if (r == 4)
        getsI2C(tab, 24);


    NotAckI2C();
    while (SSPCON2bits.ACKEN);



    StopI2C();
    while (SSPCON2bits.PEN);
testEEW:
    return (1);
}

void ecrire_tab_presence_TPA(char p) //ECRIRE LETAT (au sens resistif presence....) DE LA VOIE ET DES CAPTEURS ICI C JUSTE POUR LA MESURE ADRESSABLE
{
    unsigned int l;
    char i;



    if (p == 1) {
        l = Tab_existance1[1];
        l = 128 * l + 10000;



        if (Memo_autorisee_1)
            ECRIRE_EEPROMBYTE(3, l, &Tab_existance1, 4);
    }


    if (p == 2) {
        l = Tab_existance2[1];
        l = 128 * l + 10000;


        if (Memo_autorisee_2)
            ECRIRE_EEPROMBYTE(3, l, &Tab_existance2, 4);
    }


    p = 0;
    delay_qms(10);
    LIRE_EEPROM(3, l, &TRAMEIN, 4); //LIRE DANS EEPROM
    p = 0;
}

void MODIFICATION_TABLE_DEXISTENCE(char *v, char eff, char etat) //eff pour effacer (eff=2 alors recopie) alors et etat ligne pour fusible HS ou CC ou surtension 
{

    unsigned int y, z;
    char fa, nbrcapteurs, jjj;

    nbrcapteurs = 0;
    v[0] = '#';
    y = 100 * (v[1] - 48) + 10 * (v[2] - 48)+(v[3] - 48); //VOIE

    y = 128 * y + 10000;

    LIRE_EEPROM(3, y, &TMP, 4); //LIRE DANS EEPROM

    z = 10 * (v[4] - 48)+(v[5] - 48); //CODAGE

    if (eff != 2) //RECOPIE
    {
        if (eff == 0) //CREATION
        {
            TMP[z + 3] = v[6]; //RECOPIE SI R OU A #00101R
            fa = TMP[z + 3];
        }

        if (eff == 1) //EFFACEMENT
        {

            // VOIR voie a 'v' qd il y a que des 'F' 
            for (jjj = 2; jjj < 20; jjj++) //VOIR COMBIEN EXISTENT
            {
                if (TMP[jjj] == 'A') {

                    delay_qms(1);
                    nbrcapteurs = nbrcapteurs + 1; // ON INCREMENTE
                }
            }
            TMP[z + 3] = 'F'; //CAPTEUR EFFACEE DE LA TABLE DEXISTENCE


            if (nbrcapteurs < 2) {
                TMP[2] = 'v';
                TMP[21] = 'v';
            }

        }
        TMP[2] = fa + 0x20; //r ou a
        TMP[21] = fa + 0x20;
    }

    if ((etat == 1) || (etat == 2)) {
        TMP[2] = 'h';
    }

    if ((((etat == 4) || (etat == 5))&&(TMP[2] != 'c')) || (etat == 8)) {
        TMP[2] = TMP[21];
    }

    if ((etat == 6) || (etat == 7)) {
        TMP[2] = 'c';
    }

    //ecrire_tab_presence_TPR;
    ECRIRE_EEPROMBYTE(3, y, &TMP, 4);

    delay_qms(10);
    LIRE_EEPROM(3, y, &TMP, 4); //LIRE DANS EEPROM pour verifier

    delay_qms(10);

}

unsigned char LIRE_HORLOGE(unsigned char zone) {
    unsigned char TMP;
    TMP = litHORLOGE_i2c(0xD1, zone);


    if (zone == 0)
        TMP = TMP & 0x7F; // supprimle le bit 7 seconde
    if (zone == 1)
        TMP = TMP & 0x7F; //laisse le bit7 a zero minutes
    if (zone == 2)
        TMP = TMP & 0x3F; //met a zero les bits 6 et 7 pour le mode 24h  
    if (zone == 3)
        TMP = TMP & 0x07; // laisse ts les bits sauf 0 1 2 a 0 
    if (zone == 4)
        TMP = TMP & 0x3F; // met a 0 les bits 7 et 6 date 
    if (zone == 5)
        TMP = TMP & 0x1F; // met a 0 les bits 7 et 6 et 5 mois
    if (zone == 6)
        TMP = TMP;

    return (TMP);
}

signed char litHORLOGE_i2c(unsigned char NumeroH, unsigned char AddH) {
    char Data;

    StartI2C();
    WriteI2C(NumeroH & 0xFE); // adresse de la DS1307
    WriteI2C(AddH); // suivant le choix utilisateur
    RestartI2C();
    WriteI2C(NumeroH); // on veut lire
    Data = ReadI2C(); // lit la valeur 
    StopI2C();
    return (Data);
}

char VOIR_TABLE_EXISTENCE_POUR_OEIL(char *v) {

    unsigned int y, z;
    char fa;

    v[0] = '#';

    y = 100 * (v[1] - 48) + 10 * (v[2] - 48)+(v[3] - 48); //VOIE

    y = 128 * y + 10000;

    LIRE_EEPROM(3, y, &TMP, 4); //LIRE DANS EEPROM
    TMP[2] = 'a';
    fa = TMP[2];




    return (fa); //Retourne si voie resistive ou adressable
}

void TRANSFORMER_TRAME_MEM_EN_COM(void) {

    int t;
    int u;
    u = 8;
    TRAMEOUT[0] = '#';

    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    //ecrire dans I2C

    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;





    t = 0;
    for (t = 0; t < 13; t++) {
        TRAMEOUT[u] = TRAMEIN[u - 7];
        u = u + 1;

    }

    t = 0;
    for (t = 0; t < 18; t++) {
        TRAMEOUT[u] = TRAMEIN[u - 7];
        u = u + 1;
    }



    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;




    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;








    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;




    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;

    TRAMEOUT[u] = '*';

    //ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEIN),calcul_addmemoire(&TRAMEIN),&TRAMEIN,1); //ECRITURE EN EEPROM
    //LIRE_EEPROM(choix_memoire(&TRAMEOUT),calcul_addmemoire(&TRAMEOUT),&TRAMEIN); //LIRE DANS EEPROM
    pointeur_out_in = u;


}

unsigned int calcul_CRC(char far *txt, unsigned char lg_txt) {
    unsigned char ii, nn;
    unsigned int crc;
    char CRC[5];

    char far *p;
    crc = CRC_START;
    p = txt;

    //essai[2]=*p;
    ii = 0;
    nn = 0;
    //for( ii=0; ii<lg_txt ; ii++, p++)
    do {
        crc ^= (unsigned int) *p;
        //for(; nn<8; nn++)
        do {
            if (crc & 0x8000) {
                crc <<= 1;
                //UTIL;
                crc ^= CRC_POLY;
            } else {
                crc <<= 1;
            }
            nn++;
        } while (nn < 8);
        ii = ii + 1;
        p++;
        if (nn == 8) {
            nn = 0;
        }
    } while (ii < lg_txt);
    ii = 0;


    IntToChar5(crc, CRC, 5);


    TRAMEOUT[pointeur_out_in] = CRC[0];
    TRAMEOUT[pointeur_out_in + 1] = CRC[1];
    TRAMEOUT[pointeur_out_in + 2] = CRC[2];
    TRAMEOUT[pointeur_out_in + 3] = CRC[3];
    TRAMEOUT[pointeur_out_in + 4] = CRC[4];
    TRAMEOUT[pointeur_out_in + 5] = '*';

    return crc;

}

void IntToChar5(unsigned int value, char *chaine, char Precision) {
    unsigned int Mil, Cent, Diz, Unit, DMil;
    int count = 0;

    // initialisation des variables
    Mil = 0;
    Cent = 0;
    Diz = 0;
    Unit = 0;
    DMil = 0;

    // si la valeur n'est pas nulle
    if (value != 0) {
        if (Precision >= 5) // si l'utilisateur d�sire les milliers
        {
            // conversion des milliers
            DMil = value / 10000;
            if (DMil != 0) {
                *(chaine + count) = DMil + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }
        if (Precision >= 4) // si l'utilisateur d�sire les milliers
        {
            // conversion des milliers
            Mil = value - (DMil * 10000);
            Mil = Mil / 1000;
            if (Mil != 0) {
                *(chaine + count) = Mil + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        if (Precision >= 3) // si l'utilisateur d�sire les centaines
        {
            // conversion des centaines
            Cent = value - (Mil * 1000)-(DMil * 10000);
            Cent = Cent / 100;
            if (Cent != 0) {
                *(chaine + count) = Cent + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        if (Precision >= 2) // si l'utilisateur d�sire les dizaines
        {
            // conversion des dizaines
            Diz = value - (Mil * 1000) - (Cent * 100)-(DMil * 10000);
            Diz = Diz / 10;
            if (Diz != 0) {
                *(chaine + count) = Diz + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        // conversion unit�s
        Unit = value - (Mil * 1000) - (Cent * 100) - (Diz * 10)-(DMil * 10000);
        *(chaine + count) = Unit + 48;
        if (*(chaine + count) < 48) // limites : 0 et 9
            *(chaine + count) = 48;
        if (*(chaine + count) > 57)
            *(chaine + count) = 57;
    } else // if (value == 0)
    {
        //*(chaine) = 32; // ecrit un espace devant le nombre
        for (Mil = 0; Mil < Precision; Mil++) // inscription de '0' dans toute la chaine
            *(chaine + Mil) = 48;
    }

} // fin de la fonction de conversion INT => CHAR

void TRAMEENVOIRS485DONNEES(void) {
    unsigned char u;
    unsigned char tmp;
    tmp = 0;



    do {

        u = TRAMEOUT[tmp];
        SAVcaracOEIL = u;
        RS485(u);


        LEDTX = !LEDTX;
        tmp = tmp + 1;
    } while ((u != '*') && (tmp < 200));


    //delay_qms(100);

}

void RS485(unsigned char carac) {
    //carac = code(carac);
    if ((carac != SAVcaracOEIL)&&(FUNCTIONUSE == 'O'))
        carac = SAVcaracOEIL;

    if ((carac != SAVcaracOEIL)&&(FUNCTIONUSE == 'E')) //ERREUR TPR SCRUTATION
        carac = SAVcaracOEIL;

    RX_TX = 1; //AUTORISATION TRANSMETTRE


    TXSTA1bits.TXEN = 0;

    RCSTA1bits.CREN = 0; // interdire reception

    TXSTA1bits.TXEN = 1; // autoriser transmission


    TXREG1 = carac;

    //__no_operation();
    //__no_operation();
    //__no_operation();
    //__no_operation();
    while (TXSTA1bits.TRMT == 0); // attend la fin de l'�mission
    //     __no_operation();
    TXSTA1bits.TXEN = 0;

    RX_TX = 0; //AUTORISATION TRANSMETTRE
    //delayms(100);
}

void Init_I2C(void) {

    //here is the I2C setup from the Seeval32 code.
    DDRCbits.RC3 = 1; //Configure SCL as Input
    DDRCbits.RC4 = 1; //Configure SDA as Input
    SSP1STAT = 0x00; //Disable SMBus & Slew Rate Control 80
    SSP1CON1 = 0x28; //Enable MSSP Master  28
    SSP1CON2 = 0x00; //Clear MSSP Conr// 0x3B for 400kHz  (24mhz/((4*4OOkHz))-1 = 14 ou 3B en hexol Bits
    SSP1ADD = 0x3B; //0x0E
}

void Init_RS485(void) {



    TRISCbits.TRISC5 = 0;
    TRISCbits.TRISC7 = 1;
    TRISCbits.TRISC6 = 0;
    TRISCbits.TRISC5 = 0; //RC5 en sortie	

    TRISAbits.TRISA4 = 0; //RA4 RX_TX en sortie OU DERE



    RCSTA1bits.SPEN = 1; // Enable serial port
    TXSTA1bits.SYNC = 0; // Async mode	
    BAUDCON1bits.BRG16 = 1;

    TXSTA1bits.BRGH = 1; //haute vitesse    
    // set 9600 bauds
    SPBRG1 = SPEED; // Write baudrate to SPBRG1
    SPBRGH1 = SPEED >> 8; // For 16-bit baud rate generation





    IPR1bits.RCIP = 1; // Set high priority
    PIR1bits.RCIF = 0; // met le drapeau d'IT � 0 (plus d'IT)
    PIE1bits.RCIE = 0; // ne g�n�re pas d'IT pour la liaison s�rie

    TXSTA1bits.TXEN = 0; // transmission inhib�e
    RCSTA1bits.RX9 = 0; // r�ception sur 8 bits
    TXSTA1bits.TX9 = 0; // transmission sur 8 bits
    RCSTA1bits.CREN = 0; // interdire reception
    RCONbits.IPEN = 1; // Enable priority levels 

}

unsigned char ECRIRE_HORLOGE(unsigned char zone, unsigned char Time) {
    unsigned char TMP;

    if (zone == 0)
        zone = zone & 0x7F; //laisse l'horloge CHbits a 0 secondes tres important si ce bit n'est pas a 0 lhorloge ne marche pas
    if (zone == 1)
        zone = zone & 0x7F; //laisse le bit7 a zero minutes
    if (zone == 2)
        zone = zone & 0x3F; //met a zero les bits 6 et 7 pour le mode 24h  
    if (zone == 3)
        zone = zone & 0x07; // laisse ts les bits sauf 0 1 2 a 0 
    if (zone == 4)
        zone = zone & 0x3F; // met a 0 les bits 7 et 6 date 
    if (zone == 5)
        zone = zone & 0x1F; // met a 0 les bits 7 et 6 et 5 mois
    if (zone == 6)
        zone = zone;
    if (zone == 7)
        zone = zone; //registre control ex: 00010000 =) oscillation de 1hz out





    TMP = envoyerHORLOGE_i2c(0xD0, zone, Time);
    return (TMP);
}

unsigned char envoyerHORLOGE_i2c(unsigned char NumeroH, unsigned char ADDH, unsigned char dataH0) {

    StartI2C();
    WriteI2C(NumeroH); // adresse de l'horloge temps r�el
    WriteI2C(ADDH);
    WriteI2C(dataH0);
    StopI2C();
    delay_qms(11);

}

void init_uc(void) {
    // 1=INPUT    0=OUTPUT
    // ENTREES ANALOGIQUES  AUTRES 
    TRISAbits.TRISA0 = 1; // AN0   V_ligne_AM1 	mesure tension Amont du capteur voies impaires
    TRISAbits.TRISA1 = 1; // AN1	 V_ligne_AM2 	mesure tension Amont du capteur voies paires
    TRISAbits.TRISA2 = 1; // AN2	 V_ligne_AV1_A 	mesure tension Aval du capteur voies impaires  (apr�s fusible)
    TRISAbits.TRISA3 = 1; // AN3	 V_ligne_AV2_A 	mesure tension Aval du capteur voies paires  (apr�s fusible)
    TRISAbits.TRISA4 = 0; // RX_TX	
    TRISAbits.TRISA5 = 1; // AN4	 I_conso_1		mesure tension(courant de consommation) du capteur voies impaires
    TRISAbits.TRISA6 = 0; //	OSC
    TRISAbits.TRISA7 = 0; //	OSC

    TRISBbits.TRISB0 = 1; //	ALIM_OFF (INT)
    TRISBbits.TRISB1 = 1; //	VAL_RS485_EXT
    TRISBbits.TRISB2 = 1; //	FREQ_TPA
    TRISBbits.TRISB3 = 1; //	HORLOGE
    TRISBbits.TRISB4 = 1; //	VAL_I2C_EXT
    TRISBbits.TRISB5 = 0; //	PGM	}
    TRISBbits.TRISB6 = 0; //	PGC	]> ICSP
    TRISBbits.TRISB7 = 0; //	PGD	}

    TRISCbits.TRISC0 = 0; //	DECH_L1			==> modif piste sur CI
    TRISCbits.TRISC1 = 0; //	OK_I2C_EXT
    TRISCbits.TRISC2 = 0; //	I2C_INTERNE
    TRISCbits.TRISC3 = 1; //	SCL_I2C
    TRISCbits.TRISC4 = 1; //	SDA_I2C
    //	TRISCbits.TRISC5 = 0;	//	OK_RS485_EXT
    //	TRISCbits.TRISC6 = 0;	//	TX_485
    //	TRISCbits.TRISC7 = 1;	//	RX_485

    TRISDbits.TRISD0 = 0; //	D0	
    TRISDbits.TRISD1 = 0; //	D1
    TRISDbits.TRISD2 = 0; //	D2
    TRISDbits.TRISD3 = 0; //	D3
    TRISDbits.TRISD4 = 0; //	D4
    TRISDbits.TRISD5 = 0; //	D5
    TRISDbits.TRISD6 = 0; //	D6
    TRISDbits.TRISD7 = 0; //	D7
    // 1=INPUT    0=OUTPUT
    TRISEbits.TRISE0 = 0; //	RS_DI	
    TRISEbits.TRISE1 = 0; //	R/W	
    TRISEbits.TRISE2 = 0; //	E			
    TRISEbits.TRISE3 = 0; //	Data_Rel
    TRISEbits.TRISE4 = 0; //	Clk_Rel
    TRISEbits.TRISE5 = 0; //	Rck_Rel
    TRISEbits.TRISE6 = 0; //	Rst_Rel
    TRISEbits.TRISE7 = 0; //	Test_Fus


    TRISFbits.TRISF0 = 1; // AN5   I_conso_2		mesure tension(courant de consommation) du capteur voies paires 
    TRISFbits.TRISF1 = 1; // AN6	 I_modul_1		mesure tension(courant de modulation) du capteur voies impaires
    TRISFbits.TRISF2 = 1; // AN7	 I_modul_2		mesure tension(courant de modulation) du capteur voies paires
    TRISFbits.TRISF3 = 1; // AN8   Sel A0 Lecture en entr�e Analogique!!!
    TRISFbits.TRISF4 = 1; // AN9	 Sel A1 Lecture en entr�e Analogique!!!
    TRISFbits.TRISF5 = 1; // AN10  Sel A2 Lecture en entr�e Analogique!!!
    TRISFbits.TRISF6 = 0; // DECH_L1 AN11	 <===  entr�e analogique LIBRE   ==> modif piste sur CI
    TRISFbits.TRISF7 = 0; // DECH_L2

    TRISGbits.TRISG0 = 0; //	Sel_Freq	(0 voie impaire)(1 voie paire)
    TRISGbits.TRISG1 = 0; //	CPT_ON_1
    TRISGbits.TRISG2 = 0; //	UN_CPT_1			
    TRISGbits.TRISG3 = 0; //	CPT_ON_2
    TRISGbits.TRISG4 = 0; //	UN_CPT_2
    //	(TRISGbits.TRISG5 = 1;	//	/MCLR)

    TRISHbits.TRISH0 = 0; //  AL_CABLE_ON + LED ALARME
    TRISHbits.TRISH1 = 0; //  /RST
    TRISHbits.TRISH2 = 0; //	LED_TX
    TRISHbits.TRISH3 = 0; //	LED_RX
    TRISHbits.TRISH4 = 1; // AN12  V_48V_1 mesure tension 48V voies impaires  
    TRISHbits.TRISH5 = 1; // AN13	 V_48V_2 mesure tension 48V voies paires 
    TRISHbits.TRISH6 = 1; // AN14  V_ligne_AV1_B 	mesure gain+ tension Aval du capteur voies impaires  (apr�s fusible)
    TRISHbits.TRISH7 = 1; // AN15  V_ligne_AV2_B 	mesure gain+ tension Aval du capteur voies paires  (apr�s fusible)

    TRISJbits.TRISJ0 = 0; //	CS1
    TRISJbits.TRISJ1 = 0; //	CS2
    TRISJbits.TRISJ2 = 0; //	Pt2
    TRISJbits.TRISJ3 = 1; // 	Jumper (entr�e pour TPR)
    TRISJbits.TRISJ4 = 1; //	MODE TEST	
    TRISJbits.TRISJ5 = 0; //	Val_R_I2C
    TRISJbits.TRISJ6 = 0; //	V_SUP_1
    TRISJbits.TRISJ7 = 0; //	V_SUP_2




    // A/D PORT Configuration
    ADCON1 = 0b00000111; // ANALOG: AN0 -> AN7 - (Vref+ = AVDD(5v) et Vref =AVSS(0v) ) 

    ADCON0 = 0; // CAN AU REPOS

    //	ADCON1bits.VCFG1 = 0 ;	// VDD
    //	ADCON1bits.VCFG0 = 0 ;	// VSS

    //	ADCON1bits.PCFG3 = 1 ;	// SELECTION DES ENTREES ANALOGIQUES
    //	ADCON1bits.PCFG2 = 0 ;	// AN0 a AN6 INCLUS     
    //	ADCON1bits.PCFG1 = 0 ;	
    //	ADCON1bits.PCFG0 = 0 ;	

    ADCON2bits.ADFM = 1; // FORMAT DU RESULTAT JUSTIFI� A DROITE
    ADCON2bits.ACQT2 = 1; // ACQUISITION = 12 TAD
    ADCON2bits.ACQT1 = 0;
    ADCON2bits.ACQT0 = 1;

    ADCON2bits.ADCS2 = 0; // HORLOGE TAD 1�S POUR F = 32MHz
    ADCON2bits.ADCS1 = 1; // % 32
    ADCON2bits.ADCS0 = 0;
}