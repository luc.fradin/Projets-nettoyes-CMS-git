/* 
 * File:   RecupCapteur.h
 * Author: LFRAD
 *
 * Created on 2 juillet 2021, 14:18
 */

#ifndef RECUPCAPTEUR_H
#define	RECUPCAPTEUR_H

#define		LEDRX		PORTHbits.RH3	//  sortie LED RX	OK_RI
#define		HORLOGE		PORTBbits.RB3 	//	Signal carr� 1Hz de l'horloge	OK_RI
#define		CPT_ON_1	LATGbits.LATG1 		// Valid tension sur capteur voies impaires		ok_ri
#define		UN_CPT_1 	LATGbits.LATG2		// choix valeur tension capteur voies impaires		ok_ri				
#define		CPT_ON_2	PORTGbits.RG3 		// Valid tension sur capteur voies paires		ok_ri
#define		UN_CPT_2	LATGbits.LATG4 		// choix valeur tension capteur voies paires		ok_ri
#define		DECH_L1		PORTFbits.RF6		// D�charge ligne capteur voies impaires	ok_ri		
#define		DECH_L2		PORTFbits.RF7		// D�charge ligne capteur voies paires		ok_ri
#define		V_SUP1 	LATJbits.LATJ6 		// Choix valeur tension capteur voies impaires		ok_ri
#define		V_SUP2 	PORTJbits.RJ7		// Choix valeur tension capteur voies paires		ok_ri
#define PARAM_SCLASS auto

#define i_conso_min 	20 		// = 2.0mA =>seuil mini de d�tection d'un courant capteur (i_conso est x10) (20)
#define i_repos_min 	150 	// = 150�A =>seuil pout indiquer qu'un seul capteur sur la ligne      A VOIR!!!!!!!!!!!
#define def_i_conso 	80 		// defaut si > 8.0 mA
#define def_i_modul  	40 		// defaut si > 4.0 mA
#define def_i_repos  	150 	// defaut si > 150 �A
#define def_pression_B  800 	// defaut si < 800 mbar
#define def_pression_H  1800 	// defaut si > 1800 mbar

#define Icons_cc 100 //cc DOUBLE DECLARATION 


#define V_Ligne_Max	5	// tension max acceptable sur la ligne d'interrogation capteurs
#define max_instable 3  // valeur max d'�cart fr�quence (Hz) pour signaler une instabilit� du capteur


#define		Sel_Freq		LATGbits.LATG0 		//sortie s�lection voie fr�quence capteur � mesure (0 voie impaire)(1 voie paire)OK_RI

#define AN0		0b00000000			// V_ligne_AM1
#define AN1		0b00000100			// V_ligne_AM2
#define IRepos1_AN2		0b00001000			// V_ligne_AV1_A (courant de repos sur TPA  et tension sur TPR => voies impaires )
#define IRepos2_AN3 	0b00001100			// V_ligne_AV2_A (courant de repos sur TPA  et tension sur TPR => voies paires )
#define IConso1_AN4		0b00010000			// I_conso_1
#define IConso2_AN5     0b00010100			// I_conso_2
#define IModul1_AN6		0b00011000			// I_modul_1
#define IModul2_AN7		0b00011100			// I_modul_2
#define AN8		0b00100000			// SEL 0   	lecture de l'entr�e en analogique!
#define AN9		0b00100100			// SEL 1	lecture de l'entr�e en analogique!
#define AN10	0b00101000			// SEL 2	lecture de l'entr�e en analogique!
#define AN11	0b00101100			// <===  entr�e analogique LIBRE
#define AN12	0b00110000			// V_48V_1 
#define AN13	0b00110100			// V_48V_2 
#define AN14	0b00111000			// V_ligne_AV1_B  
#define AN15	0b00111100			// V_ligne_AV2_B

#define TRIS_Data_HC595    TRISEbits.TRISE3 //data     
#define TRIS_Clk_HC595     TRISEbits.TRISE4 //clock   
#define TRIS_Rck_HC595     TRISEbits.TRISE5 //    
#define TRIS_Rst_HC595     TRISEbits.TRISE6 //reset    
#define TRIS_OE_HC595      TRISEbits.TRISE7 //output enable     

#define Data_HC595 		LATEbits.LATE3 
#define Clk_HC595 	 LATEbits.LATE4
#define Rck_HC595 		LATEbits.LATE5 
#define Rst_HC595   	LATEbits.LATE6 
#define OE_HC595   		LATEbits.LATE7 
#define CRC_POLY 0x01021 //0x01021
#define CRC_START 0xFFFF
#define		LEDTX		PORTHbits.RH2 	//  sortie LED TX	OK_RI
#define		RX_TX		LATAbits.LATA4 	//  sortie RX_TX	OK_RI
#define DERE PORTAbits.RA4 // direction RX TX

#define DX2 PORTHbits.RH3
#define		Val_R_I2C		PORTJbits.RJ5 		//sortie validation r�sistance de tirage � vdd,sur bus I2C	OK_RI
#define SPEED 624 // 624 set 9600 bauds 311 pour 19230bauds  208 pour 28800 Bauds 38 pour 38400 vitesse moyenne 51 115200 grande vitesse   156 38400
#define ENVOIE TXSTA1bits.TXEN 
#define RECEPTION RCSTA1bits.CREN
#define FINTRANS TXSTA1bits.TRMT
#define FINRECEPTION PIR1bits.RCIF

#define _ADIF	PIR1bits.ADIF	// INTERRUPTION FIN CONVERSION ANALOGIQUE
#define _ADON	ADCON0bits.ADON	// VALIDE LE CAN
#define _ADGO	ADCON0bits.GO	// LANCE ACQUISITION ET CONVERSION
#define _ADDONE	ADCON0bits.DONE	// TEST SI ACQUISITION ET CONVERSION EN COURS
#define abs(x) ((x) > 0 ? (x) : (-x))


#define Coeff_I_Conso1 458 
#define Coeff_I_Conso2 458
#define Coeff_I_Modul1 294 
#define Coeff_I_Modul2 294

#define Coeff_I_Repos1 157
#define Coeff_I_Repos2 157

void EnvoieTrame(unsigned char *cTableau, char cLongueur);
unsigned char RecoitTrame(unsigned char *cTableau, char cLongueur);

void init_uc(void);
void Init_I2C(void);
void Init_RS485(void);
void Init_Tab_tpa(void);

unsigned int Mesure_I_Conso(char cMesure, unsigned int Coeff);
unsigned int Mesure_I_Modul(char cMesure, unsigned int Coeff);
unsigned int Mesure_I_Repos(char cMesure, unsigned int Coeff);
unsigned char Instable(signed int* iFreq);
unsigned int Mesure_Freq(void);
signed int CalculFreq(char cSelecFreq);

unsigned int acqui_ana(unsigned char adcon);
unsigned int acqui_ana_16_plus(unsigned char adcon);
void Delay10KTCYx(PARAM_SCLASS unsigned char);
void DELAY_SW_250MS(void);
void DELAY_MS(int ms);

signed char EESequRead(unsigned char cControl, unsigned int address, unsigned char *rdptr, unsigned char length);
signed char EEPaWrite(unsigned char cControl, unsigned int address, unsigned char *wrptr);

void Calcul_Deb(unsigned int* deb_temp);

void IntToChar5(unsigned int value, unsigned char *chaine, char Precision);
unsigned char envoyerHORLOGE_i2c(unsigned char NumeroH, unsigned char ADDH, unsigned char dataH0);

unsigned int calcul_addmemoire(unsigned char cVoie, unsigned char cCapteur);
unsigned char choix_memoire(unsigned char cVoie);

void TRANSFORMER_TRAME_MEM_EN_COM(unsigned char* cEntree, unsigned char* cSortie);
unsigned int calcul_CRC(unsigned char far *txt, unsigned char lg_txt);
void MODIFICATION_TABLE_DEXISTENCE(unsigned char *v, char eff, char etat);

void mesure_test_cc(unsigned char cImpaire, unsigned char cPaire, unsigned char* cSortie);

void Prog_HC595(unsigned char rel_voies_impaires, unsigned char rel_voies_paires, unsigned char alarme_ca, unsigned char test_fus);
void ecriture_trame_i2c(unsigned char cVoie, unsigned int* iModul, unsigned int* iConso, unsigned int* iRepos, unsigned char* cEtat, unsigned int* iPression, unsigned int* iSeuil, unsigned char* cEntree, unsigned char* cSortie);
void RECUPERE_TRAME_ET_CAL_ETAT(unsigned char* cEtat, unsigned int* iPression, unsigned int* iSeuil, char cCapteur, char cTypeCarte);
void TableExistence(unsigned char cVoie, unsigned char* cSortie);
void EcritureTrameCapteurOeil(char cVoie, char cCapteur, unsigned int* iModul, unsigned int* iConso, unsigned int* iRepos, unsigned char* cEtat, unsigned int* iPression, unsigned int* iSeuil, unsigned char* cTrame);
void EcritureTrameCapteur(char cVoie, char cCapteur, unsigned char* cEtat, unsigned int* iPression, unsigned int* iSeuil, unsigned char* cTrame);
void EcritureTrameVoie(char cVoie, unsigned char* cEtat, unsigned int* iPression, unsigned int* iSeuil, unsigned char* cTrame);
void Mesure_DEB_TPA(char cCapteurmax, char cVoieImpaire, char cVoiePaire, unsigned char cAlarme, char cFusibleHS, unsigned char* cTrameSortie);

//void MesureCapteurs(char cCapteurMax, char cVoieImpaire, char cVoiePaire, unsigned char* cTrame);

//const unsigned int Coeff_I_Conso1 = 458; // pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53
//const unsigned int Coeff_I_Conso2 = 458;
//const unsigned int Coeff_I_Modul1 = 294; // pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53
//const unsigned int Coeff_I_Modul2 = 294;
//
//const unsigned int Coeff_I_Repos1 = 157;
//const unsigned int Coeff_I_Repos2 = 157;


#endif	/* RECUPCAPTEUR_H */

