/* 
 * File:   FonctionsScrutation.c
 * Author: LFRAD
 *
 * Created on 23 ao�t 2021, 08:11
 */

#include <p18f8723.h>
#include <stdio.h>
#include <stdlib.h>
#include "RecupCapteur.h"
#include <i2c.h>

#pragma udata udata1

const double q = 0.001220703125;

unsigned int Timer1L; // pour sauvegarder les valeurs � l'interruption de INT2 (utilis� dans la mesure de fr�quence)
unsigned int Timer1H;
unsigned int t1ov_cnt; //	compteur de d�bordement du timer1  (de 65535 � 0000)
unsigned int cpt_int2;


char COURTCIRCUIT;

unsigned int Tab_I_Repos_1[17];
unsigned int Tab_I_Repos_2[17];



#pragma udata udata2

unsigned int Tab_Pression_1[17];
unsigned int Tab_Pression_2[17];
unsigned int Tab_I_Conso_1[17];
unsigned int Tab_I_Conso_2[17];
unsigned char Tab_Etat_1[17];
unsigned char Tab_Etat_2[17];
unsigned int Tab_I_Modul_1[17];
unsigned int Tab_I_Modul_2[17];

void EnvoieTrame(unsigned char *cTableau, char cLongueur) { //Envoie trame au RS485
    int iBcl;   //Variable de boucle
    ENVOIE = 1; // autoriser transmission
    for (iBcl = 0; iBcl < cLongueur; iBcl++) {      //Boucle allant du premier caract�re � envoyer au dernier
        TXREG1 = *(cTableau + iBcl);        //Envoie du caract�re point�
        while (FINTRANS == 0); //Attente fin transmission  
    }
    ENVOIE = 0; //Fin de transmission
}

unsigned char RecoitTrame(unsigned char *cTableau, char cLongueur) {    //Recoit trame RS485
    int iBcl;   //Variable de boucle
    unsigned char cErreur;  //Variable d'erreur � renvoyer
    unsigned int iWatchDog; // Chien de garde
    iWatchDog = 0;
    cErreur = 'a';  //Caract�re informant qu'il n'y a pas d'erreur
    RECEPTION = 1; // D�but reception
    for (iBcl = 0; iBcl < cLongueur; iBcl++) {  //Boucle allant du premier caract�re � recevoir au dernier
        if (iWatchDog < 20000) {    //Si le watchDog n'est pas d�pass�
            iWatchDog = 0;  //R�initialisation WatchDog
            while ((FINRECEPTION == 0) && (iWatchDog < 20000)) {    //Tant qu'aucune r�ception et WatchDog non d�pass�
                iWatchDog++;    //Incr�mentation du WatchDog
            }
            *(cTableau + iBcl) = RCREG1;    //Lecture du caract�re re�u
            if (RCSTAbits.OERR || iWatchDog == 20000) { //Si Erreur OERR ou WatchDog d�pass�
                RECEPTION = 0;
                RECEPTION = 1;  //R�initialisation r�ception
                cErreur = 'o';  //Caract�re d'erreur OERR
                *(cTableau + iBcl) = 0; //Valeur d'erreur
            } else if (RCSTAbits.FERR) {    //Si erreur FERR
                cErreur = 'f';  //Caract�re d'erreur FERR
            }
        } else {    //Si WatchDog d�pass�
            RCREG1; //Vidage du tampon de reception
            *(cTableau + iBcl) = 0; //Valeur d'erreur
        }
    }
    RECEPTION = 0;  //Fin de Reception
    return cErreur; //Renvoie de l'erreur
}

void init_uc(void) {
    TRISAbits.TRISA0 = 1; // AN0   V_ligne_AM1 	mesure tension Amont du capteur voies impaires
    TRISAbits.TRISA1 = 1; // AN1	 V_ligne_AM2 	mesure tension Amont du capteur voies paires
    TRISAbits.TRISA2 = 1; // AN2	 V_ligne_AV1_A 	mesure tension Aval du capteur voies impaires  (apr�s fusible)
    TRISAbits.TRISA3 = 1; // AN3	 V_ligne_AV2_A 	mesure tension Aval du capteur voies paires  (apr�s fusible)
    TRISAbits.TRISA4 = 0; // RX_TX	
    TRISAbits.TRISA5 = 1; // AN4	 I_conso_1		mesure tension(courant de consommation) du capteur voies impaires
    TRISAbits.TRISA6 = 0; //	OSC
    TRISAbits.TRISA7 = 0; //	OSC

    TRISBbits.TRISB0 = 1; //	ALIM_OFF (INT)
    TRISBbits.TRISB1 = 1; //	VAL_RS485_EXT
    TRISBbits.TRISB2 = 1; //	FREQ_TPA
    TRISBbits.TRISB3 = 1; //	HORLOGE
    TRISBbits.TRISB4 = 1; //	VAL_I2C_EXT
    TRISBbits.TRISB5 = 0; //	PGM	}
    TRISBbits.TRISB6 = 0; //	PGC	]> ICSP
    TRISBbits.TRISB7 = 0; //	PGD	}

    TRISCbits.TRISC0 = 0; //	DECH_L1			==> modif piste sur CI
    TRISCbits.TRISC1 = 0; //	OK_I2C_EXT
    TRISCbits.TRISC2 = 0; //	I2C_INTERNE
    TRISCbits.TRISC3 = 1; //	SCL_I2C
    TRISCbits.TRISC4 = 1; //	SDA_I2C
    TRISCbits.TRISC5 = 0; //	OK_RS485_EXT
    TRISCbits.TRISC6 = 0; //	TX_485
    TRISCbits.TRISC7 = 1; //	RX_485

    TRISDbits.TRISD0 = 0; //	D0	
    TRISDbits.TRISD1 = 0; //	D1
    TRISDbits.TRISD2 = 0; //	D2
    TRISDbits.TRISD3 = 0; //	D3
    TRISDbits.TRISD4 = 0; //	D4
    TRISDbits.TRISD5 = 0; //	D5
    TRISDbits.TRISD6 = 0; //	D6
    TRISDbits.TRISD7 = 0; //	D7
    // 1=INPUT    0=OUTPUT
    TRISEbits.TRISE0 = 0; //	RS_DI	
    TRISEbits.TRISE1 = 0; //	R/W	
    TRISEbits.TRISE2 = 0; //	E			
    TRISEbits.TRISE3 = 0; //	Data_Rel
    TRISEbits.TRISE4 = 0; //	Clk_Rel
    TRISEbits.TRISE5 = 0; //	Rck_Rel
    TRISEbits.TRISE6 = 0; //	Rst_Rel
    TRISEbits.TRISE7 = 0; //	Test_Fus


    TRISFbits.TRISF0 = 1; // AN5   I_conso_2		mesure tension(courant de consommation) du capteur voies paires 
    TRISFbits.TRISF1 = 1; // AN6	 I_modul_1		mesure tension(courant de modulation) du capteur voies impaires
    TRISFbits.TRISF2 = 1; // AN7	 I_modul_2		mesure tension(courant de modulation) du capteur voies paires
    TRISFbits.TRISF3 = 1; // AN8   Sel A0 Lecture en entr�e Analogique!!!
    TRISFbits.TRISF4 = 1; // AN9	 Sel A1 Lecture en entr�e Analogique!!!
    TRISFbits.TRISF5 = 1; // AN10  Sel A2 Lecture en entr�e Analogique!!!
    TRISFbits.TRISF6 = 0; // DECH_L1 AN11	 <===  entr�e analogique LIBRE   ==> modif piste sur CI
    TRISFbits.TRISF7 = 0; // DECH_L2

    TRISGbits.TRISG0 = 0; //	Sel_Freq	(0 voie impaire)(1 voie paire)
    TRISGbits.TRISG1 = 0; //	CPT_ON_1
    TRISGbits.TRISG2 = 0; //	UN_CPT_1			
    TRISGbits.TRISG3 = 0; //	CPT_ON_2
    TRISGbits.TRISG4 = 0; //	UN_CPT_2

    TRISHbits.TRISH0 = 0; //  AL_CABLE_ON + LED ALARME
    TRISHbits.TRISH1 = 0; //  /RST
    TRISHbits.TRISH2 = 0; //	LED_TX
    TRISHbits.TRISH3 = 0; //	LED_RX
    TRISHbits.TRISH4 = 1; // AN12  V_48V_1 mesure tension 48V voies impaires  
    TRISHbits.TRISH5 = 1; // AN13	 V_48V_2 mesure tension 48V voies paires 
    TRISHbits.TRISH6 = 1; // AN14  V_ligne_AV1_B 	mesure gain+ tension Aval du capteur voies impaires  (apr�s fusible)
    TRISHbits.TRISH7 = 1; // AN15  V_ligne_AV2_B 	mesure gain+ tension Aval du capteur voies paires  (apr�s fusible)

    TRISJbits.TRISJ0 = 0; //	CS1
    TRISJbits.TRISJ1 = 0; //	CS2
    TRISJbits.TRISJ2 = 0; //	Pt2
    TRISJbits.TRISJ3 = 1; // 	Jumper (entr�e pour TPR)
    TRISJbits.TRISJ4 = 1; //	MODE TEST	
    TRISJbits.TRISJ5 = 0; //	Val_R_I2C
    TRISJbits.TRISJ6 = 0; //	V_SUP_1
    TRISJbits.TRISJ7 = 0; //	V_SUP_2

    // A/D PORT Configuration
    ADCON1 = 0b00000111; // ANALOG: AN0 -> AN7 - (Vref+ = AVDD(5v) et Vref =AVSS(0v) ) 

    ADCON0 = 0; // CAN AU REPOS

    ADCON2bits.ADFM = 1; // FORMAT DU RESULTAT JUSTIFI� A DROITE
    ADCON2bits.ACQT2 = 1; // ACQUISITION = 12 TAD
    ADCON2bits.ACQT1 = 0;
    ADCON2bits.ACQT0 = 1;

    ADCON2bits.ADCS2 = 0; // HORLOGE TAD 1�S POUR F = 32MHz
    ADCON2bits.ADCS1 = 1; // % 32
    ADCON2bits.ADCS0 = 0;

    INTCONbits.PEIE = 0;
    INTCON2bits.RBIP = 0; // interrupt Hight priorit� sur port B
    INTCONbits.RBIE = 0; //valide  interrupt sur port B
    // pour d�tecter coupure alim
    INTCONbits.TMR0IE = 0; // 0 = Disables the TMR0 overflow interrupt    
    INTCONbits.INT0IE = 1; // INT0 External Interrupt Enable bit 1 = Enables the INT0 external interrupt 
    INTCON2bits.INTEDG0 = 0; //External Interrupt 0 Edge Select bit 1 = Interrupt on rising edge 0 = Interrupt on falling edge bit 
    //
    INTCON2bits.INTEDG1 = 1; //pour interruption d�tection demande RS485 sur front montant  (RB1/INT1)

    INTCON3bits.INT1IF = 0; // reinit flag RS485 pour le premier demmarage on autorise pas la com

    INTCON3bits.INT1IE = 1; // valide l'interruption pour RS485 1 pr reactiver

    INTCONbits.GIE = 1;
    
    TRISHbits.RH0 = 1;
}

void Init_I2C(void) {

    //here is the I2C setup from the Seeval32 code.
    DDRCbits.RC3 = 1; //Configure SCL as Input
    DDRCbits.RC4 = 1; //Configure SDA as Input
    SSP1STAT = 0x00; //Disable SMBus & Slew Rate Control 80
    SSP1CON1 = 0x28; //Enable MSSP Master  28
    SSP1CON2 = 0x00; //Clear MSSP Conr// 0x3B for 400kHz  (24mhz/((4*4OOkHz))-1 = 14 ou 3B en hexol Bits
    SSP1ADD = 0x3B; //0x0E
}

void Init_RS485(void) {

    TRISCbits.TRISC5 = 0;
    TRISCbits.TRISC7 = 1;
    TRISCbits.TRISC6 = 0;
    TRISCbits.TRISC5 = 0; //RC5 en sortie	

    TRISAbits.TRISA4 = 0; //RA4 RX_TX en sortie OU DERE

    RCSTA1bits.SPEN = 1; // Enable serial port
    TXSTA1bits.SYNC = 0; // Async mode	
    BAUDCON1bits.BRG16 = 1;

    TXSTA1bits.BRGH = 1; //haute vitesse    
    // set 9600 bauds
    SPBRG1 = SPEED & 0xFF; // Write baudrate to SPBRG1
    SPBRGH1 = SPEED >> 8; // For 16-bit baud rate generation

    IPR1bits.RCIP = 1; // Set high priority
    PIR1bits.RCIF = 0; // met le drapeau d'IT � 0 (plus d'IT)
    PIE1bits.RCIE = 0; // ne g�n�re pas d'IT pour la liaison s�rie

    TXSTA1bits.TXEN = 0; // transmission inhib�e
    RCSTA1bits.RX9 = 0; // r�ception sur 8 bits
    TXSTA1bits.TX9 = 0; // transmission sur 8 bits
    RCSTA1bits.CREN = 0; // interdire reception
    RCONbits.IPEN = 1; // Enable priority levels 

}

void Init_Tab_tpa(void) { //reset tableau de mesures des tpa
    int iBcl;
    for (iBcl = 0; iBcl < 17; iBcl++) {
        Tab_Pression_1[iBcl] = 0; //17 caract�res	
        Tab_I_Repos_1[iBcl] = 0; //17 caract�res	  
        Tab_I_Modul_1[iBcl] = 0; //17 caract�res		
        Tab_I_Conso_1[iBcl] = 0; //34 caract�res	
        Tab_Etat_1[iBcl] = 0; //17 caract�res	

        Tab_Pression_2[iBcl] = 0; //17 caract�res	
        Tab_I_Repos_2[iBcl] = 0; //17 caract�res	  
        Tab_I_Modul_2[iBcl] = 0; //17 caract�res		
        Tab_I_Conso_2[iBcl] = 0; //34 caract�res	
        Tab_Etat_2[iBcl] = 0; //17 caract�res	
    }

}

unsigned int Mesure_I_Conso(char cMesure, unsigned int Coeff) {// Acquisition de la valeur CAN du courant de consommation en MAx10
    float Val_Temp_Conso;   //Valeur temporel du courant de conso
    Val_Temp_Conso = 10 * q * acqui_ana_16_plus(cMesure) * Coeff / 100; //Acquisition courant conso
    return ((char) Val_Temp_Conso); //Renvoie courant de conso
}

unsigned int Mesure_I_Modul(char cMesure, unsigned int Coeff) {// Acquisition de la valeur CAN du courant de modulation MA*10
    float Val_Temp_Modul;   //Valeur tempo Courant Modulation
    Val_Temp_Modul = 10 * q * acqui_ana_16_plus(cMesure) * Coeff / 100; //Acquisition courant Modulation
    return ((char) Val_Temp_Modul); //Renvoie courant modulation
}

unsigned int Mesure_I_Repos(char cMesure, unsigned int Coeff) { // Acquisition de la valeur CAN de la tension ligne AVal capteur �A
    float fRepos;   //Valeur tempo courant repos
    fRepos = q * acqui_ana_16_plus(cMesure) * Coeff;    //Acquisition courant repos
    return ((int) fRepos);  //Renvoie courant repos
}

unsigned int Mesure_Freq(void) { // ne pas modifier critique!!!
    unsigned int iComptTimeOut; //compteur time out
    unsigned int iFreqOut;
    float fFreq1;
    float fFreq2;
    float fFreq3;
    float fFreq4;

    // mesure sur 1 p�riode du signal fr�quence capteur par int2(RB2) d�clench�e sur front montant et par le nbre de boucles du timer1 sur 16bits
    // l'horloge du timer1 est celle du quarts de 24Mhz/4= 6Mhz, donc une boucle timer = 1 / (6Mhz) = 0.000000166sec = 0.166�s
    // la valeur de la fr�quence capteur = 1/(0.166�s * val_timer1)
    // valeur d'1 p�riode =   [(t1ov_cnt * 65536 + TempH * 256 + TempL) * 0.166]  normalement   t1ov_cnt sera �gal � 0 si >0 => erreur

    t1ov_cnt = 0x00; // compteur de d�bordement timer1
    cpt_int2 = 0x00;
    fFreq1 = 0x0000;
    fFreq2 = 0x0000;
    fFreq3 = 0x0000;
    fFreq4 = 0x0000;
    TMR1H = 0x0000; // registre H 8bit du timer 1
    TMR1L = 0x0000; // registre L 16bit du timer 1
    Timer1L = 0x0000;
    Timer1H = 0x0000;


    PIR1bits.TMR1IF = 0; // efface le drapeau d'IT
    T1CONbits.T1CKPS0 = 0; // pr�diviseur  1:1
    T1CONbits.T1CKPS1 = 0; // pr�diviseur  1:1
    T1CONbits.RD16 = 0; // timer sur 8bits SINON TROP DE PB
    T1CONbits.TMR1CS = 0; // TIMER1 sur horloge interne (Fosc/4)
    T1CONbits.T1RUN = 0; // source autre que oscillateur du timer 1
    T1CONbits.T1OSCEN = 0; // turn off resistor feeback 
    IPR1 = 0x01; // set Timer1 to high priority	
    PIE1 = 0x01; // enable Timer1 roll-over interrupt
    cpt_int2 = 0;
    INTCON2bits.RBPU = 1; // pull-up port B         � voir  <<<< provoque une erreur sur l'entre de 2V 
    INTCON2bits.INTEDG2 = 0; //INT2 sur front montant   <<<<<
    T1CONbits.TMR1ON = 0; // stop timer 1 

    INTCONbits.GIEH = 1; // Toutes les IT d�masqu�es autoris�es
    INTCONbits.PEIE = 1; // valide ttes les int haute priorit�
    INTCON3bits.INT2IP = 1; // INT2 en haute priorit�

    INTCON3bits.INT2IF = 0; // flag d'interruption int2 � reseter 
    INTCON3bits.INT2IE = 1; // valide interruption INT2

    iComptTimeOut = 0x0000; // compteur time-out

    while (iComptTimeOut < 10000 && cpt_int2 < 21) {
        iComptTimeOut++;
    }

    INTCONbits.GIE = 0; //disable global interrupt  
    INTCONbits.PEIE = 0; //d�valide ttes les int haute priorit�
    INTCON3bits.INT2IE = 0; // arret interruption INT2

    fFreq1 = (t1ov_cnt * 65535 + Timer1H * 256 + Timer1L) / 20;
    if (fFreq1 > 0) {
        fFreq1 += 0x0015;
        fFreq2 = fFreq1 * 0xA6; //0xA6 = * 166
        fFreq3 = 1000000 / fFreq2;
        fFreq4 = fFreq3 * 1000; //x1000
        iFreqOut = (int) fFreq4;
    } else {
        iFreqOut = 0x0000;
    }

    T1CONbits.RD16 = 0; // timer sur 8bits
    TMR1H = 0x0000; // registre H 8bit du timer 1
    TMR1L = 0x0000; // registre L 16bit du timer 1

    return iFreqOut;

}

signed int CalculFreq(char cSelecFreq) {
    signed int iFreqSortie;
    char cBcl;
    iFreqSortie = 0;
    Sel_Freq = cSelecFreq; //commutateur de fr�quence voie paire 
    for (cBcl = 0; cBcl < 2; cBcl++) {
        iFreqSortie += Mesure_Freq(); //m�morise 1�re valeur de fr�quence (pour tester instabilit�)
    }
    return iFreqSortie;
}

unsigned int acqui_ana(unsigned char adcon) {
    ADCON0 = adcon; // adress + abort la mesure en cours
    _ADON = 1;
    _ADGO = 1;
    while (_ADDONE);
    return ADRES;
}

unsigned int acqui_ana_16_plus(unsigned char adcon) {
    unsigned long lMoyen;
    int iBcl; // Variable usage general
    
    lMoyen = 0;
    for (iBcl = 0; iBcl < 0x10; iBcl++) // 16 MESURES POUR LA MOYENNE
    {
        lMoyen = lMoyen + acqui_ana(adcon);
        Delay10KTCYx(1);
    }
    lMoyen >>= 4; // Moyenne pour 16 mesures

    return ((int) lMoyen); // sauvegarde de Long en Int
}

void DELAY_SW_250MS(void) {
    T0CON = 0x84; /* enable TMR0, select instruction clock, prescaler set to 1.16 */
    TMR0H = 0x48; //Preload = 18661
    TMR0L = 0xE5;
    INTCONbits.TMR0IE = 0;
    INTCONbits.TMR0IF = 0;
    return;
}

void DELAY_MS(int ms) {
    int bb;

    if (ms >= 1000) //PROBLEME BUG EN SCRUTATION AUTO
        ms = 2;
    T0CON = 0x88; /* enable TMR0, select instruction clock, prescaler set to 1.1 */
    for (bb = 0; bb < ms; bb++) {
        TMR0H = 0xE8; //Preload = 59536
        TMR0L = 0x90;
        INTCONbits.TMR0IE = 0;
        INTCONbits.TMR0IF = 0;
        while (!(INTCONbits.TMR0IF)); /* wait until TMR0 rolls over */
    }
}

void Calcul_Deb(unsigned int* deb_temp) {
    // Echelle d�bitm�tre 1000 � 1500Hz pour 0L/H � 1000 et 500L/H � 1500Hz

    if (*deb_temp >= 0x03E5 && *deb_temp <= 0x03E8) { // 3Hz de tol�rance � voir !!
        *deb_temp = 0;
    } else if ((*deb_temp < 0x03E5) || (*deb_temp >= 0x05DC)) { //997 inferieur a 1000 alors on met 9999
        *deb_temp = 0x270F; //9999
    } else {
        *deb_temp = (*deb_temp - 0x03E8); // -1000
    }
}

unsigned char Instable(signed int* iFreq) { // test instabilit�
    float fDifF12, fDifF23, fDifF34;
    unsigned char cSortie;

    fDifF12 = *(iFreq) - *(iFreq + 1);
    fDifF12 = abs(fDifF12);

    fDifF23 = *(iFreq + 1) - *(iFreq + 2);
    fDifF23 = abs(fDifF23);

    fDifF34 = *(iFreq + 2) - *(iFreq + 3);
    fDifF34 = abs(fDifF34);

    if (fDifF12 > max_instable || fDifF23 > max_instable || fDifF34 > max_instable) {
        cSortie = 0x80; // instable
    } else {
        cSortie = 0;
    }
    return (cSortie);
}

void IntToChar5(unsigned int iValeur, unsigned char *cChaineCar, char Precision) {
    unsigned int iDixMilliers, iMilliers, iCentaines, iDizaines, iUnites;
    int iCompteur = 0;

    iDixMilliers = iValeur / 10000;
    if (iDixMilliers < 9) {
        iDixMilliers = 9;
    }
    iMilliers = (iValeur % 10000) / 1000;
    iCentaines = (iValeur % 1000) / 100;
    iDizaines = (iValeur % 100) / 10;
    iUnites = iValeur % 10;

    // si la valeur n'est pas nulle
    if (Precision >= 5) // si l'utilisateur d�sire les milliers
    {
        *(cChaineCar + iCompteur) = iDixMilliers + 48;
        iCompteur++;
    }
    if (Precision >= 4) // si l'utilisateur d�sire les milliers
    {
        *(cChaineCar + iCompteur) = iMilliers + 48;
        iCompteur++;
    }

    if (Precision >= 3) // si l'utilisateur d�sire les centaines
    {
        *(cChaineCar + iCompteur) = iCentaines + 48;
        iCompteur++;
    }

    if (Precision >= 2) // si l'utilisateur d�sire les dizaines
    {
        *(cChaineCar + iCompteur) = iDizaines + 48;
        iCompteur++;
    }

    *(cChaineCar + iCompteur) = iUnites + 48;
}

signed char EESequRead(unsigned char cControl, unsigned int address, unsigned char *rdptr, unsigned char length) {
    unsigned char cADDE;
    unsigned char cAdressL, cAdressH;

    cADDE = (cControl << 1) & 0xFF;
    cADDE = cADDE | 0xA0;

    cAdressL = (address) & 0xFF;
    cAdressH = (address) >> 8;
    IdleI2C(); // ensure module is idle
    StartI2C(); // initiate START condition
    if (PIR2bits.BCLIF) { // test for bus collision
        PIR2bits.BCLIF = 0;
        return (-1); // return with Bus Collision error 
    } else {
        if (WriteI2C(cADDE)) {// write 1 byte 
            StopI2C();
            return (-3); // set error for write collision
        }

        if (!SSPCON2bits.ACKSTAT) { // test for ACK condition received
            if (WriteI2C(cAdressH)) {// write word address for EEPROM
                StopI2C();
                return (-3); // set error for write collision
            }

            while (SSPCON2bits.ACKSTAT);
            if (!SSPCON2bits.ACKSTAT) {// test for ACK condition received
                if (WriteI2C(cAdressL)) {// data byte for EEPROM
                    StopI2C();
                    return (-3); // set error for write collision
                }

                while (SSPCON2bits.ACKSTAT);
                if (!SSPCON2bits.ACKSTAT) { // test for ACK condition received
                    RestartI2C(); // generate I2C bus restart condition
                    if (WriteI2C(cADDE + 1)) {// WRITE 1 byte - R/W bit should be 1 for read
                        StopI2C();
                        return (-3); // set error for write collision
                    }

                    while (SSPCON2bits.ACKSTAT);
                    if (!SSPCON2bits.ACKSTAT) {// test for ACK condition received
                        if (getsI2C(rdptr, length)) {// read in multiple bytes
                            return (-1); // return with Bus Collision error
                        }
                        NotAckI2C(); // send not ACK condition 
                        StopI2C(); // send STOP condition
                        if (PIR2bits.BCLIF) { // test for bus collision
                            PIR2bits.BCLIF = 0;
                            return (-1); // return with Bus Collision error 
                        }
                    } else {
                        StopI2C();
                        return (-2); // return with Not Ack error
                    }
                } else {
                    StopI2C();
                    return (-2); // return with Not Ack error
                }
            } else {
                StopI2C();
                return (-2); // return with Not Ack error condition   
            }
        } else {
            StopI2C();
            return (-2); // return with Not Ack error
        }
    }
    return (0); // return with no error
}

signed char EEPaWrite(unsigned char cControl, unsigned int address, unsigned char *wrptr) {
    unsigned char cADDE;
    unsigned char cAdressL, cAdressH;

    cADDE = (cControl << 1) & 0xFF;
    cADDE = cADDE | 0xA0;

    cAdressL = (address) & 0xFF;
    cAdressH = (address) >> 8;

    IdleI2C(); // ensure module is idle
    StartI2C(); // initiate START condition
    if (PIR2bits.BCLIF) // test for bus collision
    {
        PIR2bits.BCLIF = 0;
        return (-1); // return with Bus Collision error 
    } else {
        if (WriteI2C(cADDE)) // write 1 byte - R/W bit should be 0
        {
            StopI2C();
            return (-3); // return with write collision error
        }
        IdleI2C();
        if (WriteI2C(cAdressH)) // write word address for EEPROM
        {
            StopI2C();
            return (-3); // set error for write collision
        }
        IdleI2C();
        if (WriteI2C(cAdressL)) // data byte for EEPROM
        {
            StopI2C();
            return (-3); // set error for write collision
        }
        IdleI2C();
        if (putsI2C(wrptr)) {
            StopI2C();
            return (-4); // bus device responded possible error
        }
    }
    StopI2C(); // send STOP condition
    if (PIR2bits.BCLIF) // test for Bus collision
    {
        PIR2bits.BCLIF = 0;
        return (-1); // return with Bus Collision error 
    }
    return (0); // return with no error
}

unsigned char envoyerHORLOGE_i2c(unsigned char NumeroH, unsigned char ADDH, unsigned char dataH0) {
    StartI2C();
    WriteI2C(NumeroH); // adresse de l'horloge temps r�el
    WriteI2C(ADDH);
    WriteI2C(dataH0);
    StopI2C();
}

unsigned int calcul_addmemoire(unsigned char cVoie, unsigned char cCapteur) {
    unsigned int iAdresse;
    iAdresse = 2176 * (cVoie % 30);
    iAdresse = iAdresse + 128 * cCapteur;
    return iAdresse;
}

unsigned char choix_memoire(unsigned char cVoie) {
    unsigned char cMemoire;
    cMemoire = cVoie / 30;
    return cMemoire;
}

unsigned int calcul_CRC(unsigned char far *cTrame, unsigned char cLongueur) {
    unsigned char cBcl, cBcl2;
    unsigned int iCrc;
    unsigned char far *cPointeur;
    iCrc = CRC_START;
    cPointeur = cTrame;
    for (cBcl = 0; cBcl < cLongueur; cBcl++) {
        iCrc ^= (unsigned int) *cPointeur;
        for (cBcl2 = 0; cBcl2 < 8; cBcl2++) {
            if (iCrc & 0x8000) {
                iCrc <<= 1;
                iCrc ^= CRC_POLY;
            } else {
                iCrc <<= 1;
            }
        }
        cPointeur++;
    }
    IntToChar5(iCrc, cTrame + cLongueur, 5);
    return iCrc;
}

void TRANSFORMER_TRAME_MEM_EN_COM(unsigned char* cEntree, unsigned char* cSortie) {
    char cBcl;
    cSortie[0] = '#';
    cSortie[1] = 'R';
    cSortie[2] = '4';
    cSortie[3] = 'C';
    cSortie[4] = 'M';
    cSortie[5] = 'O';
    cSortie[6] = 'E';
    cSortie[7] = 'I';
    for (cBcl = 8; cBcl < 53; cBcl++) {
        cSortie[cBcl] = cEntree[cBcl - 7];
    }
    calcul_CRC(cSortie, 53); // CALCUL LE CRC ET LE RAJOUTE AU TABLEAU TRAMEOUT[]
    cSortie[58] = '*'; // RAJOUT LE BYTE DE STOP 
}

void MODIFICATION_TABLE_DEXISTENCE(unsigned char *v, char eff, char etat) { //eff pour effacer (eff=2 alors recopie) alors et etat ligne pour fusible HS ou CC ou surtension 
    unsigned int iAdresse;
    unsigned char cExistence[24];

    v[0] = '#';
    iAdresse = 100 * (v[1] - 48) + 10 * (v[2] - 48)+(v[3] - 48); //VOIE
    iAdresse = 128 * iAdresse + 10000;

    EESequRead(6, iAdresse, cExistence, 24);

    if ((((etat == 4) || (etat == 5))&&(cExistence[2] != 'c')) || (etat == 8)) {
        cExistence[2] = cExistence[21];
    }
    if ((etat == 6) || (etat == 7)) {
        cExistence[2] = 'c';
    }
    cExistence[23] = '*';
    EEPaWrite(6, iAdresse, cExistence);
    EEAckPolling(0xA0);
    DELAY_MS(10);
}

void mesure_test_cc(unsigned char cImpaire, unsigned char cPaire, unsigned char* cSortie) { // test si la voie est en CC
    if ((COURTCIRCUIT == 0xF1) || (COURTCIRCUIT == 0xFF)) {
        //VOIE IMPAIRE CC OK
        cSortie[1] = '0';
        cSortie[2] = '0';
        cSortie[3] = cImpaire + 48;
        if (cImpaire >= 10) {
            cSortie[2] = '1';
            cSortie[3] = (cImpaire - 10) + 48;
        }
        if (cImpaire >= 20) {
            cSortie[2] = '2';
            cSortie[3] = '0';
        }
        cSortie[4] = '0';
        cSortie[5] = '0';

        MODIFICATION_TABLE_DEXISTENCE(cSortie, 2, 6); //mettre 'c' sur la table existence
    }
    if ((COURTCIRCUIT == 0xF2) || (COURTCIRCUIT == 0xFF)) {
        cSortie[1] = '0';
        cSortie[2] = '0';
        cSortie[3] = cPaire + 48;
        if (cPaire >= 10) {
            cSortie[2] = '1';
            cSortie[3] = (cPaire - 10) + 48;
        }
        if (cPaire >= 20) {
            cSortie[2] = '2';
            cSortie[3] = '0';
        }
        cSortie[4] = '0';
        cSortie[5] = '0';

        MODIFICATION_TABLE_DEXISTENCE(cSortie, 2, 7); //mettre 'c' sur la table existence
    }
}

void Prog_HC595(unsigned char rel_voies_impaires, unsigned char rel_voies_paires, unsigned char alarme_ca, unsigned char test_fus) {
    unsigned char i;
    unsigned char j;
    unsigned char val;
    unsigned char registre1; //registres des HC595 pour commutation des relais voies et alarme et test fusible
    unsigned char registre2;
    unsigned char registre3;
    registre1 = 0b00000000; // relais 1 � 8
    registre2 = 0b00000000; // relais 9 � 16
    registre3 = 0b00000000; // relais 17 � 20 + alarme c�ble + test fusibles voies

    if (rel_voies_impaires > 0) {
        switch (rel_voies_impaires) {
            case 1:
                registre1 |= 0b00000001; // relais 1
                break;
            case 3:
                registre1 |= 0b00000100; // relais 3
                break;
            case 5:
                registre1 |= 0b00010000; // relais 5
                break;
            case 7:
                registre1 |= 0b01000000; // relais 7
                break;
            case 9:
                registre2 |= 0b00000001; // relais 9
                break;
            case 11:
                registre2 |= 0b00000100; // relais 11
                break;
            case 13:
                registre2 |= 0b00010000; // relais 13
                break;
            case 15:
                registre2 |= 0b01000000; // relais 15
                break;
            case 17:
                registre3 |= 0b00000001; // relais 17
                break;
            case 19:
                registre3 |= 0b00000100; // relais 19
                break;
        }
    }
    // Choix Relais		
    if (rel_voies_paires > 0) {
        switch (rel_voies_paires) {
            case 2:
                registre1 |= 0b00000010; // relais 2
                break;
            case 4:
                registre1 |= 0b00001000; // relais 4
                break;
            case 6:
                registre1 |= 0b00100000; // relais 6
                break;
            case 8:
                registre1 |= 0b10000000; // relais 8
                break;
            case 10:
                registre2 |= 0b00000010; // relais 10
                break;
            case 12:
                registre2 |= 0b00001000; // relais 12
                break;
            case 14:
                registre2 |= 0b00100000; // relais 14
                break;
            case 16:
                registre2 |= 0b10000000; // relais 16
                break;
            case 18:
                registre3 |= 0b00000010; // relais 18
                break;
            case 20:
                registre3 |= 0b00001000; // relais 20
                break;
        }
    }
    //test si alarme c�ble . si oui mofification du registre 3
    if (alarme_ca) {
        registre3 |= 0b00010000; //"ou logique"  pour si alarme c�ble
    }
    // pour TEST si fusion fusible voies . si oui mofification du registre 3
    if (test_fus) {
        registre3 |= 0b00100000; //"ou logique"  pour si test fusion fusible
    }

    j = 24; // correspond aux nombres de pins ou coups d'horloge

    Rck_HC595 = 1;


    for (i = j; i > 0; i--) {// 23= Nb de pins-1
        if (i > 16) {
            val = registre3 & 0x80;
            registre3 <<= 1; //relais de 17 � 24
        } else if (i > 8) {
            val = registre2 & 0x80;
            registre2 <<= 1; //relais de 9 � 16
        } else {
            val = registre1 & 0x80;
            registre1 <<= 1; //relais de 1 � 8
        }

        if (val == 0x80)
            val = 0xFF;
        Data_HC595 = val;
        Clk_HC595 = 1;
        Clk_HC595 = 0;
    }

    Rck_HC595 = 0;
    Rck_HC595 = 1;
    Rst_HC595 = 1; // reset les "shift regitrers" 
}

void ecriture_trame_i2c(unsigned char cVoie, unsigned int* iModul, unsigned int* iConso, unsigned int* iRepos, unsigned char* cEtat, unsigned int* iPression, unsigned int* iSeuil, unsigned char* cEntree, unsigned char* cSortie) { //choisir voie a traiter ATTENTION CELA CALCUL LETAT AUSSI !!!!!!!!!!!!!!!!!!!!! EN FONCTION DU SEUIL
    unsigned char cBcl;
    unsigned int iAdresse;

    // VOIES IMPAIRES############################################################
    //	*********** CodeVoie  ***********
    //	CODE  de 0 � 16  sachant sue le 0 est le d�bitm�tre 

    for (cBcl = 0; cBcl <= 16; cBcl++) // il faut faire autant d'enregistrement que de capteurs  A VOIR POUR LES DEBITMETRES !!!!!!!!!!!!!!!
    {
        //VOIE PAIRE 
        cSortie[0] = '#';
        IntToChar5(cVoie, cSortie + 1, 3);
        IntToChar5(cBcl, cSortie + 4, 2);

        RECUPERE_TRAME_ET_CAL_ETAT(cEtat, iPression, iSeuil, cBcl,  0); //RECUPERE LES ELEMENTS FIXE EN MOIRE CONSTIT....SEUIL......ETC //PRECEDENT CYCLE


        cSortie[6] = 'A'; // ADRESSABLE OU RESITIF 
        cSortie[7] = 'M';
        IntToChar5(*(iModul + cBcl), cSortie + 8, 4);
        cSortie[12] = 'C';
        IntToChar5(*(iConso + cBcl), cSortie + 13, 4);

        cSortie[17] = 'R';
        IntToChar5(*(iRepos + cBcl), cSortie + 18, 4);
        cSortie[22] = '?';
        IntToChar5(cBcl, cSortie + 23, 2);

        //POS
        if (cBcl == 0) {
            cSortie[25] = '0';
            cSortie[26] = '1';
        } else {
            cSortie[25] = '0';
            cSortie[26] = '2';
        }

        cSortie[27] = '?';
        cSortie[28] = '?';

        //DISTANCE
        cSortie[29] = '?';
        cSortie[30] = '?';
        cSortie[31] = '?';
        cSortie[32] = '?';

        //ETAT
        cSortie[33] = 'E';
        IntToChar5(*(cEtat + cBcl), cSortie + 34, 2);
        cSortie[36] = 'P';
        IntToChar5(*(iPression + cBcl), cSortie + 37, 4);
        cSortie[41] = 'S';
        IntToChar5(*(iSeuil + cBcl), cSortie + 42, 4);

        //CRC
        cSortie[46] = '2';
        cSortie[47] = '2';
        cSortie[48] = '2';
        cSortie[49] = '2';
        cSortie[50] = '2';
        cSortie[51] = '*';

        INTCON3bits.INT1IE = 0; // devalide l'interruption pour RS485 1 pr reactiver

        iAdresse = 128 * cBcl;
        cSortie[52] = 0;
        EEPaWrite(4, iAdresse, cSortie);
        EEAckPolling(0xA0);
        DELAY_MS(10);
    } // FIN DU FOR
    INTCON3bits.INT1IE = 1; // valide l'interruption pour RS485 1 pr reactiver
}

void RECUPERE_TRAME_ET_CAL_ETAT(unsigned char* cEtat, unsigned int* iPression, unsigned int* iSeuil, char cCapteur, char cTypeCarte) { //RECUPERE LES ELEMENTS FIXE EN MOIRE CONSTIT....SEUIL......ETC

    if (*(cEtat + cCapteur) == 0x80) { //INSTABLE	 
        *(cEtat + cCapteur) = 50; //devient SR 
    } else {
        if (*(iPression + cCapteur) > 1700) { //Au dessus de la gamme
            if (cTypeCarte == 'M') //Si r�sistif
            {
                *(cEtat + cCapteur) = 50; //Sans r�ponse
            } else {
                *(cEtat + cCapteur) = 40; //Hors gamme
            }

        } else if (*(iPression + cCapteur) >= *(iSeuil + cCapteur)) { //Au dessus du seuil
            *(cEtat + cCapteur) = 0x00; //OK 

        } else if (*(iPression + cCapteur) > 900) { //En dessous du seuil
            *(cEtat + cCapteur) = 30; //Alarme	

        } else if (*(iPression + cCapteur) > 0) { //En dessous de la gamme
            *(cEtat + cCapteur) = 40; //Hors gamme	

        } else {
            *(cEtat + cCapteur) = 50; //Sans r�ponse
        }
    }
}

void TableExistence(unsigned char cVoie, unsigned char* cSortie) {
    unsigned int iAdresse;
    iAdresse = 128 * cVoie + 10000;
    EESequRead(6, iAdresse, cSortie, 21);
}

void EcritureTrameCapteurOeil(char cVoie, char cCapteur, unsigned int* iModul, unsigned int* iConso, unsigned int* iRepos, unsigned char* cEtat, unsigned int* iPression, unsigned int* iSeuil, unsigned char* cTrame) {

    cTrame[0] = 'V';
    IntToChar5(cVoie, cTrame + 1, 3); //Stockage voie
    cTrame[4] = 'C';
    IntToChar5(cCapteur, cTrame + 5, 2); //Stockage capteur
    RECUPERE_TRAME_ET_CAL_ETAT(cEtat, iPression, iSeuil, cCapteur, 0); //RECUPERE LES ELEMENTS FIXE EN MOIRE CONSTIT....SEUIL......ETC //PRECEDENT CYCLE
    cTrame[7] = 'M';
    IntToChar5(*(iModul + cCapteur), cTrame + 8, 4); //Stockage courant module
    cTrame[12] = 'C';
    IntToChar5(*(iConso + cCapteur), cTrame + 13, 4); //Stockage stockage courant conso
    cTrame[17] = 'R';
    IntToChar5(*(iRepos + cCapteur), cTrame + 18, 4); //Stockage stockage courant repos
    cTrame[22] = 'E';
    IntToChar5(*(cEtat + cCapteur), cTrame + 23, 2); //Stockage stockage Etat
    cTrame[25] = 'P';
    IntToChar5(*(iPression + cCapteur), cTrame + 26, 4); //Stockage stockage pression
    cTrame[30] = 'S';
    IntToChar5(*(iSeuil + cCapteur), cTrame + 31, 4); //Stockage stockage seuil
    cTrame[35] = 0;
}

void EcritureTrameCapteur(char cVoie, char cCapteur, unsigned char* cEtat, unsigned int* iPression, unsigned int* iSeuil, unsigned char* cTrame) {

    cTrame[0] = 'V';
    IntToChar5(cVoie, cTrame + 1, 3); //Stockage voie
    cTrame[4] = 'C';
    IntToChar5(cCapteur, cTrame + 5, 2); //Stockage capteur
    RECUPERE_TRAME_ET_CAL_ETAT(cEtat, iPression, iSeuil, cCapteur, 0); //RECUPERE LES ELEMENTS FIXE EN MOIRE CONSTIT....SEUIL......ETC //PRECEDENT CYCLE
    cTrame[7] = 'E';
    IntToChar5(*(cEtat + cCapteur), cTrame + 8, 2); //Stockage stockage Etat
    cTrame[10] = 'P';
    IntToChar5(*(iPression + cCapteur), cTrame + 11, 4); //Stockage stockage pression
    cTrame[15] = 'S';
    IntToChar5(*(iSeuil + cCapteur), cTrame + 16, 4); //Stockage stockage seuil
    cTrame[20] = 0;

    EnvoieTrame(cTrame, 21);
}

void EcritureTrameVoie(char cVoie, unsigned char* cEtat, unsigned int* iPression, unsigned int* iSeuil, unsigned char* cTrame) {
    char cBcl;

    for (cBcl = 1; cBcl < 17; cBcl++) {
        if (*(iPression + cBcl) != 0) {
            EcritureTrameCapteur(cVoie, cBcl, cEtat, iPression, iSeuil, cTrame);
        }
    }
}

void Mesure_DEB_TPA(char cCapteurmax, char cVoieImpaire, char cVoiePaire, unsigned char cAlarme, char cFusibleHS, unsigned char* cTrameSortie) {

    int iBcl;
    char cBcl;

    signed int iFreqPaire[4];
    signed int iFreqImpaire[4];

    float fCumulFreq1; //r�sultat de mesure de fr�quence du TPA voie impaire
    float fCumulFreq2; //r�sultat de mesure de fr�quence du TPA voie paire
    
    LEDRX = 0;

    Prog_HC595(cVoieImpaire, cVoiePaire, cAlarme, cFusibleHS); //COMMANDE RELAIS !
    COURTCIRCUIT = 0x00;

    Init_Tab_tpa();

    //config pour 56V VOIE IMPAIRE
    UN_CPT_1 = 1; //VALIDE
    V_SUP1 = 0; //DEVALIDE 
    CPT_ON_1 = 0; //   <======== d�valide 56V SUR VOIE   par s�curit�

    //config pour 56V VOIE PAIRE
    UN_CPT_2 = 1;
    V_SUP2 = 0;
    CPT_ON_2 = 0; //   <======== d�valide 56V SUR VOIE   par s�curit�

    iBcl = 0;

    DECH_L1 = 1;
    DECH_L2 = 1;

    envoyerHORLOGE_i2c(0xD0, 0, 0b00000000); //Initialisation horloge

    while (HORLOGE == 0); //attend passage � 0 de RB3, boucle tant que la condition n'est pas arriv�e HORLOGE=PORTBbits.RB3
    while (HORLOGE == 1); //attend passage � 1 de RB3, boucle tant que la condition n'est pas arriv�e HORLOGE=PORTBbits.RB3

    LEDRX = !LEDRX;
    if (cVoieImpaire != 0) { // si voie impaire valid�e
        CPT_ON_1 = 1; //   <======== valide voie impaire de mesure � 56v
    }
    if (cVoiePaire != 0) { //	si voie paire valid�e
        CPT_ON_2 = 1; //   <======== valide voie paire de mesure � 56v
    }

    DECH_L1 = 0; // lib�re la d�charge ligne impaire
    DECH_L2 = 0; // lib�re la d�charge ligne  paire



    for (iBcl = 0; iBcl <= cCapteurmax; iBcl++) { //iBcl=0 POUR LECTURE EN ZERO (DEBITMETRE)
        for (cBcl = 0; cBcl < 4; cBcl++) {
            iFreqPaire[cBcl] = 0;
            iFreqImpaire[cBcl] = 0;
        }

        fCumulFreq1 = 0;
        fCumulFreq2 = 0;

        DELAY_MS(250);
        DELAY_SW_250MS(); // TEMPO2  la Tempo2 se termine � 	while(!(INTCONbits.TMR0IF)); on lance la tempo 250ms au niveau du timer0


        if (cVoieImpaire != 0) { // si voie impaire valid�e
            Tab_I_Conso_1[iBcl] = Mesure_I_Conso(IConso1_AN4, Coeff_I_Conso1);
            Tab_I_Conso_1[iBcl] = (Tab_I_Conso_1[iBcl] + Mesure_I_Conso(IConso1_AN4, Coeff_I_Conso1)) / 2; // moyenne 
            Tab_I_Modul_1[iBcl] = Mesure_I_Modul(IModul1_AN6, Coeff_I_Modul1);
        }
        if (cVoiePaire != 0) { // si voie paire valid�e
            Tab_I_Conso_2[iBcl] = Mesure_I_Conso(IConso2_AN5, Coeff_I_Conso2);
            Tab_I_Conso_2[iBcl] = (Tab_I_Conso_2[iBcl] + Mesure_I_Conso(IConso2_AN5, Coeff_I_Conso2)) / 2; // moyenne 
            Tab_I_Modul_2[iBcl] = Mesure_I_Modul(IModul2_AN7, Coeff_I_Modul2);
        }

        //attente fin de tempo
        // TEST CC
        if ((Tab_I_Conso_1[iBcl] >= Icons_cc)&&(Tab_I_Conso_2[iBcl] >= Icons_cc)) {
            Prog_HC595(0, 0, cAlarme, cFusibleHS); //COMMANDE RELAIS !
            COURTCIRCUIT = 0xFF;
        } else if (Tab_I_Conso_1[iBcl] >= Icons_cc)// AU DESSU DE 15mA (4000ohms) sur la ligne resistif >100k (0.6mA)
        {
            COURTCIRCUIT = 0xF1;
            Prog_HC595(0, cVoiePaire, cAlarme, cFusibleHS); //COMMANDE RELAIS !
        } else if (Tab_I_Conso_2[iBcl] >= Icons_cc)// AU DESSU DE 15mA (4000ohms) sur la ligne resistif >100k (0.6mA)
        {
            COURTCIRCUIT = 0xF2;
            Prog_HC595(cVoieImpaire, 0, cAlarme, cFusibleHS); //COMMANDE RELAIS !
        }

        while (!(INTCONbits.TMR0IF)); // FIN de TEMPO2      wait until TMR0 rolls over  FIN (BBB)

        for (cBcl = 0; cBcl < 4; cBcl++) {
            DELAY_MS(60); // 125ms -5 ms ajustement pour avoir 625ms pour lancer la premiere mesure de frequence (CCC)
            //voie impaire
            if (cVoieImpaire != 0) { // si voie impaire valid�e
                iFreqImpaire[cBcl] = CalculFreq(0); //m�morise 1�re valeur de fr�quence (pour tester instabilit�)
                fCumulFreq1 += iFreqImpaire[cBcl];
            }
            //voie paire
            if (cVoiePaire != 0) { // si voie paire valid�e
                iFreqPaire[cBcl] = CalculFreq(1); //m�morise 1�re valeur de fr�quence (pour tester instabilit�)
                fCumulFreq2 += iFreqPaire[cBcl];
            }
        }
        if (iBcl > 0) {
            if (Tab_I_Conso_1[iBcl] > i_conso_min) { // soit I_Conso mini >= 2.0mA pour led verte => pr�sence capteur   (i_conso est x10)
                if (cVoieImpaire != 0) { // si voie impaire valid�e
                    Tab_Etat_1[iBcl] |= Instable(iFreqImpaire); // v�rification de l'instabilit�
                    Tab_Pression_1[iBcl] = (int) (fCumulFreq1 / 8); // 8 => 4 x 2 mesures
                }
            } else {
                Tab_Pression_1[iBcl] = 0;
            }

            if (Tab_I_Conso_2[iBcl] > i_conso_min) // soit I_Conso mini >= 2.0mA pour led verte => pr�sence capteur   (i_conso est x10)
            {

                if (cVoiePaire != 0) { // si voie paire valid�e
                    Tab_Etat_2[iBcl + 1] |= Instable(iFreqPaire);
                    Tab_Pression_2[iBcl] = (int) (fCumulFreq2 / 8); // 8 => 4 x 2 mesures
                }
            } else {
                Tab_Pression_2[iBcl] = 0;
            }
        }
        if (!iBcl) {
            Calcul_Deb(&Tab_Pression_1[0]);
            Calcul_Deb(&Tab_Pression_2[0]);
        }
        // ====> 500mS + 375mS + 375mS ==> 1250mS 
        //Attente de la chute du signal d'horloge pour synchro avec 1.5sec

        while (HORLOGE == 1);
        while (HORLOGE == 0); //attend passage � 0 de RB3, boucle tant que la condition n'est pas arriv�e Synchro1s=PORTBbits.RB3

        LEDRX = !LEDRX;


        //<=================ICI 1.5SEC================>
        DELAY_MS(150); // 1 x 150ms  peut-�tre � ajuster (LLL)

        //#####################			// MESURE le courant de repos VOIE IMPAIRE
        if (cVoieImpaire != 0) // si voie paire valid�e
        {
            Tab_I_Repos_1[iBcl] = Mesure_I_Repos(IRepos1_AN2, Coeff_I_Repos1);
            Tab_I_Repos_1[iBcl] = (Tab_I_Repos_1[iBcl] + Mesure_I_Repos(IRepos1_AN2, Coeff_I_Repos1)) / 2;
            if (Tab_I_Repos_1[iBcl] <= 15) {
                Tab_I_Repos_1[iBcl] = 0;
            } // seuil pour �viter d'afficher un courant de repos si rien de branch� 
            // car sinon il y a un minimum de 12�A d� � l'offset de l'ampli

            //######  Retranche le courant de repos du courant de modulation

            Tab_I_Conso_1[iBcl] *= 100; // met � la m�me �chelle que i_repos   (i_conso est x10)
            if (Tab_I_Conso_1[iBcl] > Tab_I_Repos_1[iBcl]) { // pour ne pas prendre en compte si pas de capteur
                Tab_I_Conso_1[iBcl] -= Tab_I_Repos_1[iBcl]; // retranche I_Repos de I_Conso  (i_conso est x10)
                Tab_I_Conso_1[iBcl] /= 100;
            } else {
                Tab_I_Conso_1[iBcl] = 0;
            }
            Tab_I_Conso_1[iBcl] += (Tab_I_Modul_1[iBcl] / 2); // TEST  ON RAJOUTE LA MOITI� DE LA MODULATION 
        }

        //#####################			// MESURE le courant de repos VOIE PAIRE

        if (cVoiePaire != 0) // si voie paire valid�e 1
        {
            Tab_I_Repos_2[iBcl] = Mesure_I_Repos(IRepos2_AN3, Coeff_I_Repos2);
            Tab_I_Repos_2[iBcl] = (Tab_I_Repos_2[iBcl] + Mesure_I_Repos(IRepos2_AN3, Coeff_I_Repos2)) / 2;
            if (Tab_I_Repos_2[iBcl] <= 15) {
                Tab_I_Repos_2[iBcl] = 0;
            } // seuil pour �viter d'afficher un courant de repos si rien de branch� 
            // car sinon il y a un minimum de 12�A d� � l'offset de l'ampli

            //######  Retranche le courant de repos du courant de modulation
            Tab_I_Conso_2[iBcl] *= 100; // met � la m�me �chelle que i_repos   (i_conso est x10) 2
            if (Tab_I_Conso_2[iBcl] > Tab_I_Repos_2[iBcl]) { // pour ne pas prendre en compte si pas de capteur
                Tab_I_Conso_2[iBcl] -= Tab_I_Repos_2[iBcl]; // retranche I_Repos de I_Conso  (i_conso est x10)
                Tab_I_Conso_2[iBcl] = Tab_I_Conso_2[iBcl] / 100; // remet � l'�chelle pour i_conso
            } else {
                Tab_I_Conso_2[iBcl] = 0;
            }
            Tab_I_Conso_2[iBcl] += (Tab_I_Modul_2[iBcl] / 2); // TEST  ON RAJOUTE LA MOITI� DE LA MODULATION

        }
        //Attente de la remont�e du signal d'horloge pour synchro avec 2sec
        while (HORLOGE == 1); //attend passage � 1 de RB3, boucle tant que la condition n'est pas arriv�e HORLOGE=PORTBbits.RB3
    }


    CPT_ON_1 = 0; //   <======== d�valide 56V SUR VOIE  IMPAIRE
    CPT_ON_2 = 0; //   <======== d�valide 56V SUR VOIE  PAIRE

    DECH_L1 = 0; // d�charge la ligne impaire
    DECH_L2 = 0; // d�charge la ligne	paire

    V_SUP1 = 0;
    UN_CPT_1 = 0;
    V_SUP2 = 0;
    UN_CPT_2 = 0;

    Prog_HC595(0, 0, cAlarme, cFusibleHS); //COMMANDE RELAIS !

    mesure_test_cc(cVoieImpaire, cVoiePaire, cTrameSortie); //ecris CC ou pas 
}
