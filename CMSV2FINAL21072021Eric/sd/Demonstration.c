/******************************************************************************
 *
 *               Microchip Memory Disk Drive File System
 *
 ******************************************************************************
 * FileName:        Demonstration.c
 * Dependencies:    FSIO.h
 * Processor:       PIC18
 * Compiler:        C18
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the ï¿½Companyï¿½) for its PICmicroï¿½ Microcontroller is intended and
 * supplied to you, the Companyï¿½s customer, for use solely and
 * exclusively on Microchip PICmicro Microcontroller products. The
 * software is owned by the Company and/or its supplier, and is
 * protected under applicable copyright laws. All rights are reserved.
 * Any use in violation of the foregoing restrictions may subject the
 * user to criminal sanctions under applicable laws, as well as to
 * civil liability for the breach of the terms and conditions of this
 * license.
 *
 * THIS SOFTWARE IS PROVIDED IN AN ï¿½AS ISï¿½ CONDITION. NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 * TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 * IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
*****************************************************************************/


/*****************************************************************************
	Note:  This file is included to give you a basic demonstration of how the
           functions in this library work.  Prototypes for these functions,
           along with more information about them, can be found in FSIO.h
*****************************************************************************/

//DOM-IGNORE-BEGIN
/********************************************************************
 Change History:
  Rev            Description
  ----           -----------------------
  1.2.4 - 1.2.6  No Change
  1.2.6          Add support for the PIC18F46J50_PIM
  1.3.4          Added support for PIC18F8722 on PIC18 Explorer Board
********************************************************************/
//DOM-IGNORE-END

#include <p18f8723.h> //biblioth�que du Microcontroleur
#include <stdlib.h> //biblioth�que 
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <i2c.h> 
#include "FSIO.h"


























#if defined(__18F8723)
	#pragma config OSC=HS, FCMEN=OFF, IESO=OFF, PWRT=OFF, WDT=OFF, LVP=OFF, XINST=OFF
#else
#endif

#pragma udata udata
  
// MEMOIRE la taille #03301R111122223333BRA/4444/22/1COMMENTAIRESICIROB03326111525001020000456781108650888* soit 86octets
unsigned char TRAMEOUT[125];  
//#pragma udata udata 
unsigned char TRAMEIN[125];

  

//#pragma udata udata2
//unsigned char TMP[250];


#pragma udata udata1
//MEMOIRE/////////////////////
char VOIECOD[6];
char TYPE[1];
char CMOD[4];
char CCON[4];
char CREP[4];
char CONSTITUTION[13];
char COMMENTAIRE[20];
char CABLE[3];
char JJ[2];
char MM[2];
char HH[2];
char mm[2];
char ii[1];
char COD[2];
char POS[2];
char iiii[4];
char DISTANCE[5];
char ETAT[2];
char VALEUR[4];
char SEUIL[4];
char TYPECARTE[5];
//////////////////////////

////// TRAMES IDENTIQUES
char START[1];
char ID1[2];
char ID2[2];
unsigned char FONCTION[3];
char CRC[5];
char STOP[1];
char iii;
//PARTIE DE LISTE


////// TRAMES ERREUR
char TYPE_ERREUR[2];
char INFORMATION_ERREUR[20];
char NBRECAPTEURS_CABLE;
/////

char PRIO_AUTO_IO; //0x00 rien //0x01 
//char TOTALCAPTEURS[] A REVOIR TRAME ENORME
///// TRAME  HORLOGE
char HORLOGE[12];
/////TRAME EFFACER CAPTEUR
char CAPTEUR_A_EFFACER[5];
/////DEMANDE INFOS A LA CARTE MERE ou demande a la carte relais
char DEMANDE_INFOS_CAPTEURS[10];
////OEIL
char OEIL_CAPTEUR[5];
////
char PAGE[6];

char INSTRUCTION[25]; //pour l'envoi d'instruction

char capt_exist;

float VDD1,VDD2,VDD3,VDD4,VDD5,Vtemp,VDD6,VBAT; 

char NORECEPTION;


int pointeur_out_in;
char tmp,tmp2;
unsigned int TMPI;

char ENTREES1,ENTREES2; 

char HEURE,MINUTE,SECONDE,MOIS,JOUR,ANNEE,DATE;

#pragma udata udata2
char TMP[125];  // PR SAV DES TRAMES
char P_RESERVOIR[15];
char P_SORTIE[15];
char P_SECOURS[15];
char POINT_ROSEE[11];
char DEBIT_GLOBAL[11];
char TEMPS_CYCLE_MOTEUR[11];
char TEMPS_MOTEUR[15];
char PRESENCE_220V[6];
char PRESENCE_48V[6];



char DATAFIX[10];



#pragma udata udata3
char SAVTAB[25];


char FONCTION_ACTIVATION_AUTO;
char numdecarteRI;
char TROPDENONREPONSE;

char CRC_ETAT;
char ETAT_IO;
char FIRST_DEMAR;

char MODERAPID;

char TYPECOMMANDE;
char temoinfinvoieZIZ;
char IOTRAVAIL;

char PASDE_S; 
char cycleN; //CYCLE 1 DIC CYCLE 2 ZIZ 
char RESCRUTATION;
char NUMEROCARTE;

unsigned int start;
char NBREDEMANDEIO;

char funct;
















void delay_us(char tempo);
void delay_ms(char tempo);

void resetTO(void);
void calcul_CRC(void);
void controle_CRC(void);

void recuperer_trame_memoire (char r,char mode);
void construire_trame_memoire (void);
void RECUPERATION_DES_TRAMES (char *tab);
void Init_RS485(void);
char RECEVOIR_RS485(unsigned char tout);
void RS485(unsigned char carac);
void ENVOIE_DES_TRAMES (char R,int fonctionRS,char *tab);
void TEMPO(char seconde);
void TRAMERECEPTIONRS485DONNEES (unsigned char tout);
unsigned char putstringI2C( unsigned char *wrptr );
 
void TRAMEENVOIRS485DONNEES (void); 
void SAV_EXISTENCE(char *tt);



unsigned char ECRIRE_HORLOGE(unsigned char zone,unsigned char time);  
unsigned char envoyerHORLOGE_i2c(unsigned char NumeroH,unsigned char ADDH, unsigned char dataH0);
unsigned char LIRE_HORLOGE(unsigned char zone);
signed char litHORLOGE_i2c(unsigned char NumeroH,unsigned char AddH);
void creer_trame_horloge (void); 
void entrer_trame_horloge (void);

char VALIDER_CAPTEUR_CM(void);

//FONCTION POUR LES TRAMES STANDARDS
void intervertirOUT_IN(void);


void RECHERCHE_DONNEES_GROUPE(unsigned char ici);
void Init_I2C(void);
unsigned char LIRE_EEPROM(unsigned char ADDE,unsigned int ADDHL,unsigned char *tab,char r);
unsigned char ECRIRE_EEPROMBYTE(unsigned char ADDE,unsigned int ADDHL,unsigned char *tab,char g);
unsigned int calcul_addmemoire (unsigned char *tab);
unsigned char choix_memoire(unsigned char *tab);
char FCYCLE,TESTCYCLE;
void IntToChar(signed int value, char *chaine,int Precision);
void TRANSFORMER_TRAME_MEM_EN_COM (void);
void EFFACEMENT (unsigned char *tab);


char trouver_carte_suivantvoie(char *tab);

void GROUPE_EN_MEMOIRE(unsigned char *tab,char lui);
void EFFACER_LISTE(char t);
void METTRE_EN_MEMOIRE_LISTE(unsigned char *tab,char t);
void INTERROGER_TOUT_LES_CAPTEURS(char t);

void CONTROLE_CHARGE_BATT(void);
void MESURE_DES_TENSIONS (void);
float CAN (char V);
void lire_horloge(void);
//INTERRUPTIONS

 
void  SERIALISATION (void);

void identificationINTER(char dequi,char fairequoi );


void REPOSAQUIENVOYER (unsigned char *tabo);
void AQUIENVOYER (unsigned char *tabo);
void PREPARATION_PAGE_AUTO(void);

float AVERAGE (char canal,char avemax,char megmax);

char EFFACER_CAPTEUR_SUR_CM(char gh);
void MODIFICATION_TABLE_DEXISTENCE(char *v,char eff,char etat); //eff pour effacer (eff=2 alors recopie) alors et etat ligne pour fusible HS ou CC ou surtension 
void CHERCHER_DONNNEES_CABLES(char *g,char j);

unsigned char SPI_WriteByte(unsigned char val);
unsigned char SD_SendCommand(char CMD, unsigned long sct, char CRC);
unsigned char SD_ReadSingleBlock(unsigned long sector);
unsigned char SD_WriteSingleBlock(unsigned long sector);
unsigned char sd_reset();
void init_spi (void);
static void spiReceiveWait();
unsigned spiDataReady(); //Check whether the data is ready to read
void spi_high();
void spi_low();
void spiWrite(unsigned char dat); //Write data to SPI bus;
char spiRead() ; 
void RECEVOIR_PICSD(char t);
void IDENTIF_TYPE_CARTE (void);
void TROUVER_ROBINET(void);

void inter (void);
//void voir_si_voie_existe(void);
void REMISE_EN_FORME_PAGE(void);

void ecriresurSD(char yui);


void lireTRAMESDBN (unsigned int zone,unsigned char longueur);
void lireTRAMESD (unsigned int zone,unsigned char longueur);

unsigned int CRC16(char far *txt, unsigned  char lg_txt);
void IntToChar5(unsigned int value, char *chaine,char Precision);
void CRC_verif(void);
void excel (void);
void DESexcel (void);
//RS485
#define DERE PORTAbits.RA4 // direction RX TX
#define RX PORTCbits.RC7 // RX
#define INT0 PORTBbits.RB0 // INT0
#define INT1 PORTBbits.RB1 // INT1
#define INT2 PORTBbits.RB2 // INT2

#define I2C_MEMOIRE PORTJbits.RJ1 
#define I2C_HORLOGE PORTJbits.RJ0 
#define I2C_INTERNE PORTJbits.RJ2




#define CHARGE_ON PORTEbits.RE1
#define DX1 PORTHbits.RH2
#define DX2 PORTHbits.RH3
 
#define STATO PORTJbits.RJ3
#define STAT PORTEbits.RE7


//BASCULE
#define SHLD PORTDbits.RD0
#define CLK PORTDbits.RD3
#define INH PORTDbits.RD2


//RS 485 AUTO ET ISOLATEUR
//#define CMvAU PORTCbits.RC0
//#define  AUvCM PORTBbits.RB4
//

#define IO_EN_COM PORTHbits.RH4


#define DRS485_R1 PORTFbits.RF3
#define DRS485_R2 PORTFbits.RF4
#define DRS485_R3 PORTFbits.RF5
#define DRS485_R4 PORTFbits.RF6
#define DRS485_R5 PORTFbits.RF7


#define CYCLE_R1 PORTJbits.RJ4
#define CYCLE_R2 PORTJbits.RJ5
#define CYCLE_R4 PORTJbits.RJ6
#define CYCLE_R3 PORTJbits.RJ7
#define CYCLE_R5 PORTCbits.RC2


#define DRS485_IO PORTBbits.RB4 /////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!





void main (void)
{
spi_low();
init_spi  ();

 
    
//spiInit(SPI_SLAVE_SS_EN, SPI_DATA_SAMPLE_MIDDLE, SPI_CLOCK_IDLE_LOW, SPI_IDLE_2_ACTIVE);
 start=0;
 while(1)
  {//

spi_low();
init_spi  ();


//RECEVOIR_PICSD(0x00);
//spiWrite('X'); //Write data to SPI bus
 



  while (!MDD_MediaDetect());   ////////////////////// d�tection de pr�sence de la carte m�moire 

	// Initialize the library
	while (!FSInit());         /////////////////initialiser la carte SD


//lireTRAMESD (0,91); //LIRE TRAME SD PAR 91 pour trame donnees capteurs
//lireTRAMESD (91,91); //LIRE TRAME SD PAR 91 pour trame donnees capteurs

//goto testlire;
RECEVOIR_PICSD(0x00);
if (TRAMEIN[2]=='W') //DEMARRAGE
ecriresurSD('D');
if (TRAMEIN[2]=='V') //CYCLIQUE
ecriresurSD('C');
if (TRAMEIN[2]=='M') //MINITEL
ecriresurSD('M');
if (TRAMEIN[2]=='N') //MINITEL
ecriresurSD('N');
if (TRAMEIN[2]=='B') //MINITEL
ecriresurSD('B');


if (TRAMEIN[2]=='R')
{
testlire:
lireTRAMESD (start,117); //LIRE TRAME SD PAR 117 pour trame donnees capteurs


 DESexcel();

start=start+117;


//goto testlire;
if (TRAMEOUT[1]=='$') //FIN DE FICHIER
{
start=0;
//RECEVOIR_PICSD(0xFF);
}
RECEVOIR_PICSD(0x00);//0 91


//ecriresurSD();
//lireTRAMESD (91,91); //LIRE TRAME SD PAR 91 pour trame donnees capteurs
//RECEVOIR_PICSD(0x00);
}


if (TRAMEIN[2]=='L')
{
testlire2:
lireTRAMESDBN (start,99); //LIRE TRAME SD PAR 117 pour trame donnees capteurs
for (iii=0;iii<100;iii++)
{
TRAMEOUT[iii]=TMP[iii];
}

start=start+99;


//goto testlire;
if (TRAMEOUT[1]=='$') //FIN DE FICHIER
{
start=0;
//RECEVOIR_PICSD(0xFF);
}
RECEVOIR_PICSD(0x00);//0 91


//ecriresurSD();
//lireTRAMESD (91,91); //LIRE TRAME SD PAR 91 pour trame donnees capteurs
//RECEVOIR_PICSD(0x00);
}


} 


 

} //FIN MAIN

static void spiReceiveWait()
{
  while (SSP1STATbits.BF==0); // Wait for Data Receipt complete
}


unsigned spiDataReady() //Check whether the data is ready to read
{
  if(SSP1STATbits.BF)
    return 1;
  else
    return 0;
}


char spiRead()    // Read the received data
{
  spiReceiveWait();      // Wait until all bits receive
  return(SSP1BUF); // Read the received data from the buffer
}


void spiWrite(unsigned char dat) //Write data to SPI bus
{
  SSP1BUF = dat;
}



void init_spi  (void)

{




TRISCbits.TRISC2 = 1;  // RD7/SS - Output (Chip Select)
  
TRISCbits.TRISC3= 1;   // RD4/SDO - Output (Serial Data Out)
TRISCbits.TRISC4= 1;   // RD5/SDI - Input (Serial Data In)
TRISCbits.TRISC5= 0;   // RD6/SCK - Output (Clock)


 
   SSP1STATbits.SMP = 0;        // input is valid in the middle of clock
    SSP1STATbits.CKE = 0;        // 0  for rising edge is data capture
 

	SSP1CON1bits.CKP = 0;        // high value is passive state
  
    SSP1CON1bits.SSPM3 = 0;        // speed f/64(312kHz), Master
    SSP1CON1bits.SSPM2 = 1;
    SSP1CON1bits.SSPM1 = 0;
    SSP1CON1bits.SSPM0 = 1;
 
    SSP1CON1bits.SSPEN = 1;        // enable SPI






 



}


void spi_low()
{
SSP1CON1=0x22; //SPI clk use the system  375Khz (32)
}

////////////////////////////////////////////////
//set high band.
void spi_high()
{
SSP1CON1=0x21; //SPI clk use the system clk--fosc/64 0x31
}
 

void ecriresurSD(char yui)
{ 

	FSFILE * pointer;
FSFILE * pointer1;
//	char path[30];
	char count = 30;
	char * pointer2;
	SearchRec rec;
	unsigned char attributes;
	unsigned char size = 0;
int i, j;










   //********* Initialize Peripheral Pin Select (PPS) *************************
    //  This section only pertains to devices that have the PPS capabilities.
    //    When migrating code into an application, please verify that the PPS
    //    setting is correct for the port pins that are used in the application.
    
    while (!MDD_MediaDetect());   ////////////////////// d�tection de pr�sence de la carte m�moire 

	// Initialize the library
	while (!FSInit());         /////////////////initialiser la carte SD

#ifdef ALLOW_WRITES

if ((HORLOGE[1]<1)&&(HORLOGE[1]>12))
{

if (SetClockVars ((2000+HORLOGE[0]),HORLOGE[1],HORLOGE[2],HORLOGE[3], HORLOGE[4],HORLOGE[5]))          ////r�glage de l'heure 
		while(1);
}
else
{
	if (SetClockVars (2017, 11, 29, 15, 5, 26))          ////r�glage de l'heure 
while(1);
}

///////////////////////////////////////////////////////////////////deuxi�me fichier////////////////////////////////////////////
//	// Create a second file
	if (yui=='D') //DEMARRAGE
	pointer = FSfopenpgm ("DEMCMS.TXT", "w");
	if (yui=='C') //CYCLIQUE
	pointer = FSfopenpgm ("SAV.TXT", "w");
	if (yui=='M') //PAR MINITEL
	pointer = FSfopenpgm ("SAVM.TXT", "w");
	if (yui=='N') //PAR MINITEL
	pointer = FSfopenpgm ("SAVN.TXT", "w");
	if (yui=='B') //BLOC NOTES
	pointer = FSfopenpgm ("SBLOC.TXT", "w");
	if (pointer == NULL)
		while(1);
i=0;
do {

////ICI ON RECUPERER LES DONNEES DE MICRO 1
RECEVOIR_PICSD(0x00);
if (yui!='B')
excel();

//	// Write the string to it again
	if (yui=='B') {
	TRAMEIN[98]=0x0D; //SAUT DE LIGNE
	TRAMEIN[99]=0x0A;
	TMP[0]=	TRAMEIN[0];
	TMP[1]=	TRAMEIN[1];
	TMP[2]=	TRAMEIN[2];
	if (TMP[2]=='$')
	yui='B';	
	if (FSfwrite ((void *)TRAMEIN, 1, 99, pointer) != 99)  
		while(1); 
					}
    
	if (yui!='B') {
	if (FSfwrite ((void *)TMP, 1, 117, pointer) != 117)  //90 CARACTERES TRAME #R1CM............* SANS R1CM 86
		while(1); 
					}


    i++;
//TRAMEIN[2]=i;
}while(TMP[2]!='$'); //1700 max //A CAUSE DE;
//	// Close the file
	if (FSfclose (pointer))
		while(1);
 


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif
//FSremovepgm ("FILE75.txt");       //// �ffacer  un fichier.
///////////////////////////////////////////////////////////////////////////////////////////////////////////


}

void lireTRAMESDBN (unsigned int zone,unsigned char longueur)
{

	FSFILE * pointer;
FSFILE * pointer1;
//	char path[30];
	char count = 30;
	char * pointer2;
	SearchRec rec;
	unsigned char attributes;
	unsigned char size = 0;
int i, j;

unsigned int yy;



	// Open file 1 in read mode
	pointer = FSfopenpgm ("SBLOC.TXT", "r");                 ///ouvrir un fichier
	if (pointer == NULL)
		while(1);



   if (FSfseek(pointer,zone, SEEK_SET))                             /////////////// d�finir la posotion dans le fichier 0 DEBUT 91 SUIVANTES 91*N
		while(1);
  
	if (FSfread (TMP, longueur, 1, pointer) != 1) //////////////////lire des donn�es  90 ELEMENTS #CMR1....*
		while(1);

		////ICI ON ENVOIE LES DONNEES A MICRO 1
		//RECEVOIR_PICSD(0x00);
 		
	//if (FSfeof (pointer))            /////////////tester la fin de texte 
	//	while(1);




	// Close the file
	if (FSfclose (pointer))/////////////////fermer le fichier 
		while(1);

	//{
	//	while(1);
	//}
}


void lireTRAMESD (unsigned int zone,unsigned char longueur)
{

	FSFILE * pointer;
FSFILE * pointer1;
//	char path[30];
	char count = 30;
	char * pointer2;
	SearchRec rec;
	unsigned char attributes;
	unsigned char size = 0;
int i, j;

unsigned int yy;



	// Open file 1 in read mode
	pointer = FSfopenpgm ("DEMCMS.TXT", "r");                 ///ouvrir un fichier
	if (pointer == NULL)
		while(1);



   if (FSfseek(pointer,zone, SEEK_SET))                             /////////////// d�finir la posotion dans le fichier 0 DEBUT 91 SUIVANTES 91*N
		while(1);
  
	if (FSfread (TMP, longueur, 1, pointer) != 1) //////////////////lire des donn�es  90 ELEMENTS #CMR1....*
		while(1);

		////ICI ON ENVOIE LES DONNEES A MICRO 1
		//RECEVOIR_PICSD(0x00);
 		
	//if (FSfeof (pointer))            /////////////tester la fin de texte 
	//	while(1);




	// Close the file
	if (FSfclose (pointer))/////////////////fermer le fichier 
		while(1);

	//{
	//	while(1);
	//}
}







//////////////////////////////////////////////////////////////////////////////
/////////////////////tompo////////////////////////////////////////////////
void delay_ms(char tempo)
{

}  






//////////////////////////////////////////////////////////////////
// fonction permettant d'initialiser le port s�rie				//
//////////////////////////////////////////////////////////////////


void Init_RS485(void)
{
}





//////////////////////////////////////////////////////////////////
// reception d'un caract�re sur le port s�rie		//
//////////////////////////////////////////////////////////////////


char RECEVOIRST_RS485(void)
{
}













 

  



  
 
 
 
 
 
//////////////////////////////////////////////////////////////////
// reception d'un caract�re sur le port s�rie	avec tempo max	//
//////////////////////////////////////////////////////////////////
     
    
char RECEVOIR_RS485(unsigned char ttout) 
{
}
  

//////////////////////////////////////////////////////////////////
// �mission d'un caract�re sur le port s�rie					//
//////////////////////////////////////////////////////////////////

void RS485(unsigned char carac)
{ 
}






//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UNIQUEMENT POUR CARTE MERE ET A LA DEMANDE DE LA CARTE MERE (LORS d'une PREMIERE TRAME CM-->Rx  et  erreurs)  //
//ECRIRE 1 pour carte R1 ,2 pour R2 etc..................                           
//puis le numero de la fonction exemple 

// pour DIC=68*2+73+67=276 (avec code decimal des carac ascii) DEMANDE INFOS CAPTEUR A LA CARTE RELAIS	//	
//puis le tableau contenant les donn�es a envoyer suivant le code de la fonction																								                //
// le resultat est stock� dans la TRAMEOUT[]
//////////////////////////////////////////////////////////////////////////////////////////////////////


 

void ENVOIE_DES_TRAMES (char R,int fonctionRS,char *tab) 





{




}







 
void recuperer_trame_memoire (char w,char mode)  //RECUPERE TRAME MET DANS LES VARIABLES PUIS ECRIT DANS L'EPROOM


{

  

}



 
void construire_trame_memoire (void)  // ICI ON RECUPERE LE CONTENU DE LA MEMOIRE ET ON LE MET DANS UNE TRAME COM ET DANS LES VARIABLES


{


 
}
 

void calcul_CRC(void)
{


}



void controle_CRC(void)
{




}




 





void resetTO(void)
{



}






 
// CALCUL EMPLACEMENT MEMOIRE
unsigned int calcul_addmemoire (unsigned char *tab)
{

}

// choix du numero memoire suivant N�voie
unsigned char choix_memoire(unsigned char *tab)
{

}





//Lecture dans l'EEPROM N�0 N�1 N�2 ou N�3 envoyer 000 ou 001......
unsigned char LIRE_EEPROM(unsigned char ADDE,unsigned int ADDHL,unsigned char *tab,char r)
{
}   
   
        
             
                
         
  







void Init_I2C(void)
{
}

 









//Ecriture dans l'EEPROM N�0 N�1 N�2 ou N�3 envoyer 000 ou 001......adresse haute et basse et une donn�e sur 8bits

unsigned char ECRIRE_EEPROMBYTE(unsigned char ADDE,unsigned int ADDHL,unsigned char *tab,char g)
{

}





void  SERIALISATION (void)
{

}



 

void identificationINTER(char dequi,char fairequoi ) //F,F  exemple INTER...(6(carte AU),0x00(envoie ERR00))

{ 










 


 




}





void AQUIENVOYER (unsigned char *tabo)
{
}


void REPOSAQUIENVOYER (unsigned char *tabo)
{


	

}
















void reglerhorloge (char *tab)
{



}



void lire_horloge(void)

{





}





void TRANSFORMER_TRAME_MEM_EN_COM (void)

{



}



void PREPARATION_PAGE_AUTO(void)

 

{


unsigned char y,t;
char i;
char tati[6];
unsigned int l;

									




tati[4]=PAGE[4];
tati[3]=PAGE[3];
tati[2]=PAGE[2];
tati[1]=PAGE[1];
tati[0]=PAGE[0];






resetTO();

//for (y=0;y<10;y++)
//{ 


//METTRE LA TABLE EXISTENCE EN TMP PUIS SI r alors regarder codage 1 et remplir le reste de FF si a alors trouver les A et mettre FF ailleurs
l=100*(tati[1]-48)+10*(tati[2]-48)+1*(tati[3]-48);
l=128*l+30000;
LIRE_EEPROM(3,l,&TMP,4); //LIRE DANS EEPROM lexistance du capteur sur la voie

 

LIRE_EEPROM(choix_memoire(&PAGE),calcul_addmemoire(&PAGE),&TRAMEIN,1); //LIRE DANS EEPROM
TRAMEOUT[0]='#';
TRAMEOUT[8]=TRAMEIN[1];
TRAMEOUT[9]=TRAMEIN[2];
TRAMEOUT[10]=TRAMEIN[3];




	for (t=11;t<29;t++) 
	{
	TRAMEOUT[t]=TRAMEIN[t+21];
	}


//DEBIT
TRAMEOUT[29]=TRAMEIN[6]; //TYPE   
TRAMEOUT[30]=TRAMEIN[4]; //CODAGE 0
TRAMEOUT[31]=TRAMEIN[5];  //CODAGE 0	
TRAMEOUT[32]=TRAMEIN[75];  //ETAT
TRAMEOUT[33]=TRAMEIN[76];  //ETAT
if (TMP[3]=='F')
{
TRAMEOUT[32]='F';  //PAS DE CAPTEURS
TRAMEOUT[33]='F';  //PAS DE CAPTEURS

}
if (TMP[2]=='i') //INCIDENT PAT
{
TRAMEOUT[32]='9';  //INCIDENT
TRAMEOUT[33]='9';  
}
if (TMP[2]=='c') //CC PAT
{
TRAMEOUT[32]='8';  //CC
TRAMEOUT[33]='8';  
}
if (TMP[2]=='h') //FUS PAT
{
TRAMEOUT[32]='7';  //FUS
TRAMEOUT[33]='7';  
}



 

//CAPTEURS DE PRESSION
for (t=1;t<17;t++) 
				{
				//1 ier capteur COD1
				
				PAGE[4]='0';
				if (t>=10)
				PAGE[4]='1';
				
				if (t<10)
				PAGE[5]=t+48;
				if (t==10)
				PAGE[5]='0';
				if (t>=10)
				PAGE[5]=(t-10)+48;
				
				LIRE_EEPROM(choix_memoire(&PAGE),calcul_addmemoire(&PAGE),&TRAMEIN,1); //LIRE DANS EEPROM
				TRAMEOUT[34+4*(t-1)]=TRAMEIN[4]; //CODAGE 1
				TRAMEOUT[35+4*(t-1)]=TRAMEIN[5];  //CODAGE 1

					
				TRAMEOUT[36+4*(t-1)]=TRAMEIN[75];  //ETAT 1
				TRAMEOUT[37+4*(t-1)]=TRAMEIN[76];  //ETAT 1
					
					
 			

				
					if (TMP[3+t]=='F')
					{
					TRAMEOUT[36+4*(t-1)]='F';  //PAS DE CAPTEURS
					TRAMEOUT[37+4*(t-1)]='F';  //PAS DE CAPTEURS
					}

					if ((TMP[2]=='r')&&(t==1)) //RESISTIF et codage 1
					{
					TRAMEOUT[36+4*(t-1)]=TRAMEIN[75];  //ETAT 1
					TRAMEOUT[37+4*(t-1)]=TRAMEIN[76];  //ETAT 1
					}


					if (TMP[2]=='i') //INCIDENT PAT
					{
					TRAMEOUT[36+4*(t-1)]='9'; 
					TRAMEOUT[37+4*(t-1)]='9';  //PAS DE CAPTEURS
					}
					if (TMP[2]=='c') //CC PAT
					{
					TRAMEOUT[36+4*(t-1)]='8'; 
					TRAMEOUT[37+4*(t-1)]='8';  //PAS DE CAPTEURS
					}				
					if (TMP[2]=='h') //FUS PAT
					{
					TRAMEOUT[36+4*(t-1)]='7'; 
					TRAMEOUT[37+4*(t-1)]='7';  //PAS DE CAPTEURS
					}
						
						
							
				}




//delay_ms(1);






//delay_ms(1);


}


 


//********************************************************************/

unsigned char putstringI2C( unsigned char *wrptr )
{



unsigned char x;

unsigned int PageSize;
PageSize=128;


  for (x = 0; x < PageSize; x++ ) // transmit data until PageSize  
  {
    if ( SSPCON1bits.SSPM3 )      // if Master transmitter then execute the following
    { // 
	
	 	
		      if ( putcI2C ( *wrptr ) )   // write 1 byte
		{
		        return ( -3 );            // return with write collision error
		}
	

      IdleI2C();                  // test for idle condition
      if ( SSPCON2bits.ACKSTAT )  // test received ack bit state
      {
        return ( -2 );            // bus device responded with  NOT ACK
      }                           // terminateputstringI2C() function
    }
    else                          // else Slave transmitter
    {
      PIR1bits.SSPIF = 0;         // reset SSPIF bit

	
      SSPBUF = *wrptr;            // load SSPBUF with new data
		    

	  SSPCON1bits.CKP = 1;        // release clock line 
      while ( !PIR1bits.SSPIF );  // wait until ninth clock pulse received

      if ( ( !SSPSTATbits.R_W ) && ( !SSPSTATbits.BF ) )// if R/W=0 and BF=0, NOT ACK was received
      {
        return ( -2 );            // terminateputstringI2C() function
      }
    }

 	

  wrptr ++;                       // increment pointer 

  
  	 
 }                               // continue data writes until null character
  return ( 0 );
}




//////////////////////////////////////////////
// fonction de conversion INT => CHAR
void IntToChar(signed int value, char *chaine,int Precision)
{
signed int Mil, Cent, Diz, Unit;
int countTT = 0;

// initialisation des variables
Mil = 0; Cent = 0; Diz = 0; Unit = 0;
/*
if (value < 0) // si c'est un nombre n�gatif
    {
    value = value * (-1);
    *chaine = 45; // signe '-'
    countTT = 1;  // c'est un nombre n�gatif
    }
else // si c'est un nombre positif
    {
    countTT = 1;
    *chaine = 32; // code ASCII du signe espace ' '
    }
*/
// si la valeur n'est pas nulle
if (value != 0)
    {
    if (Precision >= 4) // si l'utilisateur d�sire les milliers
         {
         // conversion des milliers
         Mil = value / 1000;
         if (Mil != 0)
               {
               *(chaine+countTT) = Mil + 48;
               if (*(chaine+countTT) < 48)
                     *(chaine+countTT) = 48;
               if (*(chaine+countTT) > 57)
                     *(chaine+countTT) = 57;
               }
         else
               *(chaine+countTT) = 48;
         countTT++;
         }

    if (Precision >= 3) // si l'utilisateur d�sire les centaines
        {
        // conversion des centaines
        Cent = value - (Mil * 1000);
        Cent = Cent / 100;
        if (Cent != 0)
            {
            *(chaine+countTT) = Cent + 48;
            if (*(chaine+countTT) < 48)
                  *(chaine+countTT) = 48;
            if (*(chaine+countTT) > 57)
                  *(chaine+countTT) = 57;
            }
        else
            *(chaine+countTT) = 48;
            countTT++;
      }

    if (Precision >= 2) // si l'utilisateur d�sire les dizaines
         {
         // conversion des dizaines
         Diz = value - (Mil * 1000) - (Cent * 100);
         Diz = Diz / 10;
         if (Diz != 0)
            {
            *(chaine+countTT) = Diz + 48;
            if (*(chaine+countTT) < 48)
                  *(chaine+countTT) = 48;
            if (*(chaine+countTT) > 57)
                  *(chaine+countTT) = 57;
            }
         else
            *(chaine+countTT) = 48; 
         countTT++;
         }

    // conversion unit�s
    Unit = value - (Mil * 1000) - (Cent * 100) - (Diz * 10);
    *(chaine+countTT) = Unit + 48;
    if (*(chaine+countTT) < 48)       // limites : 0 et 9
            *(chaine+countTT) = 48;
    if (*(chaine+countTT) > 57)
            *(chaine+countTT) = 57;
    }
else // if (value == 0)
    {
    //*(chaine) = 32; // ecrit un espace devant le nombre
    for (Mil=0;Mil<Precision;Mil++)   // inscription de '0' dans toute la chaine
          *(chaine+Mil) = 48;
    }

} // fin de la fonction de conversion INT => CHAR




void MESURE_DES_TENSIONS (void)


{
TRISAbits.RA0=1;
TRISAbits.RA1=1;
TRISAbits.RA2=1;
TRISAbits.RA3=1;

TRISAbits.RA5=1;

TRISFbits.RF0=1;
TRISFbits.RF1=1;
TRISFbits.RF2=1;

ADCON1=0x07; //de AN0 � AN7
ADCON2=0xB8;             //DROITE ET TEMPS CAN



//float VDD1,VDD2,VVD3,VDD4,VDD5,Vtemp,VDD6,VBAT; 

VDD1=2*AVERAGE (0,5,1); //CARTE VDD1 R1
VDD2=2*AVERAGE (1,5,1);//CARTE VDD2 R2
VDD3=2*AVERAGE (2,5,1);//CARTE VDD3 R3
VDD4=2*AVERAGE (3,5,1);//CARTE VDD4 R4
Vtemp=AVERAGE (4,5,1);//temperature
VDD5=2*AVERAGE (5,5,1);//CARTE VDD5 R5
VDD6=2*AVERAGE (6,5,1);//CARTE VDD6 IO
VBAT=2*AVERAGE (7,5,1);//VBAT //4.055176V tension batterie charg�e on observe >4.05 et ca diminue a 4.05

//4.131348 a 48.70mA qd on est en charge
// ca se coupe tt seul descend a 4.05 environ on coupe le mode charge batterie 1ier mosfet
//et ca descend  jusqu'au niveau souhait�(exemple 3.6V) puis on rebranche mosfet1



delay_ms(100);
















}


void EFFACEMENT (unsigned char *tab)
{

}


//////////////////////////////////
// fonction CAN///////////////////
//////////////////////////////////
float CAN (char V)
{

}
 



float AVERAGE (char canal,char avemax,char megmax)

{
}






void CONTROLE_CHARGE_BATT(void)

{

char testchargeon;

TRISJbits.RJ3=0; //STATO
TRISEbits.RE7=1; //STAT

testchargeon=0;
if (CHARGE_ON==1)
testchargeon=1;






//TEST ETAT BATTERIE
if(testchargeon==0)  //SI ON A ALLUME CHARGE 
{
STATO=1; //ON TEST SI CHARGE TERMINEE


					if (STAT==1)
					{
					delay_ms(100); //EN CHARGE
					}
			
					if (STAT==0)
					{
					delay_ms(100); //CHARGE COMPLETE
					CHARGE_ON=1; //ETEINDRE CHARGE 
					//goto CONTROLE;
					}
}
 

if(testchargeon==1)  //SI ON A ETEINT CHARGE 
{

if (VBAT<=3.3) //ON REGARDE SI BATTERIE FAIBLE
CHARGE_ON=0; //si le cas on rallume charge

}



// TEST TEMPERATURE DANGER

STATO=0;

if(testchargeon==0)  //SI CHARGE ALLUMEE 
{					
		if (STAT==1) //TEMPERATURE OK
		{

			delay_ms(100);  	
			CHARGE_ON=0;  //ALLUMER CHARGE 

		}
		if (STAT==0) //PROBLEME TEMPERATURE test valable si CHARGE_ON=0 (circuit charge aliment�) sinon ne pas prendre en compte
		{
		

		
		CHARGE_ON=1;  //ON ETEINT MEME SI C PROTEGE PAR LE CIRCUIT IL FAUDRAIT JUSTE RAJOUTER UNE INFORMATION POUR OPERATEUR A VOIR !!!!!!
				

		} 
}







			


}


void INTERROGER_TOUT_LES_CAPTEURS(char t)

{
unsigned int i,y,ADD;
unsigned char R;
//LIRE_EEPROM(7,ADD,&TRAMEOUT,0);








				for (i=0;i<13000;i=i+128)
				
				{
				if (t==0)
				LIRE_EEPROM(7,i,&TMP,0); //LISTE COMPLETE			
				if (t==1)
				LIRE_EEPROM(5,i,&TMP,0); //LISTE AVEC GRAND CHANGEMENT
				delay_ms(6); //IMPORTANT		
				if (TMP[98]=='F')
				i=15000;    //FIN D'INTERROGATION


				for (y=0;y<17;y++)
				{
				DEMANDE_INFOS_CAPTEURS[0]=TMP[13+5*y];
				DEMANDE_INFOS_CAPTEURS[1]=TMP[14+5*y];
				DEMANDE_INFOS_CAPTEURS[2]=TMP[15+5*y];
				DEMANDE_INFOS_CAPTEURS[3]=TMP[16+5*y];
				DEMANDE_INFOS_CAPTEURS[4]=TMP[17+5*y];

				if (DEMANDE_INFOS_CAPTEURS[0]=='x')
				goto finliste;				

				ADD=100*(DEMANDE_INFOS_CAPTEURS[0]-48);
				ADD=ADD+10*(DEMANDE_INFOS_CAPTEURS[1]-48);  // ON REGARDE LA VOIE
				ADD=ADD+(DEMANDE_INFOS_CAPTEURS[2]-48);
				

				R=5;
				if (ADD<79)
				R=4;


				if (ADD<59)
				R=3;
				
				if (ADD<39)
				R=2;
				
				if (ADD<19)
				R=1;

			



				ENVOIE_DES_TRAMES (R,276,&DEMANDE_INFOS_CAPTEURS) ;
				}





				}

finliste: 

delay_ms(100);

}





void EFFACER_LISTE(char t)

{
unsigned int i;

for (i=0;i<125;i++)
{
TRAMEIN[i]=0xFF;

}


for (i=0;i<13000;i=i+128)

{
if (t==0)
ECRIRE_EEPROMBYTE(7,i,&TRAMEIN,0); //LISTE AVEC CHANGEMENT IMPORTANT
if (t==1)
ECRIRE_EEPROMBYTE(5,i,&TRAMEIN,0);//LISTE COMPLETE

delay_ms(6); //IMPORTANT
}

//LIRE_EEPROM(7,128,&TRAMEOUT,0);
//delay_ms(100);

}










void METTRE_EN_MEMOIRE_LISTE(unsigned char *tab,char t)

{



}




void GROUPE_EN_MEMOIRE(unsigned char *tab,char lui) 

{



}







unsigned char envoyerHORLOGE_i2c(unsigned char NumeroH,unsigned char ADDH, unsigned char dataH0)
{

 				
}








signed char litHORLOGE_i2c(unsigned char NumeroH,unsigned char AddH) 
{     





} 









unsigned char ECRIRE_HORLOGE(unsigned char zone,unsigned char Time)
{

}





unsigned char LIRE_HORLOGE(unsigned char zone)
{

}


void creer_trame_horloge (void)
{




}




void entrer_trame_horloge (void)
{




}




char trouver_carte_suivantvoie(char *tab)

{



unsigned int i;
unsigned int t;





t=0;
i=tab[2]-48;
t=t+1*i;
i=tab[1]-48;
t=t+10*i;
i=tab[0]-48;
t=t+100*i;

 



if (t<=100)
i=5;
if (t<=80)
i=4;
if (t<=60)
i=3;
if (t<=40)
i=2;
if (t<=20)
i=1;



return (i);

}





void TEMPO(char seconde)
{


}











































///////////////////////////////////////////////
//write one block*



unsigned char SD_WriteSingleBlock(unsigned long sector)
{

}



////////////////////////////////////////////////
//read one block.
unsigned char SD_ReadSingleBlock(unsigned long sector)
{

}





 




unsigned char SD_SendCommand(char CMD, unsigned long sct, char CRC)
{

}





///////////////////////////////////////////////
//write one byte.  // 8fronts d'horloge  
unsigned char SPI_WriteByte(unsigned char val)
{

}








    ////////////////////////////////////////////////
    //SD reset.
    unsigned char sd_reset()
    {
} 












// permet de faire une pause de x �s
void delay_us(char tempo)
{
char i;

for (i=0;i<tempo;i++)
  {
  //__no_operation();     // 5 pauses pour 1�s
  //__no_operation();
  //__no_operation();
  //__no_operation();
  //__no_operation();
  }
}





void SAV_EXISTENCE(char *tt)
{

}


 

void RECHERCHE_DONNEES_GROUPE(unsigned char ici)


{



}








char VALIDER_CAPTEUR_CM(void)
{





}



void IDENTIF_TYPE_CARTE (void)


{



}



void TROUVER_ROBINET(void)
{ 

}





//void voir_si_voie_existe(void)
//{
//VOIECOD[0]='0';
//VOIECOD[1]='0';
//VOIECOD[2]='1';

//CHERCHER_DONNNEES_CABLES(&VOIECOD,111); //CAPTEURS UN A UN
//if (capt_exist==1)
//{

//}

//}
 
 


void REMISE_EN_FORME_PAGE(void)

{








}








//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////// v�rification du CRC des trames recue ///////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void CRC_verif(void){


}




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// calcul du CRC pour /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CRC_POLY 0x01021 //0x01021
#define CRC_START 0xFFFF
unsigned int CRC16(char far *txt, unsigned  char lg_txt)
{ 



}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////// CONVERSION UNSIGNED INT TO CHAR///////////////////////////////////////////////////////////////////////////////////////// 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void IntToChar5(unsigned int value, char *chaine,char Precision)
{


} // fin de la fonction de conversion INT => CHAR




char EFFACER_CAPTEUR_SUR_CM(char gh){}


void intervertirOUT_IN(void){}
void CHERCHER_DONNNEES_CABLES(char *g,char j){}

void TRAMERECEPTIONRS485DONNEES (unsigned char tout){}


void TRAMEENVOIRS485DONNEES (void){}



void RECEVOIR_PICSD(char t)
{

unsigned char oop,z,recept[12];

oop=-1;	 
 
  
///////////////////RECEVOIR POUR METTRE EN SD

while (!SSP1STATbits.BF);
oop=spiRead();    // activation
spiWrite('-'); //a
oop=spiRead();    // activation
spiWrite('-'); //a

recept[0]=spiRead();    // j
spiWrite('-'); //a
recept[1]=spiRead();    //j
spiWrite('-'); //a
recept[2]=spiRead();    // m
spiWrite('-'); //a
recept[3]=spiRead();    //m
spiWrite('-'); //a
recept[4]=spiRead();    // a
spiWrite('-'); //a
recept[5]=spiRead();    //a
spiWrite('-'); //a
recept[6]=spiRead();    // h
spiWrite('-'); //a
recept[7]=spiRead();    //h
spiWrite('-'); //a
recept[8]=spiRead();    //m
spiWrite('-'); //a
recept[9]=spiRead();    //m
spiWrite('-'); //a
recept[10]=spiRead();    //ordre
spiWrite('-'); //a
recept[11]=spiRead();    // ordre
spiWrite('-'); //a


HORLOGE[0]=10*(recept[4]-48)+(recept[5]-48);
HORLOGE[1]=10*(recept[2]-48)+(recept[3]-48);
HORLOGE[2]=10*(recept[0]-48)+(recept[1]-48);
HORLOGE[3]=10*(recept[6]-48)+(recept[7]-48);
HORLOGE[4]=10*(recept[8]-48)+(recept[9]-48);
HORLOGE[5]=0; //secondes
//	if (SetClockVars (2000+HORLOGE[0],HORLOGE[1],HORLOGE[2],HORLOGE[3], HORLOGE[4],HORLOGE[5]))          ////r�glage de l'heure 






oop=-1;
 


do
{
oop++;
//RECEVOIR_PICSD(&TRAMEIN);


if (recept[10]=='R')
{
TRAMEIN[oop]=spiRead();    // Read the received data
spiWrite(TRAMEOUT[oop]); // ON ENVOIE A MICRO1
z=TRAMEOUT[oop];
}
else
{
TRAMEIN[oop]=spiRead();    // Read the received data
spiWrite('!'); //ON RECOIT DE MICRO1
z=TRAMEIN[oop];
}


}while (z!='*');

if (recept[10]=='R') //ON ENVOIE L'ETOILE DERNIER CARAC
{
TRAMEIN[oop]=spiRead();    // Read the received data
spiWrite(TRAMEOUT[oop]); // ON ENVOIE A MICRO1
z=TRAMEOUT[oop];

}



//////////////////////// FIN  DE RECEPTION POUR SD

}



void excel (void)
{

TMP[0]=TRAMEIN[0];

TMP[1]=';';

TMP[2]=TRAMEIN[1]; //VOIE
TMP[3]=TRAMEIN[2];
TMP[4]=TRAMEIN[3];

TMP[5]=';';

TMP[6]=TRAMEIN[4]; //CODAGE
TMP[7]=TRAMEIN[5];

TMP[8]=';';

TMP[9]=TRAMEIN[6]; //TYPE

TMP[10]=';';



TMP[11]=TRAMEIN[7]; //CMOD
TMP[12]=TRAMEIN[8];
TMP[13]=TRAMEIN[9];
TMP[14]=TRAMEIN[10];


TMP[15]=';';



TMP[16]=TRAMEIN[11]; //Cconso
TMP[17]=TRAMEIN[12];
TMP[18]=TRAMEIN[13];
TMP[19]=TRAMEIN[14];


TMP[20]=';';



TMP[21]=TRAMEIN[15]; //Crepos
TMP[22]=TRAMEIN[16];
TMP[23]=TRAMEIN[17];
TMP[24]=TRAMEIN[18];


TMP[25]=';';



TMP[26]=TRAMEIN[19]; //constitution
TMP[27]=TRAMEIN[20];
TMP[28]=TRAMEIN[21];
TMP[29]=TRAMEIN[22];
TMP[30]=TRAMEIN[23]; 
TMP[31]=TRAMEIN[24];
TMP[32]=TRAMEIN[25];
TMP[33]=TRAMEIN[26];
TMP[34]=TRAMEIN[27]; 
TMP[35]=TRAMEIN[28];
TMP[36]=TRAMEIN[29];
TMP[37]=TRAMEIN[30];
TMP[38]=TRAMEIN[31];



TMP[39]=';';




TMP[40]=TRAMEIN[32]; //ROBINET
TMP[41]=TRAMEIN[33];
TMP[42]=TRAMEIN[34];
TMP[43]=TRAMEIN[35];



TMP[44]=';';



TMP[45]=TRAMEIN[36]; //COMMENTAIRE
TMP[46]=TRAMEIN[37];
TMP[47]=TRAMEIN[38];
TMP[48]=TRAMEIN[39];
TMP[49]=TRAMEIN[40];
TMP[50]=TRAMEIN[41];
TMP[51]=TRAMEIN[42];
TMP[52]=TRAMEIN[43]; 
TMP[53]=TRAMEIN[44];
TMP[54]=TRAMEIN[45];
TMP[55]=TRAMEIN[46];
TMP[56]=TRAMEIN[47];
TMP[57]=TRAMEIN[48];
TMP[58]=TRAMEIN[49];





TMP[59]=';';



TMP[60]=TRAMEIN[50]; //VOIE
TMP[61]=TRAMEIN[51];
TMP[62]=TRAMEIN[52];
 

TMP[63]=';';
 


TMP[64]=TRAMEIN[53]; //date j
TMP[65]=TRAMEIN[54];

TMP[66]=';';



TMP[67]=TRAMEIN[55]; //date M
TMP[68]=TRAMEIN[56];

TMP[69]=';';



TMP[70]=TRAMEIN[57]; //date h
TMP[71]=TRAMEIN[58];

TMP[72]=';';



TMP[73]=TRAMEIN[59]; //VOIE m
TMP[74]=TRAMEIN[60]; //VOIE



TMP[75]=';';




TMP[76]=TRAMEIN[61]; // ?


TMP[77]=';';



TMP[78]=TRAMEIN[62]; //CODE
TMP[79]=TRAMEIN[63];

TMP[80]=';';



TMP[81]=TRAMEIN[64]; //POS
TMP[82]=TRAMEIN[65];


TMP[83]=';';

TMP[84]=TRAMEIN[66]; //????
TMP[85]=TRAMEIN[67];
TMP[86]=TRAMEIN[68]; 
TMP[87]=TRAMEIN[69];


TMP[88]=';';


TMP[89]=TRAMEIN[70]; //DISTANCE
TMP[90]=TRAMEIN[71];
TMP[91]=TRAMEIN[72]; 
TMP[92]=TRAMEIN[73];
TMP[93]=TRAMEIN[74]; 


TMP[94]=';';

TMP[95]=TRAMEIN[75];
TMP[96]=TRAMEIN[76]; //ETAT

TMP[97]=';';

TMP[98]=TRAMEIN[77]; //VALEUR
TMP[99]=TRAMEIN[78];
TMP[100]=TRAMEIN[79];
TMP[101]=TRAMEIN[80];

TMP[102]=';';

TMP[103]=TRAMEIN[81]; //SEUIL
TMP[104]=TRAMEIN[82];
TMP[105]=TRAMEIN[83];
TMP[106]=TRAMEIN[84];

TMP[107]=';';

TMP[108]=TRAMEIN[85]; //CRC
TMP[109]=TRAMEIN[86];
TMP[110]=TRAMEIN[87];
TMP[111]=TRAMEIN[88];
TMP[112]=TRAMEIN[89];

TMP[113]=';';

TMP[114]=TRAMEIN[90]; //*

TMP[115]=0x0D; //SAUT DE LIGNE
TMP[116]=0x0A;

}


void DESexcel (void)
{

TRAMEOUT[0]		=	TMP[0]	;	
					
					
					
TRAMEOUT[1]		=	TMP[2]	;	 //VOIE
TRAMEOUT[2]		=	TMP[3]	;	
TRAMEOUT[3]		=	TMP[4]	;	
					
					
					
TRAMEOUT[4]		=	TMP[6]	;	 //CODAGE
TRAMEOUT[5]		=	TMP[7]	;	
					
					
					
TRAMEOUT[6]		=	TMP[9]	;	 //TYPE
					
					
					
					
					
TRAMEOUT[7]		=	TMP[11]	;	 //CMOD
TRAMEOUT[8]		=	TMP[12]	;	
TRAMEOUT[9]		=	TMP[13]	;	
TRAMEOUT[10]		=	TMP[14]	;	
					
					
					
					
					
					
TRAMEOUT[11]		=	TMP[16]	;	 //Cconso
TRAMEOUT[12]		=	TMP[17]	;	
TRAMEOUT[13]		=	TMP[18]	;	
TRAMEOUT[14]		=	TMP[19]	;	
					
					
					
					
					
					
TRAMEOUT[15]		=	TMP[21]	;	 //Crepos
TRAMEOUT[16]		=	TMP[22]	;	
TRAMEOUT[17]		=	TMP[23]	;	
TRAMEOUT[18]		=	TMP[24]	;	
					
					
					
					
					
					
TRAMEOUT[19]		=	TMP[26]	;	 //constitution
TRAMEOUT[20]		=	TMP[27]	;	
TRAMEOUT[21]		=	TMP[28]	;	
TRAMEOUT[22]		=	TMP[29]	;	
TRAMEOUT[23]		=	TMP[30]	;	 
TRAMEOUT[24]		=	TMP[31]	;	
TRAMEOUT[25]		=	TMP[32]	;	
TRAMEOUT[26]		=	TMP[33]	;	
TRAMEOUT[27]		=	TMP[34]	;	 
TRAMEOUT[28]		=	TMP[35]	;	
TRAMEOUT[29]		=	TMP[36]	;	
TRAMEOUT[30]		=	TMP[37]	;	
TRAMEOUT[31]		=	TMP[38]	;	
					
					
					
					
					
					
					
					
TRAMEOUT[32]		=	TMP[40]	;	 //ROBINET
TRAMEOUT[33]		=	TMP[41]	;	
TRAMEOUT[34]		=	TMP[42]	;	
TRAMEOUT[35]		=	TMP[43]	;	
					
					
					
					
					
					
					
TRAMEOUT[36]		=	TMP[45]	;	 //COMMENTAIRE
TRAMEOUT[37]		=	TMP[46]	;	
TRAMEOUT[38]		=	TMP[47]	;	
TRAMEOUT[39]		=	TMP[48]	;	
TRAMEOUT[40]		=	TMP[49]	;	
TRAMEOUT[41]		=	TMP[50]	;	
TRAMEOUT[42]		=	TMP[51]	;	
TRAMEOUT[43]		=	TMP[52]	;	 
TRAMEOUT[44]		=	TMP[53]	;	
TRAMEOUT[45]		=	TMP[54]	;	
TRAMEOUT[46]		=	TMP[55]	;	
TRAMEOUT[47]		=	TMP[56]	;	
TRAMEOUT[48]		=	TMP[57]	;	
TRAMEOUT[49]		=	TMP[58]	;	
					
					
					
					
					
					
					
					
					
TRAMEOUT[50]		=	TMP[60]	;	 //VOIE
TRAMEOUT[51]		=	TMP[61]	;	
TRAMEOUT[52]		=	TMP[62]	;	
					
					
					
					
					
					
TRAMEOUT[53]		=	TMP[64]	;	 //date j
TRAMEOUT[54]		=	TMP[65]	;	
					
					
					
					
					
TRAMEOUT[55]		=	TMP[67]	;	 //date M
TRAMEOUT[56]		=	TMP[68]	;	
					
					
					
					
					
TRAMEOUT[57]		=	TMP[70]	;	 //date h
TRAMEOUT[58]		=	TMP[71]	;	
					
					
					
					
					
TRAMEOUT[59]		=	TMP[73]	;	 //VOIE m
TRAMEOUT[60]		=	TMP[74]	;	 //VOIE
					
					
					
					
					
					
					
					
TRAMEOUT[61]		=	TMP[76]	;	 // ?
					
					
					
					
					
					
TRAMEOUT[62]		=	TMP[78]	;	 //CODE
TRAMEOUT[63]		=	TMP[79]	;	
					
					
					
					
					
TRAMEOUT[64]		=	TMP[81]	;	 //POS
TRAMEOUT[65]		=	TMP[82]	;	
					
					
					
					
TRAMEOUT[66]		=	TMP[84]	;	 //????
TRAMEOUT[67]		=	TMP[85]	;	
TRAMEOUT[68]		=	TMP[86]	;	 
TRAMEOUT[69]		=	TMP[87]	;	
					
					
					
					
					
TRAMEOUT[70]		=	TMP[89]	;	 //DISTANCE
TRAMEOUT[71]		=	TMP[90]	;	
TRAMEOUT[72]		=	TMP[91]	;	 
TRAMEOUT[73]		=	TMP[92]	;	
TRAMEOUT[74]		=	TMP[93]	;	 
					
					
					
					
TRAMEOUT[75]		=	TMP[95]	;	
TRAMEOUT[76]		=	TMP[96]	;	 //ETAT
					
					
					
TRAMEOUT[77]		=	TMP[98]	;	 //VALEUR
TRAMEOUT[78]		=	TMP[99]	;	
TRAMEOUT[79]		=	TMP[100]	;	
TRAMEOUT[80]		=	TMP[101]	;	
					
					
					
TRAMEOUT[81]		=	TMP[103]	;	 //SEUIL
TRAMEOUT[82]		=	TMP[104]	;	
TRAMEOUT[83]		=	TMP[105]	;	
TRAMEOUT[84]		=	TMP[106]	;	
					
					
					
TRAMEOUT[85]		=	TMP[108]	;	 //CRC
TRAMEOUT[86]		=	TMP[109]	;	
TRAMEOUT[87]		=	TMP[110]	;	
TRAMEOUT[88]		=	TMP[111]	;	
TRAMEOUT[89]		=	TMP[112]	;	
					
					
					
TRAMEOUT[90]		=	TMP[114]	;	 //*

 
}




 
















