#include <HC595.h>
#include <delay.h>

unsigned char Num_voie;
unsigned char registre1;   //registres des HC595 pour commutation des relais voies et alarme et test fusible
unsigned char registre2;
unsigned char registre3;


// Programmation des HC595 pour les relais des voies de mesure et de l'alarme c�ble

//unsigned char Data;

void Prog_HC595 (unsigned char rel_voies_impaires,unsigned char rel_voies_paires,unsigned char alarme_ca,unsigned char test_fus)
	{
		unsigned char i;
		unsigned char j;
		unsigned char val;
//		unsigned char test;
				// reset registres
				registre1 = 0b00000000; 	// relais 1 � 8
				registre2 = 0b00000000;		// relais 9 � 16
				registre3 = 0b00000000;		// relais 17 � 20 + alarme c�ble + test fusibles voies

//		registre1=0xFF;
//	registre2=0xFF;
//	 registre3=0xFF;
//goto y;
		// Choix Relais		
	if (rel_voies_impaires > 0)
		{
			switch(rel_voies_impaires)
			{
			case 1:
				registre1 |= 0b00000001; 	// relais 1
			break;
			case 3:
				registre1 |= 0b00000100; 	// relais 3
			break;
			case 5:
				registre1 |= 0b00010000; 	// relais 5
			break;
			case 7:
				registre1 |= 0b01000000; 	// relais 7
			break;
			case 9:
				registre2 |= 0b00000001; 	// relais 9
			break;
			case 11:
				registre2 |= 0b00000100; 	// relais 11
			break;
			case 13:
				registre2 |= 0b00010000; 	// relais 13
			break;
			case 15:
				registre2 |= 0b01000000; 	// relais 15
			break;
			case 17:
				registre3 |= 0b00000001; 	// relais 17
			break;
			case 19:
				registre3 |= 0b00000100; 	// relais 19
			break;
			 }
		}
		// Choix Relais		
	if (rel_voies_paires > 0)
		{
			switch(rel_voies_paires)
			{
			case 2:
				registre1 |= 0b00000010; 	// relais 2
			break;
			case 4:
				registre1 |= 0b00001000; 	// relais 4
			break;
			case 6:
				registre1 |= 0b00100000; 	// relais 6
			break;
			case 8:
				registre1 |= 0b10000000; 	// relais 8
			break;
			case 10:
				registre2 |= 0b00000010; 	// relais 10
			break;
			case 12:
				registre2 |= 0b00001000; 	// relais 12
			break;
			case 14:
				registre2 |= 0b00100000; 	// relais 14
			break;
			case 16:
				registre2 |= 0b10000000; 	// relais 16
			break;
			case 18:
				registre3 |= 0b00000010; 	// relais 18
			break;
			case 20:
				registre3 |= 0b00001000; 	// relais 20
			break;
			 } 
		}
 //test si alarme c�ble . si oui mofification du registre 3
				if (alarme_ca){
				registre3|= 0b00010000;   //"ou logique"  pour si alarme c�ble
				}	
// pour TEST si fusion fusible voies . si oui mofification du registre 3
				if (test_fus){
				registre3|= 0b00100000;   //"ou logique"  pour si test fusion fusible
				}
//				Prog_HC595(registre1,registre2,registre3);


y:

//		test = 0;
		j = 24;   // correspond aux nombres de pins ou coups d'horloge
	
	
		Rst_HC595 = 0;  // reset les "shift regitrers"
		
		//Clk_HC595 = 0;
		//delay_ms(1);
		//Clk_HC595 = 1;
		//delay_ms(1);
		//Clk_HC595 = 0;
		//delay_ms(1);
		//Clk_HC595 = 1;
		Rst_HC595 = 1;  // reset les "shift regitrers"
		
		Rck_HC595 = 0;
		delay_qms(1);
		Rck_HC595 = 1;

//PORTEbits.RE6=1;
		//�criture donn�es
	
//j=1;

		for (i=j;i>0;i--)    // 23= Nb de pins-1
		{
		//delay_ms(1);
		//DELAY_MS (1);
	//	Clk_HC595 =1;
	//	Data_HC595 = val;
		//Data_HC595 = val;
	//	Clk_HC595 = 0;
	//	Rck_HC595 = 0;
		//delay_ms(1);
	//	Rck_HC595 = 1;
		//PORTEbits.RE4=0;	
		if (i >16)
			{
			val = registre3 &0x80;  
			registre3<<=1; //relais de 17 � 24
			}

		if (i> 8 && i<17)
			{
			val = registre2 &0x80;  
			registre2<<=1; //relais de 9 � 16
			}
		if (i<9)
			{
			val = registre1 &0x80;  
			registre1<<=1; //relais de 1 � 8
			}
		
		if (val==0x80)
		val=0xFF;	
	
		//	delay_ms(1);
		Data_HC595 = val;
		Clk_HC595 = 1;
		Clk_HC595 = 0;

		//Rck_HC595 = 1;
		//
		//Rck_HC595 = 0;
	//	delay_qms(1);

		}	 

		Rck_HC595 = 0;
		//delay_ms(1);
		Rck_HC595 = 1;
		Rst_HC595 = 1;  // reset les "shift regitrers" 
		
	}










