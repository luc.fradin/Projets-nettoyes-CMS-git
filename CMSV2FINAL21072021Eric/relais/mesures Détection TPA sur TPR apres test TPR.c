
#include <p18f8723.h>					/* Defs PIC 18F8723 */
#include <mesures.h>
#include <GLCD.h>						/* Gestion GLCD */
#include <inituc.h>
#include <delay.h>						/* Defs DELAY functions */
#include <ftoa.h>						/* Adapt Chaine de caract�re */
#include <ecrans.h>
#include <rtc.h>
#include <analog.h>
#include <delays.h>
#include <math.h>


extern char DATAtoLCD[30];

//const rom unsigned int Table_R[20];
//const rom unsigned int TableFreq[20];

//extern unsigned int V_Batt;
//extern float V_Batt_Calc;
//extern float V_Batt_Aff;
//extern float V_48_Calc;
//extern float V_Ligne_AM_Calc;
unsigned char Nb_Capteurs_max;   //16 � voir 
unsigned char Nb_TPA_ok; // indique le nb de capteurs qui ont r�pondus lors d'un interrogation  
unsigned char mes_groupe; // pour indiquer (dans la boucle de mesure des pcpg) une mesure du groupe de pressu 
unsigned char mes_debit; // pour indiquer (dans la boucle de mesure des pcpg) une mesure du d�bitm�tre 
extern unsigned char Pres_Debit;  // pour fin de mesure  d�bitm�tre en 0	

extern unsigned char Num_Ligne;		//num�ro ligne en cours
extern unsigned char Der_Ligne;   //derniere ligne interrog�e
extern unsigned char I_Conso;
extern unsigned char I_Modul;
extern unsigned int I_Repos;
unsigned int I_Repos1;
unsigned int I_Repos2;
unsigned int I_Repos3;


extern float V_Ligne_AM_Calc;  // TENSION SUR CAPTEUR
extern float V_Resistif;       // TENSION AUX BORNES DE LA RESISTANCE DE MESURE
extern float V_48_Calc;

extern unsigned char Tab_Code[17];
extern unsigned int Tab_Pression[17];
extern unsigned int Tab_I_Repos[17];			  
extern unsigned char Tab_I_Modul[17];		
extern unsigned int Tab_I_Conso[17];			
extern unsigned char Tab_Etat[17];

extern unsigned char Boucle_TP;


extern unsigned char F_ecran_adres;	 	// flag pour affichage �cran TPA r�duit(0) ou complet(1)
//extern unsigned char F_aff_sauve_tpa; 	// flag pour m�mo affichage: Sauvegarde ? (sauvegarde des mesures pour TP adressables)
//extern unsigned char F_sauve_mes_tpa; 	// flag pour m�mo sauvegarde valid�e des mesures des tp adressables
//extern unsigned char F_memo_back_light; 	// flag pour m�mo sauvegarde position back_light				
//extern unsigned char F_mode_scrut; 		// flag pour signaler d�but et fin de scrutation
//extern unsigned char F_it_chenillard; 	// flag pour chenillard
//extern unsigned char F_sup3mes_tpa;	 	// flag pour indiquer si ecran complet ou r�duit et > � 3 mesures

unsigned int Timer1L;
unsigned int Timer1H;

float tempF1;
float tempF2;
float tempF3;
float tempF4;
float Cumul_F;
unsigned int Freq1;
unsigned int Freq2;
unsigned int Freq3;
unsigned int Freq4;
unsigned int Freq;
unsigned int Freq_R;

unsigned char fin_mesures;// si 1 indique que l'interrogation est termin�e (c'est pour valider l'analyse des courants pour indiquer les d�fauts et le affichages inverses



extern unsigned int t1ov_cnt;
extern unsigned int cpt_int2;
extern unsigned char val1TP;

#define i_conso_min 20 		// = 2.0mA =>seuil mini de d�tection d'un courant capteur (i_conso est x10)
#define i_repos_min 150 	// = 150�A =>seuil pout indiquer qu'un seul capteur sur la ligne 

#define V_Ligne_Max	5	// tension max acceptable sur la ligne d'interrogation capteurs
#define max_instable 3  // valeur max d'�cart fr�quence (Hz) pour signaler une instabilit� du capteur



/* Resume allocation of romdata into the default section */
#pragma		code	USER_ROM   // retour � la zone de code*/

void Boucle_Mesure_PCPG	()			
{
int tpa;
int irep;  //indicateur r�ponse d'un tpa, seulement dans le cas ou le courant de repos est < 150�A (pour stopper la sructation d�s que mesur�, si un seul tpa sur la ligne)
int nb;	// variable compteur
unsigned int I_Conso1;
unsigned int I_Conso2;

				niveau_batt();
				Freq  = 0x0000;
				Freq1 = 0x0000;	//
				Freq2 = 0x0000;	//
				Freq3 = 0x0000;	//
				Freq4 = 0x0000;	//
				Cumul_F=0;
				I_Conso1=0x0000;
				I_Conso2=0x0000;
				Nb_TPA_ok=0;  // indique le nb de capteurs trouv�s en interrogation
				fin_mesures=0; // si 1 indique que l'interrogation est termin�e (c'est pour valider l'analyse des courants pour indiquer les d�fauts et le affichages inverses
				Nb_Capteurs_max = 17;   //16 tp + 1 d�bitm�tre en 0
				if (mes_groupe == 1) // si mesure groupe
						{
				  Nb_Capteurs_max = 11;   //10 tp + position 0
						}
				if (mes_debit == 1) // si mesure debit
						{
				  Nb_Capteurs_max = 1;   //0 tp + position 0
						}


				tempF1=0x0000;
				tempF2=0x0000;
				tempF3=0x0000;
				tempF4=0x0000;

				CPT_ON = 0;		//   <======== d�valide ligne de mesure � 48v par s�curit�
				tpa=0;
				irep=1;  // tant que irep = 1 => scrutation

				IMP_TRANS=1;    //*impulsion sur relais pour mode transtest
				DELAY_MS (50);	//*
				IMP_TRANS=0;	//*

//			b_ModeScrut=1;
				// Par s�curit�, contr�le de la tension ligne avant interrogation
			//	Mesure_Ligne_AM();  // s�curit� pr�sence tension sur ligne , � voir si n�cessaire
				
				// ajouter test du 48V

				// synchronisation de l'horloge de 1hz du ci clock DS1307 pour cadencer la lecture des capteurs ttes les 2sec
/*
				set_time(0x07,0x00);  // adresse= 0x07 valeur 0x00 pour d�valider SQR/OUT  � 1Hz
			DELAY_125MS(1);
				set_time(0x07,0x10);  // adresse= 0x07 valeur 0x10 pour valider SQR/OUT  � 1Hz
				HT_ON = 1;				 // mise en route du convertisseur 48V 
 			DELAY_125MS(1);		//  
*/			
				HT_ON = 1;				 // mise en route du convertisseur 48V 

				while(Synchro1s); //attend passage � 0 de RB3, boucle tant que la condition n'est pas arriv�e Synchro1s=PORTBbits.RB3
				while(!Synchro1s); //attend passage � 1 de RB3, boucle tant que la condition n'est pas arriv�e Synchro1s=PORTBbits.RB3
//===>	0Ms
				DECH_LIGN = 0;   // lib�re la d�charge ligne
				Led_jaune = 1;          //1
				while (touche_BP4 == 0);

				Tab_Code[tpa] = tpa;  // Tab_Code = 0
//				tpa=1;
				Num_Ligne = 0;		// num�ro de la ligne de mesure pour affichage
	
				CPT_ON = 1;		//   <======== valide ligne de mesure � 48v

/*  partie supprim�e POUR LECTURE EN ZERO (DEBITMETRE)

			DELAY_SEC (1); //==>  d�lai de 1+0.5sec pour masquer le 1er front montant au bout de 1sec de l'horloge DS1307
	 		DELAY_500MS (1);
 			DELAY_MS (125);		//  pour masquer 1er front down
*/
///				Mesure_I_Repos();
///				if (I_Repos<150){
///				Nb_Capteurs_max = 1; // Nb capteurs + 1 
///				ir = 1;
///				}
//				else{
//				Nb_Capteurs_max = (I_Repos/75) + 1;  // � voir si utile et si peut fonctionner
//				}		

//				while(Synchro1s==0); //attend passage � 1 de RB3, boucle tant que la condition n'est pas arriv�e Synchro1s=PORTBbits.RB3
//===>	?sec


	for (tpa=0; tpa < Nb_Capteurs_max ;tpa++)  //tpa=0 POUR LECTURE EN ZERO (DEBITMETRE)
//	for (tpa=1; tpa < Nb_Capteurs_max ;tpa++)
			{
				Freq = 0;
				Freq1 =0;
				Freq2 =0;
				Freq3 =0;
				Freq4 =0;
				tempF1=0;
				tempF2=0;
				tempF3=0;
				tempF4=0;
				Cumul_F=0;

// ====> 0mS
//				niveau_batt();
		DELAY_125MS (2);		//Tempo1  250ms!!!!!!!!!!!!!!!!!!!!
				Num_Ligne = tpa;		// num�ro de la ligne de mesure pour affichage				
				Tab_Code[tpa] = tpa;	  // � voir si � conserver ainsi que Num_Capteur

		DELAY_SW_250MS ();			 // TEMPO2  la Tempo2 se termine � 	while(!(INTCONbits.TMR0IF));
				Delay10KTCYx(100);	 //<== voir s'il faut plus de tempo  pour mesurer un peu plus tard la conso  
				Mesure_I_Conso();
				I_Conso1=I_Conso;
				Delay10KTCYx(10);
				Mesure_I_Conso();
				I_Conso2=I_Conso;
				I_Conso1+=I_Conso2; // pour la moyenne
				Mesure_I_Modul();

				if (I_Conso > i_conso_min )	// soit I_Conso mini >= 2.0mA pour led verte => pr�sence d'un capteur   (20 pour 2.0mA)  (i_conso est x10)
					{
					Led_verte = 1;	// pour indiquer une r�ponse capteur ou d�bitm�tre
					}

		while(!(INTCONbits.TMR0IF)); // FIN de TEMPO2      wait until TMR0 rolls over 

// ====> 500mS

			DELAY_125MS (1);		// 125ms
		//####### lecture Fr�quence de r�ponse des capteurs = 4 fois 2 mesures de fr�q sur 20 p�riodes #######
				for(nb=0;nb<2;nb++){
				Mes_freq_v2();			// mesure de la fr�quence 	
				Freq1 += Freq;	//m�morise 1�re valeur de fr�quence
				}
			DELAY_125MS (1);		// 125ms
				for(nb=0;nb<2;nb++){
				Mes_freq_v2();			// mesure de la fr�quence 	
				Freq2 += Freq;	//m�morise 2�me valeur de fr�quence
				}
			DELAY_125MS (1);		// 125ms
				for(nb=0;nb<2;nb++){
				Mes_freq_v2();			// mesure de la fr�quence 	
				Freq3 += Freq;	//m�morise 3�me valeur de fr�quence
				}
			DELAY_125MS (1);		// 125ms   
				for(nb=0;nb<2;nb++){
				Mes_freq_v2();			// mesure de la fr�quence 	
				Freq4 += Freq;	//m�morise 4�me valeur de fr�quence
				}
//====> 500 + 375mS => 875mS

//			DELAY_125MS (1);		// modif= 375ms
//			DELAY_MS (20);		// 80ms  

				if (I_Conso > i_conso_min)	// soit I_Conso mini >= 2.0mA pour led verte => pr�sence capteur   (i_conso est x10)
					{
				Instable(tpa);
				Freq = (int)(Cumul_F/8);  // 80 = 4 x 2 mesures
    			Nb_TPA_ok++;  // indique le nb de capteurs trouv�s en interrogation@@@@
					}
					else
					{
				Freq = 0;
					}
				Tab_Pression[tpa] = Freq ;



			if (mes_groupe  )		// pour n'afficher  que si mesures groupe
			{
				if ((Freq <890 || Freq >1800)&& (tpa!=7)&& (tpa!=0)) {Led_rouge = 1;}	// pour indiquer une mauvaise r�ponse param�tres groupe

			}


		// pour avoir le nb de capteurs trouv�s en interrogation
//				if ((I_Conso > i_conso_min) && (Freq != 0))	// si I_Conso_min >= 2.0mA et Freq non nulle on a un capteur
//					{
//    			Nb_TPA_ok++;  // indique le nb de capteurs trouv�s en interrogation
//					}




// ====> 500mS + 375mS + 375mS ==> 1250mS 
//Attente de la chute du signal d'horloge pour synchro avec 1.5sec
				while(Synchro1s); //attend passage � 0 de RB3, boucle tant que la condition n'est pas arriv�e Synchro1s=PORTBbits.RB3

//<=================ICI 1.5SEC================>

			DELAY_MS (150);		// 1 x 150ms  peut-�tre � ajuster

// ====> 1500+150= 1650mS
/*
				Mesure_I_Conso();	//Mesure et attente de la fin du courant de consommation pour mesure du courant de repos
				while (I_Conso>(I_Conso2-10))	//le courant de consommation est retomb� � 0 
					{				
					Mesure_I_Conso();	//<=========================pr�voir une s�curit� de blocage
					}
//			DELAY_MS (20);		// 1 x 20ms  ici on peut mesurer le courant de repos
*/

		if (mes_groupe == 0 && mes_debit==0 ) // � faire uniquement si en mesure TP
			{
			// ici on peut mesurer le courant de repos
//					Led_rouge = 1;	// test
				Mesure_I_Repos();
				I_Repos1=I_Repos;
			DELAY_MS (5);		
				Mesure_I_Repos();
				I_Repos2=I_Repos;
				I_Repos=(I_Repos1+I_Repos2)/2;
//					Led_rouge = 0;	// test
				if (I_Repos<=15){I_Repos=0;}    // seuil pour �viter d'afficher un courant de repos si rien de branch� 
										    	// car sinon il y a un minimum de 12�A d� � l'offset de l'ampli
												
				Tab_I_Repos[tpa]=I_Repos;			    	//I_Repos est en (�A)	  
				Tab_I_Modul[tpa]=I_Modul;					//I_Modul est en (mA x10)

//######  Retranche le courant de repos du courant de modulation
				I_Conso1/=2;   		// moyenne
				I_Conso1*=100;  	 // met � la m�me �chelle que i_repos   (i_conso est x10)
				if (I_Conso1>I_Repos) {  // pour ne pas prendre en compte si pas de capteur
				I_Conso1-=I_Repos;  // retranche I_Repos de I_Conso  (i_conso est x10)
				}
				else {I_Conso1=0;}
				I_Conso1/=100;		// remet � l'�chelle pour i_conso
//######
				I_Conso1+= (I_Modul/2);  // TEST  ON RAJOUTE LA MOITI� DE LA MODULATION
				Tab_I_Conso[tpa]=I_Conso1;     //I_Conso en (mA x10) 
//				Tab_I_Conso[tpa]=(I_Conso1+I_Conso2)/2;     //I_Conso en (mA x10) 
				

			if (((I_Repos<i_repos_min) && tpa>=1) && (Tab_I_Conso[tpa]> i_conso_min) && (val1TP))  // il n'y a qu'un capteur et il a r�pondu  (!!si val1TP=1 la boucle s'arrete si capteur a r�pondu)
				{
				irep = 0;
				}

				Pres_Debit=0;  // si d�bitm�tre, il  a �t� mesur� et  par d�faut il est mis en absent (0)	

				if (F_ecran_adres)		//si = 1  = complet
					{   
				ecran_aff_TPA_complet();  
					}
				else
					{	
				ecran_aff_TPA_reduit(); 
					}		
			}

			if (mes_groupe )		// pour n'afficher  que si mesures groupe
			{
		 aff_mes_groupe();
			}


				
				Led_rouge = 0;
				Led_verte = 0;
//Attente de la remont�e du signal d'horloge pour synchro avec 2sec
				while(!Synchro1s); //attend passage � 1 de RB3, boucle tant que la condition n'est pas arriv�e Synchro1s=PORTBbits.RB3
			
			//FIN DE BOUCLE MESURE CAPTEURS PCPG  ou groupe
				if (touche_BP4 == 0 || irep==0)
						{
				Nb_Capteurs_max = tpa;		// utilis� en mode scrolling  -1
				fin_mesures = 1;
					break;
						}
			}	

		

////				set_time(0x07,0x00);  // adrese= 0x07 valeur 0x10 pour d�valider SQR/OUT  � 1Hz
				Boucle_TP = 0;		//	permet de stopper l'affichage des valeurs
//				MemoBits.Bit4 = 0;
				CPT_ON = 0;		// d�valide ligne de mesure 
				UN_CPT = 0; 	// convertisseur 80V OFF
				HT_ON = 0; 		// convertisseur 60V OFF
//				DELAY_MS (10);		// 10ms 
				DECH_LIGN = 0;   // D�charge la ligne
				Led_verte = 0;
				Led_jaune = 0;
				if (mes_groupe==0 && mes_debit==0)// � effectuer si pas en mesure groupe ni debit
					{
				SetPos(0,60);								// Positionner le curseur
				PutMessage("Fin des Mesures  ");				  		// Ecrire un message  L=17 caract
				while (touche_BP4 == 0); // pour attente touche relach�e
				DELAY_SEC (1);
				Scrolling_Ecran_tpa(); // r�affiche l'�cran complet ou r�duit pour la mise � jour des �tats et des d�faut en affichage inverse
				DELAY_SEC (1);
				SetPos(0,60);								// Positionner le curseur
				PutMessage("Pret mesures/Enregistrer");				  		// Ecrire un message  L=17caract
//				PutMessage("Sauve: touche VAL");				  		// Ecrire un message  L=17 caract
				DELAY_SEC (1);
					}					
				mes_groupe=0;  // retour en mode mesure capteurs
				mes_debit=0;  // retour en mode mesure capteurs
}



void Mes_freq_v2()
{
unsigned int cpt_to; //compteur time out

// mesure sur 1 p�riode du signal fr�quence capteur par int2(RB2) d�clench�e sur front montant et par le nbre de boucles du timer1 sur 16bits
// l'horloge du timer1 est celle du quarts de 24Mhz/4= 6Mhz, donc une boucle timer = 1 / (6Mhz) = 0.000000166sec = 0.166�s
// la valeur de la fr�quence capteur = 1/(0.166�s * val_timer1)
// valeur d'1 p�riode =   [(t1ov_cnt * 65536 + TempH * 256 + TempL) * 0.166]  normalement   t1ov_cnt sera �gal � 0 si >0 => erreur

			t1ov_cnt=0x00;	// compteur de d�bordement timer1
			cpt_int2 = 0x00;
			Freq = 0x0000;     // fr�quence de capteur
			tempF1 = 0x0000;
			tempF2 = 0x0000;
			tempF3 = 0x0000;
			tempF4 = 0x0000;
				Freq1=0;
				Freq2=0;
				Freq3=0;
				Freq4=0;
			TMR1H = 0x0000;		// registre H 8bit du timer 1
			TMR1L = 0x0000;		// registre L 16bit du timer 1
			Timer1L = 0x0000;
		   	Timer1H = 0x0000;


			PIR1bits.TMR1IF = 0; // efface le drapeau d'IT
			T1CONbits.T1CKPS0 = 0;	// pr�diviseur  1:1
			T1CONbits.T1CKPS1 = 0;	// pr�diviseur  1:1
			T1CONbits.RD16 = 0; // timer sur 8bits SINON TROP DE PB
			T1CONbits.TMR1CS = 0;	// TIMER1 sur horloge interne (Fosc/4)
			T1CONbits.T1RUN = 0; 		// source autre que oscillateur du timer 1
			T1CONbits.T1OSCEN = 0;	// turn off resistor feeback 
//   			RCONbits.IPEN = 1; 	//enable priority interrupt   => � priori pas besoin d'int du timer1 sauf si roll-over en cas d'absence de fr�quence
																	//pour pouvoir stopper le compteur
		   	IPR1 = 0x01;		// set Timer1 to high priority	
			PIE1 = 0x01;    	// enable Timer1 roll-over interrupt
//			INTCON = 0xC0;		//GIE=1 PEIE=1 enable global and peripheral interrupts



// CONFIG POUR INT2(RB2)
			cpt_int2=0;
			INTCONbits.RBIE = 0;	//d�valide  interrupt sur port B
//			INTCONbits.RBIF = 0;	//RESET  interrupt 	FLAG sur port B  
			INTCON2bits.RBPU = 0;	// pull-up port B         � voir  <<<<
			INTCON2bits.INTEDG2 = 0;	//INT2 sur front montant   <<<<<
			INTCON2bits.RBIP = 0;	// interrupt Hight priorit� sur port B   <<<<<
			T1CONbits.TMR1ON = 0;  // stop timer 1 

			INTCONbits.GIEH=1; // Toutes les IT d�masqu�es autoris�es
			INTCONbits.PEIE = 1;  // valide ttes les int haute priorit�
			INTCON3bits.INT2IP = 1;  // INT2 en haute priorit�

			INTCON3bits.INT2IF = 0;	// flag d'interruption int2 � reseter 
			INTCON3bits.INT2IE = 1;	// valide interruption INT2

	// time out en cas de non r�ponse il faut cpt_to <500(~800Hz) et > 240(1827Hz)
	//		Si <240 f < 800Hz => valeur 800
	//		Si >500 f > 1800Hz => valeur 1800

			cpt_to=0x0000;  // compteur time-out

//			while (cpt_to<500 && cpt_int2<2)
			while (cpt_to<10000 && cpt_int2< 21)   
					{
						cpt_to++;
					}		
/*			if (cpt_to < 240 || cpt_to > 500 || cpt_int2<2)
					{
				Freq = 0;
				return;
					}   */



//			while (cpt_int2<2);
			INTCONbits.GIE = 0; 	//disable global interrupt  
			INTCONbits.PEIE = 0;	//d�valide ttes les int haute priorit�
			INTCON3bits.INT2IE = 0;	// arret interruption INT2
//			while(1);


			

//			tempF1 = (Timer1H * 256 + Timer1L);
			tempF1 =  ( Timer1H * 256 + Timer1L);
			tempF1 = ( tempF1 + t1ov_cnt*65535)/20;
			if (tempF1>0)
					{
//			if (tempF1 >= 0x0001)  //0x0A
//						{  
				tempF1+=0x0015; 
//						}	//  corrige le temps de retard du aux instructions de d�marrage du timer1 

			tempF2 = tempF1 * 0xA6 ;   //0xA6 = * 166
			tempF3 = 1/(tempF2 /1000000);
			tempF4 = tempF3 *1000;   //x1000
				Cumul_F+=tempF4;
			Freq = (int)tempF4;
					}			
			else
					{
				Freq=0x0000;
				Cumul_F=0x0000;
					}

			T1CONbits.RD16 = 0; // timer sur 8bits
			TMR1H = 0x0000;		// registre H 8bit du timer 1
			TMR1L = 0x0000;		// registre L 16bit du timer 1

}

void Instable(int tpa)  // test instabilit�
{
				if (Freq1 >= Freq2)
					{
					tempF1=Freq1-Freq2;
					}
				else			
					{
					tempF1=Freq2-Freq1;
					}

				//---------

				if (Freq2 >= Freq3)
					{
					tempF2=Freq2-Freq3;
					}
				else			
					{
					tempF2=Freq3-Freq2;
					}

				//---------

				if (Freq3 >= Freq4)
					{
					tempF3=Freq3-Freq4;
					}
				else			
					{
					tempF3=Freq4-Freq3;
					}

				if (tempF1>max_instable||tempF2>max_instable||tempF3>max_instable)
					{
					Tab_Etat[tpa]|= 0x80;  // instable
					}

}



void Mesure_Ligne_AM(void)
{
// TEST de s�curit� avant mesure capteurs
				Mesure_V_Ligne_AM();
				if (V_Ligne_AM_Calc >V_Ligne_Max)		//V_Ligne_Max
//				if (V_Ligne_AM_Calc <10)		//V_Ligne_Max
				{
				ClearScreen ();
				SetPos(0,24);		//x , y						 
				PutMessage("Tension Ligne:");  	
				ftoa(V_Ligne_AM_Calc,DATAtoLCD,1);				// Convertir un float en une chaine de caract�res
				SetPos(80,24);	//x , y							// Positionner le curseur	
 				PutFloat(DATAtoLCD);
				SetPos(100,24);		//x , y						 
				PutMessage("Volts");  	
				}
}





void Mesure_TPR()
{
int r;
//int RTP_table;
int Seuil_R;
int R_TP;
int Cpt_Mesures_AV ;   //compteur pour boucle de mesure => v�rifier si capteur adressable � la place d'un r�sistif
int Cpt_defauts;       // compteur pour nb de fois �cart de mesures_AV



float R_1K = 0.95;   // 0.95 en Kohm
float I_1k;
//float Gain = 10.09;     //avec r88=100k et r89=11k
float Gain = 8.50; 	//avec r88=150k et r89=20k
float ref_56V = 56;   //(53.6) � ajuster (augmenter la valeur de tension pour augmenter la valeur du capteur)
float I_offset_Ampli = 12;  //12
float V_Resistif_mem1; //  V_Resistif m�moris� 1   mesure en aval du capteur

//float V_Resistif_mem2; //  V_Resistif m�moris� 2   mesure en aval du capteur  si la valeur est sup ou inf de plus de 100mV
//init variables
//mesure V_resistif
// mesure V_48v_AM  en amont du capteur r�sistif
// calcul de la valeur de la tension en fonction de V48AM,Vresistif,R1K,....
// lecture table pour connaitre la position
// lecture table2 pour avoir la valeur de fr�quence en fonction de la position
				r = 0;
				Freq_R=0;
				IMP_TRANS=1;    //*impulsion sur relais pour mode transtest
				DELAY_MS (50);	//*
				IMP_TRANS=0;	//*

				CPT_ON = 0;		//   <======== d�valide ligne de mesure � 48v par s�curit�
				PONT_ON = 1;    // zener off = 1
				HT_ON = 1;				 // mise en route du convertisseur 48V 
				DELAY_MS (200);		// d�lai pour sabiliser le 48v
				UN_CPT=0;			//pour ne pas avoir le 80v (si=0)
				Led_rouge = 0;          
				Led_jaune = 1;          //1
				DECH_LIGN = 0;   // lib�re la d�charge ligne
				CPT_ON = 1;		//   <======== valide ligne de mesure � 48v




				DELAY_SEC (1);		//
				

				Mesure_V_Resistif(); // valeur = V_Resistif (int)   mesure en aval du capteur
				Mesure_V_Ligne_AM();

				CPT_ON = 0;		//   <======== d�valide ligne de mesure 
				HT_ON = 0;				 // Arret convertisseur 48V 
				Led_jaune = 0;          //1
				DECH_LIGN = 1;   // d�charge ligne



// il faut corriger V_Resistif car d�pend de V_Ligne_AM et donc varie avec tension du 48V
// la valeur max V_1K_Max est calcul�e pour une tension de 56V et 100K sur R1K et un gain de 10.09 pour l'ampli
// 100K correspond au minimum de valeur d'un capteur r�sistif
		if (V_Resistif < 150)
			{
			Freq_R = 8888;
			}
		else
			{

				V_Resistif = (V_Resistif/ref_56V)*V_Ligne_AM_Calc;
				I_1k = (V_Resistif/Gain)/ R_1K;
				I_1k -=I_offset_Ampli;
				R_TP = (int)(((V_Ligne_AM_Calc*1000) - V_Resistif)/I_1k);

				
					if (R_TP<95 )	Freq_R = 0;     // <==  ajustements 
					if (R_TP>=95 )	Freq_R = 900;      //100 vraie valeur
					if (R_TP>=105 )	Freq_R = 934;      //110 vraie valeur
					if (R_TP>=120 )	Freq_R = 969;      //122 vraie valeur
					if (R_TP>=135 )	Freq_R = 1003;
					if (R_TP>=150 )	Freq_R = 1038;
					if (R_TP>=166 )	Freq_R = 1072;
					if (R_TP>=186 )	Freq_R = 1107;
					if (R_TP>=208 )	Freq_R = 1141;
					if (R_TP>=232 )	Freq_R = 1176;
					if (R_TP>=265 )	Freq_R = 1210;
					if (R_TP>=301 )	Freq_R = 1245;
					if (R_TP>=344 )	Freq_R = 1279;
					if (R_TP>=400 )	Freq_R = 1314;
					if (R_TP>=468 )	Freq_R = 1348;
					if (R_TP>=568 )	Freq_R = 1383;
					if (R_TP>=698 )	Freq_R = 1417;
					if (R_TP>=898 )	Freq_R = 1452;
					if (R_TP>=1200) Freq_R = 1486;
					if (R_TP>=1820)	Freq_R = 1521;
					if (R_TP>=3820)	Freq_R = 1555;			
			}


		if(Freq_R != 0)
			{
//*********ici modif 05-2015 pour d�tection si capteur adressable � la place d'un r�sistif **********
				DECH_LIGN = 1;   // d�charge ligne
				DELAY_MS (200);		// d�lai pour d�charger la ligne
				DECH_LIGN = 0;   // lib�re la d�charge ligne

				HT_ON = 1;				 // mise en route du convertisseur 48V 
				DELAY_MS (50);		// d�lai pour sabiliser le 48v
				CPT_ON = 1;		//   <======== valide ligne de mesure � 48v
				Led_jaune = 1;          //1



				Cpt_defauts =0;
				displayNum(0,0,Cpt_defauts);

				DELAY_MS (200);		// d�lai pour sabiliser le 48v
				Cpt_Mesures_AV = 0;  // compteur pour la boucle
				V_Resistif_mem1=0;
				Cpt_Mesures_AV = 0;  // compteur pour la boucle
				Cpt_defauts = 0;       // compteur pour nb de fois �cart de mesures_AV
				Mesure_V_Resistif(); // valeur = V_Resistif (int)   mesure en aval du capteur
				V_Resistif_mem1 = V_Resistif;

				for(Cpt_Mesures_AV==0;Cpt_Mesures_AV<=15;Cpt_Mesures_AV++)  //15 x100mS = 1500 mS  +  200  => 1700ms  
				{
				// 1 mesure toutes les 100msec
				Mesure_V_Resistif(); // valeur = V_Resistif (int)   mesure en aval du capteur
				if ((V_Resistif <= V_Resistif_mem1-50) || (V_Resistif > V_Resistif_mem1+50))	 // si sup ou inf de 50mV				
					{
				Cpt_defauts +=1;
				displayNum(0,0,Cpt_defauts);
				V_Resistif_mem1 = V_Resistif; // � voir si il faut m�moriser � chaque mesure ou uniquement � la premi�re valeur
					}
				DELAY_MS (100);	//*
				V_Resistif_mem1 = V_Resistif;
				}

				CPT_ON = 0;		//   <======== d�valide ligne de mesure 
				HT_ON = 0;				 // Arret convertisseur 48V 
				Led_jaune = 0;  

        
				if(Cpt_defauts >=1)
				{
				Led_rouge = 1; 
				Freq_R = 9999;

				return;         
				} 
			}
//*********FIN modif 05-2015 pour d�tection si capteur adressable � la place d'un r�sistif **********

}








						




