// eeprom.c
// GESTION DE L'EEPROM

#include <stdio.h>
#include <string.h>
#include <p18f8723.h>
#include <stdlib.h>
//#include <delays.h> 
#include <i2c.h>
//#include <GLCD.h>
#include <delay.h>
#include <ftoa.h>
extern unsigned char TMP[125];
extern unsigned char TRAMEIN[125];
extern char SAVMEMOEIL;
extern unsigned int SAVADOEIL;
extern char FUNCTIONUSE;

extern unsigned char SAVADDE; //BUG SCRUTATION RESISTIFS SUR ADRESSABLE ACCES MEMOIRE
extern unsigned int SAVADDHL;
extern unsigned int SAVtab;
extern unsigned char SAVr;



//********************************************************************/

unsigned char putstringI2C(unsigned char *wrptr)
{



unsigned char x;

unsigned int PageSize;
PageSize=128;


  for (x = 0; x < PageSize; x++ ) // transmit data until PageSize  
  {
    if ( SSPCON1bits.SSPM3 )      // if Master transmitter then execute the following
    { // 
	
	 	
		      if ( putcI2C ( *wrptr ) )   // write 1 byte
		{
		        return ( -3 );            // return with write collision error
		}
	

      IdleI2C();                  // test for idle condition
      if ( SSPCON2bits.ACKSTAT )  // test received ack bit state
      {
        return ( -2 );            // bus device responded with  NOT ACK
      }                           // terminateputstringI2C() function
    }
    else                          // else Slave transmitter
    {
      PIR1bits.SSPIF = 0;         // reset SSPIF bit

	
      SSPBUF = *wrptr;            // load SSPBUF with new data
		    

	  SSPCON1bits.CKP = 1;        // release clock line 
      while ( !PIR1bits.SSPIF );  // wait until ninth clock pulse received

      if ( ( !SSPSTATbits.R_W ) && ( !SSPSTATbits.BF ) )// if R/W=0 and BF=0, NOT ACK was received
      {
        return ( -2 );            // terminateputstringI2C() function
      }
    }

 	

  wrptr ++;                       // increment pointer 

  
  	 
 }                               // continue data writes until null character
  return ( 0 );
}


 


//Lecture dans l'EEPROM N�0 N�1 N�2 ou N�3 envoyer 000 ou 001......
unsigned char LIRE_EEPROM(unsigned char ADDE,unsigned int ADDHL,unsigned char *tab,char r)
{
char h;
unsigned char t;
unsigned char TMP2;
unsigned char TMP4;
unsigned char AddH,AddL;

if ((ADDE!=SAVMEMOEIL)&&(FUNCTIONUSE=='O'))
ADDE=SAVMEMOEIL;

if ((ADDHL!=SAVADOEIL)&&(FUNCTIONUSE=='O'))
{
ADDHL=SAVADOEIL;
tab=TRAMEIN;
}


//if (r==2)
//r=2;


if ((ADDE!=SAVADDE)&&(FUNCTIONUSE=='R'))
ADDE=SAVADDE;
if ((ADDHL!=SAVADDHL)&&(FUNCTIONUSE=='R'))
{
r=SAVr;
ADDHL=SAVADDHL;
tab=TMP;
}

if ((ADDE!=SAVADDE)&&(FUNCTIONUSE=='r'))
{
ADDE=SAVADDE;
r=SAVr;
ADDHL=SAVADDHL;
tab=TRAMEIN;
}

if ((ADDHL!=SAVADDHL)&&(FUNCTIONUSE=='r'))
{
r=SAVr;
ADDHL=SAVADDHL;
tab=TRAMEIN;
}

if ((ADDE!=SAVADDE)&&(FUNCTIONUSE=='T'))
ADDE=SAVADDE;
if ((ADDHL!=SAVADDHL)&&(FUNCTIONUSE=='T'))
{
r=SAVr;
ADDHL=SAVADDHL;
tab=TRAMEIN;
}




//MAXERIE A2 
if (ADDE==3)
ADDE=6;
if (ADDE==2)
ADDE=4;
if (ADDE==1)
ADDE=2;


 


AddL=ADDHL;
AddH=ADDHL>>8;



TMP2=ADDE;
TMP2=TMP2<<1;
TMP2=TMP2|0xA0;


   IdleI2C(); 
   StartI2C(); 
   //IdleI2C();	
   while ( SSPCON2bits.SEN ); 	

   WriteI2C(TMP2); // 0 sur le bit RW pour indiquer une lecture
   IdleI2C();
  
        


    WriteI2C(AddH); // 0 sur le bit RW pour indiquer une lecture
  IdleI2C();

  while ( SSPCON2bits.RSEN );   

	WriteI2C(AddL); // 0 sur le bit RW pour indiquer une lecture
   IdleI2C();


	RestartI2C();
	while (SSPCON2bits.RSEN);
	

	//AckI2C();
	TMP2=TMP2|0x01;
 WriteI2C(TMP2); // 1 sur le bit RW pour indiquer une lecture
   IdleI2C();

 

			//tab[85]='*';
			if (r==1) // pour trame memoire
			getsI2C(tab,86);  
			if (r==0)	
			getsI2C(tab,108);  
			if 	(r==3)	
			getsI2C(tab,116); 
			if 	(r==4)	
			getsI2C(tab,24); 


   NotAckI2C(); 
   while(SSPCON2bits.ACKEN); 



   StopI2C();  
   while (SSPCON2bits.PEN);
testEEW:
   return (1); 
} 




//Ecriture dans l'EEPROM N�0 N�1 N�2 ou N�3 envoyer 000 ou 001......adresse haute et basse et une donn�e sur 8bits

unsigned char ECRIRE_EEPROMBYTE(unsigned char ADDE,unsigned int ADDHL,unsigned char *tab,char g)
{
unsigned char TMP2,TMP5;
unsigned int h;
unsigned char t;
unsigned char AddH,AddL;





if ((ADDE!=SAVADDE)&&(FUNCTIONUSE=='R'))
ADDE=SAVADDE;
if ((ADDHL!=SAVADDHL)&&(FUNCTIONUSE=='R'))
{
g=SAVr;
ADDHL=SAVADDHL;
tab=TMP;
}








//MAXERIE A2 
if (ADDE==3)
ADDE=6;
if (ADDE==2)
ADDE=4;
if (ADDE==1)
ADDE=2;




TMP2=ADDE;
			TMP2=TMP2<<1; 
			TMP2=0XA0|TMP2;

			
AddL=(ADDHL);
AddH=(ADDHL)>>8;			
			
if 	(g==1) // POUR FONCTIONNEMENT TRAME MEMOIRE		
tab[85]='*';   //FIN DE TRAME MEMOIRE			
if 	(g==3) // POUR FONCTIONNEMENT TRAME MEMOIRE		
tab[115]='*';   //FIN DE TRAME GROUPE
if 	(g==4) // POUR FONCTIONNEMENT TRAME MEMOIRE		
tab[23]='*';   //FIN DE TRAME EXISTANCE


			
			 
  IdleI2C();                      // ensure module is idle
  StartI2C();                     // initiate START condition
  while ( SSPCON2bits.SEN );      // wait until start condition is over 
  WriteI2C(TMP2);        // write 1 byte - R/W bit should be 0
  IdleI2C();                      // ensure module is idle
  WriteI2C(AddH);            // write HighAdd byte to EEPROM 
  IdleI2C();                      // ensure module is idle
  WriteI2C(AddL);             // write LowAdd byte to EEPROM
  IdleI2C();                      // ensure module is idle
  putstringI2C (tab);         // pointer to data for page write
  IdleI2C();                      // ensure module is idle
  StopI2C();                      // send STOP condition
  while ( SSPCON2bits.PEN );      // wait until stop condition is over 
  return ( 0 );                   // return with no error	
		
 				


    
		return 0;	// FIN DE BOUCLE a *

                     
        
        

 


}













// CALCUL EMPLACEMENT MEMOIRE
unsigned int calcul_addmemoire (unsigned char *tab)
{
unsigned int t;
unsigned int i;
unsigned int r;

 

t=0;
i=tab[3];
i=i-48;
t=t+1*i;
i=tab[2];
i=i-48;
t=t+10*i;
i=tab[1];
i=i-48;
t=t+100*i;


r=2176*(t);
if (t>29)
r=2176*(t-30);
if (t>59)
r=2176*(t-60);
if (t>89)
r=2176*(t-90);


t=0;
i=tab[5]-48;
t=1*i;
i=tab[4]-48;
t=t+10*i;
i=t;

t=r+128*i;



delay_qms(10);



return t;


}


// choix du numero memoire suivant N�voie
unsigned char choix_memoire(unsigned char *tab)
{
unsigned int i;
unsigned int t;
unsigned char r;




t=0;
i=tab[3]-48;
t=t+1*i;
i=tab[2]-48;
t=t+10*i;
i=tab[1]-48;
t=t+100*i;

r=0;
if (t>29)
r=1;
if (t>59)
r=2;
if (t>89)
r=3;




delay_qms(10);
return r;

}




