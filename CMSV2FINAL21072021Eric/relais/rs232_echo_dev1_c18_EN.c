/************************************************************************************************

Demonstration program that repeating characters received on the serial port
 
Author: Manzoni Giovanni, Hardelettrosoft
Licensed under GNU GPL
 
  -> Support: http://howto.hardelettrosoft.com/
 


*************************************************************************************************

Log
02/06/2012 
- First version

13/03/2013 
- Added comments
- Change the name of some functions

************************************************************************************************/

#include <p18F8723.h> 
#include "delays.h"
#include "adc.h"
#include "usart.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#pragma config WDTEN = OFF 		// Watch dog timer is always disabled. SWDTEN has no effect. 
#pragma config LVP = OFF		// Low Voltage Programming disabled
#pragma config DEBUG = OFF		// Debug off
#pragma config FOSC = HSMP	 	// HS oscillator (medium power 4-16 MHz) 
#pragma config PLLCFG = OFF 		// Oscillator used directly 
#pragma config PRICLKEN = ON 		// Primary clock is always enabled  
#pragma config FCMEN = OFF 		// Fail-Safe Clock Monitor disabled 
#pragma config IESO = OFF 		// Oscillator Switchover mode enabled
#pragma config PWRTEN = ON 		// Power up timer enabled 
#pragma config BOREN = OFF 		// Brown-out Reset disabled in hardware and software 
#pragma config PBADEN = OFF 		// PORTB<5:0> pins are configured as digital I/O on Reset 
#pragma config MCLRE = EXTMCLR 		// MCLR pin enabled, RE3 input pin disabled  
#pragma config STVREN = ON 		// Stack full/underflow will cause Reset 


void init_hw(void);
void blink(void);
void rx_handler (void);

#pragma code rx_interrupt = 0x8		// interrupt vector  - High Priority
void rx_int (void){
	_asm goto rx_handler _endasm
}
#pragma code

/*
Function: void rx_handler (void)
Purpose: Interrupt handler
Return: -
Parameters: -
Revision: 1
Note: -
*/ 
#pragma interrupt rx_handler
void rx_handler (void){
	unsigned char  c[2];
	char dummy; 

    if(RCSTA1bits.FERR == 1){ 			// check if Framing error 
        					// AN774 P.12
		dummy = RCREG;      		// read data byte with error
		TXSTA1bits.TXEN=0; 
		TXSTA1bits.TXEN=1; 
    }
	else if (RCSTA1bits.OERR==1){ 		// check for Overflow Error 
						// AN774 P.12
		dummy = RCREG; 			// read data byte in the FIFO with possible error
		dummy = RCREG; 			// read data byte in the FIFO with possible error
		RCSTA1bits.CREN=0; 		// if error present, reset OERR so that reception can continue
		RCSTA1bits.CREN=1;
    }
	else {
		gets1USART((char *)c,1 );	// read 1 chr
		c[1]=0;				// Terminate string with Null chr
		puts1USART((char *)c); 		// echo back the data recieved back to host
	}
}

void main() {
  
    	init_hw();

   	for(;;) {
		blink(); 
	} 
}


/*
Function: void init_hw(void)
Purpose: Init hw
Return: -
Parameters: -
Revision: 2
Note: -
*/ 
void init_hw(void){

	// OSCILLATOR
	// 0   Device enters Sleep mode on SLEEP instruction
	// 000 
	// 1 Device is running from the clock defined by FOSC<3:0> of the CONFIG1H register
	// 0 read only
	// 00 Primary clock

	OSCCON = 0b00001000 ; 

/*
	A0: an sens. temp.	-> in  1  
	A1: an trimmer		-> in  1  
	A2: dip1  		-> in  1  
	A3: dip2		-> in  1  
	A4: dip4		-> in  1  
	A5: dip5		-> in  1 
	A6: osc			-> out 0
	A7: osc			-> in  1
*/

	TRISA= 0xBF;


/*
	B0: Button P11		-> in  1  
	B1: Button P12		-> in  1  
	B2: Button P13		-> in  1  
	B3: dip3		-> in  1  
	B4: rs485 DE		-> out 0  
	B5: rs485 /RE		-> out 0 
	B6: rs232 TX2		-> in  1
	B7: rs232 RX2		-> in  1

*/

	TRISB= 0xCF;    


/*

	C0: Led cpu		-> out 0  
	C1: Button P14		-> in  1  
	C2: Button P15		-> in  1  
	C3: SCK			-> out 0  
	C4: SDI			-> out 0  
	C5: SDO			-> out 0 
	C6: rs232 TX1		-> in  1
	C7: rs232 RX1		-> in  1
*/

	TRISC= 0xC6;  


/*
	D0: Led 0		-> out 0  
	D1: Led 0		-> out 0  
	D2: Led 0		-> out 0  
	D3: Led 0		-> out 0  
	D4: Led 0		-> out 0  
	D5: Led 0		-> out 0 
	D6: Led 0		-> out 0
	D7: Led 0		-> out 0

*/

	TRISD= 0x00;


/*
	E0: Led 0		-> in  1  
	E1: Led 0		-> in  1  
	E2: Led 0		-> in  1  
*/

	TRISE= 0xff;


	OpenADC(	ADC_FOSC_RC & ADC_RIGHT_JUST & ADC_12_TAD , 
				ADC_CH0 & ADC_CH1 & ADC_INT_OFF,
				ADC_REF_VDD_VDD & ADC_REF_VDD_VSS );

	// 9600 baud @ 0% error !! = 14745600 / (64/(23 +1 ))  
	Open1USART( USART_TX_INT_OFF &
				USART_RX_INT_ON &        //Interrupt seriale abilitato
				USART_ASYNCH_MODE &
				USART_EIGHT_BIT &
				USART_CONT_RX &
				USART_BRGH_LOW , 23); 
	
	RCONbits.IPEN = 1;		// Enable interrupt priority 
	IPR1bits.RCIP = 1; 		// Make receive interrupt high priority 
	INTCONbits.GIEH = 1; 		// Enable all high priority interrupts 

	//0 = DIGITAL
	ANSELAbits.ANSA0 = 1; 
	ANSELAbits.ANSA1 = 1; 
	ANSELAbits.ANSA2 = 0; 
	ANSELAbits.ANSA3 = 0; 
	ANSELAbits.ANSA5 = 0; 
	ANSELEbits.ANSE0 = 0; 
	ANSELEbits.ANSE1 = 0; 
	ANSELEbits.ANSE2 = 0; 
	ANSELCbits.ANSC2 = 0;     
	ANSELCbits.ANSC3 = 0;    
	ANSELCbits.ANSC4 = 0;    
	ANSELCbits.ANSC5 = 0;    
	ANSELCbits.ANSC6 = 0; 
	ANSELCbits.ANSC7 = 0;    

}


/*
Function: void blink(void)
Purpose: Blink LED
Return: -
Parameters: -
Revision: 1
Note:
	Delay of 250ms
	Cycles = (TimeDelay * Fosc) / 4
	Cycles = (250ms * 14.7456MHz) / 4
	Cycles = 921,600
*/ 
void blink(void){

	PORTCbits.RC0 = 1;
	Delay100TCYx(26); 
	Delay10KTCYx(91); 
	PORTCbits.RC0 = 0;
	Delay100TCYx(26); 
	Delay10KTCYx(91); 
}
