	// Sauvegarde des parametres moteurs ( si parametres identique, inutile d'enregistrer )
	temp = Rd_eeprom_i2c_int( &P_debm_int_epp );
	if ( temp != P_debm_int )
		Wr_eeprom_i2c_int( &P_debm_int_epp, P_debm_int);
	// Test si donn�es bien m�moris�es dans EEPROM I2C, si mauvais => defaut interface !
	temp = Rd_eeprom_i2c_int( &P_debm_int_epp );
	if ( temp != P_debm_int )
		F_etat_interface_3 = 1 ;






		//  INITIALISATION PAR DEFAUT DES PARAMETRES DU MOTEUR A LA MISE SOUS TENSION DU BOITIER
		T_pre_int = Rd_eeprom_i2c_int( &T_pre_int_epp );

		F_dem_int = Rd_eeprom_i2c_int( &F_dem_int_epp );

		T_dem_int = Rd_eeprom_i2c_int( &T_dem_int_epp );

		F_dep_int = Rd_eeprom_i2c_int( &F_dep_int_epp );

		F_fin_int = Rd_eeprom_i2c_int( &F_fin_int_epp );

		T_fin_int = Rd_eeprom_i2c_int( &T_fin_int_epp );

		T_post_int = Rd_eeprom_i2c_int( &T_post_int_epp );











# pragma romdata eeprom_data24=0xF00150			// 	Nb de pas en defaut sur les I phases pour declencher un defaut majeur
rom const char	_NB_PAS_DEF_I_FT_char_epp[5];
rom const char	_2pts24[1] = ":";
rom const int	_NB_PAS_DEF_I_FT_int_epp;

# pragma romdata eeprom_data25=0xF00158			// 	Nb de pas en defaut sur les I phases pour declencher un defaut majeur
rom const char	_NB_PAS_OK_I_FT_char_epp[5];
rom const char	_2pts25[1] = ":";
rom const int	_NB_PAS_OK_I_FT_int_epp;

# pragma romdata eeprom_data26=0xF00200			// 	A la suite : parametres moteur
rom const int	T_pre_int_epp;		// Dur�e pr�-enable (ms)
rom const int	F_dem_int_epp;		// Frequence de d�marrage (Hz)
rom const int	T_dem_int_epp;		// Dur�e de demarrage (ms)
rom const int	F_dep_int_epp;		// Frequence de palier (Hz)
rom const int	F_fin_int_epp;		// Frequence d'arret (Hz)
rom const int	T_fin_int_epp;		// Dur�e de l'arret (ms)
rom const int	T_post_int_epp;		// Dur�e post-enable (ms)
rom const int	I_min_int_epp;		// Seuil minimum de courant (mA)
rom const int	I_max_int_epp;		// Seuil maximum de courant (mA)
rom const int	P_debm_int_epp;		// D�but Zone morte		( pas)
rom const int	P_finm_int_epp;		// Fin Zone morte		( pas)

#pragma romdata		/* Resume allocation of romdata into the default section */











# pragma romdata eeprom_dataG=0xF00100
rom const double gain_I_mot_eep ;		// G0
rom const char	slatch20[1] = "/";
rom const double gain_inv_I_mot_eep ;	// K0
rom const char	slatch21[1] = "/";
rom const double offset_I_mot_eep ;
rom const char	slatch22[1] = "/";
rom const double gain_I_ph12_eep ;		// G12
rom const char	slatch23[1] = "/";
rom const double gain_inv_I_ph12_eep ;	// K12
rom const char	slatch24[1] = "/";
rom const double offset_I_ph12_eep ;
rom const char	slatch25[1] = "/";
rom const double gain_I_ph34_eep ;		// G34
rom const char	slatch26[1] = "/";
rom const double gain_inv_I_ph34_eep ;	// K34
rom const char	slatch27[1] = "/";
rom const double offset_I_ph34_eep ;
rom const char	slatch28[1] = "/";

# pragma romdata eeprom_data20=0xF00130			// 	Nb de pas en defaut sur le I moteur pour declencher un defaut majeur
rom const char	_NB_PAS_DEF_I_MOT_char_epp[5];
rom const char	_2pts20[1] = ":";
rom const int	_NB_PAS_DEF_I_MOT_int_epp;

# pragma romdata eeprom_data21=0xF00138			// 	Nb de pas en defaut sur le I moteur pour declencher un defaut majeur
rom const char	_NB_PAS_OK_I_MOT_char_epp[5];
rom const char	_2pts21[1] = ":";
rom const int	_NB_PAS_OK_I_MOT_int_epp;

# pragma romdata eeprom_data22=0xF00140			// 	Nb de pas en defaut sur les I phases pour declencher un defaut majeur
rom const char	_NB_PAS_DEF_I_PH_char_epp[5];
rom const char	_2pts22[1] = ":";
rom const int	_NB_PAS_DEF_I_PH_int_epp;










/*----------------- ges_cmd_PDIP -----------------------------------------------*/
void ges_cmd_PDIP (void)		// Pas en defaut / correct pour I moteur	
{
/* Format reponse

*/
	unsigned int N = 0 ; // Variable usage general

	// Sauvegarde EEPROM serie
	Wr_eeprom_i2c_str ( &_NB_PAS_DEF_I_PH_char_epp, _NB_PAS_DEF_I_PH_char, N = 5 );
	Wr_eeprom_i2c_int ( &_NB_PAS_DEF_I_PH_int_epp, _NB_PAS_DEF_I_PH_int );

	Wr_eeprom_i2c_str ( &_NB_PAS_OK_I_PH_char_epp, _NB_PAS_OK_I_PH_char, N = 5 );
	Wr_eeprom_i2c_int ( &_NB_PAS_OK_I_PH_int_epp, _NB_PAS_OK_I_PH_int );

	// Sauvegarde EEPROM process
	Wr_eeprom_str ( &_NB_PAS_DEF_I_PH_char_epp, _NB_PAS_DEF_I_PH_char, N = 5 );
	Wr_eeprom_int ( &_NB_PAS_DEF_I_PH_int_epp, _NB_PAS_DEF_I_PH_int );

	Wr_eeprom_str ( &_NB_PAS_OK_I_PH_char_epp, _NB_PAS_OK_I_PH_char, N = 5 );
	Wr_eeprom_int ( &_NB_PAS_OK_I_PH_int_epp, _NB_PAS_OK_I_PH_int );

} // fin ges_cmd_PDIP











# pragma romdata eeprom_data12=0xF000C0
rom const char	I_ph34_ref_haut_char_epp[4];	// M�me chose courant phases 3 et 4
rom const char	_2pts11[1] = ":";
rom const int	I_ph34_ref_haut_int_epp;
rom const char	slatch11[1] = "/";

# pragma romdata eeprom_data13=0xF000D0			// Coeff en Pourcent de la diminution du seuil de tolerance inferieur sur le courant de phase
rom const char	coef_I_ph_min_char_epp[3];
rom const char	_2pts12[1] = ":";
rom const int	coef_I_ph_min_int_epp;
rom const char	slatch12[1] = "/";



extern void Rd_eeprom_i2c_str ( unsigned int adr_eeprom,  char *string, unsigned int long_string );









/******************** Rd_eeprom_double ************************************************/
void Rd_eeprom_double( unsigned int adr_eeprom, char *data )
{	/* lit un DOUBLE ou un LONG dans l'eeprom du PIC18
	adr_eeprom = adresse de la source l'eeprom : Rd_eeprom_double( & adr_eeprom,  &data)/ (le pointeur adr_eeprom est convertit en INT))  !!
	Place la donn�e m�moris�e � l'adresse indiqu�e par *char
	*/

	*data  = ReadEEPROM(adr_eeprom) ;
	data ++ ;
	*data = ReadEEPROM(adr_eeprom + 1 );
	data ++ ;
	*data = ReadEEPROM(adr_eeprom + 2 );
	data ++ ;
	*data = ReadEEPROM(adr_eeprom + 3 );

} // fin Rd_eeprom_double

/******************** Wr_eeprom_str ************************************************/
void Wr_eeprom_str ( unsigned int adr_eeprom,  char *string, unsigned int long_string )
{ 	/* ecrit une chaine de caracteres dans l'eeprom du PIC18
	adr_eeprom = adresse du destinataire dans l'eeprom : Wr_eeprom_int( & adr_eeprom,  data)/ (le pointeurest convertit en INT))  !!
	string = Adresse ( en INT ou en *Ptr ) de la chaine de donn�es � m�moriser en INT
	long_string = longueur de la chaine de caracteres
	*/
	unsigned int temp = 0 ; // variable temporaire

while (temp++ < long_string) WriteEEPROM(adr_eeprom++,*string++);

} // fin Wr_eeprom_str

/******************** Rd_eeprom_str ************************************************/
void Rd_eeprom_str ( unsigned int adr_eeprom,  char *string, unsigned int long_string )
{ 	/* Lit une chaine de caracteres dans l'eeprom du PIC18
	adr_eeprom = adresse du destinataire dans l'eeprom : Rd_eeprom_int( & adr_eeprom,  data)/ (le pointeurest convertit en INT))  !!
	string = Adresse (en *Ptr ) ou ranger la chaine de donn�es r�cup�r�e en m�moire
	long_string = longueur de la chaine de caracteres
	*/
	unsigned int temp = 0 ; // variable temporaire
	unsigned char temp_char = 0 ; // variable temporaire

while (temp++ < long_string) 
	{
	temp_char = ReadEEPROM(adr_eeprom++);
	*string++ = temp_char;	
	// *string++ = ReadEEPROM(adr_eeprom++);
	//string++;
	}	
} // fin Rd_eeprom_str
/***********************************************************************************/
