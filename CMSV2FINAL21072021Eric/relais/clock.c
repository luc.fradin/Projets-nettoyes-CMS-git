#include <p18f8723.h>
#include <clock.h>
#include <rtc.h>
#include <GLCD.h>						/* Gestion GLCD */
#include <delay.h>						/* timers (interruptions)*/
#include <inituc.h>


unsigned int Unites,Dizaines;        // Global var.
unsigned int Tval;
extern unsigned char Tab_Horodatage[10];		//10 octets	 //10 caract�res	  


//void date(unsigned int date, unsigned int month,unsigned int year);
//void day(unsigned int disp);
//void decode(unsigned int data);

unsigned int posx,posy;
unsigned int posx_mem,posy_mem;

void DisplayTime()
{

	box(0,0,127,63);
	posx = 10;
	posy = 20;
	posx_mem = posx;
//	posy_mem = posy;
	Tval = get_time(0x02);		//heures
	decode();
    displayHNum(posx+=15,posy,Dizaines);
	displayHNum(posx+=10,posy,Unites);
	SetPos(posx+=10,posy);
	PutHChar(45);

	Tval = get_time(0x01);		//minutes
	decode();
    displayHNum(posx+=7,posy,Dizaines);
	displayHNum(posx+=10,posy,Unites);
	SetPos(posx+=10,posy);
	PutHChar(45);

	Tval = get_time(0x00);		//secondes
	decode();
    displayHNum(posx+=7,posy,Dizaines);
	displayHNum(posx+=10,posy,Unites);

	posy +=20;
	posx = posx_mem ;
	Tval = get_time(0x03);		//jour
	SetPos(posx_mem,posy);
	PutMessage("        ");
	SetPos(posx_mem,posy);
	day(Tval);

	Tval = get_time(0x04);		//date
	decode();
    displayNum(posx+=50,posy,Dizaines);
	displayNum(posx+=6,posy,Unites);
	SetPos(posx+=6,posy);
	PutChar(45);

	Tval = get_time(0x05);		//mois
	decode();
    displayNum(posx+=6,posy,Dizaines);
	displayNum(posx+=6,posy,Unites);
	SetPos(posx+=6,posy);
	PutChar(45);

	Tval = get_time(0x06);		//ann�e
	decode();
    displayNum(posx+=6,posy,20);
    displayNum(posx+=12,posy,Dizaines);
	displayNum(posx+=6,posy,Unites);

// VERSION:
SetPos(10,56); 
PutMessage("V:1.15"); //A,M,J    <###############################################VERSION###############################
	DELAY_MS (10);

}


void decode()
{
    Dizaines=Tval>>4;
    Unites=Tval&0x0F;
}


void day(unsigned int disp)
{
    switch(disp)
    {
	case 1:PutMessage("Dimanche ");
               break;
	case 2:PutMessage("Lundi    ");
               break;
	case 3:PutMessage("Mardi    ");
               break;
	case 4:PutMessage("Mercredi ");
               break;
	case 5:PutMessage("Jeudi    ");
               break;
	case 6:PutMessage("Vendredi ");
               break;
	case 7:PutMessage("Samedi   ");
               break;
    }
}



void encodeH()
{
			Tval=Dizaines<<4;
			Tval=Unites+Tval;
}

void Set_Horloge()
{

/*unsigned int SetHeures,SetMinutes,SetSecondes;
unsigned int SetJour;
unsigned int SetDate,SetMois,SetAnnee; */
unsigned int Heures,Minutes,Secondes;
unsigned int Jour;
unsigned int Date,Mois,Annee;
unsigned int DizHeures,UnitHeures,DizMinutes,UnitMinutes,DizSecondes,UnitSecondes;
unsigned int DizDate,UnitDate,DizMois,UnitMois,DizAnnee,UnitAnnee;
unsigned int pos;		// position du curseur
unsigned int SetTouche;  // touche=0: pas activ�e, touche = 1 pour le +, touche= 2 pour le -				
unsigned int Aff;  // mem si affichage ecran displaytime
				SetTouche=0;
				pos = 1;		// pos 1 = Heures; pos 2 = Minutes; .......
				SetPos(5,0);
  				PutMessage("REGLAGE HORLOGE");		// Ecrire un message  nu_retour
				hline(0,127,10) ;			// ligne horizontale

 				SetPos(5,20);				// 
  				PutMessage("HH-MM-SS");
 				SetPos(5,35);				// 
  				PutMessage("JOUR");
 				SetPos(5,50);				// 
  				PutMessage("DD-MM-AA");

				Tval = get_time(0x02);		//lecture heures
				decode();
				UnitHeures = Unites;
				DizHeures = Dizaines;

				Tval = get_time(0x01);		//lecture minutes
				decode();
				UnitMinutes = Unites;
				DizMinutes = Dizaines;

				Tval = get_time(0x00);		//lecture secondes
				decode();
				UnitSecondes = Unites;
				DizSecondes = Dizaines;

				Jour = get_time(0x03);		//lecture jour

				Tval = get_time(0x04);		//lecture date
				decode();
				UnitDate = Unites;
				DizDate = Dizaines;

				Tval = get_time(0x05);		//lecture mois
				decode();
				UnitMois = Unites;
				DizMois = Dizaines;

				Tval = get_time(0x06);		//lecture annee
				decode();
				UnitAnnee = Unites;
				DizAnnee = Dizaines;


				while (touche_5 != 0 )
		{
				Aff=0;
	// ************** affichage des valeurs ****************


				if (touche_2==0)   // gestion de la position du curseur
						{
					pos+=1;
					if (pos==0x0E){pos=1;}
						}
				if (touche_1==0)
						{ 
				if (pos==1){pos=0x0D;}
					else {pos-=1;}
						} 
				while (touche_2 == 0 ||touche_1 ==0 ); // attente touche relach�e
	
//				DELAY_MS (200);		*/

	

				SetTouche=0;		// gestion touches incr�mentation ou d�cr�mentation
				if (touche_0 ==0)
						{SetTouche= 1;}
				if	(touche_3==0)	
						{SetTouche= 2;}
				while (touche_0 == 0 || touche_3 ==0 ); 	// attente touche relach�e


				switch (pos)	//gestion des valeurs en fonction de l'incr�mentation ou d�cr�mentation
				{
				case 1: 	// Diz Heures
   				SetPos(70,60);				
  				PutMessage("            ");
				SetPos(70,28);				// 
  				PutMessage("            ");
				SetPos(70,28);				// 
  				PutMessage("^");
				if (SetTouche==1){DizHeures++;}	
				if (SetTouche==2 && DizHeures ==0){DizHeures = 0;}		
				else if (SetTouche==2){DizHeures--;}
				if (DizHeures >2){ DizHeures = 2;}		
 				if (DizHeures ==2 && UnitHeures >3) {UnitHeures = 3;}									
 	            break;
				case 2:		// Unit Heures
 				SetPos(70,28);				
  				PutMessage("            ");
 				SetPos(76,28);				// 
  				PutMessage("^");
				if (SetTouche==1){UnitHeures++;}	
				if (SetTouche==2 && UnitHeures ==0){ UnitHeures = 0;}		
				else if(SetTouche==2 ){UnitHeures--;}
				if (DizHeures<2 && UnitHeures >9)  {UnitHeures = 9;}		
 				if (DizHeures ==2 && UnitHeures >3) {UnitHeures = 3;}		
	            break;

				case 3:		// Diz Minutes
 				SetPos(70,28);				//  D
  				PutMessage("            ");
 				SetPos(88,28);				// 
  				PutMessage("^");
				if (SetTouche==1){DizMinutes++;}	
				if (SetTouche==2 && DizMinutes ==0){DizMinutes = 0;}		
				else if (SetTouche==2){DizMinutes--;}
				if (DizMinutes >5){ DizMinutes = 5;}		
 	            break;
				case 4:		// Unit Minutes
 				SetPos(70,28);				// 
  				PutMessage("            ");
 				SetPos(94,28);				// 
  				PutMessage("^");
				if (SetTouche==1){UnitMinutes++;}	
				if (SetTouche==2 && UnitMinutes ==0){ UnitMinutes = 0;}
				else if (SetTouche==2){UnitMinutes--;}
				if (UnitMinutes >9)  {UnitMinutes = 9;}		
 	            break;
									
				case 5:		// Diz Secondes
				SetPos(70,28);				//  D
  				PutMessage("            ");
 				SetPos(106,28);				// 
  				PutMessage("^");
				if (SetTouche==1){DizSecondes++;}	
				if (SetTouche==2 && DizSecondes ==0){DizSecondes = 0;}		
				else if (SetTouche==2){DizSecondes--;}
				if (DizSecondes >5){ DizSecondes = 5;}		
 	            break;
				case 6:		// Unit Minutes
				SetPos(70,44);				// 
  				PutMessage("            ");
				SetPos(70,28);				// 
  				PutMessage("            ");
 				SetPos(112,28);				// 
  				PutMessage("^");
				if (SetTouche==1){UnitSecondes++;}	
				if (SetTouche==2 && UnitSecondes ==0){ UnitSecondes = 0;}
				else if (SetTouche==2){UnitSecondes--;}
				if (UnitSecondes >9)  {UnitSecondes = 9;}		
 	            break;

				case 7:		// Jour
 				SetPos(70,60);				// 
  				PutMessage("            ");
				SetPos(70,28);				// 
  				PutMessage("            ");
 				SetPos(70,44);				// 
  				PutMessage("^");
				if (SetTouche==1){Jour++;}	
				if (SetTouche==2 && Jour ==1){Jour = 1;}
				else if (SetTouche==2){Jour--;}
				if (Jour >7){Jour = 7;}					
 	            break;


				case 8:		// Diz date
  				SetPos(70,44);				// 
  				PutMessage("            ");
				SetPos(70,28);				// 
  				PutMessage("            ");
				SetPos(70,60);				// 
  				PutMessage("            ");
 				SetPos(70,60);				// 
  				PutMessage("^");
				if (SetTouche==1){DizDate++;}	
				if (SetTouche==2 && DizDate ==0){DizDate = 0;}		
				else if (SetTouche==2){DizDate--;}
				if (DizDate >3){ DizDate = 3;}		
 				if (DizDate ==3 && UnitDate >1) {UnitDate = 1;}		
 	            break;
				case 9:		// Unit Date
 				SetPos(70,60);				// 
  				PutMessage("            ");
 				SetPos(76,60);				// 
  				PutMessage("^");
				if (SetTouche==1){UnitDate++;}	
				if (SetTouche==2 && UnitDate ==0){ UnitDate = 0;}
				else if (SetTouche==2){UnitDate--;}
				if (DizDate<3 && UnitDate >9)  {UnitDate = 9;}		
 				if (DizDate ==3 && UnitDate >1) {UnitDate = 1;}		
 	            break;

				case 0x0A:		// Diz Mois
  				SetPos(70,44);				// 
 				PutMessage("            ");
 				SetPos(70,60);				// 
  				PutMessage("            ");
 				SetPos(88,60);				// 
  				PutMessage("^");
				if (SetTouche==1){DizMois++;}	
				if (SetTouche==2 && DizMois ==0){ DizMois = 0;}
				else if (SetTouche==2){DizMois--;}
				if (DizMois >1){ DizMois = 1;}		
 				if (DizMois ==1 && UnitMois >2) {UnitMois = 2;}		
 	            break;
				case 0x0B:		// Unit Mois
 				SetPos(70,60);				// 
  				PutMessage("            ");
 				SetPos(94,60);				// 
  				PutMessage("^");
				if (SetTouche==1){UnitMois++;}	
				if (SetTouche==2 && UnitMois ==0){ UnitMois = 0;}
				else if (SetTouche==2){UnitMois--;}
				if (DizMois<1 && UnitMois >9)  {UnitMois = 9;}		
 				if (DizMois ==1 && UnitMois >2) {UnitMois = 2;}		
 	            break;
				
				case 0x0C:		// Diz Annee
 				SetPos(70,60);				// 
  				PutMessage("            ");
 				SetPos(106,60);				// 
  				PutMessage("^");
				if (SetTouche==1){DizAnnee++;}	
				if (SetTouche==2){DizAnnee--;}
				if (DizAnnee >9){ DizAnnee = 9;}		
 	            break;
				case 0x0D:		// Unit Annee
				SetPos(70,28);				// 
  				PutMessage("            ");
 				SetPos(70,60);				// 
  				PutMessage("            ");
 				SetPos(112,60);				// 
  				PutMessage("^");
				if (SetTouche==1){UnitAnnee++;}	
				if (SetTouche==2){UnitAnnee--;}
				if (UnitAnnee >9){UnitAnnee = 9;}		
	            break;								
				}

				displayNum(70,20,DizHeures);  
				displayNum(76,20,UnitHeures);
  				PutMessage("-");
				displayNum(88,20,DizMinutes);
				displayNum(94,20,UnitMinutes);  				PutMessage("-");
				displayNum(106,20,DizSecondes);
				displayNum(112,20,UnitSecondes);
				SetPos(70,35);							
			    switch(Jour)
 				   {
				case 1:PutMessage("Dimanche ");
 	            break;
				case 2:PutMessage("Lundi    ");
                break;
				case 3:PutMessage("Mardi    ");
                break;
				case 4:PutMessage("Mercredi ");
                break;
				case 5:PutMessage("Jeudi    ");
                break;
				case 6:PutMessage("Vendredi ");
                break;
				case 7:PutMessage("Samedi   ");
                break;
    				}


				displayNum(70,50,DizDate);
				displayNum(76,50,UnitDate);
  				PutMessage("-");
				displayNum(88,50,DizMois);
				displayNum(94,50,UnitMois);  				PutMessage("-");
				displayNum(106,50,DizAnnee);
				displayNum(112,50,UnitAnnee);


				if	(touche_4==0)			// Encodage des valeurs de l'horloge et �criture dans le circuit DS1307
						{
					Dizaines=DizHeures;
					Unites= UnitHeures;
					encodeH();
					Heures = Tval;

					Dizaines=DizMinutes;
					Unites= UnitMinutes;
					encodeH();
					Minutes = Tval;

					Dizaines=DizSecondes;
					Unites= UnitSecondes;
					encodeH();
					Secondes = Tval;

					Dizaines=DizDate;
					Unites= UnitDate;
					encodeH();
					Date = Tval;

					Dizaines=DizMois;
					Unites= UnitMois;
					encodeH();
					Mois = Tval;

					Dizaines=DizAnnee;
					Unites= UnitAnnee;
					encodeH();
					Annee = Tval;

					
					reset_time(Heures,Minutes,Secondes,Jour,Date,Mois,Annee);
						}


				while (touche_4 == 0 ); 	// attente touche relach�e

//***********************************

				if	(touche_6==0)			// affichage de l'horloge
						{
					Aff=1;
					ClearScreen();								// Effacement de tout l'�cran
						}
				while(touche_6==0)			// affichage de l'horloge
						{
					DisplayTime();
						}
				if (Aff==1){  // pour r�afficher reglage horloge
				ClearScreen();					// Effacement de tout l'�cran
				PutMessage("REGLAGE HORLOGE");		
				hline(0,127,10) ;			// ligne horizontale

 				SetPos(5,20);				// 
  				PutMessage("HH-MM-SS");
 				SetPos(5,35);				// 
  				PutMessage("JOUR");
 				SetPos(5,50);				// 
  				PutMessage("DD-MM-AA");
				}	
//*************************************				

		}

}


void Horodatage_mesures()
{


				Tval = get_time(0x04);		//date
				decode();
					Tab_Horodatage[0]=Dizaines+48;
					Tab_Horodatage[1]=Unites+48;
				Tval = get_time(0x05);		//mois
				decode();
					Tab_Horodatage[2]=Dizaines+48;
					Tab_Horodatage[3]=Unites+48;
				Tval = get_time(0x06);		//ann�e
				decode();
					Tab_Horodatage[4]=Dizaines+48;
					Tab_Horodatage[5]=Unites+48;
				Tval = get_time(0x02);		//heures
				decode();
					Tab_Horodatage[6]=Dizaines+48;
					Tab_Horodatage[7]=Unites+48;
				Tval = get_time(0x01);		//minutes
				decode();
					Tab_Horodatage[8]=Dizaines+48;
					Tab_Horodatage[9]=Unites+48;


//				Save_tpa_i2c();

}



