//Interfacing RTC DS1307 



#include <i2c.h>

//Function Prototype

void horloge(void);
unsigned char Rd_ds1307_i2c_char( unsigned int adr_ds1307);
unsigned char Read_ds1307_I2C( unsigned char ControlByte, unsigned char HighAdd, unsigned char LowAdd, unsigned char *data, unsigned char length );
void Wr_ds1307_i2c_char( unsigned int adr_ds1307, char data_char);
unsigned char Write_ds1307_I2C( unsigned char ControlByte, unsigned char HighAdd, unsigned char LowAdd, unsigned char data );

unsigned char sec,min,hour,date,month,year;
//unsigned int adr_ds1307;
	unsigned char data,length ;


void horloge()
{

    IdleI2C();                      // ensure module is idle
    StartI2C();                     // initiate START condition
    WriteI2C(0xD0);
    WriteI2C(0x00);
    WriteI2C(0x80);    //CH = 1 Stop oscillator
    WriteI2C(0x00);    //Minute
    WriteI2C(0x01);    //Hour
    WriteI2C(0x01);    //Sunday
    WriteI2C(0x01);    //1Date
    WriteI2C(0x01);    //1 Jan
    WriteI2C(0x00);    //2000
    IdleI2C();        // F3=4K F0=1Hz 
    StopI2C(); 


    while(1)
    {
	IdleI2C(); 
     StartI2C(); 
    WriteI2C(0xD0);  // Write mode 
    WriteI2C(0x00); // Pointer=Start of Ram 
    RestartI2C(); 
  while ( SSP1CON2bits.RSEN );     // wait until re-start condition is over 
    WriteI2C(0xD1);  //Read 
   sec = ReadI2C(); 
//	getsI2C( data, length );
    NotAckI2C(); 
  while ( SSP1CON2bits.ACKEN );    // wait until ACK sequence is over 
    StopI2C(); 
//    AckI2C(); 
//    sec = ReadI2C(); 

     
//	*data = ReadI2C();
//	sec = ReadI2C();

	 sec = Rd_ds1307_i2c_char(0x00);
        min = Rd_ds1307_i2c_char(0x01);
        hour = Rd_ds1307_i2c_char(0x02);
        date = Rd_ds1307_i2c_char(0x04);
        month = Rd_ds1307_i2c_char(0x05);
        year = Rd_ds1307_i2c_char(0x06);
     }   
 

}




unsigned char Rd_ds1307_i2c_char( unsigned int adr_ds1307)
{
	unsigned char ControlByte ;
	unsigned char data ; // variable temporaire
	unsigned char HighAddres ;
	unsigned char LowAddres ;
	unsigned char length ; 
	unsigned char *ptr_data ;

	ptr_data = &data ;

	ControlByte = 0b11010000 ;      // WRITE

	length = 1 ;

	LowAddres  = adr_ds1307 ;
	HighAddres = adr_ds1307 >> 8 ;
	
	Read_ds1307_I2C( ControlByte, HighAddres, LowAddres, ptr_data, length ) ;	// LSB

	return (data) ; // memory_data;

} // fin Rd_ds1307_i2c_char





unsigned char Read_ds1307_I2C( unsigned char ControlByte, unsigned char HighAdd, unsigned char LowAdd, unsigned char *data, unsigned char length )

{  
  IdleI2C();                      // ensure module is idle
  StartI2C();                     // initiate START condition
  while ( SSP1CON2bits.SEN );      // wait until start condition is over 
  WriteI2C( ControlByte );        // write 1 byte 
  IdleI2C();                      // ensure module is idle
  WriteI2C( HighAdd );            // WRITE word address to EEPROM
  IdleI2C();                      // ensure module is idle
  while ( SSP1CON2bits.RSEN );     // wait until re-start condition is over 
  WriteI2C( LowAdd );             // WRITE word address to EEPROM
  IdleI2C();                      // ensure module is idle
  RestartI2C();                   // generate I2C bus restart condition
  while ( SSP1CON2bits.RSEN );     // wait until re-start condition is over 
  WriteI2C( ControlByte | 0x01 ); // WRITE 1 byte - R/W bit should be 1 for read
  IdleI2C();                      // ensure module is idle
  getsI2C( data, length );       // read in multiple bytes
  NotAckI2C();                    // send not ACK condition
  while ( SSP1CON2bits.ACKEN );    // wait until ACK sequence is over 
  StopI2C();                      // send STOP condition
  while ( SSP1CON2bits.PEN );      // wait until stop condition is over 
  return ( 0 );                   // return with no error

}



void Wr_ds1307_i2c_char( unsigned int adr_ds1307, char data_char)
{ 
	unsigned char ControlByte ; 
	unsigned char HighAddres ;
	unsigned char LowAddres ;

	ControlByte = 0b11010000 ;      // WRITE

	LowAddres = adr_ds1307 ;
	HighAddres = adr_ds1307 >> 8 ;
	
	Write_ds1307_I2C( ControlByte, HighAddres, LowAddres, data_char ) ;

} // fin adr_ds1307_i2c_char

unsigned char Write_ds1307_I2C( unsigned char ControlByte, unsigned char HighAdd, unsigned char LowAdd, unsigned char data )
{
  IdleI2C();                      // ensure module is idle
  StartI2C();                     // initiate START condition
  while ( SSP1CON2bits.SEN );      // wait until start condition is over 
  WriteI2C( ControlByte );        // write 1 byte - R/W bit should be 0
  IdleI2C();                      // ensure module is idle
  WriteI2C( HighAdd );            // write address byte to EEPROM
  IdleI2C();                      // ensure module is idle
  WriteI2C( LowAdd );             // write address byte to EEPROM
  IdleI2C();                      // ensure module is idle
  WriteI2C ( data );              // Write data byte to EEPROM
  IdleI2C();                      // ensure module is idle
  StopI2C();                      // send STOP condition
  while ( SSP1CON2bits.PEN );      // wait until stop condition is over 
  while (EEAckPolling(ControlByte));  //Wait for write cycle to complete
  return ( 0 );                   // return with no error
}









