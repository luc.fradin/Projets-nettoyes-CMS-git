// Analogique.c


#include <p18f8723.h>				

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <delays.h>
#include <math.h>
#include <analog.h>

#define AN0		0b00000000			// V_ligne_AM1
#define AN1		0b00000100			// V_ligne_AM2
#define AN2		0b00001000			// V_ligne_AV1_A (courant de repos sur TPA  et tension sur TPR => voies impaires )
#define AN3		0b00001100			// V_ligne_AV2_A (courant de repos sur TPA  et tension sur TPR => voies paires )
#define AN4		0b00010000			// I_conso_1
#define AN5		0b00010100			// I_conso_2
#define AN6		0b00011000			// I_modul_1
#define AN7		0b00011100			// I_modul_2
#define AN8		0b00100000			// SEL 0   	lecture de l'entr�e en analogique!
#define AN9		0b00100100			// SEL 1	lecture de l'entr�e en analogique!
#define AN10	0b00101000			// SEL 2	lecture de l'entr�e en analogique!
#define AN11	0b00101100			// <===  entr�e analogique LIBRE
#define AN12	0b00110000			// V_48V_1 
#define AN13	0b00110100			// V_48V_2 
#define AN14	0b00111000			// V_ligne_AV1_B  
#define AN15	0b00111100			// V_ligne_AV2_B



float V_48v_Calc_1;
float V_48v_Calc_2;
float V_Ligne_AM_Calc_1;
float V_Ligne_AM_Calc_2;
float V_Ligne_AV_Calc;
float Val_Temp;
float V_Ligne_AV_Calc_1;
float V_Ligne_AV_Calc_2;
float V_Ligne_AVB_Calc_1;
float V_Ligne_AVB_Calc_2;

float COEFF; //coeff correcteur pour mesure


unsigned int I_Conso_1;
unsigned int I_Conso_2;
unsigned int I_Modul_1;
unsigned int I_Modul_2;
unsigned int I_Repos_1;
unsigned int I_Repos_2;
unsigned int V_Ana;   // valeur convertisseur analogique
float V_Resistif_1;   // valeur tension AV_1 pour capteur resistif  sur voies impaires
float V_Resistif_2;   // valeur tension AV_2 pour capteur resistif  sur voies paires



const double q=0.001220703125;



#define V_Can_Max  	4095   	// max 4095 pour le convertisseur (12bits)
#define I_Conso_Max  150    //<==� revoir 		 multipli� par 10 et affichage 			avant modif(200=20mA)
#define I_Modul_Max  150	// car multipli� par 10 et affichage 10.0

#define V_CAN 5


#define I_Repos_Max  619	//	�A avec nouveau gain de 8.50
//calcul=> 5000mV(sortie de l'ampli)/8.5(gain)/0.95Kohm (r�sistance de mesure) = 618.9



#define V_48v_Max 	 80   	// max 80V sur V_48V avec gain ampli 1/16
#define V_48v_min    50    // = 50volts => si tension inf�rieure on stoppe le programme et on affiche anomalie 48V



// Coefficients de correction
unsigned int Coeff_I_Modul1,Coeff_I_Modul2;      	// pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53
unsigned int Coeff_I_Conso1,Coeff_I_Conso2;   	// pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53
unsigned int Coeff_I_Repos1,Coeff_I_Repos2;    	// pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53
unsigned int Coeff_Resis1,Coeff_Resis2; 


#define V_1K_Max 	 5000   	// � revoir 
// la valeur max V_1K_Max est calcul�e pour une tension de 56V et 100K sur R1K et un gain de 8.50 pour l'ampli  AVEC NOUVEAU GAIN r88=150k et r89=20k  => (150/20)+1 = 8.50

unsigned char Num_Carte; // num�ro(position sur carte m�re de la carte relais


/* Resume allocation of romdata into the default section */
//# pragma romdata
#pragma		code	USER_ROM   // retour � la zone de code*/



// --------- Mesure batterie ----------------
//void Mesure_V_Batt (void)
//{
//		V_Ana = acqui_ana_16 (AN0) ;    //(16 mesures)
//			V_Batt_Calc = V_Ana;
//		V_Batt_Calc = ((V_Batt_Calc * V_Batt_Max)  / V_Can_Max) ;
//} // fin



// --------- Mesure ligne en amont du capteur ----------------
void Mesure_V_Ligne_AVB_1()								// Acquisition de la valeur CAN de la tension ligne AMont capteur
{
COEFF=1.0900;
		V_Ana = acqui_ana_16 (AN14) ;    //(16 mesures)
			Val_Temp = V_Ana;
		Val_Temp =q*Val_Temp;
		V_Ligne_AVB_Calc_1 =(Val_Temp)*COEFF; 
Val_Temp = V_Ana;
} // fin



// --------- Mesure ligne en amont du capteur ----------------
void Mesure_V_Ligne_AVB_2()								// Acquisition de la valeur CAN de la tension ligne AMont capteur
{
COEFF=1.0900;
		V_Ana = acqui_ana_16 (AN15) ;    //(16 mesures)
			Val_Temp = V_Ana;
		Val_Temp =q*Val_Temp;
		V_Ligne_AVB_Calc_2 =(Val_Temp)*COEFF; //16 gain (150K+10K)/10K;
Val_Temp = V_Ana;
} // fin




// --------- Mesure ligne en amont du capteur ----------------
void Mesure_V_Ligne_AM_1()								// Acquisition de la valeur CAN de la tension ligne AMont capteur
{
		V_Ana = acqui_ana_16 (AN0) ;    //(16 mesures)
			Val_Temp = V_Ana;
		Val_Temp =q*Val_Temp;
		V_Ligne_AM_Calc_1 =(Val_Temp*16); //16 gain (150K+10K)/10K
Val_Temp = V_Ana;
} // fin
// --------- Mesure ligne en amont du capteur ----------------
void Mesure_V_Ligne_AM_2()								// Acquisition de la valeur CAN de la tension ligne AMont capteur
{
		V_Ana = acqui_ana_16 (AN1) ;    //(16 mesures)
			Val_Temp = V_Ana;
		Val_Temp =q*Val_Temp;
		V_Ligne_AM_Calc_2 =(Val_Temp*16); //16 gain (150K+10K)/10K
Val_Temp = V_Ana;
} // fin



// --------- Mesure courant de repos par ligne en aval du capteur ----------------
void Mesure_I_Repos_1()								// Acquisition de la valeur CAN de la tension ligne AVal capteur �A
{
		V_Ana = 0x0000;
		Val_Temp = 0x0000;
		I_Repos_1= 0x0000;

		V_Ana = acqui_ana_16_plus (AN2) ;    //(16 mesures avec tempo entre les mesures)
		Val_Temp = V_Ana;
		Val_Temp =q*Val_Temp;
		Val_Temp = (Val_Temp*Coeff_I_Repos1/1) ;
		I_Repos_1= (int)Val_Temp;

} // fin

 
// --------- Mesure courant de repos par ligne en aval du capteur ----------------
void Mesure_I_Repos_2()								// Acquisition de la valeur CAN de la tension ligne AVal capteur �A
{
		V_Ana = 0x0000;
		Val_Temp = 0x0000;
		I_Repos_2= 0x0000;

		V_Ana = acqui_ana_16_plus (AN3) ;    //(16 mesures avec tempo entre les mesures)
		Val_Temp = V_Ana;
		Val_Temp =q*Val_Temp;
		Val_Temp = (Val_Temp*Coeff_I_Repos2/1) ;
		I_Repos_2= (int)Val_Temp;

} // fin

 

// --------- Mesure tension ligne en aval du capteur pour transducteurs r�sistifs----------------
void Mesure_V_Resistif_1()								// Acquisition de la valeur CAN de la tension ligne AVal capteur
{
		V_Ana = acqui_ana_16_plus (AN2) ;    //(16 mesures avec tempo entre les mesures)
		Val_Temp = V_Ana;
		Val_Temp =q*Val_Temp;	
		Val_Temp=Val_Temp*Coeff_Resis1;
		V_Resistif_1= Val_Temp;

}

// --------- Mesure tension ligne en aval du capteur pour transducteurs r�sistifs----------------
void Mesure_V_Resistif_2()								// Acquisition de la valeur CAN de la tension ligne AVal capteur
{
		V_Ana = acqui_ana_16_plus (AN3) ;    //(16 mesures avec tempo entre les mesures)
		Val_Temp = V_Ana;
		Val_Temp =q*Val_Temp;
		Val_Temp=Val_Temp*Coeff_Resis2;
		V_Resistif_2= Val_Temp;

}



// --------- Mesure courant de consommation du capteur ----------------
void Mesure_I_Conso_1()								// Acquisition de la valeur CAN du courant de consommation en MA
{


		V_Ana = acqui_ana_16_plus (AN4) ;    //(16 mesures avec tempo entre les mesures)
		Val_Temp =q*V_Ana;
		Val_Temp=10*	Val_Temp*Coeff_I_Conso1 / 100;
		I_Conso_1 = (char)Val_Temp;


//		I_Conso = (int)Val_Temp;

	
}
// --------- Mesure courant de consommation du capteur ----------------
void Mesure_I_Conso_2()								// Acquisition de la valeur CAN du courant de consommation en MAx10
{

	V_Ana = acqui_ana_16_plus (AN5) ;    //(16 mesures avec tempo entre les mesures)
		Val_Temp = q*V_Ana;
		Val_Temp =10 * Val_Temp * Coeff_I_Conso2 / 100;     // AJUSTEMENT GAIN
		I_Conso_2 = (char)Val_Temp;
//		I_Conso = (int)Val_Temp;
}


// --------- Mesure courant de modulation du capteur ----------------
void Mesure_I_Modul_1 ()							// Acquisition de la valeur CAN du courant de modulation MA*10
{
Val_Temp=0;
		V_Ana = acqui_ana_16_plus (AN6) ;    //(16 mesures avec tempo entre les mesures)
		Val_Temp = V_Ana;
		Val_Temp =q*Val_Temp;
		Val_Temp=10*Val_Temp * Coeff_I_Modul1 / 100;     // AJUSTEMENT GAIN

		I_Modul_1 = (char)Val_Temp;
		//I_Modul_1=10*I_Modul_1;

}
// --------- Mesure courant de modulation du capteur ----------------
void Mesure_I_Modul_2 ()							// Acquisition de la valeur CAN du courant de modulation MA*10
{
Val_Temp=0;
		V_Ana = acqui_ana_16_plus (AN7) ;    //(16 mesures avec tempo entre les mesures)
		Val_Temp = V_Ana;
		Val_Temp =q*Val_Temp;
		Val_Temp=10*Val_Temp * Coeff_I_Modul2 / 100;     // AJUSTEMENT GAIN
		I_Modul_2 = (char)Val_Temp;
		//I_Modul_2=10*I_Modul_2;
}

// --------- Mesure courant de modulation du capteur ----------------
void Mes_Num_Carte ()							// Acquisition de la valeur pour d�finir la position de la carte entre 1 et 5
{
unsigned int V_Ana0;
unsigned int V_Ana1;
unsigned int V_Ana2;
unsigned char Sel_A0;
unsigned char Sel_A1;
unsigned char Sel_A2;


 		Sel_A0=0;
 		Sel_A1=0;
		Sel_A2=0;

		V_Ana0 = acqui_ana_4 (AN8) ;    //(4 mesures avec tempo entre les mesures)
		V_Ana1 = acqui_ana_4 (AN9) ;    //(4 mesures avec tempo entre les mesures)
		V_Ana2 = acqui_ana_4 (AN10) ;    //(4 mesures avec tempo entre les mesures)
		if (V_Ana0>2047){Sel_A0=1;}
		if (V_Ana1>2047){Sel_A1=2;}
		if (V_Ana2>2047){Sel_A2=4;}
			Num_Carte=Sel_A0+Sel_A1+Sel_A2;
}
// --------- Mesure convertisseur 48V_1 voies impaires ----------------
void Mesure_V_48v_1()
//void Mesure_V_48v (float V_48_Calc)
{
COEFF=15.90;
		V_Ana = acqui_ana_16 (AN12) ;    //(16 mesures)
		Val_Temp = V_Ana;
			Val_Temp =q*Val_Temp;
	V_48v_Calc_1 =(Val_Temp*COEFF); //16 gain (150K+10K)/10K
Val_Temp = V_Ana;	
} // fin
// --------- Mesure convertisseur 48V_2 voies paires ----------------
void Mesure_V_48v_2 ()
//void Mesure_V_48v (float V_48_Calc)
{
COEFF=15.90;
		V_Ana = acqui_ana_16 (AN13) ;    //(16 mesures)
		Val_Temp = V_Ana;
			Val_Temp =q*Val_Temp;
		V_48v_Calc_2 =(Val_Temp*COEFF); //16 gain (150K+10K)/10K
Val_Temp = V_Ana;
} // fin



 // fin







//---------------- Acquisition mesure analogique -------------------------
unsigned int acqui_ana (unsigned char adcon )
{


//LED1_ALIM_VL = ~PORTJbits.RJ1 ;		// test
	ADCON0 = adcon ;  	// adress + abort la mesure en cours
	_ADON = 1 ;	
	_ADGO = 1 ;
	
	while ( _ADDONE ) 
	{
	//	Nop();
	}

	return ADRES ;	
}	// fin acqui_ana ()

//---------------- Acquisition mesure analogique -------------------------
unsigned int acqui_ana_2 (unsigned char adcon )
{
//	unsigned long II;
	unsigned int I;
//	unsigned int N ; // Variable usage general
//	int i ; // Variable usage general

//	N = 0 ;
	I = 0 ;
//	II = 0;
//	for ( i = 0; i < 0x2; i++ )	// 2 MESURES POUR LA MOYENNE
//	{
//		N = acqui_ana (adcon) ;
//		II = II + N;
//	}
//	II >>= 1 ;		// Moyenne pour 2 mesures

//	N = (int) II;		// sauvegarde de Long en Int
//	return (N);

// MODIF 2 ET 1
//	I = acqui_ana (adcon) ;
//	I += acqui_ana (adcon) ;
//	I >>= 1 ;		// Moyenne pour 2 mesures
//	return (I);

//	return ADRES ;	

//LED1_ALIM_VL = ~PORTJbits.RJ1 ;		// test

	ADCON0 = adcon ;  	// adress + abort la mesure en cours
	_ADON = 1 ;	
	_ADGO = 1 ;
	
	while ( _ADDONE ) 
	{
	//	Nop();
	}
	I = ADRES ;

	_ADGO = 1 ;
	while ( _ADDONE ) 
	{
	//	Nop();
	}

	I += ADRES ;
	I >>= 1 ;		// Moyenne pour 2 mesures
	return (I);

//LED1_ALIM_VL = ~PORTJbits.RJ1 ;		// test

}	// fin acqui_ana_2()
//---------------- Acquisition mesure analogique -------------------------
unsigned int acqui_ana_4 (unsigned char adcon )
{
	unsigned long II;
	unsigned int N ; // Variable usage general
	int i ; // Variable usage general

	N = 0 ;
	II = 0;
	for ( i = 0; i < 0x4; i++ )	// 4 MESURES POUR LA MOYENNE
	{
		N = acqui_ana (adcon) ;
		II = II + N;
	}
	II >>= 2 ;		// Moyenne pour 4 mesures

	N = (int) II;		// sauvegarde de Long en Int
	return (N);

}	// fin acqui_ana_4()
/*----------------- acqui_ana_16 -----------------------------------------------*/
unsigned int acqui_ana_16 (unsigned char adcon )
{
	unsigned long II;
	unsigned int N ; // Variable usage general
	int i ; // Variable usage general



	N = 0 ;
	II = 0;
	for ( i = 0; i < 0x10; i++ )	// 16 MESURES POUR LA MOYENNE
	{
		N = acqui_ana (adcon) ;
		II = II + N;
	}
	II >>= 4 ;		// Moyenne pour 16 mesures

	N = (int) II;		// sauvegarde de Long en Int
	return (N);



}

/*----------------- acqui_ana_16_plus -----------------------------------------------*/
		//16 mesures avec tempo entre les mesures)
unsigned int acqui_ana_16_plus (unsigned char adcon )
{



	unsigned long II;
	unsigned int N ; // Variable usage general
	int i ; // Variable usage general



	N = 0 ;
	II = 0;

	for ( i = 0; i < 0x10; i++ )	// 16 MESURES POUR LA MOYENNE
	{
		N = acqui_ana (adcon) ;
		II = II + N;
	Delay10KTCYx(1);
	}
	II >>= 4 ;		// Moyenne pour 16 mesures

	N = (int) II;		// sauvegarde de Long en Int
	return (N);



}




	// fin acqui_ana_16_plus ()





 




////////////////////////////////////////////////////////////////////////////////////////////////

