
#include <inituc.h>
#include <ecrans.h>
#include <GLCD.h>
#include <delay.h>						/* Defs DELAY functions */
#include <ftoa.h>						/* Adapt Chaine de caract�re */
#include <rtc.h>						/* Adapt Chaine de caract�re */
#include <clock.h>
#include <codage.h>
#include <mesures.h>

extern unsigned int Unites,Dizaines;        // Global var.
extern unsigned int Tval;

extern unsigned char 	Tab_Horodatage[10];		//10 octets	 //10 caract�res	  
extern unsigned char 	Tab_Constit[17][10];	//170 octets //constitution 
extern unsigned char 	Tab_VoieRobTete[10];	//10 octets 	VOIE 2 + ROB 2 + TETE 6	 
extern unsigned char 	Tab_Central[14];		//14 octets	 //14 caract�res	  
extern unsigned int 	Tab_Pression[17];		//34 octets   en 0 valeur du d�bit  si ffff => pas de d�bitm�tre
extern unsigned int 	Tab_I_Repos[17];		//34 octets 17 caract�res	  1 d�bitmetre et 16 TP
extern unsigned char 	Tab_I_Modul[17];		//17 octets	17 caract�res	  1 d�bitmetre et 16 TP
extern unsigned int 	Tab_I_Conso[17];		//34 octets	17 caract�res	  1 d�bitmetre et 16 TP
extern unsigned char 	Tab_Etat[17];			//17 octets  
									  // total: //347 octets

extern unsigned char F_ecran_adres;	 	// flag pour affichage �cran TPA r�duit(0) ou complet(1)
extern unsigned char F_aff_sauve_tpa; 	// flag pour m�mo affichage: Sauvegarde ? (sauvegarde des mesures pour TP adressables)
extern unsigned char F_sauve_mes_tpa; 	// flag pour m�mo sauvegarde valid�e des mesures des tp adressables
extern unsigned char F_retour_sauve;	// flag pour indiquer fin de sauvegarde

extern unsigned char F_memo_back_light; 	// flag pour m�mo sauvegarde position back_light				
extern unsigned char F_mode_scrut; 		// flag pour signaler d�but et fin de scrutation
extern unsigned char F_it_chenillard; 	// flag pour chenillard
extern unsigned char F_sup3mes_tpa;	 	// flag pour indiquer si ecran complet ou r�duit et > � 3 mesures

extern unsigned char 	Tab_Code[17];				//17 octets 17 caract�res	  1 d�bitmetre et 16 TP 

		
unsigned char Num_Ligne; 	// num�ro de la ligne de mesure pour affichage
extern unsigned char Nb_TPA_ok; // indique le nb de capteurs qui ont r�pondus lors d'un interrogation  

#define def_i_conso 	80 		// defaut si > 8.0 mA
#define def_i_modul  	40 		// defaut si > 4.0 mA
#define def_i_repos  	150 	// defaut si > 150 �A
#define def_pression_B  800 	// defaut si < 800 mbar
#define def_pression_H  1800 	// defaut si > 1800 mbar


#define def_tampon_H  4200 		// defaut si pression tampon > 4200 mbar
#define def_tampon_B  1500 		// defaut si pression tampon < 1500 mbar
#define def_cable_H    550 		// defaut si pression cable > 550 mbar
#define def_cable_B    450 		// defaut si pression cable < 450 mbar
#define def_secours_B1  3000 	// defaut si pression secours < 3000 mbar
#define def_secours_B2  1500 	// defaut si pression secours < 3000 mbar
#define def_rosee_H     253 	// defaut si pint de ros�e > -20�C   253�K



//extern unsigned int V_Batt;
unsigned int V_Batt;
extern float V_Batt_Calc;
 float V_Batt_Aff;

extern unsigned int Code_Lect;
extern unsigned int Code_Prog;
extern unsigned char Mode_Scrolling;

//unsigned int Hval;
//unsigned int Dizaines,Unites;
unsigned char eff=0;
extern unsigned char Pres_Debit;  // indique pr�sence d�bitm�tre pour affichage
extern unsigned char mes_debit; // pour indiquer (dans la boucle de mesure des pcpg) une mesure du d�bitm�tre 

extern unsigned int Freq_R;
unsigned Etat_tpa;				// indique l'�tat des tpa apres mesure (pr�sents ok, absent, d�fauts...)
unsigned int debit_cable;		// D�bit mesur� en cours



// MENUS
#pragma		code	USER_ROM   // retour � la zone de code

void menu_principal()
{
				ClearScreen();								// Effacement de tout l'�cran
				niveau_batt();
				SetPos(10,10);								// Positionner le curseur
				PutMessage(" Menu Principal ");					// Ecrire un message  nu_retour
				hline(0,127,20) ;							// R�aliser une ligne horizontale
				SetPos(10,30);								// Positionner le curseur
				PutMessage(" TP Adressables ");					// Ecrire un message  nu_retour
 				SetPos(10,38);								// Positionner le curseur
				PutMessage(" TP Resistifs   ");					// Ecrire un message
 				SetPos(10,46);								// Positionner le curseur
				PutMessage(" Debitmetres    ");				  		// Ecrire un message  
 				SetPos(10,54);								// Positionner le curseur
				PutMessage(" Groupe         ");				  		// Ecrire un message  
				SetPos(10,56);								// Positionner le curseur
				PutMessage(" Parametrages   ");				  		// Ecrire un message  

}
void menu_principal_C0()
{
 				SetPos(10,38);								// Positionner le curseur
				PutMessage(" TP Resistifs    ");					// Ecrire un message					
				SetPos(10,30);								// Positionner le curseur
				PutMessageInv(" TP Adressables");					// Ecrire un message  nu_retour			
}	
				
void menu_principal_C1()
{
				SetPos(10,30);								// Positionner le curseur
				PutMessage(" TP Adressables");					// Ecrire un message  nu_retour
				SetPos(10,46);								// Positionner le curseur
				PutMessage(" Debitmetres    ");				  		// Ecrire un message  
	 			SetPos(10,38);								// Positionner le curseur
				PutMessageInv(" TP Resistifs    ");					// Ecrire un message					
}	
				
void menu_principal_C2()
{
 				SetPos(10,38);								// Positionner le curseur
				PutMessage   (" TP Resistifs    ");					// Ecrire un message
				SetPos(10,54);								// Positionner le curseur
				PutMessage   (" Groupe          ");				  		// Ecrire un message  
				SetPos(10,46);								// Positionner le curseur
				PutMessageInv(" Debitmetres    ");				  		// Ecrire un message  
}
					
void menu_principal_C3()
{
				SetPos(10,56);								// Positionner le curseur
				PutMessage   (" Parametrages  ");				  		// Ecrire un message  
 				SetPos(10,46);								// Positionner le curseur
				PutMessage   (" Debitmetres    ");				  		// Ecrire un message  
				SetPos(10,54);								// Positionner le curseur
				PutMessageInv(" Groupe         ");				  		// Ecrire un message  
}	
void menu_principal_C4()
{
				SetPos(10,54);								// Positionner le curseur
				PutMessage   (" Groupe          ");				  		// Ecrire un message  
				SetPos(10,56);								// Positionner le curseur
				PutMessageInv(" Parametrages  ");				  		// Ecrire un message  
}					

//******************************************************************************************************

void menu_TPAD()
{
				ClearScreen ();
				niveau_batt();
				SetPos(10,10);								// Positionner le curseur
				PutMessage(" TP Adressables ");					// Ecrire un message  nu_retour
				hline(0,127,20) ;							// R�aliser une ligne horizontale
				SetPos(10,30);								// Positionner le curseur
				PutMessage(" Mesure     ");					// Ecrire un message
				SetPos(10,38);								// Positionner le curseur
				PutMessage(" Codage     ");				  		// Ecrire un message  
 				SetPos(10,46);								// Positionner le curseur
				PutMessage(" Lecture Memoire");				  		// Ecrire un message  
//				SetPos(10,54);								// Positionner le curseur
//				PutMessage(" Retour  <--    ");				  		// Ecrire un message  
}
void menu_TPAD_C0()
{
 				SetPos(10,38);								// Positionner le curseur
				PutMessage(" Codage     ");					// Ecrire un message					
				SetPos(10,30);								// Positionner le curseur
				PutMessageInv(" Mesure     ");					// Ecrire un message  nu_retour			
}	
				
void menu_TPAD_C1()
{
				SetPos(10,30);								// Positionner le curseur
				PutMessage(" Mesure     ");					// Ecrire un message  nu_retour
				SetPos(10,46);								// Positionner le curseur
				PutMessage(" Lecture Memoire");				  		// Ecrire un message  
	 			SetPos(10,38);								// Positionner le curseur
				PutMessageInv(" Codage     ");					// Ecrire un message					
}	
				
void menu_TPAD_C2()
{
 				SetPos(10,38);								// Positionner le curseur
				PutMessage(" Codage     ");					// Ecrire un message
//				SetPos(10,54);								// Positionner le curseur
//				PutMessage(" Retour  <--    ");				  		// Ecrire un message  
				SetPos(10,46);								// Positionner le curseur
				PutMessageInv(" Lecture Memoire");				  		// Ecrire un message  
}
					
//void menu_TPAD_C3()
//{
 //				SetPos(10,46);								// Positionner le curseur
//				PutMessage(" Lecture Memoire");				  		// Ecrire un message  
//				SetPos(10,54);								// Positionner le curseur
//				PutMessageInv(" Retour  <--    ");				  		// Ecrire un message  
//}	


// FIN  MENUS




// ECRANS DE MESURES TPA pour afficher en mode scrolling

void Scrolling_Ecran_tpa()
{
unsigned char tp;
unsigned char Ligne;		// position x Ligne d'�criture dans le tableau
unsigned char ltp;

				tp = Num_Ligne;
				ClearScreen ();

//				if (Num_Ligne > 3)
//					{
//				F_sup3mes_tpa = 1;   //flag pour m�moriser si sup � 3 mesures
//					}
//				else
//					{
//				F_sup3mes_tpa = 0;   //flag pour m�moriser si sup � 3 mesures
//					}

		if (Num_Ligne >= 3 || Num_Ligne == 0)
			{
	     	    if (F_ecran_adres)						// affichage �cran complet (bit = 1)
					{
				ecran_adressables_complet();//				
				if (Num_Ligne != 0){
				Affiche_complet(tp-2,1,Ligne);
				Affiche_complet(tp-1,2,Ligne);
				Affiche_complet(tp,3,Ligne);
									}											
					}
					else
					{
				ecran_adressables_reduit();//				
				if (Num_Ligne != 0){
				Affiche_reduit(tp-2,1,Ligne);
				Affiche_reduit(tp-1,2,Ligne);
				Affiche_reduit(tp,3,Ligne);						
									}											
					}
			}
		else if(Num_Ligne == 2)
			{

	     	    if (F_ecran_adres)						// affichage �cran complet (bit = 1)
					{
				ecran_adressables_complet();//				
				if (Num_Ligne != 0){
//				Affiche_complet(tp-2,1,Ligne);
				Affiche_complet(tp-1,1,Ligne);
				Affiche_complet(tp,2,Ligne);
									}											
					}
					else
					{
				ecran_adressables_reduit();//				
				if (Num_Ligne != 0){
//				Affiche_reduit(tp-2,1,Ligne);
				Affiche_reduit(tp-1,1,Ligne);
				Affiche_reduit(tp,2,Ligne);						
									}											
					}
			}
		else if(Num_Ligne == 1)
			{

	     	    if (F_ecran_adres)						// affichage �cran complet (bit = 1)
					{
				ecran_adressables_complet();//				
				if (Num_Ligne != 0){
				Affiche_complet(tp,1,Ligne);
									}											
					}
					else
					{
				ecran_adressables_reduit();//				
				if (Num_Ligne != 0){
				Affiche_reduit(tp,1,Ligne);						
									}											
					}
			}


// AFFICHAGE MESURE �cran reduit
/*
				if (Num_Ligne > 3)
				{
				F_sup3mes_tpa = 1;   //flag pour m�moriser si sup � 3 mesures
				}
				else 
				{
				ltp = Num_Ligne;
				Affiche_reduit(tp,ltp,Ligne);
				F_sup3mes_tpa = 0;   //flag pour m�moriser si sup � 3 mesures
				}
*/

}






void ecran_adressables_complet()
{
				niveau_batt();
				if (Pres_Debit==0) //si == 1 => d�bitm�tre pr�sent => pas d'affichage TP Adressable
					{
				SetPos(5,0);
  				PutMessage("TP Adressables   ");		// Ecrire un message  nu_retour
					}
				SetPos(1,16);
  				PutMessage("TP");		// Ecrire un message  nu_retour
				SetPos(16,16);
  				PutMessage("Pres");	// Ecrire un message  nu_retour
				SetPos(40,16);
  				PutMessage("Repo");	// Ecrire un message  nu_retour
				SetPos(62,16);
  				PutMessage("Cons");	// Ecrire un message  nu_retour
				SetPos(85,16);
  				PutMessage("Modu");	// Ecrire un message  nu_retour
				SetPos(110,16);
  				PutMessage("Etat");	// Ecrire un message  nu_retour
				SetPos(16,24);
  				PutMessage("mbar");	// Ecrire un message  nu_retour
				SetPos(44,24);
  				PutMessage("uA");	// Ecrire un message  nu_retour
				SetPos(66,24);
  				PutMessage("mA");	// Ecrire un message  nu_retour
				SetPos(90,24);
  				PutMessage("mA");	// Ecrire un message  nu_retour


				hline(0,128,14) ;							// ligne horizontale
				hline(0,128,30) ;							// "
				vline (13,15,56);							// ligne verticale
				vline (39,15,56);							// "
				vline (61,15,56);							// ligne verticale
				vline (84,15,56);							// "
				vline (108,15,56);							// "

/*test
				SetPos(5,8);
  				displaySmallNum(5,8,1);	// Ecrire un message  nu_retour
  				displaySmallNum(10,8,2);	// Ecrire un message  nu_retour
  				displaySmallNum(15,8,3);	// Ecrire un message  nu_retour

fin test  */



}  // fin �cran adressables	complet

void ecran_adressables_reduit()
{

				niveau_batt();

				if (Pres_Debit==0) //si == 1 => d�bitm�tre pr�sent => pas d'affichage TP Adressable
					{
				SetPos(5,0);
  				PutMessage("TP Adressables   ");		// Ecrire un message  nu_retour
					}
				SetPos(7,16);
  				PutMessage("TP");		// Ecrire un message  nu_retour
				SetPos(27,16);
  				PutMessage("mbar");	
				SetPos(55,16);
  				PutMessage("Etat");	// Ecrire un message  nu_retour
				hline(2,76,14) ;							// ligne horizontale
				hline(2,75,28) ;							// "
				vline (2,15,56);							// ligne verticale
				vline (22,15,56);							// ligne verticale
				vline (52,15,56);							// "
				vline (75,15,56);							// "

				SetPos(82,16);
  				PutMessage("Presents:");	// Ecrire un message  nu_retour
				box(77,14,127,56);						//x1,y1,x2,y2
//				displaySmallNum(8,8,2);


/* 			
				displaySmallNum(83,24,1);
// 				displaySmallNum(94,24,2);
				SetPos(94,24);
 				PutMessage("-");
				displaySmallNum(105,24,3);
				displaySmallNum(116,24,4);

				displaySmallNum(83,32,5);
 				displaySmallNum(94,32,6);
				displaySmallNum(105,32,7);
				displaySmallNum(116,32,8);

				displaySmallNum(82,40,9);
 				displaySmallNum(90,40,10);
				displaySmallNum(103,40,11);
				displaySmallNum(115,40,12);

				displaySmallNum(78,48,13);
 				displaySmallNum(90,48,14);
				displaySmallNum(104,48,15);
				displaySmallNum(116,48,16); 
*/
}  // fin �cran adressables	r�duit



void ecran_aff_TPA_reduit()  //�cran affichage TPadressables mode r�duit
{
unsigned char tp;
unsigned char Ligne;		// position x Ligne d'�criture dans le tableau
unsigned char ltp;

				tp = Num_Ligne;

// AFFICHAGE MESURE �cran reduit

				if (Num_Ligne > 3)
				{
				F_sup3mes_tpa = 1;   //flag pour m�moriser si sup � 3 mesures
				ClearScreen ();
				SetPos(0,60);								// Positionne le curseur
				PutMessage("Mesures en cours");				// 17 caract�res	
				ecran_adressables_reduit();//				
				Affiche_reduit(tp-2,1,Ligne);
				Affiche_reduit(tp-1,2,Ligne);
				Affiche_reduit(tp,3,Ligne);						
				}
				else 
				{
				ltp = Num_Ligne;
				Affiche_reduit(tp,ltp,Ligne);
				F_sup3mes_tpa = 0;   //flag pour m�moriser si sup � 3 mesures
				}
//PREMIERE MESURE �cran r�duit
//	  				displayNum (20,Ligne,Tab_Code[tp]);
//					displayNum (42,Ligne,Tab_Pression[tp]);					// Ecrire un INT en "normal" (-> Chiffres + Lettres )
//					SetPos(78,Ligne);
///*******					switch_Etat(Etat);
//FIN PREMIERE MESURE �cran r�duit
}


void Affiche_reduit(char tp,char ltp,char Ligne)
{
unsigned char n_tp;  // pour afficher tp pr�sents ou absents
				n_tp=0;


				switch (ltp)
					{
				case 0:
	  				// N� Ligne utilis�e pour d�bitm�tres
				break;
				case 1:
	  			Ligne = 35 ;	// position x Ligne d'�criture dans le tableau
				break;
				case 2:
	  			Ligne = 44 ;	// position x Ligne d'�criture dans le tableau
				break;
				case 3:
	  			Ligne = 51 ;	// position x Ligne d'�criture dans le tableau
				break;
					}

				Etat_tpa = 0x00;		// pour affichage commentaire de la case Etat

//if ((Tab_Code[tp]==0 && Tab_I_Conso[tp] > 20 && Tab_Pression[0] !=0)||Pres_Debit == 1)  //pr�sence d�bitm�tre
				Calcul_Deb (); // mise � l'�chelle du d�bit
if (Tab_Code[tp]==0 &&	Pres_Debit == 1)  //  									<===== � supprimer (test)??
	{ 
	// ici cadre d�bitm�tre et affichage valeur 
				SetPos(5,0);
  				PutMessage("DEBIT:            ");		
				displayNum (50,0,debit_cable);				// DEBIT  affichage normal
  				PutMessage(" L/H");
	}
	else if (Tab_Code[tp]==0 ||	Pres_Debit == 0)
	{
				SetPos(5,0);
  				PutMessage("Debitmetre Absent");
	}

///		if (Tab_Code[tp]>0 &&Tab_Pression[tp]!=0)   // pour ne pas afficher le capteur 0(d�bitm�tre) en premiere ligne du tableau et n'afficher que les capteurs pr�sents
		if (Tab_Code[tp]>0 )   // pour ne pas afficher le capteur 0(d�bitm�tre) en premiere ligne du tableau des �tats
			{



				if (Tab_Code[tp]>=10)   // pour bien positionner les chiffres
				{
  				displayNum (8,Ligne,Tab_Code[tp]);					// Code
				}
				else
				{
  				displayNum (13,Ligne,Tab_Code[tp]);					// Code
				}
/*
				//*  PRESSION
				if (Tab_Pression[tp]>=1000)
				{
				displayNum (40,Ligne,Tab_Pression[tp]);				// Pression
				}
				else 
				{
				displayNum (45,Ligne,Tab_Pression[tp]);				// Pression
				}
*/

				//* PRESSION
				if (Tab_Pression[tp]>=1000)		//	>= 1000
				{
					if (Tab_Pression[tp]>def_pression_H)			// Si Pression sup�rieure � 1800mbar => affichage inverse
							{
						displayNumInv (27,Ligne,Tab_Pression[tp]);
						Etat_tpa|=0x10;		//- pour affichage case etat HG
							}	
						else 	
							{
						displayNum (27,Ligne,Tab_Pression[tp]);				// Pression  affichage normal
						Etat_tpa|=0x01;		//- pour affichage case etat OK
							}	
				}
				else if (Tab_Pression[tp]<1000 && Tab_Pression[tp]!=0)	// Si Pression	< 1000 et != de 0
				{
					if (Tab_Pression[tp]<def_pression_B)				// Si Pression inf�rieure � 800mbar => affichage inverse
							{
						displayNumInv (30,Ligne,Tab_Pression[tp]);
						Etat_tpa|=0x10;		//- pour affichage case etat HG
							}	
						else 
							{
						displayNum (30,Ligne,Tab_Pression[tp]);				// Pression  affichage normal
						Etat_tpa|=0x01;		//- pour affichage case etat OK
							}	
				}
				else // Si Pression	=  0
				{
				SetPos(35,Ligne);
	  			PutMessage("-");	// capteur r�ponse absent
				Etat_tpa|=0x02;		//- pour affichage case etat ABSENT
				}


				//* ETAT
				SetPos(58,Ligne);
				switch_Etat(Etat_tpa,Ligne);

				Tab_Etat[tp]= Etat_tpa;

				for (n_tp=1;n_tp<=16;n_tp++)
				if (Tab_Etat[n_tp]==1){
				switch_Present(n_tp);       	// affichage pr�sent pour le tableau  
				}
				else if (Tab_Etat[n_tp]==2){
				switch_Absent(n_tp);  			// affichage absent pour le tableau  
				}
				else if (Tab_Etat[n_tp]>2){
				switch_Present(n_tp);  			// affichage pr�sent avec d�fauts pour le tableau  
				}
				//	

				Mode_Scrolling ==0;
		}
}
 // fin �cran affichage TPadressables mode r�duit










//#########################################�cran affichage TP adressables mode complet###########################################
void ecran_aff_TPA_complet() 
{
unsigned char tp;
unsigned char ltp;
char DATAtoLCD[30];
unsigned char Ligne;
float temp_float;
				tp = Num_Ligne;

// AFFICHAGE MESURE �cran complet

				if (Num_Ligne > 3)
				{
				F_sup3mes_tpa = 1;   //flag pour m�moriser si sup � 3 mesures
				ClearScreen ();
				SetPos(0,60);								// Positionne le curseur
				PutMessage("Mesures en cours ");		// 17 caracteres
				ecran_adressables_complet();//				
				Affiche_complet(tp-2,1,Ligne);
				Affiche_complet(tp-1,2,Ligne);
				Affiche_complet(tp,3,Ligne);						
				}
				else 
				{
				ltp = Num_Ligne;
				Affiche_complet(tp,ltp,Ligne);
				F_sup3mes_tpa = 0;   //flag pour m�moriser si sup � 3 mesures
				}
//				displaySmallNum(tp*6,7,tp);
}







void Affiche_complet(char tp,char ltp,char Ligne)
{
float temp_float;
char DATAtoLCD[30];
//unsigned char Ligne;

				switch (ltp)
					{
				case 0:
	  				// N� Ligne utilis�e pour les d�bitm�tre
				break;
				case 1:
	  			Ligne = 35 ;	// position x Ligne d'�criture dans le tableau
				break;
				case 2:
	  			Ligne = 44 ;	// position x Ligne d'�criture dans le tableau
				break;
				case 3:
	  			Ligne = 51 ;	// position x Ligne d'�criture dans le tableau
				break;
					}
				Etat_tpa = 0x00;		// pour affichage case etat

//if ((Tab_Code[tp]==0 && Tab_I_Conso[tp] > 20 && (Tab_Pression[0] !=0)||Pres_Debit == 1))  //pr�sence d�bitm�tre
				Calcul_Deb (); // mise � l'�chelle du d�bit
if (Tab_Code[tp]==0 &&	Pres_Debit == 1)  //  									<===== � supprimer (test)??
	{ 
	// ici cadre d�bitm�tre et affichage valeur 
				SetPos(5,0);
  				PutMessage("DEBIT:            ");		
				displayNum (50,0,debit_cable);				// DEBIT  affichage normal
  				PutMessage(" L/H");
	}
	else if (Tab_Code[tp]==0 ||	Pres_Debit == 0)
	{
				SetPos(5,0);
  				PutMessage("Debitmetre Absent");
	}

		if (Tab_Code[tp]>0)   // pour ne pas afficher le capteur 0(d�bitm�tre) en premiere ligne du tableau
			{

				if (Tab_Code[tp]>=10){
  				displayNum (1,Ligne,Tab_Code[tp]);					// Code
				}
				else{
  				displayNum (6,Ligne,Tab_Code[tp]);					// Code
				}
				//* PRESSION
				if (Tab_Pression[tp]>=1000)		//	>= 1000
				{
					if (Tab_Pression[tp]>def_pression_H)			// Si Pression sup�rieure � 1800mbar => affichage inverse
							{
						displayNumInv (15,Ligne,Tab_Pression[tp]);
						Etat_tpa|=0x10;		//- pour affichage case etat HG
							}	
						else 	
							{
						displayNum (15,Ligne,Tab_Pression[tp]);				// Pression  affichage normal
						Etat_tpa|=0x01;		//- pour affichage case etat OK
							}	
				}
				else if (Tab_Pression[tp]<1000 && Tab_Pression[tp]!=0)	// Si Pression	< 1000 et != de 0
				{
					if (Tab_Pression[tp]<def_pression_B)				// Si Pression inf�rieure � 800mbar => affichage inverse
							{
						displayNumInv (20,Ligne,Tab_Pression[tp]);
						Etat_tpa|=0x10;		//- pour affichage case etat HG
							}	
						else 
							{
						displayNum (20,Ligne,Tab_Pression[tp]);				// Pression  affichage normal
						Etat_tpa|=0x01;		//- pour affichage case etat ok
							}	
				}
				else // Si Pression	=  0
				{
				SetPos(24,Ligne);
	  			PutMessage("-");	// capteur r�ponse absent
				Etat_tpa|=0x02;		//- pour affichage case etat ABSENT
				}


				//*	COURANT DE REPOS  il ne faut faire ce calcul que lorsque on arrive � la 16eme scrutation
				if ((Tab_I_Repos[tp] >=100) && (Tab_I_Repos[tp] < (def_i_repos+(def_i_repos*Nb_TPA_ok)))) // (def_i_repos*Nb_TPA_ok) pour calculer i_repos en fontion du nb de capteurs
				{
  				displayNum (42,Ligne,Tab_I_Repos[tp]);		// courant de repos en �A
				}	
				else if (Tab_I_Repos[tp] > (def_i_repos+(def_i_repos*Nb_TPA_ok)))
				{				
				displayNumInv (42,Ligne,Tab_I_Repos[tp]);
				Etat_tpa|=0x08;		//- pour affichage case Etat d�faut courant
				}
				if (Tab_I_Repos[tp] < 100) {
				displayNum (45,Ligne,Tab_I_Repos[tp]);
				}	
				//* COURANT DE CONSOMMATION
				temp_float=(float)Tab_I_Conso[tp];		// remet l'int en float pour l'affichage, pour diviser /10  et afficher avec virgule
				temp_float/=10;
				ftoa (temp_float,DATAtoLCD,1);				// Convertir un float en une chaine de caract�res		
//				SetPos(67,Ligne);								// Positionner le curseur
				if (Tab_I_Conso[tp] > (def_i_conso + Tab_I_Repos[tp]/100)) // par exemple i_conso= 80 pour 8.0mA et i_repos = 121 �A donc en mA*10 on aura 0.121mA => 80+1,21 => 81 si un seul capteur
				{
				SetPos(63,Ligne);								// Positionner le curseur
				PutFloatInv (DATAtoLCD); // Affiche le float en inverse
				Etat_tpa|=0x08;		//- pour affichage case Etat d�faut courant
				}		
				else
				{
				SetPos(67,Ligne);								// Positionner le curseur
				PutFloat (DATAtoLCD);						// Afficher le float en normal
				}
				//* COURANT DE MODULATION
				temp_float=(float)Tab_I_Modul[tp];		// remet l'int en float pour l'affichage, pour diviser /10  et afficher avec virgule
				temp_float/=10;
				ftoa (temp_float,DATAtoLCD,1);				// Convertir un float en une chaine de caract�res		
				SetPos(90,Ligne);								// Positionner le curseur
				PutFloat (DATAtoLCD);						// Afficher le float (taille normale)
				SetPos(111,Ligne);

				//* ETAT
				SetPos(110,Ligne);
				switch_Etat(Etat_tpa,Ligne);
				Tab_Etat[tp]= Etat_tpa;
			} //	if (Tab_Code[tp]!=0)

}
 // fin �cran affichage TPadressables mode complet




void eff_valeurs_complet()
{

//				FillWhiteZone(0,30, 126,60);
				// redessine les lignes verticales
//				vline (13,15,64);							// ligne verticale
//				vline (39,15,64);							// "
//				vline (61,15,64);							// ligne verticale
//				vline (84,15,64);							// "
//				vline (108,15,64);							// "
				ClearScreen ();
				ecran_adressables_complet();//				
}



void switch_Etat(char Etat,char Ligne)
	{
				switch (Etat)
					{
				case 0x00:
									//00= capteur non mesur�
				break;
				case 0x01:
	  			PutMessage("Ok");	//01= capteur r�ponse OK
				break;
				case 0x02:
	  			PutMessage("Abs");	//02= capteur r�ponse Absent
				break;
				case 0x04:
	  			PutMessage("N/R");	//04= capteur r�ponse Non R�ponse
				break;
				case 0x08:
	  			PutMessage("D/C");	//08= capteur r�ponse D�faut Courant
				break;
				case 0x10:
	  			PutMessage("HG");	//10= capteur r�ponse Hors Gamme
				break;
				case 0x20:
	  			PutMessage("CC");	//20= capteur r�ponse Court Circuit
				break;
				case 0x40:
	  			PutMessage("Def");	//40= capteur r�ponse Court Circuit  =>  pour indiquer un d�faut en mode �cran r�duit
				break;
				case 0x80:
	  			PutMessage("Ins");	//80= capteur r�ponse Instable =>  pour indiquer un d�faut en mode �cran r�duit
				break;
					}
	}

void switch_Present(char i)  // affichage pour tableau "Presents" du mode r�duit
	{
				switch (i)
					{
				case 0:
				break;
				case 1:
				displaySmallNum(83,24,1);
				break;
				case 2:
 				displaySmallNum(94,24,2);
				break;
				case 3:
				displaySmallNum(105,24,3);
				break;
				case 4:
				displaySmallNum(116,24,4);
				break;
				case 5:
				displaySmallNum(83,32,5);
				break;
				case 6:
 				displaySmallNum(94,32,6);
				break;
				case 7:
				displaySmallNum(105,32,7);
				break;
				case 8:
				displaySmallNum(116,32,8);
				break;
				case 9:
				displaySmallNum(82,40,9); 
				break;
				case 10:
				displaySmallNum(90,40,10);
				break;
				case 11:
				displaySmallNum(103,40,11);
				break;
				case 12:
				displaySmallNum(115,40,12);
				break;
				case 13:
				displaySmallNum(78,48,13); 
				break;
				case 14:
				displaySmallNum(90,48,14);
				break;
				case 15:
				displaySmallNum(104,48,15);
				break;
				case 16:
				displaySmallNum(116,48,16); 
				break;

					}

	}


void switch_Absent(char i)			// affichage pour tableau "Presents" du mode r�duit
	{
				switch (i)
					{
				case 0:
				break;
				case 1:
				SetPos(83,24);
				break;
				case 2:
				SetPos(94,24);
				break;
				case 3:
				SetPos(105,24);
				break;
				case 4:
				SetPos(116,24);
				break;
				case 5:
				SetPos(83,32);
				break;
				case 6:
 				SetPos(94,32);
				break;
				case 7:
				SetPos(105,32);
				break;
				case 8:
				SetPos(116,32);
				break;
				case 9:
				SetPos(83,40); 
				break;
				case 10:
				SetPos(94,40);
				break;
				case 11:
				SetPos(105,40);
				break;
				case 12:
				SetPos(116,40);
				break;
				case 13:
				SetPos(83,48); 
				break;
				case 14:
				SetPos(94,48);
				break;
				case 15:
				SetPos(105,48);
				break;
				case 16:
				SetPos(116,48); 
				break;
					}
 				PutMessage("-");

	}




void ecran_memo_TPadressables() 
	{
			unsigned char i;			//code ascii du caract�re s�lectionn� (lettres)
			unsigned char j;			//code ascii du caract�re s�lectionn� (chiffres)
			unsigned char k;
			unsigned char posC;			// position du caract�re pour la constitution
			unsigned char posV;			// position du caract�re pour la 2eme ligne (Voie)
			unsigned char posR;			// position du caract�re pour la 2eme ligne (Robinet)
			unsigned char posT;			// position du caract�re pour la 2eme ligne (Tete)
			unsigned char posCe;		// position du caract�re pour le central
			unsigned char posCinit;		// position du 1er caract�re pour la constitution
			unsigned char posVinit;		// position du 1er caract�re pour la Voie
			unsigned char posRinit;		// position du 1er caract�re pour le Robinet
			unsigned char posTinit;		// position du 1er caract�re pour la T�te
			unsigned char posCeinit;	// position du 1er caract�re pour le Central
			unsigned char la;			// largeur 1 caract�re 
			unsigned char caractere;	// si 0 caract�res A B C......si 1 caract�res 0 1 2 3
			unsigned char mem;			// autorisation de sauvegarde (=0)
			unsigned char NoCaract;		// num�ro du caract�re entr� pour la Constitution
			unsigned char NoCaractV;	// num�ro du caract�re entr� pour la Voie (2 caract�res)
			unsigned char NoCaractR;	// num�ro du caract�re entr� pour le Robinet (2 caract�res)
			unsigned char NoCaractT;	// num�ro du caract�re entr� pour la Tete
			unsigned char NoCaractCe;	// num�ro du caract�re entr� pour le Central
			unsigned char L_cv;			// pour indiquer si on est sur la ligne "constit" ou sur la ligne "Voie,Rob"
			unsigned char TA;           // num�ro du capteur � renseigner
			unsigned char valTA;        // validation des infos du capteur � renseigner

	for (TA=1;TA<=16;TA++)		// boucle pour renseigner chaque capteur trouv�
		{
		if  (Tab_Etat[TA]==0){				// pour stopper la boucle si 0 (fin capteurs)
			TA=17;  // il n'y a plus de capteurs,=> stop de la boucle					
		}		

		if  (Tab_Etat[TA]==1)
			{
				ClearScreen ();								// Effacement de tout l'�cran
				mem=0;			// 
				valTA=0;
				caractere = 0;  //chiffres (1) ou lettres (0)
				NoCaract=0;		// num�ro du caract�re entr� pour la constitution (de 0 � 9)
				NoCaractV=0;	// num�ro du caract�re entr� pour la voie et le robinet (de 0 � 1)
				NoCaractR=0;	// num�ro du caract�re entr� pour le robinet (de 0 � 1)
				NoCaractT=4;	// num�ro du caract�re entr� pour la Tete (de 4 � 9) de 4 parce qu'apres Voie et Rob dans tab
				NoCaractCe=0;	// num�ro du caract�re entr� pour le Central (de 0 � 14)  15 caract�res
				L_cv = 0;       //0 = �criture sur ligne constit,  1 = �criture sur ligne voie_rob

//				niveau_batt();
//				SetPos(0,0);
//  				PutMessage("SAUVEGARDE DES MESURES");				// Ecrire un message  nu_retour
				SetPos(0,0);
  				PutMessage("Informations TP");
				displayNum (80,0,TA);		//AFFICHE NUMERO DE CAPTEUR				
//				k=0;
//				posC = k+43; // position du 1er caract�re pour constit
				posC = 44;		// position du 1er caract�re pour constit
				posCinit=posC;	//sauvegarde la position du 1er caract�re pour constit	
				posV=24;			// position du premier caractere pour Voie
				posVinit=posV;	//sauvegarde la position du 1er caract�re pour voie	
//				posR=51;			// position du premier caractere pour Robinet
				posR=86;			// position du premier caractere pour Robinet
				posRinit=posR;	//sauvegarde la position du 1er caract�re pour Robinet	
				posT=25;			// position du premier caractere pour t�te
//				posT=83;			// position du premier caractere pour t�te
				posTinit=posT;	//sauvegarde la position du 1er caract�re pour t�te	
				posCe=41;			// position du premier caractere pour central
				posCeinit=posCe;//sauvegarde la position du 1er caract�re pour central

				vline(1,16,24);
				SetPos(2,16);
  				PutMessageInv("Constit:");
//				SetPos(k+=36,16);
				SetPos(38,16);
 				PutMessage("SR");
				plot(posC+=6,22);
				plot(posC+=6,22);
				plot(posC+=6,22);
				SetPos(posC+=6,22);
				PutChar(47);		// 47 = /
				plot(posC+=6,22);
				plot(posC+=6,22);
				plot(posC+=6,22);
				SetPos(posC+=6,22);
				PutChar(47);		// 47 = /
				plot(posC+=6,22);
				plot(posC+=6,22);
				SetPos(posC+=6,22);
				PutChar(47);		// 47 = /
				plot(posC+=6,22);
				plot(posC+=6,22);
				posC=posCinit;		// Repositionnement du 1er caract�re pour constit

				SetPos(2,30);
  				PutMessage("Voie:");					// Ecrire un message  nu_retour
				plot(posV,30);  	 //22
				plot(posV+6,30);	//28

				SetPos(48,30);
  				PutMessage("Robinet:");					// Ecrire un message  nu_retour
				plot(posR,30);   //51
				plot(posR+6,30);	 //57

//				SetPos(65,30);
				SetPos(2,38);
  				PutMessage("Tete:");					// Ecrire un message  nu_retour
				plot(posT,38);	//83
				plot(posT+=6,38);
				plot(posT+=6,38);
				SetPos(posT+=6,38);
				PutChar(47);		// 47 = /
				plot(posT+=6,38);
				plot(posT+=6,38);
				plot(posT+=6,38);
				posV=posVinit;			// Repositionnement du premier caractere pour Voie
				posR=posRinit;			// Repositionnement du premier caractere pour Rob.
				posT=posTinit;			// Repositionnement du premier caractere pour T�te
				posCe=posCeinit;			//sauvegarde la position du 1er caract�re pour central

				SetPos(2,46);
  				PutMessage("Central:..........................................");


				i=65; // lettre A par d�faut
				j=48; // chiffre 0 par d�faut

				// lettres par d�faut s�lectionn�
				FillBlackZone(39,54,51,63);
				SetPos(43,63);	 //52			// par d�faut choix caract�res A.....
				PutCharInv(i);
				// chiffres
				FillWhiteZone(79,54,91,63);
				SetPos(83,63);	 //52			// par d�faut choix caract�res A.....
				PutChar(j);					

//				SetPos(pos_curseur+6,36);
//				PutChar(94);		//CURSEUR caract�re "^"			

				L_cv = 0;	//	pour d�marrer sur constitution

	//######BOUCLE DE SAISIE INFORMATIONS###########

//				while(touche_BP5!=0 )		// attente touche  
				while(touche_BP5!=0 && valTA==0)		// attente touche  et  saisie capteur termin�e
	{		// boucle principale

				niveau_batt();

				la = 6;			// largeur caract�re standard sauf W...

				 if(touche_BP4==0 && touche_BP2==0)
			{ 	
					L_cv += 1;
					if (L_cv > 4){L_cv = 0;}

				switch (L_cv)  // s�lection du champ � compl�ter
					{
				case 0:
				SetPos(2,16);
  				PutMessageInv("Constit:"); 	//<== S�lection
				vline(1,16,24);
				SetPos(2,30);
  				PutMessage("Voie:"); 					
				unvline(1,24,32);
				SetPos(48,30);
  				PutMessage("Robinet:");					
				unvline(47,24,32);
				SetPos(2,38);
  				PutMessage("Tete:");					
				unvline(1,32,40);
				SetPos(2,46);
  				PutMessage("Central:");					
				unvline(1,40,48);
				break;
				case 1:
				SetPos(2,16);
  				PutMessage("Constit:");
				unvline(1,16,24);
				SetPos(2,30);
  				PutMessageInv("Voie:");	//<== S�lection
				vline(1,24,32);
				SetPos(48,30);
  				PutMessage("Robinet:");	
				unvline(47,24,32);
				SetPos(2,38);
  				PutMessage("Tete:");					
				unvline(1,32,40);
				SetPos(2,46);
  				PutMessage("Central:");					
				unvline(1,40,48);
				break;
				case 2:
				SetPos(2,16);
  				PutMessage("Constit:");
				unvline(1,16,24);
				SetPos(2,30);
  				PutMessage("Voie:");					
				unvline(1,24,32);
				SetPos(48,30);
  				PutMessageInv("Robinet:");	//<== S�lection
				vline(47,24,32);
				SetPos(2,38);
  				PutMessage("Tete:"); 			
				unvline(1,32,40);		
				SetPos(2,46);
  				PutMessage("Central:");				
				unvline(1,40,48);
				break;
				case 3:
				SetPos(2,16);
  				PutMessage("Constit:");
				unvline(1,16,24);
				SetPos(2,30);
  				PutMessage("Voie:");					
				unvline(1,24,32);
				SetPos(48,30);
  				PutMessage("Robinet:");					
				unvline(47,24,32);
				SetPos(2,38);
  				PutMessageInv("Tete:");	//<== S�lection
				vline(1,32,40);
				SetPos(2,46);
  				PutMessage("Central:");					
				unvline(1,40,48);
				break;
				case 4:
				SetPos(2,16);
  				PutMessage("Constit:");
				unvline(1,16,24);
				SetPos(2,30);
  				PutMessage("Voie:");					
				unvline(1,24,32);
				SetPos(48,30);
  				PutMessage("Robinet:");					
				unvline(47,24,32);
				SetPos(2,38);
  				PutMessage("Tete:");					
				unvline(1,32,40);
				SetPos(2,46);
  				PutMessageInv("Central:");	//<== S�lection			(14 caract�res)
				vline(1,40,48);
				break;
					}
				DELAY_MS (20);
				while(touche_BP4==0 && touche_BP2==0);	    // attente touches relach�es
		    }  


				box(39,54,51,63);
				box(79,54,91,63);

		      	//######CHOIX CHIFFRES###########
				 if(touche_BP2==0 && touche_BP4!=0)
					 		{ 	
				// chiffres
				box(39,54,51,63);
				box(79,54,91,63);
				FillBlackZone(80,55,90,62);
				SetPos(83,63);	 //52		
				PutCharInv(j);					
//				FillWhiteZone(39,54,51,63);
				FillWhiteZone(40,55,50,62);
				SetPos(43,63);	 //52			
				PutChar(i);
				caractere = 1;
							}
		      	//######CHOIX LETTRES###########
		       if(touche_BP1==0 && touche_BP4!=0)	
					 		{ 	
				// lettres
				box(39,54,51,63);
				box(79,54,91,63);
//				FillWhiteZone(79,54,91,63);
				FillWhiteZone(80,55,90,62);
				SetPos(83,63);	 //52			
				PutChar(j);					
//				FillBlackZone(39,54,51,63);
				FillBlackZone(40,55,50,62);
				SetPos(43,63);	 //52			// par d�faut choix caract�res A.....
				PutCharInv(i);
				caractere = 0;
							}


				//############Les lettres#############
			if (caractere ==0)     // Les lettres
				{
		       if(touche_BP0==0 && touche_BP4!=0 && (i<90))	
					{ 	
				FillBlackZone(39,54,51,63);
				SetPos(43,63); 	//52
				PutCharInv(++i);					
//				SetPos(33,63); 	// essais pr�sentation lettre avant et apr�s celle s�lectionn�e
//				PutChar(i-1);	// idem				
//				SetPos(53,63);	// idem	 	
//				PutChar(i+1);	// idem						
				DELAY_MS (100);
				while(touche_BP0==0)	    // attente touche relach�e
						{
					if (touche_BP0==0)
							{
					DELAY_MS (500);
						while(touche_BP0==0 && touche_BP4!=0 && (i<90))
								{
							FillBlackZone(39,54,51,63);
							SetPos(43,63); 	//52
							PutCharInv(++i);					
							DELAY_MS (300);
								}	
							}
						} // FIN  while(touche_BP0==0)		// attente touche relach�e 

					}

		       if(touche_BP3 ==0  && touche_BP4!=0 && (i>65))	
					{ 							
				FillBlackZone(39,54,51,63);
				SetPos(43,63);   //52
				PutCharInv(--i);					
				DELAY_MS (100);
				while(touche_BP3==0)		// attente touche relach�e
						{
					if (touche_BP3==0)
							{
					DELAY_MS (500);
						while(touche_BP3==0 && touche_BP4!=0 && (i>65))
								{
							FillBlackZone(39,54,51,63);
							SetPos(43,63);   //52
							PutCharInv(--i);					
							DELAY_MS (300);
								}	
							}
						}

					}				 
				 }    	// fin if caractere = 0
				//#########################




				//############Les chiffres#############
			if (caractere ==1)      // Les chiffres
				{
    	       if(touche_BP0==0 && touche_BP4!=0 && (j<57))	
				{ 	
				FillBlackZone(79,54,91,63);
				SetPos(83,63); //52
				PutCharInv(++j);					
				DELAY_MS (100);
				while(touche_BP0==0)		// attente touche relach�e
						{
					if (touche_BP0==0)
							{
					DELAY_MS (500);
						while(touche_BP0==0 && touche_BP4!=0 && (j<57))
								{
							FillBlackZone(79,54,91,63);
							SetPos(83,63); //52
							PutCharInv(++j);					
							DELAY_MS (300);
								}	
							}
						} // FIN  while(touche_BP0==0)		// attente touche relach�e 
					}
			

		       if(touche_BP3 ==0  && touche_BP4!=0 && (j>44))	
					 { 							
				FillBlackZone(79,54,91,63);
				SetPos(83,63); //52
				PutCharInv(--j);					
				DELAY_MS (100);
				while(touche_BP3==0)		// attente touche relach�e
						{
					if (touche_BP3==0)
							{
					DELAY_MS (500);
						while(touche_BP3==0 && touche_BP4!=0 && (j>44))
								{
						FillBlackZone(79,54,91,63);
						SetPos(83,63); //52
						PutCharInv(--j);					
						DELAY_MS (300);
								}	
							}
						} // FIN  while(touche_BP3==0)		// attente touche relach�e 

					}				 
				 }    	// fin if caractere = 1
				//#########################



				//######################### ECRITURE LIGNE CONSTIT###############

		        if(touche_BP6 ==0 &&  touche_BP4!=0 &&  L_cv == 0 && NoCaract<=9)   
			{
				SetPos(posC+=la,16);
				if (caractere==0){
				PutChar(i);
				Tab_Constit[TA][NoCaract]=i; 
// ###############################################ici �crire caract�re dans tab_constit pour tableau de sauvegarde
				}
				else if (caractere==1){
				PutChar(j);
				Tab_Constit[TA][NoCaract]=j; 
// ###############################################ici �crire caract�re dans tab_constit pour tableau de sauvegarde
				}
				if (NoCaract==2){
				posC+=1;  //
//				PutMessage("/");	
				PutChar(47);		// 47 = /
				posC+=5;  
				}
				if (NoCaract==5){
				posC+=1;  //
				PutChar(47);		// 47 = /
				posC+=5;  //
				}
				if (NoCaract==7){
				posC+=1;  //
				PutChar(47);		// 47 = /
				posC+=5;  
				}
//-				if (NoCaract==9){
//-				L_cv == 1;
//-				}
				NoCaract++;				
//				if (i==87){posC+=2;}		// pour ajuster taille de W					
//				if (i==73){posC-=2;}		// pour ajuster taille de I
//				pos_curseur = posC;
				DELAY_MS (20);
				while(touche_BP6==0);		// attente touche relach�e
			}
/*				if((touche_BP6 ==0 && caractere ==1)&& (posC<120))
							{
				SetPos(posC+=la,16);
				PutChar(j);					
//				pos_curseur = posC;
				DELAY_MS (20);
				while(touche_BP6==0);		// attente touche relach�e
							}
*/
				//#########################FIN ECRITURE LIGNE CONSTIT###############


				//#############CORRECTION LIGNE CONSTIT############
				if(touche_BP4==0 && touche_BP1==0 &&  L_cv == 0 && (posC>=posCinit+6))		
					{
				DELAY_MS (20);

				NoCaract-=1;
				if ((NoCaract==7) || ( NoCaract==5) || (NoCaract==2) )
					{
				if (posC >=6) {posC-=6;}  // pour sauter le "/"
					}

				FillWhiteZone(posC,13,posC+5,22);
				plot(posC,22);  //pour r�afficher le point apr�s effacement
				if (posC >=6) {posC-=6;}
			
				DELAY_MS (20);
				while(touche_BP1==0);		// attente touche relach�e
					}
				//#############FIN CORRECTION LIGNE CONSTIT############



				//######################### ECRITURE VOIE ###############
		        if(touche_BP6 ==0 &&  touche_BP4!=0 && L_cv == 1 && NoCaract<=2) // (2 caract�res 0-1 pour voie )
					{
				DELAY_MS (20);
				if (caractere==1)	//pour n'�crire que des chiffres (1) )
						{		   

						posV=posVinit;
						if (NoCaractV==0)
							{
						SetPos(posV,30);  // 
							}
						else
							{
						SetPos(posV+=6,30);
							}

						PutChar(j);     // j pour les chiffres

						Tab_VoieRobTete[NoCaractV]=j;		// m�morise caract�res voie
// ###############################################ici �crire caract�re  dans tab_constit pour tableau de sauvegarde
						NoCaractV++;
						}		
				DELAY_MS (20);
				while(touche_BP6==0);		// attente touche relach�e
					}
			
				//#########################FIN ECRITURE  VOIE ###############

		

	//#########################CORRECTION VOIE ###############
				if((touche_BP4==0) && (touche_BP1==0) &&  (L_cv == 1)&& (NoCaractV>0))		
					{
				DELAY_MS (20);
				NoCaractV-=1;

					if (NoCaractV==0)
							{
					posV=posVinit;
					SetPos(posV,30);  // 
							}
					else if (NoCaractV==1)
							{
					posV=posVinit+6;	
					SetPos(posV,30);
							}


					 
				FillWhiteZone(posV,24,posV+5,30);
				plot(posV+1,30);  //pour r�afficher le point apr�s effacement			
//				if (NoCaractV==1) {(NoCaractV=0);}

				DELAY_MS (20);
				while(touche_BP1==0);		// attente touche relach�e
					}
				//#########################FIN CORRECTION VOIE ###############
			



				//######################### ECRITURE ROB ###############
		        if(touche_BP6 == 0 &&  touche_BP4!=0 && L_cv == 2 && NoCaract<=2) // (2 caract�res 0-1 pour robinet)
					{
				DELAY_MS (20);
				if (caractere==1)	//pour n'�crire que des chiffres (1) )
						{		   

						posR=posRinit;
						if (NoCaractR==0)
							{
						SetPos(posR,30);  // 
							}
						else
							{
						SetPos(posR+=6,30);
							}

						PutChar(j);     // j pour les chiffres

						Tab_VoieRobTete[NoCaractR+2]=j;		// m�morise caract�res voie
	// ###############################################ici �crire caract�re  dans tab_constit pour tableau de sauvegarde
						NoCaractR++;
						}		
				DELAY_MS (20);
				while(touche_BP6==0);		// attente touche relach�e
					}
			
	//#########################FIN ECRITURE LIGNE ROB###############

		

	//#########################CORRECTION ROB ###############
				if((touche_BP4==0) && (touche_BP1==0) &&  (L_cv == 2)&& (NoCaractR>0))		
					{
				DELAY_MS (20);
				NoCaractR-=1;

					if (NoCaractR==0)
							{
					posR=posRinit;
					SetPos(posR,30);  // 
							}
					else if (NoCaractR==1)
							{
					posV=posRinit+6;	
					SetPos(posR,30);
							}


					 
				FillWhiteZone(posR,24,posR+5,30);
				plot(posR+1,30);  //pour r�afficher le point apr�s effacement			

				DELAY_MS (20);
				while(touche_BP1==0);		// attente touche relach�e
					}
	//#########################FIN CORRECTION ROB ###############
			


	//#########################  ECRITURE  TETE  ###############

		        if(touche_BP6 ==0 &&  touche_BP4!=0 &&  L_cv == 3)   
							{
				if (NoCaractT==4)
					{
					posT=posTinit;	
					SetPos(posT,38);
					}
				else
					{
					SetPos(posT+=6,38);
					}

				if (caractere==0){
				PutChar(i);
				Tab_VoieRobTete[NoCaractT]=i;		// m�morise caract�res Tete
	// ###############################################ici �crire caract�re dans Tete pour tableau de sauvegarde
				}
				else if (caractere==1){
				PutChar(j);
				Tab_VoieRobTete[NoCaractT]=j;		// m�morise caract�res Tete
	// ###############################################ici �crire caract�re dans Tete pour tableau de sauvegarde
				}
				if (NoCaractT==6){
				posT+=1;  //
				PutChar(47);		// 47 = /
				posT+=5;  
				}

				NoCaractT++;				
				DELAY_MS (20);
				while(touche_BP6==0);		// attente touche relach�e
							}
				//#########################FIN  ECRITURE  TETE  ###############


				//#############  CORRECTION  TETE  ############
				if(touche_BP4==0 && touche_BP1==0 &&  L_cv == 3 && (NoCaractT>4))		
					{
				DELAY_MS (20);

				NoCaractT-=1;
				if (NoCaractT==6)
					{
				if (posT >=6) {posT-=6;}  // pour sauter le "/"
					}

				FillWhiteZone(posT,32,posT+5,38);
				plot(posT,38);  //pour r�afficher le point apr�s effacement
				if (posT >=6) {posT-=6;}
			
				DELAY_MS (20);
				while(touche_BP1==0);		// attente touche relach�e
					}
				//#############FIN  CORRECTION  TETE   ############


				//######################### ECRITURE CENTRAL ###############

		        if(touche_BP6 ==0 &&  touche_BP4!=0 &&  L_cv == 4 && NoCaractCe<=13)   
			{
				if (NoCaractCe == 0)
					{
					posCe = posCeinit;
					SetPos(posCe,42);
					}
					else
					{
					SetPos(posCe+=6,42);
					}
				if (caractere==0){
				PutChar(i);
				Tab_Central[NoCaractCe]=i; 
// ###############################################ici �crire caract�re dans tab_constit pour tableau de sauvegarde
				}
				else if (caractere==1){
				PutChar(j);
				Tab_Central[NoCaractCe]=j; 
// ###############################################ici �crire caract�re dans tab_constit pour tableau de sauvegarde
				}
				NoCaractCe++;				
				DELAY_MS (20);
				while(touche_BP6==0);		// attente touche relach�e
			}
				//######################### FIN ECRITURE CENTRAL ###############


				//############# CORRECTION CENTRAL ############
				if((touche_BP4==0) && (touche_BP1==0) &&  (L_cv == 4) && (NoCaractCe>0))		
//				if(touche_BP4==0 && touche_BP1==0 &&  L_cv == 4 )		
					{
				DELAY_MS (20);
				NoCaractCe-=1;


				FillWhiteZone(posCe,39,posCe+5,47);
				plot(posCe,46);  //pour r�afficher le point apr�s effacement
				if (posCe >=6) {posCe-=6;}
			
				DELAY_MS (20);
				while(touche_BP1==0);		// attente touche relach�e
					}
				//############# FIN CORRECTION CENTRAL ############



					DELAY_MS (10);

			//#########################POUR Sauvegarde de la page###############				
				if(touche_BP4==0 && touche_BP6==0)
					{
					valTA=1;    // valide la fin des informations pour le capteur en cours
				DELAY_MS (20);
				while(touche_BP4==0 && touche_BP6==0);		// attente touches relach�es				
					}

	} 	//######FIN  SAISIE COMMENTAIRES###########
	


		}          // fin de if (Tab_Etat[TA])

	}		// fin de for(TA=1;.....

			ClearScreen ();	
			Sauvegarde_TA();
			F_retour_sauve=1;
			DELAY_MS (20);
}
//FIN ecran_memo_TPadressables



void Sauvegarde_TA()
{
 unsigned char mem;
			//############SAUVEGARDE############
//				DELAY_MS (50);
//				while (touche_BP5==0);
//				DELAY_MS (50);
				mem = 1; //pointe OUI
				SetPos(0,8);
  				PutMessage("La saisie des informations");		// Ecrire un message  nu_retour
				SetPos(15,16);
  				PutMessage("est terminee");		// Ecrire un message  nu_retour
				SetPos(0,32);
  				PutMessage("Confirmez la sauvegarde");		// Ecrire un message  nu_retour

				SetPos(35,50);
  				PutMessage("OUI");		// Ecrire OUI
				SetPos(75,50);
  				PutMessage("NON");		// Ecrire NON
				flecheH(40,60);								



				while (touche_BP6!=0 )
					{
				if(touche_BP2==0){		 
					flecheH(80,60);								// fl�che pour NON et m�mo � 0
				FillWhiteZone(40,57,48,60);
				mem = 0;
							  	}			
				if(touche_BP1==0){		
					flecheH(40,60);								// fl�che pour OUI et m�mo � 1
				FillWhiteZone(80,57,88,60);
				mem = 1;
							  	}
					}				


				if (touche_BP6==0 && mem==1)		
					{
// ###############################################ici �crire caract�re de l'horodatage dans tab_constit pour tableau de sauvegarde
// valeurs de l'horodatage en ascii   => � voir
				Tval = get_time(0x04);		//date
				decode();
					Tab_Horodatage[0]=Dizaines+48;
					Tab_Horodatage[1]=Unites+48;
				Tval = get_time(0x05);		//mois
				decode();
					Tab_Horodatage[2]=Dizaines+48;
					Tab_Horodatage[3]=Unites+48;
				Tval = get_time(0x06);		//ann�e
				decode();
					Tab_Horodatage[4]=Dizaines+48;
					Tab_Horodatage[5]=Unites+48;
				Tval = get_time(0x02);		//heures
				decode();
					Tab_Horodatage[6]=Dizaines+48;
					Tab_Horodatage[7]=Unites+48;
				Tval = get_time(0x01);		//minutes
				decode();
					Tab_Horodatage[8]=Dizaines+48;
					Tab_Horodatage[9]=Unites+48;

				
// ##############################################################sous-programme de sauvegarde ici
				ClearScreen ();	
 				Save_tpa_i2c();

//				FillWhiteZone(0,40,127,63);
				SetPos(10,40);
		 		PutMessage("Sauvegarde effectuee    ");		   
				F_aff_sauve_tpa=0;	// positionne le bit � 0 pour ne pas afficher Sauvegarde ?   
//			Lect_tpa_i2c ();  // � supprimer pour test
				F_sauve_mes_tpa=0;	// bit pour m�mo sauvegarde valid�e des mesures des tp adressables
					}

				else if (touche_BP6==0 && mem==0)
					{						
				ClearScreen ();	
//				FillWhiteZone(0,40,127,63);
 				SetPos(10,24);
 		 		PutMessage("Sauvegarde annulee     ");	 
					}

				DELAY_SEC (2);
				while (touche_BP6==0 );   // attend touche relach�e
				
	//############FIN SAUVEGARDE############


}   //############FIN SAUVEGARDE_TA############






void ecran_groupe()
{
char posz;      //position des labels en x
char posx;      //positions en x des parametres
				posz = 70;	//position des labels en x
				posx = 0;

				niveau_batt();
				SetPos(120,8);								
				PutMessage("G");  
				SetPos(120,16);								
				PutMessage("R");  
				SetPos(120,24);								
				PutMessage("O");  
				SetPos(120,32);								
				PutMessage("U");  
				SetPos(120,40);								
				PutMessage("P");  
				SetPos(120,48);								
				PutMessage("E");  
				vline(117,6,56);
				vline(127,6,56);
				hline(117,127,6);
				hline(117,127,56);

				SetPos(posx,0);								
				PutMessage	("P.Tampon         "); 
				SetPos(posz+3,0);								
				PutMessage(" mBar"); 
 
				SetPos(posx,8);								
				PutMessage	("P.Cable           "); 
				SetPos(posz+3,8);								
				PutMessage(" mBar"); 

 				SetPos(posx,16);								
				PutMessage	("P.Secours        ");  
				SetPos(posz+3,16);								
				PutMessage(" mBar");
 
 				SetPos(posx,24);								
				PutMessage("Pt.Rosee                  ");
				SetPos(posz-3,24);								
				PutMessage(" K");
//				unbox(posz+29,24,posz+31,26);
				box(posz+30,24,posz+32,26);
 				SetPos(posz+34,24);		//34						
				PutMessage("C");
 
				SetPos(posx,32);								
				PutMessage("Debit G         "); 
 				SetPos(posz-3,32);								
				PutMessage(" L/H ");

				SetPos(posx,40);								
				PutMessage("T.Cycle         ");
 				SetPos(posz-3,40);								
				PutMessage(" Sec ");
  
				SetPos(posx,48);								
				PutMessage("T.Moteur       ");
 				SetPos(posz-3,48);								
				PutMessage(" H   ");
  
				SetPos(0,56);								
				PutMessage("P.220v");
				voyant_off(41,59);
  
				SetPos(63,56);								
				PutMessage(" P.48v");  
				voyant_off(103,59);


}





void aff_mes_groupe()
{
char test;
char i;
char posw;		//position des valeurs en x
char posy;		//position des valeurs en y
char posz;      //position des labels en x
char posx;		//�tiquettes
unsigned int pression_tampon;
unsigned int pression_cable;
unsigned int pression_secours;
 float pt_roseeK;   // en Kelvin
unsigned char pt_roseeC;	// en �C
unsigned char signe;		// signe si pt de rosee en �C
unsigned int debit;
unsigned int tps_cycle_mot;
unsigned int tps_fonct_mot;

unsigned int temp;		//variable temporaire
 
/*#define def_tampon_H  4200 		// defaut si pression tampon > 4200 mbar
#define def_tampon_B  1500 		// defaut si pression tampon < 1500 mbar
#define def_cable_H    550 		// defaut si pression cable > 550 mbar
#define def_cable_B    450 		// defaut si pression cable < 450 mbar
#define def_secours_B1  3000 	// defaut si pression secours < 3000 mbar
#define def_secours_B2  1500 	// defaut si pression secours < 3000 mbar
#define def_rosee_H     253 	// defaut si pint de ros�e > -20�C   253�K
*/
//METTRE UNE SECURITE POUR LA SOUSTRACTION Xval-900

				test = 0;    //�  utiliser pour tester l'affichage et les calculs 			1 = test 0 = normal
//				test = 1;    //�  utiliser pour tester l'affichage et les calculs 			1 = test 0 = normal

				posx = 0;
				posw = 50;   //position des valeurs en x
				posz = 70;	//position des labels en x
				i=Num_Ligne;

		//SECURITE POUR LA SOUSTRACTION Xval-900		
		if	(Tab_Pression[i]<900 && Tab_Pression[i]>=897)   // 3 hz de marge
						{
					Tab_Pression[i]=900;
						}  
			else if (Tab_Pression[i]<897) 
						{
					(Tab_Pression[i]=0x0000);
						}  // d�faut r�ponse

		switch (i)
			{
//		if	(Tab_Pression[i]==0)
//				{	break;}


		case 0:
			// N� Ligne pas utilis�e (pour d�bitm�tres)
		break;


		case 1:
		//* PRESSION TAMPON  
		posy = 0 ;	// position x Ligne d'�criture dans le tableau
		if (Tab_Pression[i] >= 900 || test==1)
			{
				pression_tampon = ((Tab_Pression[i])-900)*10 ;  //mise � l'�chelle tampon
				if (test==1){	pression_tampon = (1500-900)*10 ; } //test

					if (pression_tampon <1000 && pression_tampon >=100)
							{		
						posw+=6;
							}		
					if (pression_tampon <100 && pression_tampon >=10)
							{		
						posw+=12;
							}		
					if (pression_tampon <10 )
							{		
						posw+=18;
							}
					if ((pression_tampon>=def_tampon_H)||(pression_tampon<=def_tampon_B))			// Si d�faut => affichage inverse
							{
				SetPos(posx,0);								
				PutMessageInv("P.Tampon         "); 
				SetPos(posz+3,0);								
				PutMessageInv(" mBar"); 
						displayNumInv (posw,posy,pression_tampon);
							}	
						else 	
							{
						displayNum (posw,posy,pression_tampon);				// Pression  affichage normal
							}	
			}
			else {
				SetPos(posx,0);								
				PutMessageInv("P.Tampon         "); 
				SetPos(posz+3,0);								
				PutMessageInv(" mBar"); 
 				}	
		break;

		case 2:
		//* PRESSION CABLE
		posy = 8 ;	// position x Ligne d'�criture dans le tableau

		if (Tab_Pression[i] >= 900 || test==1)
			{
						  //mise � l'�chelle cable
						pression_cable = ((Tab_Pression[i])-900) ;  //mise � l'�chelle cable
				if (test==1){pression_cable = ((980)-900) ; } //test

					if (pression_cable <1000 && pression_cable >=100)
							{		
						posw+=6;
							}		
					if (pression_cable <100 && pression_cable >=10)
							{		
						posw+=12;
							}		
					if (pression_cable <10 )
							{		
						posw+=18;
							}

					if ((pression_cable>=def_cable_H)||(pression_cable<=def_cable_B))			// Si d�faut => affichage inverse
							{
				SetPos(posx,8);								
				PutMessageInv("P.Cable           "); 
				SetPos(posz+3,8);								
				PutMessageInv(" mBar"); 
						displayNumInv (posw,posy,pression_cable);
							}	
						else 	
							{
						displayNum (posw,posy,pression_cable);				// Pression  affichage normal
							}
			}
			else {
				SetPos(posx,8);								
				PutMessageInv("P.Cable           "); 
				SetPos(posz+3,8);								
				PutMessageInv(" mBar");
				}	
		break;



		case 3:
		//* PRESSION SECOURS  
		posy = 16 ;	// position x Ligne d'�criture dans le tableau
		if (Tab_Pression[i] >= 900 || test==1)
			{
						pression_secours = ((Tab_Pression[i])-900)*10 ;  //mise � l'�chelle secours
				if (test==1){pression_secours = (980-900)*10 ; } //test

					if (pression_secours <1000){posw+=6;}			
					if ((pression_secours<=def_tampon_H)||(pression_secours<=def_tampon_B))			// Si d�faut => affichage inverse
							{
 				SetPos(posx,16);								
				PutMessageInv("P.Secours        ");  
				SetPos(posz+3,16);								
				PutMessageInv(" mBar");
						displayNumInv (posw,posy,pression_secours);
							}	
						else 	
							{
						displayNum (posw,posy,pression_secours);				// Pression  affichage normal
							}
			}	
			else {
 				SetPos(posx,16);								
				PutMessageInv	("P.Secours        ");  
				SetPos(posz+3,16);								
				PutMessageInv(" mBar");
				}	
		break;





		case 4:
		//* PT DE ROSEE
		posy = 24 ;	// position x Ligne d'�criture dans le tableau

		if (Tab_Pression[i] >= 900 || test==1)
			{
						pt_roseeK = ((Tab_Pression[i]-900)*0.0314)+235 ;  //mise � l'�chelle  pt d ros�e
				if(test==1){pt_roseeK = ((1600-900)*0.0314)+235 ;}  //test
						pt_roseeK = (int)pt_roseeK;		
							if (pt_roseeK<273)   // convertion pt de rosee en �C
								{
							pt_roseeC = 273-pt_roseeK;
							signe = 0;   // -
								}
							else		
								{
							pt_roseeC = pt_roseeK - 273;
							signe = 1;   // +
								}

					if (pt_roseeK>=def_rosee_H)			// Si d�faut => affichage inverse
							{

				SetPos(posx,24);								
				PutMessageInv("Pt.Rosee                  ");
				SetPos(posz-3,24);								
				PutMessageInv(" K");
						displayNumInv (posw+1,posy,pt_roseeK);
							if (signe == 0)
								{
							SetPos(posw+30,posy);								
							PutMessageInv("-");
								}
				unbox(posz+30,24,posz+32,26);
 				SetPos(posz+34,24);		//34						
				PutMessageInv("C");
						displayNumInv (posw+36,posy,pt_roseeC);

							}	
						else 	
							{
						displayNum(posw+1,posy,pt_roseeK);				// Pression  affichage normal  +1
							if (signe == 0)
								{
							SetPos(posw+30,posy);								
							PutMessage("-");
								}
						displayNum(posw+36,posy,pt_roseeC);
							}
				}	
			else {
 				SetPos(posx,24);								
				PutMessageInv("Pt.Rosee                  ");
				SetPos(posz-3,24);								
				PutMessageInv(" K");
				unbox(posz+30,24,posz+32,26);
 				SetPos(posz+34,24);		//34						
				PutMessageInv("C");
				}	
		break;


		case 5:
		//* DEBIT  
		posy = 32 ;	// position x Ligne d'�criture dans le tableau

		if (Tab_Pression[i] >= 900 || test==1)
			{
					// Mise � l'�chelle d�bit
					temp = (Tab_Pression[i]-900);    
				if (test==1){temp = (980-900) ; } //test

					if (temp>=0 && temp <=200)	//900 � 1100
							{
						debit = temp * 2;
							}
					if (temp>=201 && temp <=400)	//1101 � 1300
							{
						debit = temp * 4;
							}
					if (temp>=401 && temp <=700)	//1301 � 1600
							{
						debit = temp * 10;
							}

					if (debit <1000 && debit >=100)
							{		
						displayNum (posw+1,posy,debit);				// DEBIT  affichage normal
							}		
					if (debit <100 && debit >=10)
							{		
						displayNum (posw+7,posy,debit);				// DEBIT  affichage normal
							}		
					if (debit <10 )
							{		
						displayNum (posw+13,posy,debit);				// DEBIT  affichage normal
							}		
			}	
			else {
				SetPos(posx,32);								
				PutMessageInv("Debit G         "); 
 				SetPos(posz-3,32);								
				PutMessageInv(" L/H  ");
				}	
		break;


		case 6:
		//* TEMPS DE CYCLE MOTEUR  
		posy = 40 ;	// position x Ligne d'�criture dans le tableau

		if (Tab_Pression[i] >= 900 || test==1)
			{
					// Mise � l'�chelle temps de cycle moteur					   
					temp = (Tab_Pression[i]-900);    
				if (test==1){temp = (980-900) ; } //test

					if (temp>=0 && temp <=200)	//900 � 1100
							{
						tps_cycle_mot = temp * 1;
							}
					if (temp>=201 && temp <=400)	//1101 � 1300
							{
						tps_cycle_mot = temp * 8;
							}
					if (temp>=401 && temp <=700)	//1301 � 1600
							{
						tps_cycle_mot = temp * 18;
							}

					if (tps_cycle_mot >=1000)
							{		
						displayNum (posw,posy,tps_cycle_mot);				// DEBIT  affichage normal
							}		
					if (tps_cycle_mot <1000 && tps_cycle_mot >=100)
							{		
						displayNum (posw+1,posy,tps_cycle_mot);				// DEBIT  affichage normal
							}		
					if (tps_cycle_mot <100 && tps_cycle_mot >=10)
							{		
						displayNum (posw+7,posy,tps_cycle_mot);				// DEBIT  affichage normal
							}		
					if (tps_cycle_mot <10 )
							{		
						displayNum (posw+13,posy,tps_cycle_mot);				// DEBIT  affichage normal
							}		
			}	
			else {
				SetPos(posx,40);								
				PutMessageInv("T.Cycle         ");
 				SetPos(posz-3,40);								
				PutMessageInv(" Sec  ");
				}	
		break;


		case 7:		// pas utilis�e
		break;


		case 8:
		//* TEMPS DE FONCTIONNEMENT MOTEUR  
		posy = 48 ;	// position x Ligne d'�criture dans le tableau
		if (Tab_Pression[i] >= 900 || test==1)
			{
					// Mise � l'�chelle temps de fonctionnement moteur
					temp = (Tab_Pression[i]-900);    
				if (test==1){temp = (980-900) ; } //test
					if (temp>=0 && temp <=200)	//900 � 1100
							{
						tps_fonct_mot = temp * 1;
							}
					if (temp>=201 && temp <=400)	//1101 � 1300
							{
						tps_fonct_mot = temp * 8;
							}
					if (temp>=401 && temp <=700)	//1301 � 1600
							{
						tps_fonct_mot = temp * 18;
							}

					if (tps_fonct_mot >=1000 )
							{		
						displayNum (posw,posy,tps_fonct_mot);				// DEBIT  affichage normal
							}		
					if (tps_fonct_mot <1000 && tps_fonct_mot >=100)
							{		
						displayNum (posw+1,posy,tps_fonct_mot);				// DEBIT  affichage normal
							}		
					if (tps_fonct_mot <100 && tps_fonct_mot >=10)
							{		
						displayNum (posw+7,posy,tps_fonct_mot);				// DEBIT  affichage normal
							}		
					if (tps_cycle_mot <10 )
							{		
						displayNum (posw+13,posy,tps_fonct_mot);				// DEBIT  affichage normal
							}		
			}

			else {
				SetPos(posx,48);								
				PutMessageInv("T.Moteur         ");
 				SetPos(posz-3 ,48);								
				PutMessageInv(" H    ");
				}	
		break;


		case 9:
		// PRESENCE 220V
		posy = 56 ;	// position x Ligne d'�criture dans le tableau
		if (Tab_Pression[i] >= 900 || test==1)
			{
					temp=(Tab_Pression[i]);    
		if ( test==1){	temp=1600;}
					if (temp>1300)
						{
					voyant_on(41,60);
					SetPos(36,posy);								
//					PutMessage("ON");  
						}
					else
						{
					SetPos(0,posy);								
					PutMessageInv("P.220v");  
//					SetPos(36,posy);								
//					PutMessageInv("OFF");  
						}
			}
			else {
				SetPos(0,posy);								
				PutMessageInv("P.220v");  
				}	

		break;


		case 10:
		// PRESENCE 48V
	 	posy = 56 ;	// position x Ligne d'�criture dans le tableau
		if (Tab_Pression[i] >= 900 || test==1)
			{
					temp=(Tab_Pression[i]);    
		if ( test==1){	temp=1600;}
					if (temp>1300)
						{
					voyant_on(103,60);
					SetPos(98,posy);								
//					PutMessage("ON");  
						}
					else
						{
					SetPos(63,posy);								
					PutMessageInv(" P.48v");  
//					SetPos(98,posy);								
//					PutMessageInv("OFF");  
						}
			}
			else {
				SetPos(63,posy);								
				PutMessageInv(" P.48v");  
			}	

		break;
		}
	
}	



void ecran_resistifs()	
		{

				niveau_batt();

				SetPos(13,0);
  				PutMessage("   TP Resistifs   ");		// Ecrire un message  nu_retour
				box(5,29,78,48);							// R�aliser un rectangle
				box(83,29,123,48);							// R�aliser un rectangle

 				SetPos(8,20);								// Positionner le curseur
  				PutMessage("Mesure (mbar)");	
 				SetPos(95,20);								// Positionner le curseur
				PutMessage("Etat");				  		// Ecrire un message  
  				SetPos(15,60);								// Positionner le curseur

		}  // fin �cran r�sistifs


void ecran_TPR_Mes()	
		{
				FillWhiteZone(6,30,77,47);  
				FillWhiteZone(84,30,122,47);  


				SetPos(15,60);								// Positionner le curseur
				PutMessage("Mesure en cours ...");				  		// Ecrire un message  
				Mesure_TPR();
				DELAY_MS (200);
				SetPos(15,60);								// Positionner le curseur
				PutMessage("                         ");				  		// Ecrire un message  
				displayHNum (28,34,Freq_R); 

		}  // fin �cran TPR



void ecran_debitmetres()

{
				niveau_batt();

				SetPos(13,0);
  				PutMessage("   DEBITMETRES   ");		// Ecrire un message  nu_retour
				box(5,29,78,48);							// R�aliser un rectangle
				box(83,29,123,48);							// R�aliser un rectangle

 				SetPos(8,20);								// Positionner le curseur
  				PutMessage("Mesure (L/H)");	
 				SetPos(95,20);								// Positionner le curseur
				PutMessage("Etat");				  		// Ecrire un message  
  				SetPos(15,60);								// Positionner le curseur
}


void ecran_DEB_mes()
{
unsigned int temp;

				FillWhiteZone(6,30,77,47);  
				FillWhiteZone(84,30,122,47);  


				SetPos(15,60);								// Positionner le curseur
				PutMessage("Mesure en cours ...");				  		// Ecrire un message  
				Boucle_Mesure_PCPG();
				mes_debit=0;
				DELAY_MS (200);
				SetPos(15,60);								// Positionner le curseur
				PutMessage("                         ");				  		// Ecrire un message  

				Calcul_Deb (); // mise � l'�chelle du d�bit

				if (Pres_Debit == 0) //soit absent
					{
				SetPos(88,36);							
				PutMessage("Absent");
					}
				else
					{
				displayHNum (28,34,debit_cable);
				SetPos(88,36);							
				PutMessage("OK");				  	 
					}

}

void Calcul_Deb ()

{
//unsigned int temp;
			// Echelle d�bitm�tre 1000 � 1500Hz pour 0L/H � 1000 et 500L/H � 1500Hz

				if (Tab_Pression[0]>=0x03E5 && Tab_Pression[0]<=0x03E8)  // 3Hz de tol�rance � voir !!
						{
					Tab_Pression[0]=0x03E8;   //1000
						}
					else if (Tab_Pression[0]<0x03E5)  //997
						{
					Tab_Pression[0]=0x270F;		//9999
					Pres_Debit = 0; //d�bitm�tre absent
						}
				
  				 if (Tab_Pression[0]==0x270F)  	//9999
						{
					debit_cable = 0x270F;  //pour indiquer que pas de r�ponse
					Pres_Debit = 0; //d�bitm�tre absent
						}
					else
						{
					debit_cable = Tab_Pression[0]-0x03E8;  // -1000
					Pres_Debit = 1; //d�bitm�tre pr�sent
//					Tab_Pression[0]=debit_cable;
						}

}


//#####################################    MODE BCTA   ##############################################
void ecran_codage()

{
				unsigned char mem_cod_lec;			// ?????autorisation de sauvegarde (=1)
				int lect_en_cours =0;
				int prog_en_cours =0;
//				niveau_batt();
 //   			Code_Prog = 1;

				SetPos(3,15);
  				PutMessage("CODAGE TP ADRESSABLES");		// Ecrire un message  nu_retour
//				hline(0,127,15) ;							// R�aliser une ligne horizontale

//// 				SetPos(20,35);								// Positionner le curseur
////  				PutMessage("Code TP  ");
 				SetPos(20,50);								// Positionner le curseur
  				PutMessage("Codage");	
  				SetPos(70,50);								// Positionner le curseur
				PutMessage("Lecture");				  		// Ecrire un message  
//				FillWhiteZone(80,57,88,60);  
				mem_cod_lec = 1;
				flecheH(33,60);								// fl�che pour codage et m�mo � 1
				Display_Num_C_Prog();


				while (touche_BP5 != 0 )
		{
				niveau_batt();


	//############# MODE LECTURE ############mem_cod_lec = 0#
				if(touche_BP2==0)
					{		 
					flecheH(83,60);								// fl�che pour lecture et m�mo � 0
				FillWhiteZone(33,57,41,60);
				FillWhiteZone(20,24,100,44);   
				mem_cod_lec = 0;
//				Display_Num_C_Lect();
					}
	//############# FIN MODE LECTURE #############

	//############# MODE CODAGE #############mem_cod_lec = 1#			
				if(touche_BP1==0)
					{		
					flecheH(33,60);								// fl�che pour codage et m�mo � 1
				FillWhiteZone(83,57,91,60);
				FillWhiteZone(20,24,100,44); 
				mem_cod_lec = 1;
				Display_Num_C_Prog();
					}				
				DELAY_MS (10);
     //#############FIN MODE CODAGE#############



				if (mem_cod_lec==1)
				{
	//############# INCREMENTATION NUMERO #############
				if(touche_BP0==0 && Code_Prog<=15)
							{		 
							Code_Prog ++;
							eff=1;    // pour ne pas effacer sans arr�t
							}
//				while (touche_BP0 == 0);  // attente touche relach�e
	//#############FIN INCREMENTATION NUMERO #############

	//############# DECREMENTATION NUMERO #############
				if(touche_BP3==0 && Code_Prog>=2)
							{		 
							Code_Prog --;
							eff=1;    // pour ne pas effacer sans arr�t
							}
//				while (touche_BP3 == 0);  // attente touche relach�e
						if (eff==1)
							{
						Display_Num_C_Prog();
							}

				while (touche_BP0 == 0 || touche_BP3 == 0);  // attente touches relach�es
	//############# DECREMENTATION NUMERO #############



			//### FIN incr�mentation num�ro ###

				}
				if (mem_cod_lec==0 && lect_en_cours != 1)   
							{
// � voir							FillWhiteZone(40,24,100,36);   //(80,24,100,36)
							}

	//>>>>>>>>>>>>>>>>>>>	LECTURE CODE   <<<<<<<<<<<<<<<<<<<<<<<<
	
				if (touche_BP4 == 0 && mem_cod_lec==0 )
							{
/*					flecheH(83,60);								// fl�che pour lecture et m�mo � 0
					FillWhiteZone(33,57,41,60);
					FillWhiteZone(20,24,100,36);   
					mem_cod_lec = 0;    */
	
							lect_en_cours = 1;
 							FillWhiteZone(20,24,100,36);   //(80,24,100,36)
							DELAY_MS (100);
					SetPos(40,40);								// Positionner le curseur
 					PutMessage("Patientez... ");


// 							SetPos(38,30);								// Positionner le curseur
//							PutMessage("Transfert...");
							TSTcc();
							LectCodeTP();
////							DELAY_MS (1000);
					    	lect_en_cours=0;
							Display_Num_C_Lect();
	
////							SetPos(20,35);								
////  							PutMessage("Terminee  ");
//							Code_Prog = Code_Lect;
							DELAY_MS (1000);
							Led_rouge=0;
							Led_verte=0;
							Led_jaune=0;
							}
	//>>>>>>>>>>>>>>>>>>> FIN LECTURE CODE   <<<<<<<<<<<<<<<<<<<<<<<<


	//>>>>>>>>>>>>>>>>>>>  PROGRAMMATION CODE   <<<<<<<<<<<<<<<<<<<<<<<<

				if (touche_BP4 == 0 && mem_cod_lec==1)
						{

						prog_en_cours = 1;
						DELAY_MS (100);
					SetPos(40,40);								// Positionner le curseur
 					PutMessage("Patientez... ");
						ProgCodeTP();
						DELAY_MS (400);  //300

					flecheH(83,60);								// fl�che Lecture pour lecture 
					FillWhiteZone(33,57,41,60);
					FillWhiteZone(20,24,100,36);   
						LectCodeTP();
						lect_en_cours = 0;
						if (Code_Lect != Code_Prog)
							{
						Led_verte=0;
						Led_rouge=1;
							}
						Display_Num_C_Lect();
						DELAY_MS (2000);
//						flecheH(33,60);								// fl�che pour codage et m�mo � 1
//						FillWhiteZone(83,57,91,60);
						Led_rouge=0;
						Led_verte=0;
						Led_jaune=0;
						mem_cod_lec = 0;    // apres codage on reste en mode lecture  (0)

////	 				SetPos(20,35);								// Positionner le curseur
////  					PutMessage("Terminee  ");
						}
////	 				SetPos(20,35);								// Positionner le curseur
////	  				PutMessage("Code TP  ");
////					SetPos(5,25);								// Positionner le curseur
////  					PutMessage("                  ");
////					SetPos(30,40);								// Positionner le curseur
////  					PutMessage("           ");

	//>>>>>>>>>>>>>>>>>>>FIN  PROGRAMMATION CODE   <<<<<<<<<<<<<<<<<<<<<<<<

	}

}




void Display_Num_C_Prog()
{
						FillWhiteZone(20,24,100,46);  
							eff=0;
  	 				if	(Code_Prog < 10)
							{
 							displayHNum (60,28,Code_Prog);    //91,28 -> 60,28
							}
					else
							{
							displayHNum (51,28,Code_Prog);    //82,28 -> 51,28
							}

}

void Display_Num_C_Lect()
{
				FillWhiteZone(20,24,100,46);   
				if (Led_rouge==0)  //si pas de d�faut
						{
					if	(Code_Lect < 10 )
							{
						displayHNum (60,28,Code_Lect);     //91,28 -> 60,28
							}
					else
							{
						displayHNum (51,28,Code_Lect);   //82,28 -> 50,28
							}
   				    	}	
}



// FIN ECRANS DE MESURES

void ecran_lect_mem(void)
{
int test;

				niveau_batt();
//				SetPos(3,0);
//  				PutMessage("LECTURE MEMOIRE");		// Ecrire un message  nu_retour
				//	TOTO
				SetPos(1,8);
  				PutMessage("TP");		// N� du TPA
				SetPos(16,8);
  				PutMessage("V/R");	// Voie/Robinet
				SetPos(38,8);
  				PutMessage("Mbar");	// Mesure
 				SetPos(67,8);
 				PutMessage("Constitution");	// Ecrire un message  nu_retour

				hline(0,128,7) ;							// ligne horizontale
				hline(0,128,15) ;						// "
				vline (13,7,63);							// ligne verticale
				vline (35,7,63);							// "
				vline (61,7,63);							// ligne verticale
				test=0x2222;
				displaySmallNum (37,16,test);	
			
}



void ecran_param()
{
				niveau_batt();
				SetPos(10,10);
  				PutMessage("PARAMETRAGES");		// Ecrire un message  nu_retour
				hline(0,127,20) ;							// R�aliser une ligne horizontale
 				SetPos(10,30);								// Positionner le curseur
  				PutMessage(" Reglage horloge");
 				SetPos(10,38);								// Positionner le curseur
  				PutMessage(" Raz memoire    ");	
  				SetPos(10,46);								// Positionner le curseur
				PutMessage(" ADECEF         ");				  		// Ecrire un message  
}



void menu_param_C0()
{
				SetPos(10,30);								// Positionner le curseur
				PutMessageInv(" Reglage horloge");					// Ecrire un message  nu_retour			
 				SetPos(10,38);								// Positionner le curseur
  				PutMessage   (" Raz memoire    ");	
}	
				
void menu_param_C1()
{
				SetPos(10,30);								// Positionner le curseur
				PutMessage   (" Reglage horloge");					// Ecrire un message  nu_retour
	 			SetPos(10,38);								// Positionner le curseur
  				PutMessageInv(" Raz memoire    ");	
				SetPos(10,46);								// Positionner le curseur
				PutMessage   (" ADECEF         ");				  		// Ecrire un message  
}	
				
void menu_param_C2()
{
 				SetPos(10,38);								// Positionner le curseur
				PutMessage   (" Raz memoire    ");					// Ecrire un message
//				SetPos(10,54);								// Positionner le curseur
//				PutMessage   ("  autre         ");				  		// Ecrire un message  
				SetPos(10,46);								// Positionner le curseur
				PutMessageInv(" ADECEF         ");				  		// Ecrire un message  
}
					
//void menu_param_C3()
//{
 //				SetPos(10,46);								// Positionner le curseur
//				PutMessage   ("Raz memoire    ");				  		// Ecrire un message  
//				SetPos(10,54);								// Positionner le curseur
//				PutMessageInv(" autre         ");				  		// Ecrire un message  
//}	



void ecran_Raz_Memoire()
{
				niveau_batt();
				SetPos(10,10);
  				PutMessage("Raz Memoire");		// Ecrire un message  nu_retour
				hline(0,127,20) ;							// R�aliser une ligne horizontale


}

void ecran_adecef()
{

				niveau_batt();
				SetPos(10,10);
  				PutMessage("CONFIGURATION");		// Ecrire un message  nu_retour
				hline(0,127,20) ;							// R�aliser une ligne horizontale


}



// FIN  MENUS




void niveau_batt()   // symbole de la batterie avec curseur de remplissage
{
				char niv_batt;
				box(109,0,124,4);							// rectangle batterie
				box(124,1,125,3);							// rectangle batterie
				Mesure_V_Batt();
				V_Batt =(int)(V_Batt_Calc*10);				// 
				V_Batt_Aff = V_Batt_Calc;					// pour. m�moriser et afficher la valeur
				if (V_Batt >= 67){
				V_Batt-=67;}	// min 6.7v x10 = 67  82-67 = 15 				
				else{V_Batt=0;}		
//				if (V_Batt <0){V_Batt=0;}
//				else if (V_Batt > 15){V_Batt=15;}
				if (V_Batt > 15){V_Batt=15;}
													// 123-15=110
				niv_batt = V_Batt + 109;	
				FillBlackZone (109,1,niv_batt,3);		// zone remplie = 15 donc >=8.2V
				niv_batt = V_Batt + 110;	
				FillWhiteZone (niv_batt,1,123,3);    	// zone vide = 0 donc <=6.7V
}	





void reset_leds()

{
				Led_rouge =1;
				DELAY_MS (15);
				Led_verte =1;
				DELAY_MS (15);
				Led_jaune =1;
				DELAY_MS (15);
//				Back_light=1;
				DELAY_MS (100);
				Led_rouge =0;
				Led_verte =0;
				Led_jaune =0;

}



void flip_flop_back_light()
{
				if (touche_BP1==0 && touche_BP2==0 && F_memo_back_light == 1)
				{
				Back_light = 0;
				F_memo_back_light = 0;				
				while (touche_BP1==0 || touche_BP2==0);
				}
				if (touche_BP1==0 && touche_BP2==0 && F_memo_back_light == 0)
				{
				Back_light = 1;
				F_memo_back_light = 1;			
				while (touche_BP1==0 || touche_BP2==0);
				}
}



 
			