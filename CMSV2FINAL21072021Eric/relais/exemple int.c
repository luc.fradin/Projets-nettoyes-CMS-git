#include <p18f2423.h> // Oscillator -"HS",Watchdog Timer - disable 
[...] 
///////////////////////////////////////////////////////////////////////////////// 
#pragma interrupt InterruptTimers 
void InterruptTimers(void) 
{ 
static unsigned int m,k; 

//------------- timer0 interrupt-------------- 
if(INTCONbits.TMR0IF&&INTCONbits.TMR0IE) 
{ 
if(k++ == 200) 
{ 
PORTCbits.RC1=!PORTCbits.RC1; //trigger for LED1 
k=0; 
} 
TMR0L=240; 
INTCONbits.TMR0IF=0; // clear the flag of interrupt 
} //end TMR0-interrupt 

//------------- timer2 interrupt-------------- 
if(PIR1bits.TMR2IF&&PIE1bits.TMR2IE) 
{ 
if(m++ == 1000) 
{ 
PORTCbits.RC2=!PORTCbits.RC2; //trigger for LED2 
m=0; 
} 
PIR1bits.TMR2IF = 0; 
} //end TMR2-interrupt 

INTCONbits.GIE = 1; 
} //end function "interrupt" 
[...] 
} 