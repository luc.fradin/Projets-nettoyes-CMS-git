
#include <inituc.h>
#include <ecrans.h>
#include <eeprom.h>
#include <GLCD.h>
#include <delay.h>						/* Defs DELAY functions */
#include <memo.h>


//prototype
void gestion_lecture_memoire (void);
void Gestion_Enreg_Ecriture(void);
void Increment_adresse(void);
void Decrement_adresse(void);
void Raffraichissement_Aff(void);

extern  void Init_Tab_tpa(void);  //reset tableau de mesures des tpa
extern unsigned char F_ecran_adres;
extern unsigned char Num_Capteur ;
extern unsigned char Num_Ligne;
extern unsigned char Nb_Capteurs_max;
extern unsigned char Tab_Code[17];
extern unsigned int Tab_I_Repos[17];	// lecture du courant de repos pour voir la fin de la mesure enregistr�e		  
extern unsigned char Tab_Central[14];

extern unsigned int Offset_Lect; // adresse de l'enregistrement en lecture eeprom i2c
extern unsigned int Der_Enreg;  // adresse du dernier enregistrement en EEprom i2c
unsigned int Aff_Enreg; // adresse de l'enregistrement affich�
extern unsigned int No_der_enreg;
/*
				SetPos(1,0);
  				PutMessage("Central: MIRIBEL");		// N� du TPA
				SetPos(0,8);
  				PutMessage("Voie:");		// voie en lecture
*/
extern unsigned char Tab_VoieRobTete[10];	//10 octets 	VOIE 2 + ROB 2 + TETE 6	 
extern unsigned char Tab_Type_Enr[1];  		//1 octets  pour m�moriser le type d'enregistrement (TPA=0,Groupe=1,D�bitm�tre=2,TPR=3,Icra=4)


void gestion_lecture_memoire()
{
int tpa;
int modif_aff; //si changement de numero d'enregistrement
int fin_lect_groupe;
unsigned char i;

				modif_aff=1;
				Aff_Enreg = No_der_enreg;
				Init_Tab_tpa();  // reset des tableaux            
				Nb_Capteurs_max = 17;
				Num_Capteur =0;
				Num_Ligne = 0;
				fin_lect_groupe=0;


		while (!touche_BP6 ); // attend touche relach�e


				Scrolling_Ecran_tpa();

				Lect_tpa_i2c ();   //LECTURE EEPROM-I2C

   	while (touche_BP5 !=0 ){
// �cran adressable


				i=0;
				SetPos(0,8);								// Positionner le curseur
				for (i=0;i<=13;i++)
				{
				if(Tab_Central[i]!=0x20){  // = espace
				PutChar(Tab_Central[i]);
						 	}				
				}	

				SetPos(0,56);								// Positionner le curseur
				PutMessage("Voie:");		// 
				SetPos(25,56);								// Positionner le curseur
				if (Tab_VoieRobTete[0]>=48 && Tab_VoieRobTete[0]<=57)PutChar(Tab_VoieRobTete[0]);
				if (Tab_VoieRobTete[1]>=48 && Tab_VoieRobTete[1]<=57)PutChar(Tab_VoieRobTete[1]);	 

				SetPos(90,56);								// Positionner le curseur
				PutMessage("Enr:");		// 
				displayNum (118,56,Aff_Enreg);

			// choix affichage �cran complet ou reduit avec les touches < ou >
			if (touche_BP2 == 0 ||touche_BP1 == 0 ) 
				{
				if (touche_BP2 == 0)  // si touche > alors �crans complet
					{ 
				ClearScreen ();								// Effacement de tout l'�cran
			    F_ecran_adres=1;						// affichage �cran complet (bit = 1)
				ecran_adressables_complet();
					}
				while (touche_BP2 == 0);

				if (touche_BP1 == 0)  // si touche < alors �crans r�duit
					{ 
				ClearScreen ();								// Effacement de tout l'�cran
	     	    F_ecran_adres=0;						// affichage �cran r�duit (bit = 0)
				ecran_adressables_reduit();
					}
				Scrolling_Ecran_tpa();

				while (touche_BP1== 0 || touche_BP2== 0);           
				}

//				Scrolling_Ecran_tpa(); //� ajouter si on supprime la ligne dessus =>	Init_Tab_tpa();  // reset des tableaux  !� supprimer pour r�affichage valeurs!


				if (touche_BP0==0 && Num_Ligne>3  )    	// scrolling vertical si > 3 mesures et si scrutation termin�e	&& F_sup3mes_tpa
					{
					Num_Ligne--;
 					DELAY_MS (10);
					}
				if (touche_BP3==0  && Num_Ligne<Nb_Capteurs_max && Num_Ligne<16)		// scrolling vertical si > 3 mesures et si scrutation termin�e   (Nb_Capteurs_max-1)
					{
					Num_Ligne++;
	 				DELAY_MS (10);
					}
				if (touche_BP0==0||touche_BP3==0)	
					{
					Scrolling_Ecran_tpa();
				while (touche_BP3== 0 || touche_BP0== 0);           
					}
// *************FIN    scrolling vertical si > 3 mesures***************

// *****************decrementation incrementation adresse memoire******
		while (touche_BP6==0)
		{
				if (touche_BP3==0 && touche_BP6==0 )    	// scrolling vertical si > 3 mesures et si scrutation termin�e	&& F_sup3mes_tpa
					{
					Decrement_adresse();
				modif_aff=1;
				Raffraichissement_Aff();
	 				DELAY_MS (30);
					}
				while(touche_BP3==0);  // attend touche relach�e

				if (touche_BP0==0  && touche_BP6==0 )		// scrolling vertical si > 3 mesures et si scrutation termin�e   (Nb_Capteurs_max-1)
					{
					Increment_adresse();
				modif_aff=1;
				Raffraichissement_Aff();
	 				DELAY_MS (30);
					}
				while(touche_BP0==0);  // attend touche relach�e

/*				if (touche_BP0==0||touche_BP3==0 )	
					{
				while (touche_BP3== 0 || touche_BP0== 0 || touche_BP6==0);           
					}*/
	
// *****************FIN decrementation incrementation adresse memoire******




	if (touche_BP4 == 0 || modif_aff==1 )
		{
				Init_Tab_tpa();  // reset des tableaux            
				Offset_Lect= (Aff_Enreg-1)*0x180;
				Lect_tpa_i2c ();   //LECTURE EEPROM-I2C
				Num_Capteur =0;
				while (touche_BP4 ==0);
				ClearScreen ();								// Effacement de tout l'�cran
	for (tpa=0; tpa < 17 ;tpa++)  //tpa=0 POUR LECTURE EN ZERO (DEBITMETRE)
			{
				if (Tab_I_Repos[tpa] == 0 ){					
				Nb_Capteurs_max = tpa;
									}
				Tab_Code[tpa] = tpa;	  // � voir si � conserver ainsi que Num_Capteur

			}
				Num_Ligne = 3;		// num�ro de la ligne de mesure pour affichage				
			Scrolling_Ecran_tpa();

		}
				modif_aff=0;
 				DELAY_MS (30);

			}//	while (touche_BP5)
 				DELAY_MS (5);
 				DELAY_MS (5);
	}  // else fin ecran groupe
} // FIN de  gestion_lecture_memoire()




void Raffraichissement_Aff()
{
unsigned char i=0;

				Offset_Lect= (Aff_Enreg-1)*0x180;
				Lect_tpa_i2c ();   //LECTURE EEPROM-I2C
				SetPos(0,8);								// Positionner le curseur
				for (i=0;i<=13;i++)
				{
				if(Tab_Central[i]!=0x20){  // = espace
				PutChar(Tab_Central[i]);
						 	}				
				}	

				SetPos(0,56);								// Positionner le curseur
				PutMessage("Voie:");		// 
				SetPos(25,56);								// Positionner le curseur
				if (Tab_VoieRobTete[0]>=48 && Tab_VoieRobTete[0]<=57)PutChar(Tab_VoieRobTete[0]);
				if (Tab_VoieRobTete[1]>=48 && Tab_VoieRobTete[1]<=57)PutChar(Tab_VoieRobTete[1]);	 


}


void Increment_adresse()
{
			if (Aff_Enreg < No_der_enreg)   // 
			{
			Aff_Enreg += 1;
				SetPos(90,56);								// Positionner le curseur
				PutMessage("Enr:");		// 
				displayNum (118,56,Aff_Enreg);
			}	
}

void Decrement_adresse()
{
//unsigned int Offset_Enr; // adresse de l'enregistrement en lecture
//unsigned int Der_Enreg;  // adresse du dernier enregistrement en EEprom
//unsigned int Aff_Enreg;  // adresse de l'enregistrement affich�


			if (Aff_Enreg >= 2)   // 
			{
			Aff_Enreg -= 1;
				SetPos(90,56);								// Positionner le curseur
				PutMessage("Enr:");		// 
				displayNum (118,56,Aff_Enreg);
			}
}





