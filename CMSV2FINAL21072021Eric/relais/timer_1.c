
#include <p18f8723.h>					/* Defs PIC 18F8723 */
#include <timers.h>						/* timers (interruptions)*/



void config_Timer1_chenillard (void)
{
			TMR1H = 0x00;
			TMR1L = 0x00;
			PIR1 = 0;			//clear Timer1 interrupt flag
			T1CONbits.RD16 = 1; // timer sur 16bits
			T1CONbits.T1RUN = 1; // source autre que oscillateur du timer 1
			T1CONbits.T1CKPs<1:0>;	// prédiviseur  erreur!!!
			T1CONbits.T1OSCEN = 0;	// turn off resistor feeback
			T1CONbits.T1SYNC = 0;	// do not synchronize external clock input
			T1CONbits.TMR1CS = 0;	// pas de comptage par signal fréquence du capteur sur T13CKI broche RC0 du pic
			
			T1CONbits.T1OSCEN = 0;	// turn off resistor feeback, oscillateur de T1 off  (quartz)
   			RCONbits.IPEN = 1; 	//enable priority interrupt
		   	IPR1 = 0x01;		// set Timer1 to high priority
			PIE1 = 0x01;    	// enable Timer1 roll-over interrupt
			INTCON = 0xC0;		//GIE=1 PEIE=1 enable global and peripheral interrupts

			T1CONbits.TMR1ON = 1;  // timer 1 ON
//			INTCONbits.GIE = 0; //disable global interrupt
//			T1CONbits.TMR1ON = 0;  // stop timer 1 
}

