// eeprom.c
// GESTION DE L'EEPROM

#include <stdio.h>
#include <string.h>
#include <p18f8723.h>
#include <stdlib.h>
//#include <delays.h> 
#include <i2c.h>
//#include <GLCD.h>
#include <delay.h>
#include <ftoa.h>

//unsigned char ReadEEPROM(unsigned int ad); 
//void WriteEEPROM(unsigned int ad,unsigned char data); 
//void eepmess(unsigned int , unsigned char *p);
void Wr_eeprom_i2c_int( unsigned int adr_eeprom,  int data);
unsigned int Rd_eeprom_i2c_int( unsigned int adr_eeprom);
void Wr_eeprom_i2c_str ( unsigned int adr_eeprom,  char *string, unsigned int long_string );  
void Rd_eeprom_i2c_str ( unsigned int adr_eeprom,  char *string, unsigned int long_string );
void Rd_eeprom_i2c_double( unsigned int adr_eeprom, char *data );
void Wr_eeprom_i2c_double( unsigned int adr_eeprom, char *data);
unsigned char Rd_eeprom_i2c_char( unsigned int adr_eeprom);
void Wr_eeprom_i2c_char( unsigned int adr_eeprom,  char data_char);
//void Test_eeprom_i2c_int (void);

unsigned int adr_eeprom;
unsigned data_char;
char chaines1[] = ("TEST CHAINE");

//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//----------------------------- r�cup�ration  AN991A ----------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
/************************************************************************
*     Function Name:    HDByteWriteI2C                                  *   
*     Parameters:       EE memory ControlByte, address and data         *
*     Description:      Writes data one byte at a time to I2C EE        *
*                       device. This routine can be used for any I2C    *
*                       EE memory device, which only uses 2 bytes of     *
*                       address data as in the 24LC32A/64/128/256. *
*                                                                       *     
************************************************************************/

unsigned char HDByteWriteI2C( unsigned char ControlByte, unsigned char HighAdd, unsigned char LowAdd, unsigned char data )
{
  IdleI2C();                      // ensure module is idle
  StartI2C();                     // initiate START condition
  while ( SSP1CON2bits.SEN );      // wait until start condition is over 
  WriteI2C( ControlByte );        // write 1 byte - R/W bit should be 0
  IdleI2C();                      // ensure module is idle
  WriteI2C( HighAdd );            // write address byte to EEPROM
  IdleI2C();                      // ensure module is idle
  WriteI2C( LowAdd );             // write address byte to EEPROM
  IdleI2C();                      // ensure module is idle
  WriteI2C ( data );              // Write data byte to EEPROM
  IdleI2C();                      // ensure module is idle
  StopI2C();                      // send STOP condition
  while ( SSP1CON2bits.PEN );      // wait until stop condition is over 
  while (EEAckPolling(ControlByte));  //Wait for write cycle to complete
  return ( 0 );                   // return with no error
}

/********************************************************************
*     Function Name:    HDByteReadI2C                               *
*     Parameters:       EE memory ControlByte, address, pointer and *
*                       length bytes.                               *
*     Description:      Reads data string from I2C EE memory        *
*                       device. This routine can be used for any I2C*
*                       EE memory device, which only uses 2 bytes of *
*                       address data as in the 24LC32A/64/128/256. *
*                                                                   *  
********************************************************************/

unsigned char HDByteReadI2C( unsigned char ControlByte, unsigned char HighAdd, unsigned char LowAdd, unsigned char *data, unsigned char length )
{
  IdleI2C();                      // ensure module is idle
  StartI2C();                     // initiate START condition
  while ( SSP1CON2bits.SEN );      // wait until start condition is over 
  WriteI2C( ControlByte );        // write 1 byte 
  IdleI2C();                      // ensure module is idle
  WriteI2C( HighAdd );            // WRITE word address to EEPROM
  IdleI2C();                      // ensure module is idle
  while ( SSP1CON2bits.RSEN );     // wait until re-start condition is over 
  WriteI2C( LowAdd );             // WRITE word address to EEPROM
  IdleI2C();                      // ensure module is idle
  RestartI2C();                   // generate I2C bus restart condition
  while ( SSP1CON2bits.RSEN );     // wait until re-start condition is over 
  WriteI2C( ControlByte | 0x01 ); // WRITE 1 byte - R/W bit should be 1 for read
  IdleI2C();                      // ensure module is idle
  getsI2C( data, length );       // read in multiple bytes
  NotAckI2C();                    // send not ACK condition
  while ( SSP1CON2bits.ACKEN );    // wait until ACK sequence is over 
  StopI2C();                      // send STOP condition
  while ( SSP1CON2bits.PEN );      // wait until stop condition is over 
  return ( 0 );                   // return with no error
}

/********************************************************************
*     Function Name:    HDSequentialReadI2C                         *
*     Parameters:       EE memory ControlByte, address, pointer and *
*                       length bytes.                               *
*     Description:      Reads data string from I2C EE memory        *
*                       device. This routine can be used for any I2C*
*                       EE memory device, which only uses 2 bytes of*
*                       address data as in the 24xx32 - 24xx512.    *
*                                                                   *  
********************************************************************/

unsigned char HDSequentialReadI2C( unsigned char ControlByte, unsigned char HighAdd, unsigned char LowAdd, unsigned char *rdptr, unsigned char length )
{
  IdleI2C();                      // ensure module is idle
  StartI2C();                     // initiate START condition
  while ( SSP1CON2bits.SEN );      // wait until start condition is over 
  WriteI2C( ControlByte );        // write 1 byte 
  IdleI2C();                      // ensure module is idle
  WriteI2C( HighAdd );            // WRITE word address to EEPROM
  IdleI2C();                      // ensure module is idle
  WriteI2C( LowAdd );             // write HighAdd byte to EEPROM
  IdleI2C();                      // ensure module is idle
  RestartI2C();                   // generate I2C bus restart condition
  while ( SSP1CON2bits.RSEN );     // wait until re-start condition is over 
  WriteI2C( ControlByte | 0x01 ); // WRITE 1 byte - R/W bit should be 1 for read
  IdleI2C();                      // ensure module is idle
  getsI2C( rdptr, length );       // read in multiple bytes
  NotAckI2C();                    // send not ACK condition
  while ( SSP1CON2bits.ACKEN );    // wait until ACK sequence is over 
  StopI2C();                      // send STOP condition
  while ( SSP1CON2bits.PEN );      // wait until stop condition is over 
  return ( 0 );                   // return with no error
}


//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//----------------------------- FIN  r�cup�ration  AN991A ----------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------


/******************** Wr_eeprom_i2c_char ************************************************/
void Wr_eeprom_i2c_char( unsigned int adr_eeprom, char data_char)
{ 	/* ecrit un INT ( un entier )dans l'eeprom du PIC18
	adr_eeprom = adresse du destinataire dans l'eeprom : Wr_eeprom_i2c_int( & adr_eeprom,  data)/ (le pointeurest convertit en INT))  !!
	data = donn�e � m�moriser en INT ou unsigned INT
	Note : L'octet MSB est � droite, l'octet LSB est � gauche
	*/

	unsigned char ControlByte ; 
	unsigned char HighAddres ;
	unsigned char LowAddres ;

	ControlByte = 0b10100000 ;      // VOIR VALEUR DANS MEMO.C (SELON L'EEPROM)

	LowAddres = adr_eeprom ;
	HighAddres = adr_eeprom >> 8 ;
	
	HDByteWriteI2C( ControlByte, HighAddres, LowAddres, data_char ) ;

} // fin Wr_eeprom_i2c_char

/******************** Wr_eeprom_i2c_int ************************************************/
void Wr_eeprom_i2c_int( unsigned int adr_eeprom,  int data)
{ 	/* ecrit un INT ( un entier )dans l'eeprom du PIC18
	adr_eeprom = adresse du destinataire dans l'eeprom : Wr_eeprom_i2c_int( & adr_eeprom,  data)/ (le pointeur est convertit en INT))  !!
	data = donn�e � m�moriser en INT ou unsigned INT
	Note : L'octet MSB est � droite, l'octet LSB est � gauche
	*/

	unsigned char ControlByte ; 
	char data_char ;
	unsigned char HighAddres ;
	unsigned char LowAddres ;
	char *ptr_char ;	// variable temporaire
	void *ptr_void ;

	ControlByte = 0b10100000 ;      // voir MEMO.C	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	// LSB
	LowAddres = adr_eeprom ;
	HighAddres = adr_eeprom >> 8 ;
	
	ptr_void = &data ;
	ptr_char = ptr_void ;
	data_char = *ptr_char; 
 
	HDByteWriteI2C( ControlByte, HighAddres, LowAddres, data_char ) ;

	// MSB
	adr_eeprom ++ ;
	LowAddres  = adr_eeprom ;
	HighAddres = adr_eeprom >> 8 ;
//	data_char  = data ;
//	data_char = temp >>8 ;				// msb
	ptr_char ++ ;
	data_char = *ptr_char;
	HDByteWriteI2C( ControlByte, HighAddres, LowAddres, data_char ) ;

} // fin Wr_eeprom_i2c_int

/******************** Wr_eeprom_i2c_double ************************************************/
void Wr_eeprom_i2c_double( unsigned int adr_eeprom, char *data)
{ 	/* ecrit un LONG ou un DOUBLE dans l'eeprom du PIC18
	adr_eeprom = adresse du destinataire dans l'eeprom : Wr_eeprom_i2c_int( & adr_eeprom,  data)/ (le pointeurest convertit en INT))  !!
	*data = adresse de la donn�e � m�moriser sur 4 octets
	Note : L'octet MSB est � droite, l'octet LSB est� gauche
	*/
	unsigned char ControlByte ;
	unsigned char data_char ; // variable temporaire
	unsigned char HighAddres ;
	unsigned char LowAddres ;

	ControlByte = 0b10100000 ;      // voir MEMO.C	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	// LSB_L
	LowAddres  = adr_eeprom ;
	HighAddres = adr_eeprom >> 8 ;
	data_char  = *data ;
	HDByteWriteI2C( ControlByte, HighAddres, LowAddres, data_char ) ;

	// LSB_M
	data ++ ;
	adr_eeprom ++ ;
	LowAddres  = adr_eeprom ;
	HighAddres = adr_eeprom >> 8 ;
	data_char  = *data ;
	HDByteWriteI2C( ControlByte, HighAddres, LowAddres, data_char ) ;

	// MSB_L
	data ++ ;
	adr_eeprom ++ ;
	LowAddres  = adr_eeprom ;
	HighAddres = adr_eeprom >> 8 ;
	data_char  = *data ;
	HDByteWriteI2C( ControlByte, HighAddres, LowAddres, data_char ) ;

	// MSB_M
	data ++ ;
	adr_eeprom ++ ;
	LowAddres  = adr_eeprom ;
	HighAddres = adr_eeprom >> 8 ;
	data_char  = *data ;
	HDByteWriteI2C( ControlByte, HighAddres, LowAddres, data_char ) ;

} // fin Wr_eeprom_i2c_double

/******************** Wr_eeprom_i2c_str ************************************************/
void Wr_eeprom_i2c_str ( unsigned int adr_eeprom,  char *string, unsigned int long_string )
{ 	/* ecrit une chaine de caracteres dans l'eeprom du PIC18
	adr_eeprom = adresse du destinataire dans l'eeprom : Wr_eeprom_i2c_int( & adr_eeprom,  data)/ (le pointeurest convertit en INT))  !!
	string = Adresse ( en INT ou en *Ptr ) de la chaine de donn�es � m�moriser en INT
	long_string = longueur de la chaine de caracteres
	*/

	unsigned char ControlByte ; 
	unsigned char temp ; // variable temporaire
	unsigned char HighAddres ;
	unsigned char LowAddres ;
	unsigned char data ;
//	unsigned char *ptr_char ;	// variable temporaire

	ControlByte = 0b10100000 ;      // voir MEMO.C	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	temp = 0 ;


	while (temp < long_string)
	{
		LowAddres  = adr_eeprom ;
		HighAddres = adr_eeprom >> 8 ;
	
		data = *string ;

		HDByteWriteI2C( ControlByte, HighAddres, LowAddres, data );

		temp++ ;
		string ++ ;
		adr_eeprom ++ ;
	}

} // fin Wr_eeprom_i2c_str

//*********************Rd_eeprom_i2c_char *****************************************
unsigned char Rd_eeprom_i2c_char( unsigned int adr_eeprom)
{	/* lit un CHAR dans l'eeprom du PIC18
	adr_eeprom = adresse du destinataire dans l'eeprom : Rd_eeprom_i2c_int( & adr_eeprom,  data)/ (le pointeurest convertit en INT))  !!
	Retourne la donn�e m�moris�e en INT
	*/
	unsigned char ControlByte ;
	unsigned char data ; // variable temporaire
	unsigned char HighAddres ;
	unsigned char LowAddres ;
	unsigned char length ; 
	unsigned char *ptr_data ;

	ptr_data = &data ;

	ControlByte = 0b10100000 ;      // voir MEMO.C	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	length = 1 ;

	LowAddres  = adr_eeprom ;
	HighAddres = adr_eeprom >> 8 ;
	
	HDByteReadI2C( ControlByte, HighAddres, LowAddres, ptr_data, length ) ;	// LSB

	return (data) ; // memory_data;

} // fin Rd_eeprom_i2c_char

/******************** Rd_eeprom_i2c_int ************************************************/
unsigned int Rd_eeprom_i2c_int( unsigned int adr_eeprom)
{	/* lit un INT dans l'eeprom du PIC18
	adr_eeprom = adresse du destinataire dans l'eeprom : ,  data = Rd_eeprom_i2c_int( & adr_eeprom )/ (le pointeurest convertit en INT))  !!
	Retourne la donn�e m�moris�e en INT
	*/
	unsigned char ControlByte ;
	unsigned char HighAddres ;
	unsigned char LowAddres ;
	unsigned char length ; 
	unsigned int data ;
	char *ptr_char ;
	void *ptr_void ;

	ControlByte = 0b10100000 ;      // voir MEMO.C	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	length = 1 ;

	ptr_void = &data ;
	ptr_char = ptr_void ;

	LowAddres  = adr_eeprom ;
	HighAddres = adr_eeprom >> 8 ;
	HDByteReadI2C( ControlByte, HighAddres, LowAddres, ptr_char, length ) ;	// LSB

	ptr_char ++ ;
	adr_eeprom ++ ;
	LowAddres  = adr_eeprom ;
	HighAddres = adr_eeprom >> 8 ;
	HDByteReadI2C( ControlByte, HighAddres, LowAddres, ptr_char, length ) ;	// MSB

	return (data) ; // memory_data;

} // fin Rd_eeprom_i2c_int

/******************** Rd_eeprom_i2c_double ************************************************/
void Rd_eeprom_i2c_double( unsigned int adr_eeprom, char *data )
{	/* lit un DOUBLE ou un LONG dans l'eeprom du PIC18
	adr_eeprom = adresse de la source l'eeprom : Rd_eeprom_i2c_double( & adr_eeprom,  &data)/ (le pointeur adr_eeprom est convertit en INT))  !!
	Place la donn�e m�moris�e � l'adresse indiqu�e par *char
	*/

	unsigned char ControlByte ;
	unsigned char temp_char ;	// variable temporaire
	unsigned char HighAddres ;
	unsigned char LowAddres ;
	unsigned char length ; 

	ControlByte = 0b10100000 ;     // voir MEMO.C	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	length = 1 ;

	LowAddres  = adr_eeprom ;
	HighAddres = adr_eeprom >> 8 ;
	HDByteReadI2C( ControlByte, HighAddres, LowAddres, data, length ) ;	// LSB_L

	data ++ ;
	adr_eeprom ++ ;
	LowAddres  = adr_eeprom ;
	HighAddres = adr_eeprom >> 8 ;
	HDByteReadI2C( ControlByte, HighAddres, LowAddres, data, length ) ;	// LSB_M

	data ++ ;
	adr_eeprom ++ ;
	LowAddres  = adr_eeprom ;
	HighAddres = adr_eeprom >> 8 ;
	HDByteReadI2C( ControlByte, HighAddres, LowAddres, data, length ) ;	// MSB_L

	data ++ ;
	adr_eeprom ++ ;
	LowAddres  = adr_eeprom ;
	HighAddres = adr_eeprom >> 8 ;
	HDByteReadI2C( ControlByte, HighAddres, LowAddres, data, length ) ;	// MSB_M

} // fin Rd_eeprom_i2c_double

/******************** Rd_eeprom_i2c_str ************************************************/
void Rd_eeprom_i2c_str ( unsigned int adr_eeprom,  char *data, unsigned int long_string )
{ 	/* Lit une chaine de caracteres dans l'eeprom du PIC18
	adr_eeprom = adresse du destinataire dans l'eeprom : Rd_eeprom_i2c_int( & adr_eeprom,  data)/ (le pointeurest convertit en INT))  !!
	string = Adresse (en *Ptr ) ou ranger la chaine de donn�es r�cup�r�e en m�moire
	long_string = longueur de la chaine de caracteres
	*/

	unsigned char ControlByte ;
	unsigned int temp ; 		// variable temporaire
	unsigned char HighAddres ;
	unsigned char LowAddres ;
	unsigned char length ; 

	temp = 0 ;

	ControlByte = 0b10100000 ;      // voir MEMO.C	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	length = 1 ;

	while (temp < long_string)
	{
		LowAddres  = adr_eeprom ;
		HighAddres = adr_eeprom >> 8 ;

		HDByteReadI2C( ControlByte, HighAddres, LowAddres, data, length ) ;
		data ++ ;
		temp ++ ;
		adr_eeprom ++ ;
	}

} // fin Rd_eeprom_i2c_str









/***********************************************************************************/


