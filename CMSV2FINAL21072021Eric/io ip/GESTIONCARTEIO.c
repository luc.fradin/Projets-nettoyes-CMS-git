#include <p18f97j60.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <i2c.h> 
#include <GESTIONCARTEIO.h>


#define COUPMODEM PORTEbits.RE0
#define RL    PORTBbits.RB1
#define DSR   PORTHbits.RH2
#define RAZCMS	PORTBbits.RB3
#define DTR   PORTHbits.RH1
#define RTS   PORTHbits.RH4
//#define RX_TX 

#define I2CMEMO3 PORTDbits.RD3
#define I2CMEMO PORTDbits.RD0
#define I2CMEMO2 PORTDbits.RD2
#define I2CINTR PORTDbits.RD1
#define I2CHORLG PORTDbits.RD4
#define IO_EN_COM PORTDbits.RD5



#define DERE PORTAbits.RA4 // direction RX TX
#define LEDC PORTFbits.RF0
#define LEDD PORTFbits.RF1
//#define PORTHbits.RH5 PORTHbits.RH5
#define LEDRJTEST1 PORTAbits.RA0
#define LEDRJTEST2 PORTAbits.RA1

 #define DEMANDERS485 PORTBbits.RB2



//#pragma config WDT=OFF, FOSC2=ON, FOSC=HSPLL, ETHLED=ON
//#pragma config XINST=OFF


#pragma udata udata0
 unsigned char iii;unsigned int SPEED;
 unsigned char kkk;
char Crtc;
 char carac;
// char carac1[];
unsigned char npage, nchamps;
static enum _ETAT1 //QUELLE PAGE ON EST ?
	{
		I = 0,   /////page d'aceuille avec mots de passe 
		M,       ///// page menu
        A,       ///// page 1
		B,       ///// page2
		C,       ///// page3
		D,       ///// page4
		E,       ///// page5
		F,       ///// page6
		G,       ///// page6b
		H,       ///// page6c
        K,       ///// page8
		L,       ///// page 9 adecef reset
	} ETAT1 = I;
  


#pragma udata udata1
char debit[4];
//char tableau[40] ; //RECEPTION MINITEL
char tableau2[48] = {'A','T', 'A','T',0X0D};
//char tableau1[10];
char accordUSEROUADMI; //USER OU ADMI


//unsigned int CRC;
unsigned int CRC1;
char tmp,tmp2,u;
char ACTIONENCOURS;
char VOIECOD[5];
char TYPE[1];
char CMOD[4];
char CCON[4];
char CREP[4];
char CONSTITUTION[13];
char COMMENTAIRE[20];
//char COMMENTAIREsansrob[20];
char CABLE[3];
char JJ[2];
char MM[2];
char AA[4];
char HH[2];
char mm[2];
char ss[2];
char ii[2];
char COD[2];
char POS[2];
char iiii[4];
char DISTANCE[5];
char ETAT[4];
char VALEUR[4];
char SEUIL2[4];
char SEUIL[4];
char y[1];
//char START [1];
//char ID1[2];
//char ID2[2];
//unsigned char FONCTION[3];
char CRC2[5];
//char STOP[1];
char valeur[4];
char NORECEP232;
char ETAT_CONNECT_MINITEL;
//#pragma udata udata2
char capt_exist;
char TERMINEACTION;
char n_voie[2];
char nb_oct;
char identifiant;
 char CMouPAS;
char testtimer;









#pragma udata udata3
//char TRAMEIN[125];
int repet=0;  //DIRE SI ON  REP. LA PAGE  
signed int decal=0;
int max_decalage=0;
int accord, accordmp;
char  pos_init_cursur[4];
unsigned char max_champ=0;
int  coordon[7];
char kk;
char groupe[11];
char ETAT_M1[23];
char desig[21];
char pos_capteur[5];
//char n_voie_1[2];
//char n_voie_2[2];
//char n_voie_3[2];
//char n_voie_4[2];
//char n_voie_5[2];
//char n_voie[2];

char voie_select[2];
char n_carte;


char ET_TD[4],ET_GR[4],ET_TP[4],ET_TR[4],ET_I[4],ET_C[4],ET_F[4]; //ALARME,HG,NONREPONSE,INTER,



 









#pragma udata udata4
////// TRAMES IDENTIQUES
char START[1];
char ID1[2];
char ID2[2];
unsigned char FONCTION[3];
char CRC485[5];
char STOP[1];
int pointeur_out_in;
char DEMANDE_INFOS_CAPTEURS[10];
char TYPE_ERREUR[2];
char INFORMATION_ERREUR[20];
char NORECEPTION;
char TYPECARTE[5];

char MODERAPID;
char NBCOM; //BUG ZIZ 

char REINITMODEM;

#pragma udata udata5
char TMP[125];  // PR SAV DES TRAMES
char P_RESERVOIR[15];
char P_SORTIE[15];
char P_SECOURS[15];
char POINT_ROSEE[11];
char DEBIT_GLOBAL[11];
char TEMPS_CYCLE_MOTEUR[11];
char TEMPS_MOTEUR[19];
char PRESENCE_220V[6];
char PRESENCE_48V[6];
char CHERCHER_DONNEES_GROUPE(void);
//#pragma udata udata6
//MEMOIRE/////////////////////
void variables_groupe(char t);










//////////////////////////



#pragma udata udata10
//char TRAMEOUT[500];

 
#pragma udata udata13
char type_capteur [13];
char ID_rack[30];
char T_alarme[3];
char T_sortie[3];
char acc_u[4];
char acc_conf[4];
char nr_cog[15];
char nr_cms[15];
char passerelle[15];
char DNS[15];
char CHG_CODAGE;
char NBRECAPTEURS_CABLE;
unsigned char 	Tab_Horodatage[12];
char HORLOGET[12];
char STOPMINITEL;
char COMMENTAIRESAV[20];

char ROB [2];
char DEMANDE_DE_LAUTO;
char REA_DESACTIVE;
int pas;



void TEMPO(char seconde);
void calcul_CRC(void);
char RECEVOIRST_RS485(void);
void TRAMEENVOIRS485DONNEES (void);
void Init_RS485(void);
//void TRAMEENVOIRS232DONNEES (int val);
//void TRAMERECEPTIONRS232DONNEES (int val2) ;
 char RECEVOIR_RS232(void);
void RS485(unsigned char carac);
void init_RS232(void);
void delay_qms(char tempo);
void CMS_TO_TELGAT (void);
//void init_modem (char youyou);
//unsigned int CRC16(char far *txt, unsigned int lg_txt);
void IntToChar(unsigned int value, char *chaine,char Precision);
void CRC_verif(void);
void load_data_eeprom (void);
unsigned char LIRE_EEPROM(unsigned char ADDE,unsigned int ADDHL,unsigned char *tab,char r);
unsigned char ECRIRE_EEPROMBYTE(unsigned char ADDE,unsigned int ADDHL,unsigned char *tab,char g);
unsigned int calcul_addmemoire (unsigned char *tab);
unsigned char choix_memoire(unsigned char *tab);
void ENVOIE_DES_TRAMES (char R,int fonctionRS,char *tab); 
 void TRAMECAPTEURBIDON(char *taz);

//char RECEVOIR_RS485(char tout);
//void TRAMERECEPTIONRS485DONNEES (char ttout); 

void controle_CRC(void);
void Init_I2C(void);

void SAV_EXISTENCE(char *tt);
void TRANSFORMER_TRAME_MEM_EN_COM (void);
void recuperer_trame_memoire (char w);  //RECUPERE TRAME MET DANS LES VARIABLES PUIS ECRIT DANS L'EPROOM
void voir_qui_existe (void);
void TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(char c,char n); // que faire,numero cod
void INTERROMPRE(char gg);


void page (char  num);
int verifMP(void);
void  minitel(void);
void tabulation (void);
void saisie (void);
void gauche (void);
void droite (void);
void gestion_page (void);
void position_init (char Npage, char Nchamps);
void haut (void);
void envoi_cursur(void); 
void gestion_menu1 (void);
void gestion_menu2 (void);
void gestion_menu3 (void);
void gestion_menu5(void);
void nom_site (void);
void gestion_menu6 (void);
void gestion_menu6a(void);
void TRAMERECEPTIONminitel (void);
void gestion_menu6b (void);
void gestion_menu8 (void);


 
char gestion_rob (void);


void MODIFICATION_TABLE_DEXISTENCE(char *v,char eff,char etat);

void analyser_les_alarmes_en_cours(void);
void infos_etats(char *y,char t); //ADDITIONNE LE TYPE D'ETAT
void CHERCHER_DONNNEES_CABLES(char *g,char j);
unsigned char resume_voies(char y,char deb); //ANALYSE LES VOIES EXIST. y numero carte et deb mettre dans TRAMEOUT A PARTIR DE LA
void verif_heure_date (void);
 void RB1(void);

void CREER_TRAMEIN_CAPTEUR_DU_MINITEL(void);
void afficher_donnees_si_existe(char *y);
void enregistrer_donnees (void);
void sup_capteur (void); 
void calcul_c_l (void);   
char VALIDER_CAPTEUR(void); 
char EFFACER_CAPTEUR(void);

void Recuperer_param_cms(void);
void GROUPE_EN_MEMOIRE(unsigned char *tab,char lui);



unsigned char LIRE_HORLOGE(unsigned char zone);
void entrer_trame_horloge (void);
void creer_trame_horloge (void);  //CREATION D'UNR TRAME AVEC LES DONNEES DE L'HORLOGE
void LIRE_HEURE_MINITEL (void);
void ENVOYER_GROUPE (void);
char TYPEDECARTE(void);

char MOUCHARD(char carte);
char MOUCHARD2(void);
void testINT2 (void);
////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/////////////////INTERRUPT SERVICE ROUTINE RS232 /////////////////
#pragma code RB1 = 0x08
 void RB1(void)
{
INTCONbits.GIE = 0;
STOPMINITEL=0x00;
LEDD=1;
RTS=0;
DTR=0;


for (y[0]=0;y[0]<19;y[0]++)
COMMENTAIRE[y[0]]='#';



	 if (CMouPAS==0)
	{
	ACTIONENCOURS=0x00;
	}		
//CHERCHER_DONNEES_GROUPE();
if(INTCON3bits.INT1IF)	//detection 
{ 
REINITMODEM=0x00;
IO_EN_COM=1; // IO EST EN COM
					LEDC=1;
					TEMPO(1);
 					
						ETAT_CONNECT_MINITEL=0xFF; 
						//LEDC=0;
						INTERROMPRE(ACTIONENCOURS);
						//LEDC=0;
					 	TRAMERECEPTIONRS232DONNEES (250); //2<CR> 	
						
						COMMENTAIRE[0]=TRAMEIN[0];
						COMMENTAIRE[1]=TRAMEIN[1];
					
					
					
					
					
						
						if (STOPMINITEL==0xFF)
						goto finINT;
					//	LEDC=0;	
						if ((TRAMEIN[0]=='2') || ((TRAMEIN[1]=='2')))
						{		
						TRAMERECEPTIONRS232DONNEES (250); //2<CR>
					
						COMMENTAIRE[2]=TRAMEIN[0];
						COMMENTAIRE[3]=TRAMEIN[1];
						}
					
						if (STOPMINITEL==0xFF)
						goto finINT;
						
						
					
					//	LEDD=0;
					
					
					  
					
					
					    CMS_TO_TELGAT ();
					
					finINT:
					LEDC=0;
					
					//LEDC=0;
					//LEDD=0;
					if (REINITMODEM==0xFF)  //SEULEMENT SI COUPURE ANORMALE AU BOUT DE 2MN
						{				
						TEMPO(2);
						COUPMODEM=1;
						TEMPO(2);
						COUPMODEM=0;
						//TEMPO(1);
						//init_modem (0);	// A REMETTRE
						TEMPO(10);//TEMPO(10);             /// AEF
						init_modem (0);     ///AEF	
						goto reinit;
						}
					TEMPO(10);//TEMPO(10);             /// AEF
					init_modem (1);     ///AEF
					
reinit:							

					INTCON3bits.INT1IF=0;
					INTCONbits.GIE = 1;  
//IO_EN_COM=0; // IO EST plus EN COM
}
NBCOM=1;
LEDD=0;
LEDC=0;
//IO_EN_COM=0; // IO EST plus EN COM




}
 


////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////routine principale//////////////////////////////////////////////
void mainGESTIONCARTEIO (void)
{


unsigned char tabt;

DEBUTCOM:
TRISB=0xF7; //TT EN ENTREES SAUF RB3 RAZ CMS
RAZCMS=0;


CMouPAS=1;
testtimer=0;


ETAT_CONNECT_MINITEL=0x00;


TRISEbits.TRISE0=0;
TRISFbits.TRISF0=0; //EN SORTIE
TRISFbits.TRISF1=0;
TRISAbits.TRISA4=0;

TRISAbits.TRISA0=0;
TRISAbits.TRISA1=0;

LEDRJTEST1=0;
LEDRJTEST2=0;  

//TRISHbits.TRISH5=0;
TRISDbits.TRISD3=0;
TRISDbits.TRISD2=0; //LES I2C
TRISDbits.TRISD0=0;
TRISDbits.TRISD4=0;
TRISDbits.TRISD1=0;
TRISDbits.TRISD5=0;

IO_EN_COM=0; // IO N'EST PAS EN COM
TRISH=0b11001101;

NBCOM=0;


COUPMODEM=1;
TEMPO(2);
  
COUPMODEM=0; // PAR DEFAUT REPOSE MODEM ALLUME
TEMPO(2); 
PORTHbits.RH5=0;//!!!!!!!!!!!!!!

ADCON0=0x00;
ADCON1=0x0F;

//COUPMODEM=1;

//int i=0; 
//int j=0;
//TRISB=0xF7; //TT EN ENTREES SAUF RB3 RAZ CMS
//TRISH=0b11001101;
TRISCbits.TRISC5=0; 
TRISCbits.TRISC7=1;
TRISCbits.TRISC6=0;
TRISAbits.TRISA4=0;

PORTHbits.RH3=0;

ACTIONENCOURS=0;
//LEDRJTEST2=1;
init_RS232();	
	
TEMPO(1);// AVANT DE CONFIGURER MODEM !!!!!!!!!!!!!!!
STOPMINITEL=0x00;
init_modem (0);	// A REMETTRE
//minitel ();											





Init_RS485(); // A REMETTRE
Init_I2C();

//LEDRJTEST2=0;
//

//CHERCHER_DONNEES_GROUPE; //ASUP


INTCONbits.GIE=0; //AUTORISATION INT GENERALE



RCONbits.IPEN = 1; // activation niveau de priorit� il y en pas mais il faut pr 0x08
INTCONbits.RBIE=0;
INTCON2bits.INTEDG1=0; //FRONT DESCENDANT RB1

INTCON3bits.INT1IP=1; //?
INTCON3bits.INT1IE=1; //?




I2CMEMO3=0;
I2CMEMO=1;
I2CMEMO2=1;
I2CINTR=1;
I2CHORLG=1;
DEMANDE_DE_LAUTO=0x00;

//GROUPE_EN_MEMOIRE(TRAMEIN,3) ;
							
Recuperer_param_cms();



			LIRE_HEURE_MINITEL(); // ON LIT L'HEURE AU DEMARRAGE
			if (AA[2]=='0' && AA[3]=='0') //ANNEE 2000 ON RELANCE LE TIMER DANS LES SECONDES ici on regles l'heure)
			{//hh mm ss JJ MM AA 
			I2CHORLG=0;  
			HORLOGET[0]='0';
			HORLOGET[1]='7';	 	
			HORLOGET[2]='1';
			HORLOGET[3]='5';	 	
			HORLOGET[4]='0';
			HORLOGET[5]='0';	
			HORLOGET[6]='2';
			HORLOGET[7]='9';	
			HORLOGET[8]='1';
			HORLOGET[9]='1';	 	
			HORLOGET[10]='0';
			HORLOGET[11]='1';	 	
			entrer_trame_horloge (); //ECRITURE 
			I2CHORLG=1;
			} 
 

	//do
	//{	
	//LIRE_HEURE_MINITEL();
	//TEMPO(1);
	//}
	//while(1);

//LEDRJTEST2=1;

LEDC=0; 
LEDD=0; 
INTCON3bits.INT1IF=0;
INTCONbits.GIE=1; //AUTORISATION INT GENERALE
PORTAbits.RA4=0; // EN RECEPTION
DERE=0; //AUTORISATION DE RECEVOIR
RCSTA2bits.CREN  = 0; 




//DEMANDE_INFOS_CAPTEURS[0]='#';
//DEMANDE_INFOS_CAPTEURS[1]='0';
//DEMANDE_INFOS_CAPTEURS[2]='2';
//DEMANDE_INFOS_CAPTEURS[3]='2';
//DEMANDE_INFOS_CAPTEURS[4]='0';
//DEMANDE_INFOS_CAPTEURS[5]='1';
//LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM
//voir_qui_existe();


//goto ici; //A ENELVER
//analyser_les_alarmes_en_cours();

									if (CMouPAS==1)
									{
									

									
									
//									param:
									//goto fintest;
									
									
									//336,324,343,333,341
									
									
									//TEMPO(6);
									 //DEMANDE PARAMETRE ICI L'INTERRUPTION EST INTERDITE L'OPAERATION N'EST PAS LENTE EN THEORIE VALIDEE SEULE
									
										DEMANDE_INFOS_CAPTEURS[0]='x';
										DEMANDE_INFOS_CAPTEURS[1]='x';
										DEMANDE_INFOS_CAPTEURS[2]='x';
										DEMANDE_INFOS_CAPTEURS[3]='x';
										testINT2 ();
										if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
										{	
											ENVOIE_DES_TRAMES (0,336,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE PARAM 5 secondes ou plus
											if (TRAMEIN[8]=='D') //SI DD ON REFAIT LA DEMANDE
											{	
											TEMPO(10);
												testINT2 ();
												if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
												{	
												ENVOIE_DES_TRAMES (0,336,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE	
												}
											}
										Recuperer_param_cms(); //EST CE UTILE DE RECUPERER TT LE TEMPS
										}


//LEDRJTEST1=0;
										PORTHbits.RH5=0;
										TEMPO(1);
//goto param;
 										//TEMPO(6);
//goto noziz;
 
 
										//DEMANDEGROUPE ICI L'INTERRUPTION EST INTERDITE L'OPAERATION N'EST PAS LENTE EN THEORIE VALIDEE SEULE
										DEMANDE_INFOS_CAPTEURS[0]='x';
										DEMANDE_INFOS_CAPTEURS[1]='x';
										DEMANDE_INFOS_CAPTEURS[2]='x';
										DEMANDE_INFOS_CAPTEURS[3]='x'; 
										testINT2 ();
										if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
											{
											ENVOIE_DES_TRAMES (0,324,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE ENVIRON 7secondes ou plus
											if (TRAMEIN[8]=='D') //SI DD ON REFAIT LA DEMANDE
											{	
											TEMPO(10);
											testINT2 ();
												if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
												{
												ENVOIE_DES_TRAMES (0,324,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE	
												}
											}
											}

										//LEDRJTEST1=1;
										PORTHbits.RH5=0;
										//--------------------------------

										
										heure:
										//goto fintest;
										//TEMPO(6);
										TEMPO(1);
										DEMANDE_INFOS_CAPTEURS[0]='x';
										DEMANDE_INFOS_CAPTEURS[1]='x';
										DEMANDE_INFOS_CAPTEURS[2]='x';
										DEMANDE_INFOS_CAPTEURS[3]='x';
										testINT2 ();
										if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
										{
											ENVOIE_DES_TRAMES (0,341,&DEMANDE_INFOS_CAPTEURS) ; //ENVIRON 5secondes ou plus DEMANDE HEURE ICI L'INTERRUPTION EST INTERDITE L'OPAERATION N'EST PAS LENTE EN THEORIE VALIDEE SEULE
												if (TRAMEIN[8]=='D') //SI DD ON REFAIT LA DEMANDE
											{	
											TEMPO(10);
												testINT2 ();
																					if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
																					{
											ENVOIE_DES_TRAMES (0,341,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE	
																					}
												}
										
										}
										
										PORTHbits.RH5=0;
										//LEDRJTEST1=1;
										
										
										
										} //FIN DEMANDE GROUPE ET PARAM ET HEURE AU DEBUT SI CMS LAISSE LIO



TEMPO(1);
//ici:
//MOUCHARD('2');
//DEMANDE_INFOS_CAPTEURS[0]='#';
//DEMANDE_INFOS_CAPTEURS[1]='0';
//DEMANDE_INFOS_CAPTEURS[2]='1';
//DEMANDE_INFOS_CAPTEURS[3]='9';
//MOUCHARD2();
		while (1) 
		{
	
//ENVOYER_GROUPE();
//RECEVOIR_RS232();	
//voir_qui_existe();
//CMS_TO_TELGAT ();  //pour test local
                    
//	voir_qui_existe(); //PREPARATION DES TRAMES CPATEURS A ENVOYER




 

		testINT2 ();
		if (REA_DESACTIVE==0xFF)
		{REA_DESACTIVE=0x00;
		goto REAOFF;
		
		}
//ENVOYER_GROUPE();

	




					if (testtimer==1) //ENVIRON 24s au lieu de 30seconde coeff environ 0.8
					{
									for (TRAMEIN[124]=1;TRAMEIN[124]<=10;TRAMEIN[124]++) //10SECONDES PAR PAS DE 100ms
									{	
										
									  //TEST TIMER 10SECONDES PAR PAS DE 1S EN REALITE ENVIRON 8,46S donc 1= environ 804ms
									LEDC=1;
									TEMPO(1);
									 LEDC=!LEDC;
									TEMPO(1);
									LEDC=!LEDC;
									TEMPO(1);
									LEDC=!LEDC;
									TEMPO(1);
									LEDC=!LEDC;
									TEMPO(1);
									LEDC=!LEDC;
									TEMPO(1);
									LEDC=!LEDC;
									TEMPO(1);
									LEDC=!LEDC;
									TEMPO(1);
									LEDC=!LEDC;
									TEMPO(1);
									LEDC=!LEDC;
									TEMPO(1);
									LEDC=1;
									
									TEMPO(5);
									
									for (testtimer=1;testtimer<=100;testtimer++) //10SECONDES PAR PAS DE 100ms ICI 7.93secondes donc 100= environ 80ms
									{
									delay_qms(100);
									LEDC=!LEDC;
									}
									
									LEDC=0;
									TEMPO(5);
									}// 10x
					TRAMEIN[124]='-';	
					}

//	tmp=ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEIN),calcul_addmemoire(&TRAMEIN),&TRAMEIN,1); //ECRITURE EN EEPROM


// 	tmp=LIRE_EEPROM(choix_memoire(&TRAMEIN),calcul_addmemoire(&TRAMEIN),&TRAMEIN,1); //LIRE DANS EEPROM







//LEDRJTEST1=1;
 

//LEDD=!LEDD;
delay_qms(200);
 // LEDD=!LEDD;
delay_qms(200);
//LEDD=!LEDD;
delay_qms(200);
//LEDD=!LEDD;
delay_qms(200);
//LEDD=!LEDD;
delay_qms(200);
//LEDD=!LEDD;
delay_qms(200);
//LEDD=!LEDD;
delay_qms(200);  



//LEDC=1;


//TEMPO(60);
//TEMPO(60);
//TEMPO(60);
//TEMPO(60);
//TEMPO(60);
//TEMPO(60);
//TEMPO(60);




//LEDC=0;

//LEDRJTEST1=0;

//LEDD=0;
 


DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='1';
DEMANDE_INFOS_CAPTEURS[2]='2';
//CHERCHER_DONNNEES_CABLES(&DEMANDE_INFOS_CAPTEURS);


//voir_qui_existe(); //PREPARATION DES TRAMES CPATEURS A ENVOYER


//ENVOIE_DES_TRAMES (0,329,&TRAMEIN) ; //CREATION CAPTEUR SUIVANT TRA. MEMOIRE SI ERRFF (TERMINEACTION=1) ALORS L LA CREATION/MODIF AIT EU LIEU

// pompe direction assist�e pour berlingo
//analyser_les_alarmes_en_cours();

 


//----------------------------

// EFFACEMENT CAPTEUR
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='0';
DEMANDE_INFOS_CAPTEURS[2]='1';
DEMANDE_INFOS_CAPTEURS[3]='0';
DEMANDE_INFOS_CAPTEURS[4]='1';
//ENVOIE_DES_TRAMES (0,320,&TRAMEIN) ; //CREATION CAPTEUR SUIVANT TRA. MEMOIRE

NBCOM=3;
 
COM:
TEMPO(30); //ATTENDRE 30 ICI CAR ON ATTEND 20 DANS LA CM DE NON REPONSE AVANT DE FAIRE LES DIC
COM2:

			if (DEMANDE_DE_LAUTO!=0x00) //ON BLOQUE TANT QUE 
			{
			if (IO_EN_COM==1)
			IO_EN_COM=0; // IO EST plus EN COM 


						testINT2 ();
			if (REA_DESACTIVE==0xFF)
			{
			REA_DESACTIVE=0x00;
			//goto REAOFF;
			}
			TEMPO(1);
			//if (NBCOM==1)
			
			goto COM2;
			//else			
			

			}




	if (CMouPAS==1)
	{

REAOFF:

if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
NBCOM=2;
goto COM;
}
//if (NBCOM<=2) //BUG
//{
//NBCOM=0;
//NBCOM=3;
//goto COM;
//}


TEMPO(1);
//LEDRJTEST1=0;
 PORTHbits.RH5=0;
TEMPO(1);
//goto noziz;
//goto fintest; 


MODERAPID=1;


if ((TYPECARTE[0]=='E')||(TYPECARTE[0]=='M'))
{
//---------------------------------------------------------------------------------------------------------------
TEMPO(1);

// DEMANDE INFOS CAPTEURS ICI L'INTERRUPTION EST AUTORISEE
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='0';
DEMANDE_INFOS_CAPTEURS[2]='1';
DEMANDE_INFOS_CAPTEURS[3]='0';
DEMANDE_INFOS_CAPTEURS[4]='0';
DEMANDE_INFOS_CAPTEURS[5]='0';
DEMANDE_INFOS_CAPTEURS[6]='2';
DEMANDE_INFOS_CAPTEURS[7]='0';
DEMANDE_INFOS_CAPTEURS[8]='1';
DEMANDE_INFOS_CAPTEURS[9]='6';

testINT2 ();

if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
ENVOIE_DES_TRAMES (0,343,&DEMANDE_INFOS_CAPTEURS) ;//ENVIRON 1.49mn UNE CARTE ET 8.50mn LES 100VOIES ET 17CAPTEURS
  PORTHbits.RH5=0;
 //LEDRJTEST1=1;
}
 
if ((TYPECARTE[1]=='E')||(TYPECARTE[1]=='M'))
{
//---------------------------------------------------------------------------------------------------------------
TEMPO(6);

// DEMANDE INFOS CAPTEURS ICI L'INTERRUPTION EST AUTORISEE
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='2';
DEMANDE_INFOS_CAPTEURS[2]='1';
DEMANDE_INFOS_CAPTEURS[3]='0';
DEMANDE_INFOS_CAPTEURS[4]='0';
DEMANDE_INFOS_CAPTEURS[5]='0';
DEMANDE_INFOS_CAPTEURS[6]='4';
DEMANDE_INFOS_CAPTEURS[7]='0';
DEMANDE_INFOS_CAPTEURS[8]='1';
DEMANDE_INFOS_CAPTEURS[9]='6';

testINT2 ();


if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}		
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
ENVOIE_DES_TRAMES (0,343,&DEMANDE_INFOS_CAPTEURS) ;//ENVIRON 1.49mn UNE CARTE ET 8.50mn LES 100VOIES ET 17CAPTEURS
  PORTHbits.RH5=0;
 //LEDRJTEST1=1;
}

if ((TYPECARTE[2]=='E')||(TYPECARTE[2]=='M'))
{
//---------------------------------------------------------------------------------------------------------------
TEMPO(6);

// DEMANDE INFOS CAPTEURS ICI L'INTERRUPTION EST AUTORISEE
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='4';
DEMANDE_INFOS_CAPTEURS[2]='1';
DEMANDE_INFOS_CAPTEURS[3]='0';
DEMANDE_INFOS_CAPTEURS[4]='0';
DEMANDE_INFOS_CAPTEURS[5]='0';
DEMANDE_INFOS_CAPTEURS[6]='6'; //6
DEMANDE_INFOS_CAPTEURS[7]='0';//0
DEMANDE_INFOS_CAPTEURS[8]='1';
DEMANDE_INFOS_CAPTEURS[9]='6';

testINT2 ();
if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
ENVOIE_DES_TRAMES (0,343,&DEMANDE_INFOS_CAPTEURS) ;//ENVIRON 1.49mn UNE CARTE ET 8.50mn LES 100VOIES ET 17CAPTEURS
  PORTHbits.RH5=0;
 //LEDRJTEST1=1;
}


if ((TYPECARTE[3]=='E')||(TYPECARTE[3]=='M'))
{
//---------------------------------------------------------------------------------------------------------------
TEMPO(6);

// DEMANDE INFOS CAPTEURS ICI L'INTERRUPTION EST AUTORISEE
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='6';
DEMANDE_INFOS_CAPTEURS[2]='1';
DEMANDE_INFOS_CAPTEURS[3]='0';
DEMANDE_INFOS_CAPTEURS[4]='0';
DEMANDE_INFOS_CAPTEURS[5]='0';
DEMANDE_INFOS_CAPTEURS[6]='8';
DEMANDE_INFOS_CAPTEURS[7]='0';
DEMANDE_INFOS_CAPTEURS[8]='1';
DEMANDE_INFOS_CAPTEURS[9]='6';

testINT2 ();
if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
ENVOIE_DES_TRAMES (0,343,&DEMANDE_INFOS_CAPTEURS) ;//ENVIRON 1.49mn UNE CARTE ET 8.50mn LES 100VOIES ET 17CAPTEURS
  PORTHbits.RH5=0; 
 //LEDRJTEST1=1;
}

if ((TYPECARTE[4]=='E')||(TYPECARTE[4]=='M'))
{

TEMPO(6); 

// DEMANDE INFOS CAPTEURS ICI L'INTERRUPTION EST AUTORISEE
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='8';
DEMANDE_INFOS_CAPTEURS[2]='1';
DEMANDE_INFOS_CAPTEURS[3]='0';
DEMANDE_INFOS_CAPTEURS[4]='0';
DEMANDE_INFOS_CAPTEURS[5]='1'; //1
DEMANDE_INFOS_CAPTEURS[6]='0';
DEMANDE_INFOS_CAPTEURS[7]='0';
DEMANDE_INFOS_CAPTEURS[8]='1';
DEMANDE_INFOS_CAPTEURS[9]='6';

testINT2 ();
if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
ENVOIE_DES_TRAMES (0,343,&DEMANDE_INFOS_CAPTEURS) ;//ENVIRON 1.49mn UNE CARTE ET 8.50mn LES 100VOIES ET 17CAPTEURS
  PORTHbits.RH5=0;
 //LEDRJTEST1=1;
}


//goto fintest;
TEMPO(6);

noziz:
//----------------------------------------------------------------------------------------------------------------
if ((TYPECARTE[0]=='E')||(TYPECARTE[0]=='M'))
{ 
  TEMPO(2);

 //DEMANDE TABLE EXISTENCE ICI L'INTERRUPTION EST INTERDITE L'OPAERATION N'EST PAS LENTE EN THEORIE
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='1';
//DEMANDE_INFOS_CAPTEURS[2]='3';
//DEMANDE_INFOS_CAPTEURS[3]='0';
//DEMANDE_INFOS_CAPTEURS[4]='0';
testINT2 ();	
if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
{
ENVOIE_DES_TRAMES (0,333,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE EXISTENCE A CM 8 SECONDES LA CARTE
	if (TRAMEIN[8]=='D') //SI DD ON REFAIT LA DEMANDE
	{	
	TEMPO(10); //RE ESSAYE AU BOUT DE 6secondes
											testINT2 ();
											if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
											{
											ENVOIE_DES_TRAMES (0,333,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE	
											}
	}
}
//LEDRJTEST1=0;
PORTHbits.RH5=0;
}
//----------------------------------------------------------------------------------------------------------------



//----------------------------------------------------------------------------------------------------------------
if ((TYPECARTE[1]=='E')||(TYPECARTE[1]=='M'))
{ 
  TEMPO(2);

 //DEMANDE TABLE EXISTENCE ICI L'INTERRUPTION EST INTERDITE L'OPAERATION N'EST PAS LENTE EN THEORIE
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='2';
//DEMANDE_INFOS_CAPTEURS[2]='3';
//DEMANDE_INFOS_CAPTEURS[3]='0';
//DEMANDE_INFOS_CAPTEURS[4]='0';
testINT2 ();	
if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
{
ENVOIE_DES_TRAMES (0,333,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE EXISTENCE A CM
	if (TRAMEIN[8]=='D') //SI DD ON REFAIT LA DEMANDE
	{	
	TEMPO(10);
		testINT2 ();
											if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
											{
	ENVOIE_DES_TRAMES (0,333,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE	
		}
}
}
//LEDRJTEST1=0;
PORTHbits.RH5=0;
}
//----------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------
if ((TYPECARTE[2]=='E')||(TYPECARTE[2]=='M'))
{ 
  TEMPO(2);

 //DEMANDE TABLE EXISTENCE ICI L'INTERRUPTION EST INTERDITE L'OPAERATION N'EST PAS LENTE EN THEORIE
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='3';
//DEMANDE_INFOS_CAPTEURS[2]='3';
//DEMANDE_INFOS_CAPTEURS[3]='0';
//DEMANDE_INFOS_CAPTEURS[4]='0';
testINT2 ();	
if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
{
ENVOIE_DES_TRAMES (0,333,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE EXISTENCE A CM
	if (TRAMEIN[8]=='D') //SI DD ON REFAIT LA DEMANDE
	{	
	TEMPO(10);
	testINT2 ();
											if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
											{
	ENVOIE_DES_TRAMES (0,333,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE	
	}
	}
}
//LEDRJTEST1=0;
PORTHbits.RH5=0;
}
//----------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------
if ((TYPECARTE[3]=='E')||(TYPECARTE[3]=='M'))
{ 
  TEMPO(2);

 //DEMANDE TABLE EXISTENCE ICI L'INTERRUPTION EST INTERDITE L'OPAERATION N'EST PAS LENTE EN THEORIE
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='4';
//DEMANDE_INFOS_CAPTEURS[2]='3';
//DEMANDE_INFOS_CAPTEURS[3]='0';
//DEMANDE_INFOS_CAPTEURS[4]='0';
testINT2 ();	
if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
{
ENVOIE_DES_TRAMES (0,333,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE EXISTENCE A CM
	if (TRAMEIN[8]=='D') //SI DD ON REFAIT LA DEMANDE
	{	
	TEMPO(10);
		testINT2 ();
											if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
											{
	ENVOIE_DES_TRAMES (0,333,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE	
											}
	}
}
//LEDRJTEST1=0;
PORTHbits.RH5=0;
}
//----------------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------------
if ((TYPECARTE[4]=='E')||(TYPECARTE[4]=='M'))
{ 
  TEMPO(2);

 //DEMANDE TABLE EXISTENCE ICI L'INTERRUPTION EST INTERDITE L'OPAERATION N'EST PAS LENTE EN THEORIE
DEMANDE_INFOS_CAPTEURS[0]='0';
DEMANDE_INFOS_CAPTEURS[1]='5';
//DEMANDE_INFOS_CAPTEURS[2]='3';
//DEMANDE_INFOS_CAPTEURS[3]='0';
//DEMANDE_INFOS_CAPTEURS[4]='0';
testINT2 ();	
if (IO_EN_COM==1)
{
IO_EN_COM=0; // IO EST plus EN COM 
goto COM;
}
if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
{
ENVOIE_DES_TRAMES (0,333,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE EXISTENCE A CM
	if (TRAMEIN[8]=='D') //SI DD ON REFAIT LA DEMANDE
	{	
	TEMPO(10);
			testINT2 ();
											if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
											{
		ENVOIE_DES_TRAMES (0,333,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE	
		}
	}
}
//LEDRJTEST1=0;
PORTHbits.RH5=0;
}



//----------------------------------------------------------------------------------------------------------------

TEMPO(1); 
										//DEMANDEGROUPE ICI L'INTERRUPTION EST INTERDITE L'OPAERATION N'EST PAS LENTE EN THEORIE VALIDEE SEULE
										DEMANDE_INFOS_CAPTEURS[0]='x';
										DEMANDE_INFOS_CAPTEURS[1]='x';
										DEMANDE_INFOS_CAPTEURS[2]='x';
										DEMANDE_INFOS_CAPTEURS[3]='x'; 
										testINT2 ();
										if (IO_EN_COM==1)
										{
										IO_EN_COM=0; // IO EST plus EN COM 
										goto COM;
										}
										if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
											{
											ENVOIE_DES_TRAMES (0,324,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE ENVIRON 7secondes ou plus
											if (TRAMEIN[8]=='D') //SI DD ON REFAIT LA DEMANDE
											{	
											TEMPO(10);
											testINT2 ();
		 										if (DEMANDE_DE_LAUTO==0x00) // SI PAS DEMANDE AUTO
												{
												ENVOIE_DES_TRAMES (0,324,&DEMANDE_INFOS_CAPTEURS) ; //DEMANDE GROUPE	
												}
											}
											}

										//LEDRJTEST1=1;
										PORTHbits.RH5=0;
										//--------------------------------









 


 //TEMPO(30); // POUR 30 ON A 25Secondes DONC 100ms DELAY_qms ENVIRON 83MS

NBCOM=3;
//TEMPO(20); //pour 100 1mn23s pour 10 8s
fintest:
//TEMPO(10);

PORTHbits.RH5=0;
goto COM;
 
//if ((TYPECARTE[0]=='E')||(TYPECARTE[0]=='M'))
//{
	//for (pas=0;pas<;pas++)
	//{
			testINT2 ();
			if (REA_DESACTIVE==0xFF)
			{
			REA_DESACTIVE=0x00;
			goto REAOFF;  
			
			}
			
//	}
//}








                                                            
//delay_qms(100);
//delay_qms(100);
//delay_qms(100);
delay_qms(100);

//RS485('U');
//RS485('A');
//RS485('B');


//RECEVOIRST_RS485();
  
//--------------------------------


  

						

							}//FIN(CMouPAS==1)

	



}//FIN WHILE(1)

}
 
 
 
     
     
  
///////////////////////////////////////////////////////////// 
//////////r�c�ption des donn�es/////////////////////////////
  
    
  
 char RECEVOIR_RS232(void) 
{
unsigned int cop,cop2;
//RX_TX=0;

TXSTA1bits.TXEN=0;
RCSTA1bits.CREN =1;
 NORECEP232=0x00;
cop=0;
cop2=0;	//cop2<40 environ 13s 400envrion 2mn
while ((PIR1bits.RC1IF==0)&&(cop2<400)&&(STOPMINITEL!=0xFF))//while (PIR1bits.RC1IF==0) et 0x11 caractere de FIN MODEM
{
cop++; 
	
//cop2=0;
		if (cop>50000)
		{
		cop2++;
		cop=0;
		}
}
 





carac=RCREG1;

if (cop2==400)
REINITMODEM=0xFF;


if ((cop2==400)||(carac==0x11))
STOPMINITEL=0xFF;





TXSTA1bits.TXEN =1;
RCSTA1bits.CREN =0;




//RCSTAbits.CREN  = 0; 
//printf("%u \n",carac);
//PIR1bits.RC1IF=0;
return(carac);
}

////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
//////////////////////////////////////envoi sur rs232/////////
void RS232(char carac1)
{
//RX_TX=1;
TXSTA1bits.TXEN =1;
RCSTA1bits.CREN =0; 

TXREG1=carac1;
while (TXSTA1bits.TRMT == 0);

while(!PIR1bits.TXIF);

TXSTA1bits.TXEN=0;
RCSTA1bits.CREN =1;  
//RX_TX=0;
}
     
        
   
///////////////////////////////////////////////////////////////////////////////
/////////////////////tompo////////////////////////////////////////////////
void delay_qms(char tempo)
{ 
unsigned char j,k,l;
   
for (j=0;j<tempo;j++) 
{
  for (k=0;k<10;k++)
    {  
    for (l=0;l<40;l++)
      {//
      //__no_operation();
      //__no_operation();
      //__no_operation();
      //__no_operation();
      //__no_operation();
      }
    }
  }  
j=0;
}  

  

/////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////
////////////////////////////initiation de la communication rs232///////////////////
void init_RS232(void)
{
//DTR=0;
//RTS=0;


RCSTA1bits.SPEN =1;
TXSTA1bits.SYNC =0;
BAUDCON1bits.BRG16 =0;

TXSTA1bits.BRGH =0;
SPEED =624;
SPBRG1=40;
//SPBRGH1=0x00;

IPR1bits.RC1IP =1;

//PIE1bits.RC1IE =1;

TXSTA1bits.TXEN=1;
RCSTA1bits.RX9=0;
TXSTA1bits.TX9= 0;
RCSTA1bits.CREN =1;



RCONbits.IPEN =1;
INTCONbits.GIE=1; // autorise IT 
INTCONbits.PEIE=1; 
PIE1bits.TX1IE   = 0; 
PIE1bits.RC1IE =0;


//PIR1bits.RC1IF =0;
}
 





//////////////////////////////////////////////////////////////////
// ENVOI D'UNE TRAME COMPLETE RS232 MODEM        		   	        //
//////////////////////////////////////////////////////////////////



// STOCKAGE DU RESULTAT DANS LA TRAMEOUT[]

void TRAMEENVOIRS232DONNEES (int val)
{
unsigned char u;
unsigned int tmp;
tmp=0;
TXSTA1bits.TXEN =1;
RCSTA1bits.CREN =0;

do 
{
ret1:
		u=TRAMEOUT[tmp];	
		RS232(u);
		//if ((u!='=') && (tmp==0))
		//goto ret1;
	
		
		tmp=tmp+1;
}
while ((u!=0x0D) && (tmp<val)&& (u!=0x04));

TXSTA1bits.TXEN =0;
RCSTA1bits.CREN =1;

delay_qms(100);
 
}


 
 





//////////////////////////////////////////////////////////////////
// RECEPTION D'UNE TRAME COMPLETE RS485          		   		//
//////////////////////////////////////////////////////////////////

//exemple #R1CMDIC03300R111122223333BRA/EEEE/22/1COMMENTAIRESICIROB0332611152500102000045678110865088822222*

// STOCKAGE DU RESULTAT DANS LA TRAMEIN[]




void TRAMERECEPTIONRS232DONNEES (int val2) 
{
unsigned char u;
unsigned char tmp;
tmp=0;
TXSTA1bits.TXEN =0;
RCSTA1bits.CREN =1;

do
{
ret1:
		u=RECEVOIR_RS232();
		//if ((u!='=') && (tmp==0))
		//goto ret1;
	
		TRAMEIN[tmp]=u;
		tmp=tmp+1;
}
while ((STOPMINITEL!=0xFF)&&(u!=0x0D) && (u!='N') && (u!=0x06) && (u!=0x15) && (u!=0x0F)&&(tmp<val2)); // <CR> ou 'N' (ICOG:N ou IMIN) ou <ACK> ou <NOTACK> ou <NOTACK autre> ou nombre de car. maaxi


TXSTA1bits.TXEN =1;
RCSTA1bits.CREN =0;

if (STOPMINITEL==0xFF)
delay_qms(100);



//delay_qms(100);
 
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////// initialisation du modem//////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void init_modem (char youyou){

//si 0 config avec ATZ
//si 1 config sans ATZ  
	//DTR=0;


																	TRAMEOUT[0]='A';//#   ////// racrochage de ligne en cas ou la ligne est toujours fonctionnelle
																	TRAMEOUT[1]='T';
																	TRAMEOUT[2]='H';
										 							TRAMEOUT[3]='0';
                                                                    TRAMEOUT[4]=0x0D;

                                                                    TRAMEENVOIRS232DONNEES (6); //CMS OK

																// TRAMERECEPTIONRS232DONNEES ();
                                                                  
	                                                                

																	TRAMEOUT[0]='A';                   //// initialisation 
																	TRAMEOUT[1]='T';
																	TRAMEOUT[2]=0x0D;
                                                                   TRAMEENVOIRS232DONNEES (4); //CMS OK

																//	TRAMERECEPTIONRS232DONNEES ();
                                                               	    delay_qms(50);
																	TRAMEOUT[0]='A';
																	TRAMEOUT[1]='T';              ////r�initialisation restauration
                                                                     
																	if (youyou==0) 
																	{	
																	TRAMEOUT[2]='Z';
																	TRAMEOUT[3]=0x0D;
                                                                    TRAMEENVOIRS232DONNEES (5); //CMS OK
																	//TRAMERECEPTIONRS232DONNEES ();
																	TRAMEOUT[0]='A';                   //// initialisation 
																	TRAMEOUT[1]='T';
																	TRAMEOUT[2]=0x0D;
                                                                   TRAMEENVOIRS232DONNEES (4); //CMS OK																		
             													    }



																	if ((youyou==1)||(youyou==3))
																	{																	
																	TRAMEOUT[2]=0x0D;
                                                                    TRAMEENVOIRS232DONNEES (4); //CMS OK
																	//TRAMERECEPTIONRS232DONNEES ();
              													    }
																
														

 

																	if ((youyou==0) || (youyou=1))
																	{	

				                                           
																	//TRAMERECEPTIONRS232DONNEES ();
delay_qms(50);	                                                               
																	TRAMEOUT[0]='A';       /////////d�but d'une commande AT
                                                                    TRAMEOUT[1]='T';
																	TRAMEOUT[2]='&';      ////
                                                                    TRAMEOUT[3]='F';      //// permet de configurer le modem
                                                                    TRAMEOUT[4]='B';      /////// B0: mode de fonctionnement automatique (vitesse)
                                                                    TRAMEOUT[5]='0';
                                                                    TRAMEOUT[6]='X';      //////  X0: Envoie seulement les messages OK, CONNECT, RING,NO CARRIER, ERROR.
                                                                    TRAMEOUT[7]='0';
                                                                    TRAMEOUT[8]='E';      /////   E0: pas d'echo
                                                                    TRAMEOUT[9]='0';
                                                                    TRAMEOUT[10]='V';     //////  V0: messages de forme num�riqe
                                                                    TRAMEOUT[11]='0';
                                                                    TRAMEOUT[12]='L';     //////  L3: niveau sonore maximal
                                                                    TRAMEOUT[13]='3';
                                                                    TRAMEOUT[14]='M';     //////  M2: haut parleur constamment en service
                                                                    TRAMEOUT[15]='2';
                                                                    TRAMEOUT[16]='S';     //////  S0=2: Nombre de sonneries avant d�crochage
                                                                    TRAMEOUT[17]='0';
                                                                    TRAMEOUT[18]='='; 
                                                                    TRAMEOUT[19]='2';
                                                                    TRAMEOUT[20]='S';      ////   S7=60: Temps d�attente maximum entre la prise de ligne et la connexion.
                                                                    TRAMEOUT[21]='7';
                                                                    TRAMEOUT[22]='=';
                                                                    TRAMEOUT[23]='6';
                                                                     TRAMEOUT[24]='0';
                                                                    TRAMEOUT[25]='&';      /////  &K4: Contr�le de flux par caract�res : XON/XOFF
                                                                    TRAMEOUT[26]='K';
                                                                    TRAMEOUT[27]='4'; 
                                                                    TRAMEOUT[28]=0x0D;

                                                                    TRAMEENVOIRS232DONNEES (30);

																	TRAMERECEPTIONRS232DONNEES (256); //0<CR> veut dire ok
                                                                    delay_qms(50);
                                                                    TRAMEOUT[0]='A';  /// initialisation apr�s configuration 
                                                                    TRAMEOUT[1]='T';
                                                                    TRAMEOUT[2]=0x0D;
                                     				
															TRAMEENVOIRS232DONNEES (4);
																TRAMERECEPTIONRS232DONNEES (256); //0<CR> veut ok


																	delay_qms(50);

																	delay_qms(50);
																	delay_qms(50);

																	} 

}
  

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////// routine d'�change entre telgat et CMS/////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void CMS_TO_TELGAT (void){
																if ((TRAMEIN[0]=='2') || (TRAMEIN[1]=='2')) //BUG ON A DES FOIS 02<CR> ou 2<CR>
                                                             	{
																TRAMERECEPTIONRS232DONNEES (250); // 1<CR>
																	COMMENTAIRE[4]=TRAMEIN[0];
																	COMMENTAIRE[5]=TRAMEIN[1];
																}

																if (STOPMINITEL==0xFF)
																goto fincmstelgat;

															//	LEDC=0;
	                                                            TRAMERECEPTIONRS232DONNEES (250);  // I:COG,C:N ou I:MIN
																	COMMENTAIRE[6]=TRAMEIN[0];
																	COMMENTAIRE[7]=TRAMEIN[1];
																	COMMENTAIRE[8]=TRAMEIN[2];
																	COMMENTAIRE[9]=TRAMEIN[3];
																	COMMENTAIRE[10]=TRAMEIN[4];
																	COMMENTAIRE[11]=TRAMEIN[5];
																	COMMENTAIRE[12]=TRAMEIN[6];



																//LEDD=1;
																if (STOPMINITEL==0xFF)
																goto fincmstelgat;	
															//	LEDD=0;
                                        /////////////////////////////// envoyer le num�ro de t�l�phone /////////////////////////////////////////

                                                kkk=0;
                                                    etiq:


														
													//INTERROMPRE(ACTIONENCOURS);	//INTERROMPRE CYCLE	
                                                                  if (TRAMEIN[2]=='M' || TRAMEIN[3]=='M' )
																	{		
                                                                      minitel(); //MINITEL
                                                                     goto MINITEL;
																	}                  



                                                                  if ((TRAMEIN[8]=='N')|| (TRAMEIN[9]=='N') ){
																		//	LEDC=1;				
                                                                      goto TELGAT; //TELGAT
                                                                                       }
                                                                   else {
                                                                         //goto etiq;
                                                                          kkk++; 
                                                                             if (kkk==2){goto etiq1;}
                                                                              else {goto etiq;}


                                                                        }
                                             
													

												   TELGAT:   // TELGAT                   
                                                                    ETAT_CONNECT_MINITEL=0x00; // CONNECTION 29/08/2017
                                                                    TRAMEOUT[0]=0x02;
                                                                    TRAMEOUT[1]=nr_cms[0];
                                                                    TRAMEOUT[2]=nr_cms[1];
                                                                    TRAMEOUT[3]=nr_cms[2];
                                                                    TRAMEOUT[4]=nr_cms[3];
                                                                    TRAMEOUT[5]=nr_cms[4];
                                                                    TRAMEOUT[6]=nr_cms[5];
                                                                    TRAMEOUT[7]=nr_cms[6];
                                                                    TRAMEOUT[8]=nr_cms[7];
                                                                    TRAMEOUT[9]=nr_cms[8];
                                                                    TRAMEOUT[10]=nr_cms[9];
                                                                    TRAMEOUT[11]=0x03;
																	CRC=CRC16(TRAMEOUT, 12);   ///////// calcul de CRC
                                                                 	IntToChar(CRC,tableau,5);   //////// conversion du CRC du int en char
                                                                    TRAMEOUT[12]=tableau[0];
                                                                    TRAMEOUT[13]=tableau[1];
                                                                    TRAMEOUT[14]=tableau[2];
                                                                    TRAMEOUT[15]=tableau[3];
                                                                    TRAMEOUT[16]=tableau[4];
                                                                    TRAMEOUT[17]=0x0D;
																    TRAMEENVOIRS232DONNEES (19);  ////// envoi de la trame 
                                                                    TRAMERECEPTIONRS232DONNEES (256);   /////  r�c�ption de r�ponse
                                                                   switch (TRAMEIN[0]) {
                                                                      case (0x06):             //////////////// v�rification du caract�re recue si ask on continue si non on renvoie une autre fois avant de passer a la suite 
                                                                     goto etiq2;
                                                                      case (0x0f):
                                                                     kkk++;
                                                                     if (kkk==2){
                                                                     goto etiq2;
                                                                     } 
                                                                      else{goto etiq1;} 
                                                                      default :
                                                                        break;
                                                                       }
			                                                       	delay_qms(50);
                                   		                            kkk=0;
                                  etiq2 :                            
                                                                    TRAMEOUT[0]=0x02;    //////////////////////////////////////  type de communication �tendu E ou normal N
                                                                    TRAMEOUT[1]='C';
                                                                    TRAMEOUT[2]=':';
                                                                    TRAMEOUT[3]='N';
                                                                    TRAMEOUT[4]=0x03;
                                                                    CRC=CRC16(TRAMEOUT, 5);   /// calcul de CRC
                                                                 	IntToChar(CRC,tableau,5); /// convertir le CRC en CHAR pour l'envoyer 
                                                                    TRAMEOUT[5]=tableau[0];    ////// ajouter le CRC a la trame de sortie
                                                                    TRAMEOUT[6]=tableau[1];
                                                                    TRAMEOUT[7]=tableau[2];
                                                                    TRAMEOUT[8]=tableau[3];
                                                                    TRAMEOUT[9]=tableau[4];
                                                                    TRAMEOUT[10]=0x0D;
                                                                	TRAMEENVOIRS232DONNEES (12);  
                                                                    TRAMERECEPTIONRS232DONNEES (256);
                                                                    switch (TRAMEIN[0]) {
                                                                      case (0x06): 
                                                                     goto etiq3;
                                                                      case (0x0f):
                                                                     kkk++;
                                                                     if (kkk==2){
                                                                     goto etiq3;
                                                                     } 
                                                                      else{goto etiq2;} 
                                                                      default :
                                                                        break;
                                                                       }
                                         kkk=0;
			                                                       
                                     etiq3 :                        
                                                                    TRAMEOUT[0]=0x02;      /////////////// envoyer le nom du site
                                                                    TRAMEOUT[1]='S';
                                                                    TRAMEOUT[2]=':';
                                                                    TRAMEOUT[3]=ID_rack[0];
                                                                    TRAMEOUT[4]=ID_rack[1];
                                                                    TRAMEOUT[5]=ID_rack[2];
                                                                    TRAMEOUT[6]=ID_rack[3];
                                                                    TRAMEOUT[7]=ID_rack[4];
                                                                    TRAMEOUT[8]=ID_rack[5];
                                                                    

                                                                    TRAMEOUT[9]=ID_rack[6];
                                                                    TRAMEOUT[10]=ID_rack[7];
                                                                    TRAMEOUT[11]=ID_rack[8];
                                                                    TRAMEOUT[12]=ID_rack[9];
                                                                    TRAMEOUT[13]=ID_rack[10];
																	TRAMEOUT[14]=ID_rack[11];
                                                                    TRAMEOUT[15]=ID_rack[12];
                                                                    TRAMEOUT[16]=ID_rack[13];
                                                                    TRAMEOUT[17]=ID_rack[14];
                                                                    TRAMEOUT[18]=ID_rack[15];
																	TRAMEOUT[19]=ID_rack[16];
                                                                    TRAMEOUT[20]=ID_rack[17];
                                                                    TRAMEOUT[21]=ID_rack[18];
                                                                    TRAMEOUT[22]=ID_rack[19];
                                                                    TRAMEOUT[23]=0x03;//10531
                                                                    //TRAMEENVOIRS232DONNEES ();
                                                                    CRC=CRC16(TRAMEOUT, 24);
                                                                 	IntToChar(CRC,tableau,5);
                                                                    TRAMEOUT[24]=tableau[0];
                                                                    TRAMEOUT[25]=tableau[1];
                                                                    TRAMEOUT[26]=tableau[2];
                                                                    TRAMEOUT[27]=tableau[3];
                                                                    TRAMEOUT[28]=tableau[4];
                                                                    TRAMEOUT[29]=0x0D;
                                                                	TRAMEENVOIRS232DONNEES (31);
                                                                    TRAMERECEPTIONRS232DONNEES (256);	
                                                                    switch (TRAMEIN[0]) {
                                                                      case (0x06): 
                                                                     goto etiq4;
                                                                      case (0x0f):
                                                                     kkk++;
                                                                     if (kkk==2){
                                                                     goto etiq4;
                                                                     } 
                                                                      else{goto etiq3;} 
                                                                      default :
                                                                        break;
                                                                       }
                                                                    kkk=0;
                                            etiq4:                
                                                                    TRAMEOUT[0]=0x02;        ////////////
                                                                    TRAMEOUT[1]='E';
                                                                    TRAMEOUT[2]=':';
                                                                    TRAMEOUT[3]='0';
                                                                    TRAMEOUT[4]=0x03;
                                                                    CRC=CRC16(TRAMEOUT, 5);
                                                                 	IntToChar(CRC,tableau,5);
                                                                    TRAMEOUT[5]=tableau[0];
                                                                    TRAMEOUT[6]=tableau[1];
                                                                    TRAMEOUT[7]=tableau[2];
                                                                    TRAMEOUT[8]=tableau[3];
                                                                    TRAMEOUT[9]=tableau[4];
                                                                    TRAMEOUT[10]=0x0D;
                                                                 	
                                                                	TRAMEENVOIRS232DONNEES (12);
                	
                                                                   TRAMERECEPTIONRS232DONNEES (256);	
	                                                                 switch (TRAMEIN[0]) {
                                                                      case (0x06): 
                                                                     goto etiq5;
                                                                      case (0x0f):
                                                                     kkk++;
                                                                     if (kkk==2){
                                                                     goto etiq5;
                                                                     } 
                                                                      else{goto etiq4;} 
                                                                      default :
                                                                        break;
                                                                       }
                                                                      kkk=0;
                                                                   
                                etiq5:              // ////////// envoyer les capteurs
																   if (CMouPAS==1)
																	ENVOYER_GROUPE();
																	voir_qui_existe(); //PREPARATION DES TRAMES CPATEURS A ENVOYER
                                                     
                                                                                                                        							//i=i+1;
                                
                                                     etiq10:  
                                                                    TRAMEOUT[0]=0X04;     //////////// bits de fin de trame 
                                                                    TRAMEENVOIRS232DONNEES (1);
                                                                    TRAMERECEPTIONRS232DONNEES (3);
                                                                    kkk=0;
																	
				

//                                                                 	TRAMEOUT[0]='A';//# racrochage 
//																	TRAMEOUT[1]='T';
//																	TRAMEOUT[2]='H';
//										 							TRAMEOUT[3]='0';
                                                                    //TRAMEOUT[4]=0x0D;
                   													





MINITEL:

fincmstelgat:
delay_qms(10);
etiq1:
delay_qms(10);




}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// calcul du CRC pour /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CRC_POLY 0x01021 //0x01021
#define CRC_START 0xFFFF
unsigned int CRC16(char far *txt, unsigned  int lg_txt)
{ 
	 unsigned char nn;
	unsigned int ii;
	unsigned  int crc;
    
	 char far *p;
	crc = CRC_START;
	p=txt;

//essai[2]=*p;
ii=0;
nn=0;
	//for( ii=0; ii<lg_txt ; ii++, p++)
   do {
crc ^= (unsigned int)*p;
//for(; nn<8; nn++)
           do {
                  if (crc & 0x8000)
                     {
                         crc<<=1;
//UTIL;
                          crc^=CRC_POLY;
                      }
                      else
                      {crc<<=1;
                      }
                       nn++;
              }while (nn<8);
                      ii=ii+1;
                      p++;
if (nn==8){nn=0;}
     } while (ii<lg_txt);
ii=0;
return crc;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////// CONVERSION UNSIGNED INT TO CHAR///////////////////////////////////////////////////////////////////////////////////////// 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void IntToChar(unsigned int value, char *chaine,char Precision)
{
unsigned int Mil, Cent, Diz, Unit, DMil;
int count = 0;

// initialisation des variables
Mil = 0; Cent = 0; Diz = 0; Unit = 0;DMil=0;
/*
if (value < 0) // si c'est un nombre n�gatif
    {
    value = value * (-1);
    *chaine = 45; // signe '-'
    count = 1;  // c'est un nombre n�gatif
    }
else // si c'est un nombre positif
    {
    count = 1;
    *chaine = 32; // code ASCII du signe espace ' '
    }
*/
// si la valeur n'est pas nulle
if (value != 0)
    {
if (Precision >= 5) // si l'utilisateur d�sire les milliers
         {
         // conversion des milliers
         DMil = value / 10000;
         if (DMil != 0)
               {
               *(chaine+count) = DMil + 48;
               if (*(chaine+count) < 48)
                     *(chaine+count) = 48;
               if (*(chaine+count) > 57)
                     *(chaine+count) = 57;
               }
         else
               *(chaine+count) = 48;
         count++;
         }
    if (Precision >= 4) // si l'utilisateur d�sire les milliers
         {
         // conversion des milliers
         Mil = value-(DMil*10000);
         Mil = Mil / 1000;
         if (Mil != 0)
               {
               *(chaine+count) = Mil + 48;
               if (*(chaine+count) < 48)
                     *(chaine+count) = 48;
               if (*(chaine+count) > 57)
                     *(chaine+count) = 57;
               }
         else
               *(chaine+count) = 48;
         count++;
         }

    if (Precision >= 3) // si l'utilisateur d�sire les centaines
        {
        // conversion des centaines
        Cent = value - (Mil * 1000)-(DMil*10000);
        Cent = Cent / 100;
        if (Cent != 0)
            {
            *(chaine+count) = Cent + 48;
            if (*(chaine+count) < 48)
                  *(chaine+count) = 48;
            if (*(chaine+count) > 57)
                  *(chaine+count) = 57;
            }
        else
            *(chaine+count) = 48;
            count++;
      }

    if (Precision >= 2) // si l'utilisateur d�sire les dizaines
         {
         // conversion des dizaines
         Diz = value - (Mil * 1000) - (Cent * 100)-(DMil*10000);
         Diz = Diz / 10;
         if (Diz != 0)
            {
            *(chaine+count) = Diz + 48;
            if (*(chaine+count) < 48)
                  *(chaine+count) = 48;
            if (*(chaine+count) > 57)
                  *(chaine+count) = 57;
            }
         else
            *(chaine+count) = 48;
         count++;
         }

    // conversion unit�s
    Unit = value - (Mil * 1000) - (Cent * 100) - (Diz * 10)-(DMil*10000);
    *(chaine+count) = Unit + 48;
    if (*(chaine+count) < 48)       // limites : 0 et 9
            *(chaine+count) = 48;
    if (*(chaine+count) > 57)
            *(chaine+count) = 57;
    }
else // if (value == 0)
    {
    //*(chaine) = 32; // ecrit un espace devant le nombre
    for (Mil=0;Mil<Precision;Mil++)   // inscription de '0' dans toute la chaine
          *(chaine+Mil) = 48;
    }

} // fin de la fonction de conversion INT => CHAR


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////// v�rification du CRC des trames recue ///////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void CRC_verif(void){
iii=0;
for (iii=0;iii<255;iii++){

if (TRAMEIN[iii]==0x03){
   CRC1=CRC16(TRAMEIN, iii+1) ;
   IntToChar(CRC1,tableau,5); 
  tableau1[0]= TRAMEIN[iii+1];
  tableau1[1]= TRAMEIN[iii+2];
  tableau1[2]= TRAMEIN[iii+3];
  tableau1[3]= TRAMEIN[iii+4];
  tableau1[4]= TRAMEIN[iii+5];
  if ((tableau1[0]!=tableau[0]) && (tableau1[1]!=tableau[1]) && (tableau1[2]!=tableau[2]) && (tableau1[3]!=tableau[3]) && (tableau1[4]!=tableau[4]));
iii=0;

}
}
}
  

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////// recup�ration des donn�es dans les eeproms ///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void load_data_eeprom (void)
{

int f=0;

}
 

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////// 1 ERE PAGE DU MINITEL///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void page (char num)
{

int j;
if (num == 'I')
{
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////page mot de passe///////////////////////////////////////////////////////////

TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='4';
TRAMEOUT[3]='a';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x39;
TRAMEOUT[6]=0x7F;
TRAMEOUT[7]=0x1B;
TRAMEOUT[8]=0x3A;
TRAMEOUT[9]=0x32;
TRAMEOUT[10]=0x7D;
TRAMEOUT[11]=0x1B;
TRAMEOUT[12]=0x5B;
TRAMEOUT[13]=0x32;
TRAMEOUT[14]=0x4A;
TRAMEOUT[15]=0x1B;
TRAMEOUT[16]=0x5B;
TRAMEOUT[17]=0x30;
TRAMEOUT[18]=0x6D;
TRAMEOUT[19]=0x1B;
TRAMEOUT[20]=0x5B;
TRAMEOUT[21]=0x35;
TRAMEOUT[22]=0x3B;
TRAMEOUT[23]=0x33;
TRAMEOUT[24]=0x30;
TRAMEOUT[25]=0x48;
TRAMEOUT[26]=0x54;
TRAMEOUT[27]=0x79;
TRAMEOUT[28]=0x70;
TRAMEOUT[29]=0x65;
TRAMEOUT[30]=0x20;
TRAMEOUT[31]=0x43;
TRAMEOUT[32]=0x6F;
TRAMEOUT[33]=0x6E;
TRAMEOUT[34]=0x6E;
TRAMEOUT[35]=0x65;
TRAMEOUT[36]=0x78;
TRAMEOUT[37]=0x69;
TRAMEOUT[38]=0x6F;
TRAMEOUT[39]=0x6E;
TRAMEOUT[40]=0x20;
TRAMEOUT[41]=0x3A;
TRAMEOUT[42]=0x1B;
TRAMEOUT[43]=0x5B;
TRAMEOUT[44]=0x30;
TRAMEOUT[45]=0x6D;
TRAMEOUT[46]=0x1B;
TRAMEOUT[47]=0x5B;
TRAMEOUT[48]=0x37;
TRAMEOUT[49]=0x3B;
TRAMEOUT[50]=0x33;
TRAMEOUT[51]=0x35;
TRAMEOUT[52]=0x48;
TRAMEOUT[53]=0x1B;
TRAMEOUT[54]=0x5B;
TRAMEOUT[55]=0x37;
TRAMEOUT[56]=0x6D;
TRAMEOUT[57]=0x20;
TRAMEOUT[58]=0x52;
TRAMEOUT[59]=0x54;
TRAMEOUT[60]=0x43;
TRAMEOUT[61]=0x20;
TRAMEOUT[62]=0x1B;
TRAMEOUT[63]=0x5B;
TRAMEOUT[64]=0x30;
TRAMEOUT[65]=0x6D;
TRAMEOUT[66]=0x1B;
TRAMEOUT[67]=0x5B;
TRAMEOUT[68]=0x31;
TRAMEOUT[69]=0x30;
TRAMEOUT[70]=0x3B;
TRAMEOUT[71]=0x33;
TRAMEOUT[72]=0x34;
TRAMEOUT[73]=0x48;
TRAMEOUT[74]=0x53;
TRAMEOUT[75]=0x69;
TRAMEOUT[76]=0x74;
TRAMEOUT[77]=0x65;
TRAMEOUT[78]=0x3A;
TRAMEOUT[79]=0x20;
TRAMEOUT[80]=0x1B;
TRAMEOUT[81]=0x5B;
TRAMEOUT[82]=0x37;
TRAMEOUT[83]=0x6D;
TRAMEOUT[84]=0x1B;
TRAMEOUT[85]=0x5B;
TRAMEOUT[86]=0x30;///////////////////31
TRAMEOUT[87]=0x33;///////////////////32
TRAMEOUT[88]=0x3B;
TRAMEOUT[89]=0x32;
TRAMEOUT[90]=0x30;
TRAMEOUT[91]=0x48;
TRAMEOUT[92]=0x20;
TRAMEOUT[93]=0x41;  //NOM DU SITE
TRAMEOUT[94]=0x44;
TRAMEOUT[95]=0x45;
TRAMEOUT[96]=0x43;
TRAMEOUT[97]=0x45;
TRAMEOUT[98]=0x46;
TRAMEOUT[99]=0x20;
TRAMEOUT[100]=0x54;
TRAMEOUT[101]=0x45;
TRAMEOUT[102]=0x43;
TRAMEOUT[103]=0x48;
TRAMEOUT[104]=0x4E;
TRAMEOUT[105]=0x4F;
TRAMEOUT[106]=0x4C;
TRAMEOUT[107]=0x4F;
TRAMEOUT[108]=0x47;
TRAMEOUT[109]=0x59;
TRAMEOUT[110]=0x20;
TRAMEOUT[111]=0x1B;
TRAMEOUT[112]=0x5B;
TRAMEOUT[113]=0x37;
TRAMEOUT[114]=0x6D;
TRAMEOUT[115]=0x1B;
TRAMEOUT[116]=0x5B;
TRAMEOUT[117]=0x33;
TRAMEOUT[118]=0x3B;
TRAMEOUT[119]=0x33;
TRAMEOUT[120]=0x38;

TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<121;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}

TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='4';
TRAMEOUT[3]='b';
TRAMEOUT[4]=0x48;
TRAMEOUT[5]=0x20;
TRAMEOUT[6]=0x4E;
TRAMEOUT[7]=0x4F;
TRAMEOUT[8]=0x55;
TRAMEOUT[9]=0x56;
TRAMEOUT[10]=0x45;
TRAMEOUT[11]=0x41;
TRAMEOUT[12]=0x55;
TRAMEOUT[13]=0x20;
TRAMEOUT[14]=0x43;
TRAMEOUT[15]=0x4D;
TRAMEOUT[16]=0x53;
TRAMEOUT[17]=0x20;
TRAMEOUT[18]=0x32;
TRAMEOUT[19]=0x2E;
TRAMEOUT[20]=0x30;
TRAMEOUT[21]=0x20;
TRAMEOUT[22]=0x1B;
TRAMEOUT[23]=0x5B;
TRAMEOUT[24]=0x30;
TRAMEOUT[25]=0x6D;
TRAMEOUT[26]=0x20;
TRAMEOUT[27]=0x1B;
TRAMEOUT[28]=0x5B;
TRAMEOUT[29]=0x31;
TRAMEOUT[30]=0x38;
TRAMEOUT[31]=0x3B;
TRAMEOUT[32]=0x33;
TRAMEOUT[33]=0x30;
TRAMEOUT[34]=0x48;
TRAMEOUT[35]=0x20;
TRAMEOUT[36]=0x4D;
TRAMEOUT[37]=0x6F;
TRAMEOUT[38]=0x74;
TRAMEOUT[39]=0x20;
TRAMEOUT[40]=0x64;
TRAMEOUT[41]=0x65;
TRAMEOUT[42]=0x20;
TRAMEOUT[43]=0x70;
TRAMEOUT[44]=0x61;
TRAMEOUT[45]=0x73;
TRAMEOUT[46]=0x73;
TRAMEOUT[47]=0x65;
TRAMEOUT[48]=0x3A;
TRAMEOUT[49]=0x20;
TRAMEOUT[50]=0x1B;
TRAMEOUT[51]=0x5B;
TRAMEOUT[52]=0x37;
TRAMEOUT[53]=0x6D;
TRAMEOUT[54]=0x1B;
TRAMEOUT[55]=0x5B;
TRAMEOUT[56]=0x32;
TRAMEOUT[57]=0x30;
TRAMEOUT[58]=0x3B;
TRAMEOUT[59]=0x33;
TRAMEOUT[60]=0x34;
TRAMEOUT[61]=0x48;
TRAMEOUT[62]=0x20;
TRAMEOUT[63]=0x20;
TRAMEOUT[64]=0x20;
TRAMEOUT[65]=0x20;
TRAMEOUT[66]=0x20;
TRAMEOUT[67]=0x1B;
TRAMEOUT[68]=0x5B;
TRAMEOUT[69]=0x31;
TRAMEOUT[70]=0x38;
TRAMEOUT[71]=0x3B;
TRAMEOUT[72]=0x35;
TRAMEOUT[73]=0x35;
TRAMEOUT[74]=0x48;
TRAMEOUT[75]=0x1B;
TRAMEOUT[76]=0x5B;
TRAMEOUT[77]=0x30;
TRAMEOUT[78]=0x6D;
TRAMEOUT[79]=0x1B;
TRAMEOUT[80]=0x5B;
TRAMEOUT[81]=0x31;
TRAMEOUT[82]=0x3B;
TRAMEOUT[83]=0x35;
TRAMEOUT[84]=0x39;
TRAMEOUT[85]=0x48;//DATE
TRAMEOUT[86]=0x20; //J
TRAMEOUT[87]=0x20; //J
TRAMEOUT[88]=0x20; // / 2F
TRAMEOUT[89]=0x20; //M
TRAMEOUT[90]=0x20; //M 
TRAMEOUT[91]=0x20; // /
TRAMEOUT[92]=0x20; //2
TRAMEOUT[93]=0x20; //0
TRAMEOUT[94]=0x20; //0
TRAMEOUT[95]=0x20; //0
TRAMEOUT[96]=0x20;
TRAMEOUT[97]=0x20;
TRAMEOUT[98]=0x20;  //H
TRAMEOUT[99]=0x20; //H
TRAMEOUT[100]=0x20; // :
TRAMEOUT[101]=0x20; //m
TRAMEOUT[102]=0x20; //m
TRAMEOUT[103]=0x1B;
TRAMEOUT[104]=0x5B;
TRAMEOUT[105]=0x32;
TRAMEOUT[106]=0x30;
TRAMEOUT[107]=0x3B;
TRAMEOUT[108]=0x33;
TRAMEOUT[109]=0x34;
TRAMEOUT[110]=0x48;
TRAMEOUT[111]='*';

 

j=0;
for (iii=4;iii<112;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}

TRAMEENVOIRS232DONNEES (107);


}
else if (num == '1')
{
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////page 1 du minitel actuel ///////////////////////////////////////////////////////
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='1';
TRAMEOUT[3]='a';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x32;
TRAMEOUT[7]=0x4A;
TRAMEOUT[8]=0x1B;
TRAMEOUT[9]=0x5B;
TRAMEOUT[10]=0x31;
TRAMEOUT[11]=0x3B;
TRAMEOUT[12]=0x33;
TRAMEOUT[13]=0x35;
TRAMEOUT[14]=0x48;
TRAMEOUT[15]=0x53;
TRAMEOUT[16]=0x69;
TRAMEOUT[17]=0x74;
TRAMEOUT[18]=0x65;
TRAMEOUT[19]=0x3A;
TRAMEOUT[20]=0x20;
TRAMEOUT[21]=0x1B;
TRAMEOUT[22]=0x5B;
TRAMEOUT[23]=0x32;
TRAMEOUT[24]=0x33;
TRAMEOUT[25]=0x3B;
TRAMEOUT[26]=0x35;
TRAMEOUT[27]=0x48;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]=0x55;
TRAMEOUT[30]=0x74;
TRAMEOUT[31]=0x69;
TRAMEOUT[32]=0x6C;
TRAMEOUT[33]=0x69;
TRAMEOUT[34]=0x73;
TRAMEOUT[35]=0x65;
TRAMEOUT[36]=0x7A;
TRAMEOUT[37]=0x20;
TRAMEOUT[38]=0x6C;
TRAMEOUT[39]=0x65;
TRAMEOUT[40]=0x73;
TRAMEOUT[41]=0x20;
TRAMEOUT[42]=0x1B;
TRAMEOUT[43]=0x5B;
TRAMEOUT[44]=0x37;
TRAMEOUT[45]=0x6D;
TRAMEOUT[46]=0x46;
TRAMEOUT[47]=0x6C;
TRAMEOUT[48]=0x65;
TRAMEOUT[49]=0x63;
TRAMEOUT[50]=0x68;
TRAMEOUT[51]=0x65;
TRAMEOUT[52]=0x73;
TRAMEOUT[53]=0x20;
TRAMEOUT[54]=0x64;
TRAMEOUT[55]=0x65;
TRAMEOUT[56]=0x20;
TRAMEOUT[57]=0x44;
TRAMEOUT[58]=0x69;
TRAMEOUT[59]=0x72;
TRAMEOUT[60]=0x65;
TRAMEOUT[61]=0x63;
TRAMEOUT[62]=0x74;
TRAMEOUT[63]=0x69;
TRAMEOUT[64]=0x6F;
TRAMEOUT[65]=0x6E;
TRAMEOUT[66]=0x73;
TRAMEOUT[67]=0x1B;
TRAMEOUT[68]=0x5B;
TRAMEOUT[69]=0x30;
TRAMEOUT[70]=0x6D;
TRAMEOUT[71]=0x20;
TRAMEOUT[72]=0x70;
TRAMEOUT[73]=0x6F;
TRAMEOUT[74]=0x75;
TRAMEOUT[75]=0x72;
TRAMEOUT[76]=0x20;
TRAMEOUT[77]=0x63;
TRAMEOUT[78]=0x68;
TRAMEOUT[79]=0x61;
TRAMEOUT[80]=0x6E;
TRAMEOUT[81]=0x67;
TRAMEOUT[82]=0x65;
TRAMEOUT[83]=0x72;
TRAMEOUT[84]=0x20;
TRAMEOUT[85]=0x64;
TRAMEOUT[86]=0x27;
TRAMEOUT[87]=0x65;
TRAMEOUT[88]=0x63;
TRAMEOUT[89]=0x72;
TRAMEOUT[90]=0x61;
TRAMEOUT[91]=0x6E;
TRAMEOUT[92]=0x2C;
TRAMEOUT[93]=0x20;
TRAMEOUT[94]=0x1B;
TRAMEOUT[95]=0x5B;
TRAMEOUT[96]=0x37;
TRAMEOUT[97]=0x6D;
TRAMEOUT[98]=0x45;
TRAMEOUT[99]=0x53;
TRAMEOUT[100]=0x43;
TRAMEOUT[101]=0x1B;
TRAMEOUT[102]=0x5B;
TRAMEOUT[103]=0x30;
TRAMEOUT[104]=0x6D;
TRAMEOUT[105]=0x3A;
TRAMEOUT[106]=0x20;
TRAMEOUT[107]=0x53;
TRAMEOUT[108]=0x6F;
TRAMEOUT[109]=0x72;
TRAMEOUT[110]=0x74;
TRAMEOUT[111]=0x69;
TRAMEOUT[112]=0x65;
TRAMEOUT[113]=0x1B;
TRAMEOUT[114]='*';
j=0;
for (iii=4;iii<114;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (110);
}
else if (num == '2')
{
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////page 2 du minitel actuel ///////////////////////////////////////////////////////
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='2';
TRAMEOUT[3]='a';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x32;
TRAMEOUT[7]=0x4A;
TRAMEOUT[8]=0x1B;
TRAMEOUT[9]=0x5B;
TRAMEOUT[10]=0x31;
TRAMEOUT[11]=0x3B;
TRAMEOUT[12]=0x33;
TRAMEOUT[13]=0x38;
TRAMEOUT[14]=0x48;
TRAMEOUT[15]=0x53;
TRAMEOUT[16]=0x69;
TRAMEOUT[17]=0x74;
TRAMEOUT[18]=0x65;
TRAMEOUT[19]=0x3A;
TRAMEOUT[20]=0x20;
TRAMEOUT[21]=0x20;
TRAMEOUT[22]=0x20;
TRAMEOUT[23]=0x20;
TRAMEOUT[24]=0x1B;
TRAMEOUT[25]=0x5B;
TRAMEOUT[26]=0x31;
TRAMEOUT[27]=0x3B;
TRAMEOUT[28]=0x32;
TRAMEOUT[29]=0x48;
TRAMEOUT[30]=0x0A;
TRAMEOUT[31]=0x0A;
TRAMEOUT[32]=0x4C;
TRAMEOUT[33]=0x69;
TRAMEOUT[34]=0x73;
TRAMEOUT[35]=0x74;
TRAMEOUT[36]=0x65;
TRAMEOUT[37]=0x20;
TRAMEOUT[38]=0x64;
TRAMEOUT[39]=0x65;
TRAMEOUT[40]=0x73;
TRAMEOUT[41]=0x20;
TRAMEOUT[42]=0x61;
TRAMEOUT[43]=0x6C;
TRAMEOUT[44]=0x61;
TRAMEOUT[45]=0x72;
TRAMEOUT[46]=0x6D;
TRAMEOUT[47]=0x65;
TRAMEOUT[48]=0x73;
TRAMEOUT[49]=0x20;
TRAMEOUT[50]=0x65;
TRAMEOUT[51]=0x6E;
TRAMEOUT[52]=0x20;
TRAMEOUT[53]=0x63;
TRAMEOUT[54]=0x6F;
TRAMEOUT[55]=0x75;
TRAMEOUT[56]=0x72;
TRAMEOUT[57]=0x73;
TRAMEOUT[58]=0x3A;
TRAMEOUT[59]=0x1B;
TRAMEOUT[60]=0x5B;
TRAMEOUT[61]=0x30;
TRAMEOUT[62]=0x4B;
TRAMEOUT[63]=0x1B;
TRAMEOUT[64]=0x5B;
TRAMEOUT[65]=0x33;
TRAMEOUT[66]=0x3B;
TRAMEOUT[67]=0x32;
TRAMEOUT[68]=0x39;
TRAMEOUT[69]=0x48;
TRAMEOUT[70]=0x1B;
TRAMEOUT[71]=0x5B;
TRAMEOUT[72]=0x31;
TRAMEOUT[73]=0x3B;
TRAMEOUT[74]=0x32;
TRAMEOUT[75]=0x48;
TRAMEOUT[76]=0x0A;
TRAMEOUT[77]=0x0A;
TRAMEOUT[78]=0x0A;
TRAMEOUT[79]=0x0A;
TRAMEOUT[80]=0x2D;
TRAMEOUT[81]=0x2D;
TRAMEOUT[82]=0x2D;
TRAMEOUT[83]=0x2D;
TRAMEOUT[84]=0x2D;
TRAMEOUT[85]=0x2D;
TRAMEOUT[86]=0x2D;
TRAMEOUT[87]=0x2D;
TRAMEOUT[88]=0x2D;
TRAMEOUT[89]=0x2D;
TRAMEOUT[90]=0x2D;
TRAMEOUT[91]=0x2D;
TRAMEOUT[92]=0x2D;
TRAMEOUT[93]=0x2D;
TRAMEOUT[94]=0x2D;
TRAMEOUT[95]=0x2D;
TRAMEOUT[96]=0x2D;
TRAMEOUT[97]=0x2D;
TRAMEOUT[98]=0x2D;
TRAMEOUT[99]=0x2D;
TRAMEOUT[100]=0x2D;
TRAMEOUT[101]=0x2D;
TRAMEOUT[102]=0x2D;
TRAMEOUT[103]=0x2D;
TRAMEOUT[104]=0x2D;
TRAMEOUT[105]=0x2D;
TRAMEOUT[106]=0x2D;
TRAMEOUT[107]=0x2D;
TRAMEOUT[108]=0x2D;
TRAMEOUT[109]=0x2D;
TRAMEOUT[110]=0x2D;
TRAMEOUT[111]=0x2D;
TRAMEOUT[112]=0x2D;
TRAMEOUT[113]=0x2D;
TRAMEOUT[114]=0x2D;
TRAMEOUT[115]=0x2D;
TRAMEOUT[116]=0x2D;
TRAMEOUT[117]=0x2D;
TRAMEOUT[118]=0x2D;
TRAMEOUT[119]=0x2D;
TRAMEOUT[120]=0x2D;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='2';
TRAMEOUT[3]='b';
TRAMEOUT[4]=0x2D;
TRAMEOUT[5]=0x2D;
TRAMEOUT[6]=0x2D;
TRAMEOUT[7]=0x2D;
TRAMEOUT[8]=0x2D;
TRAMEOUT[9]=0x2D;
TRAMEOUT[10]=0x2D;
TRAMEOUT[11]=0x2D;
TRAMEOUT[12]=0x2D;
TRAMEOUT[13]=0x2D;
TRAMEOUT[14]=0x2D;
TRAMEOUT[15]=0x2D;
TRAMEOUT[16]=0x2D;
TRAMEOUT[17]=0x2D;
TRAMEOUT[18]=0x2D;
TRAMEOUT[19]=0x2D;
TRAMEOUT[20]=0x2D;
TRAMEOUT[21]=0x2D;
TRAMEOUT[22]=0x2D;
TRAMEOUT[23]=0x2D;
TRAMEOUT[24]=0x2D;
TRAMEOUT[25]=0x2D;
TRAMEOUT[26]=0x2D;
TRAMEOUT[27]=0x2D;
TRAMEOUT[28]=0x2D;
TRAMEOUT[29]=0x2D;
TRAMEOUT[30]=0x2D;
TRAMEOUT[31]=0x2D;
TRAMEOUT[32]=0x1B;
TRAMEOUT[33]=0x5B;
TRAMEOUT[34]=0x36;
TRAMEOUT[35]=0x3B;
TRAMEOUT[36]=0x32;
TRAMEOUT[37]=0x48;
TRAMEOUT[38]=0x21;
TRAMEOUT[39]=0x20;
TRAMEOUT[40]=0x54;
TRAMEOUT[41]=0x59;
TRAMEOUT[42]=0x50;
TRAMEOUT[43]=0x45;
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x44;
TRAMEOUT[46]=0x27;
TRAMEOUT[47]=0x41;
TRAMEOUT[48]=0x4C;
TRAMEOUT[49]=0x41;
TRAMEOUT[50]=0x52;
TRAMEOUT[51]=0x4D;
TRAMEOUT[52]=0x45;
TRAMEOUT[53]=0x20;
TRAMEOUT[54]=0x20;
TRAMEOUT[55]=0x20;
TRAMEOUT[56]=0x20;
TRAMEOUT[57]=0x20;
TRAMEOUT[58]=0x21;
TRAMEOUT[59]=0x20;
TRAMEOUT[60]=0x50;
TRAMEOUT[61]=0x2E;
TRAMEOUT[62]=0x47;
TRAMEOUT[63]=0x72;
TRAMEOUT[64]=0x6F;
TRAMEOUT[65]=0x75;
TRAMEOUT[66]=0x70;
TRAMEOUT[67]=0x65;
TRAMEOUT[68]=0x20;
TRAMEOUT[69]=0x21;
TRAMEOUT[70]=0x20;
TRAMEOUT[71]=0x54;
TRAMEOUT[72]=0x50;
TRAMEOUT[73]=0x2E;
TRAMEOUT[74]=0x41;
TRAMEOUT[75]=0x44;
TRAMEOUT[76]=0x52;
TRAMEOUT[77]=0x45;
TRAMEOUT[78]=0x53;
TRAMEOUT[79]=0x53;
TRAMEOUT[80]=0x41;
TRAMEOUT[81]=0x42;
TRAMEOUT[82]=0x4C;
TRAMEOUT[83]=0x45;
TRAMEOUT[84]=0x20;
TRAMEOUT[85]=0x21;
TRAMEOUT[86]=0x20;
TRAMEOUT[87]=0x54;
TRAMEOUT[88]=0x50;
TRAMEOUT[89]=0x2E;
TRAMEOUT[90]=0x52;
TRAMEOUT[91]=0x45;
TRAMEOUT[92]=0x53;
TRAMEOUT[93]=0x49;
TRAMEOUT[94]=0x53;
TRAMEOUT[95]=0x54;
TRAMEOUT[96]=0x49;
TRAMEOUT[97]=0x46;
TRAMEOUT[98]=0x20;
TRAMEOUT[99]=0x21;
TRAMEOUT[100]=0x20;
TRAMEOUT[101]=0x44;
TRAMEOUT[102]=0x45;
TRAMEOUT[103]=0x42;
TRAMEOUT[104]=0x49;
TRAMEOUT[105]=0x54;
TRAMEOUT[106]=0x21;
TRAMEOUT[107]=0x20;
TRAMEOUT[108]=0x1B;
TRAMEOUT[109]=0x5B;
TRAMEOUT[110]=0x37;
TRAMEOUT[111]=0x3B;
TRAMEOUT[112]=0x32;
TRAMEOUT[113]=0x48;
TRAMEOUT[114]=0x2D;
TRAMEOUT[115]=0x2D;
TRAMEOUT[116]=0x2D;
TRAMEOUT[117]=0x2D;
TRAMEOUT[118]=0x2D;
TRAMEOUT[119]=0x2D;
TRAMEOUT[120]=0x2D;
TRAMEOUT[121]='*';



j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='2';
TRAMEOUT[3]='c';
TRAMEOUT[4]=0x2D;
TRAMEOUT[5]=0x2D;
TRAMEOUT[6]=0x2D;
TRAMEOUT[7]=0x2D;
TRAMEOUT[8]=0x2D;
TRAMEOUT[9]=0x2D;
TRAMEOUT[10]=0x2D;
TRAMEOUT[11]=0x2D;
TRAMEOUT[12]=0x2D;
TRAMEOUT[13]=0x2D;
TRAMEOUT[14]=0x2D;
TRAMEOUT[15]=0x2D;
TRAMEOUT[16]=0x2D;
TRAMEOUT[17]=0x2D;
TRAMEOUT[18]=0x2D;
TRAMEOUT[19]=0x2D;
TRAMEOUT[20]=0x2D;
TRAMEOUT[21]=0x2D;
TRAMEOUT[22]=0x2D;
TRAMEOUT[23]=0x2D;
TRAMEOUT[24]=0x2D;
TRAMEOUT[25]=0x2D;
TRAMEOUT[26]=0x2D;
TRAMEOUT[27]=0x2D;
TRAMEOUT[28]=0x2D;
TRAMEOUT[29]=0x2D;
TRAMEOUT[30]=0x2D;
TRAMEOUT[31]=0x2D;
TRAMEOUT[32]=0x2D;
TRAMEOUT[33]=0x2D;
TRAMEOUT[34]=0x2D;
TRAMEOUT[35]=0x2D;
TRAMEOUT[36]=0x2D;
TRAMEOUT[37]=0x2D;
TRAMEOUT[38]=0x2D;
TRAMEOUT[39]=0x2D;
TRAMEOUT[40]=0x2D;
TRAMEOUT[41]=0x2D;
TRAMEOUT[42]=0x2D;
TRAMEOUT[43]=0x2D;
TRAMEOUT[44]=0x2D;
TRAMEOUT[45]=0x2D;
TRAMEOUT[46]=0x2D;
TRAMEOUT[47]=0x2D;
TRAMEOUT[48]=0x2D;
TRAMEOUT[49]=0x2D;
TRAMEOUT[50]=0x2D;
TRAMEOUT[51]=0x2D;
TRAMEOUT[52]=0x2D;
TRAMEOUT[53]=0x2D;
TRAMEOUT[54]=0x2D;
TRAMEOUT[55]=0x2D;
TRAMEOUT[56]=0x2D;
TRAMEOUT[57]=0x2D;
TRAMEOUT[58]=0x2D;
TRAMEOUT[59]=0x2D;
TRAMEOUT[60]=0x2D;
TRAMEOUT[61]=0x2D;
TRAMEOUT[62]=0x2D;
TRAMEOUT[63]=0x2D;
TRAMEOUT[64]=0x2D;
TRAMEOUT[65]=0x2D;
TRAMEOUT[66]=0x1B;
TRAMEOUT[67]=0x5B;
TRAMEOUT[68]=0x30;
TRAMEOUT[69]=0x38;
TRAMEOUT[70]=0x3B;
TRAMEOUT[71]=0x32;
TRAMEOUT[72]=0x48;
TRAMEOUT[73]=0x21;
TRAMEOUT[74]=0x20;
TRAMEOUT[75]=0x41;
TRAMEOUT[76]=0x4C;
TRAMEOUT[77]=0x41;
TRAMEOUT[78]=0x52;
TRAMEOUT[79]=0x4D;
TRAMEOUT[80]=0x45;
TRAMEOUT[81]=0x20;
TRAMEOUT[82]=0x45;
TRAMEOUT[83]=0x4E;
TRAMEOUT[84]=0x20;
TRAMEOUT[85]=0x43;
TRAMEOUT[86]=0x4F;
TRAMEOUT[87]=0x55;
TRAMEOUT[88]=0x52;
TRAMEOUT[89]=0x53;
TRAMEOUT[90]=0x20;
TRAMEOUT[91]=0x20;
TRAMEOUT[92]=0x20;
TRAMEOUT[93]=0x21;
TRAMEOUT[94]=0x1B;
TRAMEOUT[95]=0x5B;
TRAMEOUT[96]=0x30;
TRAMEOUT[97]=0x38;
TRAMEOUT[98]=0x3B;
TRAMEOUT[99]=0x33;
TRAMEOUT[100]=0x33;
TRAMEOUT[101]=0x48;
TRAMEOUT[102]=0x21;
TRAMEOUT[103]=0x1B;
TRAMEOUT[104]=0x5B;
TRAMEOUT[105]=0x30;
TRAMEOUT[106]=0x38;
TRAMEOUT[107]=0x3B;
TRAMEOUT[108]=0x34;
TRAMEOUT[109]=0x39;
TRAMEOUT[110]=0x48;
TRAMEOUT[111]=0x21;
TRAMEOUT[112]=0x1B;
TRAMEOUT[113]=0x5B;
TRAMEOUT[114]=0x30;
TRAMEOUT[115]=0x38;
TRAMEOUT[116]=0x3B;
TRAMEOUT[117]=0x36;
TRAMEOUT[118]=0x33;
TRAMEOUT[119]=0x48;
TRAMEOUT[120]=0x21;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='2';
TRAMEOUT[3]='d';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x30;
TRAMEOUT[7]=0x38;
TRAMEOUT[8]=0x3B;
TRAMEOUT[9]=0x37;
TRAMEOUT[10]=0x30;
TRAMEOUT[11]=0x48;
TRAMEOUT[12]=0x21;
TRAMEOUT[13]=0x1B;
TRAMEOUT[14]=0x5B;
TRAMEOUT[15]=0x39;
TRAMEOUT[16]=0x3B;
TRAMEOUT[17]=0x32;
TRAMEOUT[18]=0x48;
TRAMEOUT[19]=0x2D;
TRAMEOUT[20]=0x2D;
TRAMEOUT[21]=0x2D;
TRAMEOUT[22]=0x2D;
TRAMEOUT[23]=0x2D;
TRAMEOUT[24]=0x2D;
TRAMEOUT[25]=0x2D;
TRAMEOUT[26]=0x2D;
TRAMEOUT[27]=0x2D;
TRAMEOUT[28]=0x2D;
TRAMEOUT[29]=0x2D;
TRAMEOUT[30]=0x2D;
TRAMEOUT[31]=0x2D;
TRAMEOUT[32]=0x2D;
TRAMEOUT[33]=0x2D;
TRAMEOUT[34]=0x2D;
TRAMEOUT[35]=0x2D;
TRAMEOUT[36]=0x2D;
TRAMEOUT[37]=0x2D;
TRAMEOUT[38]=0x2D;
TRAMEOUT[39]=0x2D;
TRAMEOUT[40]=0x2D;
TRAMEOUT[41]=0x2D;
TRAMEOUT[42]=0x2D;
TRAMEOUT[43]=0x2D;
TRAMEOUT[44]=0x2D;
TRAMEOUT[45]=0x2D;
TRAMEOUT[46]=0x2D;
TRAMEOUT[47]=0x2D;
TRAMEOUT[48]=0x2D;
TRAMEOUT[49]=0x2D;
TRAMEOUT[50]=0x2D;
TRAMEOUT[51]=0x2D;
TRAMEOUT[52]=0x2D;
TRAMEOUT[53]=0x2D;
TRAMEOUT[54]=0x2D;
TRAMEOUT[55]=0x2D;
TRAMEOUT[56]=0x2D;
TRAMEOUT[57]=0x2D;
TRAMEOUT[58]=0x2D;
TRAMEOUT[59]=0x2D;
TRAMEOUT[60]=0x2D;
TRAMEOUT[61]=0x2D;
TRAMEOUT[62]=0x2D;
TRAMEOUT[63]=0x2D;
TRAMEOUT[64]=0x2D;
TRAMEOUT[65]=0x2D;
TRAMEOUT[66]=0x2D;
TRAMEOUT[67]=0x2D;
TRAMEOUT[68]=0x2D;
TRAMEOUT[69]=0x2D;
TRAMEOUT[70]=0x2D;
TRAMEOUT[71]=0x2D;
TRAMEOUT[72]=0x2D;
TRAMEOUT[73]=0x2D;
TRAMEOUT[74]=0x2D;
TRAMEOUT[75]=0x2D;
TRAMEOUT[76]=0x2D;
TRAMEOUT[77]=0x2D;
TRAMEOUT[78]=0x2D;
TRAMEOUT[79]=0x2D;
TRAMEOUT[80]=0x2D;
TRAMEOUT[81]=0x2D;
TRAMEOUT[82]=0x2D;
TRAMEOUT[83]=0x2D;
TRAMEOUT[84]=0x2D;
TRAMEOUT[85]=0x2D;
TRAMEOUT[86]=0x2D;
TRAMEOUT[87]=0x2D;
TRAMEOUT[88]=0x1B;
TRAMEOUT[89]=0x5B;
TRAMEOUT[90]=0x31;
TRAMEOUT[91]=0x30;
TRAMEOUT[92]=0x3B;
TRAMEOUT[93]=0x32;
TRAMEOUT[94]=0x48;
TRAMEOUT[95]=0x21;
TRAMEOUT[96]=0x20;
TRAMEOUT[97]=0x48;
TRAMEOUT[98]=0x2F;
TRAMEOUT[99]=0x47;
TRAMEOUT[100]=0x3A;
TRAMEOUT[101]=0x20;
TRAMEOUT[102]=0x48;
TRAMEOUT[103]=0x4F;
TRAMEOUT[104]=0x52;
TRAMEOUT[105]=0x53;
TRAMEOUT[106]=0x20;
TRAMEOUT[107]=0x47;
TRAMEOUT[108]=0x41;
TRAMEOUT[109]=0x4D;
TRAMEOUT[110]=0x4D;
TRAMEOUT[111]=0x45;
TRAMEOUT[112]=0x20;
TRAMEOUT[113]=0x20;
TRAMEOUT[114]=0x20;
TRAMEOUT[115]=0x21;
TRAMEOUT[116]=0x1B;
TRAMEOUT[117]=0x5B;
TRAMEOUT[118]=0x31;
TRAMEOUT[119]=0x30;
TRAMEOUT[120]=0x3B;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='2';
TRAMEOUT[3]='e';
TRAMEOUT[4]=0x33;
TRAMEOUT[5]=0x33;
TRAMEOUT[6]=0x48;
TRAMEOUT[7]=0x21;
TRAMEOUT[8]=0x1B;
TRAMEOUT[9]=0x5B;
TRAMEOUT[10]=0x31;
TRAMEOUT[11]=0x30;
TRAMEOUT[12]=0x3B;
TRAMEOUT[13]=0x34;
TRAMEOUT[14]=0x39;
TRAMEOUT[15]=0x48;
TRAMEOUT[16]=0x21;
TRAMEOUT[17]=0x1B;
TRAMEOUT[18]=0x5B;
TRAMEOUT[19]=0x31;
TRAMEOUT[20]=0x30;
TRAMEOUT[21]=0x3B;
TRAMEOUT[22]=0x36;
TRAMEOUT[23]=0x33;
TRAMEOUT[24]=0x48;
TRAMEOUT[25]=0x21;
TRAMEOUT[26]=0x1B;
TRAMEOUT[27]=0x5B;
TRAMEOUT[28]=0x31;
TRAMEOUT[29]=0x30;
TRAMEOUT[30]=0x3B;
TRAMEOUT[31]=0x37;
TRAMEOUT[32]=0x30;
TRAMEOUT[33]=0x48;
TRAMEOUT[34]=0x21;
TRAMEOUT[35]=0x1B;
TRAMEOUT[36]=0x5B;
TRAMEOUT[37]=0x31;
TRAMEOUT[38]=0x31;
TRAMEOUT[39]=0x3B;
TRAMEOUT[40]=0x32;
TRAMEOUT[41]=0x48;
TRAMEOUT[42]=0x2D;
TRAMEOUT[43]=0x2D;
TRAMEOUT[44]=0x2D;
TRAMEOUT[45]=0x2D;
TRAMEOUT[46]=0x2D;
TRAMEOUT[47]=0x2D;
TRAMEOUT[48]=0x2D;
TRAMEOUT[49]=0x2D;
TRAMEOUT[50]=0x2D;
TRAMEOUT[51]=0x2D;
TRAMEOUT[52]=0x2D;
TRAMEOUT[53]=0x2D;
TRAMEOUT[54]=0x2D;
TRAMEOUT[55]=0x2D;
TRAMEOUT[56]=0x2D;
TRAMEOUT[57]=0x2D;
TRAMEOUT[58]=0x2D;
TRAMEOUT[59]=0x2D;
TRAMEOUT[60]=0x2D;
TRAMEOUT[61]=0x2D;
TRAMEOUT[62]=0x2D;
TRAMEOUT[63]=0x2D;
TRAMEOUT[64]=0x2D;
TRAMEOUT[65]=0x2D;
TRAMEOUT[66]=0x2D;
TRAMEOUT[67]=0x2D;
TRAMEOUT[68]=0x2D;
TRAMEOUT[69]=0x2D;
TRAMEOUT[70]=0x2D;
TRAMEOUT[71]=0x2D;
TRAMEOUT[72]=0x2D;
TRAMEOUT[73]=0x2D;
TRAMEOUT[74]=0x2D;
TRAMEOUT[75]=0x2D;
TRAMEOUT[76]=0x2D;
TRAMEOUT[77]=0x2D;
TRAMEOUT[78]=0x2D;
TRAMEOUT[79]=0x2D;
TRAMEOUT[80]=0x2D;
TRAMEOUT[81]=0x2D;
TRAMEOUT[82]=0x2D;
TRAMEOUT[83]=0x2D;
TRAMEOUT[84]=0x2D;
TRAMEOUT[85]=0x2D;
TRAMEOUT[86]=0x2D;
TRAMEOUT[87]=0x2D;
TRAMEOUT[88]=0x2D;
TRAMEOUT[89]=0x2D;
TRAMEOUT[90]=0x2D;
TRAMEOUT[91]=0x2D;
TRAMEOUT[92]=0x2D;
TRAMEOUT[93]=0x2D;
TRAMEOUT[94]=0x2D;
TRAMEOUT[95]=0x2D;
TRAMEOUT[96]=0x2D;
TRAMEOUT[97]=0x2D;
TRAMEOUT[98]=0x2D;
TRAMEOUT[99]=0x2D;
TRAMEOUT[100]=0x2D;
TRAMEOUT[101]=0x2D;
TRAMEOUT[102]=0x2D;
TRAMEOUT[103]=0x2D;
TRAMEOUT[104]=0x2D;
TRAMEOUT[105]=0x2D;
TRAMEOUT[106]=0x2D;
TRAMEOUT[107]=0x2D;
TRAMEOUT[108]=0x2D;
TRAMEOUT[109]=0x2D;
TRAMEOUT[110]=0x2D;
TRAMEOUT[111]=0x1B;
TRAMEOUT[112]=0x5B;
TRAMEOUT[113]=0x31;
TRAMEOUT[114]=0x32;
TRAMEOUT[115]=0x3B;
TRAMEOUT[116]=0x32;
TRAMEOUT[117]=0x48;
TRAMEOUT[118]=0x21;
TRAMEOUT[119]=0x20;
TRAMEOUT[120]=0x4E;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='2';
TRAMEOUT[3]='f';
TRAMEOUT[4]=0x2F;
TRAMEOUT[5]=0x52;
TRAMEOUT[6]=0x3A;
TRAMEOUT[7]=0x20;
TRAMEOUT[8]=0x4E;
TRAMEOUT[9]=0x4F;
TRAMEOUT[10]=0x4E;
TRAMEOUT[11]=0x20;
TRAMEOUT[12]=0x52;
TRAMEOUT[13]=0x45;
TRAMEOUT[14]=0x50;
TRAMEOUT[15]=0x4F;
TRAMEOUT[16]=0x4E;
TRAMEOUT[17]=0x53;
TRAMEOUT[18]=0x45;
TRAMEOUT[19]=0x20;
TRAMEOUT[20]=0x20;
TRAMEOUT[21]=0x21;
TRAMEOUT[22]=0x1B;
TRAMEOUT[23]=0x5B;
TRAMEOUT[24]=0x31;
TRAMEOUT[25]=0x32;
TRAMEOUT[26]=0x3B;
TRAMEOUT[27]=0x33;
TRAMEOUT[28]=0x33;
TRAMEOUT[29]=0x48;
TRAMEOUT[30]=0x21;
TRAMEOUT[31]=0x1B;
TRAMEOUT[32]=0x5B;
TRAMEOUT[33]=0x31;
TRAMEOUT[34]=0x32;
TRAMEOUT[35]=0x3B;
TRAMEOUT[36]=0x34;
TRAMEOUT[37]=0x39;
TRAMEOUT[38]=0x48;
TRAMEOUT[39]=0x21;
TRAMEOUT[40]=0x1B;
TRAMEOUT[41]=0x5B;
TRAMEOUT[42]=0x31;
TRAMEOUT[43]=0x32;
TRAMEOUT[44]=0x3B;
TRAMEOUT[45]=0x36;
TRAMEOUT[46]=0x33;
TRAMEOUT[47]=0x48;
TRAMEOUT[48]=0x21;
TRAMEOUT[49]=0x1B;
TRAMEOUT[50]=0x5B;
TRAMEOUT[51]=0x31;
TRAMEOUT[52]=0x32;
TRAMEOUT[53]=0x3B;
TRAMEOUT[54]=0x37;
TRAMEOUT[55]=0x30;
TRAMEOUT[56]=0x48;
TRAMEOUT[57]=0x21;
TRAMEOUT[58]=0x1B;
TRAMEOUT[59]=0x5B;
TRAMEOUT[60]=0x31;
TRAMEOUT[61]=0x33;
TRAMEOUT[62]=0x3B;
TRAMEOUT[63]=0x32;
TRAMEOUT[64]=0x48;
TRAMEOUT[65]=0x2D;
TRAMEOUT[66]=0x2D;
TRAMEOUT[67]=0x2D;
TRAMEOUT[68]=0x2D;
TRAMEOUT[69]=0x2D;
TRAMEOUT[70]=0x2D;
TRAMEOUT[71]=0x2D;
TRAMEOUT[72]=0x2D;
TRAMEOUT[73]=0x2D;
TRAMEOUT[74]=0x2D;
TRAMEOUT[75]=0x2D;
TRAMEOUT[76]=0x2D;
TRAMEOUT[77]=0x2D;
TRAMEOUT[78]=0x2D;
TRAMEOUT[79]=0x2D;
TRAMEOUT[80]=0x2D;
TRAMEOUT[81]=0x2D;
TRAMEOUT[82]=0x2D;
TRAMEOUT[83]=0x2D;
TRAMEOUT[84]=0x2D;
TRAMEOUT[85]=0x2D;
TRAMEOUT[86]=0x2D;
TRAMEOUT[87]=0x2D;
TRAMEOUT[88]=0x2D;
TRAMEOUT[89]=0x2D;
TRAMEOUT[90]=0x2D;
TRAMEOUT[91]=0x2D;
TRAMEOUT[92]=0x2D;
TRAMEOUT[93]=0x2D;
TRAMEOUT[94]=0x2D;
TRAMEOUT[95]=0x2D;
TRAMEOUT[96]=0x2D;
TRAMEOUT[97]=0x2D;
TRAMEOUT[98]=0x2D;
TRAMEOUT[99]=0x2D;
TRAMEOUT[100]=0x2D;
TRAMEOUT[101]=0x2D;
TRAMEOUT[102]=0x2D;
TRAMEOUT[103]=0x2D;
TRAMEOUT[104]=0x2D;
TRAMEOUT[105]=0x2D;
TRAMEOUT[106]=0x2D;
TRAMEOUT[107]=0x2D;
TRAMEOUT[108]=0x2D;
TRAMEOUT[109]=0x2D;
TRAMEOUT[110]=0x2D;
TRAMEOUT[111]=0x2D;
TRAMEOUT[112]=0x2D;
TRAMEOUT[113]=0x2D;
TRAMEOUT[114]=0x2D;
TRAMEOUT[115]=0x2D;
TRAMEOUT[116]=0x2D;
TRAMEOUT[117]=0x2D;
TRAMEOUT[118]=0x2D;
TRAMEOUT[119]=0x2D;
TRAMEOUT[120]=0x2D;
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[121]='*';
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='2';
TRAMEOUT[3]='g';
TRAMEOUT[4]=0x2D;
TRAMEOUT[5]=0x2D;
TRAMEOUT[6]=0x2D;
TRAMEOUT[7]=0x2D;
TRAMEOUT[8]=0x2D;
TRAMEOUT[9]=0x2D;
TRAMEOUT[10]=0x2D;
TRAMEOUT[11]=0x2D;
TRAMEOUT[12]=0x2D;
TRAMEOUT[13]=0x2D;
TRAMEOUT[14]=0x2D;
TRAMEOUT[15]=0x2D;
TRAMEOUT[16]=0x2D;
TRAMEOUT[17]=0x1B;
TRAMEOUT[18]=0x5B;
TRAMEOUT[19]=0x31;
TRAMEOUT[20]=0x34;
TRAMEOUT[21]=0x3B;
TRAMEOUT[22]=0x32;
TRAMEOUT[23]=0x48;
TRAMEOUT[24]=0x21;
TRAMEOUT[25]=0x20;
TRAMEOUT[26]=0x49;
TRAMEOUT[27]=0x4E;
TRAMEOUT[28]=0x54;
TRAMEOUT[29]=0x3A;
TRAMEOUT[30]=0x20;
TRAMEOUT[31]=0x49;
TRAMEOUT[32]=0x4E;
TRAMEOUT[33]=0x54;
TRAMEOUT[34]=0x45;
TRAMEOUT[35]=0x52;
TRAMEOUT[36]=0x56;
TRAMEOUT[37]=0x45;
TRAMEOUT[38]=0x4E;
TRAMEOUT[39]=0x54;
TRAMEOUT[40]=0x49;
TRAMEOUT[41]=0x4F;
TRAMEOUT[42]=0x4E;
TRAMEOUT[43]=0x20;
TRAMEOUT[44]=0x21;
TRAMEOUT[45]=0x1B;
TRAMEOUT[46]=0x5B;
TRAMEOUT[47]=0x31;
TRAMEOUT[48]=0x34;
TRAMEOUT[49]=0x3B;
TRAMEOUT[50]=0x33;
TRAMEOUT[51]=0x33;
TRAMEOUT[52]=0x48;
TRAMEOUT[53]=0x21;
TRAMEOUT[54]=0x1B;
TRAMEOUT[55]=0x5B;
TRAMEOUT[56]=0x31;
TRAMEOUT[57]=0x34;
TRAMEOUT[58]=0x3B;
TRAMEOUT[59]=0x34;
TRAMEOUT[60]=0x39;
TRAMEOUT[61]=0x48;
TRAMEOUT[62]=0x21;
TRAMEOUT[63]=0x1B;
TRAMEOUT[64]=0x5B;
TRAMEOUT[65]=0x31;
TRAMEOUT[66]=0x34;
TRAMEOUT[67]=0x3B;
TRAMEOUT[68]=0x36;
TRAMEOUT[69]=0x33;
TRAMEOUT[70]=0x48;
TRAMEOUT[71]=0x21;
TRAMEOUT[72]=0x1B;
TRAMEOUT[73]=0x5B;
TRAMEOUT[74]=0x31;
TRAMEOUT[75]=0x34;
TRAMEOUT[76]=0x3B;
TRAMEOUT[77]=0x37;
TRAMEOUT[78]=0x30;
TRAMEOUT[79]=0x48;
TRAMEOUT[80]=0x21;
TRAMEOUT[81]=0x1B;
TRAMEOUT[82]=0x5B;
TRAMEOUT[83]=0x31;
TRAMEOUT[84]=0x35;
TRAMEOUT[85]=0x3B;
TRAMEOUT[86]=0x32;
TRAMEOUT[87]=0x48;
TRAMEOUT[88]=0x2D;
TRAMEOUT[89]=0x2D;
TRAMEOUT[90]=0x2D;
TRAMEOUT[91]=0x2D;
TRAMEOUT[92]=0x2D;
TRAMEOUT[93]=0x2D;
TRAMEOUT[94]=0x2D;
TRAMEOUT[95]=0x2D;
TRAMEOUT[96]=0x2D;
TRAMEOUT[97]=0x2D;
TRAMEOUT[98]=0x2D;
TRAMEOUT[99]=0x2D;
TRAMEOUT[100]=0x2D;
TRAMEOUT[101]=0x2D;
TRAMEOUT[102]=0x2D;
TRAMEOUT[103]=0x2D;
TRAMEOUT[104]=0x2D;
TRAMEOUT[105]=0x2D;
TRAMEOUT[106]=0x2D;
TRAMEOUT[107]=0x2D;
TRAMEOUT[108]=0x2D;
TRAMEOUT[109]=0x2D;
TRAMEOUT[110]=0x2D;
TRAMEOUT[111]=0x2D;
TRAMEOUT[112]=0x2D;
TRAMEOUT[113]=0x2D;
TRAMEOUT[114]=0x2D;
TRAMEOUT[115]=0x2D;
TRAMEOUT[116]=0x2D;
TRAMEOUT[117]=0x2D;
TRAMEOUT[118]=0x2D;
TRAMEOUT[119]=0x2D;
TRAMEOUT[120]=0x2D;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='2';
TRAMEOUT[3]='h';
TRAMEOUT[4]=0x2D;
TRAMEOUT[5]=0x2D;
TRAMEOUT[6]=0x2D;
TRAMEOUT[7]=0x2D;
TRAMEOUT[8]=0x2D;
TRAMEOUT[9]=0x2D;
TRAMEOUT[10]=0x2D;
TRAMEOUT[11]=0x2D;
TRAMEOUT[12]=0x2D;
TRAMEOUT[13]=0x2D;
TRAMEOUT[14]=0x2D;
TRAMEOUT[15]=0x2D;
TRAMEOUT[16]=0x2D;
TRAMEOUT[17]=0x2D;
TRAMEOUT[18]=0x2D;
TRAMEOUT[19]=0x2D;
TRAMEOUT[20]=0x2D;
TRAMEOUT[21]=0x2D;
TRAMEOUT[22]=0x2D;
TRAMEOUT[23]=0x2D;
TRAMEOUT[24]=0x2D;
TRAMEOUT[25]=0x2D;
TRAMEOUT[26]=0x2D;
TRAMEOUT[27]=0x2D;
TRAMEOUT[28]=0x2D;
TRAMEOUT[29]=0x2D;
TRAMEOUT[30]=0x2D;
TRAMEOUT[31]=0x2D;
TRAMEOUT[32]=0x2D;
TRAMEOUT[33]=0x2D;
TRAMEOUT[34]=0x2D;
TRAMEOUT[35]=0x2D;
TRAMEOUT[36]=0x2D;
TRAMEOUT[37]=0x2D;
TRAMEOUT[38]=0x2D;
TRAMEOUT[39]=0x2D;
TRAMEOUT[40]='*';

j=0;
for (iii=4;iii<40;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (36);
}
else if (num == '3')
{


TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='5';
TRAMEOUT[3]='a';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x32;
TRAMEOUT[7]=0x4A;
TRAMEOUT[8]=0x1B;
TRAMEOUT[9]=0x5B;
TRAMEOUT[10]=0x31;
TRAMEOUT[11]=0x3B;
TRAMEOUT[12]=0x36;
TRAMEOUT[13]=0x34;
TRAMEOUT[14]=0x48;
TRAMEOUT[15]=0x1B;
TRAMEOUT[16]=0x5B;
TRAMEOUT[17]=0x31;
TRAMEOUT[18]=0x3B;
TRAMEOUT[19]=0x33;
TRAMEOUT[20]=0x38;
TRAMEOUT[21]=0x48;
TRAMEOUT[22]=0x53;
TRAMEOUT[23]=0x69;
TRAMEOUT[24]=0x74;
TRAMEOUT[25]=0x65;
TRAMEOUT[26]=0x3A;
TRAMEOUT[27]=0x20;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]=0x1B;
TRAMEOUT[30]=0x5B;
TRAMEOUT[31]=0x31;
TRAMEOUT[32]=0x3B;
TRAMEOUT[33]=0x36;
TRAMEOUT[34]=0x34;
TRAMEOUT[35]=0x48;
TRAMEOUT[36]=0x20;
TRAMEOUT[37]=0x1B;
TRAMEOUT[38]=0x5B;
TRAMEOUT[39]=0x32;
TRAMEOUT[40]=0x3B;
TRAMEOUT[41]=0x33;
TRAMEOUT[42]=0x30;
TRAMEOUT[43]=0x48;
TRAMEOUT[44]=0x1B;
TRAMEOUT[45]=0x5B;
TRAMEOUT[46]=0x37;
TRAMEOUT[47]=0x6D;
TRAMEOUT[48]=0x20;
TRAMEOUT[49]=0x41; //NOM DU SITE
TRAMEOUT[50]=0x44;
TRAMEOUT[51]=0x45;
TRAMEOUT[52]=0x43;
TRAMEOUT[53]=0x45;
TRAMEOUT[54]=0x46;
TRAMEOUT[55]=0x20;
TRAMEOUT[56]=0x54;
TRAMEOUT[57]=0x45;
TRAMEOUT[58]=0x43;
TRAMEOUT[59]=0x48;
TRAMEOUT[60]=0x4E;
TRAMEOUT[61]=0x4F;
TRAMEOUT[62]=0x4C;
TRAMEOUT[63]=0x4F;
TRAMEOUT[64]=0x47;
TRAMEOUT[65]=0x59;
TRAMEOUT[66]=0x20;
TRAMEOUT[67]=0x1B;
TRAMEOUT[68]=0x5B;
TRAMEOUT[69]=0x30;
TRAMEOUT[70]=0x6D;
TRAMEOUT[71]=0x1B;
TRAMEOUT[72]=0x5B;
TRAMEOUT[73]=0x31;
TRAMEOUT[74]=0x3B;
TRAMEOUT[75]=0x32;
TRAMEOUT[76]=0x48;
TRAMEOUT[77]=0x1B;
TRAMEOUT[78]=0x5B;
TRAMEOUT[79]=0x33;
TRAMEOUT[80]=0x3B;
TRAMEOUT[81]=0x31;
TRAMEOUT[82]=0x36;
TRAMEOUT[83]=0x48;
TRAMEOUT[84]=0x1B;
TRAMEOUT[85]=0x5B;
TRAMEOUT[86]=0x32;
TRAMEOUT[87]=0x3B;
TRAMEOUT[88]=0x32;
TRAMEOUT[89]=0x48;
TRAMEOUT[90]=0x4E;
TRAMEOUT[91]=0x72;
TRAMEOUT[92]=0x2E;
TRAMEOUT[93]=0x20;
TRAMEOUT[94]=0x64;
TRAMEOUT[95]=0x65;
TRAMEOUT[96]=0x20;
TRAMEOUT[97]=0x63;
TRAMEOUT[98]=0x61;
TRAMEOUT[99]=0x62;
TRAMEOUT[100]=0x6C;
TRAMEOUT[101]=0x65;
TRAMEOUT[102]=0x3A;
TRAMEOUT[103]=0x20;
TRAMEOUT[104]=0x1B;
TRAMEOUT[105]=0x5B;
TRAMEOUT[106]=0x31;
TRAMEOUT[107]=0x3B;
TRAMEOUT[108]=0x32;
TRAMEOUT[109]=0x48;
TRAMEOUT[110]=0x1B;
TRAMEOUT[111]=0x5B;
TRAMEOUT[112]=0x33;
TRAMEOUT[113]=0x3B;
TRAMEOUT[114]=0x31;
TRAMEOUT[115]=0x34;
TRAMEOUT[116]=0x48;
TRAMEOUT[117]=0x1B;
TRAMEOUT[118]=0x5B;
TRAMEOUT[119]=0x37;
TRAMEOUT[120]=0x6D;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='5';
TRAMEOUT[3]='b';
TRAMEOUT[4]=0x20;
TRAMEOUT[5]=0x56;
TRAMEOUT[6]=0x49;
TRAMEOUT[7]=0x53;
TRAMEOUT[8]=0x55;
TRAMEOUT[9]=0x41;
TRAMEOUT[10]=0x4C;
TRAMEOUT[11]=0x49;
TRAMEOUT[12]=0x53;
TRAMEOUT[13]=0x41;
TRAMEOUT[14]=0x54;
TRAMEOUT[15]=0x49;
TRAMEOUT[16]=0x4F;
TRAMEOUT[17]=0x4E;
TRAMEOUT[18]=0x20;
TRAMEOUT[19]=0x44;
TRAMEOUT[20]=0x45;
TRAMEOUT[21]=0x53;
TRAMEOUT[22]=0x20;
TRAMEOUT[23]=0x44;
TRAMEOUT[24]=0x4F;
TRAMEOUT[25]=0x4E;
TRAMEOUT[26]=0x4E;
TRAMEOUT[27]=0x45;
TRAMEOUT[28]=0x45;
TRAMEOUT[29]=0x53;
TRAMEOUT[30]=0x20;
TRAMEOUT[31]=0x44;
TRAMEOUT[32]=0x27;
TRAMEOUT[33]=0x55;
TRAMEOUT[34]=0x4E;
TRAMEOUT[35]=0x20;
TRAMEOUT[36]=0x43;
TRAMEOUT[37]=0x41;
TRAMEOUT[38]=0x42;
TRAMEOUT[39]=0x4C;
TRAMEOUT[40]=0x45;
TRAMEOUT[41]=0x20;
TRAMEOUT[42]=0x45;
TRAMEOUT[43]=0x4E;
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x53;
TRAMEOUT[46]=0x45;
TRAMEOUT[47]=0x52;
TRAMEOUT[48]=0x56;
TRAMEOUT[49]=0x49;
TRAMEOUT[50]=0x43;
TRAMEOUT[51]=0x45;
TRAMEOUT[52]=0x20;
TRAMEOUT[53]=0x1B;
TRAMEOUT[54]=0x5B;
TRAMEOUT[55]=0x30;
TRAMEOUT[56]=0x6D;
TRAMEOUT[57]=0x1B;
TRAMEOUT[58]=0x5B;
TRAMEOUT[59]=0x34;
TRAMEOUT[60]=0x3B;
TRAMEOUT[61]=0x32;
TRAMEOUT[62]=0x48;
TRAMEOUT[63]=0x1B;
TRAMEOUT[64]=0x5B;
TRAMEOUT[65]=0x34;
TRAMEOUT[66]=0x3B;
TRAMEOUT[67]=0x30;
TRAMEOUT[68]=0x48;
TRAMEOUT[69]=0x2D;
TRAMEOUT[70]=0x2D;
TRAMEOUT[71]=0x2D;
TRAMEOUT[72]=0x2D;
TRAMEOUT[73]=0x2D;
TRAMEOUT[74]=0x2D;
TRAMEOUT[75]=0x2D;
TRAMEOUT[76]=0x2D;
TRAMEOUT[77]=0x2D;
TRAMEOUT[78]=0x2D;
TRAMEOUT[79]=0x2D;
TRAMEOUT[80]=0x2D;
TRAMEOUT[81]=0x2D;
TRAMEOUT[82]=0x2D;
TRAMEOUT[83]=0x2D;
TRAMEOUT[84]=0x2D;
TRAMEOUT[85]=0x2D;
TRAMEOUT[86]=0x2D;
TRAMEOUT[87]=0x2D;
TRAMEOUT[88]=0x2D;
TRAMEOUT[89]=0x2D;
TRAMEOUT[90]=0x2D;
TRAMEOUT[91]=0x2D;
TRAMEOUT[92]=0x2D;
TRAMEOUT[93]=0x2D;
TRAMEOUT[94]=0x2D;
TRAMEOUT[95]=0x2D;
TRAMEOUT[96]=0x2D;
TRAMEOUT[97]=0x2D;
TRAMEOUT[98]=0x2D;
TRAMEOUT[99]=0x2D;
TRAMEOUT[100]=0x2D;
TRAMEOUT[101]=0x2D;
TRAMEOUT[102]=0x2D;
TRAMEOUT[103]=0x2D;
TRAMEOUT[104]=0x2D;
TRAMEOUT[105]=0x2D;
TRAMEOUT[106]=0x2D;
TRAMEOUT[107]=0x2D;
TRAMEOUT[108]=0x2D;
TRAMEOUT[109]=0x2D;
TRAMEOUT[110]=0x2D;
TRAMEOUT[111]=0x2D;
TRAMEOUT[112]=0x2D;
TRAMEOUT[113]=0x2D;
TRAMEOUT[114]=0x2D;
TRAMEOUT[115]=0x2D;
TRAMEOUT[116]=0x2D;
TRAMEOUT[117]=0x2D;
TRAMEOUT[118]=0x2D;
TRAMEOUT[119]=0x2D;
TRAMEOUT[120]=0x2D;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='5';
TRAMEOUT[3]='c';
TRAMEOUT[4]=0x2D;
TRAMEOUT[5]=0x2D;
TRAMEOUT[6]=0x2D;
TRAMEOUT[7]=0x2D;
TRAMEOUT[8]=0x2D;
TRAMEOUT[9]=0x2D;
TRAMEOUT[10]=0x2D;
TRAMEOUT[11]=0x2D;
TRAMEOUT[12]=0x2D;
TRAMEOUT[13]=0x2D;
TRAMEOUT[14]=0x2D;
TRAMEOUT[15]=0x2D;
TRAMEOUT[16]=0x2D;
TRAMEOUT[17]=0x2D;
TRAMEOUT[18]=0x2D;
TRAMEOUT[19]=0x2D;
TRAMEOUT[20]=0x2D;
TRAMEOUT[21]=0x2D;
TRAMEOUT[22]=0x2D;
TRAMEOUT[23]=0x2D;
TRAMEOUT[24]=0x2D;
TRAMEOUT[25]=0x2D;
TRAMEOUT[26]=0x2D;
TRAMEOUT[27]=0x2D;
TRAMEOUT[28]=0x2D;
TRAMEOUT[29]=0x2D;
TRAMEOUT[30]=0x2D;
TRAMEOUT[31]=0x2D;
TRAMEOUT[32]=0x1B;
TRAMEOUT[33]=0x5B;
TRAMEOUT[34]=0x35;
TRAMEOUT[35]=0x3B;
TRAMEOUT[36]=0x32;
TRAMEOUT[37]=0x48;
TRAMEOUT[38]=0x4E;
TRAMEOUT[39]=0x2E;
TRAMEOUT[40]=0x20;
TRAMEOUT[41]=0x20;
TRAMEOUT[42]=0x21;
TRAMEOUT[43]=0x21;
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x20;
TRAMEOUT[46]=0x1B;
TRAMEOUT[47]=0x5B;
TRAMEOUT[48]=0x36;
TRAMEOUT[49]=0x3B;
TRAMEOUT[50]=0x32;
TRAMEOUT[51]=0x48;
TRAMEOUT[52]=0x54;
TRAMEOUT[53]=0x43;
TRAMEOUT[54]=0x41;
TRAMEOUT[55]=0x50;
TRAMEOUT[56]=0x21;
TRAMEOUT[57]=0x21;
TRAMEOUT[58]=0x1B;
TRAMEOUT[59]=0x5B;
TRAMEOUT[60]=0x35;
TRAMEOUT[61]=0x3B;
TRAMEOUT[62]=0x31;
TRAMEOUT[63]=0x30;
TRAMEOUT[64]=0x48;
TRAMEOUT[65]=0x43;
TRAMEOUT[66]=0x20;
TRAMEOUT[67]='D';
TRAMEOUT[68]=0x6D;
TRAMEOUT[69]=0x41;
TRAMEOUT[70]=0x20;
TRAMEOUT[71]=0x1B;
TRAMEOUT[72]=0x5B;
TRAMEOUT[73]=0x35;
TRAMEOUT[74]=0x3B;
TRAMEOUT[75]=0x31;
TRAMEOUT[76]=0x36;
TRAMEOUT[77]=0x48;
TRAMEOUT[78]=0x21;
TRAMEOUT[79]=0x20;
TRAMEOUT[80]=0x43;
TRAMEOUT[81]=0x20;
TRAMEOUT[82]='D';
TRAMEOUT[83]=0x6D;
TRAMEOUT[84]=0x41;
TRAMEOUT[85]=0x20;
TRAMEOUT[86]=0x20;
TRAMEOUT[87]=0x20;
TRAMEOUT[88]=0x21;
TRAMEOUT[89]=0x20;
TRAMEOUT[90]=0x43;
TRAMEOUT[91]=0x20;
TRAMEOUT[92]='u';
TRAMEOUT[93]=0x6D;
TRAMEOUT[94]=0x41;
TRAMEOUT[95]=0x20;
TRAMEOUT[96]=0x21;
TRAMEOUT[97]=0x20;
TRAMEOUT[98]=0x20;
TRAMEOUT[99]=0x20;
TRAMEOUT[100]=0x20;
TRAMEOUT[101]=0x20;
TRAMEOUT[102]=0x20;
TRAMEOUT[103]=0x20;
TRAMEOUT[104]=0x20;
TRAMEOUT[105]=0x21;
TRAMEOUT[106]=0x20;
TRAMEOUT[107]=0x20;
TRAMEOUT[108]=0x20;
TRAMEOUT[109]=0x20;
TRAMEOUT[110]=0x20;
TRAMEOUT[111]=0x20;
TRAMEOUT[112]=0x20;
TRAMEOUT[113]=0x21;
TRAMEOUT[114]=0x20;
TRAMEOUT[115]=0x20;
TRAMEOUT[116]=0x20;
TRAMEOUT[117]=0x20;
TRAMEOUT[118]=0x20;
TRAMEOUT[119]=0x20;
TRAMEOUT[120]=0x21;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='5';
TRAMEOUT[3]='d';
TRAMEOUT[4]=0x20;
TRAMEOUT[5]=0x20;
TRAMEOUT[6]=0x20;
TRAMEOUT[7]=0x20;
TRAMEOUT[8]=0x20;
TRAMEOUT[9]=0x20;
TRAMEOUT[10]=0x21;
TRAMEOUT[11]=0x1B;
TRAMEOUT[12]=0x5B;
TRAMEOUT[13]=0x36;
TRAMEOUT[14]=0x3B;
TRAMEOUT[15]=0x31;
TRAMEOUT[16]=0x30;
TRAMEOUT[17]=0x48;
TRAMEOUT[18]=0x43;
TRAMEOUT[19]=0x4F;
TRAMEOUT[20]=0x4E;
TRAMEOUT[21]=0x53;
TRAMEOUT[22]=0x4F;
TRAMEOUT[23]=0x20;
TRAMEOUT[24]=0x1B;
TRAMEOUT[25]=0x5B;
TRAMEOUT[26]=0x36;
TRAMEOUT[27]=0x3B;
TRAMEOUT[28]=0x31;
TRAMEOUT[29]=0x36;
TRAMEOUT[30]=0x48;
TRAMEOUT[31]=0x21;
TRAMEOUT[32]=0x20;
TRAMEOUT[33]=0x4D;
TRAMEOUT[34]=0x4F;
TRAMEOUT[35]=0x44;
TRAMEOUT[36]=0x55;
TRAMEOUT[37]=0x4C;
TRAMEOUT[38]=0x41;
TRAMEOUT[39]=0x54;
TRAMEOUT[40]=0x20;
TRAMEOUT[41]=0x21;
TRAMEOUT[42]=0x20;
TRAMEOUT[43]=0x52;
TRAMEOUT[44]=0x45;
TRAMEOUT[45]=0x50;
TRAMEOUT[46]=0x4F;
TRAMEOUT[47]=0x53;
TRAMEOUT[48]=0x20;
TRAMEOUT[49]=0x21;
TRAMEOUT[50]=0x20;
TRAMEOUT[51]=0x4D;
TRAMEOUT[52]=0x45;
TRAMEOUT[53]=0x53;
TRAMEOUT[54]=0x55;
TRAMEOUT[55]=0x52;
TRAMEOUT[56]=0x45;
TRAMEOUT[57]=0x20;
TRAMEOUT[58]=0x21;
TRAMEOUT[59]=0x20;
TRAMEOUT[60]=0x53;
TRAMEOUT[61]=0x45;
TRAMEOUT[62]=0x55;
TRAMEOUT[63]=0x49;
TRAMEOUT[64]=0x4C;
TRAMEOUT[65]=0x20;
TRAMEOUT[66]=0x21;
TRAMEOUT[67]=0x20;
TRAMEOUT[68]=0x45;
TRAMEOUT[69]=0x54;
TRAMEOUT[70]=0x41;
TRAMEOUT[71]=0x54;
TRAMEOUT[72]=0x20;
TRAMEOUT[73]=0x21;
TRAMEOUT[74]=0x20;
TRAMEOUT[75]=0x44;
TRAMEOUT[76]=0x49;
TRAMEOUT[77]=0x53;
TRAMEOUT[78]=0x54;
TRAMEOUT[79]=0x20;
TRAMEOUT[80]=0x21;
TRAMEOUT[81]=0x20;
TRAMEOUT[82]=0x43;
TRAMEOUT[83]=0x4F;
TRAMEOUT[84]=0x4D;
TRAMEOUT[85]=0x4D;
TRAMEOUT[86]=0x45;
TRAMEOUT[87]=0x4E;
TRAMEOUT[88]=0x54;
TRAMEOUT[89]=0x41;
TRAMEOUT[90]=0x49;
TRAMEOUT[91]=0x52;
TRAMEOUT[92]=0x45;
TRAMEOUT[93]=0x1B;
TRAMEOUT[94]=0x5B;
TRAMEOUT[95]=0x37;
TRAMEOUT[96]=0x3B;
TRAMEOUT[97]=0x32;
TRAMEOUT[98]=0x48;
TRAMEOUT[99]=0x2D;
TRAMEOUT[100]=0x2D;
TRAMEOUT[101]=0x2D;
TRAMEOUT[102]=0x2D;
TRAMEOUT[103]=0x2D;
TRAMEOUT[104]=0x2D;
TRAMEOUT[105]=0x2D;
TRAMEOUT[106]=0x2D;
TRAMEOUT[107]=0x2D;
TRAMEOUT[108]=0x2D;
TRAMEOUT[109]=0x2D;
TRAMEOUT[110]=0x2D;
TRAMEOUT[111]=0x2D;
TRAMEOUT[112]=0x2D;
TRAMEOUT[113]=0x2D;
TRAMEOUT[114]=0x2D;
TRAMEOUT[115]=0x2D;
TRAMEOUT[116]=0x2D;
TRAMEOUT[117]=0x2D;
TRAMEOUT[118]=0x2D;
TRAMEOUT[119]=0x2D;
TRAMEOUT[120]=0x2D;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='5';
TRAMEOUT[3]='e';
TRAMEOUT[4]=0x2D;
TRAMEOUT[5]=0x2D;
TRAMEOUT[6]=0x2D;
TRAMEOUT[7]=0x2D;
TRAMEOUT[8]=0x2D;
TRAMEOUT[9]=0x2D;
TRAMEOUT[10]=0x2D;
TRAMEOUT[11]=0x2D;
TRAMEOUT[12]=0x2D;
TRAMEOUT[13]=0x2D;
TRAMEOUT[14]=0x2D;
TRAMEOUT[15]=0x2D;
TRAMEOUT[16]=0x2D;
TRAMEOUT[17]=0x2D;
TRAMEOUT[18]=0x2D;
TRAMEOUT[19]=0x2D;
TRAMEOUT[20]=0x2D;
TRAMEOUT[21]=0x2D;
TRAMEOUT[22]=0x2D;
TRAMEOUT[23]=0x2D;
TRAMEOUT[24]=0x2D;
TRAMEOUT[25]=0x2D;
TRAMEOUT[26]=0x2D;
TRAMEOUT[27]=0x2D;
TRAMEOUT[28]=0x2D;
TRAMEOUT[29]=0x2D;
TRAMEOUT[30]=0x2D;
TRAMEOUT[31]=0x2D;
TRAMEOUT[32]=0x2D;
TRAMEOUT[33]=0x2D;
TRAMEOUT[34]=0x2D;
TRAMEOUT[35]=0x2D;
TRAMEOUT[36]=0x2D;
TRAMEOUT[37]=0x2D;
TRAMEOUT[38]=0x2D;
TRAMEOUT[39]=0x2D;
TRAMEOUT[40]=0x2D;
TRAMEOUT[41]=0x2D;
TRAMEOUT[42]=0x2D;
TRAMEOUT[43]=0x2D;
TRAMEOUT[44]=0x2D;
TRAMEOUT[45]=0x2D;
TRAMEOUT[46]=0x2D;
TRAMEOUT[47]=0x2D;
TRAMEOUT[48]=0x2D;
TRAMEOUT[49]=0x2D;
TRAMEOUT[50]=0x2D;
TRAMEOUT[51]=0x2D;
TRAMEOUT[52]=0x2D;
TRAMEOUT[53]=0x2D;
TRAMEOUT[54]=0x2D;
TRAMEOUT[55]=0x2D;
TRAMEOUT[56]=0x2D;
TRAMEOUT[57]=0x2D;
TRAMEOUT[58]=0x2D;
TRAMEOUT[59]=0x2D;
TRAMEOUT[60]=0x2D;

TRAMEOUT[61]='*';



j=0;
for (iii=4;iii<61;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (57);
}
else if (num == '5')
{
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='5';
TRAMEOUT[3]='a';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x32;
TRAMEOUT[7]=0x4A;
TRAMEOUT[8]=0x1B;
TRAMEOUT[9]=0x5B;
TRAMEOUT[10]=0x31;
TRAMEOUT[11]=0x3B;
TRAMEOUT[12]=0x33;
TRAMEOUT[13]=0x38;
TRAMEOUT[14]=0x48;
TRAMEOUT[15]=0x53;
TRAMEOUT[16]=0x69;
TRAMEOUT[17]=0x74;
TRAMEOUT[18]=0x65;
TRAMEOUT[19]=0x3A;
TRAMEOUT[20]=0x20;
TRAMEOUT[21]=0x1B;
TRAMEOUT[22]=0x5B;
TRAMEOUT[23]=0x30;
TRAMEOUT[24]=0x6D;
TRAMEOUT[25]=0x1B;
TRAMEOUT[26]=0x5B;
TRAMEOUT[27]=0x35;
TRAMEOUT[28]=0x3B;
TRAMEOUT[29]=0x32;
TRAMEOUT[30]=0x31;
TRAMEOUT[31]=0x48;
TRAMEOUT[32]=0x56;
TRAMEOUT[33]=0x49;
TRAMEOUT[34]=0x53;
TRAMEOUT[35]=0x55;
TRAMEOUT[36]=0x41;
TRAMEOUT[37]=0x4C;
TRAMEOUT[38]=0x49;
TRAMEOUT[39]=0x53;
TRAMEOUT[40]=0x41;
TRAMEOUT[41]=0x54;
TRAMEOUT[42]=0x49;
TRAMEOUT[43]=0x4F;
TRAMEOUT[44]=0x4E;
TRAMEOUT[45]=0x20;
TRAMEOUT[46]=0x44;
TRAMEOUT[47]=0x45;
TRAMEOUT[48]=0x53;
TRAMEOUT[49]=0x20;
TRAMEOUT[50]=0x50;
TRAMEOUT[51]=0x41;
TRAMEOUT[52]=0x52;
TRAMEOUT[53]=0x41;
TRAMEOUT[54]=0x4D;
TRAMEOUT[55]=0x45;
TRAMEOUT[56]=0x54;
TRAMEOUT[57]=0x52;
TRAMEOUT[58]=0x45;
TRAMEOUT[59]=0x53;
TRAMEOUT[60]=0x20;
TRAMEOUT[61]=0x44;
TRAMEOUT[62]=0x55;
TRAMEOUT[63]=0x20;
TRAMEOUT[64]=0x47;
TRAMEOUT[65]=0x52;
TRAMEOUT[66]=0x4F;
TRAMEOUT[67]=0x55;
TRAMEOUT[68]=0x50;
TRAMEOUT[69]=0x45;
TRAMEOUT[70]=0x1B;
TRAMEOUT[71]=0x5B;
TRAMEOUT[72]=0x37;
TRAMEOUT[73]=0x3B;
TRAMEOUT[74]=0x37;
TRAMEOUT[75]=0x48;
TRAMEOUT[76]=0x08;
TRAMEOUT[77]=0x20;
TRAMEOUT[78]=0x20;
TRAMEOUT[79]=0x20;
TRAMEOUT[80]=0x20;
TRAMEOUT[81]=0x20;
TRAMEOUT[82]=0x20;
TRAMEOUT[83]=0x20;
TRAMEOUT[84]=0x50;
TRAMEOUT[85]=0x61;
TRAMEOUT[86]=0x72;
TRAMEOUT[87]=0x61;
TRAMEOUT[88]=0x6D;
TRAMEOUT[89]=0x65;
TRAMEOUT[90]=0x74;
TRAMEOUT[91]=0x72;
TRAMEOUT[92]=0x65;
TRAMEOUT[93]=0x73;
TRAMEOUT[94]=0x20;
TRAMEOUT[95]=0x20;
TRAMEOUT[96]=0x20;
TRAMEOUT[97]=0x20;
TRAMEOUT[98]=0x20;
TRAMEOUT[99]=0x20;
TRAMEOUT[100]=0x20;
TRAMEOUT[101]=0x20;
TRAMEOUT[102]=0x20;
TRAMEOUT[103]=0x20;
TRAMEOUT[104]=0x20;
TRAMEOUT[105]=0x20;
TRAMEOUT[106]=0x56;
TRAMEOUT[107]=0x61;
TRAMEOUT[108]=0x6C;
TRAMEOUT[109]=0x65;
TRAMEOUT[110]=0x75;
TRAMEOUT[111]=0x72;
TRAMEOUT[112]=0x20;
TRAMEOUT[113]=0x20;
TRAMEOUT[114]=0x20;
TRAMEOUT[115]=0x20;
TRAMEOUT[116]=0x45;
TRAMEOUT[117]=0x74;
TRAMEOUT[118]=0x61;
TRAMEOUT[119]=0x74;
TRAMEOUT[120]=0x20;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='5';
TRAMEOUT[3]='b';
TRAMEOUT[4]=0x20;
TRAMEOUT[5]=0x20;
TRAMEOUT[6]=0x20;
TRAMEOUT[7]=0x53;
TRAMEOUT[8]=0x65;
TRAMEOUT[9]=0x75;
TRAMEOUT[10]=0x69;
TRAMEOUT[11]=0x6C;
TRAMEOUT[12]=0x2E;
TRAMEOUT[13]=0x48;
TRAMEOUT[14]=0x20;
TRAMEOUT[15]=0x20;
TRAMEOUT[16]=0x20;
TRAMEOUT[17]=0x20;
TRAMEOUT[18]=0x53;
TRAMEOUT[19]=0x65;
TRAMEOUT[20]=0x75;
TRAMEOUT[21]=0x69;
TRAMEOUT[22]=0x6C;
TRAMEOUT[23]=0x2E;
TRAMEOUT[24]=0x42;
TRAMEOUT[25]=0x20;
TRAMEOUT[26]=0x20;
TRAMEOUT[27]=0x20;
TRAMEOUT[28]=0x49;
TRAMEOUT[29]=0x6E;
TRAMEOUT[30]=0x69;
TRAMEOUT[31]=0x74;
TRAMEOUT[32]=0x1B;
TRAMEOUT[33]=0x5B;
TRAMEOUT[34]=0x39;
TRAMEOUT[35]=0x3B;
TRAMEOUT[36]=0x37;
TRAMEOUT[37]=0x48;
TRAMEOUT[38]=0x08;
TRAMEOUT[39]=0x50;
TRAMEOUT[40]=0x2E;
TRAMEOUT[41]=0x72;
TRAMEOUT[42]=0x65;
TRAMEOUT[43]=0x73;
TRAMEOUT[44]=0x65;
TRAMEOUT[45]=0x72;
TRAMEOUT[46]=0x76;
TRAMEOUT[47]=0x6F;
TRAMEOUT[48]=0x69;
TRAMEOUT[49]=0x72;
TRAMEOUT[50]=0x5B;
TRAMEOUT[51]=0x53;
TRAMEOUT[52]=0x48;
TRAMEOUT[53]=0x5D;
TRAMEOUT[54]=0x5B;
TRAMEOUT[55]=0x53;
TRAMEOUT[56]=0x42;
TRAMEOUT[57]=0x5D;
TRAMEOUT[58]=0x28;
TRAMEOUT[59]=0x6D;
TRAMEOUT[60]=0x62;
TRAMEOUT[61]=0x61;
TRAMEOUT[62]=0x72;
TRAMEOUT[63]=0x29;
TRAMEOUT[64]=0x1B;
TRAMEOUT[65]=0x5B;
TRAMEOUT[66]=0x31;
TRAMEOUT[67]=0x30;
TRAMEOUT[68]=0x3B;
TRAMEOUT[69]=0x37;
TRAMEOUT[70]=0x48;
TRAMEOUT[71]=0x08;
TRAMEOUT[72]=0x50;
TRAMEOUT[73]=0x2E;
TRAMEOUT[74]=0x73;
TRAMEOUT[75]=0x6F;
TRAMEOUT[76]=0x72;
TRAMEOUT[77]=0x74;
TRAMEOUT[78]=0x69;
TRAMEOUT[79]=0x65;
TRAMEOUT[80]=0x20;
TRAMEOUT[81]=0x20;
TRAMEOUT[82]=0x20;
TRAMEOUT[83]=0x5B;
TRAMEOUT[84]=0x53;
TRAMEOUT[85]=0x48;
TRAMEOUT[86]=0x5D;
TRAMEOUT[87]=0x5B;
TRAMEOUT[88]=0x53;
TRAMEOUT[89]=0x42;
TRAMEOUT[90]=0x5D;
TRAMEOUT[91]=0x28;
TRAMEOUT[92]=0x6D;
TRAMEOUT[93]=0x62;
TRAMEOUT[94]=0x61;
TRAMEOUT[95]=0x72;
TRAMEOUT[96]=0x29;
TRAMEOUT[97]=0x1B;
TRAMEOUT[98]=0x5B;
TRAMEOUT[99]=0x31;
TRAMEOUT[100]=0x31;
TRAMEOUT[101]=0x3B;
TRAMEOUT[102]=0x37;
TRAMEOUT[103]=0x48;
TRAMEOUT[104]=0x08;
TRAMEOUT[105]=0x50;
TRAMEOUT[106]=0x2E;
TRAMEOUT[107]=0x73;
TRAMEOUT[108]=0x65;
TRAMEOUT[109]=0x63;
TRAMEOUT[110]=0x6F;
TRAMEOUT[111]=0x75;
TRAMEOUT[112]=0x72;
TRAMEOUT[113]=0x73;
TRAMEOUT[114]=0x20;
TRAMEOUT[115]=0x5B;
TRAMEOUT[116]=0x53;
TRAMEOUT[117]=0x42;
TRAMEOUT[118]=0x5D;
TRAMEOUT[119]=0x5B;
TRAMEOUT[120]=0x53;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='5';
TRAMEOUT[3]='c';
TRAMEOUT[4]=0x74;
TRAMEOUT[5]=0x42;
TRAMEOUT[6]=0x5D;
TRAMEOUT[7]=0x28;
TRAMEOUT[8]=0x6D;
TRAMEOUT[9]=0x62;
TRAMEOUT[10]=0x61;
TRAMEOUT[11]=0x72;
TRAMEOUT[12]=0x29;
TRAMEOUT[13]=0x1B;
TRAMEOUT[14]=0x5B;
TRAMEOUT[15]=0x31;
TRAMEOUT[16]=0x32;
TRAMEOUT[17]=0x3B;
TRAMEOUT[18]=0x37;
TRAMEOUT[19]=0x48;
TRAMEOUT[20]=0x08;
TRAMEOUT[21]=0x50;
TRAMEOUT[22]=0x6F;
TRAMEOUT[23]=0x69;
TRAMEOUT[24]=0x6E;
TRAMEOUT[25]=0x74;
TRAMEOUT[26]=0x20;
TRAMEOUT[27]=0x64;
TRAMEOUT[28]=0x65;
TRAMEOUT[29]=0x20;
TRAMEOUT[30]=0x52;
TRAMEOUT[31]=0x6F;
TRAMEOUT[32]=0x73;
TRAMEOUT[33]=0x65;
TRAMEOUT[34]=0x65;
TRAMEOUT[35]=0x20;
TRAMEOUT[36]=0x20;
TRAMEOUT[37]=0x20;
TRAMEOUT[38]=0x20;
TRAMEOUT[39]=0x20;
TRAMEOUT[40]=0x28;
TRAMEOUT[41]=0x6F;
TRAMEOUT[42]=0x4B;
TRAMEOUT[43]=0x29;
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x20;
TRAMEOUT[46]=0x1B;
TRAMEOUT[47]=0x5B;
TRAMEOUT[48]=0x31;
TRAMEOUT[49]=0x33;
TRAMEOUT[50]=0x3B;
TRAMEOUT[51]=0x37;
TRAMEOUT[52]=0x48;
TRAMEOUT[53]=0x08;
TRAMEOUT[54]=0x44;
TRAMEOUT[55]=0x65;
TRAMEOUT[56]=0x62;
TRAMEOUT[57]=0x69;
TRAMEOUT[58]=0x74;
TRAMEOUT[59]=0x20;
TRAMEOUT[60]=0x47;
TRAMEOUT[61]=0x6C;
TRAMEOUT[62]=0x6F;
TRAMEOUT[63]=0x62;
TRAMEOUT[64]=0x61;
TRAMEOUT[65]=0x6C;
TRAMEOUT[66]=0x20;
TRAMEOUT[67]=0x20;
TRAMEOUT[68]=0x20;
TRAMEOUT[69]=0x20;
TRAMEOUT[70]=0x20;
TRAMEOUT[71]=0x20;
TRAMEOUT[72]=0x20;
TRAMEOUT[73]=0x28;
TRAMEOUT[74]=0x6C;
TRAMEOUT[75]=0x2F;
TRAMEOUT[76]=0x68;
TRAMEOUT[77]=0x29;
TRAMEOUT[78]=0x20;
TRAMEOUT[79]=0x1B;
TRAMEOUT[80]=0x5B;
TRAMEOUT[81]=0x31;
TRAMEOUT[82]=0x34;
TRAMEOUT[83]=0x3B;
TRAMEOUT[84]=0x37;
TRAMEOUT[85]=0x48;
TRAMEOUT[86]=0x08;
TRAMEOUT[87]=0x54;
TRAMEOUT[88]=0x65;
TRAMEOUT[89]=0x6D;
TRAMEOUT[90]=0x70;
TRAMEOUT[91]=0x73;
TRAMEOUT[92]=0x20;
TRAMEOUT[93]=0x63;
TRAMEOUT[94]=0x79;
TRAMEOUT[95]=0x63;
TRAMEOUT[96]=0x6C;
TRAMEOUT[97]=0x65;
TRAMEOUT[98]=0x20;
TRAMEOUT[99]=0x6D;
TRAMEOUT[100]=0x6F;
TRAMEOUT[101]=0x74;
TRAMEOUT[102]=0x65;
TRAMEOUT[103]=0x75;
TRAMEOUT[104]=0x72;
TRAMEOUT[105]=0x20;
TRAMEOUT[106]=0x28;
TRAMEOUT[107]=0x73;
TRAMEOUT[108]=0x29;
TRAMEOUT[109]=0x20;
TRAMEOUT[110]=0x20;
TRAMEOUT[111]=0x20;
TRAMEOUT[112]=0x1B;
TRAMEOUT[113]=0x5B;
TRAMEOUT[114]=0x31;
TRAMEOUT[115]=0x35;
TRAMEOUT[116]=0x3B;
TRAMEOUT[117]=0x37;
TRAMEOUT[118]=0x48;
TRAMEOUT[119]=0x1B;
TRAMEOUT[120]=0x5B;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='5';
TRAMEOUT[3]='d';
TRAMEOUT[4]=0x31;
TRAMEOUT[5]=0x36;
TRAMEOUT[6]=0x3B;
TRAMEOUT[7]=0x37;
TRAMEOUT[8]=0x48;
TRAMEOUT[9]=0x08;
TRAMEOUT[10]=0x54;
TRAMEOUT[11]=0x65;
TRAMEOUT[12]=0x6D;
TRAMEOUT[13]=0x70;
TRAMEOUT[14]=0x73;
TRAMEOUT[15]=0x20;
TRAMEOUT[16]=0x6D;
TRAMEOUT[17]=0x6F;
TRAMEOUT[18]=0x74;
TRAMEOUT[19]=0x65;
TRAMEOUT[20]=0x75;
TRAMEOUT[21]=0x72;
TRAMEOUT[22]=0x20;
TRAMEOUT[23]=0x20;
TRAMEOUT[24]=0x20;
TRAMEOUT[25]=0x20;
TRAMEOUT[26]=0x20;
TRAMEOUT[27]=0x20;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]=0x28;
TRAMEOUT[30]=0x68;
TRAMEOUT[31]=0x29;
TRAMEOUT[32]=0x20;
TRAMEOUT[33]=0x1B;
TRAMEOUT[34]=0x5B;
TRAMEOUT[35]=0x31;
TRAMEOUT[36]=0x37;
TRAMEOUT[37]=0x3B;
TRAMEOUT[38]=0x37;
TRAMEOUT[39]=0x48;
TRAMEOUT[40]=0x08;
TRAMEOUT[41]=0x50;
TRAMEOUT[42]=0x72;
TRAMEOUT[43]=0x65;
TRAMEOUT[44]=0x73;
TRAMEOUT[45]=0x65;
TRAMEOUT[46]=0x6E;
TRAMEOUT[47]=0x63;
TRAMEOUT[48]=0x65;
TRAMEOUT[49]=0x20;
TRAMEOUT[50]=0x32;
TRAMEOUT[51]=0x32;
TRAMEOUT[52]=0x30;
TRAMEOUT[53]=0x56;
TRAMEOUT[54]=0x20;
TRAMEOUT[55]=0x20;
TRAMEOUT[56]=0x20;
TRAMEOUT[57]=0x20;
TRAMEOUT[58]=0x20;
TRAMEOUT[59]=0x20;
TRAMEOUT[60]=0x20;
TRAMEOUT[61]=0x20;
TRAMEOUT[62]=0x20;
TRAMEOUT[63]=0x20;
TRAMEOUT[64]=0x20;
TRAMEOUT[65]=0x20;
TRAMEOUT[66]=0x1B;
TRAMEOUT[67]=0x5B;
TRAMEOUT[68]=0x31;
TRAMEOUT[69]=0x38;
TRAMEOUT[70]=0x3B;
TRAMEOUT[71]=0x37;
TRAMEOUT[72]=0x48;
TRAMEOUT[73]=0x08;
TRAMEOUT[74]=0x50;
TRAMEOUT[75]=0x72;
TRAMEOUT[76]=0x65;
TRAMEOUT[77]=0x73;
TRAMEOUT[78]=0x65;
TRAMEOUT[79]=0x6E;
TRAMEOUT[80]=0x63;
TRAMEOUT[81]=0x65;
TRAMEOUT[82]=0x20;
TRAMEOUT[83]=0x20;
TRAMEOUT[84]=0x34;
TRAMEOUT[85]=0x38;
TRAMEOUT[86]=0x56;
//TRAMEOUT[87]=0x20;
//TRAMEOUT[88]=0x20;
//TRAMEOUT[89]=0x20;
//TRAMEOUT[90]=0x20;
//TRAMEOUT[91]=0x20;
//TRAMEOUT[92]=0x20;
//TRAMEOUT[93]=0x20;
//TRAMEOUT[94]=0x20;
//TRAMEOUT[95]=0x20;
//TRAMEOUT[96]=0x20;
//TRAMEOUT[97]=0x20;
//TRAMEOUT[98]=0x20;
//TRAMEOUT[99]=0x1B;
//TRAMEOUT[100]=0x5B;
//TRAMEOUT[101]=0x32;
//TRAMEOUT[102]=0x32;
//TRAMEOUT[103]=0x3B;
//TRAMEOUT[104]=0x32;
//TRAMEOUT[105]=0x33;
//TRAMEOUT[106]=0x48;
//TRAMEOUT[107]=0x1B;
//TRAMEOUT[108]=0x5B;
//TRAMEOUT[109]=0x37;
//TRAMEOUT[110]=0x6D;
//TRAMEOUT[111]=0x52;
//TRAMEOUT[112]=0x45;
//TRAMEOUT[113]=0x54;
//TRAMEOUT[114]=0x1B;
//TRAMEOUT[115]=0x5B;
//TRAMEOUT[116]=0x30;
//TRAMEOUT[117]=0x6D;
//TRAMEOUT[118]=0x20;
//TRAMEOUT[119]=0x3D;
//TRAMEOUT[120]=0x20;
//TRAMEOUT[121]='*';
//j=0;
//for (i=4;i<122;i++){
//TRAMEOUT[j]=TRAMEOUT[i];
//j++;
//}
//TRAMEENVOIRS232DONNEES (117);
//TRAMEOUT[0]='#';
//TRAMEOUT[1]='0';
//TRAMEOUT[2]='5';
//TRAMEOUT[3]='e';
//TRAMEOUT[4]=0x76;
//TRAMEOUT[5]=0x61;
//TRAMEOUT[6]=0x6C;
//TRAMEOUT[7]=0x69;
//TRAMEOUT[8]=0x64;
//TRAMEOUT[9]=0x65;
//TRAMEOUT[10]=0x7A;
//TRAMEOUT[11]=0x20;
//TRAMEOUT[12]=0x6C;
//TRAMEOUT[13]=0x65;
//TRAMEOUT[14]=0x73;
//TRAMEOUT[15]=0x20;
//TRAMEOUT[16]=0x70;
//TRAMEOUT[17]=0x61;
//TRAMEOUT[18]=0x72;
//TRAMEOUT[19]=0x61;
//TRAMEOUT[20]=0x6D;
//TRAMEOUT[21]=0x65;
//TRAMEOUT[22]=0x74;
//TRAMEOUT[23]=0x72;
//TRAMEOUT[24]=0x65;
//TRAMEOUT[25]=0x73;
//TRAMEOUT[26]=0x20;
//TRAMEOUT[27]=0x64;
//TRAMEOUT[28]=0x75;
//TRAMEOUT[29]=0x20;
//TRAMEOUT[30]=0x67;
//TRAMEOUT[31]=0x72;
//TRAMEOUT[32]=0x6F;
//TRAMEOUT[33]=0x75;
//TRAMEOUT[34]=0x70;
//TRAMEOUT[35]=0x65;
//TRAMEOUT[36]='*';

j=0;
for (iii=4;iii<87;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (82);
}
else if (num == '6')
{
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='6';
TRAMEOUT[3]='a';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x32;
TRAMEOUT[7]=0x4A;
TRAMEOUT[8]=0x1B;
TRAMEOUT[9]=0x5B;
TRAMEOUT[10]=0x31;
TRAMEOUT[11]=0x3B;
TRAMEOUT[12]=0x33;
TRAMEOUT[13]=0x38;
TRAMEOUT[14]=0x48;
TRAMEOUT[15]=0x53;
TRAMEOUT[16]=0x69;
TRAMEOUT[17]=0x74;
TRAMEOUT[18]=0x65;
TRAMEOUT[19]=0x20;
TRAMEOUT[20]=0x3A;
TRAMEOUT[21]=0x20;
TRAMEOUT[22]=0x1B;
TRAMEOUT[23]=0x5B;
TRAMEOUT[24]=0x30;
TRAMEOUT[25]=0x6D;
TRAMEOUT[26]=0x1B;
TRAMEOUT[27]=0x5B;
TRAMEOUT[28]=0x33;
TRAMEOUT[29]=0x3B;
TRAMEOUT[30]=0x31;
TRAMEOUT[31]=0x30;
TRAMEOUT[32]=0x48;
TRAMEOUT[33]=0x21;
TRAMEOUT[34]=0x20;
TRAMEOUT[35]=0x41;
TRAMEOUT[36]=0x54;
TRAMEOUT[37]=0x54;
TRAMEOUT[38]=0x45;
TRAMEOUT[39]=0x4E;
TRAMEOUT[40]=0x54;
TRAMEOUT[41]=0x49;
TRAMEOUT[42]=0x4F;
TRAMEOUT[43]=0x4E;
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x4E;
TRAMEOUT[46]=0x45;
TRAMEOUT[47]=0x20;
TRAMEOUT[48]=0x44;
TRAMEOUT[49]=0x45;
TRAMEOUT[50]=0x43;
TRAMEOUT[51]=0x4C;
TRAMEOUT[52]=0x41;
TRAMEOUT[53]=0x52;
TRAMEOUT[54]=0x45;
TRAMEOUT[55]=0x45;
TRAMEOUT[56]=0x20;
TRAMEOUT[57]=0x51;
TRAMEOUT[58]=0x55;
TRAMEOUT[59]=0x45;
TRAMEOUT[60]=0x20;
TRAMEOUT[61]=0x55;
TRAMEOUT[62]=0x4E;
TRAMEOUT[63]=0x20;
TRAMEOUT[64]=0x53;
TRAMEOUT[65]=0x45;
TRAMEOUT[66]=0x55;
TRAMEOUT[67]=0x4C;
TRAMEOUT[68]=0x20;
TRAMEOUT[69]=0x54;
TRAMEOUT[70]=0x59;
TRAMEOUT[71]=0x50;
TRAMEOUT[72]=0x45;
TRAMEOUT[73]=0x20;
TRAMEOUT[74]=0x44;
TRAMEOUT[75]=0x45;
TRAMEOUT[76]=0x20;
TRAMEOUT[77]=0x43;
TRAMEOUT[78]=0x41;
TRAMEOUT[79]=0x50;
TRAMEOUT[80]=0x54;
TRAMEOUT[81]=0x45;
TRAMEOUT[82]=0x55;
TRAMEOUT[83]=0x52;
TRAMEOUT[84]=0x20;
TRAMEOUT[85]=0x50;
TRAMEOUT[86]=0x41;
TRAMEOUT[87]=0x52;
TRAMEOUT[88]=0x20;
TRAMEOUT[89]=0x43;
TRAMEOUT[90]=0x41;
TRAMEOUT[91]=0x52;
TRAMEOUT[92]=0x54;
TRAMEOUT[93]=0x45;
TRAMEOUT[94]=0x20;
TRAMEOUT[95]=0x21;
TRAMEOUT[96]=0x1B;
TRAMEOUT[97]=0x5B;
TRAMEOUT[98]=0x35;
TRAMEOUT[99]=0x3B;
TRAMEOUT[100]=0x32;
TRAMEOUT[101]=0x30;
TRAMEOUT[102]=0x48;
TRAMEOUT[103]=0x43;
TRAMEOUT[104]=0x41;
TRAMEOUT[105]=0x52;
TRAMEOUT[106]=0x54;
TRAMEOUT[107]=0x45;
TRAMEOUT[108]=0x20;
TRAMEOUT[109]=0x31;
TRAMEOUT[110]=0x20;
TRAMEOUT[111]=0x28;
TRAMEOUT[112]=0x56;
TRAMEOUT[113]=0x4F;
TRAMEOUT[114]=0x49;
TRAMEOUT[115]=0x45;
TRAMEOUT[116]=0x20;
TRAMEOUT[117]=0x31;
TRAMEOUT[118]=0x20;
TRAMEOUT[119]=0x41;
TRAMEOUT[120]=0x20;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='6';
TRAMEOUT[3]='b';
TRAMEOUT[4]=0x32;
TRAMEOUT[5]=0x30;
TRAMEOUT[6]=0x20;
TRAMEOUT[7]=0x29;
TRAMEOUT[8]=0x20;
TRAMEOUT[9]=0x20;
TRAMEOUT[10]=0x20;
TRAMEOUT[11]=0x3A;
TRAMEOUT[12]=0x20;
TRAMEOUT[13]=0x1B;
TRAMEOUT[14]=0x5B;
TRAMEOUT[15]=0x36;
TRAMEOUT[16]=0x3B;
TRAMEOUT[17]=0x31;
TRAMEOUT[18]=0x30;
TRAMEOUT[19]=0x48;
TRAMEOUT[20]=0x56;
TRAMEOUT[21]=0x4F;
TRAMEOUT[22]=0x49;
TRAMEOUT[23]=0x45;
TRAMEOUT[24]=0x53;
TRAMEOUT[25]=0x20;
TRAMEOUT[26]=0x44;
TRAMEOUT[27]=0x45;
TRAMEOUT[28]=0x43;
TRAMEOUT[29]=0x4C;
TRAMEOUT[30]=0x41;
TRAMEOUT[31]=0x52;
TRAMEOUT[32]=0x45;
TRAMEOUT[33]=0x45;
TRAMEOUT[34]=0x53;
TRAMEOUT[35]=0x20;
TRAMEOUT[36]=0x3A;
TRAMEOUT[37]=0x1B;
TRAMEOUT[38]=0x5B;
TRAMEOUT[39]=0x39;
TRAMEOUT[40]=0x3B;
TRAMEOUT[41]=0x32;
TRAMEOUT[42]=0x30;
TRAMEOUT[43]=0x48;
TRAMEOUT[44]=0x43;
TRAMEOUT[45]=0x41;
TRAMEOUT[46]=0x52;
TRAMEOUT[47]=0x54;
TRAMEOUT[48]=0x45;
TRAMEOUT[49]=0x20;
TRAMEOUT[50]=0x32;
TRAMEOUT[51]=0x20;
TRAMEOUT[52]=0x28;
TRAMEOUT[53]=0x56;
TRAMEOUT[54]=0x4F;
TRAMEOUT[55]=0x49;
TRAMEOUT[56]=0x45;
TRAMEOUT[57]=0x20;
TRAMEOUT[58]=0x32;
TRAMEOUT[59]=0x31;
TRAMEOUT[60]=0x20;
TRAMEOUT[61]=0x41;
TRAMEOUT[62]=0x20;
TRAMEOUT[63]=0x34;
TRAMEOUT[64]=0x30;
TRAMEOUT[65]=0x20;
TRAMEOUT[66]=0x29;
TRAMEOUT[67]=0x20;
TRAMEOUT[68]=0x20;
TRAMEOUT[69]=0x3A;
TRAMEOUT[70]=0x20;
TRAMEOUT[71]=0x1B;
TRAMEOUT[72]=0x5B;
TRAMEOUT[73]=0x31;
TRAMEOUT[74]=0x30;
TRAMEOUT[75]=0x3B;
TRAMEOUT[76]=0x31;
TRAMEOUT[77]=0x30;
TRAMEOUT[78]=0x48;
TRAMEOUT[79]=0x56;
TRAMEOUT[80]=0x4F;
TRAMEOUT[81]=0x49;
TRAMEOUT[82]=0x45;
TRAMEOUT[83]=0x53;
TRAMEOUT[84]=0x20;
TRAMEOUT[85]=0x44;
TRAMEOUT[86]=0x45;
TRAMEOUT[87]=0x43;
TRAMEOUT[88]=0x4C;
TRAMEOUT[89]=0x41;
TRAMEOUT[90]=0x52;
TRAMEOUT[91]=0x45;
TRAMEOUT[92]=0x45;
TRAMEOUT[93]=0x53;
TRAMEOUT[94]=0x20;
TRAMEOUT[95]=0x3A;
TRAMEOUT[96]=0x1B;
TRAMEOUT[97]=0x5B;
TRAMEOUT[98]=0x31;
TRAMEOUT[99]=0x33;
TRAMEOUT[100]=0x3B;
TRAMEOUT[101]=0x32;
TRAMEOUT[102]=0x30;
TRAMEOUT[103]=0x48;
TRAMEOUT[104]=0x43;
TRAMEOUT[105]=0x41;
TRAMEOUT[106]=0x52;
TRAMEOUT[107]=0x54;
TRAMEOUT[108]=0x45;
TRAMEOUT[109]=0x20;
TRAMEOUT[110]=0x33;
TRAMEOUT[111]=0x20;
TRAMEOUT[112]=0x28;
TRAMEOUT[113]=0x56;
TRAMEOUT[114]=0x4F;
TRAMEOUT[115]=0x49;
TRAMEOUT[116]=0x45;
TRAMEOUT[117]=0x20;
TRAMEOUT[118]=0x34;
TRAMEOUT[119]=0x31;
TRAMEOUT[120]=0x20;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='6';
TRAMEOUT[3]='c';
TRAMEOUT[4]=0x41;
TRAMEOUT[5]=0x20;
TRAMEOUT[6]=0x36;
TRAMEOUT[7]=0x30;
TRAMEOUT[8]=0x20;
TRAMEOUT[9]=0x29;
TRAMEOUT[10]=0x20;
TRAMEOUT[11]=0x20;
TRAMEOUT[12]=0x3A;
TRAMEOUT[13]=0x20;
TRAMEOUT[14]=0x1B;
TRAMEOUT[15]=0x5B;
TRAMEOUT[16]=0x31;
TRAMEOUT[17]=0x34;
TRAMEOUT[18]=0x3B;
TRAMEOUT[19]=0x31;
TRAMEOUT[20]=0x30;
TRAMEOUT[21]=0x48;
TRAMEOUT[22]=0x56;
TRAMEOUT[23]=0x4F;
TRAMEOUT[24]=0x49;
TRAMEOUT[25]=0x45;
TRAMEOUT[26]=0x53;
TRAMEOUT[27]=0x20;
TRAMEOUT[28]=0x44;
TRAMEOUT[29]=0x45;
TRAMEOUT[30]=0x43;
TRAMEOUT[31]=0x4C;
TRAMEOUT[32]=0x41;
TRAMEOUT[33]=0x52;
TRAMEOUT[34]=0x45;
TRAMEOUT[35]=0x45;
TRAMEOUT[36]=0x53;
TRAMEOUT[37]=0x20;
TRAMEOUT[38]=0x3A;
TRAMEOUT[39]=0x1B;
TRAMEOUT[40]=0x5B;
TRAMEOUT[41]=0x31;
TRAMEOUT[42]=0x37;
TRAMEOUT[43]=0x3B;
TRAMEOUT[44]=0x32;
TRAMEOUT[45]=0x30;
TRAMEOUT[46]=0x48;
TRAMEOUT[47]=0x43;
TRAMEOUT[48]=0x41;
TRAMEOUT[49]=0x52;
TRAMEOUT[50]=0x54;
TRAMEOUT[51]=0x45;
TRAMEOUT[52]=0x20;
TRAMEOUT[53]=0x34;
TRAMEOUT[54]=0x20;
TRAMEOUT[55]=0x28;
TRAMEOUT[56]=0x56;
TRAMEOUT[57]=0x4F;
TRAMEOUT[58]=0x49;
TRAMEOUT[59]=0x45;
TRAMEOUT[60]=0x20;
TRAMEOUT[61]=0x36;
TRAMEOUT[62]=0x31;
TRAMEOUT[63]=0x20;
TRAMEOUT[64]=0x41;
TRAMEOUT[65]=0x20;
TRAMEOUT[66]=0x38;
TRAMEOUT[67]=0x30;
TRAMEOUT[68]=0x20;
TRAMEOUT[69]=0x29;
TRAMEOUT[70]=0x20;
TRAMEOUT[71]=0x20;
TRAMEOUT[72]=0x3A;
TRAMEOUT[73]=0x20;
TRAMEOUT[74]=0x1B;
TRAMEOUT[75]=0x5B;
TRAMEOUT[76]=0x31;
TRAMEOUT[77]=0x38;
TRAMEOUT[78]=0x3B;
TRAMEOUT[79]=0x31;
TRAMEOUT[80]=0x30;
TRAMEOUT[81]=0x48;
TRAMEOUT[82]=0x56;
TRAMEOUT[83]=0x4F;
TRAMEOUT[84]=0x49;
TRAMEOUT[85]=0x45;
TRAMEOUT[86]=0x53;
TRAMEOUT[87]=0x20;
TRAMEOUT[88]=0x44;
TRAMEOUT[89]=0x45;
TRAMEOUT[90]=0x43;
TRAMEOUT[91]=0x4C;
TRAMEOUT[92]=0x41;
TRAMEOUT[93]=0x52;
TRAMEOUT[94]=0x45;
TRAMEOUT[95]=0x45;
TRAMEOUT[96]=0x53;
TRAMEOUT[97]=0x20;
TRAMEOUT[98]=0x3A;
TRAMEOUT[99]=0x1B;
TRAMEOUT[100]=0x5B;
TRAMEOUT[101]=0x32;
TRAMEOUT[102]=0x31;
TRAMEOUT[103]=0x3B;
TRAMEOUT[104]=0x32;
TRAMEOUT[105]=0x30;
TRAMEOUT[106]=0x48;
TRAMEOUT[107]=0x43;
TRAMEOUT[108]=0x41;
TRAMEOUT[109]=0x52;
TRAMEOUT[110]=0x54;
TRAMEOUT[111]=0x45;
TRAMEOUT[112]=0x20;
TRAMEOUT[113]=0x35;
TRAMEOUT[114]=0x20;
TRAMEOUT[115]=0x28;
TRAMEOUT[116]=0x56;
TRAMEOUT[117]=0x4F;
TRAMEOUT[118]=0x49;
TRAMEOUT[119]=0x45;
TRAMEOUT[120]=0x20;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='6';
TRAMEOUT[3]='d';
TRAMEOUT[4]=0x38;
TRAMEOUT[5]=0x31;
TRAMEOUT[6]=0x20;
TRAMEOUT[7]=0x41;
TRAMEOUT[8]=0x20;
TRAMEOUT[9]=0x31;
TRAMEOUT[10]=0x30;
TRAMEOUT[11]=0x30;
TRAMEOUT[12]=0x20;
TRAMEOUT[13]=0x29;
TRAMEOUT[14]=0x20;
TRAMEOUT[15]=0x3A;
TRAMEOUT[16]=0x20;
TRAMEOUT[17]=0x1B;
TRAMEOUT[18]=0x5B;
TRAMEOUT[19]=0x32;
TRAMEOUT[20]=0x32;
TRAMEOUT[21]=0x3B;
TRAMEOUT[22]=0x31;
TRAMEOUT[23]=0x30;
TRAMEOUT[24]=0x48;
TRAMEOUT[25]=0x56;
TRAMEOUT[26]=0x4F;
TRAMEOUT[27]=0x49;
TRAMEOUT[28]=0x45;
TRAMEOUT[29]=0x53;
TRAMEOUT[30]=0x20;
TRAMEOUT[31]=0x44;
TRAMEOUT[32]=0x45;
TRAMEOUT[33]=0x43;
TRAMEOUT[34]=0x4C;
TRAMEOUT[35]=0x41;
TRAMEOUT[36]=0x52;
TRAMEOUT[37]=0x45;
TRAMEOUT[38]=0x45;
TRAMEOUT[39]=0x53;
TRAMEOUT[40]=0x20;
TRAMEOUT[41]=0x3A;
TRAMEOUT[42]=0x20;
TRAMEOUT[43]=0x20;
TRAMEOUT[44]=0x1B;
TRAMEOUT[45]=0x5B;
TRAMEOUT[46]=0x32;
TRAMEOUT[47]=0x34;
TRAMEOUT[48]=0x3B;
TRAMEOUT[49]=0x32;
TRAMEOUT[50]=0x30;
TRAMEOUT[51]=0x48;
TRAMEOUT[52]=0x1B;
TRAMEOUT[53]=0x5B;
TRAMEOUT[54]=0x37;
TRAMEOUT[55]=0x6D;
TRAMEOUT[56]=0x45;
TRAMEOUT[57]=0x4E;
TRAMEOUT[58]=0x54;
TRAMEOUT[59]=0x52;
TRAMEOUT[60]=0x45;
TRAMEOUT[61]=0x45;
TRAMEOUT[62]=0x1B;
TRAMEOUT[63]=0x5B;
TRAMEOUT[64]=0x30;
TRAMEOUT[65]=0x6D;
TRAMEOUT[66]=0x20;
TRAMEOUT[67]=0x3A;
TRAMEOUT[68]=0x20;
TRAMEOUT[69]=0x50;
TRAMEOUT[70]=0x61;
TRAMEOUT[71]=0x67;
TRAMEOUT[72]=0x65;
TRAMEOUT[73]=0x73;
TRAMEOUT[74]=0x20;
TRAMEOUT[75]=0x73;
TRAMEOUT[76]=0x75;
TRAMEOUT[77]=0x69;
TRAMEOUT[78]=0x76;
TRAMEOUT[79]=0x61;
TRAMEOUT[80]=0x6E;
TRAMEOUT[81]=0x74;
TRAMEOUT[82]=0x65;
TRAMEOUT[83]=0x73;
TRAMEOUT[84]='*';

j=0;
for (iii=4;iii<84;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}

TRAMEENVOIRS232DONNEES (80);
}
else if (num == 'a') /////////////////////////////////////////////6b
{
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='7';
TRAMEOUT[3]='a';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x32;
TRAMEOUT[7]=0x4A;
TRAMEOUT[8]=0x1B;
TRAMEOUT[9]=0x5B;
TRAMEOUT[10]=0x31;
TRAMEOUT[11]=0x3B;
TRAMEOUT[12]=0x33;
TRAMEOUT[13]=0x38;
TRAMEOUT[14]=0x48;
TRAMEOUT[15]=0x53;
TRAMEOUT[16]=0x69;
TRAMEOUT[17]=0x74;
TRAMEOUT[18]=0x65;
TRAMEOUT[19]=0x3A;
TRAMEOUT[20]=0x20;
TRAMEOUT[21]=0x1B;
TRAMEOUT[22]=0x5B;
TRAMEOUT[23]=0x34;
TRAMEOUT[24]=0x3B;
TRAMEOUT[25]=0x32;
TRAMEOUT[26]=0x38;
TRAMEOUT[27]=0x48;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]=0x43;
TRAMEOUT[30]=0x4F;
TRAMEOUT[31]=0x4E;
TRAMEOUT[32]=0x46;
TRAMEOUT[33]=0x49;
TRAMEOUT[34]=0x47;
TRAMEOUT[35]=0x55;
TRAMEOUT[36]=0x52;
TRAMEOUT[37]=0x41;
TRAMEOUT[38]=0x54;
TRAMEOUT[39]=0x49;
TRAMEOUT[40]=0x4F;
TRAMEOUT[41]=0x4E;
TRAMEOUT[42]=0x20;
TRAMEOUT[43]=0x44;
TRAMEOUT[44]=0x27;
TRAMEOUT[45]=0x55;
TRAMEOUT[46]=0x4E;
TRAMEOUT[47]=0x20;
TRAMEOUT[48]=0x43;
TRAMEOUT[49]=0x41;
TRAMEOUT[50]=0x42;
TRAMEOUT[51]=0x4C;
TRAMEOUT[52]=0x45;
TRAMEOUT[53]=0x20;
TRAMEOUT[54]=0x1B;
TRAMEOUT[55]=0x5B;
TRAMEOUT[56]=0x30;
TRAMEOUT[57]=0x6D;
TRAMEOUT[58]=0x1B;
TRAMEOUT[59]=0x5B;
TRAMEOUT[60]=0x35;
TRAMEOUT[61]=0x3B;
TRAMEOUT[62]=0x33;
TRAMEOUT[63]=0x38;
TRAMEOUT[64]=0x48;
TRAMEOUT[65]=0x1B;
TRAMEOUT[66]=0x5B;
TRAMEOUT[67]=0x36;
TRAMEOUT[68]=0x3B;
TRAMEOUT[69]=0x31;
TRAMEOUT[70]=0x35;
TRAMEOUT[71]=0x48;
TRAMEOUT[72]=0x4E;
TRAMEOUT[73]=0x6F;
TRAMEOUT[74]=0x20;
TRAMEOUT[75]=0x64;
TRAMEOUT[76]=0x65;
TRAMEOUT[77]=0x20;
TRAMEOUT[78]=0x76;
TRAMEOUT[79]=0x6F;
TRAMEOUT[80]=0x69;
TRAMEOUT[81]=0x65;
TRAMEOUT[82]=0x20;
TRAMEOUT[83]=0x3A;
TRAMEOUT[84]=0x20;
TRAMEOUT[85]=0x1B;
TRAMEOUT[86]=0x5B;
TRAMEOUT[87]=0x37;
TRAMEOUT[88]=0x3B;
TRAMEOUT[89]=0x31;
TRAMEOUT[90]=0x35;
TRAMEOUT[91]=0x48;
TRAMEOUT[92]=0x52;
TRAMEOUT[93]=0x4F;
TRAMEOUT[94]=0x42;
TRAMEOUT[95]=0x3A;
TRAMEOUT[96]=0x52;
TRAMEOUT[97]=0x6F;
TRAMEOUT[98]=0x20;
TRAMEOUT[99]=0x1B;
TRAMEOUT[100]=0x5B;
TRAMEOUT[101]=0x37;
TRAMEOUT[102]=0x3B;
TRAMEOUT[103]=0x33;
TRAMEOUT[104]=0x38;
TRAMEOUT[105]=0x48;
TRAMEOUT[106]=0x1B;
TRAMEOUT[107]=0x5B;
TRAMEOUT[108]=0x39;
TRAMEOUT[109]=0x3B;
TRAMEOUT[110]=0x31;
TRAMEOUT[111]=0x35;
TRAMEOUT[112]=0x48;
TRAMEOUT[113]=0x43;
TRAMEOUT[114]=0x6F;
TRAMEOUT[115]=0x6D;
TRAMEOUT[116]=0x6D;
TRAMEOUT[117]=0x65;
TRAMEOUT[118]=0x6E;
TRAMEOUT[119]=0x74;
TRAMEOUT[120]=0x61;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='7';
TRAMEOUT[3]='b';
TRAMEOUT[4]=0x69;
TRAMEOUT[5]=0x72;
TRAMEOUT[6]=0x65;
TRAMEOUT[7]=0x20;
TRAMEOUT[8]=0x3A;
TRAMEOUT[9]=0x20;
TRAMEOUT[10]=0x20;
TRAMEOUT[11]=0x20;
TRAMEOUT[12]=0x20;
TRAMEOUT[13]=0x0A;
TRAMEOUT[14]=0x08;
TRAMEOUT[15]=0x1B;
TRAMEOUT[16]=0x5B;
TRAMEOUT[17]=0x36;
TRAMEOUT[18]=0x3B;
TRAMEOUT[19]=0x34;
TRAMEOUT[20]=0x32;
TRAMEOUT[21]=0x48;
TRAMEOUT[22]=0x4E;
TRAMEOUT[23]=0x75;
TRAMEOUT[24]=0x6D;
TRAMEOUT[25]=0x65;
TRAMEOUT[26]=0x72;
TRAMEOUT[27]=0x6F;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]=0x64;
TRAMEOUT[30]=0x65;
TRAMEOUT[31]=0x20;
TRAMEOUT[32]=0x63;
TRAMEOUT[33]=0x61;
TRAMEOUT[34]=0x72;
TRAMEOUT[35]=0x74;
TRAMEOUT[36]=0x65;
TRAMEOUT[37]=0x3A;
TRAMEOUT[38]=0x20;
TRAMEOUT[39]=0x20;
TRAMEOUT[40]=0x20;
TRAMEOUT[41]=0x0A;
TRAMEOUT[42]=0x08;
TRAMEOUT[43]=0x08;
TRAMEOUT[44]=0x08;
TRAMEOUT[45]=0x08;
TRAMEOUT[46]=0x1B;
TRAMEOUT[47]=0x5B;
TRAMEOUT[48]=0x37;
TRAMEOUT[49]=0x3B;
TRAMEOUT[50]=0x34;
TRAMEOUT[51]=0x32;
TRAMEOUT[52]=0x48;
TRAMEOUT[53]=0x4E;
TRAMEOUT[54]=0x75;
TRAMEOUT[55]=0x6D;
TRAMEOUT[56]=0x65;
TRAMEOUT[57]=0x72;
TRAMEOUT[58]=0x6F;
TRAMEOUT[59]=0x20;
TRAMEOUT[60]=0x64;
TRAMEOUT[61]=0x65;
TRAMEOUT[62]=0x20;
TRAMEOUT[63]=0x6C;
TRAMEOUT[64]=0x69;
TRAMEOUT[65]=0x67;
TRAMEOUT[66]=0x6E;
TRAMEOUT[67]=0x65;
TRAMEOUT[68]=0x3A;
TRAMEOUT[69]=0x20;
TRAMEOUT[70]=0x1B;
TRAMEOUT[71]=0x5B;
TRAMEOUT[72]=0x38;
TRAMEOUT[73]=0x3B;
TRAMEOUT[74]=0x34;
TRAMEOUT[75]=0x31;
TRAMEOUT[76]=0x48;
TRAMEOUT[77]=0x20;
TRAMEOUT[78]=0x0A;
TRAMEOUT[79]=0x08;
TRAMEOUT[80]=0x08;
TRAMEOUT[81]=0x20;
TRAMEOUT[82]=0x20;
TRAMEOUT[83]=0x1B;
TRAMEOUT[84]=0x5B;
TRAMEOUT[85]=0x32;
TRAMEOUT[86]=0x32;
TRAMEOUT[87]=0x3B;
TRAMEOUT[88]=0x31;
TRAMEOUT[89]=0x48;
TRAMEOUT[90]=0x1B;
TRAMEOUT[91]=0x5B;
TRAMEOUT[92]=0x37;
TRAMEOUT[93]=0x6D;
TRAMEOUT[94]=0x53;
TRAMEOUT[95]=0x41;
TRAMEOUT[96]=0x49;
TRAMEOUT[97]=0x53;
TRAMEOUT[98]=0x49;
TRAMEOUT[99]=0x53;
TRAMEOUT[100]=0x53;
TRAMEOUT[101]=0x45;
TRAMEOUT[102]=0x5A;
TRAMEOUT[103]=0x1B;
TRAMEOUT[104]=0x5B;
TRAMEOUT[105]=0x30;
TRAMEOUT[106]=0x6D;
TRAMEOUT[107]=0x20;
TRAMEOUT[108]=0x6C;
TRAMEOUT[109]=0x65;
TRAMEOUT[110]=0x20;
TRAMEOUT[111]=0x6E;
TRAMEOUT[112]=0x75;
TRAMEOUT[113]=0x6D;
TRAMEOUT[114]=0x65;
TRAMEOUT[115]=0x72;
TRAMEOUT[116]=0x6F;
TRAMEOUT[117]=0x20;
TRAMEOUT[118]=0x64;
TRAMEOUT[119]=0x65;
TRAMEOUT[120]=0x20;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='7';
TRAMEOUT[3]='c';
TRAMEOUT[4]=0x76;//63
TRAMEOUT[5]=0x6F;//61
TRAMEOUT[6]=0x69;//62
TRAMEOUT[7]=0x65;//6c
TRAMEOUT[8]=0x20;//65
TRAMEOUT[9]=0x20;
TRAMEOUT[10]=0x40;
TRAMEOUT[11]=0x20;
TRAMEOUT[12]=0x65;
TRAMEOUT[13]=0x64;
TRAMEOUT[14]=0x69;
TRAMEOUT[15]=0x74;
TRAMEOUT[16]=0x65;
TRAMEOUT[17]=0x72;
TRAMEOUT[18]='#';
j=0;
for (iii=4;iii<19;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (14);

}
else if (num == 'b')////////////////////////////////////////////6c
{
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='8';
TRAMEOUT[3]='a';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x37;
TRAMEOUT[7]=0x6D;
TRAMEOUT[8]=0x1B;
TRAMEOUT[9]=0x5B;
TRAMEOUT[10]=0x34;
TRAMEOUT[11]=0x3B;
TRAMEOUT[12]=0x32;
TRAMEOUT[13]=0x32;
TRAMEOUT[14]=0x48;
TRAMEOUT[15]=0x20;
TRAMEOUT[16]=0x43;
TRAMEOUT[17]=0x4F;
TRAMEOUT[18]=0x4E;
TRAMEOUT[19]=0x46;
TRAMEOUT[20]=0x49;
TRAMEOUT[21]=0x47;
TRAMEOUT[22]=0x55;
TRAMEOUT[23]=0x52;
TRAMEOUT[24]=0x41;
TRAMEOUT[25]=0x54;
TRAMEOUT[26]=0x49;
TRAMEOUT[27]=0x4F;
TRAMEOUT[28]=0x4E;
TRAMEOUT[29]=0x20;
TRAMEOUT[30]=0x44;
TRAMEOUT[31]=0x45;
TRAMEOUT[32]=0x53;
TRAMEOUT[33]=0x20;
TRAMEOUT[34]=0x43;
TRAMEOUT[35]=0x41;
TRAMEOUT[36]=0x50;
TRAMEOUT[37]=0x54;
TRAMEOUT[38]=0x45;
TRAMEOUT[39]=0x55;
TRAMEOUT[40]=0x52;
TRAMEOUT[41]=0x53;
TRAMEOUT[42]=0x20;
TRAMEOUT[43]=0x44;
TRAMEOUT[44]=0x27;
TRAMEOUT[45]=0x55;
TRAMEOUT[46]=0x4E;
TRAMEOUT[47]=0x20;
TRAMEOUT[48]=0x43;
TRAMEOUT[49]=0x41;
TRAMEOUT[50]=0x42;
TRAMEOUT[51]=0x4C;
TRAMEOUT[52]=0x45;
TRAMEOUT[53]=0x20;
TRAMEOUT[54]=0x1B;
TRAMEOUT[55]=0x5B;
TRAMEOUT[56]=0x30;
TRAMEOUT[57]=0x6D;
TRAMEOUT[58]=0x1B;
TRAMEOUT[59]=0x5B;
TRAMEOUT[60]=0x31;
TRAMEOUT[61]=0x31;
TRAMEOUT[62]=0x3B;
TRAMEOUT[63]=0x32;
TRAMEOUT[64]=0x48;
TRAMEOUT[65]=0x2D;
TRAMEOUT[66]=0x2D;
TRAMEOUT[67]=0x2D;
TRAMEOUT[68]=0x2D;
TRAMEOUT[69]=0x2D;
TRAMEOUT[70]=0x2D;
TRAMEOUT[71]=0x2D;
TRAMEOUT[72]=0x2D;
TRAMEOUT[73]=0x2D;
TRAMEOUT[74]=0x2D;
TRAMEOUT[75]=0x2D;
TRAMEOUT[76]=0x2D;
TRAMEOUT[77]=0x2D;
TRAMEOUT[78]=0x2D;
TRAMEOUT[79]=0x2D;
TRAMEOUT[80]=0x2D;
TRAMEOUT[81]=0x2D;
TRAMEOUT[82]=0x2D;
TRAMEOUT[83]=0x2D;
TRAMEOUT[84]=0x2D;
TRAMEOUT[85]=0x2D;
TRAMEOUT[86]=0x2D;
TRAMEOUT[87]=0x2D;
TRAMEOUT[88]=0x2D;
TRAMEOUT[89]=0x2D;
TRAMEOUT[90]=0x2D;
TRAMEOUT[91]=0x2D;
TRAMEOUT[92]=0x2D;
TRAMEOUT[93]=0x2D;
TRAMEOUT[94]=0x2D;
TRAMEOUT[95]=0x2D;
TRAMEOUT[96]=0x2D;
TRAMEOUT[97]=0x2D;
TRAMEOUT[98]=0x2D;
TRAMEOUT[99]=0x2D;
TRAMEOUT[100]=0x2D;
TRAMEOUT[101]=0x2D;
TRAMEOUT[102]=0x2D;
TRAMEOUT[103]=0x2D;
TRAMEOUT[104]=0x2D;
TRAMEOUT[105]=0x2D;
TRAMEOUT[106]=0x2D;
TRAMEOUT[107]=0x2D;
TRAMEOUT[108]=0x2D;
TRAMEOUT[109]=0x2D;
TRAMEOUT[110]=0x2D;
TRAMEOUT[111]=0x2D;
TRAMEOUT[112]=0x2D;
TRAMEOUT[113]=0x2D;
TRAMEOUT[114]=0x2D;
TRAMEOUT[115]=0x2D;
TRAMEOUT[116]=0x2D;
TRAMEOUT[117]=0x2D;
TRAMEOUT[118]=0x2D;
TRAMEOUT[119]=0x2D;
TRAMEOUT[120]=0x2D;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='8';
TRAMEOUT[3]='b';
TRAMEOUT[4]=0x2D;
TRAMEOUT[5]=0x2D;
TRAMEOUT[6]=0x2D;
TRAMEOUT[7]=0x2D;
TRAMEOUT[8]=0x2D;
TRAMEOUT[9]=0x2D;
TRAMEOUT[10]=0x2D;
TRAMEOUT[11]=0x2D;
TRAMEOUT[12]=0x2D;
TRAMEOUT[13]=0x2D;
TRAMEOUT[14]=0x2D;
TRAMEOUT[15]=0x2D;
TRAMEOUT[16]=0x2D;
TRAMEOUT[17]=0x2D;
TRAMEOUT[18]=0x2D;
TRAMEOUT[19]=0x2D;
TRAMEOUT[20]=0x2D;
TRAMEOUT[21]=0x2D;
TRAMEOUT[22]=0x2D;
TRAMEOUT[23]=0x2D;
TRAMEOUT[24]=0x2D;
TRAMEOUT[25]=0x2D;
TRAMEOUT[26]=0x1B;
TRAMEOUT[27]=0x5B;
TRAMEOUT[28]=0x30;
TRAMEOUT[29]=0x4B;
TRAMEOUT[30]=0x1B;
TRAMEOUT[31]=0x5B;
TRAMEOUT[32]=0x31;
TRAMEOUT[33]=0x32;
TRAMEOUT[34]=0x3B;
TRAMEOUT[35]=0x31;
TRAMEOUT[36]=0x35;
TRAMEOUT[37]=0x48;
TRAMEOUT[38]=0x1B;
TRAMEOUT[39]=0x5B;
TRAMEOUT[40]=0x30;
TRAMEOUT[41]=0x4B;
TRAMEOUT[42]=0x43;
TRAMEOUT[43]=0x6F;
TRAMEOUT[44]=0x64;
TRAMEOUT[45]=0x61;
TRAMEOUT[46]=0x67;
TRAMEOUT[47]=0x65;
TRAMEOUT[48]=0x20;
TRAMEOUT[49]=0x20;
TRAMEOUT[50]=0x20;
TRAMEOUT[51]=0x20;
TRAMEOUT[52]=0x20;
TRAMEOUT[53]=0x20;
TRAMEOUT[54]=0x3A;
TRAMEOUT[55]=0x1B;
TRAMEOUT[56]=0x5B;
TRAMEOUT[57]=0x31;
TRAMEOUT[58]=0x33;
TRAMEOUT[59]=0x3B;
TRAMEOUT[60]=0x31;
TRAMEOUT[61]=0x32;
TRAMEOUT[62]=0x48;
TRAMEOUT[63]=0x54;
TRAMEOUT[64]=0x79;
TRAMEOUT[65]=0x70;
TRAMEOUT[66]=0x65;
TRAMEOUT[67]=0x20;
TRAMEOUT[68]=0x64;
TRAMEOUT[69]=0x65;
TRAMEOUT[70]=0x20;
TRAMEOUT[71]=0x63;
TRAMEOUT[72]=0x61;
TRAMEOUT[73]=0x70;
TRAMEOUT[74]=0x74;
TRAMEOUT[75]=0x65;
TRAMEOUT[76]=0x75;
TRAMEOUT[77]=0x72;
TRAMEOUT[78]=0x3A;
TRAMEOUT[79]=0x0A;
TRAMEOUT[80]=0x08;
TRAMEOUT[81]=0x1B;
TRAMEOUT[82]=0x5B;
TRAMEOUT[83]=0x31;
TRAMEOUT[84]=0x34;
TRAMEOUT[85]=0x3B;
TRAMEOUT[86]=0x31;
TRAMEOUT[87]=0x35;
TRAMEOUT[88]=0x48;
TRAMEOUT[89]=0x1B;
TRAMEOUT[90]=0x5B;
TRAMEOUT[91]=0x30;
TRAMEOUT[92]=0x4B;
TRAMEOUT[93]=0x43;
TRAMEOUT[94]=0x6F;
TRAMEOUT[95]=0x6E;
TRAMEOUT[96]=0x73;
TRAMEOUT[97]=0x74;
TRAMEOUT[98]=0x69;
TRAMEOUT[99]=0x74;
TRAMEOUT[100]=0x75;
TRAMEOUT[101]=0x74;
TRAMEOUT[102]=0x69;
TRAMEOUT[103]=0x6F;
TRAMEOUT[104]=0x6E;
TRAMEOUT[105]=0x3A;
TRAMEOUT[106]=0x20;
TRAMEOUT[107]=0x1B;
TRAMEOUT[108]=0x5B;
TRAMEOUT[109]=0x31;
TRAMEOUT[110]=0x35;
TRAMEOUT[111]=0x3B;
TRAMEOUT[112]=0x31;
TRAMEOUT[113]=0x35;
TRAMEOUT[114]=0x48;
TRAMEOUT[115]=0x1B;
TRAMEOUT[116]=0x5B;
TRAMEOUT[117]=0x30;
TRAMEOUT[118]=0x4B;
TRAMEOUT[119]=0x53;
TRAMEOUT[120]=0x65;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='8';
TRAMEOUT[3]='c';
TRAMEOUT[4]=0x75;
TRAMEOUT[5]=0x69;
TRAMEOUT[6]=0x6C;
TRAMEOUT[7]=0x20;
TRAMEOUT[8]=0x20;
TRAMEOUT[9]=0x20;
TRAMEOUT[10]=0x20;
TRAMEOUT[11]=0x20;
TRAMEOUT[12]=0x20;
TRAMEOUT[13]=0x20;
TRAMEOUT[14]=0x3A;
TRAMEOUT[15]=0x0A;
TRAMEOUT[16]=0x08;
TRAMEOUT[17]=0x1B;
TRAMEOUT[18]=0x5B;
TRAMEOUT[19]=0x31;
TRAMEOUT[20]=0x36;
TRAMEOUT[21]=0x3B;
TRAMEOUT[22]=0x31;
TRAMEOUT[23]=0x35;
TRAMEOUT[24]=0x48;
TRAMEOUT[25]=0x1B;
TRAMEOUT[26]=0x5B;
TRAMEOUT[27]=0x30;
TRAMEOUT[28]=0x4B;
TRAMEOUT[29]=0x44;
TRAMEOUT[30]=0x69;
TRAMEOUT[31]=0x73;
TRAMEOUT[32]=0x74;
TRAMEOUT[33]=0x61;
TRAMEOUT[34]=0x6E;
TRAMEOUT[35]=0x63;
TRAMEOUT[36]=0x65;
TRAMEOUT[37]=0x20;
TRAMEOUT[38]=0x20;
TRAMEOUT[39]=0x20;
TRAMEOUT[40]=0x20;
TRAMEOUT[41]=0x3A;
TRAMEOUT[42]=0x1B;
TRAMEOUT[43]=0x5B;
TRAMEOUT[44]=0x31;
TRAMEOUT[45]=0x36;
TRAMEOUT[46]=0x3B;
TRAMEOUT[47]=0x35;
TRAMEOUT[48]=0x32;
TRAMEOUT[49]=0x48;
TRAMEOUT[50]=0x20;
TRAMEOUT[51]=0x49;
TRAMEOUT[52]=0x6E;
TRAMEOUT[53]=0x74;
TRAMEOUT[54]=0x65;
TRAMEOUT[55]=0x72;
TRAMEOUT[56]=0x76;
TRAMEOUT[57]=0x65;
TRAMEOUT[58]=0x6E;
TRAMEOUT[59]=0x74;
TRAMEOUT[60]=0x69;
TRAMEOUT[61]=0x6F;
TRAMEOUT[62]=0x6E;
TRAMEOUT[63]=0x20;
TRAMEOUT[64]=0x3A;
TRAMEOUT[65]=0x20;
TRAMEOUT[66]=0x20;
TRAMEOUT[67]=0x1B;
TRAMEOUT[68]=0x5B;
TRAMEOUT[69]=0x31;
TRAMEOUT[70]=0x35;
TRAMEOUT[71]=0x3B;
TRAMEOUT[72]=0x33;
TRAMEOUT[73]=0x35;
TRAMEOUT[74]=0x48;
TRAMEOUT[75]=0x6D;
TRAMEOUT[76]=0x42;
TRAMEOUT[77]=0x61;
TRAMEOUT[78]=0x72;
TRAMEOUT[79]=0x1B;
TRAMEOUT[80]=0x5B;
TRAMEOUT[81]=0x31;
TRAMEOUT[82]=0x36;
TRAMEOUT[83]=0x3B;
TRAMEOUT[84]=0x33;
TRAMEOUT[85]=0x35;
TRAMEOUT[86]=0x48;
TRAMEOUT[87]=0x4D;
TRAMEOUT[88]=0x65;
TRAMEOUT[89]=0x74;
TRAMEOUT[90]=0x72;
TRAMEOUT[91]=0x65;
TRAMEOUT[92]=0x73;
TRAMEOUT[93]=0x1B;
TRAMEOUT[94]=0x5B;
TRAMEOUT[95]=0x31;
TRAMEOUT[96]=0x36;
TRAMEOUT[97]=0x3B;
TRAMEOUT[98]=0x36;
TRAMEOUT[99]=0x38;
TRAMEOUT[100]=0x48;
TRAMEOUT[101]=0x4E;
TRAMEOUT[102]=0x4F;
TRAMEOUT[103]=0x4E;
TRAMEOUT[104]=0x1B;
TRAMEOUT[105]=0x5B;
TRAMEOUT[106]=0x37;
TRAMEOUT[107]=0x6D;
TRAMEOUT[108]=0x1B;
TRAMEOUT[109]=0x5B;
TRAMEOUT[110]=0x30;
TRAMEOUT[111]=0x6D;
TRAMEOUT[112]=0x1B;
TRAMEOUT[113]=0x5B;
TRAMEOUT[114]=0x36;
TRAMEOUT[115]=0x3B;
TRAMEOUT[116]=0x32;
TRAMEOUT[117]=0x39;
TRAMEOUT[118]=0x48;
TRAMEOUT[119]=0x1B;
TRAMEOUT[120]=0x5B;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (115);
TRAMEOUT[0]='#'; 
TRAMEOUT[1]='0';
TRAMEOUT[2]='8';
TRAMEOUT[3]='d';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x32;
TRAMEOUT[7]=0x33;
TRAMEOUT[8]=0x3B;
TRAMEOUT[9]=0x31;
TRAMEOUT[10]=0x48;
TRAMEOUT[11]=0x1B;
TRAMEOUT[12]=0x5B;
TRAMEOUT[13]=0x37;
TRAMEOUT[14]=0x6D;
TRAMEOUT[15]=0x41;
TRAMEOUT[16]=0x50;
TRAMEOUT[17]=0x50;
TRAMEOUT[18]=0x55;
TRAMEOUT[19]=0x59;
TRAMEOUT[20]=0x45;
TRAMEOUT[21]=0x5A;
TRAMEOUT[22]=0x1B;
TRAMEOUT[23]=0x5B;
TRAMEOUT[24]=0x30;
TRAMEOUT[25]=0x6D;
TRAMEOUT[26]=0x20;
TRAMEOUT[27]=0x20;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]=0x4F;
TRAMEOUT[30]=0x2F;
TRAMEOUT[31]=0x4E;
TRAMEOUT[32]=0x20;
TRAMEOUT[33]=0x70;
TRAMEOUT[34]=0x6F;
TRAMEOUT[35]=0x75;
TRAMEOUT[36]=0x72;
TRAMEOUT[37]=0x20;
TRAMEOUT[38]=0x61;
TRAMEOUT[39]=0x66;
TRAMEOUT[40]=0x66;
TRAMEOUT[41]=0x69;
TRAMEOUT[42]=0x63;
TRAMEOUT[43]=0x68;
TRAMEOUT[44]=0x65;
TRAMEOUT[45]=0x72;
TRAMEOUT[46]=0x20;
TRAMEOUT[47]=0x6F;
TRAMEOUT[48]=0x75;
TRAMEOUT[49]=0x69;
TRAMEOUT[50]=0x2F;
TRAMEOUT[51]=0x6E;
TRAMEOUT[52]=0x6F;
TRAMEOUT[53]=0x6E;
TRAMEOUT[54]=0x1B;
TRAMEOUT[55]=0x5B;
TRAMEOUT[56]=0x32;
TRAMEOUT[57]=0x34;
TRAMEOUT[58]=0x3B;
TRAMEOUT[59]=0x31;
TRAMEOUT[60]=0x48;
TRAMEOUT[61]=0x1B;
TRAMEOUT[62]=0x5B;
TRAMEOUT[63]=0x37;
TRAMEOUT[64]=0x6D;
TRAMEOUT[65]=0x41;
TRAMEOUT[66]=0x50;
TRAMEOUT[67]=0x50;
TRAMEOUT[68]=0x55;
TRAMEOUT[69]=0x59;
TRAMEOUT[70]=0x45;
TRAMEOUT[71]=0x5A;
TRAMEOUT[72]=0x1B;
TRAMEOUT[73]=0x5B;
TRAMEOUT[74]=0x30;
TRAMEOUT[75]=0x6D;
TRAMEOUT[76]=0x20;
TRAMEOUT[77]=0x20;
TRAMEOUT[78]=0x20;
TRAMEOUT[79]=0x41;
TRAMEOUT[80]=0x20;
TRAMEOUT[81]=0x70;
TRAMEOUT[82]=0x6F;
TRAMEOUT[83]=0x75;
TRAMEOUT[84]=0x72;
TRAMEOUT[85]=0x20;
TRAMEOUT[86]=0x75;
TRAMEOUT[87]=0x6E;
TRAMEOUT[88]=0x20;
TRAMEOUT[89]=0x61;
TRAMEOUT[90]=0x64;
TRAMEOUT[91]=0x72;
TRAMEOUT[92]=0x65;
TRAMEOUT[93]=0x73;
TRAMEOUT[94]=0x73;
TRAMEOUT[95]=0x61;
TRAMEOUT[96]=0x62;
TRAMEOUT[97]=0x6C;
TRAMEOUT[98]=0x65;
TRAMEOUT[99]=0x20;
TRAMEOUT[100]=0x65;
TRAMEOUT[101]=0x74;
TRAMEOUT[102]=0x20;
TRAMEOUT[103]=0x52;
TRAMEOUT[104]=0x20;
TRAMEOUT[105]=0x70;
TRAMEOUT[106]=0x6F;
TRAMEOUT[107]=0x75;
TRAMEOUT[108]=0x72;
TRAMEOUT[109]=0x20;
TRAMEOUT[110]=0x75;
TRAMEOUT[111]=0x6E;
TRAMEOUT[112]=0x20;
TRAMEOUT[113]=0x72;
TRAMEOUT[114]=0x65;
TRAMEOUT[115]=0x73;
TRAMEOUT[116]=0x69;
TRAMEOUT[117]=0x73;
TRAMEOUT[118]=0x74;
TRAMEOUT[119]=0x69;
TRAMEOUT[120]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (116);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='8';
TRAMEOUT[3]='e';
TRAMEOUT[4]=0x66;
TRAMEOUT[5]=0x1B;
TRAMEOUT[6]=0x5B;
TRAMEOUT[7]=0x30;
TRAMEOUT[8]=0x4B;
//TRAMEOUT[9]=0x1B;
//TRAMEOUT[10]=0x5B;
//TRAMEOUT[11]=0x39;
//TRAMEOUT[12]=0x3B;
//TRAMEOUT[13]=0x34;
//TRAMEOUT[14]=0x32;
//TRAMEOUT[15]=0x48;
//TRAMEOUT[16]=0x1B;
//TRAMEOUT[17]=0x5B;
//TRAMEOUT[18]=0x36;
//TRAMEOUT[19]=0x3B;
//TRAMEOUT[20]=0x33;
//TRAMEOUT[21]=0x32;
//TRAMEOUT[22]=0x48;
//TRAMEOUT[23]=0x1B;
//TRAMEOUT[24]='*';
j=0;
for (iii=4;iii<9;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (5);

}
else if (num == '8')
{
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='9';
TRAMEOUT[3]='a';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x32;
TRAMEOUT[7]=0x4A;
TRAMEOUT[8]=0x1B;
TRAMEOUT[9]=0x5B;
TRAMEOUT[10]=0x31;
TRAMEOUT[11]=0x3B;
TRAMEOUT[12]=0x36;
TRAMEOUT[13]=0x34;
TRAMEOUT[14]=0x48;
TRAMEOUT[15]=0x1B;
TRAMEOUT[16]=0x5B;
TRAMEOUT[17]=0x31;
TRAMEOUT[18]=0x3B;
TRAMEOUT[19]=0x33;
TRAMEOUT[20]=0x36;
TRAMEOUT[21]=0x48;
TRAMEOUT[22]=0x53;
TRAMEOUT[23]=0x69;
TRAMEOUT[24]=0x74;
TRAMEOUT[25]=0x65;
TRAMEOUT[26]=0x3A;
TRAMEOUT[27]=0x20;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]=0x1B;
TRAMEOUT[30]=0x5B;
TRAMEOUT[31]=0x37;
TRAMEOUT[32]=0x6D;
TRAMEOUT[33]=0x1B;
TRAMEOUT[34]=0x5B;
TRAMEOUT[35]=0x32;
TRAMEOUT[36]=0x3B;
TRAMEOUT[37]=0x33;
TRAMEOUT[38]=0x30;
TRAMEOUT[39]=0x48;
TRAMEOUT[40]=0x20;
TRAMEOUT[41]=0x41;
TRAMEOUT[42]=0x44;
TRAMEOUT[43]=0x45;
TRAMEOUT[44]=0x43;
TRAMEOUT[45]=0x45;
TRAMEOUT[46]=0x46;
TRAMEOUT[47]=0x20;
TRAMEOUT[48]=0x54;
TRAMEOUT[49]=0x45;
TRAMEOUT[50]=0x43;
TRAMEOUT[51]=0x48;
TRAMEOUT[52]=0x4E;
TRAMEOUT[53]=0x4F;
TRAMEOUT[54]=0x4C;
TRAMEOUT[55]=0x4F;
TRAMEOUT[56]=0x47;
TRAMEOUT[57]=0x59;
TRAMEOUT[58]=0x20;
TRAMEOUT[59]=0x1B;
TRAMEOUT[60]=0x5B;
TRAMEOUT[61]=0x30;
TRAMEOUT[62]=0x6D;
TRAMEOUT[63]=0x1B;
TRAMEOUT[64]=0x5B;
TRAMEOUT[65]=0x31;
TRAMEOUT[66]=0x3B;
TRAMEOUT[67]=0x35;
TRAMEOUT[68]=0x39;
TRAMEOUT[69]=0x48;//DATE
TRAMEOUT[70]=0x20; //J
TRAMEOUT[71]=0x20; //J
TRAMEOUT[72]=0x20; // /2F
TRAMEOUT[73]=0x20; //M
TRAMEOUT[74]=0x20; //M
TRAMEOUT[75]=0x20; // /
TRAMEOUT[76]=0x20; //A
TRAMEOUT[77]=0x20;//A
TRAMEOUT[78]=0x20;//A
TRAMEOUT[79]=0x20;//A
TRAMEOUT[80]=0x20;
TRAMEOUT[81]=0x20;
TRAMEOUT[82]=0x20; //H
TRAMEOUT[83]=0x20; //H
TRAMEOUT[84]=0x20; // :3A
TRAMEOUT[85]=0x20; //m
TRAMEOUT[86]=0x20; //m
TRAMEOUT[87]=0x1B;
TRAMEOUT[88]=0x5B;
TRAMEOUT[89]=0x34;
TRAMEOUT[90]=0x3B;
TRAMEOUT[91]=0x32;
TRAMEOUT[92]=0x37;
TRAMEOUT[93]=0x48;
TRAMEOUT[94]=0x1B;
TRAMEOUT[95]=0x5B;
TRAMEOUT[96]=0x37;
TRAMEOUT[97]=0x6D;
TRAMEOUT[98]=0x50;
TRAMEOUT[99]=0x41;
TRAMEOUT[100]=0x52;
TRAMEOUT[101]=0x41;
TRAMEOUT[102]=0x4D;
TRAMEOUT[103]=0x45;
TRAMEOUT[104]=0x54;
TRAMEOUT[105]=0x52;
TRAMEOUT[106]=0x45;
TRAMEOUT[107]=0x53;
TRAMEOUT[108]=0x20;
TRAMEOUT[109]=0x44;
TRAMEOUT[110]=0x27;
TRAMEOUT[111]=0x45;
TRAMEOUT[112]=0x58;
TRAMEOUT[113]=0x50;
TRAMEOUT[114]=0x4C;
TRAMEOUT[115]=0x4F;
TRAMEOUT[116]=0x49;
TRAMEOUT[117]=0x54;
TRAMEOUT[118]=0x41;
TRAMEOUT[119]=0x54;
TRAMEOUT[120]=0x49;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='9';
TRAMEOUT[3]='b';
TRAMEOUT[4]=0x4F;
TRAMEOUT[5]=0x4E;
TRAMEOUT[6]=0x1B;
TRAMEOUT[7]=0x5B;
TRAMEOUT[8]=0x30;
TRAMEOUT[9]=0x6D;
TRAMEOUT[10]=0x1B;
TRAMEOUT[11]=0x5B;
TRAMEOUT[12]=0x34;
TRAMEOUT[13]=0x3B;
TRAMEOUT[14]=0x32;
TRAMEOUT[15]=0x36;
TRAMEOUT[16]=0x48;
TRAMEOUT[17]=0x1B;
TRAMEOUT[18]=0x5B;
TRAMEOUT[19]=0x36;
TRAMEOUT[20]=0x3B;
TRAMEOUT[21]=0x31;
TRAMEOUT[22]=0x31;
TRAMEOUT[23]=0x48;
TRAMEOUT[24]=0x49;
TRAMEOUT[25]=0x64;
TRAMEOUT[26]=0x65;
TRAMEOUT[27]=0x6E;
TRAMEOUT[28]=0x74;
TRAMEOUT[29]=0x69;
TRAMEOUT[30]=0x66;
TRAMEOUT[31]=0x69;
TRAMEOUT[32]=0x63;
TRAMEOUT[33]=0x61;
TRAMEOUT[34]=0x74;
TRAMEOUT[35]=0x69;
TRAMEOUT[36]=0x6F;
TRAMEOUT[37]=0x6E;
TRAMEOUT[38]=0x20;
TRAMEOUT[39]=0x52;
TRAMEOUT[40]=0x61;
TRAMEOUT[41]=0x63;
TRAMEOUT[42]=0x6B;
TRAMEOUT[43]=0x20;
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x20;
TRAMEOUT[46]=0x20;
TRAMEOUT[47]=0x20;
TRAMEOUT[48]=0x20;
TRAMEOUT[49]=0x3D;
TRAMEOUT[50]=0x20;
TRAMEOUT[51]=0x20;
TRAMEOUT[52]=0x1B;
TRAMEOUT[53]=0x5B;
TRAMEOUT[54]=0x31;
TRAMEOUT[55]=0x30;
TRAMEOUT[56]=0x3B;
TRAMEOUT[57]=0x31;
TRAMEOUT[58]=0x31;
TRAMEOUT[59]=0x48;
TRAMEOUT[60]=0x54;
TRAMEOUT[61]=0x72;
TRAMEOUT[62]=0x61;
TRAMEOUT[63]=0x6E;
TRAMEOUT[64]=0x73;
TRAMEOUT[65]=0x6D;
TRAMEOUT[66]=0x69;
TRAMEOUT[67]=0x73;
TRAMEOUT[68]=0x73;
TRAMEOUT[69]=0x69;
TRAMEOUT[70]=0x6F;
TRAMEOUT[71]=0x6E;
TRAMEOUT[72]=0x20;
TRAMEOUT[73]=0x64;
TRAMEOUT[74]=0x65;
TRAMEOUT[75]=0x73;
TRAMEOUT[76]=0x20;
TRAMEOUT[77]=0x61;
TRAMEOUT[78]=0x6C;
TRAMEOUT[79]=0x61;
TRAMEOUT[80]=0x72;
TRAMEOUT[81]=0x6D;
TRAMEOUT[82]=0x65;
TRAMEOUT[83]=0x73;
TRAMEOUT[84]=0x20;
TRAMEOUT[85]=0x3D;
TRAMEOUT[86]=0x20;
TRAMEOUT[87]=0x20;
TRAMEOUT[88]=0x1B;
TRAMEOUT[89]=0x5B;
TRAMEOUT[90]=0x31;
TRAMEOUT[91]=0x32;
TRAMEOUT[92]=0x3B;
TRAMEOUT[93]=0x32;
TRAMEOUT[94]=0x36;
TRAMEOUT[95]=0x48;
TRAMEOUT[96]=0x1B;
TRAMEOUT[97]=0x5B;
TRAMEOUT[98]=0x31;
TRAMEOUT[99]=0x32;
TRAMEOUT[100]=0x3B;
TRAMEOUT[101]=0x31;
TRAMEOUT[102]=0x31;
TRAMEOUT[103]=0x48;
TRAMEOUT[104]=0x54;
TRAMEOUT[105]=0x72;
TRAMEOUT[106]=0x61;
TRAMEOUT[107]=0x6E;
TRAMEOUT[108]=0x73;
TRAMEOUT[109]=0x6D;
TRAMEOUT[110]=0x69;
TRAMEOUT[111]=0x73;
TRAMEOUT[112]=0x73;
TRAMEOUT[113]=0x69;
TRAMEOUT[114]=0x6F;
TRAMEOUT[115]=0x6E;
TRAMEOUT[116]=0x20;
TRAMEOUT[117]=0x64;
TRAMEOUT[118]=0x65;
TRAMEOUT[119]=0x73;
TRAMEOUT[120]=0x20;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='9';
TRAMEOUT[3]='c';
TRAMEOUT[4]=0x73;
TRAMEOUT[5]=0x6F;
TRAMEOUT[6]=0x72;
TRAMEOUT[7]=0x74;
TRAMEOUT[8]=0x69;
TRAMEOUT[9]=0x65;
TRAMEOUT[10]=0x73;
TRAMEOUT[11]=0x20;
TRAMEOUT[12]=0x3D;
TRAMEOUT[13]=0x20;
TRAMEOUT[14]=0x20;
TRAMEOUT[15]=0x1B;
TRAMEOUT[16]=0x5B;
TRAMEOUT[17]=0x31;
TRAMEOUT[18]=0x34;
TRAMEOUT[19]=0x3B;
TRAMEOUT[20]=0x31;
TRAMEOUT[21]=0x32;
TRAMEOUT[22]=0x48;
TRAMEOUT[23]=0x4E;
TRAMEOUT[24]=0x72;
TRAMEOUT[25]=0x2E;
TRAMEOUT[26]=0x20;
TRAMEOUT[27]=0x43;
TRAMEOUT[28]=0x4F;
TRAMEOUT[29]=0x47;
TRAMEOUT[30]=0x20;
TRAMEOUT[31]=0x3D;
TRAMEOUT[32]=0x20;
TRAMEOUT[33]=0x20;
TRAMEOUT[34]=0x1B;
TRAMEOUT[35]=0x5B;
TRAMEOUT[36]=0x31;
TRAMEOUT[37]=0x34;
TRAMEOUT[38]=0x3B;
TRAMEOUT[39]=0x34;
TRAMEOUT[40]=0x36;
TRAMEOUT[41]=0x48;
TRAMEOUT[42]=0x4E;
TRAMEOUT[43]=0x72;
TRAMEOUT[44]=0x2E;
TRAMEOUT[45]=0x2F;
TRAMEOUT[46]=0x61;
TRAMEOUT[47]=0x64;
TRAMEOUT[48]=0x72;
TRAMEOUT[49]=0x65;
TRAMEOUT[50]=0x73;
TRAMEOUT[51]=0x73;
TRAMEOUT[52]=0x65;
TRAMEOUT[53]=0x20;
TRAMEOUT[54]=0x43;
TRAMEOUT[55]=0x4D;
TRAMEOUT[56]=0x53;
TRAMEOUT[57]=0x20;
TRAMEOUT[58]=0x3D;
TRAMEOUT[59]=0x20;
TRAMEOUT[60]=0x20;
TRAMEOUT[61]=0x1B;
TRAMEOUT[62]=0x5B;
TRAMEOUT[63]=0x31;
TRAMEOUT[64]=0x38;
TRAMEOUT[65]=0x3B;
TRAMEOUT[66]=0x32;
TRAMEOUT[67]=0x31;
TRAMEOUT[68]=0x48;
TRAMEOUT[69]=0x4D;
TRAMEOUT[70]=0x69;
TRAMEOUT[71]=0x73;
TRAMEOUT[72]=0x65;
TRAMEOUT[73]=0x20;
TRAMEOUT[74]=0x40;
TRAMEOUT[75]=0x20;
TRAMEOUT[76]=0x6C;
TRAMEOUT[77]=0x27;
TRAMEOUT[78]=0x68;
TRAMEOUT[79]=0x65;
TRAMEOUT[80]=0x75;
TRAMEOUT[81]=0x72;
TRAMEOUT[82]=0x65;
TRAMEOUT[83]=0x20;
TRAMEOUT[84]=0x3D;
TRAMEOUT[85]=0x20;
TRAMEOUT[86]=0x20;
TRAMEOUT[87]=0x20;
TRAMEOUT[88]=0x20;
TRAMEOUT[89]=0x20;
TRAMEOUT[90]=0x3A;
TRAMEOUT[91]=0x20;
TRAMEOUT[92]=0x20;
TRAMEOUT[93]=0x1B;
TRAMEOUT[94]=0x5B;
TRAMEOUT[95]=0x31;
TRAMEOUT[96]=0x39;
TRAMEOUT[97]=0x3B;
TRAMEOUT[98]=0x32;
TRAMEOUT[99]=0x31;
TRAMEOUT[100]=0x48;
TRAMEOUT[101]=0x4D;
TRAMEOUT[102]=0x69;
TRAMEOUT[103]=0x73;
TRAMEOUT[104]=0x65;
TRAMEOUT[105]=0x20;
TRAMEOUT[106]=0x40;
TRAMEOUT[107]=0x20;
TRAMEOUT[108]=0x6C;
TRAMEOUT[109]=0x61;
TRAMEOUT[110]=0x20;
TRAMEOUT[111]=0x64;
TRAMEOUT[112]=0x61;
TRAMEOUT[113]=0x74;
TRAMEOUT[114]=0x65;
TRAMEOUT[115]=0x20;
TRAMEOUT[116]=0x3D;
TRAMEOUT[117]=0x20;
TRAMEOUT[118]=0x20;
TRAMEOUT[119]=0x20;
TRAMEOUT[120]=0x20;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='9';
TRAMEOUT[3]='d';
TRAMEOUT[4]=0x20;
TRAMEOUT[5]=0x2D;
TRAMEOUT[6]=0x20;
TRAMEOUT[7]=0x20;
TRAMEOUT[8]=0x20;
TRAMEOUT[9]=0x20;
TRAMEOUT[10]=0x20;
TRAMEOUT[11]=0x2D;
TRAMEOUT[12]=0x20;
TRAMEOUT[13]=0x20;
TRAMEOUT[14]=0x20;
TRAMEOUT[15]=0x20;
TRAMEOUT[16]=0x20;
TRAMEOUT[17]=0x20;
TRAMEOUT[18]=0x1B;
TRAMEOUT[19]=0x5B;
TRAMEOUT[20]=0x32;
TRAMEOUT[21]=0x31;
TRAMEOUT[22]=0x3B;
TRAMEOUT[23]=0x32;
TRAMEOUT[24]=0x32;
TRAMEOUT[25]=0x48;
TRAMEOUT[26]=0x41;
TRAMEOUT[27]=0x63;
TRAMEOUT[28]=0x63;
TRAMEOUT[29]=0x65;
TRAMEOUT[30]=0x73;
TRAMEOUT[31]=0x20;
TRAMEOUT[32]=0x55;
TRAMEOUT[33]=0x74;
TRAMEOUT[34]=0x69;
TRAMEOUT[35]=0x6C;
TRAMEOUT[36]=0x69;
TRAMEOUT[37]=0x73;
TRAMEOUT[38]=0x61;
TRAMEOUT[39]=0x74;
TRAMEOUT[40]=0x65;
TRAMEOUT[41]=0x75;
TRAMEOUT[42]=0x72;
TRAMEOUT[43]=0x20;
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x20;
TRAMEOUT[46]=0x20;
TRAMEOUT[47]=0x20;
TRAMEOUT[48]=0x3D;
TRAMEOUT[49]=0x20;
TRAMEOUT[50]=0x20;
TRAMEOUT[51]=0x1B;
TRAMEOUT[52]=0x5B;
TRAMEOUT[53]=0x32;
TRAMEOUT[54]=0x32;
TRAMEOUT[55]=0x3B;
TRAMEOUT[56]=0x32;
TRAMEOUT[57]=0x32;
TRAMEOUT[58]=0x48;
TRAMEOUT[59]=0x41;
TRAMEOUT[60]=0x63;
TRAMEOUT[61]=0x63;
TRAMEOUT[62]=0x65;
TRAMEOUT[63]=0x73;
TRAMEOUT[64]=0x20;
TRAMEOUT[65]=0x63;
TRAMEOUT[66]=0x6F;
TRAMEOUT[67]=0x6E;
TRAMEOUT[68]=0x66;
TRAMEOUT[69]=0x69;
TRAMEOUT[70]=0x67;
TRAMEOUT[71]=0x75;
TRAMEOUT[72]=0x72;
TRAMEOUT[73]=0x61;
TRAMEOUT[74]=0x74;
TRAMEOUT[75]=0x65;
TRAMEOUT[76]=0x75;
TRAMEOUT[77]=0x72;
TRAMEOUT[78]=0x20;
TRAMEOUT[79]=0x20;
TRAMEOUT[80]=0x20;
TRAMEOUT[81]=0x3D;
TRAMEOUT[82]=0x20;
TRAMEOUT[83]=0x20;
TRAMEOUT[84]=0x1B;
TRAMEOUT[85]=0x5B;
TRAMEOUT[86]=0x31;
TRAMEOUT[87]=0x36;
TRAMEOUT[88]=0x3B;
TRAMEOUT[89]=0x31;
TRAMEOUT[90]=0x31;
TRAMEOUT[91]=0x48;
TRAMEOUT[92]=0x20;
TRAMEOUT[93]=0x70;
TRAMEOUT[94]=0x61;
TRAMEOUT[95]=0x73;
TRAMEOUT[96]=0x73;
TRAMEOUT[97]=0x65;
TRAMEOUT[98]=0x72;
TRAMEOUT[99]=0x65;
TRAMEOUT[100]=0x6C;
TRAMEOUT[101]=0x6C;
TRAMEOUT[102]=0x65;
TRAMEOUT[103]=0x20;
TRAMEOUT[104]=0x3D;
TRAMEOUT[105]=0x20;
TRAMEOUT[106]=0x20;
TRAMEOUT[107]=0x1B;
TRAMEOUT[108]=0x5B;
TRAMEOUT[109]=0x31;
TRAMEOUT[110]=0x36;
TRAMEOUT[111]=0x3B;
TRAMEOUT[112]=0x34;
TRAMEOUT[113]=0x36;
TRAMEOUT[114]=0x48;
TRAMEOUT[115]=0x44;
TRAMEOUT[116]=0x4E;
TRAMEOUT[117]=0x53;
TRAMEOUT[118]=0x20;
TRAMEOUT[119]=0x3D;
TRAMEOUT[120]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (116);
}
else if (num == 'M')
{
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////// page menu //////////////////////////////////////////////////////////////////////////////////
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='0';
TRAMEOUT[3]='a';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x32;
TRAMEOUT[7]=0x4A;
TRAMEOUT[8]=0x1B;
TRAMEOUT[9]=0x5B;
TRAMEOUT[10]=0x37;
TRAMEOUT[11]=0x6D;
TRAMEOUT[12]=0x1B;
TRAMEOUT[13]=0x5B;
TRAMEOUT[14]=0x32;
TRAMEOUT[15]=0x3B;
TRAMEOUT[16]=0x33;
TRAMEOUT[17]=0x30;
TRAMEOUT[18]=0x48;
TRAMEOUT[19]=0x20;
TRAMEOUT[20]=0x20;
TRAMEOUT[21]=0x41; //NOM DU SITE
TRAMEOUT[22]=0x44;
TRAMEOUT[23]=0x45;
TRAMEOUT[24]=0x43;
TRAMEOUT[25]=0x45;
TRAMEOUT[26]=0x46;
TRAMEOUT[27]=0x20;
TRAMEOUT[28]=0x54;
TRAMEOUT[29]=0x45;
TRAMEOUT[30]=0x43;
TRAMEOUT[31]=0x48;
TRAMEOUT[32]=0x4E;
TRAMEOUT[33]=0x4F;
TRAMEOUT[34]=0x4C;
TRAMEOUT[35]=0x4F;
TRAMEOUT[36]=0x47;
TRAMEOUT[37]=0x59;
TRAMEOUT[38]=0x20;
TRAMEOUT[39]=0x1B;
TRAMEOUT[40]=0x5B;
TRAMEOUT[41]=0x30;
TRAMEOUT[42]=0x6D;
TRAMEOUT[43]=0x1B;
TRAMEOUT[44]=0x5B;
TRAMEOUT[45]=0x31;
TRAMEOUT[46]=0x3B;
TRAMEOUT[47]=0x33;
TRAMEOUT[48]=0x37;
TRAMEOUT[49]=0x48;
TRAMEOUT[50]=0x53;
TRAMEOUT[51]=0x69;
TRAMEOUT[52]=0x74;
TRAMEOUT[53]=0x65;
TRAMEOUT[54]=0x3A;
TRAMEOUT[55]=0x20;
TRAMEOUT[56]=0x20;
TRAMEOUT[57]=0x1B;
TRAMEOUT[58]=0x5B;
TRAMEOUT[59]=0x36;
TRAMEOUT[60]=0x3B;
TRAMEOUT[61]=0x31;
TRAMEOUT[62]=0x36;
TRAMEOUT[63]=0x48;
TRAMEOUT[64]=0x20; //1
TRAMEOUT[65]=0x20; // :
TRAMEOUT[66]=0x20;// 
TRAMEOUT[67]=0x20; // LISTE EVENEN..
TRAMEOUT[68]=0x20;
TRAMEOUT[69]=0x20;
TRAMEOUT[70]=0x20;
TRAMEOUT[71]=0x20;
TRAMEOUT[72]=0x20;
TRAMEOUT[73]=0x20;
TRAMEOUT[74]=0x20;
TRAMEOUT[75]=0x20;
TRAMEOUT[76]=0x20;
TRAMEOUT[77]=0x20;
TRAMEOUT[78]=0x20;
TRAMEOUT[79]=0x20;
TRAMEOUT[80]=0x20;
TRAMEOUT[81]=0x20;
TRAMEOUT[82]=0x20;
TRAMEOUT[83]=0x20;
TRAMEOUT[84]=0x20;
TRAMEOUT[85]=0x20;
TRAMEOUT[86]=0x20;
TRAMEOUT[87]=0x20;
TRAMEOUT[88]=0x20;
TRAMEOUT[89]=0x20;
TRAMEOUT[90]=0x20;
TRAMEOUT[91]=0x20;
TRAMEOUT[92]=0x20;
TRAMEOUT[93]=0x20;
TRAMEOUT[94]=0x20;
TRAMEOUT[95]=0x20;
TRAMEOUT[96]=0x20;
TRAMEOUT[97]=0x20;
TRAMEOUT[98]=0x20;
TRAMEOUT[99]=0x20;
TRAMEOUT[100]=0x20;
TRAMEOUT[101]=0x20;
TRAMEOUT[102]=0x20;
TRAMEOUT[103]=0x20;
TRAMEOUT[104]=0x1B;
TRAMEOUT[105]=0x5B;
TRAMEOUT[106]=0x35;
TRAMEOUT[107]=0x3B;
TRAMEOUT[108]=0x32;
TRAMEOUT[109]=0x48;
TRAMEOUT[110]=0x1B;
TRAMEOUT[111]=0x5B;
TRAMEOUT[112]=0x38;
TRAMEOUT[113]=0x3B;
TRAMEOUT[114]=0x31;
TRAMEOUT[115]=0x36;
TRAMEOUT[116]=0x48;
TRAMEOUT[117]=0x32;
TRAMEOUT[118]=0x3A;
TRAMEOUT[119]=0x20;
TRAMEOUT[120]=0x56;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='0';
TRAMEOUT[3]='b';
TRAMEOUT[4]=0x69;
TRAMEOUT[5]=0x73;
TRAMEOUT[6]=0x75;
TRAMEOUT[7]=0x61;
TRAMEOUT[8]=0x6C;
TRAMEOUT[9]=0x69;
TRAMEOUT[10]=0x73;
TRAMEOUT[11]=0x61;
TRAMEOUT[12]=0x74;
TRAMEOUT[13]=0x69;
TRAMEOUT[14]=0x6F;
TRAMEOUT[15]=0x6E;
TRAMEOUT[16]=0x20;
TRAMEOUT[17]=0x64;
TRAMEOUT[18]=0x65;
TRAMEOUT[19]=0x73;
TRAMEOUT[20]=0x20;
TRAMEOUT[21]=0x61;
TRAMEOUT[22]=0x6C;
TRAMEOUT[23]=0x61;
TRAMEOUT[24]=0x72;
TRAMEOUT[25]=0x6D;
TRAMEOUT[26]=0x65;
TRAMEOUT[27]=0x73;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]=0x65;
TRAMEOUT[30]=0x6E;
TRAMEOUT[31]=0x20;
TRAMEOUT[32]=0x63;
TRAMEOUT[33]=0x6F;
TRAMEOUT[34]=0x75;
TRAMEOUT[35]=0x72;
TRAMEOUT[36]=0x73;
TRAMEOUT[37]=0x1B;
TRAMEOUT[38]=0x5B;
TRAMEOUT[39]=0x37;
TRAMEOUT[40]=0x3B;
TRAMEOUT[41]=0x32;
TRAMEOUT[42]=0x48;
TRAMEOUT[43]=0x1B;
TRAMEOUT[44]=0x5B;
TRAMEOUT[45]=0x31;
TRAMEOUT[46]=0x30;
TRAMEOUT[47]=0x3B;
TRAMEOUT[48]=0x31;
TRAMEOUT[49]=0x36;
TRAMEOUT[50]=0x48;
TRAMEOUT[51]=0x33;
TRAMEOUT[52]=0x3A;
TRAMEOUT[53]=0x20;
TRAMEOUT[54]=0x56;
TRAMEOUT[55]=0x69;
TRAMEOUT[56]=0x73;
TRAMEOUT[57]=0x75;
TRAMEOUT[58]=0x61;
TRAMEOUT[59]=0x6C;
TRAMEOUT[60]=0x69;
TRAMEOUT[61]=0x73;
TRAMEOUT[62]=0x61;
TRAMEOUT[63]=0x74;
TRAMEOUT[64]=0x69;
TRAMEOUT[65]=0x6F;
TRAMEOUT[66]=0x6E;
TRAMEOUT[67]=0x20;
TRAMEOUT[68]=0x64;
TRAMEOUT[69]=0x65;
TRAMEOUT[70]=0x73;
TRAMEOUT[71]=0x20;
TRAMEOUT[72]=0x64;
TRAMEOUT[73]=0x6F;
TRAMEOUT[74]=0x6E;
TRAMEOUT[75]=0x6E;
TRAMEOUT[76]=0x65;
TRAMEOUT[77]=0x65;
TRAMEOUT[78]=0x73;
TRAMEOUT[79]=0x1B;
TRAMEOUT[80]=0x5B;
TRAMEOUT[81]=0x31;
TRAMEOUT[82]=0x31;
TRAMEOUT[83]=0x3B;
TRAMEOUT[84]=0x32;
TRAMEOUT[85]=0x48;
TRAMEOUT[86]=0x1B;
TRAMEOUT[87]=0x5B;
TRAMEOUT[88]=0x31;
TRAMEOUT[89]=0x32;
TRAMEOUT[90]=0x3B;
TRAMEOUT[91]=0x31;
TRAMEOUT[92]=0x36;
TRAMEOUT[93]=0x48;
TRAMEOUT[94]=0x35;
TRAMEOUT[95]=0x3A;
TRAMEOUT[96]=0x20;
TRAMEOUT[97]=0x47;
TRAMEOUT[98]=0x72;
TRAMEOUT[99]=0x6F;
TRAMEOUT[100]=0x75;
TRAMEOUT[101]=0x70;
TRAMEOUT[102]=0x65;
TRAMEOUT[103]=0x20;
TRAMEOUT[104]=0x64;
TRAMEOUT[105]=0x65;
TRAMEOUT[106]=0x20;
TRAMEOUT[107]=0x70;
TRAMEOUT[108]=0x72;
TRAMEOUT[109]=0x65;
TRAMEOUT[110]=0x73;
TRAMEOUT[111]=0x73;
TRAMEOUT[112]=0x75;
TRAMEOUT[113]=0x72;
TRAMEOUT[114]=0x69;
TRAMEOUT[115]=0x73;
TRAMEOUT[116]=0x61;
TRAMEOUT[117]=0x74;
TRAMEOUT[118]=0x69;
TRAMEOUT[119]=0x6F;
TRAMEOUT[120]=0x6E;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='0';
TRAMEOUT[3]='c';
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x31;
TRAMEOUT[7]=0x33;
TRAMEOUT[8]=0x3B;
TRAMEOUT[9]=0x32;
TRAMEOUT[10]=0x48;
TRAMEOUT[11]=0x1B;
TRAMEOUT[12]=0x5B;
TRAMEOUT[13]=0x31;
TRAMEOUT[14]=0x37;
TRAMEOUT[15]=0x3B;
TRAMEOUT[16]=0x32;
TRAMEOUT[17]=0x48;
TRAMEOUT[18]=0x1B;
TRAMEOUT[19]=0x5B;
TRAMEOUT[20]=0x31;
TRAMEOUT[21]=0x38;
TRAMEOUT[22]=0x3B;
TRAMEOUT[23]=0x31;
TRAMEOUT[24]=0x36;
TRAMEOUT[25]=0x48;
TRAMEOUT[26]=0x39;
TRAMEOUT[27]=0x3A;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]=0x41; //ADECEF
TRAMEOUT[30]=0x44;
TRAMEOUT[31]=0x45;
TRAMEOUT[32]=0x43;
TRAMEOUT[33]=0x45;
TRAMEOUT[34]=0x46;
TRAMEOUT[35]=0x20;
TRAMEOUT[36]=0x20;
TRAMEOUT[37]=0x20;
TRAMEOUT[38]=0x20;
TRAMEOUT[39]=0x20;
TRAMEOUT[40]=0x20;
TRAMEOUT[41]=0x20;
TRAMEOUT[42]=0x20;
TRAMEOUT[43]=0x20;
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x20;
TRAMEOUT[46]=0x20;
TRAMEOUT[47]=0x20;
TRAMEOUT[48]=0x20;
TRAMEOUT[49]=0x20;
TRAMEOUT[50]=0x20;
TRAMEOUT[51]=0x20;
TRAMEOUT[52]=0x20;
TRAMEOUT[53]=0x20;
TRAMEOUT[54]=0x20;
TRAMEOUT[55]=0x20;
TRAMEOUT[56]=0x20;
TRAMEOUT[57]=0x20; 
TRAMEOUT[58]=0x1B;
TRAMEOUT[59]=0x5B;
TRAMEOUT[60]=0x32;
TRAMEOUT[61]=0x31;
TRAMEOUT[62]=0x3B;
TRAMEOUT[63]=0x32;
TRAMEOUT[64]=0x48;
TRAMEOUT[65]=0x1B;
TRAMEOUT[66]=0x5B;
TRAMEOUT[67]=0x31;
TRAMEOUT[68]=0x34;
TRAMEOUT[69]=0x3B;
TRAMEOUT[70]=0x31;
TRAMEOUT[71]=0x36;
TRAMEOUT[72]=0x48;
TRAMEOUT[73]=0x36;
TRAMEOUT[74]=0x3A;
TRAMEOUT[75]=0x20;
TRAMEOUT[76]=0x43;
TRAMEOUT[77]=0x6F;
TRAMEOUT[78]=0x6E;
TRAMEOUT[79]=0x66;
TRAMEOUT[80]=0x69;
TRAMEOUT[81]=0x67;
TRAMEOUT[82]=0x75;
TRAMEOUT[83]=0x72;
TRAMEOUT[84]=0x61;
TRAMEOUT[85]=0x74;
TRAMEOUT[86]=0x69;
TRAMEOUT[87]=0x6F;
TRAMEOUT[88]=0x6E;
TRAMEOUT[89]=0x20;
TRAMEOUT[90]=0x64;
TRAMEOUT[91]=0x27;
TRAMEOUT[92]=0x75;
TRAMEOUT[93]=0x6E;
TRAMEOUT[94]=0x20;
TRAMEOUT[95]=0x63;
TRAMEOUT[96]=0x61;
TRAMEOUT[97]=0x62;
TRAMEOUT[98]=0x6C;
TRAMEOUT[99]=0x65;
TRAMEOUT[100]=0x1B;
TRAMEOUT[101]=0x5B;
TRAMEOUT[102]=0x31;
TRAMEOUT[103]=0x35;
TRAMEOUT[104]=0x3B;
TRAMEOUT[105]=0x32;
TRAMEOUT[106]=0x48;
TRAMEOUT[107]=0x1B;
TRAMEOUT[108]=0x5B;
TRAMEOUT[109]=0x31;
TRAMEOUT[110]=0x36;
TRAMEOUT[111]=0x3B;
TRAMEOUT[112]=0x31;
TRAMEOUT[113]=0x36;
TRAMEOUT[114]=0x48;
TRAMEOUT[115]=0x38;
TRAMEOUT[116]=0x3A;
TRAMEOUT[117]=0x20;
TRAMEOUT[118]=0x50;
TRAMEOUT[119]=0x61;
TRAMEOUT[120]=0x72;
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117);
TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='0';
TRAMEOUT[3]='d';
TRAMEOUT[4]=0x61;
TRAMEOUT[5]=0x6D;
TRAMEOUT[6]=0x65;
TRAMEOUT[7]=0x74;
TRAMEOUT[8]=0x72;
TRAMEOUT[9]=0x65;
TRAMEOUT[10]=0x73;
TRAMEOUT[11]=0x20;
TRAMEOUT[12]=0x64;
TRAMEOUT[13]=0x27;
TRAMEOUT[14]=0x65;
TRAMEOUT[15]=0x78;
TRAMEOUT[16]=0x70;
TRAMEOUT[17]=0x6C;
TRAMEOUT[18]=0x6F;
TRAMEOUT[19]=0x69;
TRAMEOUT[20]=0x74;
TRAMEOUT[21]=0x61;
TRAMEOUT[22]=0x74;
TRAMEOUT[23]=0x69;
TRAMEOUT[24]=0x6F;
TRAMEOUT[25]=0x6E;
TRAMEOUT[26]=0x20;
TRAMEOUT[27]=0x64;
TRAMEOUT[28]=0x75;
TRAMEOUT[29]=0x20;
TRAMEOUT[30]=0x73;
TRAMEOUT[31]=0x79;
TRAMEOUT[32]=0x73;
TRAMEOUT[33]=0x74;
TRAMEOUT[34]=0x65;
TRAMEOUT[35]=0x6D;
TRAMEOUT[36]=0x65;
TRAMEOUT[37]=0x1B;
TRAMEOUT[38]=0x5B;
TRAMEOUT[39]=0x32;
TRAMEOUT[40]=0x33;
TRAMEOUT[41]=0x3B;
TRAMEOUT[42]=0x31;
TRAMEOUT[43]=0x36;
TRAMEOUT[44]=0x48;
TRAMEOUT[45]=0x20;
TRAMEOUT[46]=0x1B;
TRAMEOUT[47]=0x5B;
TRAMEOUT[48]=0x37;
TRAMEOUT[49]=0x6D;
TRAMEOUT[50]=0x43;
TRAMEOUT[51]=0x68;
TRAMEOUT[52]=0x69;
TRAMEOUT[53]=0x66;
TRAMEOUT[54]=0x66;
TRAMEOUT[55]=0x72;
TRAMEOUT[56]=0x65;
TRAMEOUT[57]=0x73;
TRAMEOUT[58]=0x1B;
TRAMEOUT[59]=0x5B;
TRAMEOUT[60]=0x30;
TRAMEOUT[61]=0x6D;
TRAMEOUT[62]=0x20;
TRAMEOUT[63]=0x3D;
TRAMEOUT[64]=0x20;
TRAMEOUT[65]=0x63;
TRAMEOUT[66]=0x68;
TRAMEOUT[67]=0x6F;
TRAMEOUT[68]=0x69;
TRAMEOUT[69]=0x78;
TRAMEOUT[70]=0x20;
TRAMEOUT[71]=0x6D;
TRAMEOUT[72]=0x65;
TRAMEOUT[73]=0x6E;
TRAMEOUT[74]=0x75;
TRAMEOUT[75]=0x20;
TRAMEOUT[76]=0x20;
TRAMEOUT[77]=0x20;
TRAMEOUT[78]=0x20;
TRAMEOUT[79]=0x20;
TRAMEOUT[80]=0x20;
TRAMEOUT[81]=0x1B;
TRAMEOUT[82]=0x5B;
TRAMEOUT[83]=0x37;
TRAMEOUT[84]=0x6D;
TRAMEOUT[85]=0x45;
TRAMEOUT[86]=0x53;
TRAMEOUT[87]=0x43;
TRAMEOUT[88]=0x1B;
TRAMEOUT[89]=0x5B;
TRAMEOUT[90]=0x30;
TRAMEOUT[91]=0x6D;
TRAMEOUT[92]=0x20;
TRAMEOUT[93]=0x3D;
TRAMEOUT[94]=0x20;
TRAMEOUT[95]=0x73;
TRAMEOUT[96]=0x6F;
TRAMEOUT[97]=0x72;
TRAMEOUT[98]=0x74;
TRAMEOUT[99]=0x69;
TRAMEOUT[100]=0x65;
TRAMEOUT[101]=0x1B;
TRAMEOUT[102]=0x5B;
TRAMEOUT[103]=0x30;
TRAMEOUT[104]=0x4B;
TRAMEOUT[105]=0x1B;
TRAMEOUT[106]=0x5B;
TRAMEOUT[107]=0x31;
TRAMEOUT[108]=0x38;
TRAMEOUT[109]=0x3B;
TRAMEOUT[110]=0x35;
TRAMEOUT[111]=0x35;
TRAMEOUT[112]=0x48;
TRAMEOUT[113]=0x1B;
TRAMEOUT[114]=0x5B;
TRAMEOUT[115]=0x31;
TRAMEOUT[116]=0x3B;
TRAMEOUT[117]=0x35;
TRAMEOUT[118]=0x39;
TRAMEOUT[119]=0x48; //DATE 
TRAMEOUT[120]=0x20; //J
TRAMEOUT[121]='*';
j=0;
for (iii=4;iii<122;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (117); 

//  HEURE EVENTUELLE

TRAMEOUT[0]='#';
TRAMEOUT[1]='0';
TRAMEOUT[2]='0';
TRAMEOUT[3]='e';
TRAMEOUT[4]=0x20; //J
TRAMEOUT[5]=0x20; // /
TRAMEOUT[6]=0x20;//M
TRAMEOUT[7]=0x20;//M
TRAMEOUT[8]=0x20; // /  2F
TRAMEOUT[9]=0x20;// A
TRAMEOUT[10]=0x20; //A
TRAMEOUT[11]=0x20; //A
TRAMEOUT[12]=0x20; //A
TRAMEOUT[13]=0x20;  
TRAMEOUT[14]=0x20;
TRAMEOUT[15]=0x20; //H
TRAMEOUT[16]=0x20; //H
TRAMEOUT[17]=0x20;//3A :
TRAMEOUT[18]=0x20;//m
TRAMEOUT[19]=0x20;//m
TRAMEOUT[20]='*';

j=0;
for (iii=4;iii<21;iii++){
TRAMEOUT[j]=TRAMEOUT[iii];
j++;
}
TRAMEENVOIRS232DONNEES (16);
}

}






///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////// FONCTION MINITEL///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void minitel (void)           /////////////////fonction principale du minitel
{
//BYTE ETAT;
repet =0;                    
decal=0;
while (STOPMINITEL!=0xFF){
	
switch(ETAT1)
		{
	


			case I:  /////page mots de passe

                          if (repet ==0)     ///la boucle permit d'envoyer la page une fois seulement 
		{					max_decalage=0;
 							//acc_u[0]='c';
 							//acc_u[1]='c';
 							//acc_u[2]='c';
 							//acc_u[3]='c';

             				page ('I');       /// ENVOIE DE LA PAGE 
                            nom_site();
                            repet++;
		}
							accordUSEROUADMI=verifMP();	
     						if ((accordUSEROUADMI==1) ||(accordUSEROUADMI==2))      ///// verification du mots de passe ADMIN OU USER
                            {
                            ETAT1 = M; ///aller vers la page menu

                             }
                            else
                             {
                            repet=0;
                              ETAT1 = I; ////        rester sur la meme page 
                              
                              }    
                             break;
            case M:

               				page ('M');  ///////////envoie de la page "menu principal"
                            nom_site();
							TRAMERECEPTIONRS232DONNEES (1);
							if (STOPMINITEL==0xFF)
							goto fin_minitel;
                            repet=0;
                            capt_exist=1;
							if (TRAMEIN[0]=='1')  // TOUCHE 1
								{
								//ETAT1 = A;    ///////////entrer dans le menu 1
                                
								}
							else if (TRAMEIN[0]=='2') {           //////////entrer dans la page menu 2
								ETAT1 = B;}
							else if (TRAMEIN[0]=='3')  {ETAT1 = C;} //////////entrer dans la page menu 3
							else if (TRAMEIN[0]=='5') {ETAT1 = D;} //////////entrer dans la page menu 5
                            else if (TRAMEIN[0]=='6')  {ETAT1 = E;} //////////entrer dans la page menu 6.1
						
                            
							else  if (TRAMEIN[0]=='8') {ETAT1 = K;} //////////entrer dans la page menu 8
							else  if (TRAMEIN[0]=='9') {ETAT1 = L;} //////////entrer dans la page menu 8
						break;
            case A:                         ////////////menu 1
                   	if (repet ==0){
							page ('1');      //// la boucle permit d'envoyer la page une fois seulement 
							nom_site();
							npage='1';       ///// num�ro de la page
 							nchamps=1;		 ////num�ro de champs d'�criture 
							max_champ=10;    //// le nombre maximal de champs d'�criture dans une page
							
                            }
                            repet++;
					 gestion_menu1();

                          break;      
            case B:                      /////////////////menu2
                     	if (repet ==0){
							page ('2');
							nom_site();
							npage='2';
							nchamps=1;
                            }
                            repet++;
					 gestion_menu2();
break;
            case C:						//////////////menu3
	if (repet ==0){
							page ('3');
							nom_site();
							npage='3';
							nchamps=0;
                            }
                            repet++;
                          gestion_menu3();
					 
break;

            case D:						///////////menu 5
	if (repet ==0){
							page ('5');
							nom_site();
							npage='5';
							nchamps=0;
                            }
                            repet++;
					 gestion_menu5();
             break;
      case E:							////////menu6.1
	if (repet ==0){
							page ('6');
							nom_site();
							npage='6';
							nchamps=0;
                            }
                            repet++;
							gestion_menu6 ();                           ///////////////gestion de la premi�re page du menu6 
					 		TRAMERECEPTIONminitel();
							if (STOPMINITEL==0xFF)
							goto fin_minitel;	
                            if (TRAMEIN[0]==0x17 && TRAMEIN[1]==0x41 ) ////////////////validation et retour vers le menu principal
                             {
                             
                             ETAT1 = F;        //////////si c'est on a appui sur "entrer" on passe a la deuxi�me page du menu 6. 
         					repet =0;
                         
                             }
           				   if ( TRAMEIN[0]==0x17 && TRAMEIN[1] == 0x46 )   ////////////////echape et  revenir au menu principale 
                              {
                              repet =0;
                              ETAT1=M;
                              }
             break;
      case F:							//////menu6.2
	if (repet ==0){
							page ('a');
							npage='a';
							nom_site();
							max_champ=5;
							nchamps=1;
							decal=0;
                            position_init(npage, nchamps);
							envoi_cursur();
                            }
                                             
                            repet++;
					 gestion_menu6a ();                        ////////gestion de la deuxi�me page du menu 6
					
				                                 /////// on passe a l'�tat G "la troisi�me page du menu 6
             break;
      case G: 
	if (repet ==0){						//////menu6.3
							page ('b');
							//nom_site(); 
							npage='b';
                            decal=0;
							max_champ=6;
							nchamps=1;
                            position_init(npage, nchamps);
							envoi_cursur();
                            }
                            repet++;
					 gestion_menu6b (); 
             break;
      case K:
	if (repet ==0){						/////menu8
							page ('8');
							max_champ=11;
							nom_site();
							npage='8';
							nchamps=0;

                            }
                            repet++;
					gestion_menu8(); 
                      
             break;
		case L: //RAZ CMS
		ETAT1 = M;
		//TEMPO(2);
			TRAMEOUT[0]=0x1B;
				TRAMEOUT[1]=0x5B;
				TRAMEOUT[2]=0x32;
				TRAMEOUT[3]=0x4A;
				TRAMEOUT[4]=0x1B;
				TRAMEOUT[5]=0x5B;
				TRAMEOUT[6]=0x31;
				TRAMEOUT[7]=0x32;
				TRAMEOUT[8]=0x3B;
				TRAMEOUT[9]=0x31;
				TRAMEOUT[10]=0x33;
				TRAMEOUT[11]=0x48;
				TRAMEOUT[12]=0x20;
				TRAMEOUT[13]='C';
				TRAMEOUT[14]='O';
				TRAMEOUT[15]='D';
				TRAMEOUT[16]='E';
				TRAMEOUT[17]='?';
				TRAMEOUT[18]=' ';
				TRAMEOUT[19]=' ';
				TRAMEOUT[20]=' ';
				TRAMEOUT[21]=' ';
				TRAMEOUT[22]=' ';
				TRAMEENVOIRS232DONNEES (23);
	TRAMERECEPTIONminitel();
		if (TRAMEIN[0]=='1')
		{	
		TRAMERECEPTIONminitel();
		}
		else
		{
		goto dontraz;
		}
		if (TRAMEIN[0]=='9')
		{
			TRAMERECEPTIONminitel();
		}
		else
		{
		goto dontraz;
		}
		if (TRAMEIN[0]=='9')
		{
			TRAMERECEPTIONminitel();
		}
		else
		{
		goto dontraz;
		}

		if (STOPMINITEL==0xFF)
		goto fin_minitel;

		if (TRAMEIN[0]=='8')
		{

		if (STOPMINITEL==0xFF)
		goto fin_minitel;
				TRAMEOUT[0]=0x1B;
				TRAMEOUT[1]=0x5B;
				TRAMEOUT[2]=0x32;
				TRAMEOUT[3]=0x4A;
				TRAMEOUT[4]=0x1B;
				TRAMEOUT[5]=0x5B;
				TRAMEOUT[6]=0x31;
				TRAMEOUT[7]=0x32;
				TRAMEOUT[8]=0x3B;
				TRAMEOUT[9]=0x31;
				TRAMEOUT[10]=0x33;
				TRAMEOUT[11]=0x48;
				TRAMEOUT[12]=0x20;
				TRAMEOUT[13]='C';
				TRAMEOUT[14]='M';
				TRAMEOUT[15]='S';
				TRAMEOUT[16]=' ';
				TRAMEOUT[17]='R';
				TRAMEOUT[18]='E';
				TRAMEOUT[19]='B';
				TRAMEOUT[20]='O';
				TRAMEOUT[21]='O';
				TRAMEOUT[22]='T';
				
				TRAMEOUT[23]=' ';

				TRAMEOUT[24]='V';
				TRAMEOUT[25]='E';
				TRAMEOUT[26]='U';
				TRAMEOUT[27]='I';
				TRAMEOUT[28]='L';
				TRAMEOUT[29]='L';
				TRAMEOUT[30]='E';
				TRAMEOUT[31]='Z';
				TRAMEOUT[32]=' ';
				TRAMEOUT[33]='C';
				TRAMEOUT[34]='O';
				TRAMEOUT[35]='U';
				TRAMEOUT[36]='P'; 
				TRAMEOUT[37]='E';
				TRAMEOUT[38]='R';
				TRAMEENVOIRS232DONNEES (39);	
				TEMPO(2);
	
				RAZCMS=1; //RAZ CMS
		}	

	if (TRAMEIN[0]=='9') //MODE ESPION EEPROM
		{
		
mouchardnext:
		TRAMERECEPTIONminitel();	
		if (TRAMEIN[0]=='q')
		goto dontraz;	
		if ((TRAMEIN[0]<'1')||(TRAMEIN[0]>'5'))
		TRAMEIN[0]='1';
	
		MOUCHARD(TRAMEIN[0]);
		if (STOPMINITEL==0xFF)
		goto fin_minitel;
		//TRAMERECEPTIONminitel();
		//if (TRAMEIN[0]=='c') //ON CONTINU
		goto mouchardnext;
		//if (STOPMINITEL==0xFF)
		//goto fin_minitel;

		}

		if (TRAMEIN[0]=='7') //MODE ESPION EEPROM
		{
mouchard2next:		
		DEMANDE_INFOS_CAPTEURS[0]='#';
		TRAMERECEPTIONminitel();
	
		if (TRAMEIN[0]=='q')
		goto dontraz;
		if ((TRAMEIN[0]>'1')||(TRAMEIN[0]<'0'))
		TRAMEIN[0]='0';
		
			

		DEMANDE_INFOS_CAPTEURS[1]=TRAMEIN[0];
		if (STOPMINITEL==0xFF)
		goto fin_minitel;

		TRAMERECEPTIONminitel();
		if ((TRAMEIN[0]<'0')||(TRAMEIN[0]>'9'))
		TRAMEIN[0]='1';

		DEMANDE_INFOS_CAPTEURS[2]=TRAMEIN[0];
		if (STOPMINITEL==0xFF)
		goto fin_minitel;

		TRAMERECEPTIONminitel();
		if ((TRAMEIN[0]<'0')||(TRAMEIN[0]>'9'))
		TRAMEIN[0]='1';

		DEMANDE_INFOS_CAPTEURS[3]=TRAMEIN[0];
		if (STOPMINITEL==0xFF)
		goto fin_minitel;

		MOUCHARD2();
		

		//TRAMERECEPTIONminitel();
		//if (TRAMEIN[0]=='c') //ON CONTINU
		goto mouchard2next;
		//if (STOPMINITEL==0xFF)
		//goto fin_minitel;
		}




dontraz:
		TEMPO(1);
		break;
		




}
fin_minitel:
		if (STOPMINITEL==0xFF) //ARRET
				{
				repet=0;
				ETAT1=I;
				break;
				
				}




}

}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////// VERIFICATION DU MOT DE PASSE///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


int verifMP( void )    ///////v�rification du mots de passe

{

int k;
char erreurMPP;
erreurMPP=0;

DEBMP:
k=0;
//TRAMERECEPTIONRS232DONNEES (2);
do
{
  

   TRAMERECEPTIONRS232DONNEES (1); //ON ATTEND LE CARACTERE

// ERREUR INCONNUE on a deux ** suite a 1770 ???
			   if (TRAMEIN[0]==0x17) 
			  	{
				erreurMPP=1; 	 	 
				goto DEBMP;
				}
			
			
			    if ((TRAMEIN[0]==0x70) && (erreurMPP==1))
			   	{
				erreurMPP=0; 	 	 
				goto DEBMP;
				}	
// ERREUR INCONNUE on a deux ** suite a 1770 ???



     tableau1[k] = TRAMEIN[0];
    
    TRAMEOUT[0]='*';
    
   
   TRAMEENVOIRS232DONNEES (1); //ON ENVOIT '*'
   k++;




}while ( k<4);
TRAMERECEPTIONRS232DONNEES (2); //VALIDATION


accord = 0; //FAUX
if (tableau1[1]== acc_u[1] && tableau1[2]== acc_u[2] && tableau1[3]== acc_u[3] && tableau1[0]== acc_u[0] && TRAMEIN[0]== 0x17 && TRAMEIN[1]== 0x41) 
{
accord =2; //UTILISATEUR
} 






if (tableau1[1]== acc_conf[1] && tableau1[2]== acc_conf[2] && tableau1[3]== acc_conf[3] && tableau1[0]== acc_conf[0] && TRAMEIN[0]== 0x17 && TRAMEIN[1]== 0x41) 
{
accord =1; //ADMINISTRATEUR
} 

return accord ; 
}
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////fonction d�calage a droit/////////////////////////////////////////////////////////////

void droite (void)
{
				if (decal < max_decalage)
                {
                	 if ((npage == '8' && (nchamps == 4 || nchamps == 5) && (decal ==1 || decal ==3) ) || (npage == '8' && (nchamps == 10 || nchamps == 11) && (decal ==2 || decal ==5 || decal ==8) ))
							 			{
								 
								 			pos_init_cursur [0]= pos_init_cursur [0]+ 1;       ////////////// pour �viter de modifier le trait d'union de la date et les de point dans la date et le point dans la passerelle et le dns
                							if (pos_init_cursur [0]==0x3A){
                       							pos_init_cursur [0]= 0x30;
                     							pos_init_cursur [1]=pos_init_cursur [1] + 1;
												envoi_cursur(); 
							            	}
										}
            decal++;
		  		pos_init_cursur [0]= pos_init_cursur [0]+ 1;

                    if (pos_init_cursur [0]==0x3A){
                       pos_init_cursur [0]= 0x30;
                       pos_init_cursur [1]=pos_init_cursur [1] + 1;
                     }
                     
          		envoi_cursur();
 
                }    
                    
                    if (decal >= max_decalage )
                         {
                        
                     if (npage != 'a' && npage != '3' && nchamps< max_champ)
				     {
                     decal =0;  ///////////modif
                     tabulation ();
}
                      } // on passe au champs d'�criture suivant (champs1, champs2, champs3, champs4 sauf pour le menu 6.2 (a)et le menu3


                                                 }



  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////fonction tabulation/////////////////////////////////////////////////////////////

void tabulation (void)
{

if (nchamps < max_champ){
nchamps++;
position_init(npage, nchamps);

envoi_cursur(); 
}


}


  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////d�calage a gauche ///////////////////////////////////////////////////////////// 
 void gauche (void)

{		  
        if (decal>0  ){    //////////modif
		  pos_init_cursur [0]= pos_init_cursur [0]- 1;

                    if (pos_init_cursur [0]==0x2F){
                       pos_init_cursur [0]= 0x39;
                       pos_init_cursur [1]=pos_init_cursur [1] - 1;
                     }
                      
					envoi_cursur(); 
                     
                    decal--;
                  	 if ((npage == '8' && (nchamps == 4 || nchamps == 5) && (decal ==1 || decal ==3) ) || (npage == '8' && (nchamps == 10 || nchamps == 11) && (decal ==2 || decal ==5 || decal ==8) ))
							 			{
								 
								 			pos_init_cursur [0]= pos_init_cursur [0]- 1;
  											if (pos_init_cursur [0]==0x2F)
											{
                       						pos_init_cursur [0]= 0x39;
                      						pos_init_cursur [1]=pos_init_cursur [1] - 1;
                                            }
                      						envoi_cursur(); 
										}

                     
                          } 
         
} 
 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////validation de la saisie ///////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void saisie (void)
{

TRAMEOUT[0]=TRAMEIN[0];


     if (decal <= max_decalage)
         {
  
 			 pos_init_cursur [0]= pos_init_cursur [0]+ 1;
                if (pos_init_cursur [0]==0x3A){
                       pos_init_cursur [0]= 0x30;
                       pos_init_cursur [1]=pos_init_cursur [1] + 1;
		
                     }
    				
					tableau[decal] =TRAMEIN [0]; //sauveguarder les modification du champs de donn�es ensuite il faut appel� une retine qui enregistre les modification dans l'eeprom
       				enregistrer_donnees();
                    TRAMEENVOIRS232DONNEES (1);
					envoi_cursur(); 
                    decal++;
         }           
  if (decal >= max_decalage)
		{
				pos_init_cursur [1]= pos_init_cursur [1];
				pos_init_cursur [0]= pos_init_cursur [0];
                    envoi_cursur(); 

                    TRAMEENVOIRS232DONNEES (8);
		}



		}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////// fonction haut pour aller sur le champs d'�criture pr�cedent//////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void haut (void)
{
if (nchamps > 1){
nchamps--;
position_init(npage, nchamps);

      envoi_cursur();                

}

}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////envoie des coordonn�es du cursur //////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void envoi_cursur (void)
{

TRAMEOUT[0]=0x1B;
                     TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur avec d�calage a droite, EXEMPLE  1B 5B 34 3B 34 30 48 DEVIEN 1B 5B 34 3B 34 31 48
                     TRAMEOUT[2]=pos_init_cursur [3];
                     TRAMEOUT[3]=pos_init_cursur [2];
                     TRAMEOUT[4]=0x3B;
                     TRAMEOUT[5]=pos_init_cursur [1];
                     TRAMEOUT[6]=pos_init_cursur [0];
                     TRAMEOUT[7]=0x48;

                    TRAMEENVOIRS232DONNEES (8);

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////gestion de cursur /////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gestion_page (void)
{


                                               /////////////// reception

                           TRAMERECEPTIONminitel();
						if (STOPMINITEL==0xFF)
						goto fin_gestion;	

						//	if ((nchamps==2) && (npage=='b'))
						//	CHG_CODAGE=0; //REINIT POUR 6B 






						    if (npage=='b' && nchamps==2 &&((TRAMEIN[0]!='A' || TRAMEIN[0]!='a') || (TRAMEIN[0]=='R' || TRAMEIN[0]=='r')))
							{
								if (VOIECOD[3]=='0' && VOIECOD[4]=='0' )       ///////////////////////////// lundi 20/20/2017
                                {
               						goto capt;
								}
								else {
								 if ((TRAMEIN[0]=='A' || TRAMEIN[0]=='a') && (TYPEDECARTE()=='E'))
								{
									type_capteur[0]='T';
									type_capteur[1]='P';
									type_capteur[2]=0X20;
									type_capteur[3]='A';
									type_capteur[4]='D';
									type_capteur[5]='R';
									type_capteur[6]='E';
									type_capteur[7]='S';
									type_capteur[8]='S';
									type_capteur[9]='A';
									type_capteur[10]='B';
									type_capteur[11]='L';
									type_capteur[12]='E';
									identifiant=1;	
									enregistrer_donnees();
																
								}
								else if ((TRAMEIN[0]=='R' || TRAMEIN[0]=='r')&&(TYPEDECARTE()=='M')) //TEST DU FLAG TYPEDECARTE
								{
									type_capteur[0]='T';
									type_capteur[1]='P';
									type_capteur[2]=0X20;
									type_capteur[3]='R';
									type_capteur[4]='E';
									type_capteur[5]='S';
									type_capteur[6]='I';
									type_capteur[7]='S';
									type_capteur[8]='T';
									type_capteur[9]='I';
									type_capteur[10]='F';
									type_capteur[11]=0X20;
									type_capteur[12]=0X20;
									identifiant=2;		
									enregistrer_donnees();
							}
								}
capt:
									position_init(npage, nchamps);
									envoi_cursur();
									TRAMEOUT[0]=type_capteur[0];
									TRAMEOUT[1]=type_capteur[1];
									TRAMEOUT[2]=type_capteur[2];
									TRAMEOUT[3]=type_capteur[3];
									TRAMEOUT[4]=type_capteur[4];
									TRAMEOUT[5]=type_capteur[5];
									TRAMEOUT[6]=type_capteur[6];
									TRAMEOUT[7]=type_capteur[7];
									TRAMEOUT[8]=type_capteur[8];
									TRAMEOUT[9]=type_capteur[9];
									TRAMEOUT[10]=type_capteur[10];
									TRAMEOUT[11]=type_capteur[11];
									TRAMEOUT[12]=type_capteur[12];
									TRAMEENVOIRS232DONNEES (13);
									position_init(npage, nchamps);
									envoi_cursur();
							}		

						 //AUTORISATION MODIF PARAM if (npage=='8' && (nchamps==2 || nchamps==3) || (npage=='b' && nchamps==6  )) //Si page 8 et champ 2 ou 3 Identitification du Rack et transmission des alarmes ou page 6b champ 6 
						   if  (npage=='b' && nchamps==6  ) //page 6b champ 6 // SANS AUTORISATION


								{
                         		 if ( TRAMEIN[0]=='O' || TRAMEIN[0]=='o' )   // OUI
                              			{
                            
										position_init(npage, nchamps);
										envoi_cursur();
                                       
                                        TRAMEOUT[0]='O';
                                        TRAMEOUT[1]='U';
                                        TRAMEOUT[2]='I';
										identifiant=1; // POUR DIRE OUI OU NON
										enregistrer_donnees();
                                        TRAMEENVOIRS232DONNEES (3);
                            			 }  
		    						if ( TRAMEIN[0]=='N' || TRAMEIN[0]=='n' ) //NON  
										{
                                		position_init(npage, nchamps);
										envoi_cursur();
                                        TRAMEOUT[0]='N';
                                        TRAMEOUT[1]='O';
                                        TRAMEOUT[2]='N';
										identifiant=2;
										enregistrer_donnees();
                                       	TRAMEENVOIRS232DONNEES (3);
										}	
                                       
							}









           				   if (( TRAMEIN[0]==0x1B && TRAMEIN[1]!=0x5B) || (TRAMEIN[0]==0x17 && TRAMEIN[1]==0x46))   ////////////////echape et  revenir au menu principale de nimporte ou  ////// s
                              {
                              repet =0;
                              ETAT1=M;
                              }
 
							  else if (TRAMEIN[0]==0x2D) //EFFACEMENT CAPTEUR (-)
							{
								if (npage=='b' && nchamps==1)
								{
											sup_capteur();		///////////// supression capteur 
										//	tmp=EFFACER_CAPTEUR();
											

		
							}
							}



		                   else if (( (TRAMEIN[0]>0x40 && TRAMEIN[0]<0x5B) || (TRAMEIN[0]>0x60 && TRAMEIN[0]<0x7B) || (TRAMEIN[0]>0x2F && TRAMEIN[0]<0x3A) || TRAMEIN [0]==0x20) ) // si lettres MAJUS. ou Lettres MINUSC. ou LES LES CHIFFRES ou ESPACE
                             {
								//interdire ecriture menu 8	if ((npage=='a' && nchamps==4) || (npage=='b' && ((nchamps<6 && nchamps>2)||(nchamps==1))) ||(npage=='8' && nchamps!=2 && nchamps!=3  )) // page 6b et pas le champ intervention ou page8 et pas alarme (OUI/NON) et page6b et pas le champ type de capteur
								if ((npage=='a' && nchamps==5) || (npage=='b' && nchamps==3 )) ///MPL //// 01022017 // interdire ecriture menu8 page 6b et pas le champ intervention et page6b et pas le champ type de capteur  ////ACCE ///////////////////////////// lundi 20/20/2017
             					
									{
										 if (npage == '8' && (nchamps == 4 || nchamps == 5) && (decal ==1 || decal ==3) ) //Si heure et date de la page 8
							 			{
								 
								 			pos_init_cursur [0]= pos_init_cursur [0]+ 1;
                							if (pos_init_cursur [0]==0x3A) // incrementation position a cause des : / : / dans date et heure page 8
											{
                       							pos_init_cursur [0]= 0x30;
                     							pos_init_cursur [1]=pos_init_cursur [1] + 1;
												envoi_cursur(); 
							            	}
										}
									    // if ((TRAMEIN[0]>0x2F && TRAMEIN[0]<0x3A) && (decal<max_decalage) )
							 			if (decal<=max_decalage) // tant que on a pas termin� le champ
										{
                             				saisie();
										}

									}
                       			 	if ( (TRAMEIN[0]>0x2F && TRAMEIN[0]<0x3A )&& ((npage=='3' )|| (npage=='b' && (nchamps == 1 || nchamps == 5 || nchamps == 4 )))) // 01022017//si uniquement champ avec des chiffres (page 3)
									{
                                   		saisie();
                                	}
                             }
                     



   
          				 // interdire ecriture menu 8 else if ((TRAMEIN[0]==0x09 ) || (TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x42 )) // si tabulation ou en bas
                           	else if (((TRAMEIN[0]==0x09 ) || (TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x42 )) && (npage!='a'))  //interdire ecriture menu8 si tabulation ou en bas ///ACCE ///page 6a interdir ld�placement d'un champ a un autre
                       
							{



             //  INTERDIRE ECRITURE MENU8            		if ((npage=='b')||(npage=='8'))
                               if (npage=='b')   //  INTERDIRE ECRITURE MENU8
								{
              // 			     		tabulation:
                //                       if (npage == '8' && nchamps ==4  ) //CHAMP HEURE
        		//						{
				//						verif_heure_date(); //VERIFICATION DE lA DATE
				//						}
                  //                     if (npage == '8' && nchamps ==5) //CHAMP DATE
        			//					{
					//					verif_heure_date(); //VERIFICATION DE LA DATE
					//					}
                           		 tabulation ();
                             	}
 
								if ((npage=='b') && (nchamps ==2)) 	//ON A FORCEMENT INCREMENTE AU DESSUS	
								{
								CHG_CODAGE=1;
								}

							TRAMEIN[0]==0x00; TRAMEIN[1]==0x00; TRAMEIN[2]==0x00;   //////AEF


							}

							else if (	TRAMEIN[0]==0x08)     //////////////22022017

              				{
								TRAMEOUT[0]=0x08;
								TRAMEENVOIRS232DONNEES (1);
								decal--;
							}


																													
						      else if (TRAMEIN[0]==0x17 && TRAMEIN[1]==0x41 ) ////////////////validation et retour vers le menu principal
                             {

								
								if (npage=='b' && nchamps==1) // PAS SUR LE CODAGE
								{

								}
								else
								{	
                           		if (npage == 'a') //PASSER DE 6a a 6b
									{
								    ETAT1=G;

									repet =0;
									}
								 

                         		 else
									{
	                              		
								if (npage=='b' && nchamps!=1) //SI  on valide un capteur sauf pour le champ codage
							{
							 
								tmp=VALIDER_CAPTEUR();
								repet =0;
								//ETAT1 = M;

								//ETAT1 = M;
								}
								else
								{                   		
								ETAT1 = M;
								}									//VALIDATION		

					// interdire ecriture menu8					if (npage == '8' && nchamps ==4  )
        			//					{
					//					verif_heure_date();
					//					}
                      //                 if (npage == '8' && nchamps ==5)
        				//				{
						//				verif_heure_date();
						//				}

		                             }
								}	
							
							}

                           else if (TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x43 )//// d�calage a droite 
                             {
									//autoriser ecriture menu 8 if ((npage=='a') || (npage=='b')||(npage=='8'))
										if ((npage=='a') || (npage=='b'))//interdire ecriture menu 8
										
										{ //Uniquement pr ces pages on valide laction de la touche DROITE
																	 //decal ++;
										                             droite ();
																	
										} 
								TRAMEIN[0]==0x00; TRAMEIN[1]==0x00; TRAMEIN[2]==0x00;         ////// AEF               
                             }
                           else if (TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x41 )//// d�calage a haut 
                             {
							 //autoriser ecriture menu 8 if ((npage=='a') || ((npage=='b')&& (nchamps!=1))||(npage=='8'))
							 if  ((npage=='b')&& (nchamps!=1))////interdire ecriture menu 8
							{
                             haut ();
      						  }  
						
							
								TRAMEIN[0]==0x00; TRAMEIN[1]==0x00; TRAMEIN[2]==0x00;   ////// AEF
              
                             }
                            
                             
                             else if (TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x44)//// d�calage a gauche
                             {
							//autoriser ecriture menu 8 if ((npage=='a') || (npage=='b')||(npage=='8'))
								if ((npage=='a') || (npage=='b'))//autoriser ecriture menu 8
								  	{
										gauche();
							
                           			  }
								TRAMEIN[0]==0x00; TRAMEIN[1]==0x00; TRAMEIN[2]==0x00;    ////// AEF
							}



fin_gestion:
delay_qms(1);
                             
} 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////gestion de la 1ere page du menu //////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gestion_menu1 (void)
{
kk=0;
position_init(npage, nchamps);
JJ[0]='j';
JJ[1]='j';
MM[0]='m';
MM[1]='m';
AA[0]='a';
AA[1]='a';
AA[2]='a';
AA[3]='a';
groupe[0]='g';
groupe[1]='g';
groupe[2]='g';
groupe[3]='g';
groupe[4]='g';
groupe[5]='g';
groupe[6]='g';
groupe[7]='g';
groupe[8]='g';
groupe[9]='g';
groupe[10]='g';
pos_capteur[0]='c';
pos_capteur[1]='c';
pos_capteur[2]='c';
pos_capteur[3]='c';
pos_capteur[4]='c';

ETAT_M1[0]='E';
ETAT_M1[1]='E';
ETAT_M1[2]='E';
ETAT_M1[3]='E';
ETAT_M1[4]='E';
ETAT_M1[5]='E';
ETAT_M1[6]='E';
ETAT_M1[7]='E';
ETAT_M1[8]='E';
ETAT_M1[9]='E';
ETAT_M1[10]='E';
ETAT_M1[11]='E';
ETAT_M1[12]='E';
ETAT_M1[13]='E';
ETAT_M1[14]='E';
ETAT_M1[15]='E';
ETAT_M1[16]='E';
ETAT_M1[17]='E';
ETAT_M1[18]='E';
ETAT_M1[19]='E';
ETAT_M1[20]='E';
ETAT_M1[21]='E';
ETAT_M1[22]='E';

desig[0]='d';
desig[1]='d';
desig[2]='d';
desig[3]='d';
desig[4]='d';
desig[5]='d';
desig[6]='d';
desig[7]='d';
desig[8]='d';
desig[9]='d';
desig[10]='d';
desig[11]='d';
desig[12]='d';
desig[13]='d';
desig[14]='d';
desig[15]='d';
desig[16]='d';
desig[17]='d';
desig[18]='d';
desig[19]='d';
desig[20]='d';

do
{

TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
TRAMEOUT[2]=pos_init_cursur [3];
TRAMEOUT[3]=pos_init_cursur [2];
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=pos_init_cursur [1];
TRAMEOUT[6]=pos_init_cursur [0];
TRAMEOUT[7]=0x48;
TRAMEOUT[8]=JJ[0];
TRAMEOUT[9]=JJ[1];
TRAMEOUT[10]=0x3A;
TRAMEOUT[11]=MM[0];
TRAMEOUT[12]=MM[1];
TRAMEOUT[13]=0x3A;
TRAMEOUT[14]=AA[0];
TRAMEOUT[15]=AA[1];
TRAMEOUT[16]=AA[2];
TRAMEOUT[17]=AA[3];
TRAMEOUT[18]=0x20;
TRAMEOUT[19]=groupe[0];
TRAMEOUT[20]=groupe[1];
TRAMEOUT[21]=groupe[2];
TRAMEOUT[22]=groupe[3];
TRAMEOUT[23]=groupe[4];
TRAMEOUT[24]=groupe[5];
TRAMEOUT[25]=groupe[6];
TRAMEOUT[26]=groupe[7];
TRAMEOUT[27]=groupe[8];
TRAMEOUT[28]=groupe[9];
TRAMEOUT[29]=groupe[10];
TRAMEOUT[30]=0x20;
TRAMEOUT[31]=pos_capteur[0];
TRAMEOUT[32]=pos_capteur[1];
TRAMEOUT[33]=pos_capteur[2];
TRAMEOUT[34]=pos_capteur[3];
TRAMEOUT[35]=pos_capteur[4];
TRAMEOUT[36]=0x20;
TRAMEOUT[37]=ETAT_M1[0];
TRAMEOUT[38]=ETAT_M1[1];
TRAMEOUT[39]=ETAT_M1[2];
TRAMEOUT[40]=ETAT_M1[3];
TRAMEOUT[41]=ETAT_M1[4];
TRAMEOUT[42]=ETAT_M1[5];
TRAMEOUT[43]=ETAT_M1[6];
TRAMEOUT[44]=ETAT_M1[7];
TRAMEOUT[45]=ETAT_M1[8];
TRAMEOUT[46]=ETAT_M1[9];
TRAMEOUT[47]=ETAT_M1[10];
TRAMEOUT[48]=ETAT_M1[11];
TRAMEOUT[49]=ETAT_M1[12];
TRAMEOUT[50]=ETAT_M1[13];
TRAMEOUT[51]=ETAT_M1[14];
TRAMEOUT[52]=ETAT_M1[15];
TRAMEOUT[53]=ETAT_M1[16];
TRAMEOUT[54]=ETAT_M1[17];
TRAMEOUT[55]=ETAT_M1[18];
TRAMEOUT[56]=ETAT_M1[19];
TRAMEOUT[57]=ETAT_M1[20];
TRAMEOUT[58]=ETAT_M1[21];
TRAMEOUT[59]=ETAT_M1[22];
TRAMEOUT[60]=0x20;
TRAMEOUT[61]=desig[0];
TRAMEOUT[62]=desig[1];
TRAMEOUT[63]=desig[2];
TRAMEOUT[64]=desig[3];
TRAMEOUT[65]=desig[4];
TRAMEOUT[66]=desig[5];
TRAMEOUT[67]=desig[6];
TRAMEOUT[68]=desig[7];
TRAMEOUT[69]=desig[8];
TRAMEOUT[70]=desig[9];
TRAMEOUT[71]=desig[10];
TRAMEOUT[72]=desig[11];
TRAMEOUT[73]=desig[12];
TRAMEOUT[74]=desig[13];
TRAMEOUT[75]=desig[14];
TRAMEOUT[76]=desig[15];
TRAMEOUT[77]=desig[16];
TRAMEOUT[78]=desig[17];
TRAMEOUT[79]=desig[18];
TRAMEOUT[80]=desig[19];
TRAMEOUT[81]=desig[20];
TRAMEENVOIRS232DONNEES (82);
 pos_init_cursur [2]= pos_init_cursur [2]+ 1;
                if (pos_init_cursur [2]==0x3A){             ////////mettre a jour la ligne 
                       pos_init_cursur [2]= 0x30;
                       pos_init_cursur [3]=pos_init_cursur [3] + 1;
		
                     }  
kk++;                                 
}while (kk<18);
gestion_page();
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////gestion de la 2eme page du menu ALARMES EN COURS//////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gestion_menu2 (void)
{
kk=0;
position_init(npage, nchamps);
analyser_les_alarmes_en_cours(); //ANALYSE LES CABLES ET LES ETATS POPUR L'INSTANT


do
{








//LIMITE A 99 ALARMES

//aadress[0]='x';
//aadress[1]='x';
//agroup[0]='x';
//agroup[1]='x';
//aresis[0]='x';
//aresis[1]='x';
TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
TRAMEOUT[2]=pos_init_cursur [3];
TRAMEOUT[3]=pos_init_cursur [2];
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=pos_init_cursur [1];
TRAMEOUT[6]=pos_init_cursur [0];
TRAMEOUT[7]=0x48;

IntToChar(ET_GR[kk],TMP,2);
TRAMEOUT[8]=TMP[0]; //GROUPE
TRAMEOUT[9]=TMP[1];
TRAMEOUT[10]=0x20;
TRAMEOUT[11]=0x20;
TRAMEOUT[12]=0x20;
TRAMEOUT[13]='!';
TRAMEOUT[14]=0x20;
TRAMEOUT[15]=0x20;
TRAMEOUT[16]=0x20;
TRAMEOUT[17]=0x20;
TRAMEOUT[18]=0x20;
//TRAMEOUT[19]=0x20;
//TRAMEOUT[20]=0x20;
//TRAMEOUT[21]=0x20;
//TRAMEOUT[22]=0x20;
//TRAMEOUT[23]=0x20;


IntToChar(ET_TP[kk],TMP,2);
TRAMEOUT[19]=TMP[0]; //TA
TRAMEOUT[20]=TMP[1];
TRAMEOUT[21]=0x20;
TRAMEOUT[22]=0x20;
TRAMEOUT[23]=0x20;
TRAMEOUT[24]=0x20;
TRAMEOUT[25]=0x20;
TRAMEOUT[26]=0x20;
TRAMEOUT[27]=0x20;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]='!';
TRAMEOUT[30]=0x20;
TRAMEOUT[31]=0x20;
TRAMEOUT[32]=0x20;
TRAMEOUT[33]=0x20;
TRAMEOUT[34]=0x20;
TRAMEOUT[35]=0x20;




IntToChar(ET_TR[kk],TMP,2);
TRAMEOUT[36]=TMP[0]; //TR
TRAMEOUT[37]=TMP[1];


TRAMEOUT[38]=0x20;
TRAMEOUT[39]=0x20;
TRAMEOUT[40]=0x20;
TRAMEOUT[41]=0x20;
TRAMEOUT[42]=0x20;
TRAMEOUT[43]='!';
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x20;
IntToChar(ET_TD[kk],TMP,2);
TRAMEOUT[46]=TMP[0];
TRAMEOUT[47]=TMP[1];







TRAMEENVOIRS232DONNEES (48);
 pos_init_cursur [2]= pos_init_cursur [2]+ 2; //ligne suivante
                if (pos_init_cursur [2]==0x3A){             ////////mettre a jour la ligne 
                       pos_init_cursur [2]= 0x30;
                       pos_init_cursur [3]=pos_init_cursur [3] + 1;
		
                     }  



kk++;                                
}while (kk<4); //LES 4 x 3 champs

TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
pos_init_cursur [3]='1';
pos_init_cursur [2]='6';

TRAMEOUT[2]=pos_init_cursur [3];
TRAMEOUT[3]=pos_init_cursur [2];
TRAMEOUT[4]=0x3B;
pos_init_cursur [1]='0';
pos_init_cursur [0]='2';
TRAMEOUT[5]=pos_init_cursur [1];
TRAMEOUT[6]=pos_init_cursur [0];
TRAMEOUT[7]=0x48;
TRAMEOUT[8]='I';
TRAMEOUT[9]='N';
TRAMEOUT[10]='C';
TRAMEOUT[11]=':';
TRAMEOUT[12]=' ';
IntToChar(ET_I[0],TMP,2);
TRAMEOUT[13]=TMP[0];
TRAMEOUT[14]=TMP[1];
TRAMEOUT[15]=' '; 
TRAMEOUT[16]='C';
TRAMEOUT[17]='C';
TRAMEOUT[18]=':';
TRAMEOUT[19]=' ';
IntToChar(ET_C[0],TMP,2);
TRAMEOUT[20]=TMP[0];
TRAMEOUT[21]=TMP[1];
TRAMEOUT[22]=' ';
TRAMEOUT[23]='v';
TRAMEOUT[24]='F';
TRAMEOUT[25]='U';
TRAMEOUT[26]='S';
TRAMEOUT[27]=':';
TRAMEOUT[28]=' ';
IntToChar(ET_F[0],TMP,2);
TRAMEOUT[29]=TMP[0];
TRAMEOUT[30]=TMP[1];
TRAMEENVOIRS232DONNEES (31);


gestion_page();
}





/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////gestion de la 3eme page du menu VISUALTION DES DONNEES D'UN CAPTEUR //////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gestion_menu3 (void)
{
char nb_oct;

retour_menu3:

kk=0;
nchamps=1;
position_init(npage, nchamps);
envoi_cursur();
//do {
//gestion_page();
//}while (TRAMEIN[0]!=0x17 && TRAMEIN[1]!=0x41 && STOPMINITEL!=0xFF);


//tableau on range le numero de cable
//CHERCHER_DONNNEES_CABLES(&tableau,0); //DEBIT


nb_oct=0;


do {                         /////////ACCE
TRAMERECEPTIONminitel();
      if (STOPMINITEL==0xFF)
      goto fin_menu3; 
if ( (TRAMEIN[0]==0x17 && TRAMEIN[1]==0x46) || (TRAMEIN[0]==0x1B && TRAMEIN[1]!=0x5B))   /////aghi
goto fin_menu3; 
                      if ((TRAMEIN[0]>0x2F && TRAMEIN[0]<0x3A) && (decal<max_decalage) ){ // 0 et 9 chiffres
                                 saisie();
           if (nb_oct==decal-1)
           {
           nb_oct++;
           }
           }




     else if (TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x43 && decal<max_decalage)//// d�calage a droite 
                             {
        
                             droite ();              
                             }
    else if (TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x44)//// d�calage a gauche
                             {
       gauche();     
       nb_oct--;    /////////22022017
                             }

	else if (TRAMEIN[0]==0x1B && TRAMEIN[1]!=0x5B) //Si echap voir ancien telgat
	{
	ETAT1=M;
	repet=0;
	goto fin_menu3;
	}	

   }while (TRAMEIN[0]!=0x17 && STOPMINITEL!=0xFF);


if (STOPMINITEL==0xFF )
goto fin_menu3;


if (nb_oct==1)           /////aghi
{
tableau[2]=tableau[0];
tableau[1]='0';
tableau[0]='0';
}

if (nb_oct==2)
{
tableau[2]=tableau[1];
tableau[1]=tableau[0];
tableau[0]='0';
}

if (nb_oct==3)
{
tableau[0]=tableau[0];
tableau[1]=tableau[1];
tableau[2]=tableau[2];
}                    /////aghi







//nchamps=2;
//position_init(npage, nchamps);
//debit[0]= 'D'; //CODAGE 0
//debit[1]= 'D';
//debit[2]= 'D';
//debit[3]= 'D';
//TRAMEOUT[0]=0x1B;
//TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
//TRAMEOUT[2]=pos_init_cursur [3];
//TRAMEOUT[3]=pos_init_cursur [2];
//TRAMEOUT[4]=0x3B;
//TRAMEOUT[5]=pos_init_cursur [1];
//TRAMEOUT[6]=pos_init_cursur [0];
//TRAMEOUT[7]=0x48;
//TRAMEOUT[8]=debit[0];
//TRAMEOUT[9]=debit[1];
//TRAMEOUT[10]=debit[2];
//TRAMEOUT[11]=debit[3];
//TRAMEENVOIRS232DONNEES (12);
if (TRAMEIN[0]==0x17 && TRAMEIN[1]==0x41 )
{

nchamps=2;
position_init(npage, nchamps);

 
 
											do {

											CHERCHER_DONNNEES_CABLES(&tableau,kk); //DEBIT



											TRAMEOUT[0]=0x1B;
											TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
											TRAMEOUT[2]=pos_init_cursur [3];
											TRAMEOUT[3]=pos_init_cursur [2];
											TRAMEOUT[4]=0x3B;
											TRAMEOUT[5]=pos_init_cursur [1];
											TRAMEOUT[6]=pos_init_cursur [0];
											TRAMEOUT[7]=0x48;
										
if (capt_exist==1)
{

											TRAMEOUT[8]=COD[0];
											TRAMEOUT[9]=COD[1];
											TRAMEOUT[10]=0x20;
											TRAMEOUT[11]=0x20;
											TRAMEOUT[12]=0x20;
											TRAMEOUT[13]=0x20;
											TRAMEOUT[14]=0x20;
											TRAMEOUT[15]=0x20;
											TRAMEOUT[16]=CCON[0];
											TRAMEOUT[17]=CCON[1];
											TRAMEOUT[18]=CCON[2];
											TRAMEOUT[19]=CCON[3];
											TRAMEOUT[20]=0x20;
											TRAMEOUT[21]=0x20;
											TRAMEOUT[22]=0x20;
											TRAMEOUT[23]=0x20;
											TRAMEOUT[24]=0x20;
											TRAMEOUT[25]=0x20;
											TRAMEOUT[26]=CMOD[0];
											TRAMEOUT[27]=CMOD[1];
											TRAMEOUT[28]=CMOD[2];
											TRAMEOUT[29]=CMOD[3];
											TRAMEOUT[30]=0x20;
											TRAMEOUT[31]=0x20;
											TRAMEOUT[32]=0x20;
											TRAMEOUT[33]=0x20;
											TRAMEOUT[34]=0x20;
											TRAMEOUT[35]=CREP[0];
											TRAMEOUT[36]=CREP[1];
											TRAMEOUT[37]=CREP[2];
											TRAMEOUT[38]=CREP[3];
											TRAMEOUT[39]=0x20;
											TRAMEOUT[40]=0x20;
											TRAMEOUT[41]=0x20;
											TRAMEOUT[42]=0x20;
											TRAMEOUT[43]=VALEUR[0];
											TRAMEOUT[44]=VALEUR[1];
											TRAMEOUT[45]=VALEUR[2];
											TRAMEOUT[46]=VALEUR[3];
											TRAMEOUT[47]=0x20;
											TRAMEOUT[48]=0x20;
											TRAMEOUT[49]=0x20;
											TRAMEOUT[50]=0x20;
											TRAMEOUT[51]=0x20;
											TRAMEOUT[52]=0x20;
											TRAMEOUT[53]=SEUIL[0];
											TRAMEOUT[54]=SEUIL[1];
											TRAMEOUT[55]=SEUIL[2];
											TRAMEOUT[56]=SEUIL[3];
											TRAMEOUT[57]=0x20;
											TRAMEOUT[58]=0x20;
											TRAMEOUT[59]=0x20;
											TRAMEOUT[60]=0x20;
											TRAMEOUT[61]=ETAT[0];
											TRAMEOUT[62]=ETAT[1];
											TRAMEOUT[63]=0x20;
											TRAMEOUT[64]=0x20;
											TRAMEOUT[65]=DISTANCE[0];
											TRAMEOUT[66]=DISTANCE[1];
											TRAMEOUT[67]=DISTANCE[2];
											TRAMEOUT[68]=DISTANCE[3];
											TRAMEOUT[69]=DISTANCE[4];

											TRAMEOUT[70]=0x20;
											TRAMEOUT[71]=CONSTITUTION[0];
											TRAMEOUT[72]=CONSTITUTION[1];
											TRAMEOUT[73]=CONSTITUTION[2];

											TRAMEOUT[74]=CONSTITUTION[3];
											TRAMEOUT[75]=CONSTITUTION[4];
											TRAMEOUT[76]=CONSTITUTION[5];
											TRAMEOUT[77]=CONSTITUTION[6];
											TRAMEOUT[78]=CONSTITUTION[7];
											TRAMEOUT[79]=CONSTITUTION[8];
											TRAMEOUT[80]=CONSTITUTION[9];
											TRAMEOUT[81]=CONSTITUTION[10];
											TRAMEOUT[82]=CONSTITUTION[11];
											TRAMEOUT[83]=CONSTITUTION[12];
											TRAMEOUT[84]=0x20;
											TRAMEOUT[85]=0x20;
											TRAMEOUT[86]=0x20;
}
else
{
for (nb_oct=8;nb_oct<=86;nb_oct++)
TRAMEOUT[nb_oct]='-';

}
											TRAMEOUT[87]=0x20;
											TRAMEOUT[88]=0x20;
											TRAMEENVOIRS232DONNEES (89);
											 pos_init_cursur [2]= pos_init_cursur [2]+ 1;
											                if (pos_init_cursur [2]==0x3A)
																 {             ////////mettre a jour la ligne 
											                       pos_init_cursur [2]= 0x30;
											                       pos_init_cursur [3]=pos_init_cursur [3] + 1;
													
											                     }  
         

	  		





											kk++; 
											}while (kk<=16);  //tous les capteurs 
//gestion_page();
goto retour_menu3;
}

fin_menu3:
delay_qms(10);
repet =0;   // TOUCHE ECHAP h1746 recue precedemment  par gestion page
ETAT1=M;

}










/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////gestion de la 5eme page du menu GROUPE//////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gestion_menu5 (void)
{

kk=0;
nchamps=1;
position_init(npage, nchamps);


CHERCHER_DONNEES_GROUPE();

do {
 
variables_groupe(kk+1);





TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
TRAMEOUT[2]=pos_init_cursur [3];
TRAMEOUT[3]=pos_init_cursur [2];
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=pos_init_cursur [1];
TRAMEOUT[6]=pos_init_cursur [0];
TRAMEOUT[7]=0x48;



TRAMEOUT[8]=valeur[0];
TRAMEOUT[9]=valeur[1];
TRAMEOUT[10]=valeur[2];
TRAMEOUT[11]=valeur[3];


TRAMEOUT[12]=0x20;
TRAMEOUT[13]=0x20;
TRAMEOUT[14]=0x20;
TRAMEOUT[15]=0x20;
TRAMEOUT[16]=0x20;



if (((ETAT[0]=='3')||(ETAT[0]=='1')||(ETAT[0]=='2'))&&(ETAT[1]=='0'))
{
ETAT[0]='A';
ETAT[1]='L';
ETAT[2]='.';
ETAT[3]=' ';
}

if (ETAT[1]=='1')
{
ETAT[0]='I';
ETAT[1]='N';
ETAT[2]='T';
ETAT[3]='.';
}

if ((ETAT[0]=='0')&&(ETAT[1]=='0'))
{
ETAT[0]='O';
ETAT[1]='K';
ETAT[2]=' ';
ETAT[3]=' ';
}

if ((ETAT[0]=='4')&&(ETAT[1]=='0'))
{
ETAT[0]='H';
ETAT[1]='.';
ETAT[2]='G';
ETAT[3]='.';
}


if ((ETAT[0]=='5')&&(ETAT[1]=='0'))
{
ETAT[0]='S';
ETAT[1]='.';
ETAT[2]='R';
ETAT[3]='.';
}




TRAMEOUT[17]=ETAT[0];
TRAMEOUT[18]=ETAT[1];
TRAMEOUT[19]=ETAT[2];
TRAMEOUT[20]=ETAT[3];

TRAMEOUT[21]=0x20;
TRAMEOUT[22]=0x20;
TRAMEOUT[23]=0x20;
TRAMEOUT[24]=0x20;
TRAMEOUT[25]=0x20;
TRAMEOUT[26]=0x20;

TRAMEOUT[27]=SEUIL[0];
TRAMEOUT[28]=SEUIL[1];
TRAMEOUT[29]=SEUIL[2];
TRAMEOUT[30]=SEUIL[3];

TRAMEOUT[31]=0x20;
TRAMEOUT[32]=0x20;
TRAMEOUT[33]=0x20;
TRAMEOUT[34]=0x20;
TRAMEOUT[35]=0x20;
TRAMEOUT[36]=0x20;
TRAMEOUT[37]=0x20;

TRAMEOUT[38]=SEUIL2[0];
TRAMEOUT[39]=SEUIL2[1];
TRAMEOUT[40]=SEUIL2[2];
TRAMEOUT[41]=SEUIL2[3];

TRAMEOUT[42]=0x20;
TRAMEOUT[43]=0x20;
TRAMEOUT[44]=0x20;
TRAMEOUT[45]=0x20;

TRAMEOUT[46]=iiii[0];
TRAMEOUT[47]=iiii[1];
TRAMEOUT[48]=iiii[2];
TRAMEOUT[49]=iiii[1];
TRAMEOUT[50]=iiii[2];

TRAMEENVOIRS232DONNEES (51);
 pos_init_cursur [2]= pos_init_cursur [2]+ 1;
                if (pos_init_cursur [2]==0x3A){             ////////mettre a jour la ligne 
                       pos_init_cursur [2]= 0x30;
                       pos_init_cursur [3]=pos_init_cursur [3] + 1;
		
                     } 
kk++;
}while (kk<10);
gestion_page();
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////gestion de la 6eme page du menu CONFIG (LISTE) //////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gestion_menu6 (void)
{
//kk=0;
nchamps=1;
position_init(npage, nchamps);




 

//do {



TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
TRAMEOUT[2]=pos_init_cursur [3];
TRAMEOUT[3]=pos_init_cursur [2];
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=pos_init_cursur [1];
TRAMEOUT[6]=pos_init_cursur [0];
TRAMEOUT[7]=0x48;


TRAMEENVOIRS232DONNEES (resume_voies(1,8));  //ENVOIE CARTE 1

////////////////////ICI SANS DOUTE !!!!!!!!!!!!!!!!!!

nchamps++;



position_init(npage, nchamps);
TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
TRAMEOUT[2]=pos_init_cursur [3];
TRAMEOUT[3]=pos_init_cursur [2];
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=pos_init_cursur [1];
TRAMEOUT[6]=pos_init_cursur [0];
TRAMEOUT[7]=0x48;
TRAMEENVOIRS232DONNEES (resume_voies(2,8));  //ENVOIE CARTE 2

nchamps++;



position_init(npage, nchamps);
TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
TRAMEOUT[2]=pos_init_cursur [3];
TRAMEOUT[3]=pos_init_cursur [2];
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=pos_init_cursur [1];
TRAMEOUT[6]=pos_init_cursur [0];
TRAMEOUT[7]=0x48;
TRAMEENVOIRS232DONNEES (resume_voies(3,8));  //ENVOIE CARTE 3


nchamps++;


position_init(npage, nchamps);
TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
TRAMEOUT[2]=pos_init_cursur [3];
TRAMEOUT[3]=pos_init_cursur [2];
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=pos_init_cursur [1];
TRAMEOUT[6]=pos_init_cursur [0];
TRAMEOUT[7]=0x48;
TRAMEENVOIRS232DONNEES (resume_voies(4,8));  //ENVOIE CARTE 4
nchamps++;


position_init(npage, nchamps);
TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;////////////// envoyer les coordonn�es du cursur
TRAMEOUT[2]=pos_init_cursur [3];
TRAMEOUT[3]=pos_init_cursur [2];
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=pos_init_cursur [1];
TRAMEOUT[6]=pos_init_cursur [0];
TRAMEOUT[7]=0x48;
TRAMEENVOIRS232DONNEES (resume_voies(5,8));  //ENVOIE CARTE 5



 pos_init_cursur [2]= pos_init_cursur [2]+ 1;
                if (pos_init_cursur [2]==0x3A){             ////////mettre a jour la ligne 
                       pos_init_cursur [2]= 0x30;
                       pos_init_cursur [3]=pos_init_cursur [3] + 1;
		
                     } 
//kk++;
//}while (kk<1);
nchamps=1;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////gestion de la 6eme page du menu //////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gestion_menu6a (void)
{

char t;
char fin; ////MPL

etiq21:

nb_oct=0;
fin =0;   ////MPL




do {
TRAMERECEPTIONminitel();
						if (STOPMINITEL==0xFF)
						goto fin_menu6a;	

                      if ((TRAMEIN[0]>0x2F && TRAMEIN[0]<0x3A) && (decal<max_decalage) ){
                             				saisie();
											if (nb_oct==decal-1 )
											{
											nb_oct++;
											}
											}

//				if (TRAMEIN[0]==0x17)	
//                            {
//					goto etiquet;
//							}
 				else if (TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x43 && decal<max_decalage)//// d�calage a droite 
                             {
							 
                             droite ();              
                             }
				else if (TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x44)//// d�calage a gauche
                             {
							gauche();
							nb_oct--;   ////////22022017
                             }
                else if ((TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x42 ) && (nchamps==2)) ///// MPL debut
				{
					fin = 0xFF;
					tabulation ();
				}           
                if (TRAMEIN[0]==0x17 || (TRAMEIN[0]==0x1B && TRAMEIN[1]!=0x5B ))
                  fin =0xFF;           ///// MPL fin
  
//				else {
//                     goto etiq21;}

			}while ( fin !=0xFF); /////(decal<max_decalage);
										//	enregistrer_donnees();
if (( TRAMEIN[0]==0x1B && TRAMEIN[1]!=0x5B) || (TRAMEIN[0]==0x17 && TRAMEIN[1]==0x46))  ///////21012017
												{
													repet =0;   // TOUCHE ECHAP h1746 recue precedemment  par gestion page
                              						ETAT1=M; 
													goto  fin_menu6a;
												} // RETOUR MENU PRINCIPAL (ETAT1M)	

if ((nb_oct==0 )&&(nchamps==1)) //si aucune saisie alors on reste dans le champ
goto etiq21;



if (nchamps==1){         																	/////aghi
if ((VOIECOD[0]>='1' && VOIECOD[1]!='0') || (VOIECOD[0]=='1' && VOIECOD[2]!='0'))      
{
VOIECOD[0]='1';
VOIECOD[1]='0';
VOIECOD[2]='0';
position_init(npage, nchamps);
envoi_cursur();
TRAMEOUT[0]=VOIECOD[0];
TRAMEOUT[1]=VOIECOD[1];
TRAMEOUT[2]=VOIECOD[2];
TRAMEENVOIRS232DONNEES (3);
}
}       																			/////aghi
if (nchamps == 1 && (TRAMEIN[1]!=0x46 || TRAMEIN[1]!=0x5B )) ///// MPL  debut
{





											VOIECOD[3]='0';
											VOIECOD[4]='0';
											CHERCHER_DONNNEES_CABLES(&VOIECOD,111); //CABLE 

											if ((TYPEDECARTE()!='E') && (TYPEDECARTE()!='M')) //
											{

												nchamps=6;
											position_init(npage, nchamps);
											envoi_cursur();						
											TRAMEOUT[0]='A';				
											TRAMEOUT[1]='B';
											TRAMEOUT[2]='S';
											TRAMEOUT[3]='E';
											TRAMEOUT[4]='N';
											TRAMEOUT[5]='T';
											TRAMEOUT[6]='E';
											TRAMEENVOIRS232DONNEES (7);
											TEMPO(5);
											goto fin_menu6a;

 




											}	


											if (capt_exist==1) //CABLE EXISTANT
											{	

											nchamps=6;
											position_init(npage, nchamps);
											envoi_cursur();						
											TRAMEOUT[0]='E';				
											TRAMEOUT[1]='X';
											TRAMEOUT[2]='I';
											TRAMEOUT[3]='S';
											TRAMEOUT[4]='T';
											TRAMEOUT[5]='E';
											TRAMEOUT[6]=' ';
											TRAMEENVOIRS232DONNEES (7);


											//ROBINET DEJA ENTRE
											ROB[0]=COMMENTAIRE[2];
											ROB[1]=COMMENTAIRE[3];
											nchamps=2;
															position_init(npage, nchamps);	
															envoi_cursur();
															TRAMEOUT[0]=ROB[0];
															TRAMEOUT[1]=ROB[1];
															TRAMEENVOIRS232DONNEES (2); //NUMERO DE LA CARTE


											}
											else
											{

											nchamps=6;
											position_init(npage, nchamps);
											envoi_cursur();						
											TRAMEOUT[0]='N';				
											TRAMEOUT[1]='O';
											TRAMEOUT[2]='U';
											TRAMEOUT[3]='V';
											TRAMEOUT[4]='E';
											TRAMEOUT[5]='A';
											TRAMEOUT[6]='U';
											TRAMEENVOIRS232DONNEES (7);	
											}

dejaassoc:

nchamps=2;
position_init(npage, nchamps);
envoi_cursur();
fin=0;

goto etiq21;
}             ///// MPL  fin

if (nchamps ==2 && nb_oct==0 &&capt_exist==0)  ///////////////////////////// lundi 20/20/2017
{goto etiq21; }/////////////////////////////////////////////////////////////
etiquet:






//afficher_donnees_si_existe(&tableau);

                                            if ((TRAMEIN[0]==0x17 && TRAMEIN[1]==0x41)||(TRAMEIN[0]==0x1B && TRAMEIN[1]==0x5B && TRAMEIN[2]==0x42 )) ////MPL
										{  ///////////ON REGARDE SI ENTREE=VALIDATION h1741 ET PAS ECHAP BACK ETC
										//VOIECOD[0]=tableau[0];
										//VOIECOD[1]=tableau[1]; //ON SAV LE CABLE
										//VOIECOD[2]=tableau[2];	


															nchamps=2;
															position_init(npage, nchamps);	
															envoi_cursur();
															TRAMEOUT[0]=ROB[0];
															TRAMEOUT[1]=ROB[1];
															TRAMEENVOIRS232DONNEES (2); //NUMERO DE LA CARTE
																
															if (gestion_rob()==1) //deja associ�
															goto dejaassoc;	
                                      

															calcul_c_l(); //NUMERO DE LA CARTE ET LIGNE
															nchamps=3;  ///// MPL
															position_init(npage, nchamps);
															envoi_cursur();
															TRAMEOUT[0]=n_carte ;
															TRAMEENVOIRS232DONNEES (1); //NUMERO DE LA CARTE
															nchamps=4;   ///// MPL
												            position_init(npage, nchamps);
													        envoi_cursur();
												            TRAMEOUT[0]=n_voie[0];
												            TRAMEOUT[1]=n_voie[1];
															TRAMEENVOIRS232DONNEES (2); //NUMERO DE LA VOIE



     
                                            if (capt_exist==1) //CABLE EXISTANT
											{



															//voie_select[0]=tableau[0]; // POUR LA LE NUMERO DU CABLE SAV POUR 6B
															//voie_select[1]=tableau[1];
															//voie_select[2]=tableau[2];
														
															//interv[0]='N';
															//interv[1]='O';
															//interv[2]='N';
														

														
															nchamps=5;  ///// MPL
															position_init(npage, nchamps);
															envoi_cursur();
															for (t=4;t<18;t++) //commencer a 4 rbxx on naffiche pas le robinet
															{
															TRAMEOUT[t-4]=COMMENTAIRE[t];
															}
				
															TRAMEENVOIRS232DONNEES (14); //le reste 
											
										




											}
											else //NOUVEAU CABLE
											{

								




											}	




									
											nchamps=5;   ///// MPL
     								        position_init(npage, nchamps);
       										envoi_cursur();
                                          
													




																do { //SI AUTRE ATTENDRE VALIDATION
					         									  gestion_page();
					  									         }while ((ETAT1==F)&& (STOPMINITEL!=0xFF)); //SI VALIDATION ON VA A 6.3 ETAT1 G AEF
					                    }// FIN VALIDATION
                                            else if (( TRAMEIN[0]==0x1B && TRAMEIN[1]!=0x5B) || (TRAMEIN[0]==0x17 && TRAMEIN[1]==0x46))
												{
													repet =0;   // TOUCHE ECHAP h1746 recue precedemment  par gestion page
                              						ETAT1=M; 
												} // RETOUR MENU PRINCIPAL (ETAT1M)
                                     


//Rajouter commentaire et numero Rob au debut du commentaire si modifi� ou pas peut importe

COMMENTAIRE[0]='R';
COMMENTAIRE[1]='o';
COMMENTAIRE[2]=ROB[0];
COMMENTAIRE[3]=ROB[1];




for (t=0;t<20;t++)
{
COMMENTAIRESAV[t]=COMMENTAIRE[t];   //SAV DU CABLE COMMENTAIRE     EN xxx00 SUR LE DEBITMETRE   	
}






fin_menu6a:
delay_qms(10);


}






/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////gestion de la page 6B du menu //////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gestion_menu6b (void)
{

char o;
CHG_CODAGE=0;


VOIECOD[3]='0';     
VOIECOD[4]='1';
position_init(npage, 1); //LISTE
envoi_cursur();
TRAMEOUT[0]=VOIECOD[3];
TRAMEOUT[1]=VOIECOD[4];
TRAMEENVOIRS232DONNEES (2);




//voie_select[0]=tableau[0];




//REMPLIR LISTE CAPTEURS EXISTANTS


NBRECAPTEURS_CABLE=0;

for (o=0;o<17;o++)
{
		CHERCHER_DONNNEES_CABLES(&VOIECOD,o); //CAPTEURS UN A UN
		if (capt_exist==1)
		{
		IntToChar(o,&TMP,2);
		TRAMEOUT[8+3*o]='-';
		TRAMEOUT[9+3*o]=TMP[0];
		TRAMEOUT[10+3*o]=TMP[1];
		//TRAMEOUT[3+4*o]=TMP[2];
		NBRECAPTEURS_CABLE=NBRECAPTEURS_CABLE+1;
		}

		else
		{
		IntToChar(o,&TMP,2);
		TRAMEOUT[8+3*o]='-';
		TRAMEOUT[9+3*o]=' ';
		TRAMEOUT[10+3*o]=' ';
		//TRAMEOUT[3+4*o]=' ';
		}

}



TRAMEOUT[59]='-'; //FIN LISTE 
position_init(npage, 7); //LISTE
envoi_cursur();
TRAMEENVOIRS232DONNEES (60);


//capt_exist==1;
//CHERCHER_DONNNEES_CABLES(&voie_select,0); //CAPTEUR DEBIT



RE_6B:

o=10*(VOIECOD[3]-48)+(VOIECOD[4]-48);
CHERCHER_DONNNEES_CABLES(&VOIECOD,o); // SI LE CAPTEUR EXISTE 
 

//-----------------------------TYPE DE CAPTEUR







								if  ((TYPE[0]=='A')&&(TYPEDECARTE()=='E'))
								{
									type_capteur[0]='T';
									type_capteur[1]='P';
									type_capteur[2]=0x20;
									type_capteur[3]='A';
									type_capteur[4]='D';
									type_capteur[5]='R';
									type_capteur[6]='E';
									type_capteur[7]='S';
									type_capteur[8]='S';
									type_capteur[9]='A';
									type_capteur[10]='B';
									type_capteur[11]='L';
									type_capteur[12]='E';
								}


								if  ((TYPE[0]=='R')&&(TYPEDECARTE()=='M'))
								{
									type_capteur[0]='T';
									type_capteur[1]='P';
									type_capteur[2]=0x20;
									type_capteur[3]='R';
									type_capteur[4]='E';
									type_capteur[5]='S';
									type_capteur[6]='I';
									type_capteur[7]='S';
									type_capteur[8]='I';
									type_capteur[9]='F';
									type_capteur[10]=0X20;
									type_capteur[11]=0X20;
									type_capteur[12]=0X20;
								}


								if((VOIECOD[3]=='0' &&	VOIECOD[4]=='0')&&(TYPEDECARTE()=='E')) //SI CODAGE = '00' et carte pas resistive
								{
									type_capteur[0]='D';
									type_capteur[1]='E';
									type_capteur[2]='B';
									type_capteur[3]='I';
									type_capteur[4]='M';
									type_capteur[5]='E';
									type_capteur[6]='T';
									type_capteur[7]='R';
									type_capteur[8]='E';
									type_capteur[9]=0x20;
									type_capteur[10]=0x20;
									type_capteur[11]=0x20;
									type_capteur[12]=0x20;
									identifiant=3; // LE TYPE DE CAPTEUR  1 2 3 adress...resis.....debit..
									enregistrer_donnees();
								
								}




//TYPE DE CAPTEUR 
nchamps=2;
position_init(npage, nchamps);
envoi_cursur();
TRAMEOUT[0]=type_capteur[0];
TRAMEOUT[1]=type_capteur[1];
TRAMEOUT[2]=type_capteur[2];
TRAMEOUT[3]=type_capteur[3];
TRAMEOUT[4]=type_capteur[4];
TRAMEOUT[5]=type_capteur[5];
TRAMEOUT[6]=type_capteur[6];
TRAMEOUT[7]=type_capteur[7];
TRAMEOUT[8]=type_capteur[8];
TRAMEOUT[9]=type_capteur[9];
TRAMEOUT[10]=type_capteur[10];
TRAMEOUT[11]=type_capteur[11];
TRAMEOUT[12]=type_capteur[12];
TRAMEENVOIRS232DONNEES (13);










//CONSTITUTION
nchamps=3;
position_init(npage, nchamps);
envoi_cursur();



TRAMEOUT[0]=CONSTITUTION[0];
TRAMEOUT[1]=CONSTITUTION[1];
TRAMEOUT[2]=CONSTITUTION[2];
TRAMEOUT[3]=CONSTITUTION[3];
TRAMEOUT[4]=CONSTITUTION[4];
TRAMEOUT[5]=CONSTITUTION[5];
TRAMEOUT[6]=CONSTITUTION[6];
TRAMEOUT[7]=CONSTITUTION[7];
TRAMEOUT[8]=CONSTITUTION[8];
TRAMEOUT[9]=CONSTITUTION[9];
TRAMEOUT[10]=CONSTITUTION[10];
TRAMEOUT[11]=CONSTITUTION[11];
TRAMEOUT[12]=CONSTITUTION[12];
TRAMEENVOIRS232DONNEES (13);







nchamps=4;
position_init(npage, nchamps);
envoi_cursur();
TRAMEOUT[0]=SEUIL[0];
TRAMEOUT[1]=SEUIL[1];
TRAMEOUT[2]=SEUIL[2];
TRAMEOUT[3]=SEUIL[3];
TRAMEENVOIRS232DONNEES (4);
//------------------
if(VOIECOD[3]=='0' &&	VOIECOD[4]=='0') //////////////ACCE
{
TRAMEOUT[0]=0x20; //TD DEBIT
TRAMEOUT[1]=0x20;
TRAMEOUT[2]='L';
TRAMEOUT[3]='/';
TRAMEOUT[4]='H';
TRAMEOUT[5]=0x20;
}
else
{
TRAMEOUT[0]=0x20; //TA TR PRESSION
TRAMEOUT[1]='m';
TRAMEOUT[2]='b';
TRAMEOUT[3]='a';
TRAMEOUT[4]='r';
TRAMEOUT[5]=0x20;
}
TRAMEENVOIRS232DONNEES (6);

//----------------

nchamps=5;
position_init(npage, nchamps);
envoi_cursur();

TRAMEOUT[0]=DISTANCE[0];
TRAMEOUT[1]=DISTANCE[1];
TRAMEOUT[2]=DISTANCE[2];
TRAMEOUT[3]=DISTANCE[3];
TRAMEOUT[4]=DISTANCE[4];
TRAMEENVOIRS232DONNEES (5);


nchamps=6;
position_init(npage, nchamps);
envoi_cursur();
if (ETAT[1]=='1')
{
TRAMEOUT[0]='O';
TRAMEOUT[1]='U';
TRAMEOUT[2]='I';
}
else
{
TRAMEOUT[0]='N';
TRAMEOUT[1]='O';
TRAMEOUT[2]='N';
}

TRAMEENVOIRS232DONNEES (3);


if (CHG_CODAGE==0)
{
//NUMERO DE CODAGE
nchamps=1;
position_init(npage, nchamps);
envoi_cursur();
}
else
{
CHG_CODAGE=0;
nchamps=2;
position_init(npage, nchamps);
envoi_cursur();
}


										 	do {
											gestion_page();
											if ((CHG_CODAGE==1))
											{
											if (((VOIECOD[3]=='1')&& (VOIECOD[4]>'6'))||((VOIECOD[3]>'1')&& (VOIECOD[4]>='0'))) //EMPECHER CODAGE >16 
											{
											VOIECOD[3]='1';
											VOIECOD[4]='6';
											nchamps=1;
											position_init(npage, nchamps);
											envoi_cursur();
											TRAMEOUT[0]=VOIECOD[3];
											TRAMEOUT[1]=VOIECOD[4];	
											TRAMEENVOIRS232DONNEES (2);		
											}
											goto RE_6B;	
											}

											}while ((ETAT1==G) && (repet!=0)&& (STOPMINITEL!=0xFF)); // SI ON SORT DE LA PAGE AEF
}






/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////gestion de la page 8 du menu //////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gestion_menu8 (void)
{
char tc;

Recuperer_param_cms();

LIRE_HEURE_MINITEL();




nchamps=1;
position_init(npage, nchamps);
envoi_cursur();

//gestion_page ();

for (tc=0;tc<30;tc++)
{
TRAMEOUT[tc]=ID_rack[tc];
};














TRAMEENVOIRS232DONNEES (30);
nchamps++;
position_init(npage, nchamps);
envoi_cursur();

TRAMEOUT[0]=T_alarme[0];
TRAMEOUT[1]=T_alarme[1];
TRAMEOUT[2]=T_alarme[2];
TRAMEENVOIRS232DONNEES (3);
nchamps++;
position_init(npage, nchamps);
envoi_cursur();

TRAMEOUT[0]=T_sortie[0];
TRAMEOUT[1]=T_sortie[1];
TRAMEOUT[2]=T_sortie[2];
TRAMEENVOIRS232DONNEES (3);
nchamps++;
position_init(npage, nchamps);
envoi_cursur();








TRAMEOUT[0]=HH[0];
TRAMEOUT[1]=HH[1];
TRAMEOUT[2]=0x3A;
TRAMEOUT[3]=mm[0];
TRAMEOUT[4]=mm[1];
TRAMEENVOIRS232DONNEES (5);
nchamps++;
position_init(npage, nchamps);
envoi_cursur();

TRAMEOUT[0]=JJ[0];
TRAMEOUT[1]=JJ[1];
TRAMEOUT[2]=0x2D;
TRAMEOUT[3]=MM[0];
TRAMEOUT[4]=MM[1];
TRAMEOUT[5]=0x2D;
TRAMEOUT[6]=AA[0];
TRAMEOUT[7]=AA[1];
TRAMEOUT[8]=AA[2];
TRAMEOUT[9]=AA[3];
TRAMEENVOIRS232DONNEES (10);

nchamps++;
position_init(npage, nchamps);
envoi_cursur();


TRAMEOUT[0]=acc_u[0];
TRAMEOUT[1]=acc_u[1];
TRAMEOUT[2]=acc_u[2];
TRAMEOUT[3]=acc_u[3];
TRAMEENVOIRS232DONNEES (4);
nchamps++;
position_init(npage, nchamps);
envoi_cursur();


TRAMEOUT[0]=acc_conf[0];
TRAMEOUT[1]=acc_conf[1];
TRAMEOUT[2]=acc_conf[2];
TRAMEOUT[3]=acc_conf[3];
TRAMEENVOIRS232DONNEES (4);
nchamps++;
position_init(npage, nchamps);
envoi_cursur();


for (tc=0;tc<15;tc++)
{
TRAMEOUT[tc]=nr_cog[tc];
};
TRAMEENVOIRS232DONNEES (15);
nchamps++;
position_init(npage, nchamps);
envoi_cursur();


for (tc=0;tc<15;tc++)
{
TRAMEOUT[tc]=nr_cms[tc];
};

TRAMEENVOIRS232DONNEES (15);
nchamps++;
position_init(npage, nchamps);
envoi_cursur();

TRAMEOUT[0]=passerelle[0];
TRAMEOUT[1]=passerelle[1];
TRAMEOUT[2]=passerelle[2];
TRAMEOUT[3]=passerelle[3];
TRAMEOUT[4]=passerelle[4];
TRAMEOUT[5]=passerelle[5];
TRAMEOUT[6]=passerelle[6];
TRAMEOUT[7]=passerelle[7];
TRAMEOUT[8]=passerelle[8];
TRAMEOUT[9]=passerelle[9];
TRAMEOUT[10]=passerelle[10];
TRAMEOUT[11]=passerelle[11];
TRAMEOUT[12]=passerelle[12];
TRAMEOUT[13]=passerelle[13];
TRAMEOUT[14]=passerelle[14];

TRAMEENVOIRS232DONNEES (15);
nchamps++;
position_init(npage, nchamps);
envoi_cursur();


TRAMEOUT[0]=DNS[0];
TRAMEOUT[1]=DNS[1];
TRAMEOUT[2]=DNS[2];
TRAMEOUT[3]=DNS[3];
TRAMEOUT[4]=DNS[4];
TRAMEOUT[5]=DNS[5];
TRAMEOUT[6]=DNS[6];
TRAMEOUT[7]=DNS[7];
TRAMEOUT[8]=DNS[8];
TRAMEOUT[9]=DNS[9];
TRAMEOUT[10]=DNS[10];
TRAMEOUT[11]=DNS[11];
TRAMEOUT[12]=DNS[12];
TRAMEOUT[13]=DNS[13];
TRAMEOUT[14]=DNS[14];

TRAMEENVOIRS232DONNEES (15);
nchamps=1;
position_init(npage, nchamps);
envoi_cursur();
 											do
										    {
											gestion_page();
                                         if (STOPMINITEL==0xFF)
											break;
											}while (ETAT1==K);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////// nom de site ///////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void nom_site ( void)
{
if (ETAT1==I)                 ////////////////// emplacement du nom de site sur la page mots de passe 
{
TRAMEOUT[0]=0x1B; 
TRAMEOUT[1]=0x5B ;
TRAMEOUT[2]=0x31;
TRAMEOUT[3]=0x32;
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=0x32 ;
TRAMEOUT[6]=0x38 ;
TRAMEOUT[7]=0x48 ;
}
else                             ////////////////// emplacement du nom de site sur les autres pages 
{
                                               
TRAMEOUT[0]=0x1B; 
TRAMEOUT[1]=0x5B ;
TRAMEOUT[2]=0x30;
TRAMEOUT[3]=0x32;
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=0x33 ;
TRAMEOUT[6]=0x30 ;
TRAMEOUT[7]=0x48 ;
}


//NOM DU SITE 
TRAMEOUT[8]=0x1B ;
TRAMEOUT[9]=0x5B ;
TRAMEOUT[10]=0x37 ; //SURLIGNER EN BLANC
TRAMEOUT[11]=0x6D ;
TRAMEOUT[12]=0x20 ;
TRAMEOUT[13]=ID_rack[0];
TRAMEOUT[14]=ID_rack[1];
TRAMEOUT[15]=ID_rack[2];
TRAMEOUT[16]=ID_rack[3];
TRAMEOUT[17]=ID_rack[4];
TRAMEOUT[18]=ID_rack[5];
TRAMEOUT[19]=ID_rack[6];
TRAMEOUT[20]=ID_rack[7];
TRAMEOUT[21]=ID_rack[8];
TRAMEOUT[22]=ID_rack[9];
TRAMEOUT[23]=ID_rack[10];
TRAMEOUT[24]=ID_rack[11];
TRAMEOUT[25]=ID_rack[12];
TRAMEOUT[26]=ID_rack[13];
TRAMEOUT[27]=ID_rack[14];
TRAMEOUT[28]=ID_rack[15];
TRAMEOUT[29]=ID_rack[16];
TRAMEOUT[30]=ID_rack[17];
TRAMEOUT[31]=ID_rack[18];
TRAMEOUT[32]=ID_rack[19];
TRAMEOUT[33]=ID_rack[20];
TRAMEOUT[34]=ID_rack[21];
TRAMEOUT[35]=ID_rack[22];
TRAMEOUT[36]=ID_rack[23];
TRAMEOUT[37]=ID_rack[24];
TRAMEOUT[38]=ID_rack[25];
TRAMEOUT[39]=ID_rack[26];
TRAMEOUT[40]=ID_rack[27];
TRAMEOUT[41]=ID_rack[28];
TRAMEOUT[42]=ID_rack[29];
TRAMEOUT[43]=0x20;
TRAMEOUT[44]=0x1B ;
TRAMEOUT[45]=0x5B ;
TRAMEOUT[46]=0x30 ; //NON SURLIGNER EN BLANC
TRAMEOUT[47]=0x6D ;
TRAMEOUT[48]=0x20 ;

					TRAMEENVOIRS232DONNEES (49);
if (ETAT1==I)                ////// mettre le cursur dans la case mot de passe 
{
TRAMEOUT[0]=0x1B; 
TRAMEOUT[1]=0x5B ;
TRAMEOUT[2]=0x32;
TRAMEOUT[3]=0x30;
TRAMEOUT[4]=0x3B;
TRAMEOUT[5]=0x33 ;
TRAMEOUT[6]=0x34 ;
TRAMEOUT[7]=0x48 ;
TRAMEENVOIRS232DONNEES (8);
}

}




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////renvoyer les positions initials du cursur suivant la page//////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



void position_init (char Npage, char Nchamps)
{
decal=0;

               switch (Npage)
{
                           case 'I':
                                     if (Nchamps == 1)
                                     {   
                                        pos_init_cursur [0]='9';////////////// ligne1, colomne 59 chmaps heure et date 
										pos_init_cursur [1]='5';
										pos_init_cursur [2]='1';
                                        pos_init_cursur [3]='0';
 										max_decalage=9;
                                     }
                           
                                     else if  (Nchamps == 2) 
                                     {
                                        pos_init_cursur [0]='4';//////// ligne 20 colomne 34 :mots de passe 
										pos_init_cursur [1]='3';
										pos_init_cursur [2]='0';
                                        pos_init_cursur [3]='2';
 										max_decalage=5;
                                     }             

                           break;
                           case 'M':
                                     if (Nchamps == 1)
                                     {   
                                        pos_init_cursur [0]='9';////////////// ligne1, colomne 59 chmaps heure et date 
										pos_init_cursur [1]='5';
										pos_init_cursur [2]='1';
                                        pos_init_cursur [3]='0';
 										max_decalage=9;
                                     }
                                     else if  (Nchamps == 2) 
                                     {
                                        pos_init_cursur [0]='0';/////ligne 7 colomne 30 nom de site
										pos_init_cursur [1]='3';
										pos_init_cursur [2]='7';
                                        pos_init_cursur [3]='0';
 										max_decalage=40;
                                     } 
                           break;
                           case '1':
									 
                                     if (Nchamps == 1)
                                     {   
                                        pos_init_cursur [0]='2';////////////ligne 4 colonne 2 
										pos_init_cursur [1]='0';
										pos_init_cursur [2]='4';
                                        pos_init_cursur [3]='0';
 										max_decalage=40;
                                     }
                                   
                                        
                           break;

                           case '2':
                                     if (Nchamps == 1)
                                     {   
                                        pos_init_cursur [0]='8';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='8';
                                        pos_init_cursur [3]='0';
 										max_decalage=20;
                                     }
               

                           

                           break;
                           case '3':
                       

                                      if  (Nchamps == 1) ///////num�ro de cable
                                     {
                                        pos_init_cursur [0]='6';
										pos_init_cursur [1]='1';
										pos_init_cursur [2]='2';
                                        pos_init_cursur [3]='0';
 										max_decalage=3;
                                     }                                 
                                     
                                     else if  (Nchamps == 2)  /////////ligne 11 colonne 18 
                                     {
                                        pos_init_cursur [0]='2';
										pos_init_cursur [1]='0';
										pos_init_cursur [2]='7';
                                        pos_init_cursur [3]='0';
 										max_decalage=20;
                                     }                                 
                                  
                             
                           break;








                           case '5':
                                    if (Nchamps == 1)
                                     { 
                                        pos_init_cursur [0]='6';
										pos_init_cursur [1]='3';
										pos_init_cursur [2]='9';
                                        pos_init_cursur [3]='0';
 										max_decalage=20;
                                     }
                                    
                                     
                           break;
                           case '6':

                                    if (Nchamps == 1)
                                     { 
   										pos_init_cursur [0]='9';//1*X
										pos_init_cursur [1]='2';//10*X
										pos_init_cursur [2]='6';// 1*Y
                                        pos_init_cursur [3]='0'; //10*Y
 										max_decalage=20;  
                                     }
                                     else if  (Nchamps == 2) 
                                     {
   										pos_init_cursur [0]='9';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='0';
                                        pos_init_cursur [3]='1';
 										max_decalage=20;
                                     }                                 
                                     else if  (Nchamps == 3) 
                                     {
   										pos_init_cursur [0]='9';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='4';
                                        pos_init_cursur [3]='1';
 										max_decalage=20;
                                     }  
                                     else if  (Nchamps == 4 ) 
                                     {
   										pos_init_cursur [0]='9';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='8';
                                        pos_init_cursur [3]='1';
 										max_decalage=20;
                                     }                                 
                                     else if  (Nchamps == 5) 
                                     {
   										pos_init_cursur [0]='9';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='2';
                                        pos_init_cursur [3]='2';
 										max_decalage=20;
                                     }  

									      else if  (Nchamps == 11) 
                                     {
   										pos_init_cursur [0]='9';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='7';
                                        pos_init_cursur [3]='0';
 										max_decalage=20;
                                     }  


									else if  (Nchamps == 12) 
                                     {
   										pos_init_cursur [0]='9';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='1';
                                        pos_init_cursur [3]='1';
 										max_decalage=20;
                                     }  

									else if  (Nchamps == 13) 
                                     {
   										pos_init_cursur [0]='9';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='5';
                                        pos_init_cursur [3]='1';
 										max_decalage=20;
                                     }  

									else if  (Nchamps == 14) 
                                     {
   										pos_init_cursur [0]='9';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='9';
                                        pos_init_cursur [3]='1';
 										max_decalage=20;
                                     }  
		
									else if  (Nchamps == 15) 
                                     {
   										pos_init_cursur [0]='9';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='3';
                                        pos_init_cursur [3]='2';
 										max_decalage=20;
                                     }  
			


                                    
                           break;
                           case 'a':
 
                                    if (Nchamps == 1)
                                     { 
                                        pos_init_cursur [0]='9';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='6';
                                        pos_init_cursur [3]='0';
 										max_decalage=3;  
                                     }
                                     else if  (Nchamps == 2) 
                                     {
                                        pos_init_cursur [0]='1';  ////MPL
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='7';
                                        pos_init_cursur [3]='0';
 										max_decalage=2; 
                                     }  
                                     else if  (Nchamps == 3) 
                                     {
                                        pos_init_cursur [0]='0';
										pos_init_cursur [1]='6';
										pos_init_cursur [2]='6';
                                        pos_init_cursur [3]='0';
 										max_decalage=1; 
                                     }                                 
                                     else if  (Nchamps == 4) 
                                     {
                                        pos_init_cursur [0]='9';
										pos_init_cursur [1]='5';
										pos_init_cursur [2]='7';
                                        pos_init_cursur [3]='0';
 										max_decalage=2;
                                     }  
                                   else if (Nchamps == 5)
                                     { 
                                        pos_init_cursur [0]='0';
										pos_init_cursur [1]='3';
										pos_init_cursur [2]='9';
                                        pos_init_cursur [3]='0';
 										max_decalage=13;  //COMMENTAIRE 18CARAC
                                     }
                                  //   else if  (Nchamps == 5) 
                                    // {
                                      //  pos_init_cursur [0]='0';
										//pos_init_cursur [1]='3';
										//pos_init_cursur [2]='0';
                                        //pos_init_cursur [3]='1';
 										//max_decalage=3;
                                     //}                                 
                                  
									 else if  (Nchamps == 6) //champ out QUI INDIQUE SI NOUVEAU OU PAS !!!
                                     {
                                        pos_init_cursur [0]='0';
										pos_init_cursur [1]='3';
										pos_init_cursur [2]='8';
                                        pos_init_cursur [3]='0';
 										//max_decalage=3;
                                     }  	









                                    
                           break;
                           case 'b':
if (Nchamps ==1)
                                     { 
                                        pos_init_cursur [0]='9'; //CODAGE
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='2';
                                        pos_init_cursur [3]='1';
 										max_decalage=1;  
                                     }
                                     else if  (Nchamps == 2) 
                                     {
                                        pos_init_cursur [0]='9'; //TYPE DE CAPTEURS
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='3';
                                        pos_init_cursur [3]='1';
 										max_decalage=1;
                                     }                                 
                                     else if  (Nchamps == 3) 
                                     {
                                        pos_init_cursur [0]='9'; //CONSTITUTION
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='4';
                                        pos_init_cursur [3]='1';
 										max_decalage=12;
                                     }  
                                   else if (Nchamps == 4)
                                     { 
                                        pos_init_cursur [0]='9'; //SEUIL
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='5';
                                        pos_init_cursur [3]='1';
 										max_decalage=4;  
                                     }
                                     else if  (Nchamps == 5) 
                                     {
                                        pos_init_cursur [0]='9'; //DISTANCE
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='6';
                                        pos_init_cursur [3]='1';
 										max_decalage=4;
                                     }                                 
                                     else if  (Nchamps == 6) 
                                     {
                                        pos_init_cursur [0]='8'; //INTERVENTION
										pos_init_cursur [1]='6';
										pos_init_cursur [2]='6';
                                        pos_init_cursur [3]='1';
 										max_decalage=2;
                                     }  
   
									  else if  (Nchamps == 7)  //LISTE CAPTEURS EXISTANTS 
                                     {
                                        pos_init_cursur [0]='5';
										pos_init_cursur [1]='1';
										pos_init_cursur [2]='7';
                                        pos_init_cursur [3]='1';
 										//max_decalage=2;
                                     }  			
								
										else if  (Nchamps == 8)  //PATIENTEZ
                                     {
                                        pos_init_cursur [0]='5';
										pos_init_cursur [1]='1';
										pos_init_cursur [2]='8';
                                        pos_init_cursur [3]='1';
 										//max_decalage=2;
                                     }  



                           break;
                              case '8':
                                    
                                      if  (Nchamps == 1) 
                                     {
                                        pos_init_cursur [0]='8';
										pos_init_cursur [1]='3';
										pos_init_cursur [2]='6';
                                        pos_init_cursur [3]='0';
 										max_decalage=30;
                                     }                                 
                                     else if  (Nchamps == 2) 
                                     {
                                        pos_init_cursur [0]='8';
										pos_init_cursur [1]='3';
										pos_init_cursur [2]='0';
                                        pos_init_cursur [3]='1';
 										max_decalage=3;
                                     }  
                                     else if  (Nchamps == 3) 
                                     {
                                        pos_init_cursur [0]='8';
										pos_init_cursur [1]='3';
										pos_init_cursur [2]='2';
                                        pos_init_cursur [3]='1';
 										max_decalage=3;
                                     }                                 
                                   
                                     else if  (Nchamps == 4) 
                                     {
                                        pos_init_cursur [0]='0';
										pos_init_cursur [1]='4';
										pos_init_cursur [2]='8';
                                        pos_init_cursur [3]='1';
 										max_decalage=4;
                                     }                                 
                                     else if  (Nchamps == 5) 
                                     {
                                        pos_init_cursur [0]='0';
										pos_init_cursur [1]='4';
										pos_init_cursur [2]='9';
                                        pos_init_cursur [3]='1';
 										max_decalage=8;
                                     }  
                                     else if  (Nchamps == 6) 
                                     {
                                        pos_init_cursur [0]='6';
										pos_init_cursur [1]='4';
										pos_init_cursur [2]='1';
                                        pos_init_cursur [3]='2';
 										max_decalage=4;
                                     }                                 
                                     else if  (Nchamps == 7) 
                                     {
                                        pos_init_cursur [0]='6';
										pos_init_cursur [1]='4';
										pos_init_cursur [2]='2';
                                        pos_init_cursur [3]='2';
 										max_decalage=4;
                                     }  
                                     else if  (Nchamps == 8) 
                                     {
                                        pos_init_cursur [0]='2';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='4';
                                        pos_init_cursur [3]='1';
 										max_decalage=10;
                                     }     
  									else if  (Nchamps == 9) 
                                     {
                                        pos_init_cursur [0]='4';
										pos_init_cursur [1]='6';
										pos_init_cursur [2]='4';
                                        pos_init_cursur [3]='1';
 										max_decalage=14;
                                     }        
                                     else if  (Nchamps == 10) 
                                     {
                                        pos_init_cursur [0]='5';
										pos_init_cursur [1]='2';
										pos_init_cursur [2]='6';
                                        pos_init_cursur [3]='1';
 										max_decalage=12;
                                     }     
  									else if  (Nchamps == 11) 
                                     {
                                        pos_init_cursur [0]='8';
										pos_init_cursur [1]='5';
										pos_init_cursur [2]='6';
                                        pos_init_cursur [3]='1';
 										max_decalage=12;
                                     } 
                                    

                           break;
}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////reception minitel////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void TRAMERECEPTIONminitel (void)  //GESTION DES TOUCHES 1B, ... 1B 15 7F  NOMBRE D'OCTETS 1 OU 2 OU 3 ??????? 
{
unsigned char u;
unsigned char tmp;
tmp=0;


		u=RECEVOIR_RS232();
        TRAMEIN[tmp]=u;
	                     	 if (u==0x17) //ON CHERCHE A DETECTER ENTREE(h1741)  autres h17xx ???
                            {
           
                             u=RECEVOIR_RS232(); // Si h41 on sort
		                       tmp=tmp+1;
                              TRAMEIN[tmp]=u;
                            }
                      
                           else if (TRAMEIN[0]==0x1B) // h1Bxx  //pour les fl�ches h1B5B 41 ou 42 43 44 fleches haut bas droite gauche
                            {                             
                             u=RECEVOIR_RS232(); //ATTENTE 2IEME TOUCHE 
		                       tmp=tmp+1;
                              TRAMEIN[tmp]=u;
             				if (TRAMEIN[tmp]!=0x5B ) /// AEF SI 2xECHAP (1B)
							{
							goto fin_recep;	
							}
                             u=RECEVOIR_RS232();
		                       tmp=tmp+1;
                              TRAMEIN[tmp]=u;
                            }
                           else if (TRAMEIN[0]==0x0D) // h1Bxx  //pour les fl�ches h1B5B 41 ou 42 43 44 fleches haut bas droite gauche
                            {
                            u=RECEVOIR_RS232();
							if (u=='3') // AEF
							STOPMINITEL==0xFF;

							
                            }
                           else  //les autres cas ????
                            {
                      	   tmp=tmp;
                            }
fin_recep:
		TRAMEIN[tmp]=u;
		tmp=tmp+1;



 
//delay_qms(100);
 
}

























































 
 



//////////////////////////////////////////////////////////////////
// fonction permettant d'initialiser le port s�rie				//
//////////////////////////////////////////////////////////////////

 
void Init_RS485(void)
{

unsigned int SPEED;

//TRISCbits.TRISC5 = 0;
TRISGbits.TRISG2 = 1;
//TRISCbits.TRISC6 = 0;
 TRISGbits.TRISG1 = 0; //RC5 en sortie	

TRISAbits.TRISA4 = 0; //RA4 RX_TX en sortie OU DERE

 

  RCSTA2bits.SPEN  = 1;       // Enable serial port
  TXSTA2bits.SYNC  = 0;       // Async mode	
  BAUDCON2bits.BRG16 = 1 ;
  
  TXSTA2bits.BRGH = 1;      //haute vitesse    
  SPEED = 650;      	 		 // set 9600 bauds 650  // 207 28800bauds
  SPBRG2 = SPEED ;     		// Write baudrate to SPBRG1
  SPBRGH2 = SPEED >> 8;		// For 16-bit baud rate generation


  IPR3bits.RC2IP  = 1;       // Set high priority
  PIR3bits.RC2IF = 0;        // met le drapeau d'IT � 0 (plus d'IT)
  PIE3bits.RC2IE = 0;        // ne g�n�re pas d'IT pour la liaison s�rie

  
//  IPR1bits.RCIP  = 1;       // Set high priority
//  PIR1bits.RCIF = 0;        // met le drapeau d'IT � 0 (plus d'IT)
//  PIE1bits.RCIE = 0;        // ne g�n�re pas d'IT pour la liaison s�rie
        
  TXSTA2bits.TXEN = 0;        // transmission inhib�e
  RCSTA2bits.RX9 = 0;         // r�ception sur 8 bits
  TXSTA2bits.TX9 = 0;           // transmission sur 8 bits
  RCSTA2bits.CREN  = 0;       // interdire reception
  RCONbits.IPEN  = 1;       // Enable priority levels 

}


 
 

//////////////////////////////////////////////////////////////////
// reception d'un caract�re sur le port s�rie		//
//////////////////////////////////////////////////////////////////


char RECEVOIRST_RS485(void)
{
unsigned char Carac;
unsigned int TEMPS1,TEMPS2;
DERE=0; //AUTORISATION DE RECEVOIR
   // PORTCbits.RC5=1; //FIN DE TRANSMISSION     		 
PORTAbits.RA4=0;
RCSTA2bits.CREN  = 1; 

while ((PIR3bits.RC2IF == 0)) // max 6s pour 10
{
};       // attend une r�ception de caract�re sur RS232
//    __no_operation();

Carac = RCREG2;    // caract�re re�u
PIR3bits.RC2IF = 0;
RCSTA2bits.CREN  = 0; 
return(Carac);    // retourne le caract�re re�u sur la liaison RS232

}

 
 

 

//////////////////////////////////////////////////////////////////
// �mission d'un caract�re sur le port s�rie					//
//////////////////////////////////////////////////////////////////

void RS485(unsigned char carac)
{
//carac = code(carac);


DERE=1; //AUTORISATION TRANSMETTRE
	//	LEDC=!LEDC;
PORTAbits.RA4=1;
TXSTA2bits.TXEN = 0;

 RCSTA2bits.CREN  = 0;       // interdire reception

	TXSTA2bits.TXEN = 1;  // autoriser transmission
	

TXREG2 = carac;
	
  //__no_operation();
  //__no_operation();
  //__no_operation();
  //__no_operation();
 while (TXSTA2bits.TRMT == 0) ;    // attend la fin de l'�mission
//     __no_operation();
TXSTA2bits.TXEN = 0; 
	
DERE=0; //INTERDICTION TRANSMETTRE
//delayms(100);


}






void Init_I2C(void)
{
//	TRISCbits.TRISC3 = 1;
//	TRISCbits.TRISC4 = 1;

  //here is the I2C setup from the Seeval32 code.
	DDRCbits.RC3 = 1; //Configure SCL as Input
	DDRCbits.RC4 = 1; //Configure SDA as Input
	SSP1STAT = 0x00;   //Disable SMBus & Slew Rate Control 80
	SSP1CON1 = 0x28;  //Enable MSSP Master  28
	SSP1CON2 = 0x00;   //Clear MSSP Conr// 0x3B for 400kHz  (24mhz/((4*4OOkHz))-1 = 14 ou 3B en hexol Bits
	SSP1ADD  = 0x3B;    //0x0E
}

 


void i2c_start(void)    // Initiate a Start sequence
{
//TRISCbits.TRISC3=1;	
//TRISCbits.TRISC4=1;	
PIR1bits.SSPIF=0;
SSPCON2bits.SEN=1;
while(PIR1bits.SSPIF==0);
return;
}



void i2c_restart(void)  // Initiate a Restart sequence
{
PIR1bits.SSPIF=0;
SSPCON2bits.RSEN=1;
while(PIR1bits.SSPIF==0);
return;
}

void i2c_device(void)   // device specification
{
PIR1bits.SSPIF=0;
SSPBUF=0b11010000;   
while(PIR1bits.SSPIF==0);
while(SSPCON2bits.ACKSTAT==1);
return;
}


void i2c_write(unsigned int data)      // Write data to slave.
{
PIR1bits.SSPIF=0;
SSPBUF=data;
while(PIR1bits.SSPIF==0);
return;
}

void i2c_stop(void)     //Initiate a Stop sequence.
{
PIR1bits.SSPIF=0;
SSPCON2bits.PEN=1;
while(PIR1bits.SSPIF==0);
// TRISCbits.TRISC3=1;  //MODIF CG  avant � 0
// TRISCbits.TRISC4=1;
return;
}

unsigned int i2c_read(void)     //Read data from I2C bus.
{
unsigned int r;
PIR1bits.SSPIF=0;
SSPCON2bits.RCEN=1;

while(PIR1bits.SSPIF==0);
r=SSPBUF;
PIR1bits.SSPIF=0;
SSPCON2bits.ACKEN=1;
while(PIR1bits.SSPIF==0);
return r;
}




//********************************************************************/

unsigned char putstringI2C(unsigned char *wrptr)
{



unsigned char x;

unsigned int PageSize;
PageSize=128;


  for (x = 0; x < PageSize; x++ ) // transmit data until PageSize  
  {
    if ( SSPCON1bits.SSPM3 )      // if Master transmitter then execute the following
    { // 
	
	 	
		      if ( putcI2C ( *wrptr ) )   // write 1 byte
		{
		        return ( -3 );            // return with write collision error
		}
	

      IdleI2C();                  // test for idle condition
      if ( SSPCON2bits.ACKSTAT )  // test received ack bit state
      {
        return ( -2 );            // bus device responded with  NOT ACK
      }                           // terminateputstringI2C() function
    }
    else                          // else Slave transmitter
    {
      PIR1bits.SSPIF = 0;         // reset SSPIF bit

	
      SSPBUF = *wrptr;            // load SSPBUF with new data
		    

	  SSPCON1bits.CKP = 1;        // release clock line 
      while ( !PIR1bits.SSPIF );  // wait until ninth clock pulse received

      if ( ( !SSPSTATbits.R_W ) && ( !SSPSTATbits.BF ) )// if R/W=0 and BF=0, NOT ACK was received
      {
        return ( -2 );            // terminateputstringI2C() function
      }
    }

 	

  wrptr ++;                       // increment pointer 
 
  
  	 
 }                               // continue data writes until null character
  return ( 0 );
}


 


//Lecture dans l'EEPROM N�0 N�1 N�2 ou N�3 envoyer 000 ou 001......
unsigned char LIRE_EEPROM(unsigned char ADDE,unsigned int ADDHL,unsigned char *tab,char r)
{
char h;
unsigned char t;
unsigned char TMP2;
unsigned char TMP4;
unsigned char AddH,AddL;


//MAXERIE A2 
if (ADDE==6)
ADDE=12;
if (ADDE==5)
ADDE=10;
if (ADDE==4)
ADDE=8;
if (ADDE==3)
ADDE=6;
if (ADDE==2)
ADDE=4;
if (ADDE==1)
ADDE=2;


AddL=ADDHL;
AddH=ADDHL>>8;



TMP2=ADDE;
//TMP2=TMP2<<1;
TMP2=TMP2|0xA0;


   IdleI2C(); 
   StartI2C(); 
   //IdleI2C();	
   while ( SSPCON2bits.SEN ); 	

   WriteI2C(TMP2); // 0 sur le bit RW pour indiquer une lecture
   IdleI2C();
  
        


    WriteI2C(AddH); // 0 sur le bit RW pour indiquer une lecture
  IdleI2C();

  while ( SSPCON2bits.RSEN );   

	WriteI2C(AddL); // 0 sur le bit RW pour indiquer une lecture
   IdleI2C();


	RestartI2C();
	while (SSPCON2bits.RSEN);
	

	//AckI2C();
	TMP2=TMP2|0x01;
 WriteI2C(TMP2); // 1 sur le bit RW pour indiquer une lecture
   IdleI2C();
 
 
 
			//tab[85]='*';
			if (r==1) // pour trame memoire
			getsI2C(tab,86);  
			if (r==0)	
			getsI2C(tab,108);  
			if 	(r==3)	
			getsI2C(tab,116); 
			if 	(r==4)	
			getsI2C(tab,24); 
				if 	(r==5)	
			getsI2C(tab,120); 

   NotAckI2C(); 
   while(SSPCON2bits.ACKEN); 



   StopI2C();  
   while (SSPCON2bits.PEN);
   return (1); 
} 




//Ecriture dans l'EEPROM N�0 N�1 N�2 ou N�3 envoyer 000 ou 001......adresse haute et basse et une donn�e sur 8bits

unsigned char ECRIRE_EEPROMBYTE(unsigned char ADDE,unsigned int ADDHL,unsigned char *tab,char g)
{
unsigned char TMP2,TMP;
unsigned int h;
unsigned char t;
unsigned char AddH,AddL;

//MAXERIE A2 
if (ADDE==6)
ADDE=12;
if (ADDE==5)
ADDE=10;
if (ADDE==4)
ADDE=8;
if (ADDE==3)
ADDE=6;
if (ADDE==2)
ADDE=4;
if (ADDE==1)
ADDE=2;




TMP2=ADDE;
//			TMP2=TMP2<<1; 
			TMP2=0XA0|TMP2;

			
AddL=(ADDHL);
AddH=(ADDHL)>>8;			
			
if 	(g==1) // POUR FONCTIONNEMENT TRAME MEMOIRE		
tab[85]='*';   //FIN DE TRAME MEMOIRE			
if 	(g==3) // POUR FONCTIONNEMENT TRAME MEMOIRE		
tab[115]='*';   //FIN DE TRAME GROUPE
if 	(g==4) // POUR FONCTIONNEMENT TRAME MEMOIRE		
tab[23]='*';   //FIN DE TRAME EXISTANCE
if 	(g==5) // POUR FONCTIONNEMENT TRAME MEMOIRE		
tab[119]='*';   //FIN DE TRAME P�RAM1

			
			 
  IdleI2C();                      // ensure module is idle
  StartI2C();                     // initiate START condition
  while ( SSPCON2bits.SEN );      // wait until start condition is over 
  WriteI2C(TMP2);        // write 1 byte - R/W bit should be 0
  IdleI2C();                      // ensure module is idle
  WriteI2C(AddH);            // write HighAdd byte to EEPROM 
  IdleI2C();                      // ensure module is idle
  WriteI2C(AddL);             // write LowAdd byte to EEPROM
  IdleI2C();                      // ensure module is idle
  putstringI2C (tab);         // pointer to data for page write
  IdleI2C();                      // ensure module is idle
  StopI2C();                      // send STOP condition
  while ( SSPCON2bits.PEN );      // wait until stop condition is over 
  return ( 0 );                   // return with no error	
		
 				


    
		return 0;	// FIN DE BOUCLE a *

                     
        
        

 


}













// CALCUL EMPLACEMENT MEMOIRE
unsigned int calcul_addmemoire (unsigned char *tab)
{
unsigned int t;
unsigned int i;
unsigned int r;

 

t=0;
i=tab[3];
i=i-48;
t=t+1*i;
i=tab[2];
i=i-48;
t=t+10*i;
i=tab[1];
i=i-48;
t=t+100*i;


r=2176*(t);
if (t>29)
r=2176*(t-30);
if (t>59)
r=2176*(t-60);
if (t>89)
r=2176*(t-90);


t=0;
i=tab[5]-48;
t=1*i;
i=tab[4]-48;
t=t+10*i;
i=t;

t=r+128*i;



delay_qms(10);



return t;


}


// choix du numero memoire suivant N�voie
unsigned char choix_memoire(unsigned char *tab)
{
unsigned int i;
unsigned int t;
unsigned char r;




t=0;
i=tab[3]-48;
t=t+1*i;
i=tab[2]-48;
t=t+10*i;
i=tab[1]-48;
t=t+100*i;

r=0;
if (t>29)
r=1;
if (t>59)
r=2;
if (t>89)
r=3;




delay_qms(10);
return r;

}


















//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UNIQUEMENT POUR CARTE MERE ET A LA DEMANDE DE LA CARTE MERE (LORS d'une PREMIERE TRAME CM-->Rx  et  erreurs)  //
//ECRIRE 1 pour carte R1 ,2 pour R2 etc..................                           
//puis le numero de la fonction exemple 

// pour DIC=68*2+73+67=276 (avec code decimal des carac ascii) DEMANDE INFOS CAPTEUR A LA CARTE RELAIS	//	
//puis le tableau contenant les donn�es a envoyer suivant le code de la fonction																								                //
// le resultat est stock� dans la TRAMEOUT[]
//////////////////////////////////////////////////////////////////////////////////////////////////////


 

void ENVOIE_DES_TRAMES (char R,int fonctionRS,char *tab) 


 


{

int t;
unsigned int funct;
int y;
char u;
float h;

ID1[0]='I';
ID1[1]='O';
START[0]='#';
STOP[0]='*'; 
	switch(R)  //analyse de la fonction
		
		{
		 

			case 0 :  // R1
			ID2[0]='C';
			ID2[1]='M';
			break;


			case 1 :  // R1
			ID2[0]='R';
			ID2[1]='1';
			break;
				case 2 :  // R2
			ID2[0]='R';
			ID2[1]='2';
			break;
				case 3 :  // R3
			ID2[0]='R';
			ID2[1]='3';
			break;
				case 4 :  // R4
			ID2[0]='R';
			ID2[1]='4';
			break; 
				case 5 :  // R5
			ID2[0]='R';
			ID2[1]='5';
			break; 	

						case 6 :  // R5
			ID2[0]='A';
			ID2[1]='U';
			break; 	

			case 7 :  // IO
			ID2[0]='I';
			ID2[1]='O';
			break; 	
			
			default :
	 	
						
			break;
		
		}

 

	switch(fonctionRS)
	{

			case 319 : //RAZ CMS PA COM RS485
			

			break;

		
			case 302 :  // ERREUR


			FONCTION[0]='E';
			FONCTION[1]='R';
			FONCTION[2]='R';
			TRAMEOUT[0]=START[0];
			TRAMEOUT[1]=ID1[0];
			TRAMEOUT[2]=ID1[1];
			TRAMEOUT[3]=ID2[0];
			TRAMEOUT[4]=ID2[1];
			TRAMEOUT[5]=FONCTION[0];
			TRAMEOUT[6]=FONCTION[1];
			TRAMEOUT[7]=FONCTION[2];
			//TRAMEOUT[8]=tab[0]; 
			//TRAMEOUT[9]=tab[1];
				
								for (t=8;t<28;t++)
								{		
								TRAMEOUT[t]=tab[t-8]; 
								}
							
			pointeur_out_in=28;
			calcul_CRC(); //A COMPLETER
			TRAMEOUT[pointeur_out_in+5]=STOP[0];
			//PORTHbits.RH5=1; // demande d'interrogation carte mere
			TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE 
			//REPOSAQUIENVOYER (&ID2);
			//PORTHbits.RH5=0; // demande d'interrogation carte mere  22/04/2018
			funct = 0;	
			
			break;

			case 324:
			INTCONbits.GIE= 0; //AUTORISER LES INTERRUPTIONS GENERALES

					ACTIONENCOURS=0XBB; // ACTION ZIG 
			FONCTION[0]='Z';
			FONCTION[1]='I';
			FONCTION[2]='G';
			TRAMEOUT[0]=START[0];
			TRAMEOUT[1]=ID1[0];
			TRAMEOUT[2]=ID1[1];
			TRAMEOUT[3]=ID2[0];
			TRAMEOUT[4]=ID2[1];
			TRAMEOUT[5]=FONCTION[0];
			TRAMEOUT[6]=FONCTION[1];
			TRAMEOUT[7]=FONCTION[2];
			
			TRAMEOUT[8]=tab[0];
			TRAMEOUT[9]=tab[1];
			TRAMEOUT[10]=tab[2];
			TRAMEOUT[11]=tab[3];
			
			pointeur_out_in=12;
			calcul_CRC(); //A COMPLETER
			TRAMEOUT[pointeur_out_in+5]=STOP[0];


							tmp=0;
			//goto dieze;

		tmp2=0;
TRAMEIN[0]='x'; //REINIT
TRAMEIN[1]='x'; //REINIT
TRAMEIN[2]='x'; 
TRAMEIN[3]='x'; 
TRAMEIN[4]='x'; 



		PORTHbits.RH5=1; // demande d'interrogation carte mere
	
repeterZIG:

			//TEMPO(1);
			 
								
					TRAMERECEPTIONRS485DONNEES (18);  // SI ERR DD ON ARRETE
supertrame:

					controle_CRC();	
									if (IO_EN_COM==1) 
										goto STOPPERZIG;
	
									if ((TRAMEIN[3]=='I')&&(TRAMEIN[4]=='O')) 
								{
										if 	((TRAMEIN[8]=='D')&&(TRAMEIN[9]=='D')) //CM OCCUPEE 
										{
										PORTHbits.RH5=0;
										TEMPO(5); // ON ATTEND QUE CARTE ri RECOIT AA	
										goto STOPPERZIG; //SI LA CARTE MERE EST OCCUPEE ALORS ON ARRETE 
										}
										if 	((TRAMEIN[8]=='0')&&(TRAMEIN[9]=='0')) //CM OK 
										{
										// OK	//SI OK ON CONTINUE
											delay_qms(100); //attend un peu avant d'envoyer

										}
									
									
								}

					else
								{
								tmp2++;	
								
										if (tmp2==2) //20 si CM->Rx et Rx->CM table existence ou autre  
										goto STOPPERZIG; //sinon ON ARRETE
									
					

								//PORTHbits.RH5=0;
								goto repeterZIG;
									
								}
											


					
					delay_qms(100); //attend un peu avant d'envoyer

					TRAMEENVOIRS485DONNEES (); // ENVOI L'ORDRE ZIG A LA CARTE MERE
	
						funct=0;	
						do		

						{	
								funct =funct+1;
								TRAMERECEPTIONRS485DONNEES (9);  // SE MET ENRECPETION POUR ATTENDRE LA REPONSE TRAME 1
								controle_CRC();	

								if (IO_EN_COM==1) 
								goto STOPPERZIG;

								if (ETAT_CONNECT_MINITEL==0xFF) // SI IL Y A EU UNE INTERRUPTION AVANT ON QUITTE AVANT DE REPRENDRE
								goto STOPPERZIG;
									
								GROUPE_EN_MEMOIRE(TRAMEIN,funct-1) ;
								if (funct==3) //3ieme TRAME CONTIENT LE TYPE DE CARTE
								{
								TYPECARTE[0]=TRAMEIN[110];
								TYPECARTE[1]=TRAMEIN[111];
								TYPECARTE[2]=TRAMEIN[112];	
								TYPECARTE[3]=TRAMEIN[113];
								TYPECARTE[4]=TRAMEIN[114];
								}								


									//delay_qms(100); //attend un peu avant d'envoyer	
									INFORMATION_ERREUR[0]='0'; 
									INFORMATION_ERREUR[1]='0';			
								
								
									for (t=2;t<20;t++)
									{
									INFORMATION_ERREUR[t]='x';
									}
									ENVOIE_DES_TRAMES (0,302,&INFORMATION_ERREUR); // ON ENVOIE A CARTE 1 ICI voir les autres suivant demande


						}
						while(funct<3);
				
		
						










 

			STOPPERZIG:
		ETAT_CONNECT_MINITEL==0x00; 
		
				ACTIONENCOURS=0X00; // ACTION ZTE
			INTCONbits.GIE= 1; //AUTORISER LES INTERRUPTIONS GENERALES
			PORTHbits.RH5=0; // demande d'interrogation carte mere
			funct = 0;	






			break;


			case 341: //ZOR DEMANDE DE REGLAGE DE L'HEURE A LA CM

			ACTIONENCOURS=0X77; // ACTION ZIG 
			FONCTION[0]='Z';
			FONCTION[1]='O';
			FONCTION[2]='R';
			TRAMEOUT[0]=START[0];
			TRAMEOUT[1]=ID1[0];
			TRAMEOUT[2]=ID1[1];
			TRAMEOUT[3]=ID2[0];
			TRAMEOUT[4]=ID2[1];
			TRAMEOUT[5]=FONCTION[0];
			TRAMEOUT[6]=FONCTION[1];
			TRAMEOUT[7]=FONCTION[2];
			
			TRAMEOUT[8]=tab[0];
			TRAMEOUT[9]=tab[1];
			TRAMEOUT[10]=tab[2];
			TRAMEOUT[11]=tab[3];
			
			pointeur_out_in=12;
			calcul_CRC(); //A COMPLETER
			TRAMEOUT[pointeur_out_in+5]=STOP[0];

 
		 	tmp2=0;
			PORTHbits.RH5=1; // demande d'interrogation carte mere
			//TEMPO(1);
			repeterZOR: 
								
					TRAMERECEPTIONRS485DONNEES (9);  // SI ERR DD ON ARRETE
					controle_CRC();	
									if ((TRAMEIN[3]=='I')&&(TRAMEIN[4]=='O')) 
								{
										if 	((TRAMEIN[8]=='D')&&(TRAMEIN[9]=='D')) //CM OCCUPEE 
										{
										PORTHbits.RH5=0;
										goto STOPPERZOR; //SI LA CARTE MERE EST OCCUPEE ALORS ON ARRETE 
										}
										if 	((TRAMEIN[8]=='0')&&(TRAMEIN[9]=='0')) //CM OCCUPEE 
										{
										// OK	//SI OK ON CONTINUE
										}
									


								} 

					else
								{
								tmp2++;	

										if (tmp2==30)
										goto STOPPERZOR; //SI ON CAPTE UNE TRAME NON DESTINEE 5 fois ALORS ON ARRETE LE BUS EST OCCUPE
										if ((TRAMEIN[1]=='C') && (TRAMEIN[3]=='A')) //TRAME #CMAUERR00xxxxxx DETECTEE
										goto STOPPERZOR;
										if ((TRAMEIN[0]=='*')) //on recoit juste *
										goto STOPPERZOR;
	
								goto repeterZOR;
									
								}
											


					
					delay_qms(100); //attend un peu avant d'envoyer

					TRAMEENVOIRS485DONNEES (); // ENVOI L'ORDRE ZOR LA CARTE MERE
	
						
								TRAMERECEPTIONRS485DONNEES (9);  // SE MET ENRECPETION POUR ATTENDRE LA REPONSE TRAME PARAMETRE
								controle_CRC();	
									if (ETAT_CONNECT_MINITEL==0xFF) // SI IL Y A EU UNE INTERRUPTION AVANT ON QUITTE AVANT DE REPRENDRE
								goto STOPPERZOR;
								I2CHORLG=0;  
								
								for (funct=0;funct<12;funct++)
								{	
								HORLOGET[funct]=TRAMEIN[8+funct];//hh mm ss JJ MM AA 								 	
								}	
								entrer_trame_horloge (); //ECRITURE 
								I2CHORLG=1;

	 						
					
									delay_qms(300); //attend un peu avant d'envoyer
					
									INFORMATION_ERREUR[0]='0'; //
									INFORMATION_ERREUR[1]='0';			
								
								
									for (t=2;t<20;t++)
									{
									INFORMATION_ERREUR[t]='x';
									}
									ENVOIE_DES_TRAMES (0,302,&INFORMATION_ERREUR); // ON ENVOIE A CARTE M ICI voir les autres suivant demande
								
			
					
			
 

			LIRE_HEURE_MINITEL();	
				
		
						

 
 







 

			STOPPERZOR:
			ETAT_CONNECT_MINITEL==0xFF;
		
				ACTIONENCOURS=0X00; // ACTION ZTE
			INTCONbits.GIE= 1; //AUTORISER LES INTERRUPTIONS GENERALES
			PORTHbits.RH5=0; // demande d'interrogation carte mere
			funct = 0;	





			break;







			case 336: //ZPL 
			INTCONbits.GIE= 0;			

							ACTIONENCOURS=0XBB; // ACTION ZIG 
			FONCTION[0]='Z';
			FONCTION[1]='P';
			FONCTION[2]='L';
			TRAMEOUT[0]=START[0];
			TRAMEOUT[1]=ID1[0];
			TRAMEOUT[2]=ID1[1];
			TRAMEOUT[3]=ID2[0];
			TRAMEOUT[4]=ID2[1];
			TRAMEOUT[5]=FONCTION[0];
			TRAMEOUT[6]=FONCTION[1];
			TRAMEOUT[7]=FONCTION[2];
			
			TRAMEOUT[8]=tab[0];
			TRAMEOUT[9]=tab[1];
			TRAMEOUT[10]=tab[2];
			TRAMEOUT[11]=tab[3];
			
			pointeur_out_in=12;
			calcul_CRC(); //A COMPLETER
			TRAMEOUT[pointeur_out_in+5]=STOP[0];

			tmp2=0;
			tmp=0;
			//goto dieze;


  


			PORTHbits.RH5=1; // demande d'interrogation carte mere
			//TEMPO(1);
			repeterZPL: 
								
					TRAMERECEPTIONRS485DONNEES (9);  // SI ERR DD ON ARRETE
					controle_CRC();	



									if ((TRAMEIN[3]=='I')&&(TRAMEIN[4]=='O')) 
								{
										if 	((TRAMEIN[8]=='D')&&(TRAMEIN[9]=='D')) //CM OCCUPEE 
										{
										PORTHbits.RH5=0;
										TEMPO(5); // ON ATTEND QUE CARTE ri RECOIT AA
										goto STOPPERZPL; //SI LA CARTE MERE EST OCCUPEE ALORS ON ARRETE 
										}
										if 	((TRAMEIN[8]=='0')&&(TRAMEIN[9]=='0')) //CM OCCUPEE 
										{
										// OK	//SI OK ON CONTINUE
										}
									


								}

					else
								{
								tmp2++;	
								
										if (tmp2==30)
										goto STOPPERZPL; //SI ON CAPTE UNE TRAME NON DESTINEE 5 fois ALORS ON ARRETE LE BUS EST OCCUPE
										if ((TRAMEIN[1]=='C') && (TRAMEIN[3]=='A')) //TRAME #CMAUERR00xxxxxx DETECTEE
										goto STOPPERZPL;
										if ((TRAMEIN[0]=='*')) //on recoit juste *
										goto STOPPERZOR;	
								goto repeterZPL;
									
								}
											
  

					
					delay_qms(100); //attend un peu avant d'envoyer

					TRAMEENVOIRS485DONNEES (); // ENVOI L'ORDRE ZIG A LA CARTE MERE
	
						
								TRAMERECEPTIONRS485DONNEES (18);  // SE MET ENRECPETION POUR ATTENDRE LA REPONSE TRAME PARAMETRE
								controle_CRC();
								if (ETAT_CONNECT_MINITEL==0xFF) // SI IL Y A EU UNE INTERRUPTION AVANT ON QUITTE AVANT DE REPRENDRE
								goto STOPPERZPL;	
								if ((TRAMEIN[5]=='E')&&(TRAMEIN[6]=='R')) //SI ERR SOIT INTERRUPTION AUTOMATE CAPTEE
								goto STOPPERZPL;	
								ECRIRE_EEPROMBYTE(6,1024,&TRAMEIN,5); //ENREGISTRER PARAM. EEPROM		
					
									delay_qms(300); //attend un peu avant d'envoyer

									LIRE_EEPROM(6,1024,&TRAMEOUT,5);
					
				
		
						










 

			STOPPERZPL:
			ETAT_CONNECT_MINITEL==0x00;
		
				ACTIONENCOURS=0X00; // ACTION ZTE
			INTCONbits.GIE= 1; //AUTORISER LES INTERRUPTIONS GENERALES
			PORTHbits.RH5=0; // demande d'interrogation carte mere
			funct = 0;	

						




			break;






			case 333 :  // ZTE
			INTCONbits.GIE= 1;
				ACTIONENCOURS=0XEE; // ACTION ZIZ
			FONCTION[0]='Z';
			FONCTION[1]='T';
			FONCTION[2]='E';
			TRAMEOUT[0]=START[0];
			TRAMEOUT[1]=ID1[0];
			TRAMEOUT[2]=ID1[1];
			TRAMEOUT[3]=ID2[0];
			TRAMEOUT[4]=ID2[1];
			TRAMEOUT[5]=FONCTION[0];
			TRAMEOUT[6]=FONCTION[1];
			TRAMEOUT[7]=FONCTION[2];
			
			TRAMEOUT[8]=tab[0];
			TRAMEOUT[9]=tab[1];





			pointeur_out_in=10;
			calcul_CRC(); //A COMPLETER
			TRAMEOUT[pointeur_out_in+5]=STOP[0];
		//	PORTHbits.RH5=1; // demande d'interrogation carte mere

			//TEMPO(1); //attend un peu avant d'envoyer

	//		TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE 
			//REPOSAQUIENVOYER (&ID2);
		//	PORTHbits.RH5=0;

									tmp2=0; //SYNCHRONISATION ON ATTEND "%" debut supertrame
			tmp=0;
			//goto dieze;





PORTHbits.RH5=1; // demande d'interrogation carte mere
//TEMPO(1);
repeterZTE:
				
					
				
					
					TRAMERECEPTIONRS485DONNEES (9);  // SI ERR DD ON ARRETE
					controle_CRC();	
					
											
					if ((TRAMEIN[3]=='I')&&(TRAMEIN[4]=='O')) 
								{
										if 	((TRAMEIN[8]=='D')&&(TRAMEIN[9]=='D')) //CM OCCUPEE 
										{
										PORTHbits.RH5=0;
										goto STOPPERZTE; //SI LA CARTE MERE EST OCCUPEE ALORS ON ARRETE 
										}
										if 	((TRAMEIN[8]=='0')&&(TRAMEIN[9]=='0')) //CM OCCUPEE 
										{
										// OK	//SI OK ON CONTINUE
										}
									


								}

					else
								{
								tmp2++;	

										if (tmp2==30)
										goto STOPPERZTE; //SI ON CAPTE UNE TRAME NON DESTINEE 5 fois ALORS ON ARRETE LE BUS EST OCCUPE

								goto repeterZTE;
									
								} 
						
								

					//if (NORECEPTION==0xFF) // RIEN REcU
					//			{
					//			tmp2=tmp2+1;
					//			
					//			PORTHbits.RH5=0; // demande d'interrogation carte mere
								//delay_qms(300); //attend un peu avant d'envoyer
					//			goto repeterZTE;
					//			}
							


					delay_qms(100); //attend un peu avant d'envoyer

					TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE MERE
	





tmp2=0;


rep3:
			if (tmp2<=3) //3 ESSAIS pour le % de la super trame
				
					{
					//PORTHbits.RH5=1; // demande d'interrogation carte mere
					//TEMPO(1);
	
		//			PORTHbits.RH5=0; // demande d'interrogation carte mere
					//TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE 
dieze:
					tmp=RECEVOIR_RS485(9); // ON ATTEND '%'
					
								if (NORECEPTION==0xFF) // RIEN REcU
								{
								tmp2=tmp2+1;
								
								//PORTHbits.RH5=0; // demande d'interrogation carte mere
								//delay_qms(300); //attend un peu avant d'envoyer
								goto rep3;
								}
					}
					else			
					{
					if (tmp=='%')
					break;
					}
					


 			
			
//RECEPTION DES TRAMES EXISTANCES 

			funct = 0;

								do		
					
								{	
								
								TRAMERECEPTIONRS485DONNEES (9);  // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
									if (ETAT_CONNECT_MINITEL==0xFF) // SI IL Y A EU UNE INTERRUPTION AVANT ON QUITTE AVANT DE REPRENDRE
							goto STOPPERZTE;

								controle_CRC();	
							
								funct =funct+1;
								if (TRAMEIN[0]!='$')
								SAV_EXISTENCE(&TRAMEIN); 
					
							//	recuperer_trame_memoire (1); //STOCKAGE EN MMOIRE DE L'INFORMATION DU CAPTEUR 
					
								if (funct==25) //BLOQUE
								{
					
							
								TRAMEIN[0]='$';
					
								}
								
					 
					 
								}
					 
								while (TRAMEIN[0]!='$'); //FIN ATTENTION SI ON BLOQUE IL FAUT ATTENDRE 25 TRAMES D'AILLEURS

			


			funct = 0;
//			 LECTURE DES TABLES
			for (funct=0;funct<100;funct++)
			{
			LIRE_EEPROM(3,128*funct+30000,&TRAMEOUT,4); //LIRE DANS EEPROM
			TRAMEIN[0]!='$';
			}


STOPPERZTE:
				ETAT_CONNECT_MINITEL==0xFF;
		
				ACTIONENCOURS=0X00; // ACTION ZTE
			INTCONbits.GIE= 1; //AUTORISER LES INTERRUPTIONS GENERALES
			PORTHbits.RH5=0; // demande d'interrogation carte mere
			funct = 0;	



	



















 



			break;






				case 343 :  //ZIZ 
			
		
						//DEMANDE INFOS CAPTEUR A LA CARTE RELAIS exemple : 
						//	DEMANDE_INFOS_CAPTEURS[0]='0';
						//																					DEMANDE_INFOS_CAPTEURS[1]='0';
						//																					DEMANDE_INFOS_CAPTEURS[2]='1';
						//																					DEMANDE_INFOS_CAPTEURS[3]='0';
						//																					DEMANDE_INFOS_CAPTEURS[4]='0';
						//																					DEMANDE_INFOS_CAPTEURS[5]='0';
						//																					DEMANDE_INFOS_CAPTEURS[6]='0';
						//																					DEMANDE_INFOS_CAPTEURS[7]='3';
						//																					DEMANDE_INFOS_CAPTEURS[8]='0';
						//																					DEMANDE_INFOS_CAPTEURS[9]='0';


						//																					ENVOIE_DES_TRAMES (1,276,&DEMANDE_INFOS_CAPTEURS);


 
		INTCONbits.GIE= 1; //AUTORISER LES INTERRUPTIONS GENERALES
		ACTIONENCOURS=0XDD; // ACTION ZIZ


	

			FONCTION[0]='Z';
			FONCTION[1]='I';
			FONCTION[2]='Z';

			TRAMEOUT[0]=START[0];
			TRAMEOUT[1]=ID1[0];
			TRAMEOUT[2]=ID1[1];
			TRAMEOUT[3]=ID2[0];
			TRAMEOUT[4]=ID2[1];
			TRAMEOUT[5]=FONCTION[0];
			TRAMEOUT[6]=FONCTION[1];
			TRAMEOUT[7]=FONCTION[2];
			TRAMEOUT[8]=tab[0]; 
			TRAMEOUT[9]=tab[1];
			TRAMEOUT[10]=tab[2];
			TRAMEOUT[11]=tab[3];
			TRAMEOUT[12]=tab[4];
			if (MODERAPID==0)
			TRAMEOUT[13]='-';
 			else
			TRAMEOUT[13]='/';
			TRAMEOUT[14]=tab[5];
			TRAMEOUT[15]=tab[6];
			TRAMEOUT[16]=tab[7];
			TRAMEOUT[17]=tab[8];
			TRAMEOUT[18]=tab[9];


		  
		 //	INTCONbits.GIE= 0;
  
			pointeur_out_in=19;          // SAUVEGARDE POINTEUR DU TABLEAU
			calcul_CRC();     // CALCUL LE CRC ET LE RAJOUTE AU TABLEAU TRAMEOUT[]
			TRAMEOUT[pointeur_out_in+5]=STOP[0]; // RAJOUT LE BYTE DE STOP 
		



PORTHbits.RH5=1; // demande d'interrogation carte mere
//TEMPO(1);
//PORTHbits.RH5=0; 
repeterZIZ:
				
					
				
					
					TRAMERECEPTIONRS485DONNEES (18);  // SI ERR DD ON ARRETE
					controle_CRC();	
					
						if (IO_EN_COM==1)
						 {

						goto erreurcomIOCM;

						}				


					if ((TRAMEIN[3]=='I')&&(TRAMEIN[4]=='O')) 
								{
										if 	((TRAMEIN[8]=='D')&&(TRAMEIN[9]=='D')) //CM OCCUPEE 
										{
										//PORTHbits.RH5=0;
										TEMPO(5); // ON ATTEND QUE CARTE ri RECOIT AA



										goto STOPPERZIZ; //SI LA CARTE MERE EST OCCUPEE ALORS ON ARRETE 
										}
										


										if 	((TRAMEIN[8]=='0')&&(TRAMEIN[9]=='0')) //CM OCCUPEE 
										{

										// OK	//SI OK ON CONTINUE
										}
									


								}

					else
								{
								//tmp2++;	

								//		if (tmp2==5)
										goto STOPPERZIZ; //SI ON CAPTE UNE TRAME NON DESTINEE 5 fois ALORS ON ARRETE LE BUS EST OCCUPE

								//goto repeterZIZ;
									
								}
						
								

					//if (NORECEPTION==0xFF) // RIEN REcU
					//			{
					//			tmp2=tmp2+1;
					//			
					//			PORTHbits.RH5=0; // demande d'interrogation carte mere
								//delay_qms(300); //attend un peu avant d'envoyer
					//			goto repeterZTE;
					//			}
							


					delay_qms(100); //attend un peu avant d'envoyer AVANT IL Y  AVAIT 300 MAIS ??? PB TROP TARD

					TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE MERE





	
 
tmp2=0; //SYNCHRONISATION ON ATTEND "%" debut supertrame
tmp=0;
rep2:
			if (tmp2<=10) //10 ESSAIS
				
					{

					//TEMPO(1); //attend un peu avant d'envoyer
					//TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE 
					//PORTHbits.RH5=0; // 
					//TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE 
					tmp=RECEVOIR_RS485(9); // ON ATTEND '%'
					
						if (IO_EN_COM==1) 
						goto STOPPERZIZ;
	
								if (tmp!='%')
								{
								tmp2=tmp2+1;
									
								
								delay_qms(300); //attend un peu avant d'envoyer		

								goto rep2;
								}
					}
					else			
					{
					if (tmp=='%')
					break;
					}
					
	 				


			
 

				funct = 0;

//PORTHbits.RH5=0;

			do		

			{
			funct =funct+1;
			if (INTCON3bits.INT2IF) //L'AUTOMATE VEUT PARLER
			goto STOPPERZIZ;	
	 
			
			TRAMERECEPTIONRS485DONNEES (9);  // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
			controle_CRC();	
			
						if (IO_EN_COM==1) 
						goto erreurcomIOCM;
	
			
			if ((TRAMEIN[0]=='#')||(TRAMEIN[0]=='$')) //PAS DESTINEE A LA CARTE IO R1CM
			funct=0;
			else
			goto STOPPERZIZ;

			if (TRAMEIN[3]!='I') //PAS DESTINEE A LA CARTE IO R1CM
			goto STOPPERZIZ;



			if (ETAT_CONNECT_MINITEL==0xFF) // SI IL Y A EU UNE INTERRUPTION AVANT ON QUITTE AVANT DE REPRENDRE
			goto STOPPERZIZ;

			if (INTCON3bits.INT2IF) //L'AUTOMATE VEUT PARLER
			goto STOPPERZIZ;
			if (TRAMEIN[0]!='$')
			{
			recuperer_trame_memoire (1); //STOCKAGE EN MMOIRE DE L'INFORMATION DU CAPTEUR A REMETTRE

				if (INTCON3bits.INT2IF) //L'AUTOMATE VEUT PARLER
				goto STOPPERZIZ;	
				INFORMATION_ERREUR[0]='0'; //
				INFORMATION_ERREUR[1]='0';			
			
			
				for (t=2;t<20;t++)
				{
				INFORMATION_ERREUR[t]='x';
				}
				if (INTCON3bits.INT2IF) //L'AUTOMATE VEUT PARLER
				goto STOPPERZIZ;
				ENVOIE_DES_TRAMES (0,302,&INFORMATION_ERREUR); // ON ENVOIE A CARTE 1 ICI voir les autres suivant demande
			}

				
			//if (funct==2)
			//TRAMEOUT[0]='#';
			//DEMANDE_INFOS_CAPTEURS[0]='#';
			//DEMANDE_INFOS_CAPTEURS[1]='0';
			//DEMANDE_INFOS_CAPTEURS[2]='0';
			//DEMANDE_INFOS_CAPTEURS[3]='1';
			//DEMANDE_INFOS_CAPTEURS[4]='0';
			//DEMANDE_INFOS_CAPTEURS[5]='1';

			//LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM
			//TRAMEOUT[0]='#';
			}


			
			

			while ((TRAMEIN[1]!='$')&&(TRAMEIN[0]!='$'));




erreurcomIOCM:
		//	if (IO_EN_COM==1)
		//	{
		//	RS485('$');
		//	RS485('$');
		//	RS485('$');
		//	}


STOPPERZIZ:



			
				
		
			PORTHbits.RH5=0; // demande d'interrogation carte mere
			INTCON3bits.INT2IF=0;
			funct = 0;
			ACTIONENCOURS=0XCC; // ACTION CREATION MODIFICATION
			ETAT_CONNECT_MINITEL=0x00;
			INTCONbits.GIE= 1; //AUTORISER LES INTERRUPTIONS GENERALES





			break; 



			case 329:

			FONCTION[0]='Z';
			FONCTION[1]='C';
			FONCTION[2]='R';
			TRAMEOUT[0]=START[0];
			TRAMEOUT[1]=ID1[0];
			TRAMEOUT[2]=ID1[1];
			TRAMEOUT[3]=ID2[0];
			TRAMEOUT[4]=ID2[1];
			TRAMEOUT[5]=FONCTION[0]; 
			TRAMEOUT[6]=FONCTION[1];
			TRAMEOUT[7]=FONCTION[2];
			
			//TRAMEOUT[8]=tab[0];
			//TRAMEOUT[9]=tab[1];

			//recuperer_trame_memoire (0); //TRANSFORME LA TRAMEIN EN FORMAT MEMOIRE SUR TRAMEOUT
			//intervertirOUT_IN();
			pointeur_out_in=8;
			TRANSFORMER_TRAME_MEM_EN_COM (); // TRANSFORME LA TRAMEIN MEMOIRE en TRAMEOUUT COM
			calcul_CRC(); //A COMPLETER
			TRAMEOUT[pointeur_out_in+5]=STOP[0];


			//pointeur_out_in=10;
			//calcul_CRC(); //A COMPLETER
			//TRAMEOUT[pointeur_out_in+5]=STOP[0];

			//PORTHbits.RH5=1; // demande d'interrogation carte mere
			//TEMPO(1); //attend un peu avant d'envoyer
tmp2=0;
TERMINEACTION=0;
repeterZCR: 
		PORTHbits.RH5=1; // demande d'interrogation carte mere			
					
			//	LEDC=1;
					
					TRAMERECEPTIONRS485DONNEES (18);  // on attend 
					controle_CRC();	
				//	LEDC=0;						
					if ((TRAMEIN[3]=='I')&&(TRAMEIN[4]=='O')) 
								{
										if 	((TRAMEIN[8]=='D')&&(TRAMEIN[9]=='D')) //CM OCCUPEE 
										{
										TERMINEACTION=3; //PROBLEME
										//PORTHbits.RH5=0;
										goto STOPPERZCR; //SI LA CARTE MERE EST OCCUPEE ALORS ON ARRETE 
										}
										if 	((TRAMEIN[8]=='0')&&(TRAMEIN[9]=='0')) //CM NON OCCUPEE 
										{
										// OK	//SI OK ON CONTINUE
										}
								


								}

					else // rien ou en r�alit� rien
								{
								tmp2++;	

										if (tmp2==5)
										{
										goto STOPPERZCR; //SI ON CAPTE UNE TRAME NON DESTINEE 5 fois ALORS ON ARRETE LE BUS EST OCCUPE
										TERMINEACTION=2;//PROBLEME
										}
								PORTHbits.RH5=0; // demande d'interrogation carte mere
								goto repeterZCR;
									
								}








			TRAMEIN[0]='-';

			
			TEMPO(1); // ON ATTEND 1S AVANT D'ENVOYER
			TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE 

					
					TRAMERECEPTIONRS485DONNEES (9);  // SI ERR 00 LA CARTE M A BIEN RECU LA TRAME


encoreZCR:
					TRAMERECEPTIONRS485DONNEES (20);  // SI ERR FF TT EST OK
					controle_CRC();
					if 	(NORECEPTION==0xFF) // RIEN RECU	EN 12S
					{

					}
					else
					{
									if ((TRAMEIN[3]=='I')&&(TRAMEIN[4]=='O')) //SI TRAME PAS RECU PROBLEME
									{
									TERMINEACTION=1; //L'ACTION A ETE FAITE

													//OK
									}
									else
									{
									
									goto encoreZCR;
									}
					}

					



				
STOPPERZCR:
		
			
			funct = 0;
			ACTIONENCOURS=0X00; // ACTION REPOS
			//INTCONbits.GIE= 1; //AUTORISER LES INTERRUPTIONS GENERALES
			
			PORTHbits.RH5=0; // demande d'interrogation carte mere
			//LEDC=1;
			//LEDD=1;



			break;

 

			case 320:

			FONCTION[0]='Z';
			FONCTION[1]='F';
			FONCTION[2]='F';
			TRAMEOUT[0]=START[0];
			TRAMEOUT[1]=ID1[0];
			TRAMEOUT[2]=ID1[1];
			TRAMEOUT[3]=ID2[0];
			TRAMEOUT[4]=ID2[1];
			TRAMEOUT[5]=FONCTION[0];
			TRAMEOUT[6]=FONCTION[1];
			TRAMEOUT[7]=FONCTION[2];
			
			TRAMEOUT[8]=DEMANDE_INFOS_CAPTEURS[0];
			
			TRAMEOUT[9]=DEMANDE_INFOS_CAPTEURS[1];

			TRAMEOUT[10]=DEMANDE_INFOS_CAPTEURS[2];

			TRAMEOUT[11]=DEMANDE_INFOS_CAPTEURS[3];

			TRAMEOUT[12]=DEMANDE_INFOS_CAPTEURS[4];

			pointeur_out_in=13;

			calcul_CRC(); //A COMPLETER
			TRAMEOUT[pointeur_out_in+5]=STOP[0];


			//pointeur_out_in=10;
			//calcul_CRC(); //A COMPLETER
			//TRAMEOUT[pointeur_out_in+5]=STOP[0];

			
tmp2=0;
TERMINEACTION=0;
PORTHbits.RH5=1; // demande d'interrogation carte mere
			
repeterZFF:	
		
			TRAMERECEPTIONRS485DONNEES (9);  // on attend ERR00
					controle_CRC();	
				//	LEDC=0;						
					if ((TRAMEIN[3]=='I')&&(TRAMEIN[4]=='O')) 
								{
										if 	((TRAMEIN[8]=='D')&&(TRAMEIN[9]=='D')) //CM OCCUPEE 
										{
										//PORTHbits.RH5=0;
										TERMINEACTION=3; //PROBLEME
										goto STOPPERZFF; //SI LA CARTE MERE EST OCCUPEE ALORS ON ARRETE 
										}
										if 	((TRAMEIN[8]=='0')&&(TRAMEIN[9]=='0')) //CM NON OCCUPEE 
										{
										// OK	//SI OK ON CONTINUE
										}			
								}

								else // rien ou en r�alit� rien
								{
								tmp2++;	

										if (tmp2==5)
										{
										goto STOPPERZFF; //SI ON CAPTE UNE TRAME NON DESTINEE 5 fois ALORS ON ARRETE LE BUS EST OCCUPE
										TERMINEACTION=2; //PROBLEME	
										}	
								//PORTHbits.RH5=0; // demande d'interrogation carte mere
								goto repeterZFF;
									
								}



			TRAMEIN[0]='-';

			
			TEMPO(1); // ON ATTEND 1S AVANT D'ENVOYER
			TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE 

					
					TRAMERECEPTIONRS485DONNEES (9);  // SI ERR 00 LA CARTE M A BIEN RECU LA TRAME
			



encoreZFF:
					TRAMERECEPTIONRS485DONNEES (20);  // SI ERR FF TT EST OK
					controle_CRC();
					if 	(NORECEPTION==0xFF) // RIEN RECU	EN 12S
					{

					}
					else
					{
									if ((TRAMEIN[3]=='I')&&(TRAMEIN[4]=='O')) //SI TRAME PAS RECU PROBLEME
									{


									TERMINEACTION=1; //L'ACTION A ETE FAITE

							





									//OK
									}
									else
									{
									
									goto encoreZFF;
									}
					}





STOPPERZFF:



			PORTHbits.RH5=0; // demande d'interrogation carte mere




			break;





































			default :
	 	
						
			break;

 



	}


}







//////////////////////////////////////////////////////////////////
// ENVOI D'UNE TRAME COMPLETE RS485          		   	        //
//////////////////////////////////////////////////////////////////

//exemple #CMR1DIC03300R111122223333BRA/EEEE/22/1COMMENTAIRESICIROB0332611152500102000045678110865088822222*

// STOCKAGE DU RESULTAT DANS LA TRAMEOUT[]

void TRAMEENVOIRS485DONNEES (void)
{
unsigned char u;
unsigned char tmp;
tmp=0;

do
{
ret1:

		u=TRAMEOUT[tmp];
		RS485(u);
		//if ((u!='=') && (tmp==0))
		//goto ret1;
	
		
		tmp=tmp+1;
}
while ((u!='*') && (tmp<200));


//delay_qms(100);
 
}



void calcul_CRC(void)
{

 
CRC485[0]='2';
CRC485[1]='2';
CRC485[2]='2';
CRC485[3]='2';
CRC485[4]='2';


TRAMEOUT[pointeur_out_in]=CRC485[0];
TRAMEOUT[pointeur_out_in+1]=CRC485[1];
TRAMEOUT[pointeur_out_in+2]=CRC485[2];
TRAMEOUT[pointeur_out_in+3]=CRC485[3];
TRAMEOUT[pointeur_out_in+4]=CRC485[4];
TRAMEOUT[pointeur_out_in+5]='*';

}


















void TEMPO(char seconde)
{
char i;


for (i=0;i<seconde;i++)
{
delay_qms(100);
delay_qms(100);
delay_qms(100);
delay_qms(100);
delay_qms(100);
delay_qms(100);
delay_qms(100);
delay_qms(100);
delay_qms(100);
delay_qms(100);
}

}


void controle_CRC(void)
{

 
CRC485[0]=TRAMEIN[pointeur_out_in];
CRC485[1]=TRAMEIN[pointeur_out_in+1];
CRC485[2]=TRAMEIN[pointeur_out_in+2];
CRC485[3]=TRAMEIN[pointeur_out_in+3];
CRC485[4]=TRAMEIN[pointeur_out_in+4];



}
 




//////////////////////////////////////////////////////////////////
// RECEPTION D'UNE TRAME COMPLETE RS485          		   		//
//////////////////////////////////////////////////////////////////

//exemple #R1CMDIC03300R111122223333BRA/EEEE/22/1COMMENTAIRESICIROB0332611152500102000045678110865088822222*

// STOCKAGE DU RESULTAT DANS LA TRAMEIN[]


void TRAMERECEPTIONRS485DONNEES (char tout) 
{
unsigned char u;
unsigned char tmp;


tmp=0;

do
{
ret1:
		u=RECEVOIR_RS485(tout);

		if (tmp==0) // erreur trame 1 et 2 existence on recoit '.' ou autre d'abord puis '#' sur le premier caract
		{
		if (u=='$')
		goto tramedecal; 	
		

		if((u!='#')&&(NORECEPTION!=0xff))				
		goto ret1;
		}
		
tramedecal:
		TRAMEIN[tmp]=u;
			
		tmp=tmp+1;
}
while ((u!='$')&&(u!='*') && (tmp<200) && (NORECEPTION!=0xff));

 
delay_qms(10);
 
}
  
 
 

//////////////////////////////////////////////////////////////////
// reception d'un caract�re sur le port s�rie	avec tempo max	//
//////////////////////////////////////////////////////////////////
  
   
char RECEVOIR_RS485(char ttout)
{   
unsigned char Carac;
unsigned int TEMPS1,TEMPS2; 
ERREURRS:
DERE=0; //AUTORISATION DE RECEVOIR
 // PORTCbits.RC5=1; //FIN DE TRANSMISSION     		 
PORTAbits.RA4=0;
RCSTA2bits.CREN  = 1; 
TEMPS1=0;
TEMPS2=0; 
NORECEPTION=0x00;

while ((PIR3bits.RC2IF == 0)&&(TEMPS2<(ttout+1))) // max 6s pour 10
{
TEMPS1=TEMPS1+1;
if (TEMPS1>=50000) //ENVIRON 500ms
{ 
TEMPS2++; 
TEMPS1=0;
}
};       // attend une r�ception de caract�re sur RS232
//    __no_operation();

Carac = RCREG2;    // caract�re re�u
if ((Carac==0x00)) // EXCEPTION NE PAS PRENDRE EN COMPTE LE CARACTERE <NUL> voir si origine croisement TX RX CM
goto ERREURRS; //24052017
if (TEMPS2>=ttout)
NORECEPTION=0xff;
PIR3bits.RC2IF = 0;
RCSTA2bits.CREN  = 0; 
return(Carac);    // retourne le caract�re re�u sur la liaison RS232


}


















void SAV_EXISTENCE(char *tt)
{

//#CMAUZTE02aFAFAFFFFFFFFFFFFF0ax22222* on recoit pour azutomate ou CARTE IO pas sur carte mere (#R1CM02aFAFAFFFFFFFFFFFFF0ax99999*)



		unsigned int l;
		char i;
		
		
		
		l=tt[8]-48;
		l=l*10;
		l=l+tt[9]-48;
		i=l;	

		l=128*l+30000;
		
		
		TMP[0]='#';
		TMP[1]=i;
		for (i=5;i<37;i++)
		{
		TMP[i-3]=tt[5+i];
		}


ECRIRE_EEPROMBYTE(3,l,&TMP,4);

//delay_qms(100);

//LIRE_EEPROM(3,l,&TRAMEIN,4); //LIRE DANS EEPROM

//delay_qms(100);

} 




void recuperer_trame_memoire (char w)  //RECUPERE TRAME MET DANS LES VARIABLES PUIS ECRIT DANS L'EPROOM


{

int t;
int u;

u=8; // pr un TRAMOUT MEMOIRE 

TRAMEOUT[0]='#';


VOIECOD[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
VOIECOD[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
VOIECOD[2]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
VOIECOD[3]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
VOIECOD[4]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;


//ecrire dans I2C

TYPE[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
 

CMOD[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CMOD[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CMOD[2]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CMOD[3]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;


CCON[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CCON[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CCON[2]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CCON[3]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;



CREP[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CREP[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CREP[2]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CREP[3]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;

 
   
 

t=0;
for (t=0;t<13;t++) 
{
CONSTITUTION[t]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;

}
 
t=0;
for (t=0;t<18;t++) 
{
COMMENTAIRE[t]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
}
 
        

CABLE[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CABLE[1]==TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
CABLE[2]==TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;



JJ[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
JJ[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
 

MM[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
MM[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;


HH[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
HH[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;


mm[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
mm[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;



y[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;



COD[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
COD[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;




POS[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
POS[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;



  




iiii[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
iiii[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
iiii[2]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
iiii[3]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;




DISTANCE[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
DISTANCE[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
DISTANCE[2]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
DISTANCE[3]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
DISTANCE[3]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;

  

ETAT[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
ETAT[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;


VALEUR[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
VALEUR[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
VALEUR[2]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
VALEUR[3]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;


SEUIL[0]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
SEUIL[1]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
SEUIL[2]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;
SEUIL[3]=TRAMEIN[u];
TRAMEOUT[u-7]=TRAMEIN[u];
u=u+1;

TRAMEOUT[u-7]='*';



//ecriture ou pas
if (w==1)
ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEOUT),calcul_addmemoire(&TRAMEOUT),&TRAMEOUT,1); //ECRITURE EN EEPROM


LIRE_EEPROM(choix_memoire(&TRAMEOUT),calcul_addmemoire(&TRAMEOUT),&TRAMEIN,1); //LIRE DANS EEPROM
pointeur_out_in=u;

 

}


 

void CREER_TRAMEIN_CAPTEUR_DU_MINITEL(void) //CREER LA TRAME QUE L'ON VA SAV DANS LEEPROM

{

											TRAMEIN[0]='#';
											
											
											TRAMEIN[1]=VOIECOD[0];
											TRAMEIN[2]=VOIECOD[1];
											TRAMEIN[3]=VOIECOD[2];
											TRAMEIN[4]=VOIECOD[3];
											TRAMEIN[5]=VOIECOD[4];
											 
											
											//ecrire dans I2C
											
											TRAMEIN[6]=TYPE[0];
											
											TRAMEIN[7]=CMOD[0];
											TRAMEIN[8]=CMOD[1];
											TRAMEIN[9]=CMOD[2];
											TRAMEIN[10]=CMOD[3];
											
											
											TRAMEIN[11]=CCON[0];
											TRAMEIN[12]=CCON[1];
											TRAMEIN[13]=CCON[2];
											TRAMEIN[14]=CCON[3];
											
											
											TRAMEIN[15]=CREP[0];
											TRAMEIN[16]=CREP[1];
											TRAMEIN[17]=CREP[2];
											TRAMEIN[18]=CREP[3];
											
											
											TRAMEIN[19]=CONSTITUTION[0];//info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEIN,1,i);
											TRAMEIN[20]=CONSTITUTION[1];
											TRAMEIN[21]=CONSTITUTION[2];
										TRAMEIN[22]=CONSTITUTION[3];
										TRAMEIN[23]=CONSTITUTION[4];
										TRAMEIN[24]=CONSTITUTION[5];
										TRAMEIN[25]=CONSTITUTION[6];
										TRAMEIN[26]=CONSTITUTION[7];
										TRAMEIN[27]=CONSTITUTION[8];
										TRAMEIN[28]=CONSTITUTION[9];
										TRAMEIN[29]=CONSTITUTION[10];
										TRAMEIN[30]=CONSTITUTION[11];
										TRAMEIN[31]=CONSTITUTION[12];



										TRAMEIN[32]=COMMENTAIRESAV[0];//info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEIN,1,i);
										TRAMEIN[33]=COMMENTAIRESAV[1];
										TRAMEIN[34]=COMMENTAIRESAV[2];
										TRAMEIN[35]=COMMENTAIRESAV[3];
										TRAMEIN[36]=COMMENTAIRESAV[4];
										TRAMEIN[37]=COMMENTAIRESAV[5];
										TRAMEIN[38]=COMMENTAIRESAV[6];
										TRAMEIN[39]=COMMENTAIRESAV[7];
										TRAMEIN[40]=COMMENTAIRESAV[8];
										TRAMEIN[41]=COMMENTAIRESAV[9];
										TRAMEIN[42]=COMMENTAIRESAV[10];
										TRAMEIN[43]=COMMENTAIRESAV[11];
										TRAMEIN[44]=COMMENTAIRESAV[12];		
										TRAMEIN[45]=COMMENTAIRESAV[13];	
										TRAMEIN[46]=COMMENTAIRESAV[14];	
										TRAMEIN[47]=COMMENTAIRESAV[15];	
										TRAMEIN[48]=COMMENTAIRESAV[16];	
										TRAMEIN[49]=COMMENTAIRESAV[17];	


	//CODAGE

										TRAMEIN[50]=CABLE[0];
										TRAMEIN[51]=CABLE[1];		
										TRAMEIN[52]=CABLE[2];	
										TRAMEIN[53]=JJ[0];	
										TRAMEIN[54]=JJ[1];	
										TRAMEIN[55]=MM[0];	
										TRAMEIN[56]=MM[1];	
										TRAMEIN[57]=HH[0];	
										TRAMEIN[58]=HH[1];	
										TRAMEIN[59]=mm[0];	
										TRAMEIN[60]=mm[1];	

										TRAMEIN[61]=ii[0];		









									
										TRAMEIN[62]=COD[0];
										TRAMEIN[63]=COD[1];
										
										//POS
										
										TRAMEIN[64]=POS[0]; //POS
										TRAMEIN[65]=POS[1]; //POS
										
										
										TRAMEIN[66]=iiii[0];
										TRAMEIN[67]=iiii[1];
										TRAMEIN[68]=iiii[2];
										TRAMEIN[69]=iiii[3];

	//DISTANCE
										TRAMEIN[70]=DISTANCE[0];  //info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEIN,1,i);
										TRAMEIN[71]=DISTANCE[1];
										TRAMEIN[72]=DISTANCE[2];
										TRAMEIN[73]=DISTANCE[3];
										TRAMEIN[74]=DISTANCE[4];


//ETAT

					
										TRAMEIN[75]=ETAT[0];
										TRAMEIN[76]=ETAT[1];
										
										//FREQUENCE
							
										TRAMEIN[77]=VALEUR[0];
										TRAMEIN[78]=VALEUR[1];
										TRAMEIN[79]=VALEUR[2];
										TRAMEIN[80]=VALEUR[3];
										
							
										TRAMEIN[81]=SEUIL[0];
										TRAMEIN[82]=SEUIL[1];
										TRAMEIN[83]=SEUIL[2];
										TRAMEIN[84]=SEUIL[3];

											























}



void TRANSFORMER_TRAME_MEM_EN_COM (void)

{

int t;
int u;
u=8;
TRAMEOUT[0]='#';


VOIECOD[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
VOIECOD[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
VOIECOD[2]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
VOIECOD[3]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
VOIECOD[4]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;


//ecrire dans I2C

TYPE[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;


CMOD[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CMOD[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CMOD[2]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CMOD[3]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;


CCON[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CCON[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CCON[2]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CCON[3]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;



CREP[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CREP[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CREP[2]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CREP[3]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;

 
   
 

t=0;
for (t=0;t<13;t++) 
{
CONSTITUTION[t]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;

}
 
t=0;
for (t=0;t<18;t++) 
{
COMMENTAIRE[t]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
}
 
        

CABLE[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CABLE[1]==TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
CABLE[2]==TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;



JJ[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
JJ[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;


MM[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
MM[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;


HH[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
HH[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;


mm[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
mm[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;



iiii[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;



COD[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
COD[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;




POS[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
POS[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;



  




iiii[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
iiii[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
iiii[2]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
iiii[3]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;




DISTANCE[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
DISTANCE[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
DISTANCE[2]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
DISTANCE[3]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
DISTANCE[3]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;

  

ETAT[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
ETAT[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;


VALEUR[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
VALEUR[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
VALEUR[2]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
VALEUR[3]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;


SEUIL[0]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
SEUIL[1]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
SEUIL[2]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;
SEUIL[3]=TRAMEIN[u-7];
TRAMEOUT[u]=TRAMEIN[u-7];
u=u+1;

TRAMEOUT[u]='*';

//ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEIN),calcul_addmemoire(&TRAMEIN),&TRAMEIN,1); //ECRITURE EN EEPROM
//LIRE_EEPROM(choix_memoire(&TRAMEOUT),calcul_addmemoire(&TRAMEOUT),&TRAMEIN); //LIRE DANS EEPROM
pointeur_out_in=u;


}










 

void ENVOYER_GROUPE (void)
{
unsigned char g;
LIRE_EEPROM(6,0,&TMP,3);
//"#CMIOZIG00018111752001071500000003000104200002070450000003000010550002070450000003000010550003071500000003000103000*222*�K�R~"
//#CMIOZIG000XXXXXXXX00107150000000300000250000207012300000300000040000207XXXXXXXXXXXXXXXXXXX003071500000003000003000*222**��"
//NE PAS PRENDRE LE 002 2x !!!!!!!!! prendre le premier

				if (TMP[11]=='X') //L'AUTOMATE N A PAS DONNE LHEURE
				{
					TMP[11]='2';
					TMP[12]='9';
					TMP[13]='1';
					TMP[14]='1';
					TMP[15]='0';
					TMP[16]='7';
					TMP[17]='1';
					TMP[18]='5';
				}


for (g=0;g<255;g++)
TRAMEOUT[g]='-';



TRAMEOUT[0]=0x02;
for (g=1;g<60;g++) 
{
TRAMEOUT[g]=TMP[g+7];
}



for (g=84;g<108;g++)
{
TRAMEOUT[g-24]=TMP[g+7];
}






LIRE_EEPROM(6,128,&TMP,3);

//"#CMIOZIGxxxxxxxxxxx004070000000000002400253005070000000000000003000006070000000000001430300008071000000000000024000*222*�K�R~"

for (g=84;g<204;g++) 
{
TRAMEOUT[g]=TMP[g-65];
}


 


//"#CMIOZIGxxxxxxxxxxx009070000000003000001300010070000000003000001300xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*222*�K�R~"

LIRE_EEPROM(6,256,&TMP,3);

for (g=180;g<228;g++) 
{
TRAMEOUT[g]=TMP[g-161];
}

 



TRAMEOUT[228]=0x03; //ETX
CRC=CRC16(TRAMEOUT,229);
IntToChar(CRC,tableau,5);
TRAMEOUT[229]=tableau[0];
TRAMEOUT[230]=tableau[1];
TRAMEOUT[231]=tableau[2];
TRAMEOUT[232]=tableau[3];
TRAMEOUT[233]=tableau[4];
TRAMEOUT[234]=0x0D;






TRAMEENVOIRS232DONNEES (256); // ON ENVOIE LA VOIE LE CABLE

TRAMERECEPTIONRS232DONNEES (256); // ON RECOIT  A ACK OU PAS

}




void voir_qui_existe (void) 

{
char i,tpr,tpa,tda,j;
unsigned int l;
char nb;
char ENVOYEROUPAS;
unsigned TMPTDA; 
tpr=0;
tpa=0;
tda=0;



					for (i=1;i<=100;i+=1) //20 pour une carte de 1 � 99 	for (i=1;i<100;i+=1)
					{
pasdecarte:				
					//Tab_Voie_1[0]=i;
					//Tab_Voie_2[0]=i+1;
						if ((i>=1)&&(i<=20))
						{
							if ((TYPECARTE[0]!='E')&&(TYPECARTE[0]!='M')) //REAGARDE SI LA CARTE EXISTE ENCORE
							{
							i=21;
							goto pasdecarte;
							}
						}


						if ((i>=21)&&(i<=40))
						{
							if ((TYPECARTE[1]!='E')&&(TYPECARTE[1]!='M')) //REAGARDE SI LA CARTE EXISTE ENCORE
							{
							i=41;
							goto pasdecarte;
							}
						}
						if ((i>=41)&&(i<=60))
						{
							if ((TYPECARTE[2]!='E')&&(TYPECARTE[2]!='M')) //REAGARDE SI LA CARTE EXISTE ENCORE
							{
							i=61;
							goto pasdecarte;
							}
						}
						if ((i>=61)&&(i<=80))
						{
							if ((TYPECARTE[3]!='E')&&(TYPECARTE[3]!='M')) //REAGARDE SI LA CARTE EXISTE ENCORE
							{
							i=81;
							goto pasdecarte;
							}
						}
						if ((i>=81)&&(i<=100))
						{
							if ((TYPECARTE[4]!='E')&&(TYPECARTE[4]!='M')) //REAGARDE SI LA CARTE EXISTE ENCORE
							{
							//i=100;
							goto FINDETRANS;
							}
						}















					// VOIE IMPAIRE
					delay_qms(10);
					l=128*i+30000;
					LIRE_EEPROM(3,l,&TRAMEIN,4); //LIRE DANS EEPROM lexistance de toutes le voies



 
DEMANDE_INFOS_CAPTEURS[0]='#';
DEMANDE_INFOS_CAPTEURS[1]='0';
if (i>99)
{
DEMANDE_INFOS_CAPTEURS[1]='1';
DEMANDE_INFOS_CAPTEURS[2]='0';
DEMANDE_INFOS_CAPTEURS[3]='0';
}
else
{
DEMANDE_INFOS_CAPTEURS[2]=i/10+48;
DEMANDE_INFOS_CAPTEURS[3]=48+i-10*(DEMANDE_INFOS_CAPTEURS[2]-48);
}



///////////////// ENVOIE VOIE ADRESSABLE
		
ENVOYEROUPAS=0;	
		//if ((DEMANDE_INFOS_CAPTEURS[2]=='2')&&(DEMANDE_INFOS_CAPTEURS[3]=='9')) //TEST BUG 
		//TRAMEIN[2]='v';
	//	if ((DEMANDE_INFOS_CAPTEURS[2]=='2')&&(DEMANDE_INFOS_CAPTEURS[3]=='6')) //TEST BUG 
	//	j=0;
	//	if ((DEMANDE_INFOS_CAPTEURS[2]=='2')&&(DEMANDE_INFOS_CAPTEURS[3]=='1')) //TEST BUG 
	//	j=0;
		if (TRAMEIN[2]=='a') //voie meme carte adressable
		{
			tpa=0;
			for (j=0;j<17;j++)
			{
		
			DEMANDE_INFOS_CAPTEURS[4]=j/10+48;
			DEMANDE_INFOS_CAPTEURS[5]=48+j-10*(DEMANDE_INFOS_CAPTEURS[4]-48);
			
	//		if (j==0)
	//		{	
	//		LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM
	//		TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(0,0); // 0 ADRESSABLE en ancien telgat 
			
	//		}			

 			
			if (TRAMEIN[3+j]=='A')
				{

				ENVOYEROUPAS=1;	
				tpa=tpa+1;
				LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM				
				TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(1,tpa); // 0 ADRESSABLE en ancien telgat dans la trameout
				//TRAMEOUT[20*j]=&TMP
				delay_qms(10);				
				}	
 		 
			} //TOUTE LA VOIE
			TMPTDA=tpa;

					  		TRAMEOUT[24*(TMPTDA-1)+36]=0x03; //ETX
			                CRC=CRC16(TRAMEOUT,24*(TMPTDA-1)+37); //taille totale
                                                                 	IntToChar(CRC,tableau,5);
                                                                    TRAMEOUT[24*(TMPTDA-1)+37]=tableau[0];
                                                                    TRAMEOUT[24*(TMPTDA-1)+38]=tableau[1];
                                                                    TRAMEOUT[24*(TMPTDA-1)+39]=tableau[2];
                                                                    TRAMEOUT[24*(TMPTDA-1)+40]=tableau[3];
                                                                    TRAMEOUT[24*(TMPTDA-1)+41]=tableau[4];
                                                                    TRAMEOUT[24*(TMPTDA-1)+42]=0x0D;
RECOMMENCER:
																	if (ENVOYEROUPAS==1)
																	{	
                                                                   TRAMEENVOIRS232DONNEES (420); // ON ENVOIE LA VOIE LE CABLE max 396 16 TPA
                                                                   TRAMERECEPTIONRS232DONNEES (256); // ON RECOIT  A ACK OU PAS
																	}


																  switch (TRAMEIN[0]) {
                                                                      case (0x06): // SI ACK ON PASSE AU CAPTEUR SUIVANT
                                                                     goto CONTINUER1;
                                                                      case (0x0f): // SI SI OU NOACK
                                                                     nb++;
                                                                     if (nb==2){ // ON ATTEND DEUX FOIS DAVOIR ACK SINON PROBLEME ON PASSE AU PROCHAIN CAPTEUR
                                                                     goto CONTINUER1;
                                                                     } 
                                                                      else
																	{
																	
																	goto RECOMMENCER;
																	} 
                                                                      default :
                                                                        break;
                                                                       }
                                                                     nb=0;
                                                                    /////////////////////////envoyer la valeur du deuxi�me capteur   







		} // FIN ENVOIE VOIE ADRESSABLE
		 

CONTINUER1:
		



				if (TRAMEIN[2]=='r') //voie meme carte resitive envoyer juste le premier capteur
				{
				tpa=1;
				DEMANDE_INFOS_CAPTEURS[4]='0';
				DEMANDE_INFOS_CAPTEURS[5]='1';
				LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM
				TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(1,tpa); // 0 ADRESSABLE en ancien telgat dans la trameout
					delay_qms(10);		
						TRAMEOUT[24*(tpa-1)+36]=0x03; //ETX
										
									                CRC=CRC16(TRAMEOUT,24*(tpa-1)+37); //taille totale
						                                                                 	IntToChar(CRC,tableau,5);
						                                                                    TRAMEOUT[24*(tpa-1)+37]=tableau[0];
						                                                                    TRAMEOUT[24*(tpa-1)+38]=tableau[1];
						                                                                    TRAMEOUT[24*(tpa-1)+39]=tableau[2];
						                                                                    TRAMEOUT[24*(tpa-1)+40]=tableau[3];
						                                                                    TRAMEOUT[24*(tpa-1)+41]=tableau[4];
						                                                                    TRAMEOUT[24*(tpa-1)+42]=0x0D;
									
						
						
										                TRAMEENVOIRS232DONNEES (256); // ON ENVOIE LA VOIE LE CABLE
						                                TRAMERECEPTIONRS232DONNEES (256); // ON RECOIT  A ACK OU PAS
						
						
				
				}






if (TRAMEIN[2]=='v') //voie meme carte vide
{};
							if (TRAMEIN[2]=='c') //voie en court circuit
													{
						
							tpa=1;			
										DEMANDE_INFOS_CAPTEURS[4]='0';
										DEMANDE_INFOS_CAPTEURS[5]='1';
										//LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM
									//	TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(1,tpa); // 0 ADRESSABLE en ancien telgat dans la trameout	
							

										TRAMECAPTEURBIDON(&DEMANDE_INFOS_CAPTEURS);
										TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(1,tpa); // 0 ADRESSABLE en ancien telgat dans la trameout		
						
										delay_qms(10);		
											TRAMEOUT[26]='5';
											TRAMEOUT[27]='0';//S/R
											TRAMEOUT[28]='0';//0999mbar car codage 1 donc cc
											TRAMEOUT[29]='9';
											TRAMEOUT[30]='9';
											TRAMEOUT[31]='9';		
						
									  		TRAMEOUT[24*(tpa-1)+36]=0x03; //ETX
										
									                CRC=CRC16(TRAMEOUT,24*(tpa-1)+37); //taille totale
						                                                                 	IntToChar(CRC,tableau,5);
						                                                                    TRAMEOUT[24*(tpa-1)+37]=tableau[0];
						                                                                    TRAMEOUT[24*(tpa-1)+38]=tableau[1];
						                                                                    TRAMEOUT[24*(tpa-1)+39]=tableau[2];
						                                                                    TRAMEOUT[24*(tpa-1)+40]=tableau[3];
						                                                                    TRAMEOUT[24*(tpa-1)+41]=tableau[4];
						                                                                    TRAMEOUT[24*(tpa-1)+42]=0x0D;
									
						
						
										                TRAMEENVOIRS232DONNEES (256); // ON ENVOIE LA VOIE LE CABLE
						                                TRAMERECEPTIONRS232DONNEES (256); // ON RECOIT  A ACK OU PAS
						
						
						
						
						}






						if (TRAMEIN[2]=='h') //fusible HS voies P ou IMP
						{
						
							tpa=1;
										DEMANDE_INFOS_CAPTEURS[4]='0';
										DEMANDE_INFOS_CAPTEURS[5]='1';
									

										//LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM


										TRAMECAPTEURBIDON(&DEMANDE_INFOS_CAPTEURS);
										TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(1,tpa); // 0 ADRESSABLE en ancien telgat dans la trameout	
										
																
									
													delay_qms(10);		
											TRAMEOUT[26]='5';
											TRAMEOUT[27]='0';//S/R
											TRAMEOUT[28]='0';//0888mbar car codage 1 donc fusible hs
											TRAMEOUT[29]='8';
											TRAMEOUT[30]='8';
											TRAMEOUT[31]='8';		
						
									  		TRAMEOUT[24*(tpa-1)+36]=0x03; //ETX
										
									                CRC=CRC16(TRAMEOUT,24*(tpa-1)+37); //taille totale
						                                                                 	IntToChar(CRC,tableau,5);
						                                                                    TRAMEOUT[24*(tpa-1)+37]=tableau[0];
						                                                                    TRAMEOUT[24*(tpa-1)+38]=tableau[1];
						                                                                    TRAMEOUT[24*(tpa-1)+39]=tableau[2];
						                                                                    TRAMEOUT[24*(tpa-1)+40]=tableau[3];
						                                                                    TRAMEOUT[24*(tpa-1)+41]=tableau[4];
						                                                                    TRAMEOUT[24*(tpa-1)+42]=0x0D;
									
						
						
										                TRAMEENVOIRS232DONNEES (256); // ON ENVOIE LA VOIE LE CABLE
						                                TRAMERECEPTIONRS232DONNEES (256); // ON RECOIT  A ACK OU PAS
						
						
						
						
						}






				if (TRAMEIN[2]=='i') //adressable sur carte resistive ou a voir
				{

				tpa=1;
				DEMANDE_INFOS_CAPTEURS[4]='0';
				DEMANDE_INFOS_CAPTEURS[5]='1';
			//	LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM
			//	TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(1,tpa); // 0 ADRESSABLE en ancien telgat dans la trameout	
			

										TRAMECAPTEURBIDON(&DEMANDE_INFOS_CAPTEURS);
										TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(1,tpa); // 0 ADRESSABLE en ancien telgat dans la trameout	

				delay_qms(10);		
					TRAMEOUT[26]='5';
					TRAMEOUT[27]='0';//S/R
					TRAMEOUT[28]='0';//0777mbar car codage 1 donc incident
					TRAMEOUT[29]='7';
					TRAMEOUT[30]='7';
					TRAMEOUT[31]='7';		

			  		TRAMEOUT[24*(tpa-1)+36]=0x03; //ETX
				
			                CRC=CRC16(TRAMEOUT,24*(tpa-1)+37); //taille totale
                                                                 	IntToChar(CRC,tableau,5);
                                                                    TRAMEOUT[24*(tpa-1)+37]=tableau[0];
                                                                    TRAMEOUT[24*(tpa-1)+38]=tableau[1];
                                                                    TRAMEOUT[24*(tpa-1)+39]=tableau[2];
                                                                    TRAMEOUT[24*(tpa-1)+40]=tableau[3];
                                                                    TRAMEOUT[24*(tpa-1)+41]=tableau[4];
                                                                    TRAMEOUT[24*(tpa-1)+42]=0x0D;
			


				                TRAMEENVOIRS232DONNEES (256); // ON ENVOIE LA VOIE LE CABLE
                                TRAMERECEPTIONRS232DONNEES (256); // ON RECOIT  A ACK OU PAS


				}







//LIRE_EEPROM(choix_memoire(&TRAMEOUT),calcul_addmemoire(&TRAMEOUT),&TRAMEOUT); //LIRE DANS EEPROM


FINDETRANS:

delay_qms(10);
delay_qms(10);
 	
				
					} // FIN FOR


}



void TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(char c,char n) // que faire,numero cod
{

char k;
unsigned int temporaire;
// 0 trame ancien telgat debut N�cable J M H M ?(0)


							//if (c==0)
						
							TRAMEOUT[0]=0x02; //STX
							TRAMEOUT[1]=TMP[50]; //N�cable
							TRAMEOUT[2]=TMP[51];
							TRAMEOUT[3]=TMP[52];
							TRAMEOUT[4]=TMP[53]; //J
							TRAMEOUT[5]=TMP[54];
							TRAMEOUT[6]=TMP[55]; //M
							TRAMEOUT[7]=TMP[56];
							TRAMEOUT[8]=TMP[57]; //H
							TRAMEOUT[9]=TMP[58];		
							TRAMEOUT[10]=TMP[59]; //M
							TRAMEOUT[11]=TMP[60];	
							//TRAMEOUT[12]=TMP[61]; // ?	
	
				
//TRAMEOUT[4]='2'; //J							
//TRAMEOUT[6]='1'; //M


// 1 trame ancien telgat COD	POS	????	DISTANCE	ET	VALEUR	SEUIL


							if (c==1)
							{
						
							if ((TMP[19]=='v')&&(TMP[20]=='i')&&(TMP[21]=='r')&&(TMP[22]=='t')&&(TMP[23]=='u')&&(TMP[24]=='a')&&(TMP[25]=='l')) //capteur fictif demande de lopez
							TMP[65]='3'; //CAPTEUR TEMPERATURE
						

	
							for (k=0;k<24;k++) // ON RAJOUTE CAPTEUR 0 ,puis 1 ,puis 2
							{

							if (TMP[61+k]=='?')
							TMP[61+k]='0';
							if (n==0)
							temporaire=0;
							if (n==1)
							temporaire=0;
							if (n==2)
							temporaire=24;
							if (n==3)
							temporaire=48;
							if (n==4)
							temporaire=72;
							if (n==5)
							temporaire=96;
							if (n==6)
							temporaire=120;
								if (n==7)
							temporaire=144;
								if (n==8)
							temporaire=168;
								if (n==9)
							temporaire=192;
								if (n==10)
							temporaire=216;
								if (n==11)
							temporaire=240;
								if (n==12)
							temporaire=264;
								if (n==13)
							temporaire=288;
								if (n==14)
							temporaire=312;
								if (n==15)
							temporaire=336;
								if (n==16)
							temporaire=360;
								if (n==17)
							temporaire=384;
	
							//temporaire=(n-1)*24;
							temporaire=12+k+temporaire;
							TRAMEOUT[temporaire]=TMP[61+k]; 
							
							

											
							//00101010000 ?0102????456783100000888 ?0203?????????3000000800 ?0405?????????3000000800
									//avec ?0102 01=codage et 02=Type TA (pos) rq:les ?=0

									//si pos =01 c'est un DEBIT avec codage=00 
							}
							

							
											


							}





 
 
 
 

}


void INTERROMPRE(char gg)

{
char t;

						//if (gg==0XEE) //MODE EXISTENCE
						

 
 



						if (gg==0xDD) //ON EST EN FONCTION ZIZ ON ENVOIE ERRAA POUR DIRE A LA CM DE SARRETER
						{


						TRAMERECEPTIONRS485DONNEES (9);  // attend la derniere trame
						TEMPO(1);	
						//delay_qms(300); // ON ATTEND UN PEU

						INFORMATION_ERREUR[0]='A'; //
						INFORMATION_ERREUR[1]='A';			
					
					
						for (t=2;t<20;t++)
						{
						INFORMATION_ERREUR[t]='x';
						}
						ENVOIE_DES_TRAMES (0,302,&INFORMATION_ERREUR); // ON ENVOIE A CARTE 1 ICI voir les autres suivant demande

						}




}





void analyser_les_alarmes_en_cours(void) //ANALYSE LES CABLES ET LES ETATS POPUR L'INSTANT

{


char j,tpa,i;
unsigned int l;





for (i=0;i<5;i++)
{
ET_TD[i]=0;
ET_GR[i]=0;
ET_TP[i]=0;
ET_TR[i]=0;
ET_I[i]=0;
ET_C[i]=0;
ET_F[i]=0;

}
CHERCHER_DONNEES_GROUPE();



//ETAT
TMP[0]=P_RESERVOIR[4];
TMP[1]=P_RESERVOIR[5];
infos_etats(&TMP,4); //CAPTEURS GROUPE


//ETAT
TMP[0]=P_SORTIE[4];
TMP[1]=P_SORTIE[5];
infos_etats(&TMP,4); //CAPTEURS GROUPE



//ETAT
TMP[0]=P_SECOURS[4];
TMP[1]=P_SECOURS[5];
infos_etats(&TMP,4); //CAPTEURS GROUPE


//ETAT
TMP[0]=POINT_ROSEE[4];
TMP[1]=POINT_ROSEE[5];
infos_etats(&TMP,4); //CAPTEURS GROUPE

//ETAT
TMP[0]=DEBIT_GLOBAL[4];
TMP[1]=DEBIT_GLOBAL[5];
infos_etats(&TMP,4); //CAPTEURS GROUPE

//ETAT
TMP[0]=TEMPS_CYCLE_MOTEUR[4];
TMP[1]=TEMPS_CYCLE_MOTEUR[5];
infos_etats(&TMP,4); //CAPTEURS GROUPE



//ETAT
TMP[0]=TEMPS_MOTEUR[4];
TMP[1]=TEMPS_MOTEUR[5];
infos_etats(&TMP,4); //CAPTEURS GROUPE

//ETAT
TMP[0]=PRESENCE_220V[4];
TMP[1]=PRESENCE_220V[5];
infos_etats(&TMP,4); //CAPTEURS GROUPE


//ETAT
TMP[0]=PRESENCE_48V[4];
TMP[1]=PRESENCE_48V[5];
infos_etats(&TMP,4); //CAPTEURS GROUPE





							DEMANDE_INFOS_CAPTEURS[0]='0';
							DEMANDE_INFOS_CAPTEURS[1]='0';
							DEMANDE_INFOS_CAPTEURS[2]='0';
							DEMANDE_INFOS_CAPTEURS[3]='0';
							DEMANDE_INFOS_CAPTEURS[4]='0';


	for (i=1;i<=100;i+=1) //20 pour une carte de 1 � 99
					{

					if ((i>=1)&&(i<21)&&(TYPECARTE[0]=='V'))
					i=21;	
					if ((i>=21)&&(i<41)&&(TYPECARTE[1]=='V'))
					i=41;
					if ((i>=41)&&(i<61)&&(TYPECARTE[2]=='V'))
					i=61;
					if ((i>=61)&&(i<81)&&(TYPECARTE[3]=='V'))
					i=81;
					if ((i>=81)&&(i<=100)&&(TYPECARTE[4]=='V'))
					goto finetude;


					//Tab_Voie_1[0]=i;
					//Tab_Voie_2[0]=i+1;

					// VOIE IMPAIRE
					delay_qms(10);
					l=128*i+30000;
					LIRE_EEPROM(3,l,&TRAMEIN,4); //LIRE DANS EEPROM lexistance de toutes le voies


DEMANDE_INFOS_CAPTEURS[0]='#';
DEMANDE_INFOS_CAPTEURS[1]='0';
if (i>99)
DEMANDE_INFOS_CAPTEURS[1]='1';


DEMANDE_INFOS_CAPTEURS[2]=i/10+48;
DEMANDE_INFOS_CAPTEURS[3]=48+i-10*(DEMANDE_INFOS_CAPTEURS[2]-48);
						
						if (TRAMEIN[2]=='i') //voie EN INCIDENT
						ET_I[0]++;
						if (TRAMEIN[2]=='h') //voies FUSIBLE HS
						ET_F[0]++;
						if (TRAMEIN[2]=='c') //voie en CC
						ET_C[0]++;


						if (TRAMEIN[2]=='a') //voie meme carte adressable
							{
								tpa=0;
														for (j=0;j<17;j++)
														{
													
														DEMANDE_INFOS_CAPTEURS[4]=j/10+48;
														DEMANDE_INFOS_CAPTEURS[5]=48+j-10*(DEMANDE_INFOS_CAPTEURS[4]-48);
														
														if ((j==0)&&(TRAMEIN[3]=='A'))
														{	
														LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM
													//	TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(0,0); // 0 ADRESSABLE en ancien telgat
														infos_etats(&TMP,0); // DEBITMETRES
 
														}			
														if (j>0)
														{
											
														if (TRAMEIN[3+j]=='A')
															{
															tpa=tpa+1;
															LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM
															
															//TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(1,tpa); // 0 ADRESSABLE en ancien telgat dans la trameout
															infos_etats(&TMP,1); //CAPTEURS PRESSION



															//TRAMEOUT[20*j]=&TMP
															delay_qms(10);
											
															}	
													 
														}
														} //TOUTE LA VOIE
					
					
					
					
					
					
					
					
					
					
								}



					if (TRAMEIN[2]=='r') //voie meme carte resitive envoyer juste le premier capteur
					{
					DEMANDE_INFOS_CAPTEURS[4]='0';			
					DEMANDE_INFOS_CAPTEURS[5]='1';

					LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM
					infos_etats(&TMP,3); //CAPTEURS PRESSION RESISTIFS
					delay_qms(10);		
				
				
					}


finetude:
delay_qms(10);		









					}	//FIN DE TOUTES LES VOIES	

			
 


//	tmp=LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM
    



 

}

 

void infos_etats(char *y,char t) //ADDITIONNE LE TYPE D'ETAT

{
//x1 INTERVENTION 75-76
//ET_DEBITS=0;
//ET_GROUPE=0;
//ET_TP=0;
//ET_TR=0;
//if (t==0) //DEBIT

		if (t==1) // capteurs TP
		{
															if ((y[75]=='3')&&(y[76]=='0')) //ALARME A3	
															{
															 ET_TP[0]=ET_TP[0]+1; // ON INCREMENTE LE NOMBRE D'ALARME TP
															}
		
		
															if ((y[75]=='4')&&(y[76]=='0')) //H/G
															{
															 ET_TP[1]=ET_TP[1]+1; // ON INCREMENTE LE NOMBRE H/G TP
															}
		
		
				 											if ((y[75]=='5')&&(y[76]=='0')) //SANS REPONSE
															{
															 ET_TP[2]=ET_TP[2]+1; // ON INCREMENTE LE NOMBRE SR TP
															}													
		
		
															if (y[76]=='1') //INTERVENTION
															{
															 ET_TP[3]=ET_TP[3]+1; // ON INCREMENTE LE NOMBRE INTERVENTION TP
															}													
		
		
		
		
			}



		if (t==0) // capteurs TD
		{
															if ((y[75]=='3')&&(y[76]=='0'))  //ALARME A3	
															{
															 ET_TD[0]=ET_TD[0]+1; // ON INCREMENTE LE NOMBRE D'ALARME TD
															}
		
		
															if ((y[75]=='4')&&(y[76]=='0'))  //H/G
															{
															 ET_TD[1]=ET_TD[1]+1; // ON INCREMENTE LE NOMBRE H/G TP
															}
		
		
															if ((y[75]=='5')&&(y[76]=='0'))  //SANS REPONSE
															{
															 ET_TD[2]=ET_TD[2]+1; // ON INCREMENTE LE NOMBRE SR TP
															}													
		
		
															if (y[76]=='1') //INTERVENTION
															{
															 ET_TD[3]=ET_TD[3]+1; // ON INCREMENTE LE NOMBRE INTERVENTION TP
															}													
		
		
		
		
			}




				if (t==3) // capteurs TP resisitifs
		{
															if ((y[75]=='3')&&(y[76]=='0'))  //ALARME A3	
															{
															 ET_TR[0]=ET_TR[0]+1; // ON INCREMENTE LE NOMBRE D'ALARME TD
															}
		
		
															if ((y[75]=='4')&&(y[76]=='0'))  //H/G
															{
															 ET_TR[1]=ET_TR[1]+1; // ON INCREMENTE LE NOMBRE H/G TP
															}
		
		
															if ((y[75]=='5')&&(y[76]=='0'))  //SANS REPONSE
															{
															 ET_TR[2]=ET_TR[2]+1; // ON INCREMENTE LE NOMBRE SR TP
															}													
		
		
															if (y[76]=='1') //INTERVENTION
															{
															 ET_TR[3]=ET_TR[3]+1; // ON INCREMENTE LE NOMBRE INTERVENTION TP
															}													
		
		
		
		
			}

 
				if (t==4) //GROUPE
			{

		
															if ((y[0]=='3')&&(y[1]=='0'))  //ALARME A3	
															{
															 ET_GR[0]=ET_GR[0]+1; // ON INCREMENTE LE NOMBRE D'ALARME TD
															}
		
		
															if ((y[0]=='4')&&(y[1]=='0'))  //H/G
															{
															 ET_GR[1]=ET_GR[1]+1; // ON INCREMENTE LE NOMBRE H/G TP
															}
		
		
															if ((y[0]=='5')&&(y[1]=='0'))  //SANS REPONSE
															{
															 ET_GR[2]=ET_GR[2]+1; // ON INCREMENTE LE NOMBRE SR TP
															}													
		
		
															if (y[0]=='1') //INTERVENTION
															{
															 ET_GR[3]=ET_GR[3]+1; // ON INCREMENTE LE NOMBRE INTERVENTION TP
															}													
		
		
		



			}		





///////////////////////MANQUE LE GROUPE





}









void CHERCHER_DONNNEES_CABLES(char *g,char j) //051 cable et codage j //DONNE LES DONNEES DU CABLE ET DU CODAGE CHOISSI ET MET UNE VARIABLE EXISTENCE A UN SI CAPTEUR EXISTE

{
char i;
char tati[6];
//tmp=LIRE_EEPROM(choix_memoire(&g),calcul_addmemoire(&g),&TMP,1); //LIRE DANS EEPROM le cable
unsigned int l;

											if (j==111) // INIT COMMENTAIRE SI MENU 6a
											{		
											for (i=0;i<19;i++)
											{
											COMMENTAIRE[i]=0x20;
											}			
											}




tati[5]='0';
tati[4]='0';
tati[3]=g[2];
tati[2]=g[1];
tati[1]=g[0];
tati[0]='#';





if (j!=111)	
{												
tati[4]=j/10+48; //CODAGE
tati[5]=48+j-10*(tati[4]-48);
}


											l=100*(tati[1]-48)+10*(tati[2]-48)+1*(tati[3]-48);
											l=128*l+30000;


											
											LIRE_EEPROM(3,l,&TMP,4); //LIRE DANS EEPROM lexistance du capteur




											capt_exist=0;
											if (j==111) // ON veut savoir s'il cable
													{
													j=2; //CABLE											
													if (TMP[j]=='a') //VOIR i etc....
													capt_exist=1;
													if (TMP[j]=='r')	
													capt_exist=1;
													}
													else
													{
													j=3+j; ///CAPTEUR
													if (TMP[j]=='A')
													capt_exist=1;
													j=2 ; //Car pour resistif seul  la ligne est r
													if ((TMP[j]=='r')&&(tati[4]=='0')&&(tati[5]=='1')) //SI r uniquement pour le codage 1
													capt_exist=1;
													}


if (capt_exist==1)
{																										
LIRE_EEPROM(choix_memoire(&tati),calcul_addmemoire(&tati),&TMP,1); //LIRE DANS EEPROM voir si TRAMEIN DISPO




											

											//recuperer_trame_memoire (0); //voir utiliser cela pour recuperations des variables VOIR STOCKAGE TRAME IN	
 

											


											 TYPE[0]=TMP[6];
											
											if (TMP[7]!=0xFF) // Si memoire pas vide
											{	
											CMOD[0]=TMP[7];
											CMOD[1]=TMP[8];
											CMOD[2]=TMP[9];
											CMOD[3]=TMP[10];
											}
											else
											{
											CMOD[0]='0';
											CMOD[1]='0';
											CMOD[2]='0';
											CMOD[3]='0';
											}
											

											if (TMP[11]!=0xFF) // Si memoire pas vide
											{	
											CCON[0]=TMP[11];
											CCON[1]=TMP[12];
											CCON[2]=TMP[13];
											CCON[3]=TMP[14];
											}
											else
											{
											CCON[0]='0';
											CCON[1]='0';
											CCON[2]='0';
											CCON[3]='0';
											}	

											if (TMP[15]!=0xFF) // Si memoire pas vide
											{	
											CREP[0]=TMP[15];
											CREP[1]=TMP[16];
											CREP[2]=TMP[17];
											CREP[3]=TMP[18];
											}
											else
											{
											CREP[0]='0';
											CREP[1]='0';
											CREP[2]='0';
											CREP[3]='0';
											}





											CONSTITUTION[0]=TMP[19];
											CONSTITUTION[1]=TMP[20];
											CONSTITUTION[2]=TMP[21];
											CONSTITUTION[3]=TMP[22];
											CONSTITUTION[4]=TMP[23];
											CONSTITUTION[5]=TMP[24];
											CONSTITUTION[6]=TMP[25];
											CONSTITUTION[7]=TMP[26];
											CONSTITUTION[8]=TMP[27];
											CONSTITUTION[9]=TMP[28];
											CONSTITUTION[10]=TMP[29];
											CONSTITUTION[11]=TMP[30];			
											CONSTITUTION[12]=TMP[31];	

											//VOIR POUR COMMENTAIRE 
											//dans afficher_donnees_si_existe
 											for (i=0;i<19;i++)
											{
											COMMENTAIRE[i]=TMP[32+i];
											}		

										
											 if (TMP[50]!=0x0FF)
											{																			
											 CABLE[0]=TMP[50];
											 CABLE[1]=TMP[51];
											 CABLE[2]=TMP[52];
											}
											else
											{
											CABLE[0]=VOIECOD[0];
											CABLE[1]=VOIECOD[1];
											CABLE[2]=VOIECOD[2];	
											}



											if (TMP[53]!=0xFF) // Si memoire pas vide
										{	
										JJ[0]=TMP[53];	
										JJ[1]=TMP[54];	
										MM[0]=TMP[55];
										MM[1]=TMP[56];
										HH[0]=TMP[57];
										HH[1]=TMP[58];	
										mm[0]=TMP[59];	
										mm[1]=TMP[60];
										}
										else
										{
										JJ[0]='2';	
										JJ[1]='9';	
										MM[0]='1';
										MM[1]='1';
										HH[0]='0';
										HH[1]='7';	
										mm[0]='1';	
										mm[1]='5';

										}			
													



										if (TMP[61]!=0xFF) // Si memoire pas vide
										ii[0]=TMP[61];
										else
										ii[0]='0';		


									if (TMP[61]!=0xFF) // Si memoire pas vide	
									{
									COD[0]=TMP[62];
									COD[1]=TMP[63];
									}
									else
									{
									COD[0]=VOIECOD[3];
									COD[1]=VOIECOD[4];	

									}


										POS[0]=TMP[64];
										POS[1]=TMP[65];
	
										if (COD[0]=='0' && COD[1]=='0') // ON FORCE LA POS A 01 CODAGE 00
										{ 
										POS[0]='0';
										POS[1]='1'; //DEBIT 
										}
										else
										{
										POS[0]='0';
										POS[1]='2'; //TPA OU TPR
										}




										if (TMP[66]!=0xFF) // Si memoire pas vide	
									{	
										iiii[0]=TMP[66];
										iiii[1]=TMP[67];
										iiii[2]=TMP[68];
										iiii[3]=TMP[69];
									}
									else
									{
										iiii[0]='0';
										iiii[1]='0';
										iiii[2]='0';
										iiii[3]='0';

									}				




											 DISTANCE[0]=TMP[70];
											 DISTANCE[1]=TMP[71];
											 DISTANCE[2]=TMP[72];
											 DISTANCE[3]=TMP[73];
											DISTANCE[4]=TMP[74];



											if (TMP[75]!=0xFF) // Si memoire pas vide		
											{		
											ETAT[0]=TMP[75];
											ETAT[1]=TMP[76];
											}
											else		
											{
											ETAT[0]='0';
											ETAT[1]='0';	
											}	


											if (TMP[77]!=0xFF) // Si memoire pas vide		
											{
											VALEUR[0]=TMP[77];
											VALEUR[1]=TMP[78];
											VALEUR[2]=TMP[79];
											VALEUR[3]=TMP[80];
											}
											else
											{
											VALEUR[0]='0';
											VALEUR[1]='0';
											VALEUR[2]='0';
											VALEUR[3]='0';
											}



 





											 SEUIL[0]=TMP[81];
											SEUIL[1]=TMP[82];
											SEUIL[2]=TMP[83];
											SEUIL[3]=TMP[84];



									

 		 							 
 										
}																					
else

{





											CMOD[0]='0';
											CMOD[1]='0';
											CMOD[2]='0';
											CMOD[3]='0';

											 CCON[0]='0';
											CCON[1]='0';
											CCON[2]='0';
											CCON[3]='0';

											 CREP[0]='0';
											CREP[1]='0';
											CREP[2]='0';
											CREP[3]='0';

											
											for (i=0;i<13;i++)
											{
											CONSTITUTION[i]=0x20;
											}	


													
											
											CABLE[0]=VOIECOD[0];
											CABLE[1]=VOIECOD[1];
											CABLE[2]=VOIECOD[2];


													JJ[0]='0';	
												JJ[1]='0';	
												MM[0]='0';
												MM[1]='0';
												HH[0]='0';
												HH[1]='0';	
												mm[0]='0';
												mm[1]='0';
											
	
											ii[0]='?';

											
								 	
										COD[0]=VOIECOD[3];
										COD[1]=VOIECOD[4];

										
											


									

									//	l=10*(COD[0]-48)+(COD[1]-48)+1; //INCREMENTE LE CODAGE POUR ETRE POS
									
								
									//	IntToChar(l,&POS,2); /// convertir le POS en CHAR pour l'envoyer 
										
								
										if (COD[0]=='0' && COD[1]=='0') // ON FORCE LA POS A 01 CODAGE 00
										{ 
										POS[0]='0';
										POS[1]='1'; //DEBIT 
										}
										else
										{
										POS[0]='0';
										POS[1]='2'; //TPA OU TPR
										}

										
										
										iiii[0]='?';
										iiii[1]='?';
										iiii[2]='?';
										iiii[3]='?';



											VALEUR[0]='?';
											VALEUR[1]='?';
											VALEUR[2]='?';
											VALEUR[3]='?';




											SEUIL[0]='0';
											SEUIL[1]='8';
											SEUIL[2]='0';
											SEUIL[3]='0';
				
											for (i=0;i<6;i++)
											{
											DISTANCE[i]='0';
											}
											
										//	ETAT[0]='0';
										//	ETAT[1]='0';
											ETAT[0]='5'; //CREATION DE CAPTEUR PAR DEFAUT S/R
											ETAT[1]='0';
											


}

}







unsigned char resume_voies(char y,char deb) //ANALYSE L'ETAT VOIES EXIST. y numero carte et deb mettre dans TRAMEOUT A PARTIR DE LA pour menu 2 alarme

{


char j,i,offset,nepasprendre;
unsigned int l;

nepasprendre=0;
//TEST PRESENCE CARTE
if((TYPECARTE[0]=='V')&&(y==1))
nepasprendre=1;
if((TYPECARTE[1]=='V')&&(y==2))
nepasprendre=1;
if((TYPECARTE[2]=='V')&&(y==3))
nepasprendre=1;
if((TYPECARTE[3]=='V')&&(y==4))
nepasprendre=1;
if((TYPECARTE[4]=='V')&&(y==5))
nepasprendre=1;





j=1+(y-1)*20;



					for (i=j;i<(j+20);i+=1) //20 pour une carte de 1 � 99
					{
				
										//Tab_Voie_1[0]=i;
										//Tab_Voie_2[0]=i+1;
					
 										// VOIE IMPAIRE
										delay_qms(10);
										l=128*i+30000;
										LIRE_EEPROM(3,l,&TMP,4); //LIRE DANS EEPROM lexistance de toutes le voies
					
											TRAMEIN[i-(y-1)*20-1]='|';	
					
										
					
											if ((TMP[2]=='a')&&(nepasprendre==0)) //voie adressable declar��
												{
												
												TRAMEIN[i-(y-1)*20-1]=TMP[1];
					
					
					
					
												
												}
												
										
										
												
					
					
					
												if ((TMP[2]=='r')&&(nepasprendre==0)) //voie resistive declar��
												{
											
												TRAMEIN[i-(y-1)*20-1]=TMP[1];
					
											
											
												}
					





 






					}	//FIN DE TOUTES LES VOIES	

			
 

			offset=0;
			for (i=0;i<20;i++)
			{
			
								if (((4+i*+deb)>84)&&(offset==0)) // MAX DE LA LIGNE pas forcement 80 caracteres
								{
								position_init(6, 11+y); //PAGE 6 champ 1 
								pos_init_cursur [2]++;	
								if ((pos_init_cursur [2]-48)>9)
								pos_init_cursur [3]++;
								TRAMEOUT[4*i+deb]=0x1B;
								TRAMEOUT[4*i+deb+1]=0x5B;////////////// envoyer les coordonn�es du cursur
								TRAMEOUT[4*i+deb+2]=pos_init_cursur [3];
								TRAMEOUT[4*i+deb+3]=pos_init_cursur [2];
								TRAMEOUT[4*i+deb+4]=0x3B;
								TRAMEOUT[4*i+deb+5]=pos_init_cursur [1];
								TRAMEOUT[4*i+deb+6]=pos_init_cursur [0];
								TRAMEOUT[4*i+deb+7]=0x48;		
								offset=8; //on decale tt de 8 mnt dans trameout
	
								}	
				
			IntToChar(TRAMEIN[i],&TMP,3); /// convertir le CRC en CHAR pour l'envoyer 
			if (TRAMEIN[i]=='|')
			{
			TMP[0]=' ';
			TMP[1]=' ';
			TMP[2]=' ';
			}	
			
			TRAMEOUT[4*i+deb+offset]='-';
			TRAMEOUT[4*i+deb+1+offset]=TMP[0];
			TRAMEOUT[4*i+deb+2+offset]=TMP[1];
			TRAMEOUT[4*i+deb+3+offset]=TMP[2];
	
			
			

			} //le tableau fini a 84+deb

deb=offset+deb+4*20;
    
return deb; 

 

}




void afficher_donnees_si_existe(char *y) //A EFFACER
{
float k;
char u;
unsigned int j;
char tat[6];


tat[3]=y[2];
tat[2]=y[1];
tat[1]=y[0];
tat[0]='#';
 

u=100*(tat[1]-48);
u=u+10*(tat[2]-48);
u=u+(tat[3]-48);


k=u;

k=128.0*k;
k=k+30000;
j=k;
LIRE_EEPROM(3,j,&TMP,4); //LIRE DANS EEPROM lexistance de toutes le voies

	



CHERCHER_DONNNEES_CABLES(y,111); //051 cable et codage j ON VEUT JUSTE LES DONNEES DU CABLES


										







//CHERCHER_DONNNEES_CABLES(y,char j) //051 cable et codage j

}






/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////// verif heure et date  ///////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void verif_heure_date (void)
{
	if (nchamps == 4)
	{
		if ((tableau[0] > '2') || (tableau[0]== '2' && tableau [1]> '4') ||(tableau[2] > '5')  )  /////// heure >24 heure ou plus de 60 minute
		{
          accord=0;           //// erreur d'heure 
		}
		else 
		{
		accord=1;		/////////heure correcte 
		}
        


	}
	if (nchamps == 5)
	{
		if (  tableau[2]== '1' && tableau [3] > '2' )
		{
	    accord=0;	/////////////erreur de date 
		}

        else if (( (tableau [2]== '0' && tableau [3]== '1') || (tableau [2]== '0' && tableau [3]== '3') || (tableau [2]== '0' && tableau [3]== '5') || (tableau [2]== '0' && tableau [3]== '7')|| (tableau [2]== '0' && tableau [3]== '8')|| (tableau [2]== '1' && tableau [3]== '0')|| (tableau [2]== '1' && tableau [3]== '2') ) && (tableau [0]== '3' && tableau [1]>'1'))
		{
        accord=0;         /////////////erreur de date ////// 31 jours pour 1 mois 
		}
        else if (( (tableau [2]== '0' && tableau [3]== '4') || (tableau [2]== '0' && tableau [3]== '6') || (tableau [2]== '0' && tableau [3]== '9') || (tableau [2]== '1' && tableau [3]== '1') ) && (tableau [0]== '3' && tableau [1]> '0'))
		{
        accord=0;       /////////////erreur de date ////// 30jours
		}
		else if (tableau [2]== '0' && tableau [3]== '2' && tableau [0]== '2' && tableau [1]>'9')
        {
        accord=0;      ////////////erreur de date  ////// fevrier 
        }
		else if (tableau [2]> '1' ||   tableau [0]> '3')
        {
        accord=0;      ////////////erreur de date /////
        }
        else 
        {
		accord=1;						///////// date correcte 
		}
	}


}







void MODIFICATION_TABLE_DEXISTENCE(char *v,char eff,char etat) //eff pour effacer (eff=2 alors recopie) alors et etat ligne pour fusible HS ou CC ou surtension 
{

unsigned int y,z;
char fa;




v[0]='#';
y=100*(v[1]-48)+10*(v[2]-48)+(v[3]-48); //VOIE

y=128*y+30000;

LIRE_EEPROM(3,y,&TMP,4); //LIRE DANS EEPROM



 





z=10*(v[4]-48)+(v[5]-48); //CODAGE

if (eff==2) //RECOPIE
{
goto recop;
}



if (eff==0) //CREATION attention ici on cr�e le type capteur et le type de cable ic on a pas encore test� si le cable avant etait resistif ou r codage en 2 3 ....il faut juste 
{
TMP[z+3]=v[6]; //RECOPIE SI R OU A #00101R
fa=TMP[z+3];



}




// VOIR voie a 'v' qd il y a que des 'F' 
//ecrire_tab_presence_TPA;
// ATTENTION SI 
//if (TMP[z+3]=='v')
// OK ON PEUT
//if (TMP[z+3]=='a')
// OK ON PEUT SI v[6]=='A'
//if(TMP[z+3]=='r')
// OK ON PEUT SI v[6]=='v'



if (fa=='A')
{
TMP[2]='a';

}
if (fa=='R') //L'ADRESSABLE NE PEUT QUE EXISTER EN CODAGE1 !!!!!!!!!!!!!!!!!!!!!!
{
TMP[2]='r';
}



if (eff==1) //EFFACEMENT
{
TMP[z+3]='F'; //CAPTEUR EFFACEE DE LA TABLE DEXISTENCE
// VOIR voie a 'v' qd il y a que des 'F' 

if ((NBRECAPTEURS_CABLE==1)&&(TMP[z+3]=='F')) //SI IL N'Y A QUE UN CAPTEUR VARIABLES INIT DANS MENU6b et que on efface pas un autre cable if ((NBRECAPTEURS_CABLE==1)&&(TMP[z+3]!='F')) //SI IL N'Y A QUE UN CAPTEUR VARIABLES INIT DANS MENU6b et que on efface pas un autre cable
TMP[2]='v';  // ON EFFACE LE CABLE
 

 
}


recop:



if (etat==1)
{
TMP[2]='h';
}

if (etat==2)
{
TMP[2]='h';
}

if ((etat==4)&&(TMP[2]!='c'))
{
TMP[2]=TMP[21];
}

if ((etat==5)&&(TMP[2]!='c'))
{
TMP[2]=TMP[21];

}

if (etat==6)
{
TMP[2]='c';

}


if (etat==7)
{
TMP[2]='c';
}


if (etat==8)  // SCRUTATION POUR INHIBER DEFAUT CC
{
TMP[2]=TMP[21];
}




ecrire:
// ON RE ECRIT LE NUMERO DE VOIE DANS LA TABLE  //��a�AFF����������������* qd memoire vide
if (TMP[1]==0xFF) //VIDE
{
y=(y-10000)/128; //FORCEMENT UN ENTIER
TMP[0]='#';
TMP[1]=y;
y=128*y+30000;
}


//ecrire_tab_presence_TPR;
ECRIRE_EEPROMBYTE(3,y,&TMP,4);

delay_qms(10);
LIRE_EEPROM(3,y,&TMP,4); //LIRE DANS EEPROM pour verifier



}






/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////supression d'un capteur////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void sup_capteur (void)   /////
{
TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;///////////////////////////////envoi du texte etes vous sur de vouloir supprimer le capteur?  oui ou non
TRAMEOUT[2]=0x32;
TRAMEOUT[3]=0x4A;
TRAMEOUT[4]=0x1B;
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x31;
TRAMEOUT[7]=0x32;
TRAMEOUT[8]=0x3B;
TRAMEOUT[9]=0x31;
TRAMEOUT[10]=0x32;
TRAMEOUT[11]=0x48;
TRAMEOUT[12]=0x45;
TRAMEOUT[13]=0x74;
TRAMEOUT[14]=0x65;
TRAMEOUT[15]=0x73;
TRAMEOUT[16]=0x2D;
TRAMEOUT[17]=0x76;
TRAMEOUT[18]=0x6F;
TRAMEOUT[19]=0x75;
TRAMEOUT[20]=0x73;
TRAMEOUT[21]=0x20;
TRAMEOUT[22]=0x73;
TRAMEOUT[23]=0x75;
TRAMEOUT[24]=0x72;
TRAMEOUT[25]=0x20;
TRAMEOUT[26]=0x64;
TRAMEOUT[27]=0x65;
TRAMEOUT[28]=0x20;
TRAMEOUT[29]=0x76;
TRAMEOUT[30]=0x6F;
TRAMEOUT[31]=0x75;
TRAMEOUT[32]=0x6C;
TRAMEOUT[33]=0x6F;
TRAMEOUT[34]=0x69;
TRAMEOUT[35]=0x72;
TRAMEOUT[36]=0x20;
TRAMEOUT[37]=0x73;
TRAMEOUT[38]=0x75;
TRAMEOUT[39]=0x70;
TRAMEOUT[40]=0x70;
TRAMEOUT[41]=0x72;
TRAMEOUT[42]=0x69;
TRAMEOUT[43]=0x6D;
TRAMEOUT[44]=0x65;
TRAMEOUT[45]=0x72;
TRAMEOUT[46]=0x20;
TRAMEOUT[47]=0x6C;
TRAMEOUT[48]=0x65;
TRAMEOUT[49]=0x20;
TRAMEOUT[50]=0x63;
TRAMEOUT[51]=0x61;
TRAMEOUT[52]=0x70;
TRAMEOUT[53]=0x74;
TRAMEOUT[54]=0x65;
TRAMEOUT[55]=0x75;
TRAMEOUT[56]=0x72;
TRAMEOUT[57]=0x3F;
TRAMEOUT[58]=0x1B;
TRAMEOUT[59]=0x5B;
TRAMEOUT[60]=0x31;
TRAMEOUT[61]=0x33;
TRAMEOUT[62]=0x3B;
TRAMEOUT[63]=0x32;
TRAMEOUT[64]=0x35;
TRAMEOUT[65]=0x48;
TRAMEOUT[66]=0x4F;
TRAMEOUT[67]=0x55;
TRAMEOUT[68]=0x49;
TRAMEOUT[69]=0x20;
TRAMEOUT[70]=0x20;
TRAMEOUT[71]=0x20;
TRAMEOUT[72]=0x20;
TRAMEOUT[73]=0x20;
TRAMEOUT[74]=0x4E;
TRAMEOUT[75]=0x4F;
TRAMEOUT[76]=0x4E;
TRAMEOUT[77]=0x20;
TRAMEENVOIRS232DONNEES (78);
TRAMERECEPTIONminitel();
if (STOPMINITEL==0xFF)
goto fin_sup;



if (TRAMEIN[0]=='o' || TRAMEIN[0]=='O')  
{
TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;
TRAMEOUT[2]=0x32;
TRAMEOUT[3]=0x4A;
TRAMEOUT[4]=0x1B;///////////////////////////////envoi du texte "veuillez patienter" en attendant la supression du capteur 
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x31;
TRAMEOUT[7]=0x32;
TRAMEOUT[8]=0x3B;
TRAMEOUT[9]=0x31;
TRAMEOUT[10]=0x32;
TRAMEOUT[11]=0x48;
TRAMEOUT[12]=0x20;
TRAMEOUT[13]=0x56;
TRAMEOUT[14]=0x65;
TRAMEOUT[15]=0x75;
TRAMEOUT[16]=0x69;
TRAMEOUT[17]=0x6C;
TRAMEOUT[18]=0x6C;
TRAMEOUT[19]=0x65;
TRAMEOUT[20]=0x7A;
TRAMEOUT[21]=0x20;
TRAMEOUT[22]=0x70;
TRAMEOUT[23]=0x61;
TRAMEOUT[24]=0x74;
TRAMEOUT[25]=0x69;
TRAMEOUT[26]=0x65;
TRAMEOUT[27]=0x6E;
TRAMEOUT[28]=0x74;
TRAMEOUT[29]=0x65;
TRAMEOUT[30]=0x72;
TRAMEOUT[31]=0x20;
TRAMEOUT[32]=0x20;
TRAMEENVOIRS232DONNEES (33);
   tmp=EFFACER_CAPTEUR(); ///210232017                             /////////////////////// sortir de la boucle quand le capteur sera supprimer //a ajouter 
}
else if (TRAMEIN[0]=='n' || TRAMEIN[0]=='N')
{
ETAT1= M;
}
fin_sup:
delay_qms(10);



}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////// calculer le num�ro de la carte et le num�ro de la ligne   /////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void calcul_c_l (void)     
{
if ((VOIECOD[1]=='0' || VOIECOD[1]=='1'  || (VOIECOD[1]=='2' && VOIECOD[2]=='0'))&& VOIECOD[0]=='0')     //// carte num�ro 1 vois de 1 a 20
{
 																											
n_carte='1';
n_voie[0]=VOIECOD[1];
n_voie[1]=VOIECOD[2];
}
else if (((VOIECOD[1]=='2' && VOIECOD[2]!='0') || VOIECOD[1]=='3'  || (VOIECOD[1]=='4' && VOIECOD[2]=='0'))&& VOIECOD[0]=='0')  //// carte num�ro 2 vois de 1 a 20
{
 																											
n_carte='2';
n_voie[0]=VOIECOD[1]-2;
n_voie[1]=VOIECOD[2];
}
else if (((VOIECOD[1]=='4' && VOIECOD[2]!='0') || VOIECOD[1]=='5'  || (VOIECOD[1]=='6' && VOIECOD[2]=='0'))&& VOIECOD[0]=='0')   //// carte num�ro 3 vois de 1 a 20
{
 																											
n_carte='3';
n_voie[0]=VOIECOD[1]-4;
n_voie[1]=VOIECOD[2];
}
else if (((VOIECOD[1]=='6' && VOIECOD[2]!='0') || VOIECOD[1]=='7'  || (VOIECOD[1]=='8' && VOIECOD[2]=='0'))&& VOIECOD[0]=='0')   //// carte num�ro 4 vois de 1 a 20
{
 																										
n_carte='4';
n_voie[0]=VOIECOD[1]-6;
n_voie[1]=VOIECOD[2];
}
else if ((VOIECOD[1]=='8' && VOIECOD[2]!='0') || VOIECOD[1]=='9'  || (VOIECOD[0]=='1' && VOIECOD[2]=='0' && VOIECOD[1]=='0'  ))  //// carte num�ro 5 vois de 1 a 20
{
 																											
n_carte='5';
n_voie[0]=VOIECOD[1]-8;
n_voie[1]=VOIECOD[2];
if (VOIECOD[0]=='1')
{
n_voie[0]='2';
n_voie[1]='0';
}
}
else 
{
                ///////////////voie n'existe pas
}
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////// enregistrement des donn�es  ///////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void enregistrer_donnees (void)
{
char i;
	if ( npage== 'a' && nchamps ==1)        //////////page 6a //ON SAV LE CABLE
	{
			if (nb_oct==0) //NBRE DE CARAC MENU 6.A
			{
			VOIECOD[2]=tableau[0]; //2 =002
			VOIECOD[1]='0'; 
			VOIECOD[0]='0';	
   			}         
            if (nb_oct==1)
			{
			VOIECOD[2]=tableau[1]; //21 = 021
			VOIECOD[1]=tableau[0]; 
			VOIECOD[0]='0';	
   			}
            if (nb_oct==2)
			{
			VOIECOD[0]=tableau[0]; //213 = 213
			VOIECOD[1]=tableau[1]; 
			VOIECOD[2]=tableau[2];
   			} 

	} 
    if ( npage== 'a' && nchamps ==2)       ///////////p	ge 6a  //////MPL
	{
        
        
		if (nb_oct==0) //NBRE DE CARAC MENU 6.A
			{
			ROB[1]=tableau[0]; //2 
			ROB[0]='0'; 
			
   			}         
            if (nb_oct==1)
			{
			ROB[1]=tableau[1]; //21 
			ROB[0]=tableau[0]; 
   			}
		if( ROB [0] >= '6')
		 {
			ROB[1]='0'; //21 
			ROB[0]='6'; 	
  		 }

		if(( ROB [0] == '0')&&(ROB[1]=='0'))
		 {
			ROB[1]='1'; //21 
			ROB[0]='0'; 	
  		 }

      
	}                                                     /////// MPL
    if ( npage== 'a' && nchamps ==5)       ///////////p	ge 6a
	{
        for (i=0;i<=decal;i++)
        
		COMMENTAIRE[decal+4]=tableau[decal]; //+4 car les premiers octets contiennent le Roxx robinet
      
	}
    if ( npage== 'b' && nchamps ==2)           /////////////page 6b
	{
		if (identifiant==1)                   ///////variable qui indique si 1 debimetre, si 2 resistif si 3 adressable
        {
           TYPE[0]='A';
        }
		else if (identifiant==2)
        {
           TYPE[0]='R';
        }
		else if (identifiant==3)
        {
           TYPE[0]='A';
        }
	}
     
    if ( npage== 'b' && nchamps ==1)                //////////page6b      champ 2
	{
	     	VOIECOD[decal+3]=tableau[decal];        //////////en modifie directement le champ saisie (codage capteur)
			
	}
     
    if ( npage== 'b' && nchamps ==3)               //////////page6b      champ 3
	{
    
	 CONSTITUTION[decal]=tableau[decal];	     //////////en modifie directement le champ saisie (commentaire)
		
	}
     
    if ( npage== 'b' && nchamps ==4)               //////////page6b      champ 4
	{
    	
		SEUIL[decal]=tableau[decal];               //////////en modifie directement le champ saisie (seuil)
		
	}
     
    if ( npage== 'b' && nchamps ==5)              //////////page6b      champ 5
	{
    
		DISTANCE[decal]=tableau[decal];           //////////en modifie directement le champ saisie (distance)
		
	}
     
    if ( npage== 'b' && nchamps ==6)              //////////page6b      champ 6
	{
		if (identifiant==1)                        //////// variable qui identifie  si le capteur est en intervention ou pas 1 oui et 0 non
        {
        //   	ETAT[0]='0'; //on  ne modifie pas letat
			ETAT[1]='1';                          
        }
		else if (identifiant==2)
        {
           	//ETAT[0]='0';//on  ne modifie pas letat
			ETAT[1]='0';
        }
	} 
      
    if ( npage== '8' && nchamps ==1)                   /////////////////////page 8 champ 1
	{
    
		 	ID_rack[decal]=tableau[decal];             ///////////////////// identification du rack
	
	}
    if ( npage== '8' && nchamps ==2)                    /////////////////////page 8 champ 2   
	{
		if (identifiant==1)
        {
           	T_alarme[0]=1;
			T_alarme[1]=0;                                //////////////////////identifiant =1  alarme =oui ou identifiant =0  alarme =non
        }
		else if (identifiant==2)
        {
           	T_alarme[0]=0;
			T_alarme[1]=0;
        }
		
	}
    if ( npage== '8' && nchamps ==3)                    /////////////////////page 8 champ 3 
	{
		if (identifiant==1)
        {
           	T_sortie[0]=1;
			T_sortie[1]=0;                                    //////////////////////identifiant =1  transmission sortie =oui ou identifiant =0  transmission sortie =non
        }
		else if (identifiant==2)
        {
           	T_sortie[0]=0;
			T_sortie[1]=0;
        }		
	} 
    if ( npage== '8' && nchamps ==4)                    /////////////////////page 8 champ 4
	{
		if (decal==0)                                    //////////champs heure suivant la position du curseur (decal)
		{
		HH[0]=tableau[0];
		}
		if (decal==1)
		{
		HH[1]=tableau[1];
		}
		if (decal==2)
		{
		mm[0]=tableau[2];
		}		
		if (decal==3)
		{
		mm[0]=tableau[3];
		}					
	}
    if ( npage== '8' && nchamps ==5)                    /////////////////////page 8 champ 5
	{
		if (decal==0)
		{
		JJ[0]=tableau[0];                                   //////////champs de la date suivant la position du curseur (decal)
		}
		if (decal==0)
		{
		JJ[1]=tableau[1];
		}		
		if (decal==0)
		{
		MM[0]=tableau[2];
		}		
		if (decal==0)
		{
		MM[1]=tableau[3];
		}		
		if (decal==0)
		{
		AA[0]=tableau[4];
		}		
		if (decal==0)
		{
		AA[1]=tableau[5];
		}		
		if (decal==0)
		{
		AA[2]=tableau[6];
		}		
		if (decal==0)
		{
		AA[3]=tableau[7];
		}		
		 
	} 
    if ( npage== '8' && nchamps ==6)                     /////////////////////page 8 champ 6
	{
		acc_u[decal]=tableau[decal];                     /////////////////////// mots de passe utilisateur 

	}
    if ( npage== '8' && nchamps ==7)                     /////////////////////page 8 champ 7
	{
		acc_conf[decal]=tableau[decal];                  /////////////////////// mots de passe configuration 
	
	}
    if ( npage== '8' && nchamps ==8)                     /////////////////////page 8 champ 8
	{
		
		nr_cog[decal]=tableau[decal];	                 /////////////////////// numero COG
		
	}
    if ( npage== '8' && nchamps ==9)                     /////////////////////page 8 champ 9
	{
	
		nr_cms[decal]=tableau[decal];	                 /////////////////////// numero CMS
	
	}
     
     
}

 

char VALIDER_CAPTEUR(void)
{
char t,u;


TERMINEACTION=0; //L'ACTION A FAIRE 

if (CMouPAS==0)
TERMINEACTION=1; // AEF A SUPPRIMER
 



u=0;

 
nchamps=8;
npage='b';
 				        
position_init(npage, nchamps);
       										envoi_cursur();
											TRAMEOUT[0]='P';				
											TRAMEOUT[1]='A';
											TRAMEOUT[2]='T';
											TRAMEOUT[3]='I';
											TRAMEOUT[4]='E';
											TRAMEOUT[5]='N';
											TRAMEOUT[6]='T';
											TRAMEOUT[7]='E';
											TRAMEOUT[8]='Z';			
											TRAMEENVOIRS232DONNEES (9); 
do
{
TEMPO(5);
CREER_TRAMEIN_CAPTEUR_DU_MINITEL();//RECUPERE LES VARIABLES ET CREATION D'UNE TRAME EEPROM	

if (CMouPAS==1)
ENVOIE_DES_TRAMES (0,329,&TRAMEIN) ; //CREATION CAPTEUR SUIVANT TRA. MEMOIRE SI ERRFF (TERMINEACTION=1) ALORS L LA CREATION/MODIF AIT EU LIEU AEF

if (TERMINEACTION==3)// on a recu un 0xDD o, retente //ATTENTION TRAMEIN PLUS CORRECTE!!!!!!!!!!!!!!!!!!!!!!!!
{
TRAMERECEPTIONRS485DONNEES (18);  //ON ATTEND TRUC DU GENRE CMR1 0xAA de la carte
CREER_TRAMEIN_CAPTEUR_DU_MINITEL();//RECUPERE LES VARIABLES ET CREATION D'UNE TRAME EEPROM	
TEMPO(2);
ENVOIE_DES_TRAMES (0,329,&TRAMEIN) ; //CREATION CAPTEUR SUIVANT TRA. MEMOIRE SI ERRFF (TERMINEACTION=1) ALORS L LA CREATION/MODIF AIT EU LIEU AEF

}


		if (TERMINEACTION==1) 
		{
					 if (CMouPAS==1)
					 {						

								//VERSION AVEC CM
							//-----------------------------
							      
							TRAMEIN[0]='#'; //ICI ON REM. TOUTE LA TRAME OUT COM 485 DU CAPTEUR EN IN FORMAT MEMOIRE #00102.....
							for (t=1;t<91;t++)
							{
							TRAMEIN[t]=TRAMEOUT[t+7];
							}
							//-----------------------------

		
					}





		MODIFICATION_TABLE_DEXISTENCE (&TRAMEIN,0,0); //MODIFICATION DE LA TABLE 

		ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEIN),calcul_addmemoire(&TRAMEIN),&TRAMEIN,1); //ECRITURE EN EEPROM LE CABLE
		
		LIRE_EEPROM(choix_memoire(&TRAMEIN),calcul_addmemoire(&TRAMEIN),&TRAMEOUT,1); //ESSAI A EFFACER


		//TRAMEIN[4]='0';
		//TRAMEIN[5]='0'; //CODAGE 0
		//LIRE_EEPROM(choix_memoire(&TRAMEOUT),calcul_addmemoire(&TRAMEOUT),&TRAMEIN,1); //LIRE DANS EEPROM LIRE LE CAPTEUR 0 C ICI QUON IRA LIRE LE COMMENTAIRE CABLE
		
		
		//TRAMEOUT[0]='#';
		//TRAMEOUT[1]=VOIECOD[0];
		//TRAMEOUT[2]=VOIECOD[1];
		//TRAMEOUT[3]=VOIECOD[2];
		//TRAMEOUT[4]='0';
		//TRAMEOUT[5]='0';
		
		//for (t=0;t<18;t++) 
		//{
		//TRAMEOUT[32+t]=COMMENTAIRE[t];
		
		//}
		//ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEOUT),calcul_addmemoire(&TRAMEOUT),&TRAMEOUT,1); //ECRITURE EN EEPROM

		u=99;

//ENVOIE_DES_TRAMES (0,329,&TRAMEIN) ; //CREATION CAPTEUR SUIVANT TRA. MEMOIRE SI ERRFF (TERMINEACTION=1) ALORS L LA CREATION/MODIF AIT EU LIEU
 

		// ICI ON ENREGISTRE EGALEMENT SUR LE DEBIT PRESENT OU PAS EN CODAGE 00 POUR LE COMMENTAIRE ET ROBINET  !!!!!!
		VOIECOD[3]='0';
		VOIECOD[4]='0';
		CHERCHER_DONNNEES_CABLES(&VOIECOD,0); //CABLE 
		CREER_TRAMEIN_CAPTEUR_DU_MINITEL();
		ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEIN),calcul_addmemoire(&TRAMEIN),&TRAMEIN,1); //ECRITURE EN EEPROM

		//SAV MODIF


		}
u=u+1;
}
while (u<2); //si pas reuissi on refait une fois


nchamps=8;
npage='b';
 				        
position_init(npage, nchamps);
       										envoi_cursur();
if (TERMINEACTION!=1)
{
											
											TRAMEOUT[0]='V';				
											TRAMEOUT[1]='E';
											TRAMEOUT[2]='R';
											TRAMEOUT[3]='I';
											TRAMEOUT[4]='F';
											TRAMEOUT[5]='I';
											TRAMEOUT[6]='E';
											TRAMEOUT[7]='R';
											TRAMEOUT[8]=' ';


}

else
{

											TRAMEOUT[0]=' ';				
											TRAMEOUT[1]=' ';
											TRAMEOUT[2]=' ';
											TRAMEOUT[3]=' ';
											TRAMEOUT[4]=' ';
											TRAMEOUT[5]=' ';
											TRAMEOUT[6]=' ';
											TRAMEOUT[7]=' ';
											TRAMEOUT[8]=' ';



}


												


											TRAMEENVOIRS232DONNEES (9);





TERMINEACTION=2;

//delay_qms(100);

}





char EFFACER_CAPTEUR(void)

{
char t,u;
TERMINEACTION=0; //L'ACTION A FAIRE

if (CMouPAS==0)
TERMINEACTION=1; //AEF A SUPPRIMER


u=0;
do
{
TEMPO(5);
CREER_TRAMEIN_CAPTEUR_DU_MINITEL();//RECUPERE LES VARIABLES ET CREATION D'UNE TRAME EEPROM	
DEMANDE_INFOS_CAPTEURS[0]=TRAMEIN[1];
DEMANDE_INFOS_CAPTEURS[1]=TRAMEIN[2];
DEMANDE_INFOS_CAPTEURS[2]=TRAMEIN[3];
DEMANDE_INFOS_CAPTEURS[3]=TRAMEIN[4];
DEMANDE_INFOS_CAPTEURS[4]=TRAMEIN[5];





if (CMouPAS==1)
ENVOIE_DES_TRAMES (0,320,&TRAMEIN) ; //CREATION CAPTEUR SUIVANT TRA. MEMOIRE AEF

				if (TERMINEACTION==1)
				
				{
					 if (CMouPAS==1)
					 {	
				//VERSION AVEC CM AEF
				//----------------------------
				TRAMEIN[0]='#'; //ICI ON REM. TOUTE LA TRAME OUT COM 485 DU CAPTEUR EN IN FORMAT MEMOIRE #00102.....
				for (t=1;t<91;t++)
				{
				TRAMEIN[t]=TRAMEOUT[t+7];
				}
	 			//----------------------------
					}			
								


				MODIFICATION_TABLE_DEXISTENCE (&TRAMEIN,1,0); // ON EFFACE LE CAPTEUR DANS LA MEMOIRE TABLE EXIST DE L'IO
				u=99;
				ETAT1=M; //Pour gestion menu et retours menu
				repet=0; // idem

				}

if (TERMINEACTION==3)
u=u+1;

 
}
while (u<2); //si pas reuissi on refait une fois



if (TERMINEACTION!=1)
{
TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;
TRAMEOUT[2]=0x32;
TRAMEOUT[3]=0x4A;
TRAMEOUT[4]=0x1B;///////////////////////////////envoi du texte "veuillez patienter" en attendant la supression du capteur 
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x31;
TRAMEOUT[7]=0x32;
TRAMEOUT[8]=0x3B;
TRAMEOUT[9]=0x31;
TRAMEOUT[10]=0x33;
TRAMEOUT[11]=0x48;
TRAMEOUT[12]=0x20;
TRAMEOUT[13]='A';
TRAMEOUT[14]=0x20;
TRAMEOUT[15]='V';
TRAMEOUT[16]='E';
TRAMEOUT[17]='R';
TRAMEOUT[18]='I';
TRAMEOUT[19]='F';
TRAMEOUT[20]='I';
TRAMEOUT[21]='E';
TRAMEOUT[22]='R';

TRAMEENVOIRS232DONNEES (23);	
										


}

else
{

									TRAMEOUT[0]=0x1B;
TRAMEOUT[1]=0x5B;
TRAMEOUT[2]=0x32;
TRAMEOUT[3]=0x4A;
TRAMEOUT[4]=0x1B;///////////////////////////////envoi du texte "veuillez patienter" en attendant la supression du capteur 
TRAMEOUT[5]=0x5B;
TRAMEOUT[6]=0x31;
TRAMEOUT[7]=0x32;
TRAMEOUT[8]=0x3B;
TRAMEOUT[9]=0x31;
TRAMEOUT[10]=0x33;
TRAMEOUT[11]=0x48;
TRAMEOUT[12]=0x20;
TRAMEOUT[13]='O';
TRAMEOUT[14]='K';
TRAMEOUT[15]=' ';
TRAMEOUT[16]=' ';
TRAMEOUT[17]=' ';
TRAMEOUT[18]=' ';
TRAMEOUT[19]=' ';
TRAMEOUT[20]=' ';

TRAMEENVOIRS232DONNEES (21);	

															


}


TEMPO(2);												


									


TERMINEACTION=2;


}





void GROUPE_EN_MEMOIRE(unsigned char *tab,char lui) 

{

int ici;
if (lui==0)
ici=0; 
if (lui==1)
ici=128;
if (lui==2)
ici=256; 


ECRIRE_EEPROMBYTE(6,ici,tab,3);
delay_qms(100);
LIRE_EEPROM(6,ici,&TRAMEOUT,3);
delay_qms(100);

}



char CHERCHER_DONNEES_GROUPE(void)

{

char o;




//---------------------------------1
LIRE_EEPROM(6,0,&TMP,3);
delay_qms(100);
//VALEUR



o=35;
P_RESERVOIR[0]=TMP[o];
P_RESERVOIR[1]=TMP[o+1];
P_RESERVOIR[2]=TMP[o+2];
P_RESERVOIR[3]=TMP[o+3];


//ETAT
P_RESERVOIR[4]=TMP[o-2];
P_RESERVOIR[5]=TMP[o-1];


//SEUILH
P_RESERVOIR[6]=TMP[o+4];
P_RESERVOIR[7]=TMP[o+5];
P_RESERVOIR[8]=TMP[o+6];
P_RESERVOIR[9]=TMP[o+7];


//SEUILB
P_RESERVOIR[10]=TMP[o-11];
P_RESERVOIR[11]=TMP[o-10];
P_RESERVOIR[12]=TMP[o-9];
P_RESERVOIR[13]=TMP[o-8];

//---------------------------------------------------2

o=o+24;

//VALEUR
P_SORTIE[0]=TMP[o];
P_SORTIE[1]=TMP[o+1];
P_SORTIE[2]=TMP[o+2];
P_SORTIE[3]=TMP[o+3];

//ETAT
P_SORTIE[4]=TMP[o-2];
P_SORTIE[5]=TMP[o-1];

//SEUILH
P_SORTIE[6]=TMP[o+4];
P_SORTIE[7]=TMP[o+5];
P_SORTIE[8]=TMP[o+6];
P_SORTIE[9]=TMP[o+7];



//SEUILB
P_SORTIE[10]=TMP[o-11];
P_SORTIE[11]=TMP[o-10];
P_SORTIE[12]=TMP[o-9];
P_SORTIE[13]=TMP[o-8];


//---------------------------------------3 attention  xxxxxxx entre 2 et 3 donc deux fois
o=o+24;
o=o+24;




//VALEUR
P_SECOURS[0]=TMP[o];
P_SECOURS[1]=TMP[o+1];
P_SECOURS[2]=TMP[o+2];
P_SECOURS[3]=TMP[o+3];

//ETAT
P_SECOURS[4]=TMP[o-2];
P_SECOURS[5]=TMP[o-1];

//SEUILH
P_SECOURS[6]=TMP[o+4];
P_SECOURS[7]=TMP[o+5];
P_SECOURS[8]=TMP[o+6];
P_SECOURS[9]=TMP[o+7];

 

//SEUILB
P_SECOURS[10]=TMP[o-11];
P_SECOURS[11]=TMP[o-10];
P_SECOURS[12]=TMP[o-9];
P_SECOURS[13]=TMP[o-8];





//--------------------------------------------------------------------4




LIRE_EEPROM(6,128,&TMP,3);
delay_qms(100);


o=35;
POINT_ROSEE[0]=TMP[o];
POINT_ROSEE[1]=TMP[o+1];
POINT_ROSEE[2]=TMP[o+2];
POINT_ROSEE[3]=TMP[o+3];


//ETAT
POINT_ROSEE[4]=TMP[o-2];
POINT_ROSEE[5]=TMP[o-1];


//SEUILH
POINT_ROSEE[6]=TMP[o+4];
POINT_ROSEE[7]=TMP[o+5];
POINT_ROSEE[8]=TMP[o+6];
POINT_ROSEE[9]=TMP[o+7];



//---------------------------------------------------5

o=o+24;

//VALEUR
DEBIT_GLOBAL[0]=TMP[o];
DEBIT_GLOBAL[1]=TMP[o+1];
DEBIT_GLOBAL[2]=TMP[o+2];
DEBIT_GLOBAL[3]=TMP[o+3];

//ETAT
DEBIT_GLOBAL[4]=TMP[o-2];
DEBIT_GLOBAL[5]=TMP[o-1];

//SEUILH
DEBIT_GLOBAL[6]=TMP[o+4];
DEBIT_GLOBAL[7]=TMP[o+5];
DEBIT_GLOBAL[8]=TMP[o+6];
DEBIT_GLOBAL[9]=TMP[o+7];



o=o+24;

//-------------------------------------------------6


//VALEUR
TEMPS_CYCLE_MOTEUR[0]=TMP[o];
TEMPS_CYCLE_MOTEUR[1]=TMP[o+1];
TEMPS_CYCLE_MOTEUR[2]=TMP[o+2];
TEMPS_CYCLE_MOTEUR[3]=TMP[o+3];

//ETAT
TEMPS_CYCLE_MOTEUR[4]=TMP[o-2];
TEMPS_CYCLE_MOTEUR[5]=TMP[o-1];

//SEUILH
TEMPS_CYCLE_MOTEUR[6]=TMP[o+4];
TEMPS_CYCLE_MOTEUR[7]=TMP[o+5];
TEMPS_CYCLE_MOTEUR[8]=TMP[o+6];
TEMPS_CYCLE_MOTEUR[9]=TMP[o+7];

//PAS DE 7

//-------------------------------------8





o=o+24;
TEMPS_MOTEUR[0]=TMP[o];
TEMPS_MOTEUR[1]=TMP[o+1];
TEMPS_MOTEUR[2]=TMP[o+2];
TEMPS_MOTEUR[3]=TMP[o+3];


//ETAT
TEMPS_MOTEUR[4]=TMP[o-2];
TEMPS_MOTEUR[5]=TMP[o-1];


//SEUILH
TEMPS_MOTEUR[6]=TMP[o+4];
TEMPS_MOTEUR[7]=TMP[o+5];
TEMPS_MOTEUR[8]=TMP[o+6];
TEMPS_MOTEUR[9]=TMP[o+7];



//SEUILB
TEMPS_MOTEUR[10]=TMP[o-11];
TEMPS_MOTEUR[11]=TMP[o-10];
TEMPS_MOTEUR[12]=TMP[o-9];
TEMPS_MOTEUR[13]=TMP[o-8];



TEMPS_MOTEUR[14]=TMP[o-7];
TEMPS_MOTEUR[15]=TMP[o-6];
TEMPS_MOTEUR[16]=TMP[o-5];
TEMPS_MOTEUR[17]=TMP[o-4];
TEMPS_MOTEUR[18]=TMP[o-3];
//INITY...????



LIRE_EEPROM(6,256,&TMP,3);
delay_qms(100);

//---------------------------------------------------9

o=35;

//VALEUR
PRESENCE_220V[0]=TMP[o];
PRESENCE_220V[1]=TMP[o+1];
PRESENCE_220V[2]=TMP[o+2];
PRESENCE_220V[3]=TMP[o+3];

//ETAT
PRESENCE_220V[4]=TMP[o-2];
PRESENCE_220V[5]=TMP[o-1];



o=o+24;

//------------------------------------------------------10


//VALEUR
PRESENCE_48V[0]=TMP[o];
PRESENCE_48V[1]=TMP[o+1];
PRESENCE_48V[2]=TMP[o+2];
PRESENCE_48V[3]=TMP[o+3];

//ETAT
PRESENCE_48V[4]=TMP[o-2];
PRESENCE_48V[5]=TMP[o-1];




}



void variables_groupe(char t)

{

				valeur[0]=0x20;
				valeur[1]=0x20;
				valeur[2]=0x20;
				valeur[3]=0x20;
				
				
				ETAT[0]=0x20;
				ETAT[1]=0x20;
				ETAT[2]=0x20;
				ETAT[3]=0x20;
				
				
				
				SEUIL[0]=0x20;
				SEUIL[1]=0x20;
				SEUIL[2]=0x20;
				SEUIL[3]=0x20;
				
				SEUIL2[0]=0x20;
				SEUIL2[1]=0x20;
				SEUIL2[2]=0x20;
				SEUIL2[3]=0x20;

				iiii[0]=0x20;
				iiii[1]=0x20;
				iiii[2]=0x20;
				iiii[3]=0x20;
				iiii[4]=0x20;



if (t==1)
{
				valeur[0]=P_RESERVOIR[0];
				valeur[1]=P_RESERVOIR[1];
				valeur[2]=P_RESERVOIR[2];
				valeur[3]=P_RESERVOIR[3];
				
				
				ETAT[0]=P_RESERVOIR[4];
				ETAT[1]=P_RESERVOIR[5];
				ETAT[2]=' ';
				ETAT[3]=' ';
				
				
				
				SEUIL[0]=P_RESERVOIR[6];
				SEUIL[1]=P_RESERVOIR[7];
				SEUIL[2]=P_RESERVOIR[8];
				SEUIL[3]=P_RESERVOIR[9];
				
				SEUIL2[0]=P_RESERVOIR[10];
				SEUIL2[1]=P_RESERVOIR[11];
				SEUIL2[2]=P_RESERVOIR[12];
				SEUIL2[3]=P_RESERVOIR[13];
}



if (t==2)
{
				valeur[0]=P_SORTIE[0];
				valeur[1]=P_SORTIE[1];
				valeur[2]=P_SORTIE[2];
				valeur[3]=P_SORTIE[3];
				
				
				ETAT[0]=P_SORTIE[4];
				ETAT[1]=P_SORTIE[5];
				ETAT[2]=' ';
				ETAT[3]=' ';
				
				
				
				SEUIL[0]=P_SORTIE[6];
				SEUIL[1]=P_SORTIE[7];
				SEUIL[2]=P_SORTIE[8];
				SEUIL[3]=P_SORTIE[9];
				
				SEUIL2[0]=P_SORTIE[10];
				SEUIL2[1]=P_SORTIE[11];
				SEUIL2[2]=P_SORTIE[12];
				SEUIL2[3]=P_SORTIE[13];
}



if (t==3)
{
				valeur[0]=P_SECOURS[0];
				valeur[1]=P_SECOURS[1];
				valeur[2]=P_SECOURS[2];
				valeur[3]=P_SECOURS[3];
				
				
				ETAT[0]=P_SECOURS[4];
				ETAT[1]=P_SECOURS[5];
				ETAT[2]=' ';
				ETAT[3]=' ';
				
				
				
				SEUIL[0]=P_SECOURS[6];
				SEUIL[1]=P_SECOURS[7];
				SEUIL[2]=P_SECOURS[8];
				SEUIL[3]=P_SECOURS[9];
				
				SEUIL2[0]=P_SECOURS[10];
				SEUIL2[1]=P_SECOURS[11];
				SEUIL2[2]=P_SECOURS[12];
				SEUIL2[3]=P_SECOURS[13];
}


if (t==4)
{
				valeur[0]=POINT_ROSEE[0];
				valeur[1]=POINT_ROSEE[1];
				valeur[2]=POINT_ROSEE[2];
				valeur[3]=POINT_ROSEE[3];
				
				
				ETAT[0]=POINT_ROSEE[4];
				ETAT[1]=POINT_ROSEE[5];
				ETAT[2]=' ';
				ETAT[3]=' ';
				
				
				
				SEUIL[0]=POINT_ROSEE[6];
				SEUIL[1]=POINT_ROSEE[7];
				SEUIL[2]=POINT_ROSEE[8];
				SEUIL[3]=POINT_ROSEE[9];
				
			
}


if (t==5)
{
				valeur[0]=DEBIT_GLOBAL[0];
				valeur[1]=DEBIT_GLOBAL[1];
				valeur[2]=DEBIT_GLOBAL[2];
				valeur[3]=DEBIT_GLOBAL[3];
				
				
				ETAT[0]=DEBIT_GLOBAL[4];
				ETAT[1]=DEBIT_GLOBAL[5];
				ETAT[2]=' ';
				ETAT[3]=' ';
				
				
				
				SEUIL[0]=DEBIT_GLOBAL[6];
				SEUIL[1]=DEBIT_GLOBAL[7];
				SEUIL[2]=DEBIT_GLOBAL[8];
				SEUIL[3]=DEBIT_GLOBAL[9];
				
	
}


if (t==6)
{
				valeur[0]=TEMPS_CYCLE_MOTEUR[0];
				valeur[1]=TEMPS_CYCLE_MOTEUR[1];
				valeur[2]=TEMPS_CYCLE_MOTEUR[2];
				valeur[3]=TEMPS_CYCLE_MOTEUR[3];
				
				
				ETAT[0]=TEMPS_CYCLE_MOTEUR[4];
				ETAT[1]=TEMPS_CYCLE_MOTEUR[5];
				ETAT[2]=' ';
				ETAT[3]=' ';
				
				
				
				SEUIL[0]=TEMPS_CYCLE_MOTEUR[6];
				SEUIL[1]=TEMPS_CYCLE_MOTEUR[7];
				SEUIL[2]=TEMPS_CYCLE_MOTEUR[8];
				SEUIL[3]=TEMPS_CYCLE_MOTEUR[9];

}



if (t==8)
{
				valeur[0]=TEMPS_MOTEUR[0];
				valeur[1]=TEMPS_MOTEUR[1];
				valeur[2]=TEMPS_MOTEUR[2];
				valeur[3]=TEMPS_MOTEUR[3];
				
				
				ETAT[0]=TEMPS_MOTEUR[4];
				ETAT[1]=TEMPS_MOTEUR[5];
				ETAT[2]=' ';
				ETAT[3]=' ';
				
				
				
				SEUIL[0]=TEMPS_MOTEUR[6];
				SEUIL[1]=TEMPS_MOTEUR[7];
				SEUIL[2]=TEMPS_MOTEUR[8];
				SEUIL[3]=TEMPS_MOTEUR[9];
				
				SEUIL2[0]=TEMPS_MOTEUR[10];
				SEUIL2[1]=TEMPS_MOTEUR[11];
				SEUIL2[2]=TEMPS_MOTEUR[12];
				SEUIL2[3]=TEMPS_MOTEUR[13];
				iiii[0]=TEMPS_MOTEUR[14];
				iiii[1]=TEMPS_MOTEUR[15];
				iiii[2]=TEMPS_MOTEUR[16];
				iiii[3]=TEMPS_MOTEUR[17];
				iiii[4]=TEMPS_MOTEUR[18];


}


if (t==9)
{
				valeur[0]=PRESENCE_220V[0];
				valeur[1]=PRESENCE_220V[1];
				valeur[2]=PRESENCE_220V[2];
				valeur[3]=PRESENCE_220V[3];
				
				
				ETAT[0]=PRESENCE_220V[4];
				ETAT[1]=PRESENCE_220V[5];
				ETAT[2]=' ';
				ETAT[3]=' ';
				
				
	
}





if (t==10)
{
				valeur[0]=PRESENCE_48V[0];
				valeur[1]=PRESENCE_48V[1];
				valeur[2]=PRESENCE_48V[2];
				valeur[3]=PRESENCE_48V[3];
				
				
				ETAT[0]=PRESENCE_48V[4];
				ETAT[1]=PRESENCE_48V[5];
				ETAT[2]=' ';
				ETAT[3]=' ';
				
				
				
	
}







}


void Recuperer_param_cms(void)


{
char p;
LIRE_EEPROM(6,1024,&TMP,5);


for (p=0;p<30;p++) //IDENTIFIANT RACK
{
ID_rack[p]=TMP[8+p];
}


T_alarme[0]=TMP[38];
T_alarme[1]=TMP[39];
T_alarme[2]=TMP[40];

T_sortie[0]=TMP[41];
T_sortie[1]=TMP[42];
T_sortie[2]=TMP[43];



HH[0]='1';
HH[1]='1';
mm[0]='2';
mm[1]='2';

JJ[0]='0';
JJ[1]='1';
MM[0]='0';
MM[1]='1';
AA[0]='2';
AA[1]='0';
AA[2]='1';
AA[3]='7';

 
for (p=0;p<15;p++)
{
nr_cog[p]=TMP[p+44];
}
 
 

acc_u[0]=TMP[104];
acc_u[1]=TMP[105];
acc_u[2]=TMP[106];
acc_u[3]=TMP[107];

acc_conf[0]=TMP[108];
acc_conf[1]=TMP[109];
acc_conf[2]=TMP[110];
acc_conf[3]=TMP[111];


for (p=0;p<15;p++)
{
nr_cms[p]=TMP[p+59];
}


for (p=0;p<15;p++)
{
passerelle[p]=TMP[p+74];
}

 
 


for (p=0;p<15;p++)
{
DNS[p]=TMP[p+89];
}
 

}


unsigned char envoyerHORLOGE_i2c(unsigned char NumeroH,unsigned char ADDH, unsigned char dataH0)
{
	
 StartI2C();
   WriteI2C(NumeroH);     // adresse de l'horloge temps r�el
   WriteI2C(ADDH);
   WriteI2C(dataH0);
   StopI2C();
   delay_qms(11);
 				
}



unsigned char ECRIRE_HORLOGE(unsigned char zone,unsigned char Time)
{
unsigned char TMP;

if (zone==0)
zone=zone|0x80; //laisse l'horloge CHbits a 1 secondes tres important si ce bit n'est pas a 1 lhorloge ne marche pas
if (zone==1)
zone=zone&0x7F; //laisse le bit7 a zero minutes
if (zone==2)
zone=zone&0x3F;//met a zero les bits 6 et 7 pour le mode 24h  
if (zone==3)
zone=zone&0x07; // laisse ts les bits sauf 0 1 2 a 0 
if (zone==4)
zone=zone&0x3F; // met a 0 les bits 7 et 6 date 
if (zone==5)
zone=zone&0x1F; // met a 0 les bits 7 et 6 et 5 mois
if (zone==6)
zone=zone;
if (zone==7)
zone=zone; //registre control ex: 00010000 =) oscillation de 1hz out





TMP=envoyerHORLOGE_i2c(0xD0,zone,Time);
return(TMP);
}

signed char litHORLOGE_i2c(unsigned char NumeroH,unsigned char AddH) 
{     


  char Data;
  
  StartI2C();
  WriteI2C(NumeroH&0xFE);        // adresse de la DS1307
  WriteI2C(AddH);     // suivant le choix utilisateur
  RestartI2C();
  WriteI2C(NumeroH);        // on veut lire
  Data = ReadI2C();     // lit la valeur 
  StopI2C();
  return(Data);



} 



unsigned char LIRE_HORLOGE(unsigned char zone) //LIRE LES DONNEES HORLOGE AUX CHOIX
{
unsigned char TMP;
TMP=litHORLOGE_i2c(0xD1,zone); 


if (zone==0)
TMP=TMP&0x7F; // supprimle le bit 7 seconde
if (zone==1)
TMP=TMP&0x7F; //laisse le bit7 a zero minutes
if (zone==2)
TMP=TMP&0x3F;//met a zero les bits 6 et 7 pour le mode 24h  
if (zone==3)
TMP=TMP&0x07; // laisse ts les bits sauf 0 1 2 a 0 
if (zone==4)
TMP=TMP&0x3F; // met a 0 les bits 7 et 6 date 
if (zone==5)
TMP=TMP&0x1F; // met a 0 les bits 7 et 6 et 5 mois
if (zone==6)
TMP=TMP;










return (TMP);
}



void creer_trame_horloge (void)  //CREATION D'UNR TRAME AVEC LES DONNEES DE L'HORLOGE JJMMsshhmmAA
{

char HEURE,MINUTE,SECONDE,MOIS,JOUR,ANNEE,DATE;


ANNEE=LIRE_HORLOGE(6);
delay_qms(10);
MOIS=LIRE_HORLOGE(5);
delay_qms(10);
DATE=LIRE_HORLOGE(4);
delay_qms(10);
JOUR=LIRE_HORLOGE(3);
delay_qms(10);
HEURE=LIRE_HORLOGE(2);
delay_qms(10);
MINUTE=LIRE_HORLOGE(1);
delay_qms(10);
SECONDE=LIRE_HORLOGE(0);

Tab_Horodatage[1] = (DATE & 0x0F) + 48;
   Tab_Horodatage[0] = ((DATE & 0x30) >> 4) + 48;      // conversion BCD => ASCII
 
   Tab_Horodatage[3] = (MOIS & 0x0F) + 48;  
  Tab_Horodatage[2] = ((MOIS & 0x10) >> 4) + 48;

 Tab_Horodatage[5] = (SECONDE & 0x0F) + 48;
     Tab_Horodatage[4] = ((SECONDE & 0x70) >> 4) + 48;
 
 Tab_Horodatage[7] = (HEURE & 0x0F) + 48; 
 Tab_Horodatage[6] = ((HEURE & 0x30) >> 4) + 48;      // conversion BCD => ASCII
  

Tab_Horodatage[9] = (MINUTE& 0x0F) + 48;
   Tab_Horodatage[8] = ((MINUTE & 0x70) >> 4) + 48;

   

 Tab_Horodatage[11] = (ANNEE & 0x0F) + 48;
   Tab_Horodatage[10] = ((ANNEE & 0xF0) >> 4) + 48;
  











//HORLOGE[12];




}


void entrer_trame_horloge (void) //PREPARE TRAME POUR ECRIRE L'HORLOGE EN FORMAT 

//hh mm ss JJ MM AA 
{
char HEURE,MINUTE,SECONDE,MOIS,JOUR,ANNEE,DATE;
//ASCII->BCD
HEURE=(HORLOGET[0]-48)<<4; 
HEURE=HEURE+ (HORLOGET[1]-48) ;
MINUTE=(HORLOGET[2]-48)<<4; 
MINUTE=MINUTE+ (HORLOGET[3]-48) ;
SECONDE=(HORLOGET[4]-48)<<4; 
SECONDE=SECONDE+ (HORLOGET[5]-48) ;
DATE=(HORLOGET[6]-48)<<4;
DATE=DATE+ (HORLOGET[7]-48) ;
MOIS=(HORLOGET[8]-48)<<4; 
MOIS=MOIS+ (HORLOGET[9]-48) ;
ANNEE=(HORLOGET[10]-48)<<4;
ANNEE=ANNEE + (HORLOGET[11]-48);





ECRIRE_HORLOGE(2,HEURE);
ECRIRE_HORLOGE(1,MINUTE);
ECRIRE_HORLOGE(0,SECONDE);
ECRIRE_HORLOGE(4,DATE);
ECRIRE_HORLOGE(5,MOIS);
ECRIRE_HORLOGE(6,ANNEE);

//HORLOGE[12];

 


}



void LIRE_HEURE_MINITEL(void)
{


I2CHORLG=0;  	 	
creer_trame_horloge ();  //VERIFICATION JJMMsshhmmAA
I2CHORLG=1;
HH[0]=Tab_Horodatage[6];
HH[1]=Tab_Horodatage[7];
mm[0]=Tab_Horodatage[8];
mm[1]=Tab_Horodatage[9];
JJ[0]=Tab_Horodatage[0];
JJ[1]=Tab_Horodatage[1];
MM[0]=Tab_Horodatage[2];
MM[1]=Tab_Horodatage[3];

AA[0]='2';
AA[1]='0';
AA[2]=Tab_Horodatage[10];
AA[3]=Tab_Horodatage[11];
ss[0]=Tab_Horodatage[4];
ss[1]=Tab_Horodatage[5];

}


char gestion_rob(void)
{
char exist;

if (capt_exist=1) //si le cable existe avec un capteur reel






exist=0;
return(exist);
}


char TYPEDECARTE(void)

{
char tampon; 

tampon=TYPECARTE[4]; //POUR LA VOIE 0x81 a 100 
if (VOIECOD[0]=='0')
{
				if ((VOIECOD[1]>='6')&&(VOIECOD[1]<'8')) //ENTRE 06x et x79
				tampon=TYPECARTE[3];
				if ((VOIECOD[1]>='4')&&(VOIECOD[1]<'6')) //ENTRE 04x et x59
				tampon=TYPECARTE[2];
				if ((VOIECOD[1]>='2')&&(VOIECOD[1]<'4')) //ENTRE 02x et x39
				tampon=TYPECARTE[1];
				if ((VOIECOD[1]>='0') && (VOIECOD[1]<'2')) //ENTRE 00x et x19
				tampon=TYPECARTE[0];


if ((VOIECOD[2]=='0')&&(VOIECOD[1]=='8')) //x80
tampon=TYPECARTE[3];
if ((VOIECOD[2]=='0')&&(VOIECOD[1]=='6')) //x60
tampon=TYPECARTE[2];
if ((VOIECOD[2]=='0')&&(VOIECOD[1]=='4')) //x40
tampon=TYPECARTE[1];
if ((VOIECOD[2]=='0')&&(VOIECOD[1]=='2')) //x20
tampon=TYPECARTE[0];



 


}

return tampon;
}

 
void testINT2 (void)

{

if (INTCON3bits.INT2IF==1)
{
if (DEMANDE_DE_LAUTO==0xFF)
{

DEMANDE_DE_LAUTO=0x00;
REA_DESACTIVE=0xFF;
LEDRJTEST2=0;
LEDRJTEST1=0;
}
else
{
DEMANDE_DE_LAUTO=0xFF;
LEDRJTEST2=1;
LEDRJTEST1=1;
}

INTCON3bits.INT2IF=0;
}

}






void TRAMECAPTEURBIDON(char *taz)
{

TMP[0]='#';//#
TMP[1]=taz[1];
TMP[2]=taz[2];
TMP[3]=taz[3];
TMP[4]=taz[4];
TMP[5]=taz[5];
TMP[6]='A';
TMP[7]='0';
TMP[8]='0';
TMP[9]='0';
TMP[10]='0';
TMP[11]='0';
TMP[12]='0';
TMP[13]='0';
TMP[14]='0';
TMP[15]='0';
TMP[16]='0';
TMP[17]='0';
TMP[18]='0';
TMP[19]='P';
TMP[20]='R';
TMP[21]='O';
TMP[22]='B';
TMP[23]='L';
TMP[24]='E';
TMP[25]='M';
TMP[26]='E';
TMP[27]=' ';
TMP[28]=' ';
TMP[29]=' ';
TMP[30]=' ';
TMP[31]=' ';
TMP[32]=' ';
TMP[33]=' ';
TMP[34]=' ';
TMP[35]=' ';
TMP[36]=' ';
TMP[37]=' ';
TMP[38]=' ';
TMP[39]=' ';
TMP[40]=' ';
TMP[41]=' ';
TMP[42]=' ';
TMP[43]=' ';
TMP[44]=' ';
TMP[45]=' ';
TMP[46]=' ';
TMP[47]=' ';
TMP[48]=' ';
TMP[49]=' ';
TMP[50]=taz[1];
TMP[51]=taz[2];
TMP[52]=taz[3];
TMP[53]='0';
TMP[54]='1';
TMP[55]='0';
TMP[56]='1';
TMP[57]='0';
TMP[58]='0';
TMP[59]='0';
TMP[60]='0';
TMP[61]='?';
TMP[62]='0';
TMP[63]='1';
TMP[64]='0';
TMP[65]='2';
TMP[66]='?';
TMP[67]='?';
TMP[68]='?';
TMP[69]='?';
TMP[70]='?';
TMP[71]='?';
TMP[72]='?';
TMP[73]='?';
TMP[74]='?';
TMP[75]='5';
TMP[76]='0';
TMP[77]='0';
TMP[78]='0';
TMP[79]='0';
TMP[80]='0';
TMP[81]='0';
TMP[82]='0';
TMP[83]='0';
TMP[84]='0';
TMP[85]='2';
TMP[86]='2';
TMP[87]='7';
TMP[88]='6';
TMP[89]='7';
TMP[90]='*';


}


char MOUCHARD2(void)
{
unsigned int iop,ghd,jkl,tgy;



				//	iop=128*ghd+30000;

				//	LIRE_EEPROM(3,iop,&TRAMEIN,4); //LIRE DANS EEPROM lexistance de toutes le voies

DEMANDE_INFOS_CAPTEURS[4]='0';
DEMANDE_INFOS_CAPTEURS[5]='0';	
		
iop=1;
			for (ghd=0;ghd<=16;ghd++)
			{
			LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS),calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS),&TMP,1); //LIRE DANS EEPROM				
			//TEMPO(1);
			delay_qms(10);
			TRAMEOUT[0]=0x1B;
			TRAMEOUT[1]=0x5B;
			TRAMEOUT[2]=0x30;
			TRAMEOUT[3]=0x6D;
	
			TRAMEOUT[4]=0x1B;
			TRAMEOUT[5]=0x5B;
	
			TRAMEOUT[8]=0X3B;
			TRAMEOUT[9]=0x30;
			TRAMEOUT[10]=0x31; //COL 1
			TRAMEOUT[11]=0x48;
			for (jkl=0;jkl<120;jkl++)
			{
			TRAMEOUT[jkl+12]=TMP[jkl+6];	//ON PLACE LA TRAME MAIS DECALEE DE 12 LIGNE COL
			}
			if (iop==10)
			{
			tgy=1;
			//iop=0;
			iop=0;	
			}
			TRAMEOUT[6]=0x30+tgy; // 0
			TRAMEOUT[7]=0x30+iop; // 1 ligne
			//TRAMEOUT[38]=iop+0x30;
			//TRAMEOUT[39]=tgy+0x30;
			//TRAMEOUT[13]=TRAMEOUT[13]+0x30;	
				
			TRAMEENVOIRS232DONNEES (128);

					
			if (DEMANDE_INFOS_CAPTEURS[5]=='9')
			{
			DEMANDE_INFOS_CAPTEURS[5]=47;
			DEMANDE_INFOS_CAPTEURS[4]='1';
			} 				
			iop++;
			DEMANDE_INFOS_CAPTEURS[5]=DEMANDE_INFOS_CAPTEURS[5]+1;			
			
			}








}



char MOUCHARD(char carte)


{
int iop,ghd,jlm,jkl,tgy;
jlm=carte;
if (carte=='1')
carte=1;
if (carte=='2')
carte=21;
if (carte=='3')
carte=41;
if (carte=='4')
carte=61;
if (carte=='5')
carte=81;

tgy=0;
iop=0;
	for (ghd=carte;ghd<=(carte+19);ghd++) //ON ENV. LES TABLES D'EXISTANCES
			{
			LIRE_EEPROM(3,128*ghd+30000,&TRAMEIN,4); //LIRE DANS EEPROM	
			
	
			
			for (jkl=0;jkl<24;jkl++)
			{
			TRAMEOUT[jkl+12]=TRAMEIN[jkl];	//ON PLACE LA TRAME MAIS DECALEE DE 12 LIGNE COL
			}

			TRAMEOUT[0]=0x1B;
			TRAMEOUT[1]=0x5B;
			TRAMEOUT[2]=0x30;
			TRAMEOUT[3]=0x6D;
	
			TRAMEOUT[4]=0x1B;
			TRAMEOUT[5]=0x5B;
	
			TRAMEOUT[8]=0X3B;
			TRAMEOUT[9]=0x30;
			TRAMEOUT[10]=0x31; //COL 1
			TRAMEOUT[11]=0x48;
	
			TRAMEOUT[36]='c';
			TRAMEOUT[37]=jlm;
			
		
			TRAMEOUT[40]='/';
			
			//TRAMEIN[0]=0x31;
			tgy++;
			if (tgy==10)
			{
			iop++;
			tgy=0;
			//TRAMEIN[0]=TRAMEIN[0]+1;
			}
			TRAMEOUT[6]=0x30+iop; // 0
			TRAMEOUT[7]=0x30+tgy; // 1 ligne
			TRAMEOUT[38]=iop+0x30;
			TRAMEOUT[39]=tgy+0x30;
			TRAMEOUT[13]='x';	



			TRAMEENVOIRS232DONNEES (41);
			delay_qms(10);
			}

//	TEMPO(5);

}		