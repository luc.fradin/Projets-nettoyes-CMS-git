/*********************************************************************
 *
 *	Generic TCP Server Example Application
 *  Module for Microchip TCP/IP Stack
 *   -Implements an example "ToUpper" TCP server on port 9760 and 
 *	  should be used as a basis for creating new TCP server 
 *    applications
 *
 *********************************************************************
 * FileName:        GenericTCPServer.c
 * Dependencies:    TCP
 * Processor:       PIC18, PIC24F, PIC24H, dsPIC30F, dsPIC33F, PIC32
 * Compiler:        Microchip C32 v1.05 or higher
 *					Microchip C30 v3.12 or higher
 *					Microchip C18 v3.30 or higher
 *					HI-TECH PICC-18 PRO 9.63PL2 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright (C) 2002-2009 Microchip Technology Inc.  All rights
 * reserved.
 *
 * Microchip licenses to you the right to use, modify, copy, and
 * distribute:
 * (i)  the Software when embedded on a Microchip microcontroller or
 *      digital signal controller product ("Device") which is
 *      integrated into Licensee's product; or
 * (ii) ONLY the Software driver source files ENC28J60.c, ENC28J60.h,
 *		ENCX24J600.c and ENCX24J600.h ported to a non-Microchip device
 *		used in conjunction with a Microchip ethernet controller for
 *		the sole purpose of interfacing with the ethernet controller.
 *
 * You should refer to the license agreement accompanying this
 * Software for additional information regarding your rights and
 * obligations.
 *
 * THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT
 * WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT
 * LIMITATION, ANY WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * MICROCHIP BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF
 * PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY CLAIMS
 * BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE
 * THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER
 * SIMILAR COSTS, WHETHER ASSERTED ON THE BASIS OF CONTRACT, TORT
 * (INCLUDING NEGLIGENCE), BREACH OF WARRANTY, OR OTHERWISE.
 *
 *
 * Author               Date    	Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Howard Schlunder     10/19/06	Original
 * Microchip            08/11/10    Added ability to close session by
 *                                  pressing the ESCAPE key.
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Description of how to run the demo:
 *   1) Connect the ethernet port of the programmed demo board to a 
 *        computer either directly or through a router.
 *   2) Determine the IP address of the demo board.  This can be done several
 *        different ways.
 *      a) If you are using a demo setup with an LCD display (e.g. Explorer 16
 *           or PICDEM.net 2), the IP address should be displayed on the second
 *           line of the display.
 *      b) Open the Microchip Ethernet Device Discoverer from the start menu.
 *           Press the "Discover Devices" button to see the addresses and host
 *           names of all devices with the Announce Protocol enabled on your
 *           network.  You may have to configure your computer's firewall to 
 *           prevent it from blocking UDP port 30303 for this solution.
 *      c) If your board is connected directly with your computer with a
 *           crossover cable: 
 *              1) Open a command/DOS prompt and type 'ipconfig'.  Find the 
 *                   network adaptor that is connected to the board.  The IP
 *                   address of the board is located in the 'Default Gateway'
 *                   field
 *              2) Open up the network status for the network adaptor that
 *                   connects the two devices.  This can be done by right clicking
 *                   on the network connection icon in the network settings folder 
 *                   and select 'status' from the menu. Find the 'Default Gateway'
 *                   field.
 *   3) Open a command/DOS prompt.  Type "telnet ip_address 9760" where
 *        ip_address is the IP address that you got from step 2.
 *   4) As you type characters, they will be echoed back in your command prompt
 *        window in UPPER CASE.
 *   5) Press Escape to end the demo.
 *
 ********************************************************************/
#define __GENERICTCPSERVER_C
#include <i2c.h> 
#include "TCPIPConfig.h"
//#include "echange.h"
//if defined(STACK_USE_GENERIC_TCP_SERVER_EXAMPLE)

#include "TCPIP Stack/TCPIP.h"


// Defines which port the server will listen on
#define SERVER_PORT	1111
extern unsigned int CRC16(char far *txt, unsigned  int lg_txt);
void IntToChar(unsigned int value, char *chaine,char Precision);
//unsigned int CRC16(char far *txt, unsigned  char lg_txt);
void delay_qms(char tempo);
extern void TEMPO(char seconde);
/*****************************************************************************
  Function:
	void GenericTCPServer(void)

  Summary:
	Implements a simple ToUpper TCP Server.

  Description:
	This function implements a simple TCP server.  The function is invoked
	periodically by the stack to listen for incoming connections.  When a 
	connection is made, the server reads all incoming data, transforms it
	to uppercase, and echos it back.
	
	This example can be used as a model for many TCP server applications.

  Precondition:
	TCP is initialized.

  Parameters:
	None

  Returns:
  	None
  ***************************************************************************/

extern char nbrerecep,iboucle;

int CRC;
WORD wMaxGet, wMaxPut, wCurrentChunk;
BYTE i=0;
	WORD w, w2;
TCP_SOCKET	MySocket;
extern char	ETAT_CONNECT_MINITEL;
// extern char   	    IO_EN_COM=0; // IO EST PAS EN COM
extern char			OUVERTUREIP;
extern char tableau[60];
extern char STOPMINITEL;
extern BYTE TRAMEOUT[512];

extern char TRAMEIN[125];
//CHAR TRAMEOUT[32];
extern char ATTENDRERECEPTIONIP;
extern char PASDEDEMANDECONNECTIONTCPIP;

extern char ID_rack[30];

void GenericTCPServer(void)
{
 	char i, conf_minitel, conf_appel;
//	BYTE TRAMEOUT[32];
	
	
	static enum _TCPServerState
	{
		SM_HOME = 0,
		SM_LISTENING,
        SM_CLOSING,
	} TCPServerState = SM_HOME;

	i=0;
	conf_minitel=0;
	conf_appel=0;


//if (ATTENDRERECEPTIONIP==0xFF) //connection detect� en mode interne aller a recption
//{
//ATTENDRERECEPTIONIP==0x00; //recpetion normale
//goto RECEPTIONNER;
//}
//INITGESTIONCARTEIO ();
//PASDEDEMANDECONNECTIONTCPIP=0x00; 




	switch(TCPServerState)
	{
		case SM_HOME:
			// Allocate a socket for this server to listen and accept connections on
			//MySocket = TCPOpen(0, TCP_OPEN_SERVER, SERVER_PORT, TCP_PURPOSE_GENERIC_TCP_SERVER);
			//if(MySocket == INVALID_SOCKET)
			//	return;

			TCPServerState = SM_LISTENING;
			break;

		case SM_LISTENING: //0x01 se met en ecoute
			// See if anyone is connected to us
			//if(!TCPIsConnected(MySocket))
			//{
//				PASDEDEMANDECONNECTIONTCPIP=0x00;	
			//	return;

			//} 
			//else
			//{
//					if (ATTENDRERECEPTIONIP==0xAA) // MODE DETECTION PUIS RECPETION
//					{
//					PASDEDEMANDECONNECTIONTCPIP=0xFF;
//					ATTENDRERECEPTIONIP==0xFF;	
//					return;
//					}

			//}


			// Figure out how many bytes have been received and how many we can transmit.
//		wMaxGet = TCPIsGetReady(MySocket);	// Get TCP RX FIFO byte count
//			wMaxPut = TCPIsPutReady(MySocket);	
//	wMaxGet = wMaxPut;
				
			// Make sure we don't take more bytes out of the RX FIFO than we can put into the TX FIFO
			

			// Process all bytes that we can
			// This is implemented as a loop, processing up to sizeof(TRAMEOUT) bytes at a time.  
			// This limits memory usage while maximizing performance.  Single byte Gets and Puts are a lot slower than multibyte GetArrays and PutArrays.
RECEPTIONNER:			
		//	for(w = 0; w < wMaxGet; w += sizeof(TRAMEOUT))
		//{
//RECEPTION PREMIER CODE TELGAT
for (nbrerecep=0;nbrerecep<20;nbrerecep++) //NANCY
{
	TRAMEIN[nbrerecep]='#';
}
//TRAMEIN[2]=' '; //16092020 
wMaxGet=0;
do
{
			if(!TCPIsConnected(MySocket))
			return;

			StackTask();
     
        	StackApplications();
			wMaxGet = TCPIsGetReady(MySocket);	// Get TCP RX FIFO byte count
			wMaxPut = TCPIsPutReady(MySocket);	// Get TCP TX FIFO free space
}while (wMaxGet==0);




TCPGetArray(MySocket, TRAMEIN, wMaxGet); //RECEPTION DE TELGAT

conf_minitel = 0;
conf_appel = 0;

for (i=0; i<=wMaxGet;i++)
{
	if((TRAMEIN[i]=='I') && (TRAMEIN[i+1]==':') && (TRAMEIN[i+2]=='M') && (TRAMEIN[i+3]=='I') && (TRAMEIN[i+4]=='N')){
		conf_minitel = 1;
		break;
	}
	if((TRAMEIN[i]=='I') && (TRAMEIN[i+1]==':') && (TRAMEIN[i+2]=='C') && (TRAMEIN[i+3]=='O') && (TRAMEIN[i+4]=='G')){
		conf_appel = 1;
		break;
	}	
}


//ANALYSE DE LA DEMANDE
if (conf_minitel)//MINITEL I:MIN  max 01102020
{
	conf_minitel=0;
	STOPMINITEL=0x00;
    minitel(); //LANCEMENT DU MINITEL
}

				// Transfer the data out of the TCP RX FIFO and into our local processing buffer.

				
if (conf_appel) //TELGAT I:COG
{
          	conf_appel=0;
			TRAMEOUT[0]=0x02;
			TRAMEOUT[1]='C';
			TRAMEOUT[2]=':';
			TRAMEOUT[3]='N';
			TRAMEOUT[4]=0x03;
			CRC=CRC16(TRAMEOUT, 5);   ///////// calcul de CRC
			IntToChar(CRC,tableau,5);   //////// conversion du CRC du int en char
			TRAMEOUT[5]=tableau[0];
			TRAMEOUT[6]=tableau[1];
			TRAMEOUT[7]=tableau[2];
			TRAMEOUT[8]=tableau[3];
			TRAMEOUT[9]=tableau[4];
			TRAMEOUT[10]=0x0D;	
// Transfer the data out of our local processing buffer and into the TCP TX FIFO.
TCPPutArray(MySocket, TRAMEOUT, 11); // TRANSMETTRE LA TRAME
}
else 
{goto close;}


/* DEBUG 485
TRAMEOUT[12]='*';
TRAMEENVOIRS485DONNEES (); //NANCY
for (nbrerecep=0;nbrerecep<10;nbrerecep++) //NANCY
{
TRAMEIN[nbrerecep]='#';
}
*/
iboucle=0;
nbrerecep=0;
et1:
wMaxGet=0;
do 
{
if(!TCPIsConnected(MySocket))
return;

StackTask();     
StackApplications();
wMaxGet = TCPIsGetReady(MySocket);	// Get TCP RX FIFO byte count
wMaxPut = TCPIsPutReady(MySocket);	// Get TCP TX FIFO free space
}while (wMaxGet==0);

TCPGetArray(MySocket, TRAMEIN,2); //ATTENDRE ACK OU NOACK
//TCPGetArray(MySocket, TRAMEIN, wCurrentChunk); //ATTENDRE ACK OU NOACK

/* DEBUG 485
TRAMEOUT[0]='#';
for (nbrerecep=1;nbrerecep<4;nbrerecep++) //NANCY
{
TRAMEOUT[nbrerecep]=TRAMEIN[nbrerecep-1];
}
TRAMEOUT[4]='*';
TRAMEENVOIRS485DONNEES (); //NANCY
iboucle++;
//if (iboucle<=3)
//goto et1;
*/

//TEMPO(15); //BART
		nbrerecep=0;
		if (TRAMEIN[0]==0x05) //NO ACK
		{
		nbrerecep=100;
		//goto close;
		}
	    if (TRAMEIN[0]==0x06) //ACK
		{
		nbrerecep=50;	
		}
		if (TRAMEIN[0]!=0x06) //AUTRE
		{
		if (nbrerecep==0)
		goto et1;
		//nbrerecep++;
		}





//////////////////////////////////////////////
TRAMEOUT[0]=0x02;
TRAMEOUT[1]='S';   
TRAMEOUT[2]=':';
 TRAMEOUT[3]=ID_rack[0];
                                                                    TRAMEOUT[4]=ID_rack[1];
                                                                    TRAMEOUT[5]=ID_rack[2];
                                                                    TRAMEOUT[6]=ID_rack[3];
                                                                    TRAMEOUT[7]=ID_rack[4];
                                                                    TRAMEOUT[8]=ID_rack[5];
                                                                    

                                                                    TRAMEOUT[9]=ID_rack[6];
                                                                    TRAMEOUT[10]=ID_rack[7];
                                                                    TRAMEOUT[11]=ID_rack[8];
                                                                    TRAMEOUT[12]=ID_rack[9];
                                                                    TRAMEOUT[13]=ID_rack[10];
																	TRAMEOUT[14]=ID_rack[11];
                                                                    TRAMEOUT[15]=ID_rack[12];
                                                                    TRAMEOUT[16]=ID_rack[13];
                                                                    TRAMEOUT[17]=ID_rack[14];
                                                                    TRAMEOUT[18]=ID_rack[15];
																	TRAMEOUT[19]=ID_rack[16];
                                                                    TRAMEOUT[20]=ID_rack[17];
                                                                    TRAMEOUT[21]=ID_rack[18];
                                                                    TRAMEOUT[22]=ID_rack[19];






TRAMEOUT[23]=0x03;
CRC=CRC16(TRAMEOUT, 24);   ///////// calcul de CRC
IntToChar(CRC,tableau,5);   //////// conversion du CRC du int en char
TRAMEOUT[24]=tableau[0];
TRAMEOUT[25]=tableau[1];
TRAMEOUT[26]=tableau[2];
TRAMEOUT[27]=tableau[3];
TRAMEOUT[28]=tableau[4];
TRAMEOUT[29]=0x0D;
TCPPutArray(MySocket, TRAMEOUT, 30); //JUSQUA LA LONGUEUR DU




/*
TRAMEOUT[31]='*';
TRAMEENVOIRS485DONNEES (); //NANCY





for (nbrerecep=0;nbrerecep<10;nbrerecep++) //NANCY
{
TRAMEIN[nbrerecep]='#';
}
*/

iboucle=0;
nbrerecep=0;
et2:
do
{
if(!TCPIsConnected(MySocket))
return;
StackTask();     
StackApplications();
wMaxGet = TCPIsGetReady(MySocket);	// Get TCP RX FIFO byte count
wMaxPut = TCPIsPutReady(MySocket);	// Get TCP TX FIFO free space
}while (wMaxGet==0);
//TCPGetArray(MySocket, TRAMEIN, wCurrentChunk); // ATENDRE ACK OU NOACK
TCPGetArray(MySocket, TRAMEIN, 2); //ATTENDRE ACK OU NOACK


/* //DEBUG 485
TRAMEOUT[0]='#';
for (nbrerecep=1;nbrerecep<4;nbrerecep++) //NANCY
{
TRAMEOUT[nbrerecep]=TRAMEIN[nbrerecep-1];
}
TRAMEOUT[4]='*';
TRAMEENVOIRS485DONNEES (); //NANCY
*/
iboucle++;
//if (iboucle<=3)
//goto et2;



//TEMPO(15); //BART
		nbrerecep=0;
		if (TRAMEIN[0]==0x05) //NO ACK
		{
		nbrerecep=100;
		//goto close;
		}
	    if (TRAMEIN[0]==0x06) //ACK
		{
		nbrerecep=50;	
		}
		if (TRAMEIN[0]!=0x06) //AUTRE
		{
		if (nbrerecep==0)
		goto et2;
		//nbrerecep++;
		}




TRAMEOUT[0]=0x02;
TRAMEOUT[1]='E';
TRAMEOUT[2]=':';
TRAMEOUT[3]='0';
TRAMEOUT[4]=0x03;
//CRC=CRC16(TRAMEOUT, 5);   ///////// calcul de CRC
CRC=CRC16(TRAMEOUT, 5);   ///////// calcul de CRC
IntToChar(CRC,tableau,5);   //////// conversion du CRC du int en char
TRAMEOUT[5]=tableau[0];
TRAMEOUT[6]=tableau[1];
TRAMEOUT[7]=tableau[2];
TRAMEOUT[8]=tableau[3];
TRAMEOUT[9]=tableau[4];
TRAMEOUT[10]=0x0D;
TCPPutArray(MySocket, TRAMEOUT, 11);

/* DEBUG 485
TRAMEOUT[12]='*';
TRAMEENVOIRS485DONNEES (12); //NANCY

for (nbrerecep=0;nbrerecep<10;nbrerecep++) //NANCY
{
TRAMEIN[nbrerecep]='#';
}
*/


iboucle=0;
nbrerecep=0;
et3:
do
{
if(!TCPIsConnected(MySocket))
return;
StackTask();     
StackApplications();
wMaxGet = TCPIsGetReady(MySocket);	// Get TCP RX FIFO byte count
wMaxPut = TCPIsPutReady(MySocket);	// Get TCP TX FIFO free space
}while (wMaxGet==0);
TCPGetArray(MySocket, TRAMEIN, 2); // ATTENDRE ACK OU NOACK




/*
TRAMEOUT[0]='#';
for (nbrerecep=1;nbrerecep<4;nbrerecep++) //NANCY
{
TRAMEOUT[nbrerecep]=TRAMEIN[nbrerecep-1];
}
TRAMEOUT[4]='*';
TRAMEENVOIRS485DONNEES (); //NANCY
*/

iboucle++;
//if (iboucle<=3)
//goto et3;

//TEMPO(15); //BART
		nbrerecep=0;
		if (TRAMEIN[0]==0x05) //NO ACK
		{
		nbrerecep=100;
		//goto close;
		}
	    if (TRAMEIN[0]==0x06) //ACK
		{
		nbrerecep=50;	
		}
		if (TRAMEIN[0]!=0x06) //AUTRE
		{
		if (nbrerecep==0)
		goto et3;
		//nbrerecep++;
		}



 

//ENVOYER_GROUPE(); 
echange(); //TELGAT INFOS GROUPE ET CAPTEURS


     
TCPServerState = SM_CLOSING;

break;

close :
		case SM_CLOSING:
			// Close the socket connection.
            TCPClose(MySocket);

			TCPServerState = SM_HOME;
			PASDEDEMANDECONNECTIONTCPIP=0; //PAS DE CONNECTION AU PREMIER TEST IL DETECTE UNE CONNECTION????
			ETAT_CONNECT_MINITEL=0;
    	    //IO_EN_COM=0; // IO EST PAS EN COM
			OUVERTUREIP=0; // MODE DETECTION PUIS RECPETION
			break;
}
}



 

