
#include <p18f8723.h>

#define TRIS_Data    TRISD				//				OK_RI
#define b_TRIS_GCS1  TRISJbits.TRISJ0 //GCS1   		OK_RI
#define b_TRIS_GCS2  TRISJbits.TRISJ1 //GCS2   		OK_RI
#define b_TRIS_RSDI  TRISEbits.TRISE0 //RSDI   		OK_RI
#define b_TRIS_RW    TRISEbits.TRISE1 //RW     		OK_RI
#define b_TRIS_E     TRISEbits.TRISE2 //E      		OK_RI
#define b_TRIS_Rst   TRISHbits.TRISH1 //RST    		OK_RI

#define GLCD_Data   PORTD				//				OK_RI
#define b_GLCD_GCS1 LATJbits.LATJ0 //CGS1   		OK_RI
#define b_GLCD_GCS2 LATJbits.LATJ1 //CGS2 	 		OK_RI
#define b_GLCD_RSDI LATEbits.LATE0 //RSDI    		OK_RI
#define b_GLCD_RW   LATEbits.LATE1 //RW      		OK_RI
#define b_GLCD_E    LATEbits.LATE2 //E       		OK_RI
#define b_GLCD_Rst  LATHbits.LATH1 //RST     		OK_RI


	

void Delay(void);
unsigned char GLCD_Read(void);
void Wait_Not_Busy(void);
void GLCD_Write_Cmd(unsigned char data);
void GLCD_Write_Data (unsigned char data);
void ClearScreen(void);
void Init_GLCD(void);
void PutChar(unsigned char data);
void PutCharInv(unsigned char data);
unsigned char GLCD_Read_Data(void);
void SetPos(unsigned char x,unsigned char y);
void WritePosition(void);
void plot(unsigned char x,unsigned char y);
void hline(unsigned char x1,unsigned char x2,unsigned char y);
//void hline_tiret(unsigned char x1,unsigned char x2,unsigned char y);
void vline(unsigned char x,unsigned char y1,unsigned char y2);
void unhline(unsigned char x1,unsigned char x2,unsigned char y);
void unvline(unsigned char x,unsigned char y1,unsigned char y2);
void box(unsigned char x1,unsigned char y1,unsigned char x2,unsigned char y2);
void unbox(unsigned char x1,unsigned char y1,unsigned char x2,unsigned char y2);
void circle(int xc,int yc,int r);
void voyant_on(unsigned char x,unsigned char y);
void voyant_off(unsigned char x,unsigned char y);
void PutMessage( char rom *Message);
void PutMessageInv( char rom *Message);
void PutLogo( char far rom *logo);
void FillWhiteZone(unsigned char x1,unsigned char y1,unsigned char x2,unsigned char y2);
void FillBlackZone(unsigned char x1,unsigned char y1,unsigned char x2,unsigned char y2);
void unplot(unsigned char x,unsigned char y);
void displayNum(int x, int y, short int theValue);
void displayNumInv(int x, int y, short int theValue);
void PutSmallChar(unsigned char data);
void displaySmallNum(int x, int y, short int INTValue);
void PutSmallCharInv(unsigned char data);
void displaySmallNumInv(int x, int y, short int INTValue);
void PutFloat(char* floatdata);
void PutFloatInv(char* floatdata);
void PutHChar(unsigned char data);
void MoveDown(void);
void PutHFloat(char* floatdata);
void MoveRightH(void);
//void MoveRightSmall(void);
void displayHNum(int x, int y, short int INTValue);
void flecheH(unsigned char x,unsigned char y);
void flecheB(unsigned char x,unsigned char y);
void flecheD(unsigned char x,unsigned char y);
void flecheG(unsigned char x,unsigned char y);
void coche_on(unsigned char x,unsigned char y);
void coche_off(unsigned char x,unsigned char y);









