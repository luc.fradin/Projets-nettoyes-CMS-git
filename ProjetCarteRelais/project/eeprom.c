// eeprom.c
// GESTION DE L'EEPROM

#include <stdio.h>
#include <string.h>
#include <p18f8723.h>
#include <stdlib.h>
#include <delays.h> 
#include <eeprom.h> 


/******************** WriteEEPROM ****************************************************/
void WriteEEPROM(unsigned char ad,unsigned char data) 
{ /* Ecrit un seul octet dans l'eeprom de sauvegarde du PIC18 */
static unsigned char GIE_Status; 
EEADR = ad;  			//EEPROM memory location : LSB
//EEADRH = ad >> 8;  		//EEPROM memory location : MSB
EEADRH=0;
EEDATA = data;     		//Data to be writen  
EECON1bits.EEPGD=0;    	//Enable EEPROM write 
EECON1bits.CFGS=0;   	//Enable EEPROM write 
EECON1bits.WREN = 1;  	//Enable EEPROM write 
GIE_Status = INTCONbits.GIE; //Save global interrupt enable bit 
INTCONbits.GIE=0;   	//Disable global interrupts 
EECON2 = 0x55;    		//Required sequence to start write cycle 
EECON2 = 0xAA;     		//Required sequence to start write cycle 
EECON1bits.WR = 1;    	//Required sequence to start write cycle 
INTCONbits.GIE=GIE_Status;  //Restore the original global interrupt status 
while(EECON1bits.WR);   //Wait for completion of write sequence 
PIR2bits.EEIF = 0;    	//Disable EEPROM write // EEprom Interruption flag bit 
EECON1bits.WREN = 0;  	//Disable EEPROM write 
} // fin WriteEEPROM

/******************** ReadEEPROM ****************************************************/
unsigned char ReadEEPROM(unsigned char ad) 
{ /* Lit un seul octet dans l'eeprom de sauvegarde du PIC18 */
unsigned int memory_data; 
EEADR = ad;  			//EEPROM memory location : LSB
//EEADRH = ad >> 8;  		//EEPROM memory location : MSB 
EEADRH=0;
EECON1bits.EEPGD = 0;   //Enable read sequence 
EECON1bits.CFGS = 0;   	//Enable read sequence 
EECON1bits.RD = 1;   	//Enable read sequence 
Delay1TCY( ); 			//Delay to ensure read is completed 
Delay1TCY( ); 			// ~ NOP
return (EEDATA) ; 		// memory_data         
} // fin ReadEEPROM

/******************** eepmess ******************************************************/
void eepmess(unsigned int ad, unsigned char *p) // �crit une chaine p � l'adresse ad
{
	while (*p) WriteEEPROM(ad++,*p++);
}// fin eepmess

/******************** Wr_eeprom_int ************************************************/
void Wr_eeprom_int( unsigned int adr_eeprom,  int data)
{ 	/* ecrit un INT (un entier ) dans l'eeprom du PIC18
	adr_eeprom = adresse du destinataire dans l'eeprom : Wr_eeprom_int( & adr_eeprom,  data)/ (le pointeurest convertit en INT))  !!
	data = donn�e � m�moriser en INT ou unsigned INT
	Note : L'octet MSB est � droite, l'octet LSB est � gauche
	*/
	unsigned int temp ; // variable temporaire

	temp  = data;
	temp = temp >>8;				// msb
	WriteEEPROM(adr_eeprom+1,temp); 
	temp = (char) data;				// lsb
	WriteEEPROM(adr_eeprom,temp); 

} // fin Wr_eeprom_int

/******************** Wr_eeprom_double ************************************************/
void Wr_eeprom_double( unsigned int adr_eeprom, char *data)
{ 	/* ecrit un LONG ou un DOUBLE dans l'eeprom du PIC18
	adr_eeprom = adresse du destinataire dans l'eeprom : Wr_eeprom_int( & adr_eeprom,  data)/ (le pointeurest convertit en INT))  !!
	*data = adresse de la donn�e � m�moriser sur 4 octets
	Note : L'octet MSB est � droite, l'octet LSB est� gauche
	*/

	WriteEEPROM(adr_eeprom, *data );         // lsb_L
	data ++ ;
	WriteEEPROM(adr_eeprom+1, *data ); // lsb_M
	data ++ ;
	WriteEEPROM(adr_eeprom+2, *data ); // msb_L
	data ++ ;
	WriteEEPROM(adr_eeprom+3, *data ); // msb_M

} // fin Wr_eeprom_double

/******************** Rd_eeprom_int ************************************************/
unsigned int Rd_eeprom_int( unsigned int adr_eeprom)
{	/* lit un INT dans l'eeprom du PIC18
	adr_eeprom = adresse du destinataire dans l'eeprom : Rd_eeprom_int( & adr_eeprom,  data)/ (le pointeurest convertit en INT))  !!
	Retourne la donn�e m�moris�e en INT
	*/
	unsigned int temp ; // variable temporaire
	temp = ReadEEPROM(adr_eeprom+1);	// Lit le MSB
	temp = temp << 8;	// Recup MSB
	temp = temp + ReadEEPROM(adr_eeprom);	// Lit le LSB
	return (temp) ; // memory_data;
} // fin Rd_eeprom_int

/******************** Rd_eeprom_double ************************************************/
void Rd_eeprom_double( unsigned int adr_eeprom, char *data )
{	/* lit un DOUBLE ou un LONG dans l'eeprom du PIC18
	adr_eeprom = adresse de la source l'eeprom : Rd_eeprom_double( & adr_eeprom,  &data)/ (le pointeur adr_eeprom est convertit en INT))  !!
	Place la donn�e m�moris�e � l'adresse indiqu�e par *char
	*/

	*data  = ReadEEPROM(adr_eeprom) ;
	data ++ ;
	*data = ReadEEPROM(adr_eeprom + 1 );
	data ++ ;
	*data = ReadEEPROM(adr_eeprom + 2 );
	data ++ ;
	*data = ReadEEPROM(adr_eeprom + 3 );

} // fin Rd_eeprom_double

/******************** Wr_eeprom_str ************************************************/
void Wr_eeprom_str ( unsigned int adr_eeprom,  char *string, unsigned int long_string )
{ 	/* ecrit une chaine de caracteres dans l'eeprom du PIC18
	adr_eeprom = adresse du destinataire dans l'eeprom : Wr_eeprom_int( & adr_eeprom,  data)/ (le pointeurest convertit en INT))  !!
	string = Adresse ( en INT ou en *Ptr ) de la chaine de donn�es � m�moriser en INT
	long_string = longueur de la chaine de caracteres
	*/
	unsigned int temp = 0 ; // variable temporaire

while (temp++ < long_string) WriteEEPROM(adr_eeprom++,*string++);

} // fin Wr_eeprom_str

/******************** Rd_eeprom_str ************************************************/
void Rd_eeprom_str ( unsigned int adr_eeprom,  char *string, unsigned int long_string )
{ 	/* Lit une chaine de caracteres dans l'eeprom du PIC18
	adr_eeprom = adresse du destinataire dans l'eeprom : Rd_eeprom_int( & adr_eeprom,  data)/ (le pointeurest convertit en INT))  !!
	string = Adresse (en *Ptr ) ou ranger la chaine de donn�es r�cup�r�e en m�moire
	long_string = longueur de la chaine de caracteres
	*/
	unsigned int temp = 0 ; // variable temporaire
	unsigned char temp_char = 0 ; // variable temporaire

while (temp++ < long_string) 
	{
	temp_char = ReadEEPROM(adr_eeprom++);
	*string++ = temp_char;	
	// *string++ = ReadEEPROM(adr_eeprom++);
	//string++;
	}	
} // fin Rd_eeprom_str
/***********************************************************************************/


