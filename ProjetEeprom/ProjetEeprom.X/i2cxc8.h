/* 
 * File:   i2cxc8.h
 * Author: ADECEF
 *
 * Created on 8 juin 2021, 08:58
 */

#ifndef I2CXC8_H

//#include <xc.h>


#define   SSPENB    			0b00100000  	/* Enable serial port and configures SCK, SDO, SDI*/
#define   SLAVE_7   			0b00000110     	/* I2C Slave mode, 7-bit address*/
#define   SLAVE_10  			0b00000111    	/* I2C Slave mode, 10-bit address*/
#define   MASTER    			0b00001000     	/* I2C Master mode */
#define   MASTER_FIRMW			0b00001011		//I2C Firmware Controlled Master mode (slave Idle)
#define   SLAVE_7_STSP_INT 		0b00001110		//I2C Slave mode, 7-bit address with Start and Stop bit interrupts enabled
#define   SLAVE_10_STSP_INT 	0b00001111		//I2C Slave mode, 10-bit address with Start and Stop bit interrupts enabled
#define   SLEW_OFF  			0b10000000  	/* Slew rate disabled for 100kHz mode */
#define   SLEW_ON   			0b00000000  	/* Slew rate enabled for 400kHz mode  */



#define EnableIntI2C1                   (PIE1bits.SSP1IE = 1)
#define DisableIntI2C1                   (PIE1bits.SSP1IE = 0)
#define SetPriorityIntI2C1(priority)     (IPR1bits.SSP1IP = priority)
#define I2C1_Clear_Intr_Status_Bit     (PIR1bits.SSP1IF = 0)
#define I2C1_Intr_Status		PIR1bits.SSP1IF

#define StopI2C1()  SSP1CON2bits.PEN=1;while(SSP1CON2bits.PEN)
#define StopI2C StopI2C1

#define StartI2C1()  SSP1CON2bits.SEN=1;while(SSP1CON2bits.SEN)
#define StartI2C StartI2C1 

#define RestartI2C1()  SSP1CON2bits.RSEN=1;while(SSP1CON2bits.RSEN)
#define RestartI2C RestartI2C1

#define NotAckI2C1()     SSP1CON2bits.ACKDT=1, SSP1CON2bits.ACKEN=1;while(SSP1CON2bits.ACKEN)
#define NotAckI2C NotAckI2C1

#define AckI2C1()        SSP1CON2bits.ACKDT=0, SSP1CON2bits.ACKEN=1;while(SSP1CON2bits.ACKEN)
#define AckI2C AckI2C1

#define IdleI2C1()    while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W))
#define IdleI2C IdleI2C1

#define CloseI2C1()  SSP1CON1 &=0xDF
#define CloseI2C CloseI2C1

void OpenI2C1(  unsigned char sync_mode,  unsigned char slew );
#define OpenI2C OpenI2C1

#define DataRdyI2C1()    (SSP1STATbits.BF)
#define DataRdyI2C DataRdyI2C1

unsigned char ReadI2C1( void );
#define ReadI2C ReadI2C1
#define  getcI2C1  ReadI2C1
#define  getcI2C getcI2C1

signed char WriteI2C1(  unsigned char data_out );
#define WriteI2C WriteI2C1
#define  putcI2C1  WriteI2C1
#define  putcI2C putcI2C1

signed char getsI2C1(  unsigned char *rdptr,  unsigned char length );
#define getsI2C getsI2C1

signed char putsI2C1(  unsigned char *wrptr );
#define putsI2C putsI2C1

signed char EEAckPolling1(  unsigned char cControl );
#define EEAckPolling EEAckPolling1

signed char EEByteWrite1(  unsigned char cControl,
                            unsigned int address,
                            unsigned char data );
#define EEByteWrite EEByteWrite1

signed char EEPageWrite1(  unsigned char cControl,
                            unsigned int address,
                            unsigned char *wrptr );
#define EEPageWrite EEPageWrite1

signed int  EERandomRead1(  unsigned char cControl,  unsigned int address );
#define EERandomRead EERandomRead1

signed char EESequentialRead1(  unsigned char cControl,
                                 unsigned int address,
                                 unsigned char *rdptr,
                                 unsigned char length );
#define EESequentialRead EESequentialRead1


#define I2C1_SCL	TRISCbits.TRISC3
#define I2C1_SDA	TRISCbits.TRISC4

#endif	/* I2CXC8_H */

