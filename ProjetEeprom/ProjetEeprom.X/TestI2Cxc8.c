// PIC18F8723 Configuration Bit Settings
// 'C' source line config statements


// CONFIG1H
#pragma config OSC = HS         // Oscillator Selection bits (HS oscillator)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Two-Speed Start-up disabled)
 
// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = SBORDIS  // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
#pragma config BORV = 3         // Brown-out Voltage bits (Minimum setting)

// CONFIG2H
#pragma config WDT = OFF        // Watchdog Timer (WDT disabled (control is placed on the SWDTEN bit))
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3L
#pragma config MODE = MC        // Processor Data Memory Mode Select bits (Microcontroller mode)
#pragma config ADDRBW = ADDR20BIT// Address Bus Width Select bits (20-bit Address Bus)
#pragma config DATABW = DATA16BIT// Data Bus Width Select bit (16-bit External Bus mode)
#pragma config WAIT = OFF       // External Bus Data Wait Enable bit (Wait selections are unavailable for table reads and table writes)

// CONFIG3H
#pragma config CCP2MX = PORTC   // CCP2 MUX bit (ECCP2 input/output is multiplexed with RC1)
#pragma config ECCPMX = PORTE   // ECCP MUX bit (ECCP1/3 (P1B/P1C/P3B/P3C) are multiplexed onto RE6, RE5, RE4 and RE3 respectively)
#pragma config LPT1OSC = OFF    // Low-Power Timer1 Oscillator Enable bit (Timer1 configured for higher power operation)
#pragma config MCLRE = ON       // MCLR Pin Enable bit (MCLR pin enabled; RG5 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
#pragma config LVP = OFF        // Single-Supply ICSP Enable bit (Single-Supply ICSP disabled)
#pragma config BBSIZ = BB2K     // Boot Block Size Select bits (1K word (2 Kbytes) Boot Block size)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled (Legacy mode))

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not code-protected)
#pragma config CP1 = OFF        // Code Protection bit Block 1 (Block 1 (004000-007FFFh) not code-protected)
#pragma config CP2 = OFF        // Code Protection bit Block 2 (Block 2 (008000-00BFFFh) not code-protected)
#pragma config CP3 = OFF        // Code Protection bit Block 3 (Block 3 (00C000-00FFFFh) not code-protected)
#pragma config CP4 = OFF        // Code Protection bit Block 4 (Block 4 (010000-013FFFh) not code-protected)
#pragma config CP5 = OFF        // Code Protection bit Block 5 (Block 5 (014000-017FFFh) not code-protected)
#pragma config CP6 = OFF        // Code Protection bit Block 6 (Block 6 (01BFFF-018000h) not code-protected)
#pragma config CP7 = OFF        // Code Protection bit Block 7 (Block 7 (01C000-01FFFFh) not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Boot Block Code Protection bit (Boot Block (000000-0007FFh) not code-protected)
#pragma config CPD = OFF        // Data EEPROM Code Protection bit (Data EEPROM not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not write-protected)
#pragma config WRT1 = OFF       // Write Protection bit Block 1 (Block 1 (004000-007FFFh) not write-protected)
#pragma config WRT2 = OFF       // Write Protection bit Block 2 (Block 2 (008000-00BFFFh) not write-protected)
#pragma config WRT3 = OFF       // Write Protection bit Block 3 (Block 3 (00C000-00FFFFh) not write-protected)
#pragma config WRT4 = OFF       // Write Protection bit Block 4 (Block 4 (010000-013FFFh) not write-protected)
#pragma config WRT5 = OFF       // Write Protection bit Block 5 (Block 5 (014000-017FFFh) not write-protected)
#pragma config WRT6 = OFF       // Write Protection bit Block 6 (Block 6 (01BFFF-018000h) not write-protected)
#pragma config WRT7 = OFF       // Write Protection bit Block 7 (Block 7 (01C000-01FFFFh) not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) not write-protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block (000000-007FFF, 000FFF or 001FFFh) not write-protected)
#pragma config WRTD = OFF       // Data EEPROM Write Protection bit (Data EEPROM not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection bit Block 1 (Block 1 (004000-007FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR2 = OFF      // Table Read Protection bit Block 2 (Block 2 (008000-00BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR3 = OFF      // Table Read Protection bit Block 3 (Block 3 (00C000-00FFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR4 = OFF      // Table Read Protection bit Block 4 (Block 4 (010000-013FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR5 = OFF      // Table Read Protection bit Block 5 (Block 5 (014000-017FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR6 = OFF      // Table Read Protection bit Block 6 (Block 6 (018000-01BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR7 = OFF      // Table Read Protection bit Block 7 (Block 7 (01C000-01FFFFh) not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Boot Block Table Read Protection bit (Boot Block (000000-007FFF, 000FFF or 001FFFh) not protected from table reads executed in other blocks)


#include <xc.h>
#include "main.h"
#include <stdlib.h> //biblioth�que
#include <stdio.h>

// MEMOIRE la taille #03301R111122223333BRA/4444/22/1COMMENTAIRESICIROB03326111525001020000456781108650888* soit 86octets
unsigned char TRAMEOUT[128];
unsigned char TRAMEIN[129];

char cCompt;
char tmp;
char cFlag;



void Init_RS485(void);
void RS485(unsigned char carac);
void TRAMEENVOIRS485DONNEES(void);

void Init_I2C(void);
signed char EESequRead(unsigned char cControl, unsigned int address, unsigned char *rdptr, unsigned char length);
signed char EEPaWrite(unsigned char cControl, unsigned int address, unsigned char *wrptr);
unsigned char ECRIRE_EEPROMBYTE(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char g);
unsigned char putstringI2C(unsigned char *wrptr);
unsigned char LIRE_EEPROM(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char r);

//RS485
#define DERE PORTAbits.RA4 // direction RX TX

#define I2C_MEMOIRE PORTJbits.RJ1
#define I2C_HORLOGE PORTJbits.RJ0
#define I2C_INTERNE PORTJbits.RJ2

#define DX2 PORTHbits.RH3


void main(void) {
    
    unsigned char cBcl;
    OpenI2C(MASTER, SLEW_OFF);
    TRISCbits.TRISC4 = 1; //SDA
    TRISCbits.TRISC3 = 1; //SCL

    TRISJbits.TRISJ0 = 0; // I2C HORLOGE
    TRISJbits.TRISJ1 = 0; //I2C MEMOIRE
    TRISJbits.TRISJ2 = 0; //I2C INTERNE

    Init_I2C();
    

    I2C_MEMOIRE = 0;
    I2C_HORLOGE = 0;
    I2C_INTERNE = 1; //ACTIVATION I2C INTERNE

    Init_RS485();
    
    T0CONbits.TMR0ON = 0; //Timer Stopp�
    T0CONbits.T08BIT = 0; //Timer sur 16 bits
    T0CONbits.T0CS = 0; //Utilisation Clock Externe
    T0CONbits.T0SE = 1; //Incr�mentation haut vers bas
    T0CONbits.PSA = 0; //Prescaler Activ�
    T0CONbits.T0PS = 7; //Prescaler � 256
    INTCONbits.TMR0IE = 1;
    INTCON2bits.TMR0IP = 1;
    
    
    // Temps de d�passement Timer0 : prescaler*offset/f

    TMR0L = 0; //Octet bas du Timer � 0
    TMR0H = 0; //Octet haut du Timer � 0

    cFlag = 0;
    cCompt = 0;


    TRAMEIN[0] = '#'; //#
    TRAMEIN[1] = '3';
    TRAMEIN[2] = '0';
    TRAMEIN[3] = '5';
    TRAMEIN[4] = '1';
    TRAMEIN[5] = '5';
    TRAMEIN[6] = 'A';
    TRAMEIN[7] = '1';
    TRAMEIN[8] = '1';
    TRAMEIN[9] = '1';
    TRAMEIN[10] = '1';
    TRAMEIN[11] = '2';
    TRAMEIN[12] = '2';
    TRAMEIN[13] = '2';
    TRAMEIN[14] = '2';
    TRAMEIN[15] = '0';
    TRAMEIN[16] = '0';
    TRAMEIN[17] = '0';
    TRAMEIN[18] = '0';
    TRAMEIN[19] = 'B';
    TRAMEIN[20] = 'R';
    TRAMEIN[21] = 'A';
    TRAMEIN[22] = '/';
    TRAMEIN[23] = '8';
    TRAMEIN[24] = '8';
    TRAMEIN[25] = '8';
    TRAMEIN[26] = '8';
    TRAMEIN[27] = '/';
    TRAMEIN[28] = '2';
    TRAMEIN[29] = '2';
    TRAMEIN[30] = '/';
    TRAMEIN[31] = '1';
    TRAMEIN[32] = 'y';
    TRAMEIN[33] = 'O';
    TRAMEIN[34] = 'M';
    TRAMEIN[35] = 'M';
    TRAMEIN[36] = 'E';
    TRAMEIN[37] = 'N';
    TRAMEIN[38] = 'T';
    TRAMEIN[39] = 'A';
    TRAMEIN[40] = 'I';
    TRAMEIN[41] = 'R';
    TRAMEIN[42] = 'E';
    TRAMEIN[43] = 'S';
    TRAMEIN[44] = 'I';
    TRAMEIN[45] = 'C';
    TRAMEIN[46] = 'I';
    TRAMEIN[47] = 'R';
    TRAMEIN[48] = 'O';
    TRAMEIN[49] = 'B';
    TRAMEIN[50] = '9';
    TRAMEIN[51] = '9';
    TRAMEIN[52] = '3';
    TRAMEIN[53] = '2';
    TRAMEIN[54] = '6';
    TRAMEIN[55] = '1';
    TRAMEIN[56] = '1';
    TRAMEIN[57] = '1';
    TRAMEIN[58] = '5';
    TRAMEIN[59] = '2';
    TRAMEIN[60] = '5';
    TRAMEIN[61] = '0';
    TRAMEIN[62] = '0';
    TRAMEIN[63] = '1';
    TRAMEIN[64] = '0';
    TRAMEIN[65] = '2';
    TRAMEIN[66] = '0';
    TRAMEIN[67] = '0';
    TRAMEIN[68] = '0';
    TRAMEIN[69] = '0';
    TRAMEIN[70] = '4';
    TRAMEIN[71] = '5';
    TRAMEIN[72] = '6';
    TRAMEIN[73] = '7';
    TRAMEIN[74] = '8';
    TRAMEIN[75] = '0';
    TRAMEIN[76] = '1';
    TRAMEIN[77] = '0';
    TRAMEIN[78] = '8';
    TRAMEIN[79] = '6';
    TRAMEIN[80] = '5';
    TRAMEIN[81] = '0';
    TRAMEIN[82] = '8';
    TRAMEIN[83] = '8';
    TRAMEIN[84] = '8';
    TRAMEIN[85] = '2';
    TRAMEIN[86] = '2';
    TRAMEIN[87] = '7';
    TRAMEIN[88] = '6';
    TRAMEIN[89] = '7';
    TRAMEIN[90] = '7';
    TRAMEIN[91] = '-';
    TRAMEIN[92] = '-';
    TRAMEIN[93] = '-';
    TRAMEIN[94] = '-';
    TRAMEIN[95] = '-';
    TRAMEIN[96] = '-';
    TRAMEIN[97] = '-';
    TRAMEIN[98] = '-';
    TRAMEIN[99] = '-';
    TRAMEIN[100] = '-';
    TRAMEIN[101] = '-';
    TRAMEIN[102] = '-';
    TRAMEIN[103] = '-';
    TRAMEIN[104] = '-';
    TRAMEIN[105] = '-';
    TRAMEIN[106] = '-';
    TRAMEIN[107] = '-';
    TRAMEIN[108] = '-';
    TRAMEIN[109] = '-';
    TRAMEIN[110] = '-';
    TRAMEIN[111] = '-';
    TRAMEIN[112] = '-';
    TRAMEIN[113] = '-';
    TRAMEIN[114] = '-';
    TRAMEIN[115] = '-';
    TRAMEIN[116] = '-';
    TRAMEIN[117] = '-';
    TRAMEIN[118] = '-';
    TRAMEIN[119] = '-';
    TRAMEIN[120] = '-';
    TRAMEIN[121] = '-';
    TRAMEIN[122] = '-';
    TRAMEIN[123] = '-';
    TRAMEIN[124] = '-';
    TRAMEIN[125] = '2';
    TRAMEIN[126] = '*';
    TRAMEIN[127] = '5';
    TRAMEIN[128] = 0;

    for(cBcl = 0; cBcl < 128; cBcl++){
    TRAMEOUT[cBcl] = 0; 
    }

    while (1) // boucle infinie
    {
        if(cFlag == 0){
        T0CONbits.TMR0ON = 1; //Timer d�marr�
        tmp = ECRIRE_EEPROMBYTE(0, 128, TRAMEIN, 1); //ECRITURE EN EEPROM
//        EEPageWrite(0x0, 128, TRAMEIN);
        EEAckPolling(0xA0);
//        tmp = LIRE_EEPROM(0, 128, &TRAMEOUT, 1); //LIRE DANS EEPROM

        T0CONbits.TMR0ON = 0; //Timer d�marr�
//        tmp = LIRE_EEPROM(0, 128, &TRAMEOUT, 1); //LIRE DANS EEPROM
        EESequentialRead(0x0, 128, TRAMEOUT, 1);
        TRAMEENVOIRS485DONNEES();
        cFlag = 1;
        }        
    }
}

void Init_RS485(void) {

    unsigned int SPEED;

    TRISCbits.TRISC5 = 0;
    TRISCbits.TRISC7 = 1;
    TRISCbits.TRISC6 = 0;
    TRISCbits.TRISC5 = 0; //RC5 en sortie

    TRISAbits.TRISA4 = 0; //RA4 RX_TX en sortie OU DERE



    RCSTA1bits.SPEN = 1; // Enable serial port
    TXSTA1bits.SYNC = 0; // Async mode
    BAUDCON1bits.BRG16 = 1;

    TXSTA1bits.BRGH = 1; //haute vitesse
    SPEED = 624; // 624 set 9600 bauds 311 pour 19230bauds
    SPBRG1 = SPEED; // Write baudrate to SPBRG1
    SPBRGH1 = SPEED >> 8; // For 16-bit baud rate generation





    IPR1bits.RCIP = 1; // Set high priority
    PIR1bits.RCIF = 0; // met le drapeau d'IT � 0 (plus d'IT)
    PIE1bits.RCIE = 0; // ne g�n�re pas d'IT pour la liaison s�rie

    TXSTA1bits.TXEN = 0; // transmission inhib�e
    RCSTA1bits.RX9 = 0; // r�ception sur 8 bits
    TXSTA1bits.TX9 = 0; // transmission sur 8 bits
    RCSTA1bits.CREN = 0; // interdire reception
    RCONbits.IPEN = 1; // Enable priority levels

}

void TRAMEENVOIRS485DONNEES(void) {
    unsigned char u;
    unsigned char tmp;
    tmp = 0;

    do {
ret1:

        u = TRAMEOUT[tmp];
        if (u == 0xFF) // ON INTERDIT L'ENVOIE DE 0xFF �<NUL>235
            u = 'x';
        RS485(u);
        //if ((u!='=') && (tmp==0))
        //goto ret1;


        tmp = tmp + 1;
    } while ((u != '*') && (tmp < 200));



}

void RS485(unsigned char carac) {
    //carac = code(carac);


    DERE = 1; //AUTORISATION TRANSMETTRE
    DX2 = !DX2;

    TXSTA1bits.TXEN = 0;

    RCSTA1bits.CREN = 0; // interdire reception

    TXSTA1bits.TXEN = 1; // autoriser transmission


    TXREG1 = carac;

    //__no_operation();
    //__no_operation();
    //__no_operation();
    //__no_operation();
    while (TXSTA1bits.TRMT == 0); // attend la fin de l'�mission
    //     __no_operation();
    TXSTA1bits.TXEN = 0;

    DERE = 0; //AUTORISATION TRANSMETTRE
    //delayms(100);
}

void Init_I2C(void) {

    //here is the I2C setup from the Seeval32 code.
    DDRCbits.RC3 = 1; //Configure SCL as Input
    DDRCbits.RC4 = 1; //Configure SDA as Input
    SSP1STAT = 0x00; //Disable SMBus & Slew Rate Control 80
    SSP1CON1 = 0x28; //Enable MSSP Master  28
    SSP1CON2 = 0x00; //Clear MSSP Conr// 0x3B for 400kHz  (24mhz/((4*4OOkHz))-1 = 14 ou 3B en hexol Bits
    SSP1ADD = 0x3B; //0x0E
}

signed char EESequRead(unsigned char cControl, unsigned int address, unsigned char *rdptr, unsigned char length) {
    unsigned char cADDE;
    unsigned char cAdressL, cAdressH;
    
    cADDE = (cControl << 1) & 0xFF;
    cADDE = cADDE | 0xA0;

    cAdressL = (address) & 0xFF;
    cAdressH = (address) >> 8;
    IdleI2C(); // ensure module is idle
    StartI2C(); // initiate START condition
    if (PIR2bits.BCLIF) // test for bus collision
    {
        PIR2bits.BCLIF = 0;
        return ( -1); // return with Bus Collision error 
    } else {
        if (WriteI2C(cADDE)) // write 1 byte 
        {
            StopI2C();
            return ( -3); // set error for write collision
        }

        if (!SSPCON2bits.ACKSTAT) // test for ACK condition received
        {
            if (WriteI2C(cAdressH)) // write word address for EEPROM
            {
                StopI2C();
                return ( -3); // set error for write collision
            }

            while (SSPCON2bits.ACKSTAT);
            if (!SSPCON2bits.ACKSTAT) // test for ACK condition received
            {
                if (WriteI2C(cAdressL)) // data byte for EEPROM
                {
                    StopI2C();
                    return ( -3); // set error for write collision
                }

                while (SSPCON2bits.ACKSTAT);
                if (!SSPCON2bits.ACKSTAT) // test for ACK condition received
                {
                    RestartI2C(); // generate I2C bus restart condition
                    if (WriteI2C(cADDE + 1))// WRITE 1 byte - R/W bit should be 1 for read
                    {
                        StopI2C();
                        return ( -3); // set error for write collision
                    }

                    while (SSPCON2bits.ACKSTAT);
                    if (!SSPCON2bits.ACKSTAT)// test for ACK condition received
                    {
                        if (getsI2C(rdptr, length))// read in multiple bytes
                        {
                            return ( -1); // return with Bus Collision error
                        }

                        NotAckI2C(); // send not ACK condition 
                        StopI2C(); // send STOP condition
                        if (PIR2bits.BCLIF) // test for bus collision
                        {
                            PIR2bits.BCLIF = 0;
                            return ( -1); // return with Bus Collision error 
                        }
                    } else {
                        StopI2C();
                        return ( -2); // return with Not Ack error
                    }
                } else {
                    StopI2C();
                    return ( -2); // return with Not Ack error
                }
            } else {
                StopI2C();
                return ( -2); // return with Not Ack error condition   
            }
        } else {
            StopI2C();
            return ( -2); // return with Not Ack error
        }
    }
    return ( 0); // return with no error
}

signed char EEPaWrite(unsigned char cControl, unsigned int address, unsigned char *wrptr) {
    unsigned char cADDE;
    unsigned char cAdressL, cAdressH;
    
    cADDE = (cControl << 1) & 0xFF;
    cADDE = cADDE | 0xA0;

    cAdressL = (address) & 0xFF;
    cAdressH = (address) >> 8;

    IdleI2C(); // ensure module is idle
    StartI2C(); // initiate START condition
    if (PIR2bits.BCLIF) // test for bus collision
    {
        PIR2bits.BCLIF = 0;
        return ( -1); // return with Bus Collision error 
    } else {
        if (WriteI2C(cADDE)) // write 1 byte - R/W bit should be 0
        {
            StopI2C();
            return ( -3); // return with write collision error
        }
        IdleI2C();
        if (WriteI2C(cAdressH)) // write word address for EEPROM
        {
            StopI2C();
            return ( -3); // set error for write collision
        }
        IdleI2C();
        if (WriteI2C(cAdressL)) // data byte for EEPROM
        {
            StopI2C();
            return ( -3); // set error for write collision
        }
        IdleI2C();
        if (putsI2C(wrptr)) {
            StopI2C();
            return ( -4); // bus device responded possible error
        }
    }

    //IdleI2C();                      // ensure module is idle
    StopI2C(); // send STOP condition
    if (PIR2bits.BCLIF) // test for Bus collision
    {
        PIR2bits.BCLIF = 0;
        return ( -1); // return with Bus Collision error 
    }
    return ( 0); // return with no error
}


unsigned char ECRIRE_EEPROMBYTE(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char g) {
    unsigned char TMP2, TMP;
    unsigned int h;
    unsigned char t;
    unsigned char AddH, AddL;




    //MAXERIE A2
    if (ADDE == 6) //GROUPE ET BLOC NOTES
        ADDE = 12;
    if (ADDE == 5)
        ADDE = 10;
    if (ADDE == 4)
        ADDE = 8;
    if (ADDE == 3)
        ADDE = 6;
    if (ADDE == 2)
        ADDE = 4;
    if (ADDE == 1)
        ADDE = 2;




    TMP2 = ADDE;
    //TMP2=TMP2<<1;
    TMP2 = 0XA0 | TMP2;


    AddL = (ADDHL);
    AddH = (ADDHL) >> 8;




    IdleI2C(); // ensure module is idle
    StartI2C(); // initiate START condition
    WriteI2C(TMP2); // write 1 byte - R/W bit should be 0
    IdleI2C(); // ensure module is idle
    WriteI2C(AddH); // write HighAdd byte to EEPROM
    IdleI2C(); // ensure module is idle
    WriteI2C(AddL); // write LowAdd byte to EEPROM
    IdleI2C(); // ensure module is idle
    putstringI2C(tab); // pointer to data for page write
    IdleI2C(); // ensure module is idle
    StopI2C(); // send STOP condition
    return ( 0); // return with no error

}

unsigned char putstringI2C(unsigned char *wrptr) {



    unsigned char x;

    unsigned int PageSize;
    PageSize = 128;


    for (x = 0; x < PageSize; x++) // transmit data until PageSize
    {
        if (SSPCON1bits.SSPM3) // if Master transmitter then execute the following
        { 
            if (putcI2C(*wrptr)) // write 1 byte
            {
                return ( -3); // return with write collision error
            }
            IdleI2C(); // test for idle condition
            if (SSPCON2bits.ACKSTAT) // test received ack bit state
            {
                return ( -2); // bus device responded with  NOT ACK
            } // terminateputstringI2C() function
        } else // else Slave transmitter
        {
            PIR1bits.SSPIF = 0; // reset SSPIF bit
            SSPBUF = *wrptr; // load SSPBUF with new data
            SSPCON1bits.CKP = 1; // release clock line
            while (!PIR1bits.SSPIF); // wait until ninth clock pulse received
            if ((!SSPSTATbits.R_W) && (!SSPSTATbits.BF))// if R/W=0 and BF=0, NOT ACK was received
            {
                return ( -2); // terminateputstringI2C() function
            }
        }



        wrptr++; // increment pointer



    } // continue data writes until null character
    return ( 0);
}

unsigned char LIRE_EEPROM(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char r) {
    char h;
    unsigned char t;
    unsigned char TMP2;
    unsigned char TMP4;
    unsigned char AddH, AddL;


    //MAXERIE A2
    if (ADDE == 6) //GROUPE et bloc notes
        ADDE = 12;
    if (ADDE == 5)
        ADDE = 10;
    if (ADDE == 4)
        ADDE = 8;
    if (ADDE == 3)
        ADDE = 6;
    if (ADDE == 2)
        ADDE = 4;
    if (ADDE == 1)
        ADDE = 2;





    AddL = ADDHL;
    AddH = ADDHL >> 8;



    TMP2 = ADDE;
    //TMP2=TMP2<<1;
    TMP2 = TMP2 | 0xA0;


    IdleI2C();
    StartI2C();
    //IdleI2C();
    while (SSPCON2bits.SEN);

    WriteI2C(TMP2); // 0 sur le bit RW pour indiquer une lecture
    IdleI2C();




    WriteI2C(AddH); // 0 sur le bit RW pour indiquer une lecture
    IdleI2C();

    while (SSPCON2bits.RSEN);

    WriteI2C(AddL); // 0 sur le bit RW pour indiquer une lecture
    IdleI2C();


    RestartI2C();
    while (SSPCON2bits.RSEN);


    //AckI2C();
    TMP2 = TMP2 | 0x01;
    WriteI2C(TMP2); // 1 sur le bit RW pour indiquer une lecture
    IdleI2C();



    //tab[85]='*';
    if (r == 1) // pour trame memoire
        getsI2C(tab, 86);
    if (r == 0)
        getsI2C(tab, 108);
    if (r == 3)
        getsI2C(tab, 116);
    if (r == 4)
        getsI2C(tab, 24);
    if (r == 5)
        getsI2C(tab, 120);
    if (r == 6)
        getsI2C(tab, 100);


    NotAckI2C();
    while (SSPCON2bits.ACKEN);



    StopI2C();
    while (SSPCON2bits.PEN);
    return (1);
}
