/* 
 * File:   main.h
 * Author: ADECEF
 *
 * Created on 19 mai 2021, 09:28
 */

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.
#include <xc.h>



#ifndef MAIN_H
#define	MAIN_H


#define   SSPENB    			0b00100000  	/* Enable serial port and configures SCK, SDO, SDI*/
#define   SLAVE_7   			0b00000110     	/* I2C Slave mode, 7-bit address*/
#define   SLAVE_10  			0b00000111    	/* I2C Slave mode, 10-bit address*/
#define   MASTER    			0b00001000     	/* I2C Master mode */
#define   MASTER_FIRMW			0b00001011		//I2C Firmware Controlled Master mode (slave Idle)
#define   SLAVE_7_STSP_INT 		0b00001110		//I2C Slave mode, 7-bit address with Start and Stop bit interrupts enabled
#define   SLAVE_10_STSP_INT 	0b00001111		//I2C Slave mode, 10-bit address with Start and Stop bit interrupts enabled

/* SSPSTAT REGISTER */
#define   SLEW_OFF  			0b10000000  	/* Slew rate disabled for 100kHz mode */
#define   SLEW_ON   			0b00000000  	/* Slew rate enabled for 400kHz mode  */

void OpenI2C1(  unsigned char sync_mode,  unsigned char slew );
#define OpenI2C OpenI2C1

#define CloseI2C1()  SSP1CON1 &=0xDF
#define CloseI2C CloseI2C1

#define AckI2C1()        SSP1CON2bits.ACKDT=0, SSP1CON2bits.ACKEN=1;while(SSP1CON2bits.ACKEN)
#define AckI2C AckI2C1
#define IdleI2C1()    while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W))
#define IdleI2C IdleI2C1
#define StartI2C()  SSP1CON2bits.SEN=1;while(SSP1CON2bits.SEN)
#define StopI2C()  SSP1CON2bits.PEN=1;while(SSP1CON2bits.PEN)
#define RestartI2C()  SSP1CON2bits.RSEN=1;while(SSP1CON2bits.RSEN)
signed char WriteI2C1(  unsigned char data_out );
#define WriteI2C WriteI2C1

#define  putcI2C1  WriteI2C1
#define  putcI2C putcI2C1

signed char putsI2C1(  unsigned char *wrptr );
#define putsI2C putsI2C1

unsigned char ReadI2C1( void );
#define ReadI2C ReadI2C1
#define NotAckI2C1()     SSP1CON2bits.ACKDT=1, SSP1CON2bits.ACKEN=1;while(SSP1CON2bits.ACKEN)

#define NotAckI2C NotAckI2C1

signed char getsI2C1( unsigned char *rdptr, unsigned char length );
#define getsI2C getsI2C1

#define  getcI2C1  ReadI2C1
#define  getcI2C getcI2C1

signed char EEAckPolling(unsigned char control);
signed char EEByteWrite(unsigned char control, unsigned int address, unsigned char data);
signed char EEPageWrite(unsigned char control, unsigned int address, unsigned char *wrptr);
signed char EESequentialRead(unsigned char control, unsigned int address, unsigned char *rdptr, unsigned char length);
signed int EERandomRead(unsigned char control, unsigned int address);


#endif	/* MAIN_H */

