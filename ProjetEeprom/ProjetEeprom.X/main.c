/* 
 * File:   main.c
 * Author: ADECEF
 *
 * Created on 19 mai 2021, 08:21
 */

//C:\Program Files (x86)\Microchip\xc8\v1.37\sources\pic18\plib\i2c
//C:\CMS2Luc\ProjetsNettoyesCMSGit\i2c
//C:\Program Files (x86)\Microchip\xc8\v1.37\sources\pic18\plib\USART
//C:\CMS2Luc
#pragma config OSC = HS         // Oscillator Selection bits (HS oscillator)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Two-Speed Start-up disabled)

// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = SBORDIS  // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
#pragma config BORV = 3         // Brown-out Voltage bits (Minimum setting)

// CONFIG2H
#pragma config WDT = OFF        // Watchdog Timer (WDT disabled (control is placed on the SWDTEN bit))
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3L
#pragma config MODE = MC        // Processor Data Memory Mode Select bits (Microcontroller mode)
#pragma config ADDRBW = ADDR20BIT// Address Bus Width Select bits (20-bit Address Bus)
#pragma config DATABW = DATA16BIT// Data Bus Width Select bit (16-bit External Bus mode)
#pragma config WAIT = OFF       // External Bus Data Wait Enable bit (Wait selections are unavailable for table reads and table writes)

// CONFIG3H
#pragma config CCP2MX = PORTC   // CCP2 MUX bit (ECCP2 input/output is multiplexed with RC1)
#pragma config ECCPMX = PORTE   // ECCP MUX bit (ECCP1/3 (P1B/P1C/P3B/P3C) are multiplexed onto RE6, RE5, RE4 and RE3 respectively)
#pragma config LPT1OSC = OFF    // Low-Power Timer1 Oscillator Enable bit (Timer1 configured for higher power operation)
#pragma config MCLRE = ON       // MCLR Pin Enable bit (MCLR pin enabled; RG5 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
#pragma config LVP = OFF        // Single-Supply ICSP Enable bit (Single-Supply ICSP disabled)
#pragma config BBSIZ = BB2K     // Boot Block Size Select bits (1K word (2 Kbytes) Boot Block size)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled (Legacy mode))

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not code-protected)
#pragma config CP1 = OFF        // Code Protection bit Block 1 (Block 1 (004000-007FFFh) not code-protected)
#pragma config CP2 = OFF        // Code Protection bit Block 2 (Block 2 (008000-00BFFFh) not code-protected)
#pragma config CP3 = OFF        // Code Protection bit Block 3 (Block 3 (00C000-00FFFFh) not code-protected)
#pragma config CP4 = OFF        // Code Protection bit Block 4 (Block 4 (010000-013FFFh) not code-protected)
#pragma config CP5 = OFF        // Code Protection bit Block 5 (Block 5 (014000-017FFFh) not code-protected)
#pragma config CP6 = OFF        // Code Protection bit Block 6 (Block 6 (01BFFF-018000h) not code-protected)
#pragma config CP7 = OFF        // Code Protection bit Block 7 (Block 7 (01C000-01FFFFh) not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Boot Block Code Protection bit (Boot Block (000000-0007FFh) not code-protected)
#pragma config CPD = OFF        // Data EEPROM Code Protection bit (Data EEPROM not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not write-protected)
#pragma config WRT1 = OFF       // Write Protection bit Block 1 (Block 1 (004000-007FFFh) not write-protected)
#pragma config WRT2 = OFF       // Write Protection bit Block 2 (Block 2 (008000-00BFFFh) not write-protected)
#pragma config WRT3 = OFF       // Write Protection bit Block 3 (Block 3 (00C000-00FFFFh) not write-protected)
#pragma config WRT4 = OFF       // Write Protection bit Block 4 (Block 4 (010000-013FFFh) not write-protected)
#pragma config WRT5 = OFF       // Write Protection bit Block 5 (Block 5 (014000-017FFFh) not write-protected)
#pragma config WRT6 = OFF       // Write Protection bit Block 6 (Block 6 (01BFFF-018000h) not write-protected)
#pragma config WRT7 = OFF       // Write Protection bit Block 7 (Block 7 (01C000-01FFFFh) not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) not write-protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block (000000-007FFF, 000FFF or 001FFFh) not write-protected)
#pragma config WRTD = OFF       // Data EEPROM Write Protection bit (Data EEPROM not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection bit Block 1 (Block 1 (004000-007FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR2 = OFF      // Table Read Protection bit Block 2 (Block 2 (008000-00BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR3 = OFF      // Table Read Protection bit Block 3 (Block 3 (00C000-00FFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR4 = OFF      // Table Read Protection bit Block 4 (Block 4 (010000-013FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR5 = OFF      // Table Read Protection bit Block 5 (Block 5 (014000-017FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR6 = OFF      // Table Read Protection bit Block 6 (Block 6 (018000-01BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR7 = OFF      // Table Read Protection bit Block 7 (Block 7 (01C000-01FFFFh) not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Boot Block Table Read Protection bit (Boot Block (000000-007FFF, 000FFF or 001FFFh) not protected from table reads executed in other blocks)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "main.h"
#include <xc.h>
//#include <p18F8723.h>
//#include <i2c.h>


//#define SPEED 312 // 624 set 9600 bauds 311 pour 19230bauds  208 pour 28800 Bauds
#define ENVOIE TXSTA1bits.TXEN 
#define RECEPTION RCSTA1bits.CREN
#define FINTRANS TXSTA1bits.TRMT
#define FINRECEPTION PIR1bits.RCIF
#define I2C_MEMOIRE PORTJbits.RJ1
#define I2C_HORLOGE PORTJbits.RJ0
#define I2C_INTERNE PORTJbits.RJ2
#define   MASTER    			0b00001000     	/* I2C Master mode */
#define   SLEW_OFF  			0b10000000  	/* Slew rate disabled for 100kHz mode */
#define SPEED 624

void EnvoieTrame(signed char *cTableau, char cLongueur);
void delay(unsigned int iTemps);
void Ecriture_Eeprom(unsigned char cEeprom, unsigned int iAdress, unsigned char *cDonnee);
void Lecture_Eeprom(unsigned char cEeprom, unsigned int iAdress, unsigned char *cDonnee, unsigned char cLongueur);
unsigned char LIRE_EEPROM(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char r);
unsigned char ECRIRE_EEPROMBYTE(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char g);
signed int putstringI2C(unsigned char *wrptr);
//void ack_poll(void);

void main(void) {

    unsigned char cEeprom[128];
    unsigned char cRecep[128];
    unsigned char cADDE;
    unsigned int iAdress;
    unsigned char AddH, AddL;
    char cFlag;
    signed char cTest;
        I2C_MEMOIRE = 0;
        I2C_HORLOGE = 0;
        I2C_INTERNE = 1; //ACTIVATION I2C INTERNE


    TRISAbits.TRISA4 = 0; //RA4 RX_TX en sortie OU DERE
    PORTAbits.RA4 = 1;
    BAUDCON1bits.BRG16 = 1;
    TXSTA1bits.BRGH = 1; //haute vitesse
    SPBRG1 = SPEED & 0xFF; // Write baudrate to SPBRG1
    SPBRGH1 = SPEED >> 8; // For 16-bit baud rate generation
    TXSTA1bits.SYNC = 0; // Async mode
    RCSTA1bits.SPEN = 1; // Enable serial port

    IPR1bits.RCIP = 1; // Set high priority
    PIE1bits.RCIE = 0; // ne g�n�re pas d'IT pour la liaison s�rie

    RCSTA1bits.RX9 = 0; // r�ception sur 8 bits
    TXSTA1bits.TX9 = 0; // transmission sur 8 bits
    RCONbits.IPEN = 1; // Enable priority levels
    RECEPTION = 0; // interdire reception
    ENVOIE = 0; // autoriser transmission 

    TRISCbits.TRISC4 = 1; //SDA
    TRISCbits.TRISC3 = 1; //SCL

    TRISJbits.TRISJ0 = 0; // I2C HORLOGE
    TRISJbits.TRISJ1 = 0; //I2C MEMOIRE
    TRISJbits.TRISJ2 = 0; //I2C INTERNE
    
    //here is the I2C setup from the Seeval32 code.
    DDRCbits.RC3 = 1; //Configure SCL as Input
    DDRCbits.RC4 = 1; //Configure SDA as Input
    SSP1STAT = 0x00; //Disable SMBus & Slew Rate Control 80
    SSP1CON1 = 0x28; //Enable MSSP Master  28
    SSP1CON2 = 0x00; //Clear MSSP Conr// 0x3B for 400kHz  (24mhz/((4*4OOkHz))-1 = 14 ou 3B en hexol Bits
    SSP1ADD = 0x3B; //0x0E

    for(cFlag=0;cFlag<=127;cFlag++)
    cRecep[cFlag] = 'e';
    cFlag = 0;

    while (1) {

        if (cFlag == 0) {
            
        cEeprom[0] = '#'; //#
        cEeprom[1] = '0';
        cEeprom[2] = '0';
        cEeprom[3] = '9';
        cEeprom[4] = '1';
        cEeprom[5] = '5';
        cEeprom[6] = 'A';
        cEeprom[7] = '1';
        cEeprom[8] = '1';
        cEeprom[9] = '1';
        cEeprom[10] = '1';
        cEeprom[11] = '2';
        cEeprom[12] = '2';
        cEeprom[13] = '2';
        cEeprom[14] = '2';
        cEeprom[15] = '0';
        cEeprom[16] = '0';
        cEeprom[17] = '0';
        cEeprom[18] = '0';
        cEeprom[19] = 'B';
        cEeprom[20] = 'R';
        cEeprom[21] = 'A';
        cEeprom[22] = '/';
        cEeprom[23] = '8';
        cEeprom[24] = '8';
        cEeprom[25] = '8';
        cEeprom[26] = '8';
        cEeprom[27] = '/';
        cEeprom[28] = '2';
        cEeprom[29] = '2';
        cEeprom[30] = '/';
        cEeprom[31] = '1';
        cEeprom[32] = 'C';
        cEeprom[33] = 'O';
        cEeprom[34] = 'M';
        cEeprom[35] = 'M';
        cEeprom[36] = 'E';
        cEeprom[37] = 'N';
        cEeprom[38] = 'T';
        cEeprom[39] = 'A';
        cEeprom[40] = 'I';
        cEeprom[41] = 'R';
        cEeprom[42] = 'E';
        cEeprom[43] = 'S';
        cEeprom[44] = 'I';
        cEeprom[45] = 'C';
        cEeprom[46] = 'I';
        cEeprom[47] = 'R';
        cEeprom[48] = 'O';
        cEeprom[49] = 'B';
        cEeprom[50] = '9';
        cEeprom[51] = '9';
        cEeprom[52] = '3';
        cEeprom[53] = '2';
        cEeprom[54] = '6';
        cEeprom[55] = '1';
        cEeprom[56] = '1';
        cEeprom[57] = '1';
        cEeprom[58] = '5';
        cEeprom[59] = '2';
        cEeprom[60] = '5';
        cEeprom[61] = '0';
        cEeprom[62] = '0';
        cEeprom[63] = '1';
        cEeprom[64] = '0';
        cEeprom[65] = '2';
        cEeprom[66] = '0';
        cEeprom[67] = '0';
        cEeprom[68] = '0';
        cEeprom[69] = '0';
        cEeprom[70] = '4';
        cEeprom[71] = '5';
        cEeprom[72] = '6';
        cEeprom[73] = '7';
        cEeprom[74] = '8';
        cEeprom[75] = '0';
        cEeprom[76] = '1';
        cEeprom[77] = '0';
        cEeprom[78] = '8';
        cEeprom[79] = '6';
        cEeprom[80] = '5';
        cEeprom[81] = '0';
        cEeprom[82] = '8';
        cEeprom[83] = '8';
        cEeprom[84] = '8';
        cEeprom[85] = '2';
        cEeprom[86] = '2';
        cEeprom[87] = '7';
        cEeprom[88] = '6';
        cEeprom[89] = '7';
        cEeprom[90] = '*';
        cEeprom[91] = '-';
        cEeprom[92] = '-';
        cEeprom[93] = '-';
        cEeprom[94] = '-';
        cEeprom[95] = '-';
        cEeprom[96] = '-';
        cEeprom[97] = '-';
        cEeprom[98] = '-';
        cEeprom[99] = '-';
        cEeprom[100] = '-';
        cEeprom[101] = '-';
        cEeprom[102] = '-';
        cEeprom[103] = '-';
        cEeprom[104] = '-';
        cEeprom[105] = '-';
        cEeprom[106] = '-';
        cEeprom[107] = '-';
        cEeprom[108] = '-';
        cEeprom[109] = '-';
        cEeprom[110] = '-';
        cEeprom[111] = '-';
        cEeprom[112] = '-';
        cEeprom[113] = '-';
        cEeprom[114] = '-';
        cEeprom[115] = '-';
        cEeprom[116] = '-';
        cEeprom[117] = '-';
        cEeprom[118] = '-';
        cEeprom[119] = '-';
        cEeprom[120] = '-';
        cEeprom[121] = '-';
        cEeprom[122] = '-';
        cEeprom[123] = '-';
        cEeprom[124] = '-';
        cEeprom[125] = '-';
        cEeprom[126] = '-';
        cEeprom[127] = 0;
//            EnvoieTrame(cEeprom, 128);
            cFlag = 1;


            cADDE = 0;
            iAdress = 0x0000;
            //EnvoieTrame(cEeprom, 120);

            
            CloseI2C();
            OpenI2C(MASTER, SLEW_OFF);

            cTest =  EEByteWrite(cADDE, iAdress, 'O');
            EnvoieTrame(&cTest, 1);
            cTest = EEAckPolling(cADDE);
            EnvoieTrame(&cTest, 1);
            cTest = EERandomRead(cADDE, iAdress);
            EnvoieTrame(&cTest, 1);
            //EnvoieTrame(cRecep, 128);
            CloseI2C();

            cFlag = 1;
        }

        cFlag = 1;

    }
}

void EnvoieTrame(signed char *cTableau, char cLongueur) {
    int iBcl;
    ENVOIE = 1; // autoriser transmission
    for (iBcl = 0; iBcl < cLongueur; iBcl++) {
        TXREG1 = *(cTableau + iBcl);
        while (FINTRANS == 0); //Attente fin transmission  
    }
    ENVOIE = 0;
}

