/* 
 * File:   i2cxc8.c
 * Author: ADECEF
 *
 * Created on 8 juin 2021, 10:16
 */
//#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include "i2cxc8.h"




void OpenI2C1(unsigned char sync_mode, unsigned char slew) {
    SSP1STAT &= 0x3F; // power on state 
    SSP1CON1 = 0x00; // power on state
    SSP1CON2 = 0x00; // power on state
    SSP1CON1 |= sync_mode; // select serial mode 
    SSP1STAT |= slew; // slew rate on/off 

    I2C1_SCL = 1; // Set SCL1 (PORTC,3) pin to input
    I2C1_SDA = 1; // Set SDA1 (PORTC,4) pin to input

    SSP1CON1 |= SSPENB; // enable synchronous serial port 
}

unsigned char ReadI2C1(void) {

    if (((SSP1CON1 & 0x0F) == 0x08) || ((SSP1CON1 & 0x0F) == 0x0B)) //master mode only
        SSP1CON2bits.RCEN = 1; // enable master for 1 byte reception
    while (!SSP1STATbits.BF); // wait until byte received  
    return ( SSP1BUF); // return with read byte 
}

signed char WriteI2C1(unsigned char data_out) {
    SSP1BUF = data_out; // write single byte to SSP1BUF
    if (SSP1CON1bits.WCOL) // test if write collision occurred
        return ( -1); // if WCOL bit is set return negative #
    else {
        if (((SSP1CON1 & 0x0F) != 0x08) && ((SSP1CON1 & 0x0F) != 0x0B)) //slave mode only 
        {
            SSP1CON1bits.CKP = 1; // release clock line 
            while (!PIR1bits.SSP1IF)
                ; // wait until ninth clock pulse received

            {
                if ((!SSP1STATbits.R_W) && (!SSP1STATbits.BF))// if R/W=0 and BF=0, NOT ACK was received
                {
                    return ( -2); //Return NACK
                } else return (0); //Return ACK
            }
        } else if (((SSP1CON1 & 0x0F) == 0x08) || ((SSP1CON1 & 0x0F) == 0x0B)) //master mode only	
        {
            while (SSP1STATbits.BF); // wait until write cycle is complete      
            IdleI2C1(); // ensure module is idle
            if (SSP1CON2bits.ACKSTAT) // test for ACK condition received
                return ( -2); //Return NACK	
            else return ( 0); //Return ACK
        }
    }
}

signed char getsI2C1(unsigned char *rdptr, unsigned char length) {
    while (length--) // perform getcI2C1() for 'length' number of bytes
    {
        *rdptr++ = getcI2C1(); // save byte received
        while (SSP1CON2bits.RCEN); // check that receive sequence is over    

        if (PIR2bits.BCL1IF) // test for bus collision
        {
            return ( -1); // return with Bus Collision error 
        }


        if (((SSP1CON1 & 0x0F) == 0x08) || ((SSP1CON1 & 0x0F) == 0x0B)) //master mode only
        {
            if (length) // test if 'length' bytes have been read
            {
                SSP1CON2bits.ACKDT = 0; // set acknowledge bit state for ACK        
                SSP1CON2bits.ACKEN = 1; // initiate bus acknowledge sequence
                while (SSP1CON2bits.ACKEN); // wait until ACK sequence is over 
            }
        }

    }
    return ( 0); // last byte received so don't send ACK      
}

signed char putsI2C1(unsigned char *wrptr) {
    signed char temp;
    while (*wrptr) // transmit data until null character 
    {
        if (SSP1CON1bits.SSPM3) // if Master transmitter then execute the following
        {
            temp = putcI2C1(*wrptr);
            if (temp) return ( temp);

        } else // else Slave transmitter
        {
            PIR1bits.SSP1IF = 0; // reset SSP1IF bit
            SSP1BUF = *wrptr; // load SSP1BUF with new data
            SSP1CON1bits.CKP = 1; // release clock line 
            while (!PIR1bits.SSP1IF); // wait until ninth clock pulse received

            if ((SSP1CON1bits.CKP) && (!SSP1STATbits.BF))// if R/W=0 and BF=0, NOT ACK was received
            {
                return ( -2); // terminate PutsI2C1() function
            }
        }

        wrptr++; // increment pointer

    } // continue data writes until null character

    return ( 0);
}

signed char EEAckPolling1(unsigned char cControl) {
    unsigned char cADDE;
    cADDE = (cControl << 1) & 0xFF;
    cADDE = cADDE | 0xA0;
    
    IdleI2C(); // ensure module is idle 
    StartI2C(); // initiate START condition

    if (PIR2bits.BCL1IF) // test for bus collision
    {
        PIR2bits.BCL1IF = 0;
        return ( -1); // return with Bus Collision error 
    } else {
        if (WriteI2C(cADDE) == -1) // write byte - R/W bit should be 0
        {
            StopI2C();
            return ( -3); // set error for write collision
        }

        while (SSPCON2bits.ACKSTAT) // test for ACK condition received
        {
            RestartI2C(); // initiate Restart condition
            if (PIR2bits.BCL1IF) // test for bus collision
            {
                PIR2bits.BCLIF = 0;
                return ( -1); // return with Bus Collision error 
            }

            if (WriteI2C(cADDE) == -1) // write byte - R/W bit should be 0
            {
                StopI2C();
                return ( -3); // set error for write collision
            }
        }
    }

    StopI2C(); // send STOP condition     
    if (PIR2bits.BCLIF) // test for bus collision
    {
        PIR2bits.BCLIF = 0;
        return ( -1); // return with Bus Collision error 
    }
    return ( 0); // return with no error     
}

signed char EEByteWrite1(unsigned char cControl, unsigned int address, unsigned char data) {
    unsigned char cADDE;
    unsigned char cAdressL, cAdressH;

    cADDE = (cControl << 1) & 0xFF;
    cADDE = cADDE | 0xA0;

    cAdressL = (address) & 0xFF;
    cAdressH = (address) >> 8;
    IdleI2C(); // ensure module is idle
    StartI2C(); // initiate START condition
    if (PIR2bits.BCLIF) // test for bus collision
    {
        PIR2bits.BCLIF = 0;
        return ( -1); // return with Bus Collision error 
    } else // start condition successful
    {
        if (WriteI2C(cADDE)) // write byte - R/W bit should be 0
        {
            StopI2C();
            return ( -3); // set error for write collision
        }

        if (!SSPCON2bits.ACKSTAT) // test for ACK condition received
        {
            if (WriteI2C(cAdressH)) // write word address for EEPROM
            {
                StopI2C();
                return ( -3); // set error for write collision
            }

            if (!SSPCON2bits.ACKSTAT) // test for ACK condition received
            {
                if (WriteI2C(cAdressL)) // data byte for EEPROM
                {
                    StopI2C();
                    return ( -3); // set error for write collision
                }
                if (!SSPCON2bits.ACKSTAT) {
                    if (WriteI2C(data)) // data byte for EEPROM
                    {
                        StopI2C();
                        return ( -3); // set error for write collision
                    }
                } else {
                    StopI2C();
                    return ( -2); // return with Not Ack error condition  
                }
            } else {
                StopI2C();
                return ( -2); // return with Not Ack error condition   
            }
        } else {
            StopI2C();
            return ( -2); // return with Not Ack error condition   
        }
    }

    //IdleI2C();                      // ensure module is idle  
    StopI2C(); // send STOP condition
    if (PIR2bits.BCLIF) // test for bus collision
    {
        PIR2bits.BCLIF = 0;
        return ( -1); // return with Bus Collision error 
    }
    return ( 0); // return with no error
}

signed char EEPageWrite1(unsigned char cControl, unsigned int address, unsigned char *wrptr) {
    unsigned char cADDE;
    unsigned char cAdressL, cAdressH;
    
    cADDE = (cControl << 1) & 0xFF;
    cADDE = cADDE | 0xA0;

    cAdressL = (address) & 0xFF;
    cAdressH = (address) >> 8;

    IdleI2C(); // ensure module is idle
    StartI2C(); // initiate START condition
    if (PIR2bits.BCLIF) // test for bus collision
    {
        PIR2bits.BCLIF = 0;
        return ( -1); // return with Bus Collision error 
    } else {
        if (WriteI2C(cADDE)) // write 1 byte - R/W bit should be 0
        {
            StopI2C();
            return ( -3); // return with write collision error
        }
        IdleI2C();
        if (WriteI2C(cAdressH)) // write word address for EEPROM
        {
            StopI2C();
            return ( -3); // set error for write collision
        }
        IdleI2C();
        if (WriteI2C(cAdressL)) // data byte for EEPROM
        {
            StopI2C();
            return ( -3); // set error for write collision
        }
        IdleI2C();
        if (putsI2C(wrptr)) {
            StopI2C();
            return ( -4); // bus device responded possible error
        }
    }
    StopI2C(); // send STOP condition
    if (PIR2bits.BCLIF) // test for Bus collision
    {
        PIR2bits.BCLIF = 0;
        return ( -1); // return with Bus Collision error 
    }
    return ( 0); // return with no error
}

signed int EERandomRead1(unsigned char cControl, unsigned int address) {

    unsigned char cADDE;
    unsigned char cAdressL, cAdressH;
    
    cADDE = (cControl << 1) & 0xFF;
    cADDE = cADDE | 0xA0;

    cAdressL = (address) & 0xFF;
    cAdressH = (address) >> 8;

    IdleI2C(); // ensure module is idle
    StartI2C(); // initiate START condition
    if (PIR2bits.BCLIF) // test for bus collision
    {
        PIR2bits.BCLIF = 0;
        return ( -1); // return with Bus Collision error 
    } else {
        if (WriteI2C(cADDE)) // write 1 byte
        {
            StopI2C();
            return ( -3); // return with write collision error
        }

        if (!SSPCON2bits.ACKSTAT) // test for ACK condition received
        {
            if (WriteI2C(cAdressH)) // write word address for EEPROM
            {
                StopI2C();
                return ( -3); // set error for write collision
            }

            if (!SSPCON2bits.ACKSTAT) // test for ACK condition received
            {
                if (WriteI2C(cAdressL)) // data byte for EEPROM
                {
                    StopI2C();
                    return ( -3); // set error for write collision
                }

                if (!SSPCON2bits.ACKSTAT) // test for ACK condition, if received
                {
                    RestartI2C(); // generate I2C bus restart condition
                    while (SSPCON2bits.RSEN); // wait until re-start condition is over 
                    if (PIR2bits.BCLIF) // test for bus collision
                    {
                        PIR2bits.BCLIF = 0;
                        return ( -1); // return with Bus Collision error 
                    }

                    if (WriteI2C(cADDE + 1))// write 1 byte - R/W bit should be 1
                    {
                        StopI2C();
                        return ( -3); // return with write collision error
                    }

                    if (!SSPCON2bits.ACKSTAT)// test for ACK condition, if received
                    {
                        SSPCON2bits.RCEN = 1; // enable master for 1 byte reception
                        while (SSPCON2bits.RCEN); // check that receive sequence is over
                        NotAckI2C(); // send ACK condition
                        StopI2C(); // send STOP condition
                        if (PIR2bits.BCLIF) // test for bus collision
                        {
                            PIR2bits.BCLIF = 0;
                            return ( -1); // return with Bus Collision error 
                        }
                    } else {
                        StopI2C();
                        return ( -2); // return with Not Ack error
                    }

                } else {
                    StopI2C();
                    return ( -2); // return with Not Ack error
                }
            } else {
                StopI2C();
                return ( -2); // return with Not Ack error condition   
            }
        } else {
            StopI2C();
            return ( -2); // return with Not Ack error
        }
    }
    return ( (signed int) SSPBUF); // return with data
}

signed char EESequentialRead1(unsigned char cControl, unsigned int address, unsigned char *rdptr, unsigned char length) {
    unsigned char cADDE;
    unsigned char cAdressL, cAdressH;
    
    cADDE = (cControl << 1) & 0xFF;
    cADDE = cADDE | 0xA0;

    cAdressL = (address) & 0xFF;
    cAdressH = (address) >> 8;
    IdleI2C(); // ensure module is idle
    StartI2C(); // initiate START condition
    if (PIR2bits.BCLIF) // test for bus collision
    {
        PIR2bits.BCLIF = 0;
        return ( -1); // return with Bus Collision error 
    } else {
        if (WriteI2C(cADDE)) // write 1 byte 
        {
            StopI2C();
            return ( -3); // set error for write collision
        }

        if (!SSPCON2bits.ACKSTAT) // test for ACK condition received
        {
            if (WriteI2C(cAdressH)) // write word address for EEPROM
            {
                StopI2C();
                return ( -3); // set error for write collision
            }

            while (SSPCON2bits.ACKSTAT);
            if (!SSPCON2bits.ACKSTAT) // test for ACK condition received
            {
                if (WriteI2C(cAdressL)) // data byte for EEPROM
                {
                    StopI2C();
                    return ( -3); // set error for write collision
                }

                while (SSPCON2bits.ACKSTAT);
                if (!SSPCON2bits.ACKSTAT) // test for ACK condition received
                {
                    RestartI2C(); // generate I2C bus restart condition
                    if (WriteI2C(cADDE + 1))// WRITE 1 byte - R/W bit should be 1 for read
                    {
                        StopI2C();
                        return ( -3); // set error for write collision
                    }

                    while (SSPCON2bits.ACKSTAT);
                    if (!SSPCON2bits.ACKSTAT)// test for ACK condition received
                    {
                        if (getsI2C(rdptr, length))// read in multiple bytes
                        {
                            return ( -1); // return with Bus Collision error
                        }

                        NotAckI2C(); // send not ACK condition 
                        StopI2C(); // send STOP condition
                        if (PIR2bits.BCLIF) // test for bus collision
                        {
                            PIR2bits.BCLIF = 0;
                            return ( -1); // return with Bus Collision error 
                        }
                    } else {
                        StopI2C();
                        return ( -2); // return with Not Ack error
                    }
                } else {
                    StopI2C();
                    return ( -2); // return with Not Ack error
                }
            } else {
                StopI2C();
                return ( -2); // return with Not Ack error condition   
            }
        } else {
            StopI2C();
            return ( -2); // return with Not Ack error
        }
    }
    return ( 0); // return with no error
}





