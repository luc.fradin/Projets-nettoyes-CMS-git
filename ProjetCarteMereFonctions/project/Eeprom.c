/* 
 * File:   Eeprom.c
 * Author: ADECEF
 *
 * Created on 10 mai 2021, 10:37
 */

#include <stdio.h>
#include <stdlib.h>
#include "h/CMS.h"
#include <i2c.h>

unsigned char LIRE_EEPROM(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char r) {
    unsigned char TMP;
    unsigned char AddH, AddL;


    //MAXERIE A2
    if (ADDE == 6) //GROUPE et bloc notes
        ADDE = 12;
    if (ADDE == 5)
        ADDE = 10;
    if (ADDE == 4)
        ADDE = 8;
    if (ADDE == 3)
        ADDE = 6;
    if (ADDE == 2)
        ADDE = 4;
    if (ADDE == 1)
        ADDE = 2;

    AddL = ADDHL;
    AddH = ADDHL >> 8;



    TMP = ADDE;
    TMP = TMP | 0xA0;


    IdleI2C();
    StartI2C();
    while (SSPCON2bits.SEN);
    WriteI2C(TMP); // 0 sur le bit RW pour indiquer une lecture
    IdleI2C();
    WriteI2C(AddH); // 0 sur le bit RW pour indiquer une lecture
    IdleI2C();
    while (SSPCON2bits.RSEN);
    WriteI2C(AddL); // 0 sur le bit RW pour indiquer une lecture
    IdleI2C();
    RestartI2C();
    while (SSPCON2bits.RSEN);
    //AckI2C();
    TMP = TMP | 0x01;
    WriteI2C(TMP); // 1 sur le bit RW pour indiquer une lecture
    IdleI2C();

    if (r == 1) // pour trame memoire
        getsI2C(tab, 86);
    if (r == 0)
        getsI2C(tab, 108);
    if (r == 3)
        getsI2C(tab, 116);
    if (r == 4)
        getsI2C(tab, 24);
    if (r == 5)
        getsI2C(tab, 120);
    if (r == 6)
        getsI2C(tab, 100);


    NotAckI2C();
    while (SSPCON2bits.ACKEN);



    StopI2C();
    while (SSPCON2bits.PEN);
    return (1);
}

unsigned char ECRIRE_EEPROMBYTE(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char g) {
    unsigned char TMP;
    unsigned char AddH, AddL;




    //MAXERIE A2
    if (ADDE == 6) //GROUPE ET BLOC NOTES
        ADDE = 12;
    if (ADDE == 5)
        ADDE = 10;
    if (ADDE == 4)
        ADDE = 8;
    if (ADDE == 3)
        ADDE = 6;
    if (ADDE == 2)
        ADDE = 4;
    if (ADDE == 1)
        ADDE = 2;




    TMP = ADDE;
    TMP = 0XA0 | TMP;


    AddL = (ADDHL);
    AddH = (ADDHL) >> 8;

    if (g == 1) // POUR FONCTIONNEMENT TRAME MEMOIRE
        tab[85] = FIN_TRAME; //FIN DE TRAME MEMOIRE
    if (g == 3) // POUR FONCTIONNEMENT TRAME MEMOIRE
        tab[115] = FIN_TRAME; //FIN DE TRAME GROUPE
    if (g == 4) // POUR FONCTIONNEMENT TRAME MEMOIRE
        tab[23] = FIN_TRAME; //FIN DE TRAME EXISTANCE
    if (g == 5) // POUR FONCTIONNEMENT TRAME MEMOIRE
        tab[119] = FIN_TRAME; //FIN DE TRAME P�RAM1
    if (g == 6) // POUR FONCTIONNEMENT TRAME MEMOIRE
        tab[99] = FIN_TRAME; //FIN DE TRAME BLOC NOTES



    IdleI2C(); // ensure module is idle
    StartI2C(); // initiate START condition
    while (SSPCON2bits.SEN); // wait until start condition is over
    WriteI2C(TMP); // write 1 byte - R/W bit should be 0
    IdleI2C(); // ensure module is idle
    WriteI2C(AddH); // write HighAdd byte to EEPROM
    IdleI2C(); // ensure module is idle
    WriteI2C(AddL); // write LowAdd byte to EEPROM
    IdleI2C(); // ensure module is idle
    putstringI2C(tab); // pointer to data for page write
    IdleI2C(); // ensure module is idle
    StopI2C(); // send STOP condition
    while (SSPCON2bits.PEN); // wait until stop condition is over
    return 0; // FIN DE BOUCLE a *
}

void pic_eeprom_write(unsigned char Address, unsigned char contenu) {
    //Ecriture en EEPROM


    EEDATA = contenu; //            "
    EECON1bits.EEPGD = 0; //            "
    EECON1bits.CFGS = 0; //            "
    EECON1bits.WREN = 1; //            "

    EEADR = Address; //Configuration de l'�criture
    EECON2 = 0x55; //Sequence d'�criture
    EECON2 = 0xAA; //         "
    _asm
    BSF 0xfa6, 0x01, ACCESS // on met WR � 1pour lancer l'�criture
    _endasm
    while (PIR2bits.EEIF == 0) //Attente de la fin de l'�criture
    {
    }
    PIR2bits.EEIF = 0;
    EECON1bits.WREN = 0; //Remise en �tat du proc�d�

}

char pic_eeprom_read(unsigned char Address) {
    // Lecture en EEPROM
    EEADR = Address;
    EECON1bits.EEPGD = 0; // Configuration de la lecture
    EECON1bits.CFGS = 0;
    EECON1 = EECON1 | 0x01; //rd � 1
    return EEDATA; //Donn�e M�moris�e
}

unsigned char putstringI2C(unsigned char *wrptr) {



    unsigned char x;

    unsigned int PageSize;
    PageSize = 128;


    for (x = 0; x < PageSize; x++) // transmit data until PageSize
    {
        if (SSPCON1bits.SSPM3) // if Master transmitter then execute the following
        { //
            if (putcI2C(*wrptr)) // write 1 byte
            {
                return ( -3); // return with write collision error
            }
            IdleI2C(); // test for idle condition
            if (SSPCON2bits.ACKSTAT) // test received ack bit state
            {
                return ( -2); // bus device responded with  NOT ACK
            } // terminateputstringI2C() function
        } else // else Slave transmitter
        {
            PIR1bits.SSPIF = 0; // reset SSPIF bit


            SSPBUF = *wrptr; // load SSPBUF with new data


            SSPCON1bits.CKP = 1; // release clock line
            while (!PIR1bits.SSPIF); // wait until ninth clock pulse received

            if ((!SSPSTATbits.R_W) && (!SSPSTATbits.BF))// if R/W=0 and BF=0, NOT ACK was received
            {
                return ( -2); // terminateputstringI2C() function
            }
        }
        wrptr++; // increment pointer
    } // continue data writes until null character
    return ( 0);
}
