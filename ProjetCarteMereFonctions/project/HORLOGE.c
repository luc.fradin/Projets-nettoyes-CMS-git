/* 
 * File:   HORLOGE.c
 * Author: ADECEF
 *
 * Created on 10 mai 2021, 11:27
 */

#include <stdio.h>
#include <stdlib.h>
#include <i2c.h>
#include "h/CMS.h"

unsigned char ECRIRE_HORLOGE(unsigned char zone, unsigned char Time) {
    unsigned char TMP;

    //Time=0x00;
    if (zone == 0)
        Time = Time & 0x80; //laisse l'horloge CHbits a 1 secondes tres important si ce bit n'est pas a 1 lhorloge ne marche pas
    if (zone == 1)
        Time = Time & 0x7F; //laisse le bit7 a zero minutes
    if (zone == 2)
        Time = Time & 0x3F; //met a zero les bits 6 et 7 pour le mode 24h
    if (zone == 3)
        Time = Time & 0x07; // laisse ts les bits sauf 0 1 2 a 0
    if (zone == 4)
        Time = Time & 0x3F; // met a 0 les bits 7 et 6 date
    if (zone == 5)
        Time = Time & 0x1F; // met a 0 les bits 7 et 6 et 5 mois
    if (zone == 6)
        Time = Time;
    if (zone == 7)
        Time = Time; //registre control ex: 00010000 =) oscillation de 1hz out

    TMP = envoyerHORLOGE_i2c(0xD0, zone, Time);
    return (TMP);
}

unsigned char envoyerHORLOGE_i2c(unsigned char NumeroH, unsigned char ADDH, unsigned char dataH0) {

    StartI2C();
    WriteI2C(NumeroH); // adresse de l'horloge temps r�el
    WriteI2C(ADDH);
    WriteI2C(dataH0);
    StopI2C();
    delay_ms(11);

}

unsigned char LIRE_HORLOGE(unsigned char zone) {
    unsigned char TMP;
    TMP = litHORLOGE_i2c(0xD1, zone);


    if (zone == 0)
        TMP = TMP & 0x7F; // supprimle le bit 7 seconde
    if (zone == 1)
        TMP = TMP & 0x7F; //laisse le bit7 a zero minutes
    if (zone == 2)
        TMP = TMP & 0x3F; //met a zero les bits 6 et 7 pour le mode 24h
    if (zone == 3)
        TMP = TMP & 0x07; // laisse ts les bits sauf 0 1 2 a 0
    if (zone == 4)
        TMP = TMP & 0x3F; // met a 0 les bits 7 et 6 date
    if (zone == 5)
        TMP = TMP & 0x1F; // met a 0 les bits 7 et 6 et 5 mois
    if (zone == 6)
        TMP = TMP;

    return (TMP);
}

signed char litHORLOGE_i2c(unsigned char NumeroH, unsigned char AddH) {


    char Data;

    StartI2C();
    WriteI2C(NumeroH & 0xFE); // adresse de la DS1307
    WriteI2C(AddH); // suivant le choix utilisateur
    RestartI2C();
    WriteI2C(NumeroH); // on veut lire
    Data = ReadI2C(); // lit la valeur
    StopI2C();
    return (Data);
}

void creer_trame_horloge(unsigned char *cHorloge) {
    
    char cAnnee, cMois, cDate, cJour, cHeure, cMinute, cSeconde;

    cAnnee = LIRE_HORLOGE(6);
    delay_ms(10);
    cMois = LIRE_HORLOGE(5);
    delay_ms(10);
    cDate = LIRE_HORLOGE(4);
    delay_ms(10);
    cJour = LIRE_HORLOGE(3);
    delay_ms(10);
    cHeure = LIRE_HORLOGE(2);
    delay_ms(10);
    cMinute = LIRE_HORLOGE(1);
    delay_ms(10);
    cSeconde = LIRE_HORLOGE(0);


    *cHorloge = ((cHeure & 0xF0) >> 4) + 48; // conversion BCD => ASCII
    *(cHorloge + 1) = (cHeure & 0x0F) + 48;
    *(cHorloge + 2) = ((cMinute & 0xF0) >> 4) + 48;
    *(cHorloge + 3) = (cMinute & 0x0F) + 48;
    *(cHorloge + 4) = ((cSeconde & 0xF0) >> 4) + 48;
    *(cHorloge + 5) = (cSeconde & 0x0F) + 48;
    *(cHorloge + 6) = ((cDate & 0xF0) >> 4) + 48; // conversion BCD => ASCII
    *(cHorloge + 7) = (cDate & 0x0F) + 48;
    *(cHorloge + 8) = ((cMois & 0xF0) >> 4) + 48;
    *(cHorloge + 9) = (cMois & 0x0F) + 48;
    *(cHorloge + 10) = ((cAnnee & 0xF0) >> 4) + 48;
    *(cHorloge + 11) = (cAnnee & 0x0F) + 48;

    if ((*(cHorloge + 10) == '0')&&(*(cHorloge + 11) == '0')) {

        *(cHorloge) = '0'; // conversion BCD => ASCII
        *(cHorloge + 1) = '7';
        *(cHorloge + 2) = '1';
        *(cHorloge + 3) = '5';
        *(cHorloge + 4) = '0';
        *(cHorloge + 5) = '0';
        *(cHorloge + 6) = '2';
        *(cHorloge + 7) = '9';
        *(cHorloge + 8) = '1';
        *(cHorloge + 9) = '1';
        *(cHorloge + 10) = '0';
        *(cHorloge + 11) = '2';

    }

}

void entrer_trame_horloge(unsigned char *cHorloge) {
    
    char cHeure, cMinute, cSeconde, cDate, cMois, cAnnee;
    
    cHeure = (*(cHorloge) - 48) << 4;
    cHeure = cHeure + (*(cHorloge + 1) - 48);
    cMinute = (*(cHorloge + 2) - 48) << 4;
    cMinute = cMinute + (*(cHorloge + 3) - 48);
    cSeconde = (*(cHorloge + 4) - 48) << 4;
    cSeconde = cSeconde + (*(cHorloge + 5) - 48);
    cDate = (*(cHorloge + 6) - 48) << 4;
    cDate = cDate + (*(cHorloge + 7) - 48);
    cMois = (*(cHorloge + 8) - 48) << 4;
    cMois = cMois + (*(cHorloge + 9) - 48);
    cAnnee = (*(cHorloge + 10) - 48) << 4;
    cAnnee = cAnnee + (*(cHorloge + 11) - 48);

    ECRIRE_HORLOGE(2, cHeure);
    ECRIRE_HORLOGE(1, cMinute);
    ECRIRE_HORLOGE(0, cSeconde);
    ECRIRE_HORLOGE(4, cDate);
    ECRIRE_HORLOGE(5, cMois);
    ECRIRE_HORLOGE(6, cAnnee);
}

void lire_horloge(unsigned char *cHorloge) {

    *cHorloge = '1';
    *(cHorloge + 1) = '1';
    *(cHorloge + 2) = '1';
    *(cHorloge + 3) = '1';
    *(cHorloge + 4) = '1';
    *(cHorloge + 5) = '1';
    *(cHorloge + 6) = '1';
    *(cHorloge + 7) = '1';
    *(cHorloge + 8) = '1';
    *(cHorloge + 9) = '1';
    *(cHorloge + 10) = '1';
    *(cHorloge + 11) = '1';
}

void reglerhorlogeautres(unsigned char *cHorloge, float fV1, float fV2, float fV3, float fV4, float fV5, float fVbat, char cR1NoRep, char cR2NoRep, char cR3NoRep, char cR4NoRep, char cR5NoRep) {

    MESURE_DES_TENSIONS(&fV1, &fV2, &fV3, &fV4, &fV5, &fVbat);

    if ((fV1 > 2)&&(cR1NoRep < 12)) {
        creer_trame_horloge(cHorloge); //LIRE EEPPROM VERIFICATION
        ENVOIE_DES_TRAMES(1, 305, cHorloge);
    }
    if ((fV2 > 2)&&(cR2NoRep < 12)) // SI CARTE PAS PROBLEME
    {
        creer_trame_horloge(cHorloge);
        ENVOIE_DES_TRAMES(2, 305, cHorloge);
    }
    if ((fV3 > 2)&&(cR3NoRep < 12)) {
        creer_trame_horloge(cHorloge);
        ENVOIE_DES_TRAMES(3, 305, cHorloge);
    }
    if ((fV4 > 2)&&(cR4NoRep < 12)) {
        creer_trame_horloge(cHorloge);
        ENVOIE_DES_TRAMES(4, 305, cHorloge);
    }
    if ((fV5 > 2)&&(cR5NoRep < 12)) {
        creer_trame_horloge(cHorloge);
        ENVOIE_DES_TRAMES(5, 305, cHorloge);
    }


}