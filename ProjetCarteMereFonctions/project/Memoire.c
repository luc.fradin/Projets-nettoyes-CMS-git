/* 
 * File:   Tension.c
 * Author: ADECEF
 *
 * Created on 10 mai 2021, 12:00
 */

#include <stdio.h>
#include <stdlib.h>
#include "h/CMS.h"

void GROUPE_EN_MEMOIRE(unsigned char *tab, unsigned char *cTrameSortie, char lui) {

    int ici;
    if (lui == 0)
        ici = 0;
    if (lui == 1)
        ici = 128;
    if (lui == 2)
        ici = 256;

    ECRIRE_EEPROMBYTE(6, ici, tab, 3);
    delay_ms(100);
    LIRE_EEPROM(6, ici, cTrameSortie, 3);
    delay_ms(100);
}

void METTRE_EN_MEMOIRE_LISTE(unsigned char *tab, char t) {
    unsigned int ADD;

    ADD = 100 * (tab[99] - 48);
    ADD = ADD + 10 * (tab[100] - 48);
    ADD = ADD + (tab[101] - 48);


    ADD = 128 * ADD;

    if (t == 0)
        ECRIRE_EEPROMBYTE(7, ADD, tab, 0); //LISTE COMPLETE
    if (t == 1)
        ECRIRE_EEPROMBYTE(5, ADD, tab, 0); //LISTE AVEC CHANGEMENT IMPORTANT
}

unsigned int calcul_addmemoire(unsigned char *tab) {
    unsigned int t;
    unsigned int i;
    unsigned int r;



    t = 0;
    i = tab[3];
    i = i - 48;
    if (i > 9) {
        t = 0;
        goto nongood; //PAS NORMAL LE NUMERO DE VOIE
    }
    t = t + 1 * i;
    i = tab[2];
    i = i - 48;
    if (i > 9) {
        t = 0;
        goto nongood; //PAS NORMAL LE NUMERO DE VOIE
    }
    t = t + 10 * i;
    i = tab[1];
    i = i - 48;
    if (i > 9) {
        t = 0;
        goto nongood; //PAS NORMAL LE NUMERO DE VOIE
    }
    t = t + 100 * i;


    r = 2176 * (t);
    if (t > 29)
        r = 2176 * (t - 30);
    if (t > 59)
        r = 2176 * (t - 60);
    if (t > 89)
        r = 2176 * (t - 90);


    t = 0;
    i = tab[5] - 48;
    t = 1 * i;
    i = tab[4] - 48;
    t = t + 10 * i;
    i = t;

    t = r + 128 * i;

nongood: // nongood alors on sav a la memoire x en 0 la ou il n'y a rien

    delay_ms(10);



    return t;


}

unsigned char choix_memoire(unsigned char *tab) {
    unsigned int i;
    unsigned int t;
    unsigned char r;

    t = 0;
    i = tab[3] - 48;
    if (i > 9) {
        r = 0;
        goto nongoodMEM; //PAS NORMAL LE NUMERO DE VOIE
    }
    t = t + 1 * i;
    i = tab[2] - 48;
    if (i > 9) {
        r = 0;
        goto nongoodMEM; //PAS NORMAL LE NUMERO DE VOIE
    }
    t = t + 10 * i;
    i = tab[1] - 48;
    if (i > 9) {
        r = 0;
        goto nongoodMEM; //PAS NORMAL LE NUMERO DE VOIE
    }
    t = t + 100 * i;

    r = 0;
    if (t > 29)
        r = 1;
    if (t > 59)
        r = 2;
    if (t > 89)
        r = 3;


nongoodMEM: //SI PAS NORAMLE ON ECRIT DANS LA MEMOIRE 0


    delay_ms(10);
    return r;

}