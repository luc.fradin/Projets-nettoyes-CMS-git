/* 
 * File:   CRC.c
 * Author: ADECEF
 *
 * Created on 6 mai 2021, 09:05
 */

#include <stdio.h>
#include <stdlib.h>
#include "h/CMS.h"

#define CRC_POLY 0x01021 //0x01021
#define CRC_START 0xFFFF

void calcul_CRC(unsigned char *cCRC, unsigned char *cTrameSortie) {

    int iBcl;

    for(iBcl = 0; iBcl < 5; iBcl++){
        *(cCRC + iBcl) = '2';
        *(cTrameSortie + iBcl) = *(cCRC + iBcl);
    }
    *(cTrameSortie + 5) = FIN_TRAME;

}

void controle_CRC(unsigned char *cCRC, unsigned char *cTrameEntree) {
    
    int iBcl;

    for(iBcl = 0; iBcl < 5; iBcl++){
        *(cCRC + iBcl) = *(cTrameEntree + iBcl);
    }

}

char CRC_verif(unsigned char *cCRC, unsigned char *cTrameEntree, int iPointeurEntreeSortie) {

    char CRCcal[5];
    unsigned int fft;
    char cEtatCRC;
    //ON RECHERCHE LE CRC
    
    controle_CRC(cCRC, cTrameEntree);

    fft = CRC16(cTrameEntree, iPointeurEntreeSortie-1);

    IntToChar5(fft, CRCcal, 5);

    if ((*(cCRC) == CRCcal[0]) && (*(cCRC + 1) == CRCcal[1]) && (*(cCRC + 2) == CRCcal[2]) && (*(cCRC + 3) == CRCcal[3]) && (*(cCRC + 4) == CRCcal[4]))
        cEtatCRC = 0xFF; //CRC CORRECT
    else
        cEtatCRC = 0x00; //CRC INCORRECT
    
    return(cEtatCRC);
}

unsigned int CRC16(unsigned char far *txt, unsigned char lg_txt) {
    unsigned char ii, nn;
    unsigned int crc;

    unsigned char far *p;
    crc = CRC_START;
    p = txt;

    //essai[2]=*p;
    ii = 0;
    nn = 0;
    //for( ii=0; ii<lg_txt ; ii++, p++)
    do {
        crc ^= (unsigned int) *p;
        //for(; nn<8; nn++)
        do {
            if (crc & 0x8000) {
                crc <<= 1;
                //UTIL;
                crc ^= CRC_POLY;
            } else {
                crc <<= 1;
            }
            nn++;
        } while (nn < 8);
        ii = ii + 1;
        p++;
        if (nn == 8) {
            nn = 0;
        }
    } while (ii < lg_txt);
    ii = 0;

    return crc;
}