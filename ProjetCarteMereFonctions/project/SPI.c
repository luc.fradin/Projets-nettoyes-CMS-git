/* 
 * File:   SPI.c
 * Author: ADECEF
 *
 * Created on 10 mai 2021, 10:32
 */

#include <stdio.h>
#include <stdlib.h>
#include "h/CMS.h"


unsigned char SPI_WriteByte(unsigned char val) {
    SSP2BUF = val; // Envoie du caract�re
    while (!PIR3bits.SSP2IF); // Attendre jusque envoi accompli

    PIR3bits.SSP2IF = 0; //clear send finish flag.
    return SSP2BUF; // Retourne le caract�re re�u
}

void init_spi(void) {

    TRISDbits.TRISD7 = 0; // RD7/SS - Output (Chip Select)

    TRISDbits.TRISD4 = 0; // RD4/SDO - Output (Serial Data Out)
    TRISDbits.TRISD5 = 1; // RD5/SDI - Input (Serial Data In)
    TRISDbits.TRISD6 = 0; // RD6/SCK - Output (Clock)



    SSP2STATbits.SMP = 0; // input is valid in the middle of clock
    SSP2STATbits.CKE = 0; // 0  for rising edge is data capture


    SSP2CON1bits.CKP = 0; // high value is passive state

    SSP2CON1bits.SSPM3 = 0; // speed f/64(312kHz), Master
    SSP2CON1bits.SSPM2 = 0;
    SSP2CON1bits.SSPM1 = 1;
    SSP2CON1bits.SSPM0 = 0;

    SSP2CON1bits.SSPEN = 1; // enable SPI

    PORTDbits.RD7 = 1; // Disable Chip Select
}

void spi_low() {
    SSP2CON1 = 0x22; //SPI clk use the system  375Khz (32)
}