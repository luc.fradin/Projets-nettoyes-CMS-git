/* 
 * File:   Trames.c
 * Author: ADECEF
 *
 * Created on 6 mai 2021, 08:41
 */

#include <stdio.h>
#include <stdlib.h>
#include "h/CMS.h"

//#include <p18F8723.h> //biblioth�que du Microcontroleur

void resetTO(unsigned char *cTrameReset) {

    int iBcl;

    for (iBcl = 0; iBcl < 125; iBcl++) {
        *(cTrameReset + iBcl) = 'x';
    }
}

int recuperer_trame_memoire(char w, char mode, unsigned char *cTrameEntree, unsigned char *cTrameSortie, unsigned char *cVoieCod, unsigned char *cHorloge, char *cEtatZizAuto, char *cEtat){ //RECUPERE TRAME MET DANS LES VARIABLES PUIS ECRIT DANS L'EPROOM

    int t;
    int u;
    char cNumCarteRI;
    int iPointeurSortieEntree;
    
    u = 8; // pr un TRAMOUT MEMOIRE

    if (*(cTrameEntree + 2) == '1')
        cNumCarteRI = 1; //carte R1
    if (*(cTrameEntree + 2) == '2')
        cNumCarteRI = 2; //carte R2
    if (*(cTrameEntree + 2) == '3')
        cNumCarteRI = 3; //carte R3
    if (*(cTrameEntree + 2) == '4')
        cNumCarteRI = 4; //carte R4
    if (*(cTrameEntree + 2) == '5')
        cNumCarteRI = 5; //carte R5
    //// MULTI

    
    
    
    
    *cTrameSortie = DEBUT_TRAME;

    *(cVoieCod + u - 8) = *(cTrameEntree + u);
    u = u + 1;
    *(cVoieCod + u - 8) = *(cTrameEntree + u);
    u = u + 1;
    *(cVoieCod + u - 8) = *(cTrameEntree + u);
    u = u + 1;
    *(cVoieCod + u - 8) = *(cTrameEntree + u);
    u = u + 1;
    *(cVoieCod + u - 8) = *(cTrameEntree + u);

    if (mode == 'r') //SI on travaille en mode relatif
    {
        // MULTI
        *(cVoieCod + 1) = *(cVoieCod + 1) + 2 * (cNumCarteRI - 1); // EN ASCII 0 devient 2
        if (*(cVoieCod + 1) == 58) // si la voie 20 et 5ieme carte donc 100
        {
            *(cVoieCod + 1) = '0';
            *cVoieCod = '1';
        } else {
        }
    }



    u = 8;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;


    //ecrire dans I2C
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;


    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;

    t = 0;
    for (t = 0; t < 13; t++) {
        *(cTrameSortie + u - 7) = *(cTrameEntree + u);
        u = u + 1;

    }

    t = 0;
    for (t = 0; t < 18; t++) {
        *(cTrameSortie + u - 7) = *(cTrameEntree + u);
        u = u + 1;
    }


    *(cTrameSortie + u - 7) = *(cVoieCod);
    u = u + 1;

    *(cTrameSortie + u - 7) = *(cVoieCod + 1);
    u = u + 1;

    *(cTrameSortie + u - 7) = *(cVoieCod + 2);
    u = u + 1;


    if (*(cTrameEntree + u) == '?') //si il y a des ? sur la date on met la derniere heure contenue dans la variable horloge lors de cration par exemple avec l'automate
    {
        *(cTrameSortie + u - 7) = *(cHorloge + 6);
        u = u + 1;
        *(cTrameSortie + u - 7) = *(cHorloge + 7);
        u = u + 1;
        *(cTrameSortie + u - 7) = *(cHorloge + 8);
        u = u + 1;
        *(cTrameSortie + u - 7) = *(cHorloge + 9);
        u = u + 1;
        *(cTrameSortie + u - 7) = *(cHorloge + 0);
        u = u + 1;
        *(cTrameSortie + u - 7) = *(cHorloge + 1);
        u = u + 1;
        *(cTrameSortie + u - 7) = *(cHorloge + 2);
        u = u + 1;
        *(cTrameSortie + u - 7) = *(cHorloge + 3);
        u = u + 1;

    } else {

        *(cTrameSortie + u - 7) = *(cTrameEntree + u);
        u = u + 1;
        *(cTrameSortie + u - 7) = *(cTrameEntree + u);
        u = u + 1;
        *(cTrameSortie + u - 7) = *(cTrameEntree + u);
        u = u + 1;
        *(cTrameSortie + u - 7) = *(cTrameEntree + u);
        u = u + 1;
        *(cTrameSortie + u - 7) = *(cTrameEntree + u);
        u = u + 1;
        *(cTrameSortie + u - 7) = *(cTrameEntree + u);
        u = u + 1;
        *(cTrameSortie + u - 7) = *(cTrameEntree + u);
        u = u + 1;
        *(cTrameSortie + u - 7) = *(cTrameEntree + u);
        u = u + 1;
    }


    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;

    if (*(cTrameEntree + u) == 'X') //SI MODIF OU CREATION SUR L'AUTOMATE
    {
        if (*cEtatZizAuto == 'F') //SI nouveau capteur AUTO alors on met etat S/R en mode ZIZ avant CREATION
        {

            *cEtat = '5';
            *(cTrameSortie + u - 7) = '5';
            u = u + 1;

        } else {
            *cEtat = *cEtatZizAuto;
            *(cTrameSortie + u - 7) = *cEtatZizAuto;
            u = u + 1;

        }

    } else //fonctionnement normal
    {
        *cEtat = *(cTrameEntree + u);
        *(cTrameSortie + u - 7) = *(cTrameEntree + u);
        u = u + 1;



    }

    *(cEtat + 1) = *(cTrameEntree + u);
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    *(cTrameSortie + u - 7) = *(cTrameEntree + u);
    u = u + 1;
    
    *(cTrameSortie + u - 7) = FIN_TRAME;

    //ecriture ou pas
    if (w == 1)
        ECRIRE_EEPROMBYTE(choix_memoire(cTrameSortie), calcul_addmemoire(cTrameSortie), cTrameSortie, 1); //ECRITURE EN EEPROM

    if (*(cVoieCod + 2) == '9') //TEST
        iPointeurSortieEntree = u;

    LIRE_EEPROM(choix_memoire(cTrameSortie), calcul_addmemoire(cTrameSortie), cTrameEntree, 1); //LIRE DANS EEPROM
    iPointeurSortieEntree = u;

    return(iPointeurSortieEntree);

}

int construire_trame_memoire(unsigned char *cTrameEntree, unsigned char *cTrameSortie, unsigned char *cVoieCod, unsigned char *cDemandeInfoCapteur, char *cEtat, char *cTmp){ // ICI ON RECUPERE LE CONTENU DE LA MEMOIRE ET ON LE MET DANS UNE TRAME COM ET DANS LES VARIABLES

    int t;
    int u;
    int iPointeurSortieEntree;


    unsigned char TMPTAB[6]; //TEMPORAIRE

    for(u = 0; u < 4; u++){
        *(cVoieCod + u) = *(cDemandeInfoCapteur + u);
    }
    
    u = 8;
    
    
    *(cTrameSortie + u) = *(cVoieCod + u - 8);
    u = u + 1;

    *(cTrameSortie + u) = *(cVoieCod + u - 8);
    u = u + 1;

    *(cTrameSortie + u) = *(cVoieCod + u - 8);
    u = u + 1;

    *(cTrameSortie + u) = *(cVoieCod + u - 8);
    u = u + 1;

    *(cTrameSortie + u) = *(cVoieCod + u - 8);
    u = u + 1;


    // faire fonction recherche infos I2C

    TMPTAB[0] = DEBUT_TRAME;
    
    TMPTAB[1] = *(cVoieCod);
    TMPTAB[2] = *(cVoieCod + 1);
    TMPTAB[3] = *(cVoieCod + 2);
    TMPTAB[4] = *(cVoieCod + 3);
    TMPTAB[5] = *(cVoieCod + 4);
    *cTmp = LIRE_EEPROM(choix_memoire(TMPTAB), calcul_addmemoire(TMPTAB), cTrameEntree, 1); //LIRE DANS EEPROM ET METTRE DANS TRAMEIN


    *(cTrameSortie + u) = *(cTrameEntree + u - 7); // ON RECUPERE LA TRAME MEMOIRE
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;


    t = 0;
    for (t = 0; t < 13; t++) {
        *(cTrameSortie + u) = *(cTrameEntree + u - 7);
        u = u + 1;

    }

    t = 0;
    for (t = 0; t < 18; t++) {
        *(cTrameSortie + u) = *(cTrameEntree + u - 7);
        u = u + 1;
    }


    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;



    *cEtat = *(cTrameEntree + u - 7);
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cEtat + 1) = *(cTrameEntree + u - 7);
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;

    iPointeurSortieEntree = u;
    
    return(iPointeurSortieEntree);
}

void INTERROGER_TOUT_LES_CAPTEURS(char t, unsigned char *cTmpTab, unsigned char *cDemandeInfoCapteur) {
    unsigned int i, y, ADD;
    unsigned char R;
    for (i = 0; i < 13000; i = i + 128) {
        if (t == 0)
            LIRE_EEPROM(7, i, cTmpTab, 0); //LISTE COMPLETE
        if (t == 1)
            LIRE_EEPROM(5, i, cTmpTab, 0); //LISTE AVEC GRAND CHANGEMENT
        delay_ms(6); //IMPORTANT
        if (*(cTmpTab + 98) == 'F')
            i = 15000; //FIN D'INTERROGATION


        for (y = 0; y < 17; y++) {
            *(cDemandeInfoCapteur) = *(cTmpTab + 13 + 5 * y);
            *(cDemandeInfoCapteur + 1) = *(cTmpTab + 14 + 5 * y);
            *(cDemandeInfoCapteur + 2) = *(cTmpTab + 15 + 5 * y);
            *(cDemandeInfoCapteur + 3) = *(cTmpTab + 16 + 5 * y);
            *(cDemandeInfoCapteur + 4) = *(cTmpTab + 17 + 5 * y);

            if (*(cDemandeInfoCapteur) == 'x')
                goto finliste;

            ADD = 100 * (*(cDemandeInfoCapteur) - 48);
            ADD = ADD + 10 * (*(cDemandeInfoCapteur + 1) - 48); // ON REGARDE LA VOIE
            ADD = ADD + (*(cDemandeInfoCapteur + 2) - 48);


            R = 5;
            if (ADD < 79)
                R = 4;


            if (ADD < 59)
                R = 3;

            if (ADD < 39)
                R = 2;

            if (ADD < 19)
                R = 1;

            ENVOIE_DES_TRAMES(R, 276, cDemandeInfoCapteur);
        }
    }

finliste:

    delay_ms(100);

}

void SAV_EXISTENCE(unsigned char *tt, unsigned char *cTmpTab) {

    unsigned int l;
    char i;
    char cNumCarteRI;

    l = tt[5] - 48;
    l = l * 10;
    l = l + tt[6] - 48;
    i = l;

    l = 128 * l + 30000; //04/07/2017

    *cTmpTab = DEBUT_TRAME;
    *(cTmpTab + 1) = i;
    for (i = 2; i < 34; i++) {
        *(cTmpTab + i) = tt[5 + i];
    }

    //MULTI #.vFFFFFFFFFFFFFFFFF0vE99999*  .=0x01 en hexa
    if (*(cTmpTab + 20) == '0')
        cNumCarteRI = 1;
    if (*(cTmpTab + 20) == '1')
        cNumCarteRI = 2;
    if (*(cTmpTab + 20) == '2')
        cNumCarteRI = 3;
    if (*(cTmpTab + 20) == '3')
        cNumCarteRI = 4;
    if (*(cTmpTab + 20) == '4')
        cNumCarteRI = 5;

    l = l + 2560 * (cNumCarteRI - 1);

    *(cTmpTab + 1) = 20 * (cNumCarteRI - 1) + *(cTmpTab + 1);

    ECRIRE_EEPROMBYTE(3, l, cTmpTab, 4);
}

int VALIDER_CAPTEUR_CM(unsigned char *cTrameEntree, unsigned char *cTrameSortie, unsigned char *cVoieCod, unsigned char *cHorloge, char *cEtatZizAuto, unsigned char *cTmpTab, char *cEtat, char cNombreCapteursCable) {
    char t;
    int iPointeurSortieEntree;

    for (t = 0; t < 125; t++) // ON RECOPIE TRAMEOUT
        *(cTmpTab + t) = *(cTrameSortie + t);


    // ON COPIE DABORT LE COMMENTAIRES CABLES EN CODAGE 0
    *(cTmpTab + 11)= '0';
    *(cTmpTab + 12) = '0';
    *(cTmpTab + 69) = '0';
    *(cTmpTab + 70) = '0';
    *(cTmpTab + 71) = '0';
    *(cTmpTab + 72) = '1';
    intervertirOUT_IN(cTrameEntree, cTrameSortie);

    iPointeurSortieEntree = recuperer_trame_memoire(1, 'a', cTrameEntree, cTrameSortie, cVoieCod, cHorloge, cEtatZizAuto, cEtat); //STOCKAGE EN MMOIRE DE L'INFORMATION DU CAPTEUR ici juste pour commentaire cable



    for (t = 0; t < 125; t++) //ON RECOPIE DANS TRAMEOUT
        *(cTrameSortie + t) = *(cTmpTab + t);
    intervertirOUT_IN(cTrameEntree, cTrameSortie);
    iPointeurSortieEntree = recuperer_trame_memoire(1, 'a', cTrameEntree, cTrameSortie, cVoieCod, cHorloge, cEtatZizAuto, cEtat); //STOCKAGE EN MMOIRE DE L'INFORMATION DU CAPTEUR
    MODIFICATION_TABLE_DEXISTENCE(cTrameSortie, 0, 0, cTmpTab, cNombreCapteursCable); //MODIFICATION DE LA TABLE
    delay_ms(10);
    LIRE_EEPROM(choix_memoire(cTrameSortie), calcul_addmemoire(cTrameSortie), cTrameEntree, 1); //LIRE DANS EEPROM LIRE LE CAPTEUR 0 C ICI QUON IRA LIRE LE COMMENTAIRE CABLE
    delay_ms(10);
}

void intervertirOUT_IN(unsigned char *cTrameEntree, unsigned char *cTrameSortie) {

    int h;

    for (h = 0; h < 125; h++) {
        *(cTrameEntree + h) = *(cTrameSortie + h);
    }

}

void RECHERCHE_DONNEES_GROUPE(unsigned char ici, unsigned char *cTmpTab) {

    int lui;
    if (ici == 0)
        lui = 0;
    if (ici == 1)
        lui = 128;
    if (ici == 2)
        lui = 256;



    LIRE_EEPROM(6, lui, cTmpTab, 3);


}

void PREPARATION_PAGE_AUTO(unsigned char *cPageTab, unsigned char *cTrameEntree, unsigned char *cTrameSortie, unsigned char *cTmpTab) {


    unsigned char y, t;
    char i;
    char tati[6];
    unsigned int l;

    for(t = 0; t < 5; t++){
        tati[t] = *(cPageTab + t);
    }

    resetTO(cTrameSortie);
    l = 100 * (tati[1] - 48) + 10 * (tati[2] - 48) + 1 * (tati[3] - 48);
    l = 128 * l + 30000;
    LIRE_EEPROM(3, l, cTmpTab, 4); //LIRE DANS EEPROM lexistance du capteur sur la voie
    LIRE_EEPROM(choix_memoire(cPageTab), calcul_addmemoire(cPageTab), cTrameEntree, 1); //LIRE DANS EEPROM
    *(cTrameSortie) = DEBUT_TRAME;
    *(cTrameSortie + 8) = *(cTrameEntree + 1);
    *(cTrameSortie + 9) = *(cTrameEntree + 2);
    *(cTrameSortie + 10) = *(cTrameEntree + 3);
    for (t = 11; t < 29; t++) {
        *(cTrameSortie + t) = *(cTrameEntree + t + 21);
    }


    //DEBIT
    *(cTrameSortie + 29) = *(cTrameEntree + 6); //TYPE
    *(cTrameSortie + 30) = *(cTrameEntree + 4); //CODAGE 0
    *(cTrameSortie + 31) = *(cTrameEntree + 5); //CODAGE 0
    *(cTrameSortie + 32) = *(cTrameEntree + 75); //ETAT
    *(cTrameSortie + 33) = *(cTrameEntree + 76); //ETAT
    if (*(cTmpTab + 3) == 'F') {
        *(cTrameSortie + 32) = 'F'; //PAS DE CAPTEURS
        *(cTrameSortie + 33) = 'F'; //PAS DE CAPTEURS

    }
    if (*(cTmpTab + 2) == 'i') //INCIDENT PAT
    {
        *(cTrameSortie + 32) = '9'; //INCIDENT
        *(cTrameSortie + 33) = '9';
    }
    if (*(cTmpTab + 2) == 'c') //CC PAT
    {
        *(cTrameSortie + 32) = '8'; //CC
        *(cTrameSortie + 33) = '8';
    }
    if (*(cTmpTab + 2) == 'h') //FUS PAT
    {
        *(cTrameSortie + 32) = '7'; //FUS
        *(cTrameSortie + 33) = '7';
    }





    //CAPTEURS DE PRESSION
    for (t = 1; t < 17; t++) {
        //1 ier capteur COD1

        *(cPageTab + 4) = '0';
        if (t >= 10)
            *(cPageTab + 4) = '1';

        if (t < 10)
            *(cPageTab + 5) = t + 48;
        if (t == 10)
            *(cPageTab + 5) = '0';
        if (t >= 10)
            *(cPageTab + 5) = (t - 10) + 48;

        LIRE_EEPROM(choix_memoire(cPageTab), calcul_addmemoire(cPageTab), cTrameEntree, 1); //LIRE DANS EEPROM
        *(cTrameSortie + 34 + 4 * (t - 1)) = *(cTrameEntree + 4); //CODAGE 1
        *(cTrameSortie + 35 + 4 * (t - 1)) = *(cTrameEntree + 5); //CODAGE 1
        *(cTrameSortie + 36 + 4 * (t - 1)) = *(cTrameEntree + 75); //ETAT 1
        *(cTrameSortie + 37 + 4 * (t - 1)) = *(cTrameEntree + 76); //ETAT 1





        if (*(cTmpTab + 3 + t) == 'F') {
            *(cTrameSortie + 36 + 4 * (t - 1)) = 'F'; //PAS DE CAPTEURS
            *(cTrameSortie + 37 + 4 * (t - 1)) = 'F'; //PAS DE CAPTEURS
        }

        if ((*(cTmpTab + 2 + t) == 'r')&&(t == 1)) //RESISTIF et codage 1
        {
            *(cTrameSortie + 36 + 4 * (t - 1)) = *(cTrameEntree + 75); //ETAT 1
            *(cTrameSortie + 37 + 4 * (t - 1)) = *(cTrameEntree + 76); //ETAT 1
        }


        if (*(cTmpTab + 2) == 'i') //INCIDENT PAT
        {
            *(cTrameSortie + 36 + 4 * (t - 1)) = '9';
            *(cTrameSortie + 37 + 4 * (t - 1)) = '9'; //PAS DE CAPTEURS
        }
        if (*(cTmpTab + 2) == 'c') //CC PAT
        {
            *(cTrameSortie + 36 + 4 * (t - 1)) = '8';
            *(cTrameSortie + 37 + 4 * (t - 1)) = '8'; //PAS DE CAPTEURS
        }
        if (*(cTmpTab + 2) == 'h') //FUS PAT
        {
            *(cTrameSortie + 36 + 4 * (t - 1)) = '7';
            *(cTrameSortie + 37 + 4 * (t - 1)) = '7'; //PAS DE CAPTEURS
        }

        if ((*(cTmpTab + 2) == 'r')&&(t != 1)) //CARTE RESISTIVE
        {
            *(cTrameSortie + 36 + 4 * (t - 1)) = 'F';
            *(cTrameSortie + 37 + 4 * (t - 1)) = 'F'; //PAS DE CAPTEURS car resistif uniquement en 01
        }
    }
}

int SDR(unsigned char *cTrameEntree, unsigned char *cTrameSortie, unsigned char *cVoieCod, unsigned char *cInstruction, char *cBitSD, unsigned char *cHorloge) {
    char gigi;
    int iPointeur_out_in;
    float fV1, fV2, fV3, fV4, fV5, fVbat;


rec:
    MESURE_DES_TENSIONS(&fV1, &fV2, &fV3, &fV4, &fV5, &fVbat);
    TEMPO(3);
    if (fV1 > 2) {
        ENVOIE_DES_TRAMES(1, 301, cInstruction); //envoyer ordre formatage carte RI1
        TEMPO(3);
    }
    if (fV2 > 2) {
        ENVOIE_DES_TRAMES(2, 301, cInstruction); //envoyer ordre formatage carte RI1
        TEMPO(3);
    }
    if (fV3 > 2) {
        ENVOIE_DES_TRAMES(3, 301, cInstruction); //envoyer ordre formatage carte RI1
        TEMPO(3);
    }
    if (fV4 > 2) {
        ENVOIE_DES_TRAMES(4, 301, cInstruction); //envoyer ordre formatage carte RI1
        TEMPO(3);
    }
    if (fV5 > 2) {
        ENVOIE_DES_TRAMES(5, 301, cInstruction); //envoyer ordre formatage carte RI1
        TEMPO(3);
    }

    //ON RECOIT ERR00 DE CHAQUE CARTE MAIS ON N'EN PREND PAS COMPTE

    *(cTrameEntree + 1) = '0';
    do {
        DX10 = !DX10;
        DX20 = !DX20;

        if (*(cTrameEntree + 1) == '$')
            goto finwhi;
        delay_ms(100);
        envoyer_TRAME_PICSD(0xA1, cTrameEntree, cTrameSortie, cHorloge); //START  RECEVOIR POUR PROGRAMMER A1
        delay_ms(100);
        envoyer_TRAME_PICSD(0x00, cTrameEntree, cTrameSortie, cHorloge); //RECEP faire Nx jusqua $$$
        if (*(cTrameEntree + 4) == 50) {
            delay_ms(10);
            delay_ms(10);
        }

finwhi:
        delay_ms(10);


        //CREER LE CAPTEUR ! DANS TRAMEIN //si CRC=F22222 (TRAMEIN[86]=F ou E) debitmetre pas present sinon il existe

        if (*(cTrameEntree + 86) == 'F')
            *cBitSD = 0x00;
        if (*(cTrameEntree + 86) == 'E')
            *cBitSD = 0xFF;


        for (gigi = 0; gigi < 93; gigi++) //-#021..... ce que lon recoit
        {

            *(cTrameEntree + gigi) = *(cTrameEntree + gigi + 1);

        }




        iPointeur_out_in = TRANSFORMER_TRAME_MEM_EN_COM(cTrameEntree, cTrameSortie, cVoieCod);

        *(cTrameSortie) = DEBUT_TRAME;
        *(cTrameSortie + 1) = 'I';
        *(cTrameSortie + 2) = 'O';
        *(cTrameSortie + 3) = 'D'; //DD cad dire carte memoire
        *(cTrameSortie + 4) = 'D';
        *(cTrameSortie + 5) = 'Z';
        *(cTrameSortie + 6) = 'C';
        *(cTrameSortie + 7) = 'R';


        for (gigi = 0; gigi < 93; gigi++) //-#021..... ce que lon recoit
        {
            *(cTrameEntree + gigi) = *(cTrameSortie + gigi);
        }


        TEMPO(1);

        if ((*(cTrameEntree + 9) == '0') || (*(cTrameEntree + 9) == '1')) //001 - 019
        {
            if (fV1 > 2)
                RECUPERATION_DES_TRAMES(cTrameSortie); //ENVOIE VIRTUEL DE COMMANDE DE IO (EN REALITE CARTE SD)
        }

        if ((*(cTrameEntree + 9) == '2') || (*(cTrameEntree + 9) == '3')) //021 - 039
        {
            if (fV2 > 2)
                RECUPERATION_DES_TRAMES(cTrameSortie); //ENVOIE VIRTUEL DE COMMANDE DE IO (EN REALITE CARTE SD)
        }

        if ((*(cTrameEntree + 9) == '4') || (*(cTrameEntree + 9) == '5')) //041-059
        {
            if (fV3 > 2)
                RECUPERATION_DES_TRAMES(cTrameSortie); //ENVOIE VIRTUEL DE COMMANDE DE IO (EN REALITE CARTE SD)
        }

        if ((*(cTrameEntree + 9) == '6') || (*(cTrameEntree + 9) == '7')) //061-079
        {
            if (fV4 > 2)
                RECUPERATION_DES_TRAMES(cTrameSortie); //ENVOIE VIRTUEL DE COMMANDE DE IO (EN REALITE CARTE SD)
        }

        if ((*(cTrameEntree + 9) == '8') || (*(cTrameEntree + 9) == '9')) //081-099
        {
            if (fV5 > 2)
                RECUPERATION_DES_TRAMES(cTrameSortie); //ENVOIE VIRTUEL DE COMMANDE DE IO (EN REALITE CARTE SD)
        }




        if ((*(cTrameEntree + 9) == '2') && (*(cTrameEntree + 10) == '0')) //20
        {
            if (fV1 > 2)
                RECUPERATION_DES_TRAMES(cTrameSortie); //ENVOIE VIRTUEL DE COMMANDE DE IO (EN REALITE CARTE SD)
        }

        if ((*(cTrameEntree + 9) == '3') && (*(cTrameEntree + 10) == '0')) //30
        {
            if (fV2 > 2)
                RECUPERATION_DES_TRAMES(cTrameSortie); //ENVOIE VIRTUEL DE COMMANDE DE IO (EN REALITE CARTE SD)
        }

        if ((*(cTrameEntree + 9) == '4') && (*(cTrameEntree + 10) == '0')) //40
        {
            if (fV3 > 2)
                RECUPERATION_DES_TRAMES(cTrameSortie); //ENVOIE VIRTUEL DE COMMANDE DE IO (EN REALITE CARTE SD)
        }

        if ((*(cTrameEntree + 9) == '5') && (*(cTrameEntree + 10) == '0')) //50
        {
            if (fV4 > 2)
                RECUPERATION_DES_TRAMES(cTrameSortie); //ENVOIE VIRTUEL DE COMMANDE DE IO (EN REALITE CARTE SD)
        }

        if (*(cTrameEntree + 8) == '1') //100
        {
            if (fV5 > 2)
                RECUPERATION_DES_TRAMES(cTrameSortie); //ENVOIE VIRTUEL DE COMMANDE DE IO (EN REALITE CARTE SD)
        }





    } while (*(cTrameEntree + 1) != '$' && *(cTrameEntree + 8) != '$' && *(cTrameEntree + 9) != '$');
    DX10 = 1;
    DX20 = 1;
    *cBitSD = 0x00;
    
    return(iPointeur_out_in);

}

int TRANSFORMER_TRAME_MEM_EN_COM(unsigned char *cTrameEntree, unsigned char *cTrameSortie, unsigned char *cVoieCod) {

    int t;
    int u;
    int iPointeur_out_in;
    u = 8;
    
    *cTrameSortie = DEBUT_TRAME;
    // creer capteur a voir ou il cree et tabel exis
    *(cVoieCod + u - 8) = *(cTrameEntree + u - 7);
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cVoieCod + u - 8) = *(cTrameEntree + u - 7);
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cVoieCod + u - 8) = *(cTrameEntree + u - 7);
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cVoieCod + u - 8) = *(cTrameEntree + u - 7);
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cVoieCod + u - 8) = *(cTrameEntree + u - 7);
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;


    //ecrire dans I2C

    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;

    t = 0;
    for (t = 0; t < 13; t++) {
        *(cTrameSortie + u) = *(cTrameEntree + u - 7);
        u = u + 1;

    }

    t = 0;
    for (t = 0; t < 18; t++) {
        *(cTrameSortie + u) = *(cTrameEntree + u - 7);
        u = u + 1;
    }


    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;
    *(cTrameSortie + u) = *(cTrameEntree + u - 7);
    u = u + 1;

    *(cTrameSortie + u) = FIN_TRAME;
    iPointeur_out_in = u;
    
    return(iPointeur_out_in);
}

void EFFACEMENT(unsigned char *tab, unsigned char *cTrameEfface) {

    char t;

    *(cTrameEfface + 0) = DEBUT_TRAME; //#
    *(cTrameEfface + 1) = tab[0];
    *(cTrameEfface + 2) = tab[1];
    *(cTrameEfface + 3) = tab[2];
    *(cTrameEfface + 4) = tab[3];
    *(cTrameEfface + 5) = tab[4];


    for (t = 6; t < 125; t++) {
        *(cTrameEfface + t) = 0xFF;
    }

    ECRIRE_EEPROMBYTE(choix_memoire(cTrameEfface), calcul_addmemoire(cTrameEfface), cTrameEfface, 1); //ECRITURE EN EEPROM


}

char trouver_carte_suivantvoie(unsigned char *tab) {

    unsigned int i;
    unsigned int t;





    t = 0;
    i = tab[2] - 48;
    t = t + 1 * i;
    i = tab[1] - 48;
    t = t + 10 * i;
    i = tab[0] - 48;
    t = t + 100 * i;





    if (t <= 100)
        i = 5;
    if (t <= 80)
        i = 4;
    if (t <= 60)
        i = 3;
    if (t <= 40)
        i = 2;
    if (t <= 20)
        i = 1;



    return (i);


}

void REPOSAQUIENVOYER(unsigned char *tabo) {


    if ((tabo[0] == 'R')&&(tabo[1] == '1')) {
        DRS485_R1 = 0;

    }

    delay_ms(10);



    if ((tabo[0] == 'R')&&(tabo[1] == '2')) {
        DRS485_R2 = 0;

    }

    delay_ms(10);




    if ((tabo[0] == 'R')&&(tabo[1] == '3')) {
        DRS485_R3 = 0;

    }

    delay_ms(10);



    if ((tabo[0] == 'R')&&(tabo[1] == '4')) {
        DRS485_R4 = 0;

    }

    delay_ms(10);



    if ((tabo[0] == 'R')&&(tabo[1] == '5')) {
        DRS485_R5 = 0;

    }

    delay_ms(10);
}

void AQUIENVOYER(unsigned char *tabo) {


    if ((tabo[0] == 'R')&&(tabo[1] == '1')) {
        DRS485_R1 = 0;
        delay_ms(50);
        DRS485_R1 = 1;

    }



    if ((tabo[0] == 'R')&&(tabo[1] == '2')) {
        DRS485_R2 = 0;
        delay_ms(50);
        DRS485_R2 = 1;

    }

    if ((tabo[0] == 'R')&&(tabo[1] == '3')) {
        DRS485_R3 = 0;
        delay_ms(50);
        DRS485_R3 = 1;
    }

    if ((tabo[0] == 'R')&&(tabo[1] == '4')) {
        DRS485_R4 = 0;
        delay_ms(50);
        DRS485_R4 = 1;

    }

    if ((tabo[0] == 'R')&&(tabo[1] == '5')) {
        DRS485_R5 = 0;
        delay_ms(50);
        DRS485_R5 = 1;

    }
    delay_ms(200);
    delay_ms(200);
    delay_ms(200);
}

char EFFACER_CAPTEUR_SUR_CM(char gh, unsigned char *cDemandeInfosCapteurs, char *cNombreCapteursCable, char *cCaptExist, unsigned char *cTmpTab, unsigned char *cVoieCod, char *cEtat){
    char t, ss, aa, vv;
    
    char cSortie;

//    ss = *cVoieCod;
//    aa = *(cVoieCod + 1);
//    vv = *(cVoieCod + 2);



//    *cVoieCod = DEMANDE_INFOS_CAPTEURS[0];
//    *(cVoieCod + 1) = DEMANDE_INFOS_CAPTEURS[1];
//    *(cVoieCod + 2) = DEMANDE_INFOS_CAPTEURS[2];




    *cNombreCapteursCable = 0;


    // VOIR COMBIEN IL EXISTE DE CAPTEURS
    for (t = 0; t < 17; t++) {
        CHERCHER_DONNNEES_CABLES(cDemandeInfosCapteurs, t, cTmpTab, cVoieCod, cEtat, cCaptExist); //CAPTEURS UN A UN
        if (*cCaptExist == 1) {
            *cNombreCapteursCable ++;
        }
    }

    if (gh == 1) //SI ON VEUT LE FAIRE
    {

        // LES DONNEES SONT RANGEES DANS DEMANDE_INFOS_CAPTEURS

        *(cDemandeInfosCapteurs + 5) = *(cDemandeInfosCapteurs + 4);
        *(cDemandeInfosCapteurs + 4) = *(cDemandeInfosCapteurs + 3);
        *(cDemandeInfosCapteurs + 3) = *(cDemandeInfosCapteurs + 2);
        *(cDemandeInfosCapteurs + 2) = *(cDemandeInfosCapteurs + 1);
        *(cDemandeInfosCapteurs + 1) = *(cDemandeInfosCapteurs + 0);
        *(cDemandeInfosCapteurs + 0) = DEBUT_TRAME;

        MODIFICATION_TABLE_DEXISTENCE(cDemandeInfosCapteurs, 1, 0, cTmpTab, *cNombreCapteursCable); // ON EFFACE LE CAPTEUR DANS LA MEMOIRE TABLE EXIST DE L'IO
    }


//    VOIECOD[0] = ss;
//    *(cVoieCod + 1) = aa;
//    *(cVoieCod + 2) = vv;


    if (*cNombreCapteursCable <= 1)
        cSortie = 1;
    else
        cSortie = 0;
    
    return(cSortie);

}

void MODIFICATION_TABLE_DEXISTENCE(unsigned char *v, char eff, char etat, unsigned char *cTmpTab, char cNombreCapteursCable){ //eff pour effacer (eff=2 alors recopie) alors et etat ligne pour fusible HS ou CC ou surtension


    unsigned int y, z;
    char fa;




    v[0] = DEBUT_TRAME;
    y = 100 * (v[1] - 48) + 10 * (v[2] - 48)+(v[3] - 48); //VOIE

    y = 128 * y + 30000; //04/07/2017

    LIRE_EEPROM(3, y, cTmpTab, 4); //LIRE DANS EEPROM

    z = 10 * (v[4] - 48)+(v[5] - 48); //CODAGE

    if (eff == 2) //RECOPIE
    {
        goto recop;
    }



    if (eff == 0) //CREATION attention ici on cr�e le type capteur et le type de cable ic on a pas encore test� si le cable avant etait resistif ou r codage en 2 3 ....il faut juste
    {
        *(cTmpTab + z + 3) = v[6]; //RECOPIE SI R OU A #00101R
        fa = *(cTmpTab + z + 3);



    }


    if (fa == 'A') {
        *(cTmpTab + 2) = 'a';

    }
    if (fa == 'R') //L'ADRESSABLE NE PEUT QUE EXISTER EN CODAGE1 !!!!!!!!!!!!!!!!!!!!!!
    {
        *(cTmpTab + 2) = 'r';
    }



    if (eff == 1) //EFFACEMENT
    {
        *(cTmpTab + z + 3) = 'F'; //CAPTEUR EFFACEE DE LA TABLE DEXISTENCE
        // VOIR voie a 'v' qd il y a que des 'F'

        if (cNombreCapteursCable <= 1) //SI IL N'Y A QUE UN CAPTEUR VARIABLES INIT DANS MENU6b
            *(cTmpTab + 2) = 'v'; // ON EFFACE LE CABLE



    }


recop:



    if (etat == 1) {
        *(cTmpTab + 2) = 'h';
    }

    if (etat == 2) {
        *(cTmpTab + 2) = 'h';
    }

    if ((etat == 4)&&(*(cTmpTab + 2) != 'c')) {
        *(cTmpTab + 2) = *(cTmpTab + 21);
    }

    if ((etat == 5)&&(*(cTmpTab + 2) != 'c')) {
        *(cTmpTab + 2) = *(cTmpTab + 21);

    }

    if (etat == 6) {
        *(cTmpTab + 2) = 'c';

    }


    if (etat == 7) {
        *(cTmpTab + 2) = 'c';
    }


    if (etat == 8) // SCRUTATION POUR INHIBER DEFAUT CC
    {
        *(cTmpTab + 2) = *(cTmpTab + 21);
    }




ecrire:


    //ecrire_tab_presence_TPR;
    ECRIRE_EEPROMBYTE(3, y, cTmpTab, 4);

    delay_ms(10);
    LIRE_EEPROM(3, y, cTmpTab, 4); //LIRE DANS EEPROM pour verifier



}

void CHERCHER_DONNNEES_CABLES(unsigned char *g, char j, unsigned char *cTmpTab, unsigned char *cVoieCod, char *cEtat, char *cCaptExist){ //051 cable et codage j //DONNE LES DONNEES DU CABLE ET DU CODAGE CHOISSI ET MET UNE VARIABLE EXISTENCE A UN SI CAPTEUR EXISTE

    char i;
    unsigned char tati[6];
    unsigned int l;

    tati[5] = '0';
    tati[4] = '0';
    tati[3] = g[2];
    tati[2] = g[1];
    tati[1] = g[0];
    tati[0] = DEBUT_TRAME;

    if (j != 111) {
        tati[4] = j / 10 + 48; //CODAGE
        tati[5] = 48 + j - 10 * (tati[4] - 48);
    }


    l = 100 * (tati[1] - 48) + 10 * (tati[2] - 48) + 1 * (tati[3] - 48);
    l = 128 * l + 30000;



    LIRE_EEPROM(3, l, cTmpTab, 4); //LIRE DANS EEPROM lexistance du capteur




    *cCaptExist = 0;
    if (j == 111) // ON veut savoir s'il cable
    {
        j = 2; //CABLE
        if (*(cTmpTab + j) == 'a') //VOIR i etc....
            *cCaptExist = 1;
        if (*(cTmpTab + j) == 'r')
            *cCaptExist = 1;
        if (*(cTmpTab + j) == 'c')
            *cCaptExist = 2; //CC
        if (*(cTmpTab + j) == 'h')
            *cCaptExist = 3; //FUSIBLE
        if (*(cTmpTab + j) == 'i')
            *cCaptExist = 4; //INCIDENT

    } else {
        j += 3; ///CAPTEUR
        if (*(cTmpTab + j) == 'A')
            *cCaptExist = 1;
        if ((*(cTmpTab + 2) == 'r')&&(tati[5] == '1')&&(tati[4] == '0')) //AVANT 'R' PAR CODAGE MNT r au debut
            *cCaptExist = 1;
    }






    if (*cCaptExist == 1) {
        LIRE_EEPROM(choix_memoire(tati), calcul_addmemoire(tati), cTmpTab, 1); //LIRE DANS EEPROM voir si TRAMEIN DISPO

        *(cEtat) = *(cTmpTab + 75);
        *(cEtat + 1) = *(cTmpTab + 76);


    } else {

        l = 10 * (*(cVoieCod + 3) - 48)+(*(cVoieCod + 4) - 48) + 1;

        *(cEtat) = '0';
        *(cEtat + 1) = '0';
    }

}

void SERIALISATION(char *cEntree1, char *cEntree2) {

    char i;
    char TMP = 0X00, TMP2 = 0x00;


    SHLD = 0; //SH/LD=1
    CLK = 0; //CLK=0
    INH = 1; //INH=1


    delay_ms(100); // ATTENDRE STABILITE ENTREE


    SHLD = 1; //SH/LD=0
    CLK = 1;
    INH = 0; //INT=0

    for (i = 0; i < 7; i++) {
        TMP = TMP | INT1;
        TMP2 = TMP2 | INT2;

        CLK = 0; //CLK=0
        //delay_ms(100);     //FRONT CLK MONTANT
        CLK = 1; //CLK=1
        TMP = TMP << 1;
        TMP2 = TMP2 << 1;

    }

    TMP = TMP | INT1;
    TMP2 = TMP2 | INT2;
    SHLD = 0; //SH/LD=1
    CLK = 0; //CLK=0
    INH = 1; //INH=1

    *cEntree2 = TMP; //ETAT D7 � D0 int1
    *cEntree1 = TMP2; //ETAT  D1 � D0 int2



}

void SDW(char yui,unsigned char *cTrameEntree, unsigned char *cTrameSortie, char *cTypeCarte, float fV1, float fV2, float fV3, float fV4, float fV5, float fVbat, unsigned char *cSavTab,unsigned char *cTmpTab, char cR1NoRep, char cR2NoRep, char cR3NoRep, char cR4NoRep, char cR5NoRep, unsigned char *cHorloge) {


    unsigned int VV, ZO, bb;
    unsigned char tmpvoiecod[6], tmpchar[3];
    char flag;

    TEMPO(5);
    //ECRIRE DES TRAMES EN SD
    if (yui == 'D')
        envoyer_TRAME_PICSD(0xA2, cTrameEntree, cTrameSortie, cHorloge); //START  ECRIRE DANS SD MODE DEMARRAGE
    if (yui == 'C')
        envoyer_TRAME_PICSD(0xA3, cTrameEntree, cTrameSortie, cHorloge); //START  ECRIRE DANS SD MODE CYCLIQUE
    if (yui == 'M')
        envoyer_TRAME_PICSD(0xA4, cTrameEntree, cTrameSortie, cHorloge); //START  ECRIRE DANS SD MODE MINITEL
    if (yui == 'N')
        envoyer_TRAME_PICSD(0xA5, cTrameEntree, cTrameSortie, cHorloge); //START  ECRIRE DANS SD MODE MINITEL

    TEMPO(2);

sautecarte:
    for (VV = 1; VV < 103; VV++) {
        flag = 0;
        if (VV == 101) //INFORMATIONS SITE
        {
            LIRE_EEPROM(6, 1024, cTrameSortie, 5); //TRAME CONFIG

            *cTrameSortie = DEBUT_TRAME;
            *(cTrameSortie + 1) = '1';
            *(cTrameSortie + 2) = '0';
            *(cTrameSortie + 3) = '1';
            *(cTrameSortie + 90) = FIN_TRAME;
            envoyer_TRAME_PICSD(0xEE, cTrameEntree, cTrameSortie, cHorloge); //EM
        }
        if (VV == 102) //INFORMATIONS SITE
        {
            IDENTIF_TYPE_CARTE(cTmpTab, cTypeCarte, fV1, fV2, fV3, fV4, fV5, fVbat, cR1NoRep, cR2NoRep, cR3NoRep, cR4NoRep, cR5NoRep);
            *(cTrameSortie) = DEBUT_TRAME;
            *(cTrameSortie + 1) = '1';
            *(cTrameSortie + 2) = '0';
            *(cTrameSortie + 3) = '2';
            *(cTrameSortie + 4) = *cTypeCarte;
            *(cTrameSortie + 5) = *(cTypeCarte + 1);
            *(cTrameSortie + 6) = *(cTypeCarte + 2);
            *(cTrameSortie + 7) = *(cTypeCarte + 3);
            *(cTrameSortie + 8) = *(cTypeCarte + 4);
            *(cTrameSortie + 90) = FIN_TRAME;
            envoyer_TRAME_PICSD(0xEE, cTrameEntree, cTrameSortie, cHorloge); //EM
        }

        if ((fV1 <= 2)&&(VV < 21)) //PAS DE CARTE R1 16092020
        {
            flag = 1;
        }
        if ((fV2 <= 2)&&(VV >= 21)&&(VV < 41)) //PAS DE CARTE R2
        {
            flag = 1;
        }
        if ((fV3 <= 2)&&(VV >= 41)&&(VV < 61)) //PAS DE CARTE R3
        {
            flag = 1;
        }
        if ((fV4 <= 2)&&(VV >= 61)&&(VV < 81)) //PAS DE CARTE R4
        {
            flag = 1;
        }
        if ((fV5 <= 2)&&(VV >= 81)) //PAS DE CARTE R5
        {
            flag = 1;
        }

        if (flag == 0) {
            DX10 = !DX10;
            DX20 = !DX20;
            ZO = 128 * VV + 30000;
            LIRE_EEPROM(3, ZO, cSavTab, 4); //LIRE DANS EEPROM lexistance de toutes le voies


            if (*(cSavTab + 2) == 'a') //Voie ADRESSABLE
            {
                for (bb = 0; bb < 17; bb++) {
                    IntToChar(VV, tmpchar, 3); //ON TRANSFORME EN CHAR le numero de voie
                    tmpvoiecod[0] = DEBUT_TRAME;
                    tmpvoiecod[1] = tmpchar[0];
                    tmpvoiecod[2] = tmpchar[1];
                    tmpvoiecod[3] = tmpchar[2];
                    IntToChar(bb, tmpchar, 2); //ON TRANSFORME EN CHAR le numero de codage
                    tmpvoiecod[4] = tmpchar[0];
                    tmpvoiecod[5] = tmpchar[1];

                    LIRE_EEPROM(choix_memoire(tmpvoiecod), calcul_addmemoire(tmpvoiecod), cTrameSortie, 1); //LIRE DANS EEPROM
                    *(cTrameSortie + 85) = '2'; // AJOUT CRC ICI ON RUSE LE CRC=E22222 pour le codage0 -> existence du debitmetre sinon F2222
                    *(cTrameSortie + 86) = '2';
                    *(cTrameSortie + 87) = '2';
                    *(cTrameSortie + 88) = '2';
                    *(cTrameSortie + 89) = '2';
                    *(cTrameSortie + 90) = FIN_TRAME;



                    if (bb == 0) //LE CODAGE 0 est TOUJOURS envoy� car il contient avec certitude les infos cable
                    {
                        if (*(cSavTab + 3) == 'A') {
                            *(cTrameSortie + 85) = 'E';
                        }
                        if (*(cSavTab + 3) != 'A') {
                            *(cTrameSortie + 85) = 'F';
                        }
                        envoyer_TRAME_PICSD(0xEE, cTrameEntree, cTrameSortie, cHorloge); //EM

                    }

                    if (*(cSavTab + 3 + bb) == 'A') //ON ENVOIE QUE LES CAPTEUS ADRESSABLES
                        envoyer_TRAME_PICSD(0xEE, cTrameEntree, cTrameSortie, cHorloge); //EM

                    delay_ms(10);
                }
            }

            if (*(cSavTab + 2) == 'r') //Voie RESISTIVE
            {

                IntToChar(VV, tmpchar, 3); //ON TRANSFORME EN CHAR le numero de voie
                tmpvoiecod[0] = DEBUT_TRAME;
                tmpvoiecod[1] = tmpchar[0];
                tmpvoiecod[2] = tmpchar[1];
                tmpvoiecod[3] = tmpchar[2];
                tmpvoiecod[4] = '0';
                tmpvoiecod[5] = '1';
                LIRE_EEPROM(choix_memoire(tmpvoiecod), calcul_addmemoire(tmpvoiecod), cTrameSortie, 1); //LIRE DANS EEPROM
                *(cTrameSortie + 85) = '2'; // AJOUT CRC
                *(cTrameSortie + 86) = '2';
                *(cTrameSortie + 87) = '2';
                *(cTrameSortie + 88) = '2';
                *(cTrameSortie + 89) = '2';
                *(cTrameSortie + 90) = FIN_TRAME;



                envoyer_TRAME_PICSD(0xEE, cTrameEntree, cTrameSortie, cHorloge); //EM
                delay_ms(10);


            }
        }
    }
    envoyer_TRAME_PICSD(0xFF, cTrameEntree, cTrameSortie, cHorloge); //FIN DE TRANS.

    DX10 = 1;
    DX20 = 1;
}

void IDENTIF_TYPE_CARTE(unsigned char *cTmpTab, char *cTypeCarte, float fV1, float fV2, float fV3, float fV4, float fV5, float fVbat, char cR1NoRep, char cR2NoRep, char cR3NoRep, char cR4NoRep, char cR5NoRep) {
    unsigned int nbint;

    MESURE_DES_TENSIONS(&fV1, &fV2, &fV3, &fV4, &fV5, &fVbat);

    //de 0 � 20 carte R1

    nbint = 1; //carte R1
    LIRE_EEPROM(3, 128 * nbint + 30000, cTmpTab, 4); //LIRE DANS EEPROM TABLE EXISTENCE
    if (*(cTmpTab + 22) == 'M') //TYPE RESITIVE
        *cTypeCarte = 'M';
    else
        *cTypeCarte = 'E'; //TYPE ELECTRIQUE
    if (fV1 <= 2)
        *cTypeCarte = 'V'; //PAS DE TENSION CARTE HS ou absente
    if (cR1NoRep == 12)
        *cTypeCarte = 'V'; //CARTE DEFECTUEUSE

    nbint = 21; //carte R2
    LIRE_EEPROM(3, 128 * nbint + 30000, cTmpTab, 4); //LIRE DANS EEPROM
    if (*(cTmpTab + 22) == 'M')
        *(cTypeCarte + 1) = 'M';
    else
        *(cTypeCarte + 1) = 'E';
    if (fV2 <= 2)
        *(cTypeCarte + 1) = 'V';
    if (cR2NoRep == 12)
        *(cTypeCarte + 1) = 'V'; //CARTE DEFECTUEUSE



    nbint = 41; //carte R3
    LIRE_EEPROM(3, 128 * nbint + 30000, cTmpTab, 4); //LIRE DANS EEPROM
    if (*(cTmpTab + 22) == 'M')
        *(cTypeCarte + 2) = 'M';
    else
        *(cTypeCarte + 2) = 'E';
    if (fV3 <= 2)
        *(cTypeCarte + 2) = 'V';
    if (cR3NoRep == 12)
        *(cTypeCarte + 2) = 'V'; //CARTE DEFECTUEUSE



    nbint = 61; //carte R4
    LIRE_EEPROM(3, 128 * nbint + 30000, cTmpTab, 4); //LIRE DANS EEPROM
    if (*(cTmpTab + 22) == 'M')
        *(cTypeCarte + 3) = 'M';
    else
        *(cTypeCarte + 3) = 'E';
    if (fV4 <= 2)
        *(cTypeCarte + 3) = 'V';
    if (cR4NoRep == 12)
        *(cTypeCarte + 3) = 'V'; //CARTE DEFECTUEUSE



    nbint = 81; //carte R5
    LIRE_EEPROM(3, 128 * nbint + 30000, cTmpTab, 4); //LIRE DANS EEPROM
    if (*(cTmpTab + 22) == 'M')
        *(cTypeCarte + 4) = 'M';
    else
        *(cTypeCarte + 4) = 'E';
    if (fV5 <= 2)
        *(cTypeCarte + 4) = 'V';
    if (cR5NoRep == 12)
        *(cTypeCarte + 4) = 'V'; //CARTE DEFECTUEUSE



}

void SDWBN(unsigned char *cTrameEntree, unsigned char *cTrameSortie, unsigned char *cHorloge) {
    
    unsigned int VV;
    unsigned int iCalculBloc;

    TEMPO(5);
    
    envoyer_TRAME_PICSD(0xA7, cTrameEntree, cTrameSortie, cHorloge); //START  ECRIRE DANS SD 
    
    TEMPO(2);

    for (VV = 1; VV < 22; VV++) {

        DX10 = !DX10;
        DX20 = !DX20;

        iCalculBloc = 2048 + (VV - 1)*128;
        TEMPO(1);
        LIRE_EEPROM(6, iCalculBloc, cTrameSortie, 6); //LIRE 85caract		
        envoyer_TRAME_PICSD(0xEE, cTrameEntree, cTrameSortie, cHorloge); //EM

        delay_ms(10);
    }

    envoyer_TRAME_PICSD(0xFF, cTrameEntree, cTrameSortie, cHorloge); //FIN DE TRANS.

    DX10 = 1;
    DX20 = 1;
}

void SDRBN(unsigned char *cTrameEntree, unsigned char *cTrameSortie, unsigned char *cCRC, unsigned char *cHorloge, char *cNoReception) {
    char gigi;


recBN:

    TEMPO(3);

    *(cTrameEntree + 1) = '0';
    *(cTrameEntree + 2) = '0';
    do {
        DX10 = !DX10;
        DX20 = !DX20;

        delay_ms(100);
        envoyer_TRAME_PICSD(0xA9, cTrameEntree, cTrameSortie, cHorloge); //START  RECEVOIR POUR PROGRAMMER
        delay_ms(100);
        envoyer_TRAME_PICSD(0x00, cTrameEntree, cTrameSortie, cHorloge); //RECEP faire Nx jusqua $$$
        if (*(cTrameEntree + 2) == '$') {
            TEMPO(1);
            goto finwhiBN;
            delay_ms(10);
        }


        if (*(cTrameEntree + 4) == 50) {
            delay_ms(10);
            delay_ms(10);
        }


        delay_ms(10);
        for (gigi = 0; gigi < 99; gigi++) //-#021..... ce que lon recoit
        {

            *(cTrameSortie + gigi) = *(cTrameEntree + gigi + 1);

        }


        *(cTrameSortie + 0) = DEBUT_TRAME;
        *(cTrameSortie + 1) = 'C';
        *(cTrameSortie + 2) = 'M';
        *(cTrameSortie + 3) = 'A'; //DD cad dire carte memoire
        *(cTrameSortie + 4) = 'U';
        *(cTrameSortie + 5) = 'D';
        *(cTrameSortie + 6) = 'D';
        *(cTrameSortie + 7) = 'B';
        TRAMEENVOIRS485DONNEES(cTrameSortie); // ENVOI A LAU
        TRAMERECEPTIONRS485DONNEES(18, cNoReception, cTrameEntree); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
finwhiBN:
        controle_CRC(cCRC, cTrameEntree);

    } while (*(cTrameEntree + 2) != '$' && *(cTrameEntree + 3) != '$');
    DX10 = 1;
    DX20 = 1;
}

void REMISE_EN_FORME_PAGE(unsigned char *cTrameSortie, unsigned char *cCRC, int *iPointeurEntreeSortie) {

    //ON CACULE LES ETATS SUIVANT CAHIER DES CHARGES A VOIR PUIS ON REDUIT LA TRAME
    //#CMAUZAC041xxxxxxxxxxxxxxxxxxA00FF013102FF03FF04FF05FF06FF07FF08FF09FF10FF11FF12FF13FF14FF15FF16FF22222*

    if (*(cTrameSortie + 29) == 'F') {
        *(cTrameSortie + 30) = 'F';
        *(cTrameSortie + 31) = 'F';
    }

    //SI 1 etat 30 alors une alarme prioritaire
    //SI 1 int 01 alors intervention mais aucune alarme

    //PAR DEFAUT OK POUR L'INSTANT on pourrait H/G ET CC et FUSIBLE et i incident Res sur adre

    *(cTrameSortie + 30) = '0';
    *(cTrameSortie + 31) = '0';



    //SI INCIDENT PAT
    if (*(cTrameSortie + 32) == '9') {
        *(cTrameSortie + 30) = '9';
        *(cTrameSortie + 31) = '9';
    }
    //SI CC PAT
    if (*(cTrameSortie + 32) == '8') {
        *(cTrameSortie + 30) = '8';
        *(cTrameSortie + 31) = '8';
    }
    //SI FUS PAT
    if (*(cTrameSortie + 32) == '7') {
        *(cTrameSortie + 30) = '7';
        *(cTrameSortie + 31) = '7';
    }



    //SI AU MOINS UN H/G SUR LA VOIE

    if ((*(cTrameSortie + 32) == '4') || (*(cTrameSortie + 36) == '4') || (*(cTrameSortie + 40) == '4') || (*(cTrameSortie + 44) == '4') || (*(cTrameSortie + 48) == '4') || (*(cTrameSortie + 52) == '4') || (*(cTrameSortie + 56) == '4') || (*(cTrameSortie + 60) == '4') || (*(cTrameSortie + 64) == '4') || (*(cTrameSortie + 68) == '4') || (*(cTrameSortie + 72) == '4') || (*(cTrameSortie + 76) == '4') || (*(cTrameSortie + 80) == '4') || (*(cTrameSortie + 84) == '4') || (*(cTrameSortie + 88) == '4') || (*(cTrameSortie + 92) == '4') || (*(cTrameSortie + 96) == '4')) {
        *(cTrameSortie + 30) = '3';
        *(cTrameSortie + 31) = '0';
    }



    //SI AU MOINS UNE NON REPONSE SUR LA VOIE

    if ((*(cTrameSortie + 32) == '5') || (*(cTrameSortie + 36) == '5') || (*(cTrameSortie + 40) == '5') || (*(cTrameSortie + 44) == '5') || (*(cTrameSortie + 48) == '5') || (*(cTrameSortie + 52) == '5') || (*(cTrameSortie + 56) == '5') || (*(cTrameSortie + 60) == '5') || (*(cTrameSortie + 64) == '5') || (*(cTrameSortie + 68) == '5') || (*(cTrameSortie + 72) == '5') || (*(cTrameSortie + 76) == '5') || (*(cTrameSortie + 80) == '5') || (*(cTrameSortie + 84) == '5') || (*(cTrameSortie + 88) == '5') || (*(cTrameSortie + 92) == '5') || (*(cTrameSortie + 96) == '5')) {
        *(cTrameSortie + 30) = '3';
        *(cTrameSortie + 31) = '0';
    }




    //SI AU MOINS UNE ALARME SUR LA VOIE

    if ((*(cTrameSortie + 32) == '3') || (*(cTrameSortie + 36) == '3') || (*(cTrameSortie + 40) == '3') || (*(cTrameSortie + 44) == '3') || (*(cTrameSortie + 48) == '3') || (*(cTrameSortie + 52) == '3') || (*(cTrameSortie + 56) == '3') || (*(cTrameSortie + 60) == '3') || (*(cTrameSortie + 64) == '3') || (*(cTrameSortie + 68) == '3') || (*(cTrameSortie + 72) == '3') || (*(cTrameSortie + 76) == '3') || (*(cTrameSortie + 80) == '3') || (*(cTrameSortie + 84) == '3') || (*(cTrameSortie + 88) == '3') || (*(cTrameSortie + 92) == '3') || (*(cTrameSortie + 96) == '3')) {
        *(cTrameSortie + 30) = '3';
        *(cTrameSortie + 31) = '0';
    }





    //SI TT en  INTERVENTION
    if (((*(cTrameSortie + 33) == '1') || (*(cTrameSortie + 33) == 'F'))&&((*(cTrameSortie + 37) == '1') || (*(cTrameSortie + 37) == 'F'))&&((*(cTrameSortie + 41) == '1') || (*(cTrameSortie + 41) == 'F'))&&((*(cTrameSortie + 45) == '1') || (*(cTrameSortie + 45) == 'F'))&&((*(cTrameSortie + 49) == '1') || (*(cTrameSortie + 49) == 'F'))&&((*(cTrameSortie + 53) == '1') || (*(cTrameSortie + 53) == 'F'))&&((*(cTrameSortie + 57) == '1') || (*(cTrameSortie + 57) == 'F'))&&((*(cTrameSortie + 61) == '1') || (*(cTrameSortie + 61) == 'F'))&&((*(cTrameSortie + 65) == '1') || (*(cTrameSortie + 65) == 'F'))&&((*(cTrameSortie + 69) == '1') || (*(cTrameSortie + 69) == 'F'))&&((*(cTrameSortie + 73) == '1') || (*(cTrameSortie + 73) == 'F'))&&((*(cTrameSortie + 77) == '1') || (*(cTrameSortie + 77) == 'F'))&&((*(cTrameSortie + 81) == '1') || (*(cTrameSortie + 81) == 'F'))&&((*(cTrameSortie + 85) == '1') || (*(cTrameSortie + 85) == 'F'))&&((*(cTrameSortie + 89) == '1') || (*(cTrameSortie + 89) == 'F'))&&((*(cTrameSortie + 93) == '1') || (*(cTrameSortie + 93) == 'F'))&&((*(cTrameSortie + 97) == '1') || (*(cTrameSortie + 97) == 'F'))) {
        if (*(cTrameSortie + 30) == '3')
            *(cTrameSortie + 30) = '3'; // SI UNE ALARME PRECEDENTE
        else
            *(cTrameSortie + 30) = '0';

        *(cTrameSortie + 31) = '1';

    }

    //si tt en intervention sauf certains OK
    if (((*(cTrameSortie + 32) == '0') || (*(cTrameSortie + 33) == '1') || (*(cTrameSortie + 33) == 'F'))&&((*(cTrameSortie + 36) == '0') || (*(cTrameSortie + 37) == '1') || (*(cTrameSortie + 37) == 'F'))&&((*(cTrameSortie + 40) == '0') || (*(cTrameSortie + 41) == '1') || (*(cTrameSortie + 41) == 'F'))&&((*(cTrameSortie + 44) == '0') || (*(cTrameSortie + 45) == '1') || (*(cTrameSortie + 45) == 'F'))) {

        if (((*(cTrameSortie + 48) == '0') || (*(cTrameSortie + 49) == '1') || (*(cTrameSortie + 49) == 'F'))&&((*(cTrameSortie + 52) == '0') || (*(cTrameSortie + 53) == '1') || (*(cTrameSortie + 53) == 'F'))&&((*(cTrameSortie + 56) == '0') || (*(cTrameSortie + 57) == '1') || (*(cTrameSortie + 57) == 'F'))&&((*(cTrameSortie + 60) == '0') || (*(cTrameSortie + 61) == '1') || (*(cTrameSortie + 61) == 'F'))&&((*(cTrameSortie + 64) == '0') || (*(cTrameSortie + 65) == '1') || (*(cTrameSortie + 65) == 'F'))&&((*(cTrameSortie + 68) == '0') || (*(cTrameSortie + 69) == '1') || (*(cTrameSortie + 69) == 'F'))&&((*(cTrameSortie + 72) == '0') || (*(cTrameSortie + 73) == '1') || (*(cTrameSortie + 73) == 'F'))&&((*(cTrameSortie + 76) == '0') || (*(cTrameSortie + 77) == '1') || (*(cTrameSortie + 77) == 'F'))&&((*(cTrameSortie + 80) == '0') || (*(cTrameSortie + 81) == '1') || (*(cTrameSortie + 81) == 'F'))&&((*(cTrameSortie + 84) == '0') || (*(cTrameSortie + 85) == '1') || (*(cTrameSortie + 85) == 'F'))&&((*(cTrameSortie + 88) == '0') || (*(cTrameSortie + 89) == '1') || (*(cTrameSortie + 89) == 'F'))&&((*(cTrameSortie + 92) == '0') || (*(cTrameSortie + 93) == '1') || (*(cTrameSortie + 93) == 'F'))&&((*(cTrameSortie + 96) == '0') || (*(cTrameSortie + 97) == '1') || (*(cTrameSortie + 97) == 'F'))) {
            //mais si tt est ok avec ou pas des vides
            if (((*(cTrameSortie + 32) == '0') || (*(cTrameSortie + 33) == 'F'))&&((*(cTrameSortie + 36) == '0') || (*(cTrameSortie + 37) == 'F'))&&((*(cTrameSortie + 40) == '0') || (*(cTrameSortie + 41) == 'F'))&&((*(cTrameSortie + 44) == '0') || (*(cTrameSortie + 45) == 'F'))&&((*(cTrameSortie + 48) == '0') || (*(cTrameSortie + 49) == 'F'))&&((*(cTrameSortie + 52) == '0') || (*(cTrameSortie + 53) == 'F'))&&((*(cTrameSortie + 56) == '0') || (*(cTrameSortie + 57) == 'F'))&&((*(cTrameSortie + 60) == '0') || (*(cTrameSortie + 61) == 'F'))&&((*(cTrameSortie + 64) == '0') || (*(cTrameSortie + 65) == 'F'))&&((*(cTrameSortie + 68) == '0') || (*(cTrameSortie + 69) == 'F'))&&((*(cTrameSortie + 72) == '0') || (*(cTrameSortie + 73) == 'F'))&&((*(cTrameSortie + 76) == '0') || (*(cTrameSortie + 77) == 'F'))&&((*(cTrameSortie + 80) == '0') || (*(cTrameSortie + 81) == 'F'))&&((*(cTrameSortie + 84) == '0') || (*(cTrameSortie + 85) == 'F'))&&((*(cTrameSortie + 88) == '0') || (*(cTrameSortie + 89) == 'F'))&&((*(cTrameSortie + 92) == '0') || (*(cTrameSortie + 93) == 'F'))&&((*(cTrameSortie + 96) == '0') || (*(cTrameSortie + 97) == 'F')))
                goto nepasint;

            if (*(cTrameSortie + 30) == '3')
                *(cTrameSortie + 30) = '3'; // SI UNE ALARME PRECEDENTE
            else
                *(cTrameSortie + 30) = '0';

            *(cTrameSortie + 31) = '1';
        }

    }
nepasint:

    *iPointeurEntreeSortie = 32;
    calcul_CRC(cCRC, cTrameSortie); //COMPLETE LA TRAME TRAMEOUT[] AVEC LE CRC
}

void envoyer_TRAME_PICSD(char SENS, unsigned char *cTrameEntree, unsigned char *cTrameSortie, unsigned char *cHorloge){ //ENVOIE SIMPLEMENT  #..........* une trame memoire a la SD ou recoit une trame memoire de la SD

    unsigned char u, z;
    u = 0;

    //////////////////////// TRANSMETTRE A SD VIA PIC2



    init_spi();
    spi_low();

    delay_ms(1);
    PORTDbits.RD7 = 1;
    PORTDbits.RD7 = 0; // Enable Chip Select
    creer_trame_horloge(cHorloge);
    z = SPI_WriteByte('-'); // ON active la TRANS.
    z = SPI_WriteByte('-'); // ON active la TRANS
    z = SPI_WriteByte(*(cHorloge + 6)); // j
    z = SPI_WriteByte(*(cHorloge + 7)); // j
    z = SPI_WriteByte(*(cHorloge + 8)); // m
    z = SPI_WriteByte(*(cHorloge + 9)); // m
    z = SPI_WriteByte(*(cHorloge + 10)); // a
    z = SPI_WriteByte(*(cHorloge + 11)); // a
    z = SPI_WriteByte(*(cHorloge + 0)); // h
    z = SPI_WriteByte(*(cHorloge + 1)); // h
    z = SPI_WriteByte(*(cHorloge + 2)); // m
    z = SPI_WriteByte(*(cHorloge + 3)); // m


    if (SENS == 0xA1) //DEBUT DE TRANSMIT
    {
        z = SPI_WriteByte('S'); // ordre
        z = SPI_WriteByte('R'); // ordre
        *(cTrameEntree + u) = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte('R'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte(FIN_TRAME); // ON TRANSMET
        u++;
    }


    if (SENS == 0xA9) //DEBUT DE TRANSMIT BLOC NOTES
    {


        z = SPI_WriteByte('S'); // ordre
        z = SPI_WriteByte('R'); // ordre
        *(cTrameEntree + u) = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte('L'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte(FIN_TRAME); // ON TRANSMET
        u++;
    }


    if (SENS == 0xA2) //DEBUT DE TRANSMIT DEMARRAGE
    {


        z = SPI_WriteByte('S'); // ordre
        z = SPI_WriteByte('W'); // ordre
        *(cTrameEntree + u) = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte('W'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte(FIN_TRAME); // ON TRANSMET
        u++;
    }


    if (SENS == 0xA3) //DEBUT DE TRANSMIT CYCLIQUE
    {
        z = SPI_WriteByte('S'); // ordre
        z = SPI_WriteByte('W'); // ordre
        *(cTrameEntree + u) = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte('V'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte(FIN_TRAME); // ON TRANSMET
        u++;
    }


    if (SENS == 0xA4) //DEBUT DE TRANSMIT MINITEL
    {
        z = SPI_WriteByte('S'); // ordre
        z = SPI_WriteByte('W'); // ordre

        *(cTrameEntree + u) = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte('M'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte(FIN_TRAME); // ON TRANSMET
        u++;
    }

    if (SENS == 0xA5) //DEBUT DE TRANSMIT MINITEL
    {
        z = SPI_WriteByte('S'); // ordre
        z = SPI_WriteByte('W'); // ordre
        *(cTrameEntree + u) = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte('N'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte(FIN_TRAME); // ON TRANSMET
        u++;
    }

    if (SENS == 0xA7) //DEBUT DE TRANSMIT MINITEL
    {
        z = SPI_WriteByte('S'); // ordre
        z = SPI_WriteByte('W'); // ordre
        *(cTrameEntree + u) = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte('B'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte(FIN_TRAME); // ON TRANSMET
        u++;

    }

    if (SENS == 0xFF) //FIN DE TRANSMIT.
    {


        z = SPI_WriteByte('F'); // ordre
        z = SPI_WriteByte('I'); // ordre
        *(cTrameEntree + u) = SPI_WriteByte('$'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte('$'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte('$'); // ON TRANSMET
        u++;
        *(cTrameEntree + u) = SPI_WriteByte(FIN_TRAME); // ON TRANSMET
        u++;
    }

    if (SENS == 0xEE) {


        z = SPI_WriteByte('S'); // ordre
        z = SPI_WriteByte('V'); // ordre


        do {
            *(cTrameEntree + u) = SPI_WriteByte(*(cTrameSortie + u)); // ON TRANSMET
            u++;
        } while (*(cTrameSortie + u - 1) != FIN_TRAME);



    }

    if (SENS == 0x00) {
        z = SPI_WriteByte('R'); // ordre
        z = SPI_WriteByte('C'); // ordre

        do {
            *(cTrameEntree + u) = SPI_WriteByte('!'); // ON RECOIT
            u++; 
        } while (*(cTrameEntree + u - 1) != FIN_TRAME);

    }

    /////////////////////// FIN TRANS. VERS SD VIA PIC2

}

void TROUVER_ROBINET(unsigned char *cDemandeInfoCapteur, unsigned char *cInstruction, unsigned char *cTmpTab) { //MAXIMUM CETTE FONCTION PEUT DURER 5S
    int t;
    char robinn[4];
    char u;

    u = 0;
    for (t = 2; t <= 13; t++)
        *(cInstruction + t) = 0;



    for (t = 0; t < 100; t++) // LES 100 voies
    {


        IntToChar(t, cDemandeInfoCapteur, 3);



        *(cDemandeInfoCapteur + 3) = *(cDemandeInfoCapteur + 2);
        *(cDemandeInfoCapteur + 2) = *(cDemandeInfoCapteur + 1);
        *(cDemandeInfoCapteur + 1) = *(cDemandeInfoCapteur + 0);



        // ON REGARDE SUR LE DEBIT EN PREMIER FAIRE ATTENTION QUE LORS DE LA SCRUTATION SI A OU R LES COMMENTAIRES SOIENT AUSSI ECRIS EN CODAGE 00  et que si on cr�e un capteur on ecrit aussi le commentaire en 0
        *(cDemandeInfoCapteur) = DEBUT_TRAME;
        *(cDemandeInfoCapteur + 4) = '0';
        *(cDemandeInfoCapteur + 5) = '0';


        LIRE_EEPROM(choix_memoire(cDemandeInfoCapteur), calcul_addmemoire(cDemandeInfoCapteur), cTmpTab, 1); //LIRE DANS EEPROM
        *(cDemandeInfoCapteur)= DEBUT_TRAME;

        robinn[0] = *(cTmpTab + 32); // ON REGARDE LE COMMMENTAIRE ET SI ON TROUVE Rbxx ou xx est le numero du robinet recherch�
        robinn[1] = *(cTmpTab + 33);
        robinn[2] = *(cTmpTab + 34);
        robinn[3] = *(cTmpTab + 35);




        if ((robinn[0] == 'R') &&(robinn[1] == 'o')) //avant Rb
        {
            if ((robinn[2] == *(cInstruction)) &&(robinn[3] == *(cInstruction + 1))) //compare N�robinet dedans a celui souhait�
            {
                u++;
                if (u <= 10) // on arrete AU BOUT DE 10 VOIES SUR LE MEME ROBINET
                {
                    *(cInstruction + u + 2) = t;
                }
                *(cInstruction + 13) = u; //ICI ON INDIQUE LE NOMBRE TROUVE MEME SI PLUS QUE 10
            }
        }
    }
}
