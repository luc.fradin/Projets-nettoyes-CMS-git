/* 
 * File:   IO.c
 * Author: ADECEF
 *
 * Created on 10 mai 2021, 14:33
 */

#include <stdio.h>
#include <stdlib.h>
#include "h/CMS.h"

void stopIO(char *cEtatIO) {

    if (*cEtatIO == 0xFF) //IO ON IL FAUT LETEINDRE POUR NE PLUS AUTORISER LIO A PARLER
    {
        DRS485_IO = 1; //ON CONSIDERE QUE CETAIT A 0 POUR FAIRE UN FRONT MONTANT
        *cEtatIO = 0x00;
    }

}
