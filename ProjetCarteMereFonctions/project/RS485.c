/* 
 * File:   RS485.c
 * Author: ADECEF
 *
 * Created on 10 mai 2021, 10:50
 */

#include <stdio.h>
#include <stdlib.h>
#include "h/CMS.h"


void Init_RS485(void) {

    unsigned int SPEED;

    TRISCbits.TRISC5 = 0;
    TRISCbits.TRISC7 = 1;
    TRISCbits.TRISC6 = 0;
    TRISCbits.TRISC5 = 0; //RC5 en sortie

    TRISAbits.TRISA4 = 0; //RA4 RX_TX en sortie OU DERE



    RCSTA1bits.SPEN = 1; // Enable serial port
    TXSTA1bits.SYNC = 0; // Async mode
    BAUDCON1bits.BRG16 = 1;

    TXSTA1bits.BRGH = 1; //haute vitesse
    SPEED = 624; // 624 set 9600 bauds 311 pour 19230bauds
    SPBRG1 = SPEED; // Write baudrate to SPBRG1
    SPBRGH1 = SPEED >> 8; // For 16-bit baud rate generation





    IPR1bits.RCIP = 1; // Set high priority
    PIR1bits.RCIF = 0; // met le drapeau d'IT � 0 (plus d'IT)
    PIE1bits.RCIE = 0; // ne g�n�re pas d'IT pour la liaison s�rie

    TXSTA1bits.TXEN = 0; // transmission inhib�e
    RCSTA1bits.RX9 = 0; // r�ception sur 8 bits
    TXSTA1bits.TX9 = 0; // transmission sur 8 bits
    RCSTA1bits.CREN = 0; // interdire reception
    RCONbits.IPEN = 1; // Enable priority levels

}

char RECEVOIR_RS485(unsigned char ttout, char *cNoReception) {
    unsigned char Carac;
    unsigned int TEMPS1, TEMPS2;
    DERE = 0; //AUTORISATION DE RECEVOIR
    RCSTAbits.CREN = 1;
    TEMPS1 = 0;
    TEMPS2 = 0;
    *cNoReception = 0x00;
RS485R:

    DX1 = !DX1;
    while ((PIR1bits.RCIF == 0)&&(TEMPS2 <= (ttout + 1))) // max 6s pour 10
    {

        TEMPS1 = TEMPS1 + 1;
        if (TEMPS1 >= 50000) //ENVIRON 500ms
        {
            TEMPS2++;
            TEMPS1 = 0;
        }

    }; // attend une r�ception de caract�re sur RS232
    //    __no_operation();

    Carac = RCREG1; // caract�re re�u
    if (Carac == 0x04) // EOT recu par les cartes resitives qd elle bloque
        RESETCMS = 1; //ALORS ON RESET LE CMS

    if (TEMPS2 >= ttout)
        *cNoReception = 0xff;



    PIR1bits.RCIF = 0;
    RCSTAbits.CREN = 0;
    return (Carac); // retourne le caract�re re�u sur la liaison RS232

}

void RS485(unsigned char carac) {
    //carac = code(carac);


    DERE = 1; //AUTORISATION TRANSMETTRE
    DX2 = !DX2;

    TXSTA1bits.TXEN = 0;

    RCSTA1bits.CREN = 0; // interdire reception

    TXSTA1bits.TXEN = 1; // autoriser transmission


    TXREG1 = carac;
    while (TXSTA1bits.TRMT == 0); // attend la fin de l'�mission
    TXSTA1bits.TXEN = 0;

    DERE = 0; //AUTORISATION TRANSMETTRE
}

void TRAMERECEPTIONRS485DONNEES(unsigned char tout, char *cNoReception, unsigned char *cTrameEntree) {
    unsigned char u;
    unsigned char tmp;


    tmp = 0;

    do {
ret1:
        u = RECEVOIR_RS485(tout, cNoReception);
        *(cTrameEntree + tmp) = u;
        tmp = tmp + 1;


    } while ((u != FIN_TRAME) && (tmp < 200) && (*cNoReception != 0xff) && (u != '$')); //'$' c la fin
    delay_ms(10); //attention a ne pas enlever

}

void TRAMEENVOIRS485DONNEES(unsigned char *cTrameEnvoyee) {
    unsigned char u;
    unsigned char tmp;
    tmp = 0;

    do {
ret1:

        u = *(cTrameEnvoyee + tmp);
        if (u == 0xFF) // ON INTERDIT L'ENVOIE DE 0xFF �<NUL>235
            u = 'x';
        RS485(u);

        tmp = tmp + 1;
    } while ((u != FIN_TRAME) && (tmp < 200));
}