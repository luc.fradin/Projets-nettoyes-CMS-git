/* 
 * File:   I2C.c
 * Author: ADECEF
 *
 * Created on 10 mai 2021, 15:01
 */

#include <stdio.h>
#include <stdlib.h>
#include "h/CMS.h"


void Init_I2C(void) {
    //here is the I2C setup from the Seeval32 code.
    DDRCbits.RC3 = 1; //Configure SCL as Input
    DDRCbits.RC4 = 1; //Configure SDA as Input
    SSP1STAT = 0x00; //Disable SMBus & Slew Rate Control 80
    SSP1CON1 = 0x28; //Enable MSSP Master  28
    SSP1CON2 = 0x00; //Clear MSSP Conr// 0x3B for 400kHz  (24mhz/((4*4OOkHz))-1 = 14 ou 3B en hexol Bits
    SSP1ADD = 0x3B; //0x0E
}
