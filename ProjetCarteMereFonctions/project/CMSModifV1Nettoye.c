// PIC18F8723 Configuration Bit Settings
// 'C' source line config statements
#include <p18F8723.h> //biblioth�que du Microcontroleur

// CONFIG1H
#pragma config OSC = HS         // Oscillator Selection bits (HS oscillator)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Two-Speed Start-up disabled)
 
// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = SBORDIS  // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
#pragma config BORV = 3         // Brown-out Voltage bits (Minimum setting)

// CONFIG2H
#pragma config WDT = OFF        // Watchdog Timer (WDT disabled (control is placed on the SWDTEN bit))
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3L
#pragma config MODE = MC        // Processor Data Memory Mode Select bits (Microcontroller mode)
#pragma config ADDRBW = ADDR20BIT// Address Bus Width Select bits (20-bit Address Bus)
#pragma config DATABW = DATA16BIT// Data Bus Width Select bit (16-bit External Bus mode)
#pragma config WAIT = OFF       // External Bus Data Wait Enable bit (Wait selections are unavailable for table reads and table writes)

// CONFIG3H
#pragma config CCP2MX = PORTC   // CCP2 MUX bit (ECCP2 input/output is multiplexed with RC1)
#pragma config ECCPMX = PORTE   // ECCP MUX bit (ECCP1/3 (P1B/P1C/P3B/P3C) are multiplexed onto RE6, RE5, RE4 and RE3 respectively)
#pragma config LPT1OSC = OFF    // Low-Power Timer1 Oscillator Enable bit (Timer1 configured for higher power operation)
#pragma config MCLRE = ON       // MCLR Pin Enable bit (MCLR pin enabled; RG5 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
#pragma config LVP = OFF        // Single-Supply ICSP Enable bit (Single-Supply ICSP disabled)
#pragma config BBSIZ = BB2K     // Boot Block Size Select bits (1K word (2 Kbytes) Boot Block size)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled (Legacy mode))

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not code-protected)
#pragma config CP1 = OFF        // Code Protection bit Block 1 (Block 1 (004000-007FFFh) not code-protected)
#pragma config CP2 = OFF        // Code Protection bit Block 2 (Block 2 (008000-00BFFFh) not code-protected)
#pragma config CP3 = OFF        // Code Protection bit Block 3 (Block 3 (00C000-00FFFFh) not code-protected)
#pragma config CP4 = OFF        // Code Protection bit Block 4 (Block 4 (010000-013FFFh) not code-protected)
#pragma config CP5 = OFF        // Code Protection bit Block 5 (Block 5 (014000-017FFFh) not code-protected)
#pragma config CP6 = OFF        // Code Protection bit Block 6 (Block 6 (01BFFF-018000h) not code-protected)
#pragma config CP7 = OFF        // Code Protection bit Block 7 (Block 7 (01C000-01FFFFh) not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Boot Block Code Protection bit (Boot Block (000000-0007FFh) not code-protected)
#pragma config CPD = OFF        // Data EEPROM Code Protection bit (Data EEPROM not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not write-protected)
#pragma config WRT1 = OFF       // Write Protection bit Block 1 (Block 1 (004000-007FFFh) not write-protected)
#pragma config WRT2 = OFF       // Write Protection bit Block 2 (Block 2 (008000-00BFFFh) not write-protected)
#pragma config WRT3 = OFF       // Write Protection bit Block 3 (Block 3 (00C000-00FFFFh) not write-protected)
#pragma config WRT4 = OFF       // Write Protection bit Block 4 (Block 4 (010000-013FFFh) not write-protected)
#pragma config WRT5 = OFF       // Write Protection bit Block 5 (Block 5 (014000-017FFFh) not write-protected)
#pragma config WRT6 = OFF       // Write Protection bit Block 6 (Block 6 (01BFFF-018000h) not write-protected)
#pragma config WRT7 = OFF       // Write Protection bit Block 7 (Block 7 (01C000-01FFFFh) not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) not write-protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block (000000-007FFF, 000FFF or 001FFFh) not write-protected)
#pragma config WRTD = OFF       // Data EEPROM Write Protection bit (Data EEPROM not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection bit Block 1 (Block 1 (004000-007FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR2 = OFF      // Table Read Protection bit Block 2 (Block 2 (008000-00BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR3 = OFF      // Table Read Protection bit Block 3 (Block 3 (00C000-00FFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR4 = OFF      // Table Read Protection bit Block 4 (Block 4 (010000-013FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR5 = OFF      // Table Read Protection bit Block 5 (Block 5 (014000-017FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR6 = OFF      // Table Read Protection bit Block 6 (Block 6 (018000-01BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR7 = OFF      // Table Read Protection bit Block 7 (Block 7 (01C000-01FFFFh) not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Boot Block Table Read Protection bit (Boot Block (000000-007FFF, 000FFF or 001FFFh) not protected from table reads executed in other blocks)



#include <stdlib.h> //biblioth�que
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <i2c.h>
#include <spi.h>
#include "h/CMS.h"

#pragma udata udata1

// MEMOIRE la taille #03301R111122223333BRA/4444/22/1COMMENTAIRESICIROB03326111525001020000456781108650888* soit 86octets
unsigned char TRAMEOUT[125];

unsigned char TRAMEIN[125];

#pragma udata udata2
//MEMOIRE/////////////////////
unsigned char VOIECOD[6];
char TYPE[1];
char CMOD[4];
char CCON[4];
char CREP[4];
char CONSTITUTION[13];
char COMMENTAIRE[20];
char CABLE[3];
char JJ[2];
char MM[2];
char HH[2];
char mm[2];
char ii[1];
char COD[2];
unsigned char POS[2];
char iiii[4];
char DISTANCE[5];
char ETAT[2];
char VALEUR[4];
char SEUIL[4];
char TYPECARTE[5];
//////////////////////////

////// TRAMES IDENTIQUES
char cDebutTrame;
unsigned char ID1[2];
unsigned char ID2[2];
unsigned char FONCTION[3];
unsigned char CRC[5];
char cFinTrame;

//PARTIE DE LISTE


////// TRAMES ERREUR
char TYPE_ERREUR[2];
unsigned char INFORMATION_ERREUR[20];
char NBRECAPTEURS_CABLE;
/////

char PRIO_AUTO_IO; //0x00 rien //0x01
///// TRAME  HORLOGE
unsigned char HORLOGE[12];
/////TRAME EFFACER CAPTEUR
char CAPTEUR_A_EFFACER[5];
/////DEMANDE INFOS A LA CARTE MERE ou demande a la carte relais
unsigned char DEMANDE_INFOS_CAPTEURS[10];
////OEIL
char OEIL_CAPTEUR[5];
////
unsigned char PAGE[6];

unsigned char INSTRUCTION[25]; //pour l'envoi d'instruction

char capt_exist;

float VDD1, VDD2, VDD3, VDD4, VDD5, Vtemp, VDD6, VBAT;

char NORECEPTION;

int pointeur_out_in;
char tmp, tmp2;
int TMPI;

char ENTREES1, ENTREES2;

char HEURE, MINUTE, SECONDE, MOIS, JOUR, ANNEE, DATE;

#pragma udata udata3
unsigned char TMP[125]; // PR SAV DES TRAMES
char P_RESERVOIR[15];
char P_SORTIE[15];
char P_SECOURS[15];
char POINT_ROSEE[11];
char DEBIT_GLOBAL[11];
char TEMPS_CYCLE_MOTEUR[11];
char TEMPS_MOTEUR[15];
char PRESENCE_220V[6];
char PRESENCE_48V[6];

unsigned char DATAFIX[10];

#pragma udata udata4
unsigned char SAVTAB[25];
//#pragma udata udata5
//unsigned char tab[120];
int calculBLOC;
char FONCTION_ACTIVATION_AUTO;
char numdecarteRI;
char TROPDENONREPONSE;

char CRC_ETAT;
char ETAT_IO;
char FIRST_DEMAR; //PREMIER DEMAR DU  CMS MISE EN ALIM

char MODERAPID;

char TYPECOMMANDE;
char temoinfinvoieZIZ;
char IOTRAVAIL;

char PASDE_S;
char cycleN; //CYCLE 1 DIC CYCLE 2 ZIZ
char RESCRUTATION;
char NUMEROCARTE;


char NBREDEMANDEIO;

char PRESENCEDEBITSD; //POURSAV SD VERS CM

char R1NOREP, R2NOREP, R3NOREP, R4NOREP, R5NOREP;
char UNRESETRI1, UNRESETRI2, UNRESETRI3, UNRESETRI4, UNRESETRI5;
char MINIRESETRI;

char TYPEDECARTEOEIL;
int TIMEOUTCYCLE;


char scrutationunevoie;
char ETATZIZCRATIONAUTOMATE;

char mouchardINT0;
char DEMANDEZIGIO;

char resetheurefixe;
unsigned char SAVAUTOCYCLE;
char MAXBITE;
char MAJBLOC;
char DEMANDEZPLIO;
char ERRMM, NBMM;
char FCYCLE, TESTCYCLE;

//RS485
//#define DERE PORTAbits.RA4 // direction RX TX
//#define RX PORTCbits.RC7 // RX
//#define INT0 PORTBbits.RB0 // INT0
//#define INT1 PORTBbits.RB1 // INT1
//#define INT2 PORTBbits.RB2 // INT2
//
//#define I2C_MEMOIRE PORTJbits.RJ1
//#define I2C_HORLOGE PORTJbits.RJ0
//#define I2C_INTERNE PORTJbits.RJ2
//
//#define CHARGE_ON PORTEbits.RE1
//
//
//#define DX10 PORTHbits.RH0
//#define DX20 PORTHbits.RH1
//
//
//#define DX1 PORTHbits.RH2
//#define DX2 PORTHbits.RH3
//
//#define STATO PORTJbits.RJ3
//#define STAT PORTEbits.RE7
//
////BASCULE
//#define SHLD PORTDbits.RD0
//#define CLK PORTDbits.RD3
//#define INH PORTDbits.RD2
//
//#define IO_EN_COM PORTHbits.RH4
//
//
//#define DRS485_R1 PORTFbits.RF3
//#define DRS485_R2 PORTFbits.RF4
//#define DRS485_R3 PORTFbits.RF5
//#define DRS485_R4 PORTFbits.RF6
//#define DRS485_R5 PORTFbits.RF7
//
//
//#define CYCLE_R1 PORTJbits.RJ4
//#define CYCLE_R2 PORTJbits.RJ5
//#define CYCLE_R4 PORTJbits.RJ6
//#define CYCLE_R3 PORTJbits.RJ7
//#define CYCLE_R5 PORTCbits.RC2
//
//
//#define DRS485_IO PORTBbits.RB4 /////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//
//#define RIPOWER PORTHbits.RH6
//
//#define RESETCMS PORTHbits.RH5
//////////////////////////////////////////////////
// sous routine d'interruptionADCON2

#pragma code HighVector=0x18

void HighVector(void) {
    _asm GOTO inter _endasm
}
#pragma code // return to default code section

#pragma interrupt inter

void inter(void) {
    DX1 = 0;
    INTCONbits.GIE = 0;
    if (INTCONbits.INT0IF == 1) // R�ception de donn�es sur le port B
    {
        INTCONbits.INT0IF = 0; // met le drapeau d'IT � 0
        mouchardINT0 = 1;
        identificationINTER('F', 'F');
        delay_ms(100);
    }

    DX1 = 1;
    mouchardINT0 = 0;
    DX2 = 1;
}

void main(void) {

    PRIO_AUTO_IO = 0x00;
    TRISBbits.TRISB0 = 1;
    MAJBLOC = 'Y';
    TRISCbits.TRISC5 = 0; //ALIMENTATION CDE 12V IO
    DEMANDEZPLIO = 0;
    INTCONbits.GIE = 1; //AUTORISER LES INTERRUPTIONS GENERALES
    INTCONbits.INT0IE = 1; //Enable RB0/INT
    RCONbits.IPEN = 1; //Disable all unmasked peripheral interrupt
    SSPADD = 49; //fr�quence 100KHz pour 20Mhz
    INTCON2bits.RBPU = 1; //PUPPULL
    OpenI2C(MASTER, SLEW_OFF);
    TRISCbits.TRISC4 = 1; //SDA
    TRISCbits.TRISC3 = 1; //SCL

    TRISJbits.TRISJ0 = 0; // I2C HORLOGE
    TRISJbits.TRISJ1 = 0; //I2C MEMOIRE
    TRISJbits.TRISJ2 = 0; //I2C INTERNE

    TRISJbits.TRISJ4 = 1; //CYCLE R1
    TRISJbits.TRISJ5 = 1; //CYCLE R2
    TRISJbits.TRISJ6 = 1; //CYCLE R4
    TRISJbits.TRISJ7 = 1; //CYCLE R3

    TRISCbits.TRISC2 = 1; //CYCLE R5

    TRISHbits.TRISH0 = 0;
    TRISHbits.TRISH1 = 0;

    TRISHbits.TRISH2 = 0;
    TRISHbits.TRISH3 = 0;
    TRISHbits.TRISH4 = 1;
    TRISHbits.TRISH5 = 0;
    TRISHbits.TRISH6 = 0;

    TRISEbits.TRISE1 = 0;

    TRISEbits.TRISE7 = 1;
    TRISJbits.TRISJ3 = 0;

    TRISBbits.TRISB0 = 1;
    TRISBbits.TRISB1 = 1;
    TRISBbits.TRISB2 = 1;

    TRISDbits.TRISD0 = 0;
    TRISDbits.TRISD2 = 0;
    TRISDbits.TRISD3 = 0;

    TRISFbits.TRISF3 = 0; //SORTIE DEMANDE VERS R1 .....
    TRISFbits.TRISF4 = 0; //SORTIE DEMANDE VERS R2 .....
    TRISFbits.TRISF5 = 0; //SORTIE DEMANDE VERS R3 .....
    TRISFbits.TRISF6 = 0; //SORTIE DEMANDE VERS R4 .....
    TRISFbits.TRISF7 = 0; //SORTIE DEMANDE VERS R5 .....
    TRISCbits.TRISC0 = 0; //SORTIE AUvsCM
    TRISBbits.TRISB4 = 0;
    FIRST_DEMAR = 0xFF;
    ETAT_IO = 0xFF; //LIO PEUT PARLER
    ADCON1 = 0x07; // AN0 a AN7
    ADCON2 = 0xB8; //DROITE ET TEMPS CAN
    RESETCMS = 0;
    DRS485_R1 = 0;
    Init_I2C();

    DX1 = 1;
    DX2 = 1;
    DX10 = 0;
    DX20 = 0;



    cDebutTrame = '#';
    cFinTrame = '*';
    PORTCbits.RC5 = 0; // ON LAISSE ETEINT LA CARTE IO
    MINIRESETRI = 0x00;
    UNRESETRI1 = pic_eeprom_read(0x01);
    if (UNRESETRI1 == 0xAA) //VRAI RESET heure ou a distance CONDITION NORMALE changement carte ou ......
    {
        R1NOREP = 0;
        UNRESETRI1 = 0;
    }
    if (UNRESETRI1 == 0x33) //CARTE RI4 TROIS RESET
    {
        R1NOREP = 12; //ON EMPECHE L'INTERROGATION CODE 12
        pic_eeprom_write(0x21, 'd'); //METTRE BLOQUE
    } else {
        R1NOREP = 0; //ON TENTE ENCIRE UNE FOIS
    }
    if ((UNRESETRI1 >= 1)&&(UNRESETRI1 != 0x33)) //CARTE RI1 RESET une deux ou 3 fois
    {
        MINIRESETRI = pic_eeprom_read(0x21); //voir la valeur
        if ((MINIRESETRI == 0xFF) || (MINIRESETRI == 98)) //SI VIERGE METTRE INIT A ZERO
            MINIRESETRI = 0;

        MINIRESETRI++;
        pic_eeprom_write(0x21, MINIRESETRI); //UN RESET AUTO EN PLUS ENREGISTRE
        MINIRESETRI = 0xFF;
    }


    UNRESETRI2 = pic_eeprom_read(0x02);
    if (UNRESETRI2 == 0xAA) //VRAI RESET heure ou a distance CONDITION NORMALE changement carte ou ......
    {
        R2NOREP = 0;
        UNRESETRI2 = 0;
    }
    if (UNRESETRI2 == 0x33) //CARTE RI4 TROIS RESET
    {
        pic_eeprom_write(0x22, 'd'); //METTRE BLOQUE
        R2NOREP = 12; //ON EMPECHE L'INTERROGATION CODE 12
    } else {
        R2NOREP = 0; //ON TENTE ENCIRE UNE FOIS
    }
    if ((UNRESETRI2 >= 1)&&(UNRESETRI2 != 0x33)) //CARTE RI1 RESET une deux ou 3 fois
    {
        MINIRESETRI = pic_eeprom_read(0x22); //voir la valeur
        if ((MINIRESETRI == 0xFF) || (MINIRESETRI == 98)) //SI VIERGE METTRE INIT A ZERO
            MINIRESETRI = 0;
        MINIRESETRI++;
        pic_eeprom_write(0x22, MINIRESETRI); //UN RESET AUTO EN PLUS ENREGISTRE
        MINIRESETRI = 0xFF;
    }


    UNRESETRI3 = pic_eeprom_read(0x03);
    if (UNRESETRI3 == 0xAA) //VRAI RESET heure ou a distance CONDITION NORMALE changement carte ou ......
    {
        R3NOREP = 0;
        UNRESETRI3 = 0;
    }
    if (UNRESETRI3 == 0x33) //CARTE RI4 TROIS RESET
    {
        R3NOREP = 12; //ON EMPECHE L'INTERROGATION CODE 12
        pic_eeprom_write(0x23, 'd'); //METTRE BLOQUE
    } else {
        R3NOREP = 0; //ON TENTE ENCIRE UNE FOIS
    }
    if ((UNRESETRI3 >= 1)&&(UNRESETRI3 != 0x03)) //CARTE RI1 RESET une deux ou 3 fois ET PAS bloqu�e
    {
        MINIRESETRI = pic_eeprom_read(0x23); //voir la valeur
        if ((MINIRESETRI == 0xFF) || (MINIRESETRI == 98)) //SI VIERGE METTRE INIT A ZERO
            MINIRESETRI = 0;
        MINIRESETRI++;
        pic_eeprom_write(0x23, MINIRESETRI); //UN RESET AUTO EN PLUS ENREGISTRE
        MINIRESETRI = 0xFF;
    }


    UNRESETRI4 = pic_eeprom_read(0x04);
    if (UNRESETRI4 == 0xAA) //VRAI RESET heure ou a distance CONDITION NORMALE changement carte ou ......
    {
        R4NOREP = 0;
        UNRESETRI4 = 0;
    }
    if (UNRESETRI4 == 0x33) //CARTE RI4 TROIS RESET
    {
        R4NOREP = 12; //ON EMPECHE L'INTERROGATION CODE 12
        pic_eeprom_write(0x24, 'd'); //METTRE BLOQUE
    } else {
        R4NOREP = 0; //ON TENTE ENCIRE UNE FOIS
    }
    if ((UNRESETRI4 >= 1)&&(UNRESETRI4 != 0x33)) //CARTE RI1 RESET une deux ou 3 fois
    {
        MINIRESETRI = pic_eeprom_read(0x24); //voir la valeur
        if ((MINIRESETRI == 0xFF) || (MINIRESETRI == 98)) //SI VIERGE METTRE INIT A ZERO
            MINIRESETRI = 0;
        MINIRESETRI++;
        pic_eeprom_write(0x24, MINIRESETRI); //UN RESET AUTO EN PLUS ENREGISTRE
        MINIRESETRI = 0xFF;
    }


    UNRESETRI5 = pic_eeprom_read(0x05);
    if (UNRESETRI5 == 0xAA) //VRAI RESET heure ou a distance CONDITION NORMALE changement carte ou ......
    {
        R5NOREP = 0;
        UNRESETRI5 = 0;
    }
    if (UNRESETRI5 == 0x33) //CARTE RI4 TROIS RESET
    {
        R5NOREP = 12; //ON EMPECHE L'INTERROGATION CODE 12
        pic_eeprom_write(0x25, 'd'); //METTRE BLOQUE
    } else {
        R5NOREP = 0; //ON TENTE ENCIRE UNE FOIS
    }
    if ((UNRESETRI5 >= 1)&&(UNRESETRI5 != 0x33)) //CARTE RI1 RESET une deux ou 3 fois
    {
        MINIRESETRI = pic_eeprom_read(0x25); //voir la valeur
        if ((MINIRESETRI == 0xFF) || (MINIRESETRI == 98)) //SI VIERGE METTRE INIT A ZERO
            MINIRESETRI = 0;
        MINIRESETRI++;
        pic_eeprom_write(0x25, MINIRESETRI); //UN RESET AUTO EN PLUS ENREGISTRE
        MINIRESETRI = 0xFF;
    }

    DX10 = 1;
    DX20 = 1;
    while (1) // boucle infinie
    {
        ERRMM = 0;

        DRS485_R1 = 1;
        DRS485_R2 = 1;
        DRS485_R3 = 1;
        DRS485_R4 = 1;
        DRS485_R5 = 1;
        SAVAUTOCYCLE = 0;
        FCYCLE = 0;
        TESTCYCLE = 0;

        TRAMEIN[0] = DEBUT_TRAME; //#
        TRAMEIN[1] = '0';
        TRAMEIN[2] = '0';
        TRAMEIN[3] = '9';

        DRS485_R1 = 0;
        DRS485_R2 = 0;
        DRS485_R3 = 0;
        DRS485_R4 = 0;
        DRS485_R5 = 0;

        TRAMEIN[4] = '1';
        TRAMEIN[5] = '5';
        TRAMEIN[6] = 'A';
        TRAMEIN[7] = '1';
        TRAMEIN[8] = '1';
        TRAMEIN[9] = '1';
        TRAMEIN[10] = '1';
        TRAMEIN[11] = '2';
        TRAMEIN[12] = '2';
        TRAMEIN[13] = '2';
        TRAMEIN[14] = '2';
        TRAMEIN[15] = '0';
        TRAMEIN[16] = '0';
        TRAMEIN[17] = '0';
        TRAMEIN[18] = '0';
        TRAMEIN[19] = 'B';
        TRAMEIN[20] = 'R';
        TRAMEIN[21] = 'A';
        TRAMEIN[22] = '/';
        TRAMEIN[23] = '8';
        TRAMEIN[24] = '8';
        TRAMEIN[25] = '8';
        TRAMEIN[26] = '8';
        TRAMEIN[27] = '/';
        TRAMEIN[28] = '2';
        TRAMEIN[29] = '2';
        TRAMEIN[30] = '/';
        TRAMEIN[31] = '1';
        TRAMEIN[32] = 'C';
        TRAMEIN[33] = 'O';
        TRAMEIN[34] = 'M';
        TRAMEIN[35] = 'M';
        TRAMEIN[36] = 'E';
        TRAMEIN[37] = 'N';
        TRAMEIN[38] = 'T';
        TRAMEIN[39] = 'A';
        TRAMEIN[40] = 'I';
        TRAMEIN[41] = 'R';
        TRAMEIN[42] = 'E';
        TRAMEIN[43] = 'S';
        TRAMEIN[44] = 'I';
        TRAMEIN[45] = 'C';
        TRAMEIN[46] = 'I';
        TRAMEIN[47] = 'R';
        TRAMEIN[48] = 'O';
        TRAMEIN[49] = 'B';
        TRAMEIN[50] = '9';
        TRAMEIN[51] = '9';
        TRAMEIN[52] = '3';
        TRAMEIN[53] = '2';
        TRAMEIN[54] = '6';
        TRAMEIN[55] = '1';
        TRAMEIN[56] = '1';
        TRAMEIN[57] = '1';
        TRAMEIN[58] = '5';
        TRAMEIN[59] = '2';
        TRAMEIN[60] = '5';
        TRAMEIN[61] = '0';
        TRAMEIN[62] = '0';
        TRAMEIN[63] = '1';
        TRAMEIN[64] = '0';
        TRAMEIN[65] = '2';
        TRAMEIN[66] = '0';
        TRAMEIN[67] = '0';
        TRAMEIN[68] = '0';
        TRAMEIN[69] = '0';
        TRAMEIN[70] = '4';
        TRAMEIN[71] = '5';
        TRAMEIN[72] = '6';
        TRAMEIN[73] = '7';
        TRAMEIN[74] = '8';
        TRAMEIN[75] = '0';
        TRAMEIN[76] = '1';
        TRAMEIN[77] = '0';
        TRAMEIN[78] = '8';
        TRAMEIN[79] = '6';
        TRAMEIN[80] = '5';
        TRAMEIN[81] = '0';
        TRAMEIN[82] = '8';
        TRAMEIN[83] = '8';
        TRAMEIN[84] = '8';
        TRAMEIN[85] = '2';
        TRAMEIN[86] = '2';
        TRAMEIN[87] = '7';
        TRAMEIN[88] = '6';
        TRAMEIN[89] = '7';
        TRAMEIN[90] = FIN_TRAME;
        TRAMEIN[91] = '-';
        TRAMEIN[92] = '-';
        TRAMEIN[93] = '-';
        TRAMEIN[94] = '-';
        TRAMEIN[95] = '-';
        TRAMEIN[96] = '-';
        TRAMEIN[97] = '-';
        TRAMEIN[98] = '-';
        TRAMEIN[99] = '-';
        TRAMEIN[100] = '-';
        TRAMEIN[101] = '-';
        TRAMEIN[102] = '-';
        TRAMEIN[103] = '-';
        TRAMEIN[104] = '-';
        TRAMEIN[105] = '-';
        TRAMEIN[106] = '-';
        TRAMEIN[107] = '-';
        TRAMEIN[108] = '-';
        TRAMEIN[109] = '-';
        TRAMEIN[110] = '-';
        TRAMEIN[111] = '-';
        TRAMEIN[112] = '-';
        TRAMEIN[113] = '-';
        TRAMEIN[114] = '-';
        TRAMEIN[115] = '-';
        TRAMEIN[116] = '-';
        TRAMEIN[117] = '-';
        TRAMEIN[118] = '-';
        TRAMEIN[119] = '-';
        TRAMEIN[120] = '-';
        TRAMEIN[121] = '-';
        TRAMEIN[122] = '-';
        TRAMEIN[123] = '-';
        TRAMEIN[124] = '-';




        I2C_MEMOIRE = 0;
        I2C_HORLOGE = 0;
        I2C_INTERNE = 1; //ACTIVATION I2C INTERNE

        tmp = LIRE_HORLOGE(0); //INIT HORLOGE
        creer_trame_horloge(HORLOGE); // ON LIT LA DATE DANS L'HORLOGE


        STATO = 1; // LES DEUX POUR ETAT CHARGE

        STATO = 0;

        Init_RS485();
        SERIALISATION(&ENTREES1, &ENTREES2);

        I2C_MEMOIRE = 0; //AUTORISER ACCES MEMOIRE INTERNE
        I2C_HORLOGE = 0; //INTERDIRE ACCES HORLOGE INTERNE
        I2C_INTERNE = 1; //INTERDIRE ACCES I2C EXTERNE
        TMPI = 0;
        INTCONbits.GIE = 1; //AUTORISER LES INTERRUPTIONS GENERALES

        tmp = LIRE_HORLOGE(0);
        tmp = ECRIRE_EEPROMBYTE(choix_memoire(TRAMEIN), calcul_addmemoire(TRAMEIN), TRAMEIN, 1); //ECRITURE EN EEPROM
        tmp = LIRE_EEPROM(choix_memoire(TRAMEIN), calcul_addmemoire(TRAMEIN), TRAMEOUT, 1); //LIRE DANS EEPROM
        delay_us(1);

        creer_trame_horloge(HORLOGE); //ON LIT L'HEURE DE CM
L:

        SAVAUTOCYCLE = SAVAUTOCYCLE + 1;

        cycleN = 1; //CYCLE 1 LES DIC ET LES TEC

        PRIO_AUTO_IO = 0x00; //REPOS NI L'AUTO NI LIO EST DANS UNE FOCNTION
        MESURE_DES_TENSIONS(&VDD1, &VDD2, &VDD3, &VDD4, &VDD5, &VBAT);
        CONTROLE_CHARGE_BATT(VBAT); //GESTION DE LA CHARGE BATTERIE
        delay_ms(1);
        PORTDbits.RD7 = 1;
        PORTDbits.RD7 = 0; // Enable Chip Select ANCIENNEMENT CARTE SD
        tmp = 0;
        LIRE_EEPROM(0, TMPI, TRAMEOUT, 1); //LIRE DANS EEPROM
        delay_ms(4); //indispensable entre erciture et lecture
        TRAMEIN[5] = TRAMEIN[5] + 1;
        if (TRAMEIN[5] == ':') {
            TRAMEIN[5] = '0';
            TRAMEIN[4] = '1';
        }
        if (TRAMEIN[4] == '1') {
            if (TRAMEIN[5] == '7') {
                TRAMEIN[5] = '0';
                TRAMEIN[4] = '0';
                TMPI = TMPI + 1;
                IntToChar(TMPI, PAGE, 3);
                TRAMEIN[1] = PAGE[0];
                TRAMEIN[2] = PAGE[1];
                TRAMEIN[3] = PAGE[2];


            }
        }

        resetTO(TRAMEOUT);
        DRS485_IO = 0; //REPOS DE LA DEMANDE DE SESSION DE L'AUTOMATE POUR DEMANDER A LA CARTE IO LA PAIX

        TEMPO(3); // pour 30 on a 33s donc 100ms delay_ms enriron 110ms
        MESURE_DES_TENSIONS(&VDD1, &VDD2, &VDD3, &VDD4, &VDD5, &VBAT);
        CONTROLE_CHARGE_BATT(VBAT);
        creer_trame_horloge(HORLOGE); //ON LIT L'HEURE DE CM
        reglerhorlogeautres(HORLOGE, VDD1, VDD2, VDD3, VDD4, VDD5, VBAT, R1NOREP, R2NOREP, R3NOREP, R4NOREP, R5NOREP); //20012020

        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI PAS PREMIER DEMAR et IO(RB4->1) est AUTORISEE A MARCHER ON LA DESACTIVE
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;
        }

        if (MINIRESETRI == 0xFF)
            goto cy2; // ON VA DIRECTEMENT A LA CARTe IO CAR MINI RESET AUTO
        if ((VDD1 > 2)&&(R1NOREP <= 4)) //SI REPONSE  DE LA CARTE RI1(apres plusieurs tentatives) sinon ne l'interroge plus
        {
            TEMPO(2);
            ENVOIE_DES_TRAMES(1, 304, INSTRUCTION); //EXISTENCE COMPTER 4Secondes PAR CARTE

        }




        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI PAS PREMIER DEMAR et IO(RB4->1) est AUTORISEE A MARCHER ON LA DESACTIVE
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }

        MESURE_DES_TENSIONS(&VDD1, &VDD2, &VDD3, &VDD4, &VDD5, &VBAT);
        CONTROLE_CHARGE_BATT(VBAT);

        if ((R1NOREP >= 4)&&(R1NOREP < 12)) //R1 NA PAS REPONDU 4X pour la fonction TABLE EXISTENCE!!!!
        {
            if (UNRESETRI1 >= 2) //SI une resetRI a deja ete fait
            {
                R1NOREP = 12; //ON met cette valeur a 12 comme cela la carte n'est plus interrog�e
                pic_eeprom_write(0x01, 0x33); //DEUX RESET
            } else //si aucun resetRI n'a ete fait on fait un reset donc le RESETri EST FAIT UNE SEUL FOIS
            {
                UNRESETRI1++; //ON MEMORISE LE RESET 1 APRES ELLE EST ZAPPEE mettre dans emplacement eeprom celui ci sera a zero seulement avec un vrai reset
                delay_ms(300);
                pic_eeprom_write(0x01, UNRESETRI1); //UN RESET							//METTRE INDICATION DANS EEPROOM QU'IL SAGIT DUN RESET RI OU UTILISER RESETCMS !!!!!!!!
                RESETCMS = 1; //ON REINIT
            }
        }



        //------------------------------------------------------------------------------TABLE EXISTENCE RI2

        if ((VDD2 > 2)&&(R2NOREP <= 4)) {
            TEMPO(3);
            ENVOIE_DES_TRAMES(2, 304, INSTRUCTION); //EXISTENCE COMPTER 4Secondes PAR CARTE

        }

        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI PAS PREMIER DEMAR et IO(RB4->1) est AUTORISEE A MARCHER ON LA DESACTIVE
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }
        MESURE_DES_TENSIONS(&VDD1, &VDD2, &VDD3, &VDD4, &VDD5, &VBAT);
        CONTROLE_CHARGE_BATT(VBAT);


        if ((R2NOREP >= 4)&&(R2NOREP < 12)) {

            if (UNRESETRI2 >= 2) //SI DEUXIEME FOIS REPOND PAS APRES RESET ON LINTERDIT
            {
                R2NOREP = 12;
                pic_eeprom_write(0x02, 0x33); //TROISIEME RESET
            } else {
                UNRESETRI2++; //ON MEMORISE LE RESET 1 APRES ELLE EST ZAPPEE
                delay_ms(300);
                pic_eeprom_write(0x02, UNRESETRI2); //UN RESET puis deux jusqu'a condition plus haute  ici 3
                RESETCMS = 1; //RESET RI;
            }
        }


        //------------------------------------------------------------------------------TABLE EXISTENCE RI3

        if ((VDD3 > 2)&&(R3NOREP <= 4)) {
            TEMPO(3);
            ENVOIE_DES_TRAMES(3, 304, INSTRUCTION); //EXISTENCE COMPTER 4Secondes PAR CARTE
        }

        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI PAS PREMIER DEMAR et IO(RB4->1) est AUTORISEE A MARCHER ON LA DESACTIVE
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }


        MESURE_DES_TENSIONS(&VDD1, &VDD2, &VDD3, &VDD4, &VDD5, &VBAT);
        CONTROLE_CHARGE_BATT(VBAT);



        if ((R3NOREP >= 4)&&(R3NOREP < 12)) {

            if (UNRESETRI3 >= 2) //SI DEUXIEME FOIS REPOND PAS APRES RESET ON LINTERDIT
            {
                R3NOREP = 12;
                pic_eeprom_write(0x03, 0x33); //TROISIEME RESET
            } else {
                UNRESETRI3++; //ON MEMORISE LE RESET 1 APRES ELLE EST ZAPPEE
                delay_ms(300);
                pic_eeprom_write(0x03, UNRESETRI3); //UN RESET puis deux jusqu'a condition plus haute  ici 3
                RESETCMS = 1; //RESET RI
            }
        }



        //------------------------------------------------------------------------------TABLE EXISTENCE RI4



        if ((VDD4 > 2)&&(R4NOREP <= 4)) {
            TEMPO(3);
            ENVOIE_DES_TRAMES(4, 304, INSTRUCTION); //EXISTENCE COMPTER 4Secondes PAR CARTE

        }



        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI PAS PREMIER DEMAR et IO(RB4->1) est AUTORISEE A MARCHER ON LA DESACTIVE
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }


        MESURE_DES_TENSIONS(&VDD1, &VDD2, &VDD3, &VDD4, &VDD5, &VBAT);
        CONTROLE_CHARGE_BATT(VBAT);


        if ((R4NOREP >= 4)&&(R4NOREP < 12)) {

            if (UNRESETRI4 >= 2) //SI TROISIEME FOIS REPOND PAS APRES RESET ON LINTERDIT
            {
                R4NOREP = 12;
                pic_eeprom_write(0x04, 0x33); //TROISIEME RESET
            } else {
                UNRESETRI4++; //ON MEMORISE LE RESET 1 APRES ELLE EST ZAPPEE
                delay_ms(300);
                pic_eeprom_write(0x04, UNRESETRI4); //UN RESET puis deux jusqu'a condition plus haute  ici 3
                RESETCMS = 1; //RESET RI
            }
        }




        //------------------------------------------------------------------------------TABLE EXISTENCE RI5

        if ((VDD5 > 2)&&(R5NOREP <= 4)) {
            TEMPO(3);
            ENVOIE_DES_TRAMES(5, 304, INSTRUCTION); //EXISTENCE COMPTER 4Secondes PAR CARTE

        }



        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI PAS PREMIER DEMAR et IO(RB4->1) est AUTORISEE A MARCHER ON LA DESACTIVE
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }

        if ((R5NOREP >= 4)&&(R5NOREP < 12)) {

            if (UNRESETRI5 >= 2) //SI DEUXIEME FOIS REPOND PAS APRES RESET ON LINTERDIT
            {
                R5NOREP = 12;
                pic_eeprom_write(0x05, 0x33); //TROISIEME RESET
            } else {
                UNRESETRI5++; //ON MEMORISE LE RESET 1 APRES ELLE EST ZAPPEE

                delay_ms(300);
                pic_eeprom_write(0x05, UNRESETRI5); //UN RESET puis deux jusqu'a condition plus haute  ici 3
                RESETCMS = 1; //RESET RI
            }
        }


        //------------------------------------------------------------------------------DONNEES RI1
        DATAFIX[0] = '0';
        DATAFIX[1] = '0';
        DATAFIX[2] = '1';
        DATAFIX[3] = '0';
        DATAFIX[4] = '0';

        DATAFIX[5] = '0';
        DATAFIX[6] = '2';
        DATAFIX[7] = '0';
        DATAFIX[8] = '1';
        DATAFIX[9] = '6'; // il faut aller jusqua cela sinon 16 non incl

        MODERAPID = 1; // / ou -

        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI PAS PREMIER DEMAR et IO(RB4->1) est AUTORISEE A MARCHER ON LA DESACTIVE
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }

        MODERAPID = 1; // / ou -
        MESURE_DES_TENSIONS(&VDD1, &VDD2, &VDD3, &VDD4, &VDD5, &VBAT);
        CONTROLE_CHARGE_BATT(VBAT);
        if ((VDD1 > 2)&&(R1NOREP <= 4)) {
            TEMPO(1);
            ENVOIE_DES_TRAMES(1, 276, DATAFIX); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE
            PRIO_AUTO_IO = 0x00; //!!!!!!!!!!!


        }
        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI PAS PREMIER DEMAR et IO(RB4->1) est AUTORISEE A MARCHER ON LA DESACTIVE
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }





        //------------------------------------------------------------------------------DONNEES RI2
        MODERAPID = 1; // / ou -
        MESURE_DES_TENSIONS(&VDD1, &VDD2, &VDD3, &VDD4, &VDD5, &VBAT);
        CONTROLE_CHARGE_BATT(VBAT);



        if ((VDD2 > 2)&&(R2NOREP <= 4)) {
            TEMPO(1);
            ENVOIE_DES_TRAMES(2, 276, DATAFIX); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE
            PRIO_AUTO_IO = 0x00;


        }

        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI IO EST ACTIVE (REA ON)
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }




        //------------------------------------------------------------------------------DONNEES RI3

        MODERAPID = 1; // / ou -
        MESURE_DES_TENSIONS(&VDD1, &VDD2, &VDD3, &VDD4, &VDD5, &VBAT);
        CONTROLE_CHARGE_BATT(VBAT);
        if ((VDD3 > 2)&&(R3NOREP <= 4)) {
            TEMPO(1);
            ENVOIE_DES_TRAMES(3, 276, DATAFIX); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE

            PRIO_AUTO_IO = 0x00;

        }

        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI IO EST ACTIVE (REA ON)
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }



        //------------------------------------------------------------------------------DONNEES RI4

        MODERAPID = 1; // / ou -
        MESURE_DES_TENSIONS(&VDD1, &VDD2, &VDD3, &VDD4, &VDD5, &VBAT);
        CONTROLE_CHARGE_BATT(VBAT);
        if ((VDD4 > 2)&&(R4NOREP <= 4)) {
            TEMPO(1);
            ENVOIE_DES_TRAMES(4, 276, DATAFIX); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE

            PRIO_AUTO_IO = 0x00;

        }


        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI IO EST ACTIVE (REA ON)
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }




        //------------------------------------------------------------------------------DONNEES RI5
        MODERAPID = 1; // / ou -
        MESURE_DES_TENSIONS(&VDD1, &VDD2, &VDD3, &VDD4, &VDD5, &VBAT);
        CONTROLE_CHARGE_BATT(VBAT);
        if ((VDD5 > 2)&&(R5NOREP <= 4)) {
            TEMPO(1);
            ENVOIE_DES_TRAMES(5, 276, DATAFIX); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE

            PRIO_AUTO_IO = 0x00;

        }

fintest:
        TEMPO(2);


cy2:
        cycleN = 2; //DEUXIEME CYCLE ZIZ ET ZTE ET ZAG ET DEMANDE DE L'IO



        if (IO_EN_COM == 0) // LA CARTE IO NE DIALOGUE PAS AVEC LE MODEM
        {
            if (FIRST_DEMAR == 0XFF){ //SI PREMIER DEMMARAGE ON INIT LIO
            
                PORTCbits.RC5 = 1;
                delay_ms(100);
                PORTCbits.RC5 = 0;
                delay_ms(100);
                PORTCbits.RC5 = 1;
                IOTRAVAIL = 1;
WAITING1:

                PASDE_S = 0;
                if ((IOTRAVAIL == 1)&&(PASDE_S < 5)) {

                    IOTRAVAIL = 0;
                    if (MINIRESETRI != 0xFF) {
                        TEMPO(15); //INIT PAS LES DONNEES
                    } else {
                        TEMPO(15);
                    }
                    PASDE_S++;
                    if ((DEMANDEZPLIO < 2)&&(PASDE_S < 5)){
                        goto WAITING1;
                    }
                }



                if (DEMANDEZPLIO == 0x02){ //FIN CYCLE 2 PARAM IO2iemex DEM
                    DEMANDEZPLIO = 0x00;
                }

                if (ETAT_IO == 0xFF){ //SI IO EST ACTIVE (REA ON)
                    TEMPO(1);
                    DRS485_IO = 1; //ON DESACTIVE POUR DEMANDER INFO une deuxieme fois
                    TEMPO(1);
                    DRS485_IO = 0;
                    ETAT_IO = 0x00;
                }
                FIRST_DEMAR = 0x00;
                goto fintest2;
            } else {
                if (ETAT_IO == 0xFF){ //SI IO EST ACTIVE (REA ON)
                    TEMPO(1);
                    DRS485_IO = 1; //ON DESACTIVE
                    TEMPO(1);
                    DRS485_IO = 0;
                    ETAT_IO = 0x00;
                    goto finio;
                }
                if (ETAT_IO == 0x00){ //SI IO EST DESACTIVE (REA oFF)
                    TEMPO(1);
                    DRS485_IO = 1; //ON ACTIVE POUR DEMANDER INFO
                    TEMPO(1);
                    DRS485_IO = 0;
                    ETAT_IO = 0xFF;
                    //ON ATTEND UNE SCRUTATION COMPLETE OU LE TEMPS DES ZIZ
                    //ON ATTEND UNE SCRUTATION COMPLETE OU LE TEMPS DES ZIZ
                    PASDE_S = 0;
                    IOTRAVAIL = 1;

WAITING2:





                    if ((IOTRAVAIL == 1)&&(PASDE_S < 10)){ //ON ATTEND 10x la tempo
                        IOTRAVAIL = 0;
                        TEMPO(15); //TRES IMPORTANT IL NE FAUT PAS QUE IO DEMANDE UN TRUC A VANT
                        PASDE_S++;
                        if ((DEMANDEZIGIO == 0x00)&&(PASDE_S < 10)){
                            goto WAITING2;
                        }
                    }
                    TRAMEOUT[3] = 'A'; //
                    TRAMEOUT[4] = 'U';
                    TRAMEOUT[5] = 'I';
                    TRAMEOUT[6] = 'N';
                    TRAMEOUT[7] = 'I';
                    TRAMEOUT[8] = 'T';
                    TRAMEOUT[9] = FIN_TRAME;
                    TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A LA CARTE CONCERNEE
                    TEMPO(3);
                    if (DEMANDEZIGIO == 0xFF){ //FIN CYCLE 2 GROUPE
                        DEMANDEZIGIO = 0x00;
                    }





                    if (ETAT_IO == 0xFF){ //SI IO EST ACTIVE (REA ON)
                        DRS485_IO = 1; //ON DESACTIVE
                        TEMPO(1);
                        DRS485_IO = 0;
                        ETAT_IO = 0x00;
                    }
                    goto fintest2;


                }
            }

        } else {
            TEMPO(1);
        }




finio:
        TEMPO(1); //MINIMUM 32S temps une carte RI (30=33S)
        MESURE_DES_TENSIONS(&VDD1, &VDD2, &VDD3, &VDD4, &VDD5, &VBAT);
        CONTROLE_CHARGE_BATT(VBAT);

fintest2:
        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI PAS PREMIER DEMAR et IO(RB4->1) est AUTORISEE A MARCHER ON LA DESACTIVE
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }

        if ((VDD1 > 2)&&(R1NOREP <= 4))
            TESTCYCLE = TESTCYCLE + 1;
        if ((VDD2 > 2)&&(R2NOREP <= 4))
            TESTCYCLE = TESTCYCLE + 2;
        if ((VDD3 > 2)&&(R3NOREP <= 4))
            TESTCYCLE = TESTCYCLE + 4;
        if ((VDD4 > 2)&&(R4NOREP <= 4))
            TESTCYCLE = TESTCYCLE + 8;
        if ((VDD5 > 2)&&(R5NOREP <= 4))
            TESTCYCLE = TESTCYCLE + 16;
        TIMEOUTCYCLE = 0; //SI PROBLEME
fintest3:

        FCYCLE = 0;

        cycleN = 1;
        TEMPO(5); //MAX 70*x=350
        TIMEOUTCYCLE++;
        if ((VDD1 > 2)&&(R1NOREP <= 4)) {

            if (CYCLE_R1 == 1) {
                FCYCLE = 1 + FCYCLE;

            }
        }
        if ((VDD2 > 2)&&(R2NOREP <= 4)) {

            if (CYCLE_R2 == 1) {
                FCYCLE = 2 + FCYCLE;

            }
        }
        if ((VDD3 > 2)&&(R3NOREP <= 4)) {
            if (CYCLE_R3 == 1) {
                FCYCLE = FCYCLE + 4;
            }
        }
        if ((VDD4 > 2)&&(R4NOREP <= 4)) {
            if (CYCLE_R4 == 1) {
                FCYCLE = FCYCLE + 8;
            }
        }
        if ((VDD5 > 2)&&(R5NOREP <= 4)) {
            if (CYCLE_R5 == 1) {
                FCYCLE = FCYCLE + 16;
            }
        }

        if (MINIRESETRI == 0xFF) //MINI RESET AUTO A ETE FAIT
        {
            MINIRESETRI = 0;
            goto continuercycle;
        }
        if (TIMEOUTCYCLE >= 60) //TIMEOUT CYCLE
            goto continuercycle;
        if (FCYCLE != TESTCYCLE) //TOUTES LES CARTES NONT PAS TERMINEES
            goto fintest3;

continuercycle:

        delay_ms(10);

        FCYCLE = 0;
        TESTCYCLE = 0;
        goto L;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UNIQUEMENT POUR CARTE MERE ET A LA DEMANDE DE LA CARTE MERE (LORS d'une PREMIERE TRAME CM-->Rx  et  erreurs)  //
//ECRIRE 1 pour carte R1 ,2 pour R2 etc..................
//puis le numero de la fonction exemple

// pour DIC=68*2+73+67=276 (avec code decimal des carac ascii) DEMANDE INFOS CAPTEUR A LA CARTE RELAIS	//
//puis le tableau contenant les donn�es a envoyer suivant le code de la fonction																								                //
// le resultat est stock� dans la TRAMEOUT[]
//////////////////////////////////////////////////////////////////////////////////////////////////////

void ENVOIE_DES_TRAMES(char R, int fonctionRS, unsigned char *tab) {

    int t;
    unsigned int funct;
    int y;
    char u;
    float h;


    ID1[0] = 'C';
    ID1[1] = 'M';


    switch (R) //analyse de la fonction
    {

        case 1: // R1
            ID2[0] = 'R';
            ID2[1] = '1';
            break;
        case 2: // R2
            ID2[0] = 'R';
            ID2[1] = '2';
            break;
        case 3: // R3
            ID2[0] = 'R';
            ID2[1] = '3';
            break;
        case 4: // R4
            ID2[0] = 'R';
            ID2[1] = '4';
            break;
        case 5: // R5
            ID2[0] = 'R';
            ID2[1] = '5';
            break;

        case 6: // R5
            ID2[0] = 'A';
            ID2[1] = 'U';
            break;

        case 7: // IO
            ID2[0] = 'I';
            ID2[1] = 'O';
            break;

        default:


            break;

    }



    switch (fonctionRS) {


        case 301:

            funct = 1;
forma:
            INTCONbits.GIE = 0; //ON INTERDIT TT
            FONCTION[0] = 'F';
            FONCTION[1] = 'O';
            FONCTION[2] = 'R';
            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];
            pointeur_out_in = 8;
            calcul_CRC(CRC, TRAMEOUT); //A COMPLETER
            TRAMEOUT[pointeur_out_in + 5] = FIN_TRAME;
            funct = 0;
            AQUIENVOYER(ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
            delay_ms(100); //attend un peu avant d'envoyer
            TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A LA CARTE CONCERNEE

            TRAMERECEPTIONRS485DONNEES(25, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE


            controle_CRC(CRC, TRAMEIN);
            REPOSAQUIENVOYER(ID2);
            if (NORECEPTION == 0xFF) {
                TEMPO(1);
                funct++;
                if (funct < 3)
                    goto forma;
            }
            break;


        case 291: //ECRIRE SUR CARTE SD suivant une demande -> ENVOIE_DES_TRAMES (6,291,&INSTRUCTION);

            INTCONbits.GIE = 0; //ON INTERDIT TT
            FONCTION[0] = 'D';
            FONCTION[1] = 'D';
            FONCTION[2] = 'W';
            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];
            DX10 = 0;
            DX20 = 0;
            if (tab[0] == 'D') //DEMARRAGE FICHIER DEM
            {
                SDW('D', TRAMEIN, TRAMEOUT, TYPECARTE, VDD1, VDD2, VDD3, VDD4, VDD5, VBAT, SAVTAB, TMP, R1NOREP, R2NOREP, R3NOREP, R4NOREP, R5NOREP, HORLOGE); //PROCEDURE DECRITURE
                INTCONbits.GIE = 1;
            }
            if (tab[0] == 'C') //CYCLIQUE	AUTOMATE OU INTERNE		FICHIER SAV
                SDW('C', TRAMEIN, TRAMEOUT, TYPECARTE, VDD1, VDD2, VDD3, VDD4, VDD5, VBAT, SAVTAB, TMP, R1NOREP, R2NOREP, R3NOREP, R4NOREP, R5NOREP, HORLOGE);
            if (tab[0] == 'M') //CYCLIQUE	MINITEL	ou	FICHIER SAV
            {
                INTCONbits.GIE = 0;
                SDW('M', TRAMEIN, TRAMEOUT, TYPECARTE, VDD1, VDD2, VDD3, VDD4, VDD5, VBAT, SAVTAB, TMP, R1NOREP, R2NOREP, R3NOREP, R4NOREP, R5NOREP, HORLOGE);
                INTCONbits.GIE = 1;
            }
            if (tab[0] == 'N') //CYCLIQUE	MINITEL	ou	FICHIER SAV
            {
                INTCONbits.GIE = 0;
                SDW('N', TRAMEIN, TRAMEOUT, TYPECARTE, VDD1, VDD2, VDD3, VDD4, VDD5, VBAT, SAVTAB, TMP, R1NOREP, R2NOREP, R3NOREP, R4NOREP, R5NOREP, HORLOGE);
                INTCONbits.GIE = 1;
            }


            break;







        case 333:



            //	INTCONbits.GIE= 1;

            FONCTION[0] = 'Z';
            FONCTION[1] = 'T';
            FONCTION[2] = 'E';
            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];

            h = TMP[1] / 10;
            u = h;
            h = TMP[1] - 10 * u;

            TRAMEOUT[8] = u + 48;


            u = h;

            TRAMEOUT[9] = u + 48;



            for (y = 0; y < 23; y++) {
                TRAMEOUT[10 + y] = TMP[y + 2];
            }


            pointeur_out_in = 31;
            calcul_CRC(CRC, TRAMEOUT); //A COMPLETER
            TRAMEOUT[pointeur_out_in + 5] = FIN_TRAME;

            TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A LA CARTE CONCERNEE
            funct = 0;

            break;





        case 302: // ERREUR


            FONCTION[0] = 'E';
            FONCTION[1] = 'R';
            FONCTION[2] = 'R';
            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];
            //TRAMEOUT[8]=tab[0];
            //TRAMEOUT[9]=tab[1];

            for (t = 8; t < 28; t++) {
                TRAMEOUT[t] = tab[t - 8];
            }
            if (ERRMM == 1) {
                TRAMEOUT[6] = 'M';
                TRAMEOUT[7] = 'M';
                TRAMEOUT[8] = 'M';
                TRAMEOUT[9] = 'M';

            }
            pointeur_out_in = 28;
            calcul_CRC(CRC, TRAMEOUT); //A COMPLETER
            TRAMEOUT[pointeur_out_in + 5] = FIN_TRAME;
            AQUIENVOYER(ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
            TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A LA CARTE CONCERNEE
            REPOSAQUIENVOYER(ID2);
            funct = 0;

            break;


        case 276: //OK DIC

            INTCONbits.GIE = 1; //AUTORISER LES INTERRUPTIONS GENERALES

            TROPDENONREPONSE = 0x00;
            PRIO_AUTO_IO = 0xDD; //ON EST EN MODE INTERROGATION

            FONCTION[0] = 'D';
            FONCTION[1] = 'I';
            FONCTION[2] = 'C';

            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];
            TRAMEOUT[8] = tab[0];
            TRAMEOUT[9] = tab[1];
            TRAMEOUT[10] = tab[2];
            TRAMEOUT[11] = tab[3];
            TRAMEOUT[12] = tab[4];
            if (MODERAPID == 0)
                TRAMEOUT[13] = '-';
            if (MODERAPID == 1)
                TRAMEOUT[13] = '/';
            TRAMEOUT[14] = tab[5];
            TRAMEOUT[15] = tab[6];
            TRAMEOUT[16] = tab[7];
            TRAMEOUT[17] = tab[8];
            TRAMEOUT[18] = tab[9];

            pointeur_out_in = 19; // SAUVEGARDE POINTEUR DU TABLEAU
            calcul_CRC(CRC, TRAMEOUT); // CALCUL LE CRC ET LE RAJOUTE AU TABLEAU TRAMEOUT[]
            TRAMEOUT[pointeur_out_in + 5] = FIN_TRAME; // RAJOUT LE BYTE DE STOP


            tmp2 = 0; //SYNCHRONISATION ON ATTEND "%" debut supertrame
            tmp = 0;
rep1:
            if (tmp2 <= 4) //4 ESSAIS
            {
                AQUIENVOYER(ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
                delay_ms(100); //attend un peu avant d'envoyer
                TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A LA CARTE CONCERNEE
                REPOSAQUIENVOYER(ID2);
                tmp = RECEVOIR_RS485(9, &NORECEPTION); // ON ATTEND '%' //30082017


                if ((PRIO_AUTO_IO == 0xD1) || (PRIO_AUTO_IO == 0xD2)) // ON ARRETE ET ON PASSE EN MODE NORMAL
                {
                    TRAMERECEPTIONRS485DONNEES(9, &NORECEPTION, TRAMEIN); // SE ATTEND UNE RECEPTION AVANT D'ARRETER ATTENTION SI LA CARTE RI % PUIS 00100 trop rapide le # n'est pas dedans
                    controle_CRC(CRC, TRAMEIN);

                    INFORMATION_ERREUR[0] = 'D'; // ENVOYER A L'AUTO ATTENTION JE SUIS EN MODE DEMANDE RI
                    INFORMATION_ERREUR[1] = 'D';
                    for (t = 2; t < 20; t++) {
                        INFORMATION_ERREUR[t] = 'x';
                    }

                    if (PRIO_AUTO_IO == 0xD1) {
                    }
                    if (PRIO_AUTO_IO == 0xD2)
                        ENVOIE_DES_TRAMES(7, 302, INFORMATION_ERREUR); // ENVOYER A L'IO

                    if (PRIO_AUTO_IO == 0xD2) //ICI 1404
                    {
                        TRAMERECEPTIONRS485DONNEES(9, &NORECEPTION, TRAMEIN); // SE ATTEND UNE RECEPTION AVANT D'ARRETER //ICI 1404	//ICI 1404 IO
                        controle_CRC(CRC, TRAMEIN); //ICI 1404
                    }

                    INFORMATION_ERREUR[0] = 'A'; // ON ENVOIE UNE TRAME D'ARRET
                    INFORMATION_ERREUR[1] = 'A';
                    for (t = 2; t < 20; t++) {
                        INFORMATION_ERREUR[t] = 'x';
                    }
                    if (TRAMEIN[2] == '1')
                        ENVOIE_DES_TRAMES(1, 302, INFORMATION_ERREUR); // ON ENVOIE A CARTE 1 ICI voir les autres suivant demande
                    if (TRAMEIN[2] == '2')
                        ENVOIE_DES_TRAMES(2, 302, INFORMATION_ERREUR); // ON ENVOIE A CARTE 2 ICI voir les autres suivant demande
                    if (TRAMEIN[2] == '3')
                        ENVOIE_DES_TRAMES(3, 302, INFORMATION_ERREUR); // ON ENVOIE A CARTE 3 ICI voir les autres suivant demande
                    if (TRAMEIN[2] == '4')
                        ENVOIE_DES_TRAMES(4, 302, INFORMATION_ERREUR); // ON ENVOIE A CARTE 4 ICI voir les autres suivant demande
                    if (TRAMEIN[2] == '5')
                        ENVOIE_DES_TRAMES(5, 302, INFORMATION_ERREUR); // ON ENVOIE A CARTE 5 ICI voir les autres suivant demande
                    if ((TRAMEIN[2] != '5')&&(TRAMEIN[2] != '4')&&(TRAMEIN[2] != '3')&&(TRAMEIN[2] != '2')&&(TRAMEIN[2] != '1'))
                        ENVOIE_DES_TRAMES(1, 302, INFORMATION_ERREUR); // securit� arrete la carte au cas ou

                    REPOSAQUIENVOYER(ID2);
                    goto ARRETERDIC1;


                }

                if (tmp != '%') {
                    tmp2 = tmp2 + 1;
                    goto rep1;
                }
            } else {

                break;
            }

            REPOSAQUIENVOYER(ID2);
            funct = 0;
            do {
                funct = funct + 1;



                if ((PRIO_AUTO_IO == 0xD1) || (PRIO_AUTO_IO == 0xD2)) // ON ARRETE ET ON PASSE EN MODE NORMAL
                {

                    TRAMERECEPTIONRS485DONNEES(9, &NORECEPTION, TRAMEIN); // SE ATTEND UNE RECEPTION AVANT D'ARRETER
                    controle_CRC(CRC, TRAMEIN);


                    INFORMATION_ERREUR[0] = 'D'; // ENVOYER A L'AUTO ATTENTION JE SUIS EN MODE DEMANDE RI
                    INFORMATION_ERREUR[1] = 'D';
                    for (t = 2; t < 20; t++) {
                        INFORMATION_ERREUR[t] = 'x';
                    }

                    if (PRIO_AUTO_IO == 0xD1) {
                    }
                    if (PRIO_AUTO_IO == 0xD2)
                        ENVOIE_DES_TRAMES(7, 302, INFORMATION_ERREUR); // ENVOYER A L'IO

                    if (PRIO_AUTO_IO == 0xD2) //ICI 1404
                    {
                        TRAMERECEPTIONRS485DONNEES(9, &NORECEPTION, TRAMEIN); // SE ATTEND UNE RECEPTION AVANT D'ARRETER //ICI 1404	//ICI 1404
                        controle_CRC(CRC, TRAMEIN); //ICI 1404

                        ID2[0] = 'R';
                        ID2[1] = TRAMEIN[2];
                    } //ICI 1404

                    INFORMATION_ERREUR[0] = 'A'; // ON ENVOIE UNE TRAME D'ARRET
                    INFORMATION_ERREUR[1] = 'A';
                    delay_ms(100); //ON ATTEND UN PEU AVANT D'ARRETER LA CARTE RI
                    for (t = 2; t < 20; t++) {
                        INFORMATION_ERREUR[t] = 'x';
                    }

                    if (ID2[1] == '1')
                        ENVOIE_DES_TRAMES(1, 302, INFORMATION_ERREUR); // ON ENVOIE A CARTE 1 ICI voir les autres suivant demande
                    if (ID2[1] == '2')
                        ENVOIE_DES_TRAMES(2, 302, INFORMATION_ERREUR); // ON ENVOIE A CARTE 2 ICI voir les autres suivant demande
                    if (ID2[1] == '3')
                        ENVOIE_DES_TRAMES(3, 302, INFORMATION_ERREUR); // ON ENVOIE A CARTE 3 ICI voir les autres suivant demande
                    if (ID2[1] == '4')
                        ENVOIE_DES_TRAMES(4, 302, INFORMATION_ERREUR); // ON ENVOIE A CARTE 4 ICI voir les autres suivant demande
                    if (ID2[1] == '5')
                        ENVOIE_DES_TRAMES(5, 302, INFORMATION_ERREUR); // ON ENVOIE A CARTE 5 ICI voir les autres suivant demande

                    REPOSAQUIENVOYER(ID2);
                    goto ARRETERDIC1;


                }







                TRAMERECEPTIONRS485DONNEES(9, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE //ON ATTEND 10ms dans ce sous programme avant denvoyer ERR00
                pointeur_out_in = 92; //DEBUT DU CRC RECU POUR TRAME DIC
                CRC_ETAT = CRC_verif(CRC, TRAMEIN, pointeur_out_in); //ICI CRC_ETAT PASSE A 0xFF SI CORRECT
                if (NORECEPTION == 0xFF) //TROP DE NON REPONSE DE LA CARTE
                    TROPDENONREPONSE++;
                if (TROPDENONREPONSE == 4) //3x on arrete
                    goto ARRETERDIC1;
                if (TRAMEIN[0] != '$') {
                    INTCONbits.GIE = 0; //INTERDIRE LES INTERRUPTIONS GENERALES
                    //MEMORISER SI ETAT=0xFF a faire
                    if (CRC_ETAT == 0xFF) {
                        if (TRAMEIN[82] == '9') //BUG 98
                            TRAMEIN[82] = '0';
                        if ((TRAMEIN[83] == '8') || (TRAMEIN[83] == '9'))
                            TRAMEIN[83] = '0';
                        //PB HOROLOGE
                        if ((TRAMEIN[60] == '?') || (TRAMEIN[61] == '?') || (TRAMEIN[62] == '?') || (TRAMEIN[63] == '?') || (TRAMEIN[64] == '?') || (TRAMEIN[65] == '?') || (TRAMEIN[66] == '?') || (TRAMEIN[67] == '?')) {
                            TRAMEIN[60] = '2';
                            TRAMEIN[61] = '9';
                            TRAMEIN[62] = '1';
                            TRAMEIN[63] = '1';
                            TRAMEIN[64] = '0';
                            TRAMEIN[65] = '7';
                            TRAMEIN[66] = '1';
                            TRAMEIN[67] = '5';

                        }
                        pointeur_out_in = recuperer_trame_memoire(1, 'r', TRAMEIN, TRAMEOUT, VOIECOD, HORLOGE, &ETATZIZCRATIONAUTOMATE, ETAT); //STOCKAGE EN MMOIRE DE L'INFORMATION DU CAPTEUR
                    }
                    INTCONbits.GIE = 1; //AUTRISER LES INTERRUPTIONS GENERALES
                    INFORMATION_ERREUR[0] = '0'; //
                    INFORMATION_ERREUR[1] = '0';
                    for (t = 2; t < 20; t++) {
                        INFORMATION_ERREUR[t] = 'x';
                    }
                    ENVOIE_DES_TRAMES(ID2[1] - 48, 302, INFORMATION_ERREUR); // ON ENVOIE A CARTE x ICI voir les autres suivant demande
                }





            } while (TRAMEIN[1] != '$' && TRAMEIN[0] != '$');
            if (PRIO_AUTO_IO == 0xDD)
                PRIO_AUTO_IO = 0x00; // RETOUR EN ETAT NORMAL

ARRETERDIC1:

            INFORMATION_ERREUR[0] = 'A'; // ON ENVOIE UNE TRAME D'ARRET
            INFORMATION_ERREUR[1] = 'A';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            if (TRAMEIN[2] == '1')
                ENVOIE_DES_TRAMES(1, 302, INFORMATION_ERREUR); // ON ENVOIE A CARTE 1 ICI voir les autres suivant demande
            if (TRAMEIN[2] == '2')
                ENVOIE_DES_TRAMES(2, 302, INFORMATION_ERREUR); // ON ENVOIE A CARTE 2 ICI voir les autres suivant demande
            if (TRAMEIN[2] == '3')
                ENVOIE_DES_TRAMES(3, 302, INFORMATION_ERREUR); // ON ENVOIE A CARTE 3 ICI voir les autres suivant demande
            if (TRAMEIN[2] == '4')
                ENVOIE_DES_TRAMES(4, 302, INFORMATION_ERREUR); // ON ENVOIE A CARTE 4 ICI voir les autres suivant demande
            if (TRAMEIN[2] == '5')
                ENVOIE_DES_TRAMES(5, 302, INFORMATION_ERREUR); // ON ENVOIE A CARTE 5 ICI voir les autres suivant demande
            if ((TRAMEIN[2] != '5')&&(TRAMEIN[2] != '4')&&(TRAMEIN[2] != '3')&&(TRAMEIN[2] != '2')&&(TRAMEIN[2] != '1'))
                ENVOIE_DES_TRAMES(1, 302, INFORMATION_ERREUR); // securit� arrete la carte au cas ou

            funct = 0;


            INTCONbits.GIE = 1; //AUTORISER LES INTERRUPTIONS GENERALES
            if (PRIO_AUTO_IO == 0xD1) //ICI 1404
                identificationINTER(6, 0x00); //ICI 1404

            PRIO_AUTO_IO = 0x00; // RETOUR EN ETAT NORMAL			//ICI 1404

            break;



        case 308: // DEMANDE LISTE DES CAPTEURS A LA CARTE RELAIS




            FONCTION[0] = 'L';
            FONCTION[1] = 'I';
            FONCTION[2] = 'S';

            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];


            pointeur_out_in = 8;



            calcul_CRC(CRC, TRAMEOUT); //A COMPLETER
            TRAMEOUT[pointeur_out_in + 5] = FIN_TRAME;

            //LA TRAME EST PRETE
            AQUIENVOYER(ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
            TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A LA CARTE CONCERNEE
            REPOSAQUIENVOYER(ID2);
            do {
                TRAMERECEPTIONRS485DONNEES(9, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE

                controle_CRC(CRC, TRAMEIN);


                RECUPERATION_DES_TRAMES(TRAMEIN);
                METTRE_EN_MEMOIRE_LISTE(TRAMEIN, 0);




                INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
                INFORMATION_ERREUR[1] = '0';
                for (t = 2; t < 20; t++) {
                    INFORMATION_ERREUR[t] = 'x';
                }
                ENVOIE_DES_TRAMES(R, 302, INFORMATION_ERREUR);


            } while (TRAMEIN[98] != 'F'); // JUSQU A LA DERNIERE TRAME
            // IL Y A UNE TRAME PUIS UNE AUTRES ETC LES RANGER EN MEMOIRES A UN EMPLACEMENT SPECIFIQUE
            break;


        case 290: //DEMANDE LISTE DES CAPTEURS A LA CARTE RELAIS AVEC CHANGEMENTS IMPORTANTS

            FONCTION[0] = 'L';
            FONCTION[1] = 'I';
            FONCTION[2] = 'A';

            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];


            pointeur_out_in = 8;



            calcul_CRC(CRC, TRAMEOUT); //A COMPLETER
            TRAMEOUT[pointeur_out_in + 5] = FIN_TRAME;

            //LA TRAME EST PRETE
            AQUIENVOYER(ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
            TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A LA CARTE CONCERNEE
            REPOSAQUIENVOYER(ID2);
            do {
                TRAMERECEPTIONRS485DONNEES(9, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE

                controle_CRC(CRC, TRAMEIN);


                RECUPERATION_DES_TRAMES(TRAMEIN);
                METTRE_EN_MEMOIRE_LISTE(TRAMEIN, 1);




                INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
                INFORMATION_ERREUR[1] = '0';
                for (t = 2; t < 20; t++) {
                    INFORMATION_ERREUR[t] = 'x';
                }
                ENVOIE_DES_TRAMES(R, 302, INFORMATION_ERREUR);


            } while (TRAMEIN[98] != 'F'); // JUSQU A LA DERNIERE TRAME

            // IL Y A UNE TRAME PUIS UNE AUTRES ETC LES RANGER EN MEMOIRES A UN EMPLACEMENT SPECIFIQUE

            break;

        case 304: // TABLEAU DEXISTENCE


            PRIO_AUTO_IO = 0xEE; //ON EST EN MODE cm table exiST
            INTCONbits.GIE = 1; //AUTORISER LES INTERRUPTIONS GENERALES

            FONCTION[0] = 'T';
            FONCTION[1] = 'E';
            FONCTION[2] = 'C';

            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];

            pointeur_out_in = 8;
            calcul_CRC(CRC, TRAMEOUT); //A COMPLETER
            TRAMEOUT[pointeur_out_in + 5] = FIN_TRAME;
            funct = 0;


            tmp2 = 0; //SYNCHRONISATION ON ATTEND "%" debut supertrame
            tmp = 0;
rep3:
            if (tmp2 <= 4) //4 ESSAIS
            {
                AQUIENVOYER(ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
                delay_ms(100); //attend un peu avant d'envoyer
                TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A LA CARTE CONCERNEE
                REPOSAQUIENVOYER(ID2);
                tmp = RECEVOIR_RS485(9, &NORECEPTION); // ON ATTEND '%'
                if (NORECEPTION == 0xFF) // RIEN REcU
                {
                    if (ID2[1] == '1')
                        R1NOREP = R1NOREP + 1;
                    if (ID2[1] == '2')
                        R2NOREP = R2NOREP + 1;
                    if (ID2[1] == '3')
                        R3NOREP = R3NOREP + 1;
                    if (ID2[1] == '4')
                        R4NOREP = R4NOREP + 1;
                    if (ID2[1] == '5')
                        R5NOREP = R5NOREP + 1;


                    tmp2 = tmp2 + 1;
                    goto rep3;
                } else {
                    if (ID2[1] == '1') {
                        pic_eeprom_write(0x01, 0xAA); //RESET RI A ZERO
                        R1NOREP = 0;
                        UNRESETRI1 = 0;
                    }
                    if (ID2[1] == '2') {
                        pic_eeprom_write(0x02, 0xAA); //RESET RI A ZERO
                        R2NOREP = 0;
                        UNRESETRI2 = 0;
                    }
                    if (ID2[1] == '3') {
                        pic_eeprom_write(0x03, 0xAA); //RESET RI A ZERO
                        R3NOREP = 0;
                        UNRESETRI3 = 0;
                    }
                    if (ID2[1] == '4') {
                        pic_eeprom_write(0x04, 0xAA); //RESET RI A ZERO
                        R4NOREP = 0;
                        UNRESETRI4 = 0;
                    }
                    if (ID2[1] == '5') {
                        pic_eeprom_write(0x05, 0xAA); //RESET RI A ZERO
                        R5NOREP = 0;
                        UNRESETRI5 = 0;
                    }
                }


            } else {

                break;
            }


            REPOSAQUIENVOYER(ID2);





            funct = 0;

            do {
                funct = funct + 1;
                TRAMERECEPTIONRS485DONNEES(9, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
                pointeur_out_in = 28;
                CRC_ETAT = CRC_verif(CRC, TRAMEIN, pointeur_out_in); //ETAT_CRC est a 0xFF Si ok
                //controle_CRC(CRC, TRAMEIN);

                if (TRAMEIN[0] != '$') {
                    INTCONbits.GIE = 0; //AUTORISER LES INTERRUPTIONS GENERALES
                    //A FAIRE SI ETAT_CRC EST CORRECT 0xFF SAV
                    if (CRC_ETAT == 0xFF)
                        SAV_EXISTENCE(TRAMEIN, TMP);
                    INTCONbits.GIE = 1; //AUTORISER LES INTERRUPTIONS GENERALES
                }

                if (funct == 15) {




                }



            } while ((TRAMEIN[0] != '$')&&(NORECEPTION != 0xFF));

            funct = 0;
            //			 LECTURE DES TABLES POUR TEST
            for (funct = 0; funct < 100; funct++) {


                LIRE_EEPROM(3, 128 * funct + 30000, TRAMEOUT, 4); //LIRE DANS EEPROM
                TRAMEIN[0] != '$';


            }



            if (PRIO_AUTO_IO == 0xEE)
                PRIO_AUTO_IO = 0x00; // RETOUR EN ETAT NORMAL

            if ((PRIO_AUTO_IO == 0xE1) || (PRIO_AUTO_IO == 0xE2)) // ON A RECU UNE DEMANDE DE LIO OU AU
            {

                if (PRIO_AUTO_IO == 0xE2) //ON A DETECTER UNE DEMANDE DE L'IO PENDANT LINTERROGATION TEC
                    identificationINTER(7, 0x00); // ON FORCE MANUELLEMENT en MODE NORMALE POUR VALIDER LACTION DE IO
                if (PRIO_AUTO_IO == 0xE1) // ON A DETECTER UNE DEMANDE DE L'AUTO PENDANT LINTERROGATION TEC
                    identificationINTER(6, 0x00);


                PRIO_AUTO_IO = 0x00;
                // VOIR SI ON ARETE OU ON DONNE ERR00 OU DD
            }

            break;



        case 305: //OK HOR


            // REGLER HORLOGE DE LA CARTE RELAIS
            //HORLOGE[8] A REMPIR hh/mm/ss/aa

            PRIO_AUTO_IO = 0x77; //ON EST EN MODE HORLOGE
            //INTCONbits.GIE = 0; //21102020
            FONCTION[0] = 'H';
            FONCTION[1] = 'O';
            FONCTION[2] = 'R';

            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];
            TRAMEOUT[8] = tab[0];
            TRAMEOUT[9] = tab[1];
            TRAMEOUT[10] = tab[2];
            TRAMEOUT[11] = tab[3];
            TRAMEOUT[12] = tab[4];
            TRAMEOUT[13] = tab[5];
            TRAMEOUT[14] = tab[6];
            TRAMEOUT[15] = tab[7];
            TRAMEOUT[16] = tab[8];
            TRAMEOUT[17] = tab[9];
            TRAMEOUT[18] = tab[10];
            TRAMEOUT[19] = tab[11];
            pointeur_out_in = 20;
            calcul_CRC(CRC, TRAMEOUT); //A COMPLETER
            TRAMEOUT[pointeur_out_in + 5] = FIN_TRAME;
            funct = 0;
            tmp2 = 0; //SYNCHRONISATION ON ATTEND "%" debut supertrame
            tmp = 0;
            MAXBITE = 0;
rep2:


            if (tmp2 <= 4) //4 ESSAIS
            {
                if (PRIO_AUTO_IO == 0x00) // UNE DEMANDE DE L'AUTO A ETE 					
                    break;

                // 22022021
                if (INT0 == 1) //une demande de l'autmate a ete faite !!!!!!!! pas sur interruption juste touche
                {
                    SERIALISATION(&ENTREES1, &ENTREES2); // VOIR SI DEMANDE DE L'AUTO
                    if ((ENTREES2 & 0x02) == 0x02) {

                        PRIO_AUTO_IO = 0x00;
                        TEMPO(1);
                        identificationINTER(6, 0x00); // ON EST OK POUR LA DEMANDE DE L'AUTO PAR DEFAUT ICI

                        goto STOPPERHOR;
                    }
                }
                AQUIENVOYER(ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
                delay_ms(100); //attend un peu avant d'envoyer
                TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A LA CARTE CONCERNEE
                REPOSAQUIENVOYER(ID2);
                TRAMERECEPTIONRS485DONNEES(9, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
                controle_CRC(CRC, TRAMEIN);
                if (NORECEPTION == 0xFF) // RIEN REcU
                {
                    creer_trame_horloge(HORLOGE); //LIRE EEPPROM VERIFICATION
                    for (funct = 0; funct < 12; funct++) {
                        TRAMEOUT[funct + 8] = HORLOGE[funct];
                    }
                    tmp2 = tmp2 + 1;

                    goto rep2;
                }
            } else {
                //INTCONbits.GIE = 1; //21102020
                break;
            }

STOPPERHOR:
            //// A FAIRE CONTROLE HEURE OU CHANGER PAR TRAME OK???????????????????????????
            PRIO_AUTO_IO = 0x00;
            funct = 0;
            break;

        case 285: // CREATION CAPTEUR (MEME SI ON CHANGE JUSTE LE SEUIL)
            // UTILISATION TRAMEIN[] -> ENVOIE_DES_TRAMES (1,285,TRAMEIN[])

            FONCTION[0] = 'C';
            FONCTION[1] = 'R';
            FONCTION[2] = 'E';




            pointeur_out_in = recuperer_trame_memoire(0, 'a', TRAMEIN, TRAMEOUT, VOIECOD, HORLOGE, &ETATZIZCRATIONAUTOMATE, ETAT); //TRANSFORME LA TRAMEIN EN FORMAT MEMOIRE SUR TRAMEOUT
            intervertirOUT_IN(TRAMEIN, TRAMEOUT);
            pointeur_out_in = TRANSFORMER_TRAME_MEM_EN_COM(TRAMEIN, TRAMEOUT, VOIECOD); // TRANSFORME LA TRAMEIN MEMOIRE en TRAMEOUUT COM
            //RECURRENT
            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2]; //TRAMEOUT;


            calcul_CRC(CRC, TRAMEOUT); //A COMPLETER
            TRAMEOUT[pointeur_out_in + 5] = FIN_TRAME;

            //LA TRAME EST PRETE
            tmp2 = 0; //SYNCHRONISATION ON ATTEND "%" debut supertrame
            tmp = 0;
rep6:
            if (tmp2 <= 10) //10 ESSAIS
            {
                if ((PRESENCEDEBITSD == 0x00)&&(TRAMEIN[4] == '0')&&(TRAMEIN[5] == '0')) //Si le debitmetre nexiste pas alors prevenir de SAV en RI mais pas l'existence
                    TRAMEOUT[92] = 0; //sur le CRC mette 02222 au lieu de 22222
                AQUIENVOYER(ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
                delay_ms(100); //attend un peu avant d'envoyer
                TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A LA CARTE CONCERNEE
                REPOSAQUIENVOYER(ID2);
                TRAMERECEPTIONRS485DONNEES(9, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
                controle_CRC(CRC, TRAMEIN);
                if (NORECEPTION == 0xFF) // RIEN REcU
                {
                    tmp2 = tmp2 + 1;
                    goto rep6;
                }
            } else {

                break;
            }

            funct = 0;
            break;

        case 278: // EFFACER CAPTEUR SCR OK


            FONCTION[0] = 'E';
            FONCTION[1] = 'F';
            FONCTION[2] = 'F';

            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];
            TRAMEOUT[8] = tab[0];
            TRAMEOUT[9] = tab[1];
            TRAMEOUT[10] = tab[2];
            TRAMEOUT[11] = tab[3];
            TRAMEOUT[12] = tab[4];
            TRAMEOUT[13] = tab[5];

            pointeur_out_in = 14;
            calcul_CRC(CRC, TRAMEOUT); //A COMPLETER

            TRAMEOUT[pointeur_out_in + 5] = FIN_TRAME;

            tmp2 = 0; //SYNCHRONISATION ON ATTEND "%" debut supertrame
            tmp = 0;
rep5:
            if (tmp2 <= 10) //10 ESSAIS
            {
                delay_ms(10); //attend un peu avant d'envoyer
                AQUIENVOYER(ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
                delay_ms(100); //attend un peu avant d'envoyer
                TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A LA CARTE CONCERNEE
                REPOSAQUIENVOYER(ID2);
                TRAMERECEPTIONRS485DONNEES(9, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
                controle_CRC(CRC, TRAMEIN);
                if (NORECEPTION == 0xFF) // RIEN REcU
                {
                    tmp2 = tmp2 + 1;
                    goto rep5;
                }
            } else {

                break;
            }
            EFFACEMENT(DEMANDE_INFOS_CAPTEURS, TRAMEOUT); //VOIR SI NECCESSAIRE
            funct = 0;
            break;

        case 315: //LANCER LA DETECTION AUTOMATIQUE

            INTCONbits.GIE = 0;


            FONCTION[0] = 'S';
            FONCTION[1] = 'C';
            FONCTION[2] = 'R';
            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];
            TRAMEOUT[8] = INSTRUCTION[0];
            TRAMEOUT[9] = INSTRUCTION[1];
            pointeur_out_in = 10;
            calcul_CRC(CRC, TRAMEOUT); //A COMPLETER
            if (scrutationunevoie == 1) {
                TRAMEOUT[10] = '7'; //RUSE SUR LE CRC NON GERE POUR INDIQUER FONCTION CACHEE SCRUTATION UNE SEULE VOIE
                TRAMEOUT[11] = '7';
            }


            TRAMEOUT[pointeur_out_in + 5] = FIN_TRAME;




            tmp2 = 0; //SYNCHRONISATION ON ATTEND "%" debut supertrame
            tmp = 0;
rep4:
            if (tmp2 <= 10) //10 ESSAIS
            {
                AQUIENVOYER(ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
                delay_ms(100); //attend un peu avant d'envoyer
                TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A LA CARTE CONCERNEE
                REPOSAQUIENVOYER(ID2);
                TRAMERECEPTIONRS485DONNEES(18, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
                controle_CRC(CRC, TRAMEIN);
                if (NORECEPTION == 0xFF) // RIEN REcU
                {
                    tmp2 = tmp2 + 1;
                    goto rep4;
                }
            } else {

                break;
            }

            do {
                tmp = RECEVOIR_RS485(18, &NORECEPTION); //9 ici


                if (NORECEPTION == 0x00)
                    tmp2 = 0;

                if (tmp == '7') //ERREUR CARTE ON RETENTE
                {
                    tmp = '$';
                    TEMPO(2);
                    RESCRUTATION++;
                }

                if (tmp == 0x00) //ERREUR CARTE ON RETENTE
                {
                    tmp = '$';
                    RESCRUTATION++;
                }
            } while (tmp != '$');

            break;



        case 300: //fonction oeil

            INTCONbits.GIE = 0;

            OEIL_CAPTEUR[0] = tab[0];
            OEIL_CAPTEUR[1] = tab[1];
            OEIL_CAPTEUR[2] = tab[2];
            OEIL_CAPTEUR[3] = tab[3];
            OEIL_CAPTEUR[4] = tab[4];
            do {


                FONCTION[0] = 'O';
                FONCTION[1] = 'E';
                FONCTION[2] = 'I';
                TRAMEOUT[0] = DEBUT_TRAME;
                TRAMEOUT[1] = ID1[0];
                TRAMEOUT[2] = ID1[1];
                TRAMEOUT[3] = ID2[0];
                TRAMEOUT[4] = ID2[1];
                TRAMEOUT[5] = FONCTION[0];
                TRAMEOUT[6] = FONCTION[1];
                TRAMEOUT[7] = FONCTION[2];
                TRAMEOUT[8] = OEIL_CAPTEUR[0];
                TRAMEOUT[9] = OEIL_CAPTEUR[1];
                TRAMEOUT[10] = OEIL_CAPTEUR[2];
                TRAMEOUT[11] = OEIL_CAPTEUR[3];
                TRAMEOUT[12] = OEIL_CAPTEUR[4];


                pointeur_out_in = 13;


                calcul_CRC(CRC, TRAMEOUT); //A COMPLETER
                TRAMEOUT[pointeur_out_in + 5] = FIN_TRAME;

                tmp2 = 0; //SYNCHRONISATION ON ATTEND "%" debut supertrame
                tmp = 0;
                IDENTIF_TYPE_CARTE(TMP, TYPECARTE, VDD1, VDD2, VDD3, VDD4, VDD5, VBAT, R1NOREP, R2NOREP, R3NOREP, R4NOREP, R5NOREP); //NORMALEMENT SE FAIT PAR LE REA !!!!!!! A VERIFIER
                if (TRAMEOUT[4] == '1') {
                    TYPEDECARTEOEIL = 'E';
                    if (TYPECARTE[0] != 'E') //RESISTIVE
                    {
                        TRAMEOUT[11] = '0';
                        TRAMEOUT[12] = '1';
                        TYPEDECARTEOEIL = 'M';
                    }
                }

                if (TRAMEOUT[4] == '2') {
                    TYPEDECARTEOEIL = 'E';
                    if (TYPECARTE[1] != 'E') //RESISTIVE
                    {
                        TRAMEOUT[11] = '0';
                        TRAMEOUT[12] = '1';
                        TYPEDECARTEOEIL = 'M';
                    }
                }

                if (TRAMEOUT[4] == '3') {
                    TYPEDECARTEOEIL = 'E';
                    if (TYPECARTE[2] != 'E') //RESISTIVE
                    {
                        TRAMEOUT[11] = '0';
                        TRAMEOUT[12] = '1';
                        TYPEDECARTEOEIL = 'M';
                    }
                }

                if (TRAMEOUT[4] == '4') {
                    TYPEDECARTEOEIL = 'E';
                    if (TYPECARTE[3] != 'E') //RESISTIVE
                    {
                        TRAMEOUT[11] = '0';
                        TRAMEOUT[12] = '1';
                        TYPEDECARTEOEIL = 'M';
                    }
                }

                if (TRAMEOUT[4] == '5') {
                    TYPEDECARTEOEIL = 'E';
                    if (TYPECARTE[4] != 'E') //RESISTIVE
                    {
                        TRAMEOUT[11] = '0';
                        TRAMEOUT[12] = '1';
                        TYPEDECARTEOEIL = 'M';
                    }
                }

rep7:
                if (capt_exist == 11) //CAD A DIRE FONCTION OEIL AUTOMATE CONFIRME LEXISTENCE DE CE CAPTEUR
                {
                    if (tmp2 <= 10) //10 ESSAIS
                    {
                        AQUIENVOYER(ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
                        delay_ms(100); //attend un peu avant d'envoyer
                        //METTRE EN MODE RELATIF
                        TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A LA CARTE CONCERNEE
                        TRAMERECEPTIONRS485DONNEES(15, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
                        controle_CRC(CRC, TRAMEIN);
                        if (NORECEPTION == 0xFF) // RIEN REcU
                        {
                            tmp2 = tmp2 + 1;
                            goto rep7;
                        }
                    } else {

                        break;
                    }

                    REPOSAQUIENVOYER(ID2); // ON ARRETE L'OEIL

                    tmp2 = 0; // ON ATTEND LA TRAME
                    tmp = 0;
rep9:
                    if (tmp2 <= 3) //10 ESSAIS
                    {

                        if (TYPEDECARTEOEIL != 'E') //RESISTIVE
                            TRAMERECEPTIONRS485DONNEES(15, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE DU CAPTEUR SELECTIONNE POUR (5) environ 7s
                        if (TYPEDECARTEOEIL == 'E') //ADRESSABLE
                            TRAMERECEPTIONRS485DONNEES(41, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE DU CAPTEUR SELECTIONNE POUR (5) environ 7s
                        controle_CRC(CRC, TRAMEIN);
                        pointeur_out_in = recuperer_trame_memoire(1, 'r', TRAMEIN, TRAMEOUT, VOIECOD, HORLOGE, &ETATZIZCRATIONAUTOMATE, ETAT); //ENREGISTRE
                        if (NORECEPTION == 0xFF) // RIEN REcU
                        {
                            tmp2 = tmp2 + 1;
                            goto rep9;
                        }
                    } else {

                        break;
                    }

                    TRAMEOUT[1] = 'C';
                    TRAMEOUT[2] = 'M';
                    TRAMEOUT[3] = 'A';
                    TRAMEOUT[4] = 'U';
                    TRAMEOUT[5] = 'O';
                    TRAMEOUT[6] = 'E';
                    TRAMEOUT[7] = 'I';

                    for (tmp2 = 8; tmp2 < 93; tmp2++) {
                        TRAMEOUT[tmp2] = TRAMEIN[tmp2 - 7];


                    }
                    pointeur_out_in = 92;

                    calcul_CRC(CRC, TRAMEOUT); //A COMPLETER

                    delay_ms(50);


                } else //LE CAPTEUR NEXISTE PAS ON ENVOIE UN CODE
                {
                    TRAMEOUT[0] = DEBUT_TRAME;
                    TRAMEOUT[1] = 'C';
                    TRAMEOUT[2] = 'M';
                    TRAMEOUT[3] = 'A';
                    TRAMEOUT[4] = 'U';
                    TRAMEOUT[5] = 'O';
                    TRAMEOUT[6] = 'E';
                    TRAMEOUT[7] = 'I';

                    for (tmp2 = 8; tmp2 < 93; tmp2++) {
                        TRAMEOUT[tmp2] = '9';


                    }
                    TRAMEOUT[84] = '0';
                    TRAMEOUT[85] = '0';
                    TRAMEOUT[86] = '0';
                    TRAMEOUT[87] = '0';
                    if (capt_exist == 20) // IL Y A UN CODAGE AUTRE QUI EXISTE
                    {
                        TRAMEOUT[22] = '0';
                        TRAMEOUT[23] = '0';
                        TRAMEOUT[24] = '0';
                        TRAMEOUT[25] = '7';
                    }


                    pointeur_out_in = 92;

                    calcul_CRC(CRC, TRAMEOUT); //A COMPLETER

                    delay_ms(50);


                    TEMPO(3);
                }
                TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A L'AUTOMATE


                SERIALISATION(&ENTREES1, &ENTREES2);
                ENTREES2 = ENTREES2 & 0x02;

                TEMPO(3); // ON ATTEND PLUS RIEN SUR LA LIGNE
            } while (ENTREES2 == 2);



            // TANT QUE LE FLAG DEMANDE DE L'AUTOMATE RESTE A 1 SI LE FLAF PASSE A ZERO LE FLAGUE VERS LA CARTE Rx PASSE A 0

            REPOSAQUIENVOYER(ID2); // ON ARRETE L'OEIL

            funct = 0;

            break;

        default:


            break;



    }
}


////////////////////////////////////////////////////////////////////////////////////////
// UNIQUEMENT POUR CARTE MERE RECOIT exemple CM->R1
//
//DEMANDE D'INFORMATIONS SUR CAPTEUR A LA CARTE MERE par la carte R1
//suivant la fonction obtenu tab , et apres une interruption sur INT0
//
////////////////////////////////////////////////////////////////////////////////////////

void RECUPERATION_DES_TRAMES(unsigned char *tab) {

    unsigned int t;
    int funct;
    char etatIOAU, encoreunefois, AVECEXIST, selectionvoie;
    int voieDEM, codageDEM;
    int voieDEMFINAL, codageDEMFINAL;

    ID1[0] = tab[1];
    ID1[1] = tab[2];

    ID2[0] = tab[3];
    ID2[1] = tab[4];

    FONCTION[0] = tab[5];
    FONCTION[1] = tab[6];
    FONCTION[2] = tab[7];


    funct = (FONCTION[0] << 16)*2; // max 0-255 valeur decimal du code ascii de chaque lettre ex ERR E=69 R=82 E=82
    //methode 69*2 + 82 + 82 = 302
    funct = funct + (FONCTION[1] << 16);
    funct = funct + (FONCTION[2] << 16);

    switch (funct) //analyse de la fonction
    {
        case 291: //ECRIRE SUR SD SUIVANT DEMANDE AU
            INTCONbits.GIE = 0;
            FONCTION[0] = 'D';
            FONCTION[1] = 'D';
            FONCTION[2] = 'R';
            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];

            INSTRUCTION[0] = 'C'; //CYCLIQUE
            ENVOIE_DES_TRAMES(6, 291, INSTRUCTION); //ERCIRE SUR SD


            //INTCONbits.GIE= 0;


            INFORMATION_ERREUR[0] = 'F'; //ON SIGNALE A LAUTOMATE QUE CEST TERMINE
            INFORMATION_ERREUR[1] = 'F';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);
            INTCONbits.GIE = 1;

            break;

        case 319: //RESET RAZ
            INTCONbits.GIE = 0;
            TEMPO(2);
            if ((TRAMEIN[8] == 'r')&&(TRAMEIN[9] == 'e')) {
                pic_eeprom_write(0x04, 0xAA); //TROISIEME RESET
                pic_eeprom_write(0x03, 0xAA); //TROISIEME RESET
                pic_eeprom_write(0x02, 0xAA); //TROISIEME RESET
                pic_eeprom_write(0x01, 0xAA); //TROISIEME RESET
                pic_eeprom_write(0x05, 0xAA); //TROISIEME RESET
                pic_eeprom_write(0x20, 0xFF); //RAZ COMPTEUR
                pic_eeprom_write(0x21, 0xFF); //RAZ COMPTEUR
                pic_eeprom_write(0x22, 0xFF); //RAZ COMPTEUR
                pic_eeprom_write(0x23, 0xFF);
                pic_eeprom_write(0x24, 0xFF);
                pic_eeprom_write(0x25, 0xFF);


                RESETCMS = 1;
                RESETCMS = 1;
            }
            break;

        case 270:

            INTCONbits.GIE = 0;
            TEMPO(5); // pour 30 on a 33s donc 100ms delay_ms enriron 110ms
            FONCTION[0] = 'D';
            FONCTION[1] = 'D';
            FONCTION[2] = 'B';
            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];

            SDRBN(TRAMEIN, TRAMEOUT, CRC, HORLOGE, &NORECEPTION); //PROCEDURE DE LECTURE CARTE SD ET PROGRAMMATION CM
            INTCONbits.GIE = 1;
            break;


        case 286: //LIRE CARTE SD POUR REPROG LA CM DEMANDE FAITE PAR L'AUTOMATE


            INTCONbits.GIE = 0;
            TEMPO(5); // pour 30 on a 33s donc 100ms delay_ms enriron 110ms
            FONCTION[0] = 'D';
            FONCTION[1] = 'D';
            FONCTION[2] = 'R';
            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];

            pointeur_out_in = SDR(TRAMEIN, TRAMEOUT, VOIECOD, INSTRUCTION,&PRESENCEDEBITSD, HORLOGE); //PROCEDURE DE LECTURE CARTE SD ET PROGRAMMATION CM

            INFORMATION_ERREUR[0] = 'F'; //ON SIGNALE A LAUTOMATE QUE CEST TERMINE
            INFORMATION_ERREUR[1] = 'F';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);
            INTCONbits.GIE = 1;

            break;



        case 332: //DEMANDE LISTE VOIES PAR ROBINET

            INTCONbits.GIE = 0; // ON INTERDIT TTE DEMANDES
            FONCTION[0] = 'R';
            FONCTION[1] = 'T';
            FONCTION[2] = 'T';
            pointeur_out_in = 14; // SAUVEGARDE POINTEUR DU TABLEAU
            controle_CRC;

            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];

            INSTRUCTION[0] = TRAMEIN[8];
            INSTRUCTION[1] = TRAMEIN[9];
            TRAMEOUT[8] = INSTRUCTION[0];
            TRAMEOUT[9] = INSTRUCTION[1];
            TROUVER_ROBINET(DEMANDE_INFOS_CAPTEURS, INSTRUCTION, TMP);
            for (t = 0; t <= 10; t++) {
                IntToChar(INSTRUCTION[2 + t], DEMANDE_INFOS_CAPTEURS, 3);
                TRAMEOUT[10 + 3 * t] = DEMANDE_INFOS_CAPTEURS[0];
                TRAMEOUT[11 + 3 * t] = DEMANDE_INFOS_CAPTEURS[1];
                TRAMEOUT[12 + 3 * t] = DEMANDE_INFOS_CAPTEURS[2];
            }
            pointeur_out_in = 43; // SAUVEGARDE POINTEUR DU TABLEAU
            calcul_CRC(CRC, TRAMEOUT);
            TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A LA CARTE CONCERNEE

            break;



        case 298: //DEMANDE DE PRISE OU D'ARRET DE L'AUTOMATE REA


            INTCONbits.GIE = 0; // ON INTERDIT TTE DEMANDES
            FONCTION[0] = 'R';
            FONCTION[1] = 'E';
            FONCTION[2] = 'A';
            pointeur_out_in = 14; // SAUVEGARDE POINTEUR DU TABLEAU
            controle_CRC;

            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];
            FONCTION_ACTIVATION_AUTO = 0x00; //ACTIVATION AUTO
            if ((TRAMEIN[8] == 'F') && (TRAMEIN[9] == 'F')) //ACTIVATION PRISE DE MAIN AUTOMATE
                FONCTION_ACTIVATION_AUTO = 0xFF; //ACTIVATION AUTO

            if (FONCTION_ACTIVATION_AUTO == 0xFF) {
                if (FIRST_DEMAR != 0XFF) {
                    stopIO(&ETAT_IO);
                }
            }


            IDENTIF_TYPE_CARTE(TMP, TYPECARTE, VDD1, VDD2, VDD3, VDD4, VDD5, VBAT, R1NOREP, R2NOREP, R3NOREP, R4NOREP, R5NOREP);

            TRAMEOUT[8] = TRAMEIN[8];
            if (ETAT_IO == 0x00)
                TRAMEOUT[9] = 'F'; //MAJ PAT !
            else
                TRAMEOUT[9] = 'F';

            TRAMEOUT[10] = TYPECARTE[0];
            TRAMEOUT[11] = TYPECARTE[1];
            TRAMEOUT[12] = TYPECARTE[2];
            TRAMEOUT[13] = TYPECARTE[3];
            TRAMEOUT[14] = TYPECARTE[4];

            pointeur_out_in = 15; // SAUVEGARDE POINTEUR DU TABLEAU
            calcul_CRC(CRC, TRAMEOUT);
            TEMPO(1);
            delay_ms(200); //ATTENTE PR AUTO
            TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A LA CARTE CONCERNEE
            TEMPO(1);
            //DRS485_IO=0; //ON SIGNALE A ON SIGNALE A LA CARTE IO DE REPRENDRE SON BOULOT
            PORTBbits.RB4 = 0;
            INTCONbits.GIE = 1; //
            break;

        case 336: //IO DEMANDE A VOIR PARAM

            INTCONbits.GIE = 0; // ON INTERDIT TTE DEMANDES
            FONCTION[0] = 'Z';
            FONCTION[1] = 'P';
            FONCTION[2] = 'L';

            DEMANDEZPLIO = DEMANDEZPLIO + 1;

            pointeur_out_in = 14; // SAUVEGARDE POINTEUR DU TABLEAU
            controle_CRC;

            LIRE_EEPROM(6, 1024, TRAMEOUT, 5);
            pointeur_out_in = 115;
            calcul_CRC(CRC, TRAMEOUT); // CALCUL LE CRC ET LE RAJOUTE AU TABLEAU TRAMEOUT[]
            TRAMEOUT[pointeur_out_in + 5] = FIN_TRAME; // RAJOUT LE BYTE DE STOP

            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];

            if (INT0 == 1) //une demande de l'autmate a ete faite !!!!!!!! pas sur interruption juste touche
            {
                SERIALISATION(&ENTREES1, &ENTREES2); // VOIR SI DEMANDE DE L'AUTO
                if ((ENTREES2 & 0x02) == 0x02) {

                    PRIO_AUTO_IO = 0x00;
                    TEMPO(1);
                    identificationINTER(6, 0x00); // ON EST OK POUR LA DEMANDE DE L'AUTO PAR DEFAUT ICI

                    goto STOPPERZPL;
                }
            }
            delay_ms(200);
            TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A LA CARTE CONCERNEE
            if (DEMANDEZPLIO == 2) {
                delay_ms(200); //27102020
                TRAMEOUT[3] = 'A';
                TRAMEOUT[4] = 'U';
                TRAMEOUT[5] = 'I';
                TRAMEOUT[6] = 'N';
                TRAMEOUT[7] = 'I';
                TRAMEOUT[8] = 'T';
                TRAMEOUT[9] = FIN_TRAME;
                TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A LA CARTE CONCERNEE
            }

STOPPERZPL:

            INTCONbits.GIE = 1;


            break;


        case 341: //IO DEMANDE A VOIR L'HEURE

            INTCONbits.GIE = 0; // ON INTERDIT TTE DEMANDES
            FONCTION[0] = 'Z';
            FONCTION[1] = 'O';
            FONCTION[2] = 'R';

            pointeur_out_in = 14; // SAUVEGARDE POINTEUR DU TABLEAU
            controle_CRC;


            if (INT0 == 1) //une demande de l'autmate a ete faite !!!!!!!! pas sur interruption juste touche
            {
                SERIALISATION(&ENTREES1, &ENTREES2); // VOIR SI DEMANDE DE L'AUTO
                if ((ENTREES2 & 0x02) == 0x02) {

                    PRIO_AUTO_IO = 0x00;
                    TEMPO(1);
                    identificationINTER(6, 0x00); // ON EST OK POUR LA DEMANDE DE L'AUTO PAR DEFAUT ICI

                    goto STOPPERZOR;
                }
            }

            //VOIR L'HEURE
            creer_trame_horloge(HORLOGE);
            ENVOIE_DES_TRAMES(7, 305, HORLOGE); //CARTE IO

STOPPERZOR:


            break;

        case 340: //ZPP RECOIT PARAM CMS

            INTCONbits.GIE = 0; // ON INTERDIT TTE DEMANDES
            FONCTION[0] = 'Z';
            FONCTION[1] = 'P';
            FONCTION[2] = 'P';

            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];

            pointeur_out_in = 120; // SAUVEGARDE POINTEUR DU TABLEAU
            controle_CRC;

            ECRIRE_EEPROMBYTE(6, 1024, TRAMEIN, 5); //PARAM 1

            INFORMATION_ERREUR[0] = 'F'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = 'F';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);
            LIRE_EEPROM(6, 1024, TRAMEOUT, 5);
            RESETCMS = 1; //RESET CMS
            INTCONbits.GIE = 1;

            break;



        case 329: //CREATION CAPTEUR DEPUIS AUTOMATE OU IO

            INTCONbits.GIE = 0; // ON INTERDIT TTE DEMANDES
            FONCTION[0] = 'Z';
            FONCTION[1] = 'C';
            FONCTION[2] = 'R';

            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];

            pointeur_out_in = 8; // SAUVEGARDE POINTEUR DU TABLEAU
            controle_CRC;

            // AUTOM ENV DES ' ' au lieu de '0'
            if (TRAMEIN[8] == 0x20)
                TRAMEIN[8] = '0';
            if (TRAMEIN[9] == 0x20)
                TRAMEIN[9] = '0';
            if (TRAMEIN[10] == 0x20)
                TRAMEIN[10] = '0';
            if (TRAMEIN[11] == 0x20)
                TRAMEIN[11] = '0';
            if (TRAMEIN[12] == 0x20)
                TRAMEIN[12] = '0';
            if (TRAMEIN[69] == 0x20)
                TRAMEIN[69] = '0';
            if (TRAMEIN[70] == 0x20)
                TRAMEIN[70] = '0';
            if (TRAMEIN[71] == 0x20)
                TRAMEIN[71] = '0';
            if (TRAMEIN[72] == 0x20)
                TRAMEIN[72] = '0';

            if (TRAMEIN[77] == 0x20)
                TRAMEIN[77] = '0';

            if (TRAMEIN[78] == 0x20)
                TRAMEIN[78] = '0';
            if (TRAMEIN[79] == 0x20)
                TRAMEIN[79] = '0';
            if (TRAMEIN[80] == 0x20)
                TRAMEIN[80] = '0';
            if (TRAMEIN[81] == 0x20)
                TRAMEIN[81] = '0';


            if (TRAMEIN[57] == 0x20)
                TRAMEIN[57] = '0';
            if (TRAMEIN[58] == 0x20)
                TRAMEIN[58] = '0';
            if (TRAMEIN[59] == 0x20)
                TRAMEIN[59] = '0';
            //SEUIL _ _ _ 0
            if (TRAMEIN[88] == 0x20)
                TRAMEIN[88] = '0';
            if (TRAMEIN[89] == 0x20)
                TRAMEIN[89] = '0';
            if (TRAMEIN[90] == 0x20)
                TRAMEIN[90] = '0';
            if (TRAMEIN[91] == 0x20)
                TRAMEIN[91] = '0';

            /////FIN CORRECTION AUTOMATE

            VOIECOD[0] = TRAMEIN[8];
            VOIECOD[1] = TRAMEIN[9];
            VOIECOD[2] = TRAMEIN[10];
            VOIECOD[3] = TRAMEIN[11];
            VOIECOD[4] = TRAMEIN[12];

            etatIOAU = 0;

            if (TRAMEIN[3] == 'D') // CARTE SD
                etatIOAU = 'D';
            if ((TRAMEIN[1] == 'A')&&(TRAMEIN[3] != 'D')) // AUTOMATE ne pas envoyer ERR00 29/03/18
            {
                etatIOAU = 'U';
                TEMPO(1);
                goto AU1;
            }

            if (TRAMEIN[3] != 'D') //PAS CARTE SD
            {
                INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
                INFORMATION_ERREUR[1] = '0';
                for (t = 2; t < 20; t++) {
                    INFORMATION_ERREUR[t] = 'x';
                }
                etatIOAU = 'U';
                if (TRAMEIN[1] == 'I') //CARTE I/O
                {
                    ENVOIE_DES_TRAMES(7, 302, INFORMATION_ERREUR);
                    etatIOAU = 'I';
                } else
                    ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);

            }

AU1:

            // ICI ON RECOIT TRAME AVEC VERITABLE NUMERO CABLE ex 041 (carte RI 3)
            // PUIS ON TRANSFORME EN NUMERO VOIE RELATIVE A LA CARTE 001

            //		CALCUL DE RI

            if ((VOIECOD[1] == '1') || (VOIECOD[1] == '0'))
                numdecarteRI = 1;



            if ((VOIECOD[1] == '3') || (VOIECOD[1] == '2'))
                numdecarteRI = 2;



            if ((VOIECOD[1] == '5') || (VOIECOD[1] == '4'))
                numdecarteRI = 3;



            if ((VOIECOD[1] == '7') || (VOIECOD[1] == '6'))
                numdecarteRI = 4;


            if ((VOIECOD[1] == '9') || (VOIECOD[1] == '8'))
                numdecarteRI = 5;


            if ((VOIECOD[1] == '2')&&(VOIECOD[2] == '0'))
                numdecarteRI = 1;
            if ((VOIECOD[1] == '4')&&(VOIECOD[2] == '0'))
                numdecarteRI = 2;
            if ((VOIECOD[1] == '6')&&(VOIECOD[2] == '0'))
                numdecarteRI = 3;
            if ((VOIECOD[1] == '8')&&(VOIECOD[2] == '0'))
                numdecarteRI = 4;
            if ((VOIECOD[0] == '1')&&(VOIECOD[1] == '0')&&(VOIECOD[2] == '0'))
                numdecarteRI = 5;


            //ON TRANSFORME L'ABSOLU EN RELATIF
            if (VOIECOD[0] == '1') // voie 100
            {
                VOIECOD[0] = '0';
                VOIECOD[1] = '2'; //100
                VOIECOD[2] = '0';
            } else
                //voie 0xy
            {

                if (((VOIECOD[1] == '2') || (VOIECOD[1] == '4') || (VOIECOD[1] == '6') || (VOIECOD[1] == '8') || (VOIECOD[1] == '0'))&&(VOIECOD[2] != '0')) //on test x si x paire alors ->Voie relative 0
                    VOIECOD[1] = '0';

                if (((VOIECOD[1] == '2') || (VOIECOD[1] == '4') || (VOIECOD[1] == '6') || (VOIECOD[1] == '8') || (VOIECOD[1] == '0'))&&(VOIECOD[2] == '0')) //on test x si x paire alors ->Voie relative 0
                    VOIECOD[1] = '2';

                if ((VOIECOD[1] == '1') || (VOIECOD[1] == '3') || (VOIECOD[1] == '5') || (VOIECOD[1] == '7') || (VOIECOD[1] == '9')) //on test x si x impaire alors ->Voie relative 1 sauf si y est de 0
                    VOIECOD[1] = '1'; //sinon c la voie relative 01y

            }

            // FIN MULTI A
            TRAMEIN[8] = VOIECOD[0];
            TRAMEIN[9] = VOIECOD[1];
            TRAMEIN[10] = VOIECOD[2];

            TRAMEIN[57] = VOIECOD[0];
            TRAMEIN[58] = VOIECOD[1];
            TRAMEIN[59] = VOIECOD[2];

            if (etatIOAU != 'I') //PAS CARTE I/O
            {
                // L'AUTO ENVOIE  XXXX
                if (TRAMEIN[68] != '?')
                    TRAMEIN[68] = '?';
                if (TRAMEIN[73] != '?')
                    TRAMEIN[73] = '?';
                if (TRAMEIN[74] != '?')
                    TRAMEIN[74] = '?';
                if (TRAMEIN[75] != '?')
                    TRAMEIN[75] = '?';
                if (TRAMEIN[76] != '?')
                    TRAMEIN[76] = '?';


            }

            ENVOIE_DES_TRAMES(numdecarteRI, 285, TRAMEIN);

            // ICI IL FAUT RETRANSFORMER EN ABSOLU

            VOIECOD[1] = (VOIECOD[1] - 48) + 2 * (numdecarteRI - 1);
            if (VOIECOD[1] == 10) //VOIE 100
            {
                VOIECOD[0] = '1';
                VOIECOD[1] = '0';
                VOIECOD[2] = '0';
            } else {
                VOIECOD[1] = VOIECOD[1] + 48;
            }


            //TRAMEOUT CONTIENT LES DONNEES
            TRAMEOUT[8] = VOIECOD[0];
            TRAMEOUT[9] = VOIECOD[1];
            TRAMEOUT[10] = VOIECOD[2];

            if (etatIOAU == 'D') //TRAVAILLE AVEC CARTE SD
            {

                if ((PRESENCEDEBITSD == 0X00)&&(VOIECOD[3] == '0')&&(VOIECOD[4] == '0'))
                    goto NEPASVALIDER;

            }

            pointeur_out_in = VALIDER_CAPTEUR_CM(TRAMEIN, TRAMEOUT, VOIECOD, HORLOGE, &ETATZIZCRATIONAUTOMATE, TMP, ETAT, NBRECAPTEURS_CABLE); // ICI ON VALIDE DANS LA TABLE D'EXISTENCE  ET ON ECRIT LES TRAMES EN CM avec la copie du commentaire cable seulement en codage 0
NEPASVALIDER:
            if (etatIOAU != 'D') //SI PAS CARTE SD
            {
                INFORMATION_ERREUR[0] = 'F'; //TT EST BON donc utilisation type d'erreur
                INFORMATION_ERREUR[1] = 'F';
                for (t = 2; t < 20; t++) {
                    INFORMATION_ERREUR[t] = 'x';
                }

                if (etatIOAU == 'I') //CARTE I/O
                {
                    ENVOIE_DES_TRAMES(7, 302, INFORMATION_ERREUR);
                } else {


                    ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);


                }


            }
            PRIO_AUTO_IO = 0x00;
            INTCONbits.GIE = 1;

            break;

        case 333: //table existence depuis AU OU IO

            INTCONbits.GIE = 0; //AUTORISER LES INTERRUPTIONS GENERALES
            PRIO_AUTO_IO = 0xF1;
            FONCTION[0] = 'Z';
            FONCTION[1] = 'T';
            FONCTION[2] = 'E';

            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];

            DEMANDE_INFOS_CAPTEURS[0] = TRAMEIN[8]; // QUELLE CARTE
            DEMANDE_INFOS_CAPTEURS[1] = TRAMEIN[9];




            pointeur_out_in = 10; // SAUVEGARDE POINTEUR DU TABLEAU
            calcul_CRC(CRC, TRAMEOUT);


            funct = TRAMEIN[9] - 48;
            selectionvoie = 0x00;
            if (TRAMEIN[8] >= 65) //CODE ASCII DE A a xxxxxxx
            {


                selectionvoie = TRAMEIN[8] - 64; //SI 'A' on a 1 en decimal  donc demande d'une voie



            }




            RS485('%'); //% et $
            voieDEM = funct;

            t = 0;
            for (t = (1 + 20 * (voieDEM - 1)); t <= (20 * voieDEM); t++) {

                SERIALISATION(&ENTREES1, &ENTREES2); // VOIR SI DEMANDE DE L'AUTO
                if ((ENTREES2 & 0x02) == 0x02) //CAR DANS LE PRG CARTE IO RH5 DEMANDE EST LONGTEMPS HAUT
                    PRIO_AUTO_IO = 0xE1;


                funct = 128 * (t) + 30000;
                LIRE_EEPROM(3, funct, TMP, 4); //LIRE DANS EEPROM
                delay_ms(50);

                if ((selectionvoie == (t - (20 * (voieDEM - 1)))) || (selectionvoie == 0x00)) //SI DEMANDE UNE VOIE PAR L'AUTO OU TTE LA TABEL
                {
                    if (TRAMEIN[1] == 'I') //CARTE I/O
                    {
                        ENVOIE_DES_TRAMES(7, 333, TMP);
                    } else {
                        if (TMP[2] == 'r')
                            TMP[4] = 'R';
                        ENVOIE_DES_TRAMES(6, 333, TMP);
                    }
                }
            }
            RS485('$'); //% et $
            RS485('$'); //% et $
            RS485('$'); //% et $

            delay_ms(100);

            RS485('$'); //% et $
            RS485('$'); //% et $
            RS485('$'); //% et $

            if (PRIO_AUTO_IO == 0xE1) //ON A DETECTER UNE DEMANDE DE L'AU pendant IO
                identificationINTER(6, 0x00); // ON FORCE MANUELLEMENT en MODE NORMALE POUR VALIDER LACTION DE IO

            if (PRIO_AUTO_IO == 0xE1)
                identificationINTER(6, 0x00); // ON EST OK POUR LA DEMANDE DE L'AUTO PAR DEFAUT ICI

            PRIO_AUTO_IO = 0x00;
            INTCONbits.GIE = 1; //AUTORISER LES INTERRUPTIONS GENERALES

            break;



        case 302: //ERR DU RECEPTEUR

            funct = 0;
            TYPE_ERREUR[0] = tab[8];
            TYPE_ERREUR[1] = tab[9];


            funct = (TYPE_ERREUR[1]) - 48;
            funct = funct + 10 * (TYPE_ERREUR[0] - 48);
            //funct = funct + (TYPE_ERREUR[1]);
            t = 0;
            for (t = 0; t < 18; t++) {
                INFORMATION_ERREUR[t] = tab[t + 10];
            }

            pointeur_out_in = 27;
            controle_CRC(CRC, TRAMEIN);


            switch (funct) //ACTION A FAIRE POUR CHAQUE ERREUR ......
            {
                case 0: // OK CA C EST BIEN PASSEE

                    //delay_ms(100);
                    break;



                case 1: //carte occup�e



                    break;


                case 2: //erreur CRC


                    break;


                case 3: //voies d�fectueuses


                    break;








                default:


                    break;



            }





        case 343: //DEMANDE AU OU IO DES DONNEES DIC capteurs

            INTCONbits.GIE = 0;
            //DRS485_IO=0; //ON STOPPE LES DEMANDES DE LA CA1RTE IO
            FONCTION[0] = 'Z';
            FONCTION[1] = 'I';
            FONCTION[2] = 'Z';
            PRIO_AUTO_IO = 0xCC;
            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];

            MAXBITE = 0;

            //CORRECTION AU
            if (TRAMEIN[8] == 0x20)
                TRAMEIN[8] = '0';
            if (TRAMEIN[9] == 0x20)
                TRAMEIN[9] = '0';
            if (TRAMEIN[10] == 0x20)
                TRAMEIN[10] = '0';
            if (TRAMEIN[11] == 0x20)
                TRAMEIN[11] = '0';
            if (TRAMEIN[12] == 0x20)
                TRAMEIN[12] = '0';

            if (TRAMEIN[14] == 0x20)
                TRAMEIN[14] = '0';
            if (TRAMEIN[15] == 0x20)
                TRAMEIN[15] = '0';
            if (TRAMEIN[16] == 0x20)
                TRAMEIN[16] = '0';
            if (TRAMEIN[17] == 0x20)
                TRAMEIN[17] = '0';
            if (TRAMEIN[18] == 0x20)
                TRAMEIN[18] = '0';
            // FIN AUTO


            temoinfinvoieZIZ = 0;

            DEMANDE_INFOS_CAPTEURS[0] = TRAMEIN[8];
            DEMANDE_INFOS_CAPTEURS[1] = TRAMEIN[9];
            DEMANDE_INFOS_CAPTEURS[2] = TRAMEIN[10]; //VOIE DEPART
            DEMANDE_INFOS_CAPTEURS[3] = TRAMEIN[11];
            DEMANDE_INFOS_CAPTEURS[4] = TRAMEIN[12]; // CODAGE DEPART

            DEMANDE_INFOS_CAPTEURS[5] = TRAMEIN[14];
            DEMANDE_INFOS_CAPTEURS[6] = TRAMEIN[15];
            DEMANDE_INFOS_CAPTEURS[7] = TRAMEIN[16]; //VOIE FINALE
            DEMANDE_INFOS_CAPTEURS[8] = TRAMEIN[17];
            DEMANDE_INFOS_CAPTEURS[9] = TRAMEIN[18]; // CODAGE FINALE
            TYPECOMMANDE = TRAMEIN[13];

            voieDEM = (100 * (DEMANDE_INFOS_CAPTEURS[0] - 48))+(10 * (DEMANDE_INFOS_CAPTEURS[1] - 48))+(DEMANDE_INFOS_CAPTEURS[2] - 48);
            codageDEM = (10 * (DEMANDE_INFOS_CAPTEURS[3] - 48))+(DEMANDE_INFOS_CAPTEURS[4] - 48);
            voieDEMFINAL = (100 * (DEMANDE_INFOS_CAPTEURS[5] - 48))+(10 * (DEMANDE_INFOS_CAPTEURS[6] - 48))+(DEMANDE_INFOS_CAPTEURS[7] - 48);
            codageDEMFINAL = (10 * (DEMANDE_INFOS_CAPTEURS[8] - 48))+(DEMANDE_INFOS_CAPTEURS[9] - 48);

            AVECEXIST = 0x00;
            encoreunefois = 0x00;
            if ((ID1[0] == 'A')&&(ID1[1] == 'U')) {
                AVECEXIST = 0xff; //EXISTENCE CABLE uniquement TP
            }

            if ((voieDEM == voieDEMFINAL)&&(codageDEM == codageDEMFINAL)) //SI ON DEMANDE UN CABLE
            {
                encoreunefois = 0xFF;
                AVECEXIST = 0xff; //EXISTENCE CABLE
                if (TRAMEIN[13] == '+') //DEBIMETRE
                    AVECEXIST = 0xdd; //EXISTENCE DEBITMETRE
            }
            delay_ms(200);

            RS485('%');
            if ((ID1[0] == 'I')&&(ID1[1] == 'O')) {
                tmp = RECEVOIR_RS485(4, &NORECEPTION);
                if (tmp == '?')
                    RS485('%');

            }

refaire:

            funct = 128 * voieDEM + 30000;
            LIRE_EEPROM(3, funct, SAVTAB, 4); //LIRE DANS EEPROM lexistance de toutes le voies

CONTINUER:


            TMP[0] = DEBUT_TRAME;
            TMP[1] = DEMANDE_INFOS_CAPTEURS[0];
            TMP[2] = DEMANDE_INFOS_CAPTEURS[1];
            TMP[3] = DEMANDE_INFOS_CAPTEURS[2];
            TMP[4] = DEMANDE_INFOS_CAPTEURS[3];
            TMP[5] = DEMANDE_INFOS_CAPTEURS[4];

            //////////////////
            if (TYPECOMMANDE == '/') {

                if (voieDEM > (voieDEMFINAL)) //R1 R2 ....toujours jusqua la fin	//	if (voieDEM>(voieDEMFINAL+20)) //R1 R2 ....toujours jusqua la fin
                    goto STOPPERZIZ0;



                if (SAVTAB[2] == 'r') //Voie RESISTIVE
                {
                    if (codageDEM == 0) //envoyer le codage 0
                        goto continuer0;
                    if (codageDEM == 1) //envoyer le codage 1
                        goto continuer0;
                }

                if (SAVTAB[2] == 'a') //Voie ADRESSABLE
                {
                    if (codageDEM == 0) //envoyer le codage 0
                        goto continuer0;
                    if (SAVTAB[codageDEM + 3] == 'A') //envoyer le codage
                        goto continuer0;
                }

                if (SAVTAB[2] == 'i') //VOIE INCIDENTE
                {
                    goto continuer0; // ENVOYER TOUT
                }


                if (SAVTAB[2] == 'c') //VOIE CC
                {
                    goto continuer0; // ENVOYER TOUT
                }

                if (SAVTAB[2] == 'h') //FUS
                {
                    goto continuer0; // ENVOYER TOUT
                }


                codageDEM = codageDEM + 1;

                if (codageDEM == 17) {
                    codageDEM = 0;
                    voieDEM = voieDEM + 1;
                }


                IntToChar(voieDEM, TMP, 3);
                DEMANDE_INFOS_CAPTEURS[0] = TMP[0];
                DEMANDE_INFOS_CAPTEURS[1] = TMP[1];
                DEMANDE_INFOS_CAPTEURS[2] = TMP[2];

                IntToChar(codageDEM, TMP, 2);
                DEMANDE_INFOS_CAPTEURS[3] = TMP[0];
                DEMANDE_INFOS_CAPTEURS[4] = TMP[1];


                if ((voieDEM < (voieDEMFINAL + 1))) //if ((voieDEM<(voieDEMFINAL+21)))
                    goto refaire;
                else
                    goto STOPPERZIZ0;
            }

continuer0:

            if (INT0 == 1)
                MAXBITE = 1;

            LIRE_EEPROM(choix_memoire(TMP), calcul_addmemoire(TMP), TRAMEIN, 1); //LIRE DANS EEPROM
            //ICI

            FONCTION[0] = 'Z';
            FONCTION[1] = 'I';
            FONCTION[2] = 'Z';

            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];



            pointeur_out_in = TRANSFORMER_TRAME_MEM_EN_COM(TRAMEIN, TRAMEOUT, VOIECOD); //le pointeur est memoris� dans ce sous prg

            calcul_CRC(CRC, TRAMEOUT); // CALCUL LE CRC ET LE RAJOUTE AU TABLEAU TRAMEOUT[]


            if (AVECEXIST == 0xff) {
                if (codageDEM == 0) //CABLE
                    CHERCHER_DONNNEES_CABLES(DEMANDE_INFOS_CAPTEURS, 111, TMP, VOIECOD, ETAT, &capt_exist); //VOIR EXITENCE CABLE !!!!!!!
                if (codageDEM > 0) //CAPTEUR
                    CHERCHER_DONNNEES_CABLES(DEMANDE_INFOS_CAPTEURS, codageDEM, TMP, VOIECOD, ETAT, &capt_exist); //VOIR EXITENCE CAPTEUR !!!!!!!

                NUMEROCARTE = TMP[1]; //NUMERO DE LA VOIE
                if ((NUMEROCARTE >= 1)&&(NUMEROCARTE <= 20))
                    NUMEROCARTE = 1;
                if ((NUMEROCARTE >= 21)&&(NUMEROCARTE <= 40))
                    NUMEROCARTE = 2;
                if ((NUMEROCARTE >= 41)&&(NUMEROCARTE <= 60))
                    NUMEROCARTE = 3;
                if ((NUMEROCARTE >= 61)&&(NUMEROCARTE <= 80))
                    NUMEROCARTE = 4;
                if ((NUMEROCARTE >= 81)&&(NUMEROCARTE <= 100))
                    NUMEROCARTE = 5;


                ETATZIZCRATIONAUTOMATE = ETAT[0]; //L'ETAT EST EN MEMOIRE
                if (capt_exist == 0x00) {
                    ETATZIZCRATIONAUTOMATE = 'F'; //L'ETAT NEXISTE PAS
                    for (funct = 8; funct < 96; funct++) {
                        TRAMEOUT[funct] = 'F'; //ON EFFACE TT
                    }
                    if ((TYPECARTE[NUMEROCARTE - 1] == 'M')&&(VOIECOD[4] == '1')) //SI VOIE RESISTIVE VIDE !!!!!!! pour AU
                        TRAMEOUT[13] = 'R';
                    if ((TYPECARTE[NUMEROCARTE - 1] == 'E')&&(VOIECOD[4] == '1')) //SI VOIE RESISTIVE VIDE !!!!!!! pour AU
                        TRAMEOUT[13] = 'A';
                    TRAMEOUT[96] = '&'; //POUR L'AUTOMATE QUI SERA QUE LE CABLE OU CAPTEUR NEXISTE PAS !!!!
                    TRAMEOUT[97] = FIN_TRAME; //FIN
                }
                goto capteursZIZET;
            }


            if (AVECEXIST == 0xdd) {

                CHERCHER_DONNNEES_CABLES(DEMANDE_INFOS_CAPTEURS, 0, TMP, VOIECOD, ETAT, &capt_exist); //VOIR EXITENCE DEBITMETRE !!!!!!!


                if (capt_exist == 0x00) {

                    TRAMEOUT[96] = '&'; //POUR L'AUTOMATE QUI SERA QUE LE DEBITMETRE NEXISTE PAS !!!!

                }

            }

            
            
            
            
            
            
            
            
            
            TRAMEOUT[pointeur_out_in + 5] = FIN_TRAME; // RAJOUT LE BYTE DE STOP


            TRAMEOUT[8] = TMP[1]; //PREFERABLE CAR SI MEMOIRE VIDE ....
            TRAMEOUT[9] = TMP[2];
            TRAMEOUT[10] = TMP[3];
            TRAMEOUT[11] = TMP[4];
            TRAMEOUT[12] = TMP[5];
capteursZIZET:

            delay_ms(100); // modif 10/08/17 ATTENDRE CAR LA CARTE MERE ECRIT DANS EEPROOM 5ms
            delay_ms(100);
            funct = 0;

finATTENTEZIZ:

            TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A LA CARTE CONCERNEE
            if ((ID1[0] == 'A')&&(ID1[1] == 'U')) {
                TRAMERECEPTIONRS485DONNEES(2, &NORECEPTION, TRAMEIN);
            } else {
                TRAMERECEPTIONRS485DONNEES(15, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE ICI AVEC TIMER  //AVEC ERR00 avec timer reglable 60 origine
            }
            if (TRAMEIN[2] == '$')
                NORECEPTION = 0xFF; //CAS PARTICULIER AVEC COMIO MODEM

            if (NORECEPTION == 0xFF) // SI UNE NON RECEPTION ON ARRETE SANS DOUTE LE MINITEL !!!! IO
            {

                if ((ID1[0] == 'A')&&(ID1[1] == 'U')&&(funct < 3)) {
                    funct++;
                    goto finATTENTEZIZ;
                }
                funct++;
                if (funct >= 1) {

                    goto STOPPERZIZ0;

                } else
                    goto finATTENTEZIZ;
            }

            funct = 0;
            if ((TRAMEIN[8] == 'A')&&(TRAMEIN[9] == 'A')) //CARTE IO DEMANDE DE STOPPER CONTROLER SI CA N'EST PAS L'AUTO ??????????????qui est prioritaire
                goto STOPPERZIZ;

            if (MAXBITE == 1) //une demande de l'autmate a ete faite !!!!!!!! pas sur interruption juste touche
            {
                MAXBITE = 0;
                SERIALISATION(&ENTREES1, &ENTREES2); // VOIR SI DEMANDE DE L'AUTO
                if ((ENTREES2 & 0x02) == 0x02) {
                    RS485('$'); //FIN DE SUPERTRAME
                    RS485('$'); //FIN DE SUPERTRAME
                    RS485('$'); //FIN DE SUPERTRAME
                    RS485('A'); //FIN DE SUPERTRAME
                    RS485('U'); //FIN DE SUPERTRAME	
                    PRIO_AUTO_IO = 0x00;
                    identificationINTER(6, 0x00); // ON EST OK POUR LA DEMANDE DE L'AUTO PAR DEFAUT ICI
                    goto STOPPERZIZ;
                }
            }

            codageDEM = codageDEM + 1;

            if (codageDEM == 17) {
                temoinfinvoieZIZ = 1;
                codageDEM = 0;
                voieDEM = voieDEM + 1;
            }



            IntToChar(voieDEM, TMP, 3);
            DEMANDE_INFOS_CAPTEURS[0] = TMP[0];
            DEMANDE_INFOS_CAPTEURS[1] = TMP[1];
            DEMANDE_INFOS_CAPTEURS[2] = TMP[2];

            IntToChar(codageDEM, TMP, 2);
            DEMANDE_INFOS_CAPTEURS[3] = TMP[0];
            DEMANDE_INFOS_CAPTEURS[4] = TMP[1];

            if (((codageDEM == codageDEMFINAL)&&(voieDEM == voieDEMFINAL))&&(encoreunefois != 0xFF)) {
                encoreunefois = 0xFF;
                goto CONTINUER;
            }






            if (encoreunefois == 0x00)//SI PAS FIN
            {
                if (TYPECOMMANDE == '/') {
                    if (temoinfinvoieZIZ == 1) {
                        temoinfinvoieZIZ = 0;
                        goto refaire;
                    }
                }
                goto CONTINUER;
            }

            delay_ms(300); // ATTENDRE CAR LA CARTE MERE ECRIT DANS EEPROOM 5ms
            RS485('$'); //FIN DE SUPERTRAME
            RS485('$'); //FIN DE SUPERTRAME
            RS485('$'); //FIN DE SUPERTRAME

            delay_ms(300); // ATTENDRE CAR LA CARTE MERE ECRIT DANS EEPROOM 5ms
STOPPERZIZ0:
            RS485('$'); //FIN DE SUPERTRAME
            RS485('$'); //FIN DE SUPERTRAME
            RS485('$'); //FIN DE SUPERTRAME

STOPPERZIZ:
            //TEMPO(2);
            DRS485_IO = 0;
            DRS485_IO = 0; // ON REAUTORISE
            INTCONbits.GIE = 1;
            break;

        case 320: //EFFACER CAPTEURS DEPUIS AU OU IO

            INTCONbits.GIE = 0;
            FONCTION[0] = 'Z';
            FONCTION[1] = 'F';
            FONCTION[2] = 'F';

            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];

            //CORRECTION AU
            if (TRAMEIN[8] == 0x20)
                TRAMEIN[8] = '0';
            if (TRAMEIN[9] == 0x20)
                TRAMEIN[9] = '0';
            if (TRAMEIN[10] == 0x20)
                TRAMEIN[10] = '0';
            if (TRAMEIN[11] == 0x20)
                TRAMEIN[11] = '0';
            if (TRAMEIN[12] == 0x20)
                TRAMEIN[12] = '0';

            DEMANDE_INFOS_CAPTEURS[0] = TRAMEIN[8];
            DEMANDE_INFOS_CAPTEURS[1] = TRAMEIN[9];
            DEMANDE_INFOS_CAPTEURS[2] = TRAMEIN[10];
            DEMANDE_INFOS_CAPTEURS[3] = TRAMEIN[11];
            DEMANDE_INFOS_CAPTEURS[4] = TRAMEIN[12];

            pointeur_out_in = 8; // SAUVEGARDE POINTEUR DU TABLEAU
            calcul_CRC(CRC, TRAMEOUT);
            if (TRAMEIN[1] == 'A') //AUTOMATE PAS DE ERR00
            {
                etatIOAU = 'U';
                TEMPO(2);
                goto AU3;
            } else {

                INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
                INFORMATION_ERREUR[1] = '0';
                for (t = 2; t < 20; t++) {
                    INFORMATION_ERREUR[t] = 'x';
                }

                etatIOAU = 'U';
                if (TRAMEIN[1] == 'I') //CARTE I/O
                {
                    ENVOIE_DES_TRAMES(7, 302, INFORMATION_ERREUR);
                    etatIOAU = 'I';
                } else
                    ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);

            }

AU3:

            //METTRE EN RELATIF
            VOIECOD[0] = DEMANDE_INFOS_CAPTEURS[0];
            VOIECOD[1] = DEMANDE_INFOS_CAPTEURS[1];
            VOIECOD[2] = DEMANDE_INFOS_CAPTEURS[2];
            VOIECOD[3] = DEMANDE_INFOS_CAPTEURS[3];
            VOIECOD[4] = DEMANDE_INFOS_CAPTEURS[4];

            //ON TRANSFORME L'ABSOLU EN RELATIF
            if (VOIECOD[0] == '1') // voie 100
            {
                VOIECOD[0] == '0';
                VOIECOD[1] = '2'; //100
                VOIECOD[2] = '0';
            } else
                //voie 0xy
            {

                if (((VOIECOD[1] == '2') || (VOIECOD[1] == '4') || (VOIECOD[1] == '6') || (VOIECOD[1] == '8')) || (VOIECOD[1] == '0')&&(VOIECOD[2] != '0')) //on test x si x paire alors ->Voie relative 0
                    VOIECOD[1] = '0';

                if (((VOIECOD[1] == '2') || (VOIECOD[1] == '4') || (VOIECOD[1] == '6') || (VOIECOD[1] == '8') || (VOIECOD[1] == '0'))&&(VOIECOD[2] == '0')) //on test x si x paire alors ->Voie relative 0
                    VOIECOD[1] = '2';

                if ((VOIECOD[1] == '1') || (VOIECOD[1] == '3') || (VOIECOD[1] == '5') || (VOIECOD[1] == '7') || (VOIECOD[1] == '9')) //on test x si x impaire alors ->Voie relative 1 sauf si y est de 0
                    VOIECOD[1] = '1'; //sinon c la voie relative 01y




            }

            if (EFFACER_CAPTEUR_SUR_CM(0, DEMANDE_INFOS_CAPTEURS, &NBRECAPTEURS_CABLE, &capt_exist, TMP, VOIECOD, ETAT) == 1) // VOIR SI C'EST LE SEUL CAPTEUR SANS EFFACER SI OUI ENVOYER #AUCMZFF04116F22767* voie effac�e
            {
                VOIECOD[5] = 'F';
            } else {
                VOIECOD[5] = 'N'; //SINON ENVOYER #AUCMZFF04116N22767*
            }


            ENVOIE_DES_TRAMES(trouver_carte_suivantvoie(DEMANDE_INFOS_CAPTEURS), 278, VOIECOD); //sans # 04116 voie 041 codage 16


            INFORMATION_ERREUR[0] = 'F'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = 'F';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }

            if (etatIOAU == 'I') //CARTE I/O

                ENVOIE_DES_TRAMES(7, 302, INFORMATION_ERREUR);
            else
                ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);


            EFFACER_CAPTEUR_SUR_CM(1, DEMANDE_INFOS_CAPTEURS, &NBRECAPTEURS_CABLE, &capt_exist, TMP, VOIECOD, ETAT); // EFFACER CAPTEUR REELLEMENT
            PRIO_AUTO_IO = 0x00;
            INTCONbits.GIE = 1;

            break;

        case 334: //REGLER HORLOGE DEPUIS AU OU IO ZRH

            INTCONbits.GIE = 0;

            FONCTION[0] = 'Z';
            FONCTION[1] = 'R';
            FONCTION[2] = 'H';

            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];

            HORLOGE[0] = TRAMEIN[8];
            HORLOGE[1] = TRAMEIN[9];
            HORLOGE[2] = TRAMEIN[10];
            HORLOGE[3] = TRAMEIN[11];
            HORLOGE[4] = TRAMEIN[12];
            HORLOGE[5] = TRAMEIN[13];
            HORLOGE[6] = TRAMEIN[14];
            HORLOGE[7] = TRAMEIN[15];
            HORLOGE[8] = TRAMEIN[16];
            HORLOGE[9] = TRAMEIN[17];
            HORLOGE[10] = TRAMEIN[18];
            HORLOGE[11] = TRAMEIN[19];

            delay_ms(50);

            pointeur_out_in = 20; // SAUVEGARDE POINTEUR DU TABLEAU
            calcul_CRC(CRC, TRAMEOUT);

            INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = '0';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }

            entrer_trame_horloge(HORLOGE); //ECRIRE HORLOGE DANS CM EN EEPROM

            INFORMATION_ERREUR[0] = 'F'; //ON DIT A L'AUTOMATE QUE L'OPERATION EST TERMINEE
            INFORMATION_ERREUR[1] = 'F';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            pic_eeprom_write(0x05, 0xAA); //RESET RI A ZERO
            pic_eeprom_write(0x04, 0xAA);
            pic_eeprom_write(0x03, 0xAA);
            pic_eeprom_write(0x02, 0xAA);
            pic_eeprom_write(0x01, 0xAA);
            pic_eeprom_write(0x20, 0xFF); //RAZ COMPTEUR
            pic_eeprom_write(0x21, 0xFF); //RAZ COMPTEUR
            pic_eeprom_write(0x22, 0xFF); //RAZ COMPTEUR
            pic_eeprom_write(0x23, 0xFF);
            pic_eeprom_write(0x24, 0xFF);
            pic_eeprom_write(0x25, 0xFF);

            if (ID1[0] == 'A') //AUTO
                ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);
            if (ID1[0] == 'I') //IO
                ENVOIE_DES_TRAMES(7, 302, INFORMATION_ERREUR);
            TEMPO(2);
            if (ID2[0] != 'I') //SI PAS CARTE IO
            {
                RESETCMS = 1; //RESET CMS
            }
            PRIO_AUTO_IO = 0x00;
            INTCONbits.GIE = 1;


            break;

        case 328:

            INTCONbits.GIE = 0;
            FONCTION[0] = 'Z';
            FONCTION[1] = 'B';
            FONCTION[2] = 'R';
            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];

            if ((TRAMEIN[8] == '9')&&(TRAMEIN[8] == '9'))
                goto TOUS;
            TRAMEIN[20] = 10 * (TRAMEIN[8] - 48);
            TRAMEIN[21] = TRAMEIN[9] - 48;
            calculBLOC = TRAMEIN[20] + TRAMEIN[21];
            calculBLOC = 2048 + (calculBLOC - 1)*128;
            TEMPO(1);
            LIRE_EEPROM(6, calculBLOC, TRAMEOUT, 6); //LIRE 85caract
            TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A L'IO;
            goto UN;

TOUS:

            for (t = 1; t < 22; t++) {
                calculBLOC = 2048 + (t - 1)*128;
                //delay_ms(100);
                LIRE_EEPROM(6, calculBLOC, TRAMEOUT, 6); //LIRE 85caract
                TRAMEOUT[1] = 'C';
                TRAMEOUT[2] = 'M';
                TRAMEOUT[3] = 'I';
                TRAMEOUT[4] = 'O';
                // SI MEMOIRE VIDE
                if ((TRAMEOUT[5] != 'Z')&&(TRAMEOUT[6] != 'B')) //MEMOIRE VIDE
                {
                    TRAMEOUT[0] = DEBUT_TRAME;
                    TRAMEOUT[8] = '0';
                    TRAMEOUT[9] = '1';
                    TRAMEOUT[97] = FIN_TRAME;
                }

                TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A L'IO;


                delay_ms(100);
                TRAMERECEPTIONRS485DONNEES(18, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
                if ((TRAMEIN[8] == 'F')&&(TRAMEIN[9] == 'F')) {
                    t = t - 1;
                    if (t <= 1)
                        t = 1;
                }
            }

UN:

            MAJBLOC = 'N';

            INTCONbits.GIE = 1;
            break;


        case 313: //Envoi BLOC NOTES DE LAU
            INTCONbits.GIE = 0;
            FONCTION[0] = 'Z';
            FONCTION[1] = 'B';
            FONCTION[2] = 'C';
            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];



            for (funct = 1; funct < 22; funct++) {

                TRAMEIN[100] = 10 * (TRAMEIN[8] - 48);
                TRAMEIN[101] = (TRAMEIN[9] - 48);
                calculBLOC = TRAMEIN[100] + TRAMEIN[101];

                if (TRAMEIN[0] != DEBUT_TRAME) {
                    funct = funct - 1;
                    INFORMATION_ERREUR[0] = 'F'; //TT EST BON donc utilisation type d'erreur
                    INFORMATION_ERREUR[1] = 'F';
                    goto erreurBZ;
                }

                if ((calculBLOC > 7) && (calculBLOC < 11)) {
                    funct = funct - 1;
                    INFORMATION_ERREUR[0] = 'F'; //TT EST BON donc utilisation type d'erreur
                    INFORMATION_ERREUR[1] = 'F';
                    goto erreurBZ;
                }
                if ((calculBLOC > 17) && (calculBLOC < 21)) {
                    funct = funct - 1;
                    INFORMATION_ERREUR[0] = 'F'; //TT EST BON donc utilisation type d'erreur
                    INFORMATION_ERREUR[1] = 'F';
                    goto erreurBZ;
                }
                if ((calculBLOC < 1) || (calculBLOC > 27)) {
                    funct = funct - 1;
                    INFORMATION_ERREUR[0] = 'F'; //TT EST BON donc utilisation type d'erreur
                    INFORMATION_ERREUR[1] = 'F';
                    goto erreurBZ;
                }
                if (funct <= 0)
                    goto finBZ;

                if ((calculBLOC > 7)&&(calculBLOC <= 17))
                    calculBLOC = calculBLOC - 3;
                if ((calculBLOC > 17)&&(calculBLOC <= 27))
                    calculBLOC = calculBLOC - 6;
                TRAMEOUT[120] = calculBLOC + 48;
                calculBLOC = 2048 + (calculBLOC - 1)*128;

                ECRIRE_EEPROMBYTE(6, calculBLOC, TRAMEIN, 6); //ECRITURE EN EEPROM LIGNE 1 85 caract



                INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
                INFORMATION_ERREUR[1] = '0';
erreurBZ:
                for (t = 2; t < 20; t++) {
                    INFORMATION_ERREUR[t] = 'x';
                }
                delay_ms(100);
                ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);

                TRAMERECEPTIONRS485DONNEES(18, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE



            }
            MAJBLOC = 'Y';
            SDWBN(TRAMEIN, TRAMEOUT, HORLOGE);
            INFORMATION_ERREUR[0] = 'C'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = 'C';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            delay_ms(100);
            ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);



finBZ:
            PRIO_AUTO_IO = 0x00;
            INTCONbits.GIE = 1;

            break;


        case 324: //DEMANDE GROUPE IO OU (cartes type)
            INTCONbits.GIE = 0;
            PRIO_AUTO_IO = 0x22;
            FONCTION[0] = 'Z';
            FONCTION[1] = 'I';
            FONCTION[2] = 'G';

            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];

            if ((ID1[0] == 'I')&&(ID1[1] == 'O'))
                DEMANDEZIGIO = 0xFF;

            IDENTIF_TYPE_CARTE(TMP, TYPECARTE, VDD1, VDD2, VDD3, VDD4, VDD5, VBAT, R1NOREP, R2NOREP, R3NOREP, R4NOREP, R5NOREP);
            for (t = 0; t < 3; t++) {
                RECHERCHE_DONNEES_GROUPE(t, TMP);
                for (funct = 9; funct < 125; funct++) {
                    TRAMEOUT[funct - 1] = TMP[funct - 1];

                }
                if (t == 2) {


                    TRAMEOUT[70] = MAJBLOC;

                    funct = pic_eeprom_read(0x20);
                    if (funct != 100) //'d'
                    {
                        IntToChar(funct + 1, CRC, 2); //voir la valeur	NB RESET,CRCcal,5);
                        TRAMEOUT[97] = CRC[0];
                        TRAMEOUT[98] = CRC[1];
                    } else {
                        TRAMEOUT[97] = 'd';
                        TRAMEOUT[98] = '0';
                    }

                    funct = pic_eeprom_read(0x21);
                    if (funct != 100) //'d'
                    {
                        IntToChar(funct + 1, CRC, 2); //voir la valeur	NB RESET,CRCcal,5);
                        TRAMEOUT[99] = CRC[0];
                        TRAMEOUT[100] = CRC[1];
                    } else {
                        TRAMEOUT[99] = 'd';
                        TRAMEOUT[100] = '1';
                    }

                    funct = pic_eeprom_read(0x22);
                    if (funct != 100) //'d'
                    {
                        IntToChar(funct + 1, CRC, 2); //voir la valeur	NB RESET,CRCcal,5);
                        TRAMEOUT[101] = CRC[0];
                        TRAMEOUT[102] = CRC[1];
                    } else {
                        TRAMEOUT[101] = 'd';
                        TRAMEOUT[102] = '2';
                    }

                    funct = pic_eeprom_read(0x23);
                    if (funct != 100) //'d'
                    {
                        IntToChar(funct + 1, CRC, 2); //voir la valeur	NB RESET,CRCcal,5);
                        TRAMEOUT[103] = CRC[0];
                        TRAMEOUT[104] = CRC[1];
                    } else {
                        TRAMEOUT[103] = 'd';
                        TRAMEOUT[104] = '3';
                    }

                    funct = pic_eeprom_read(0x24);
                    if (funct != 100) //'d'
                    {
                        IntToChar(funct + 1, CRC, 2); //voir la valeur	NB RESET,CRCcal,5);
                        TRAMEOUT[105] = CRC[0];
                        TRAMEOUT[106] = CRC[1];
                    } else {
                        TRAMEOUT[105] = 'd';
                        TRAMEOUT[106] = '4';
                    }


                    funct = pic_eeprom_read(0x25);
                    if (funct != 100) //'d'
                    {
                        IntToChar(funct + 1, CRC, 2); //voir la valeur	NB RESET,CRCcal,5);
                        TRAMEOUT[107] = CRC[0];
                        TRAMEOUT[108] = CRC[1];
                    } else {
                        TRAMEOUT[107] = 'd';
                        TRAMEOUT[108] = '5';
                    }


                    TRAMEOUT[110] = TYPECARTE[0]; //ATTENTION IL FAUT QUE LE GROUPE SOIT ENVOYE UNE FOIS AU MOINS !!!! voir si on met un par defaut
                    TRAMEOUT[111] = TYPECARTE[1];
                    TRAMEOUT[112] = TYPECARTE[2];
                    TRAMEOUT[113] = TYPECARTE[3];
                    TRAMEOUT[114] = TYPECARTE[4];
                }


                pointeur_out_in = 115; // SAUVEGARDE POINTEUR DU TABLEAU
                calcul_CRC(CRC, TRAMEOUT);

                SERIALISATION(&ENTREES1, &ENTREES2); // VOIR SI DEMANDE DE L'AUTO
                if ((ENTREES2 & 0x02) == 0x02) //CAR DANS LE PRG CARTE IO RH5 DEMANDE EST LONGTEMPS HAUT
                    PRIO_AUTO_IO = 0xE1;

                TRAMEENVOIRS485DONNEES(TRAMEOUT); // ENVOI A L'IO;
                TRAMERECEPTIONRS485DONNEES(9, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE ICI ERR00 pour dire OK
                controle_CRC(CRC, TRAMEIN);

            }

            if (PRIO_AUTO_IO == 0xE1) {
                PRIO_AUTO_IO = 0x00;
                identificationINTER(6, 0x00);
            }
            PRIO_AUTO_IO = 0x00;
            INTCONbits.GIE = 1;

            break;

        case 316: //DEMANDE DENVOI DES DONNEES GROUPE DEPUIS L'AU

            INTCONbits.GIE = 0;


            FONCTION[0] = 'Z';
            FONCTION[1] = 'A';
            FONCTION[2] = 'G';

            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];



            pointeur_out_in = 8; // SAUVEGARDE POINTEUR DU TABLEAU

            INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = '0';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = '1';
            }
            ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);

            for (funct = 0; funct < 3; funct++) {
                TRAMEIN[120] = 'E';
                TRAMERECEPTIONRS485DONNEES(18, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
                controle_CRC(CRC, TRAMEIN);
                if ((TRAMEIN[1] == 'A')&&(TRAMEIN[2] == 'U')&&(TRAMEIN[3] == 'C')&&(TRAMEIN[4] == 'M')) //UNIQUEMENT LE GROUPE
                {
                    if ((funct == 0)&&(TRAMEIN[19] == '0')&&(TRAMEIN[20] == '0')&&(TRAMEIN[21] == '1')&&(TRAMEIN[120] == FIN_TRAME)) {
                        GROUPE_EN_MEMOIRE(TRAMEIN, TRAMEOUT, 0); //ECRIRE EN MEMOIRE 6
                    }
                    if ((funct == 1)&&(TRAMEIN[19] == '0')&&(TRAMEIN[20] == '0')&&(TRAMEIN[21] == '4')&&(TRAMEIN[120] == FIN_TRAME)) {
                        GROUPE_EN_MEMOIRE(TRAMEIN, TRAMEOUT, 1); //ECRIRE EN MEMOIRE 6
                    }
                    if ((funct == 2)&&(TRAMEIN[19] == '0')&&(TRAMEIN[20] == '0')&&(TRAMEIN[21] == '9')&&(TRAMEIN[120] == FIN_TRAME)) {
                        GROUPE_EN_MEMOIRE(TRAMEIN, TRAMEOUT, 2); //ECRIRE EN MEMOIRE 6
                    }
                }

                INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
                INFORMATION_ERREUR[1] = '0';
                for (t = 2; t < 20; t++) {
                    INFORMATION_ERREUR[t] = TRAMEIN[19 + t];
                }
                ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);
            }

            LIRE_EEPROM(6, 0, TRAMEOUT, 3);
            delay_ms(100);

            LIRE_EEPROM(6, 128, TRAMEOUT, 3);
            delay_ms(100);

            LIRE_EEPROM(6, 256, TRAMEOUT, 3);
            delay_ms(100);



            INTCONbits.GIE = 1
                    ;


            break;



        case 276: //DIC

            controle_CRC(CRC, TRAMEIN);
            break;

        case 308: //LIS

            break;


        case 312: //AUTOMATE DEMANDE UNE PAGE fonction ZAC OK

            INTCONbits.GIE = 0;
            pointeur_out_in = 12;
            controle_CRC(CRC, TRAMEIN);

            PAGE[0] = DEBUT_TRAME;
            PAGE[1] = TRAMEIN[9];
            PAGE[2] = TRAMEIN[10];
            PAGE[3] = TRAMEIN[11] - 1;
            PAGE[4] = '0';
            PAGE[5] = '0';

            // LA PAGE DEVIENT LA ZONE DE 0-9 exemple P001 devient zone 0

            TMPI = 100 * (PAGE[1] - 48);
            TMPI = TMPI + 10 * (PAGE[2] - 48);
            TMPI = TMPI + (PAGE[3] - 48);


            if (TMPI == 0) //ORIGINE DE LA ZONE PR LA PAGE1 c la voie 0 pour la page002  c'est la voie 10.........page010 c'est la voie 90
            {
                PAGE[1] = '0';
                PAGE[2] = '0';
                PAGE[3] = '1';
            }


            if (TMPI == 1) {
                PAGE[1] = '0';
                PAGE[2] = '1';
                PAGE[3] = '1';
            }


            if (TMPI == 2) {
                PAGE[1] = '0';
                PAGE[2] = '2';
                PAGE[3] = '1';
            }



            if (TMPI == 3) {
                PAGE[1] = '0';
                PAGE[2] = '3';
                PAGE[3] = '1';
            }



            if (TMPI == 4) {
                PAGE[1] = '0';
                PAGE[2] = '4';
                PAGE[3] = '1';
            }


            if (TMPI == 5) {
                PAGE[1] = '0';
                PAGE[2] = '5';
                PAGE[3] = '1';
            }


            if (TMPI == 6) {
                PAGE[1] = '0';
                PAGE[2] = '6';
                PAGE[3] = '1';
            }

            if (TMPI == 7) {
                PAGE[1] = '0';
                PAGE[2] = '7';
                PAGE[3] = '1';
            }


            if (TMPI == 8) {
                PAGE[1] = '0';
                PAGE[2] = '8';
                PAGE[3] = '1';
            }

            if (TMPI == 9) {
                PAGE[1] = '0';
                PAGE[2] = '9';
                PAGE[3] = '1';
            }

            for (t = 1; t < 11; t++) {

                VOIECOD[0] = PAGE[1];
                VOIECOD[1] = PAGE[2];
                VOIECOD[2] = PAGE[3];

                CHERCHER_DONNNEES_CABLES(VOIECOD, 111, TMP, VOIECOD, ETAT, &capt_exist); // on regarde si la voie existe



                if ((capt_exist == 1) || (capt_exist == 2) || (capt_exist == 3) || (capt_exist == 4)) {
                    PREPARATION_PAGE_AUTO(PAGE, TRAMEIN, TRAMEOUT, TMP); //ON PREPARE TOUTE LA TRAME
                } else {
                    //TEMPO(2); //TEST
                    TRAMEOUT[8] = PAGE[1];
                    TRAMEOUT[9] = PAGE[2];
                    TRAMEOUT[10] = PAGE[3];
                    TRAMEOUT[29] = 'F'; //TYPE F=VIDE  #CMAUZAC045xxxxxxxxxxxxxxxxxxA0050013102FF03FF04FF05FF06FF07FF08FF09FF10FF11FF12FF13FF14FF15FF16FF22222* 'TEST F
                }
                PAGE[4] = '0';
                PAGE[5] = '0';

                pointeur_out_in = 98;
                calcul_CRC(CRC, TRAMEOUT); //COMPLETE LA TRAME TRAMEOUT[] AVEC LE CRC
                TRAMEOUT[pointeur_out_in + 5] = FIN_TRAME; //COMPLETE LA TRAME TRAMEOUT[] avec STOP
                TRAMEOUT[0] = DEBUT_TRAME;
                TRAMEOUT[1] = ID2[0];
                TRAMEOUT[2] = ID2[1];
                TRAMEOUT[3] = ID1[0];
                TRAMEOUT[4] = ID1[1];

                TRAMEOUT[5] = FONCTION[0];
                TRAMEOUT[6] = FONCTION[1];
                TRAMEOUT[7] = FONCTION[2]; //TRAMEOUT;


                REMISE_EN_FORME_PAGE(TRAMEOUT, CRC, &pointeur_out_in);
                TRAMEENVOIRS485DONNEES(TRAMEOUT); //ENVOYER LA TRAME

                TRAMEIN[3] = 'Q';
                TRAMERECEPTIONRS485DONNEES(2, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE ICI AVEC TIMER 9origine

                if (NORECEPTION == 0xff) //SI PAS DE RECEPTION on refait
                {
                    TRAMEENVOIRS485DONNEES(TRAMEOUT); //ENVOYER LA TRAME

                    TRAMEIN[3] = 'Q';
                    TRAMERECEPTIONRS485DONNEES(2, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE ICI AVEC TIMER 9origine


                    if (NORECEPTION == 0xff) {
                        TRAMEENVOIRS485DONNEES(TRAMEOUT); //ENVOYER LA TRAME

                        TRAMEIN[3] = 'Q';
                        TRAMERECEPTIONRS485DONNEES(9, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE ICI AVEC TIMER 9origine
                    }

                    if (NORECEPTION == 0xff) {
                        delay_ms(200);
                        TRAMEENVOIRS485DONNEES(TRAMEOUT); //ENVOYER LA TRAME

                        TRAMEIN[3] = 'Q';
                        TRAMERECEPTIONRS485DONNEES(9, &NORECEPTION, TRAMEIN); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE ICI AVEC TIMER 9origine
                    }





                    if (NORECEPTION == 0xff) //SI PAS DE RECEPTION
                        goto finforzac;


                }
                FONCTION[0] = 'Z'; //exeption cylce suivant
                FONCTION[1] = 'A';
                FONCTION[2] = 'C';

                if (PAGE[3] == '9') //9
                {
                    if (PAGE[2] == '9') {
                        PAGE[3] = '0';
                        PAGE[2] = '0';
                        PAGE[1] = '1';
                    } else {
                        PAGE[3] = '0'; //soit avant le '0'
                        PAGE[2] = PAGE[2] + 1;
                    }
                } else {
                    PAGE[3] = t + 49;
                }

            }
finforzac:

            INFORMATION_ERREUR[0] = 'F'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = 'F';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);


            PRIO_AUTO_IO = 0x00;
            INTCONbits.GIE = 1;
            funct = 0;

            break;

        case 305: //HOR

            HORLOGE[0] = tab[8];
            HORLOGE[1] = tab[9];
            HORLOGE[2] = tab[10];
            HORLOGE[3] = tab[11];
            HORLOGE[4] = tab[12];
            HORLOGE[5] = tab[13];
            HORLOGE[6] = tab[14];
            HORLOGE[7] = tab[15];


            pointeur_out_in = 16;
            controle_CRC(CRC, TRAMEIN);

            // TOUT EST OK HORLOGE REGLEE

            break;

        case 278: //EFF
            pointeur_out_in = 8;
            controle_CRC(CRC, TRAMEIN);
            break;


        case 280: //DMC ---------->
            pointeur_out_in = 13;
            controle_CRC(CRC, TRAMEIN); // on controle le CRC

            //PREPARATION DE LA TRAME A ENVOYER
            funct = 0;

            DEMANDE_INFOS_CAPTEURS[0] = tab[8];
            DEMANDE_INFOS_CAPTEURS[1] = tab[9];
            DEMANDE_INFOS_CAPTEURS[2] = tab[10];
            DEMANDE_INFOS_CAPTEURS[3] = tab[11];
            DEMANDE_INFOS_CAPTEURS[4] = tab[12];

            pointeur_out_in = construire_trame_memoire(TRAMEIN, TRAMEOUT, VOIECOD, DEMANDE_INFOS_CAPTEURS, ETAT, &tmp); // RECUPERATION DES INFORMATIONS DE L'EEPROM DU CAPTEUR



            //RECURRENT
            TRAMEOUT[0] = DEBUT_TRAME;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2]; //TRAMEOUT;

            calcul_CRC(CRC, TRAMEOUT); //COMPLETE LA TRAME TRAMEOUT[] AVEC LE CRC
            TRAMEOUT[pointeur_out_in + 5] = FIN_TRAME; //COMPLETE LA TRAME TRAMEOUT[] avec STOP

            //LA TRAME EST PRETE A ETRE ENVOYEE
            //FAIRE DRS485=1 A VOIR LA QUELLE
            TRAMEENVOIRS485DONNEES(TRAMEOUT); //ENVOYER LA TRAME


            funct = 0;

            break;


        case 287: //DHO


            pointeur_out_in = 8;
            controle_CRC(CRC, TRAMEIN);


            lire_horloge(HORLOGE);

            ENVOIE_DES_TRAMES(1, 305, HORLOGE);



            break;


        case 285: //CRE


            controle_CRC(CRC, TRAMEIN);

            // SI TOUT EST OK ALORS CREATION OK
            break;



        case 322: //LANCE LA FONCTION OEIL PAR AUTOMATE ZEI OK


            INTCONbits.GIE = 0;
            pointeur_out_in = 13;
            DEMANDE_INFOS_CAPTEURS[0] = tab[8];
            DEMANDE_INFOS_CAPTEURS[1] = tab[9];
            DEMANDE_INFOS_CAPTEURS[2] = tab[10];
            DEMANDE_INFOS_CAPTEURS[3] = tab[11];
            DEMANDE_INFOS_CAPTEURS[4] = tab[12];



            controle_CRC(CRC, TRAMEIN); // on controle le CRC

            INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = '0';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);


            VOIECOD[0] = DEMANDE_INFOS_CAPTEURS[0];
            VOIECOD[1] = DEMANDE_INFOS_CAPTEURS[1];
            VOIECOD[2] = DEMANDE_INFOS_CAPTEURS[2];
            VOIECOD[3] = DEMANDE_INFOS_CAPTEURS[3];
            VOIECOD[4] = DEMANDE_INFOS_CAPTEURS[4];

            codageDEM = 10 * (VOIECOD[3] - 48)+(VOIECOD[4] - 48);
            CHERCHER_DONNNEES_CABLES(VOIECOD, 111, TMP, VOIECOD, ETAT, &capt_exist); // on regarde si la voie existe
            if (capt_exist != 1) //CABLE diff de R OU A donc h v i c voir fonction
            {
                capt_exist = 10; //PAS DE CABLE QUI EXISTE
            } else //SI cable existe A ou R
            {
                CHERCHER_DONNNEES_CABLES(DEMANDE_INFOS_CAPTEURS, codageDEM, TMP, VOIECOD, ETAT, &capt_exist); //VOIR EXITENCE CAPTEUR sauf debit!!!!!!!
                if (capt_exist == 1) //capteur present
                {
                    capt_exist = 11;
                } else {
                    capt_exist = 20; //PAS DE CAPTEUR POUR CE CODAGE
                }
            }
            //ON TRANSFORME L'ABSOLU EN RELATIF
            if (VOIECOD[0] == '1') // voie 100
            {
                VOIECOD[0] = '0';
                VOIECOD[1] = '2'; //100
                VOIECOD[2] = '0';
            } else
                //voie 0xy
            {

                if (((VOIECOD[1] == '2') || (VOIECOD[1] == '4') || (VOIECOD[1] == '6') || (VOIECOD[1] == '8') || (VOIECOD[1] == '0'))&&(VOIECOD[2] != '0')) //on test x si x paire alors ->Voie relative 0
                    VOIECOD[1] = '0';

                if (((VOIECOD[1] == '2') || (VOIECOD[1] == '4') || (VOIECOD[1] == '6') || (VOIECOD[1] == '8') || (VOIECOD[1] == '0'))&&(VOIECOD[2] == '0')) //on test x si x paire alors ->Voie relative 0
                    VOIECOD[1] = '2';

                if ((VOIECOD[1] == '1') || (VOIECOD[1] == '3') || (VOIECOD[1] == '5') || (VOIECOD[1] == '7') || (VOIECOD[1] == '9')) //on test x si x impaire alors ->Voie relative 1 sauf si y est de 0
                    VOIECOD[1] = '1'; //sinon c la voie relative 01y




            }


            ENVOIE_DES_TRAMES(trouver_carte_suivantvoie(DEMANDE_INFOS_CAPTEURS), 300, VOIECOD);
            //SI RIEN


            INFORMATION_ERREUR[0] = 'F'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = 'F';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }

            if (TMP[1] == 'I') //CARTE I/O
                ENVOIE_DES_TRAMES(7, 302, INFORMATION_ERREUR);
            else
                ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);





            INTCONbits.GIE = 1;
            break;





        case 260: //FONCTION INTERNE PR CARTE MERE #CMCMAAA00122767*  avec 001 la carte 1  000 si ttes

            //PREPARATION DE LA TRAME A ENVOYER
            funct = 0;

            TMPI = 100 * (TRAMEIN[8] - 48);
            TMPI = TMPI + 10 * (TRAMEIN[9] - 48);
            TMPI = TMPI + (TRAMEIN[10] - 48);





            //SI FLAG ACCORDSR1 R2 R3 serialisation ou attendre la trame err000?? a choisir


            if ((TMPI == 1) || (TMPI == 0))
                ENVOIE_DES_TRAMES(1, 290, TRAMEOUT); //RECUPERATION DE LA LISTE
            if ((TMPI == 2) || (TMPI == 0))
                ENVOIE_DES_TRAMES(2, 290, TRAMEOUT); //RECUPERATION DE LA LISTE
            if ((TMPI == 3) || (TMPI == 0))
                ENVOIE_DES_TRAMES(3, 290, TRAMEOUT); //RECUPERATION DE LA LISTE
            if ((TMPI == 4) || (TMPI == 0))
                ENVOIE_DES_TRAMES(4, 290, TRAMEOUT); //RECUPERATION DE LA LISTE
            if ((TMPI == 5) || (TMPI == 0))
                ENVOIE_DES_TRAMES(5, 290, TRAMEOUT); //RECUPERATION DE LA LISTE





            INTERROGER_TOUT_LES_CAPTEURS(1, TMP, DEMANDE_INFOS_CAPTEURS);

            break;

        case 345: //ZSR


            INTCONbits.GIE = 0;
            RESCRUTATION = 0;

            pointeur_out_in = 13;
            controle_CRC(CRC, TRAMEIN); // on controle le CRC


            delay_ms(100);

            INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = '0';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);



            //PREPARATION DE LA TRAME A ENVOYER
            funct = 0;


            if (TRAMEIN[8] == 0x20)
                TRAMEIN[8] = '0';
            if (TRAMEIN[9] == 0x20)
                TRAMEIN[9] = '0';
            if (TRAMEIN[10] == 0x20)
                TRAMEIN[10] = '0';


            TMPI = 100 * (TRAMEIN[8] - 48);
            TMPI = TMPI + 10 * (TRAMEIN[9] - 48);
            TMPI = TMPI + (TRAMEIN[10] - 48);

            INSTRUCTION[0] = TRAMEIN[11];
            INSTRUCTION[1] = TRAMEIN[12];

            DEMANDE_INFOS_CAPTEURS[0] = '0';
            DEMANDE_INFOS_CAPTEURS[1] = '0';
            DEMANDE_INFOS_CAPTEURS[2] = '1';
            DEMANDE_INFOS_CAPTEURS[3] = '0';
            DEMANDE_INFOS_CAPTEURS[4] = '0';
            DEMANDE_INFOS_CAPTEURS[5] = '0';
            DEMANDE_INFOS_CAPTEURS[6] = '2';
            DEMANDE_INFOS_CAPTEURS[7] = '0';
            DEMANDE_INFOS_CAPTEURS[8] = '1';
            DEMANDE_INFOS_CAPTEURS[9] = '6';

            MODERAPID = 0;
            if ((INSTRUCTION[0] == 'F')&&(INSTRUCTION[1] == 'F')) //TOUTE LES VOIES
            {
                INSTRUCTION[2] = 'F'; //SAV
                INSTRUCTION[3] = 'F';
                INSTRUCTION[0] = '0'; //on part de 01
                INSTRUCTION[1] = 0x2F;
                DEMANDE_INFOS_CAPTEURS[1] = '0'; //1 exemple 99 01 03
                DEMANDE_INFOS_CAPTEURS[2] = '1'; //2
                scrutationunevoie = 0;
            } else //UNIQUEMENT UNE VOIE
            {
                scrutationunevoie = 1;
                DEMANDE_INFOS_CAPTEURS[0] = TRAMEIN[8]; // ICI IL N'Y A PAS LE NUMERO DE CARTE MAIS LE NUMERO DE VOIE !!!!!
                DEMANDE_INFOS_CAPTEURS[1] = TRAMEIN[9]; //1 exemple 99 01 03 100
                DEMANDE_INFOS_CAPTEURS[2] = TRAMEIN[10]; //2

                if ((DEMANDE_INFOS_CAPTEURS[1] == '0')&&(DEMANDE_INFOS_CAPTEURS[2] != '0')) //x0x mais pas 100
                    TMPI = 1; //carte RI1
                if ((DEMANDE_INFOS_CAPTEURS[1] == '1')) //1x
                    TMPI = 1; //carte RI1

                if ((DEMANDE_INFOS_CAPTEURS[1] == '2')&&(DEMANDE_INFOS_CAPTEURS[2] != '0')) //x2x mais pas 20
                    TMPI = 2; //carte RI2
                if ((DEMANDE_INFOS_CAPTEURS[1] == '3')) //3x
                    TMPI = 2; //carte RI2

                if ((DEMANDE_INFOS_CAPTEURS[1] == '4')&&(DEMANDE_INFOS_CAPTEURS[2] != '0')) //x4x mais pas 40
                    TMPI = 3; //carte RI3
                if ((DEMANDE_INFOS_CAPTEURS[1] == '5')) //5x
                    TMPI = 3; //carte RI3

                if ((DEMANDE_INFOS_CAPTEURS[1] == '6')&&(DEMANDE_INFOS_CAPTEURS[2] != '0')) //x6x mais pas 60
                    TMPI = 4; //carte RI4
                if ((DEMANDE_INFOS_CAPTEURS[1] == '7')) //7x
                    TMPI = 4; //carte RI4

                if ((DEMANDE_INFOS_CAPTEURS[1] == '8')&&(DEMANDE_INFOS_CAPTEURS[2] != '0')) //x8x mais pas 80
                    TMPI = 5; //carte RI5
                if ((DEMANDE_INFOS_CAPTEURS[1] == '9')) //9x
                    TMPI = 5; //carte RI5

                if ((DEMANDE_INFOS_CAPTEURS[0] == '1')&&(DEMANDE_INFOS_CAPTEURS[1] == '0')&&(DEMANDE_INFOS_CAPTEURS[2] == '0')) // 100
                    TMPI = 5; //carte RI5

                if ((DEMANDE_INFOS_CAPTEURS[1] == '2')&&(DEMANDE_INFOS_CAPTEURS[2] == '0')) // 20
                    TMPI = 1; //carte RI1

                if ((DEMANDE_INFOS_CAPTEURS[1] == '4')&&(DEMANDE_INFOS_CAPTEURS[2] == '0')) // 40
                    TMPI = 2; //carte RI2

                if ((DEMANDE_INFOS_CAPTEURS[1] == '6')&&(DEMANDE_INFOS_CAPTEURS[2] == '0')) // 60
                    TMPI = 3; //carte RI1

                if ((DEMANDE_INFOS_CAPTEURS[1] == '6')&&(DEMANDE_INFOS_CAPTEURS[2] == '0')) // 80
                    TMPI = 5; //carte RI1




                // 100
                if ((DEMANDE_INFOS_CAPTEURS[0] == '1')) //SUPERIEUR A 100
                {
                    DEMANDE_INFOS_CAPTEURS[0] = '0';
                    DEMANDE_INFOS_CAPTEURS[1] = '2'; //voie 20
                    DEMANDE_INFOS_CAPTEURS[2] = '0';
                }
                // 02x 04x 06x 08x
                if (((DEMANDE_INFOS_CAPTEURS[1] == '8') || (DEMANDE_INFOS_CAPTEURS[1] == '6') || (DEMANDE_INFOS_CAPTEURS[1] == '4') || (DEMANDE_INFOS_CAPTEURS[1] == '2') || (DEMANDE_INFOS_CAPTEURS[1] == '0'))&&(DEMANDE_INFOS_CAPTEURS[1] != '0')) {
                    DEMANDE_INFOS_CAPTEURS[0] = '0';
                    DEMANDE_INFOS_CAPTEURS[1] = '0'; //voies de 1 � 9
                    //DEMANDE_INFOS_CAPTEURS[1]='0';
                }
                //020 040 060 080
                if (((DEMANDE_INFOS_CAPTEURS[1] == '8') || (DEMANDE_INFOS_CAPTEURS[1] == '6') || (DEMANDE_INFOS_CAPTEURS[1] == '4') || (DEMANDE_INFOS_CAPTEURS[1] == '2') || (DEMANDE_INFOS_CAPTEURS[1] == '0'))&&(DEMANDE_INFOS_CAPTEURS[1] == '0')) {
                    DEMANDE_INFOS_CAPTEURS[0] = '0';
                    DEMANDE_INFOS_CAPTEURS[1] = '2'; //voie 20
                    DEMANDE_INFOS_CAPTEURS[1] = '0';
                }
                //01x 03x 05x 07x 09x
                if ((DEMANDE_INFOS_CAPTEURS[1] == '1') || (DEMANDE_INFOS_CAPTEURS[1] == '3') || (DEMANDE_INFOS_CAPTEURS[1] == '5') || (DEMANDE_INFOS_CAPTEURS[1] == '7') || (DEMANDE_INFOS_CAPTEURS[1] == '9')) {
                    DEMANDE_INFOS_CAPTEURS[0] = '0';
                    DEMANDE_INFOS_CAPTEURS[1] = '1'; //voie de 10 � 19
                    //DEMANDE_INFOS_CAPTEURS[1]='0';
                }
                //LES VOIES SONT TRANSFORMEES EN RELATIVES

                INSTRUCTION[0] = DEMANDE_INFOS_CAPTEURS[1];
                INSTRUCTION[1] = DEMANDE_INFOS_CAPTEURS[2];
                DEMANDE_INFOS_CAPTEURS[5] = DEMANDE_INFOS_CAPTEURS[0];
                DEMANDE_INFOS_CAPTEURS[6] = DEMANDE_INFOS_CAPTEURS[1];
                DEMANDE_INFOS_CAPTEURS[7] = DEMANDE_INFOS_CAPTEURS[2];
                DEMANDE_INFOS_CAPTEURS[8] = '1';
                DEMANDE_INFOS_CAPTEURS[9] = '6';

                goto fin3A;

            }

debut3:
            if (INSTRUCTION[3] != 'F') //UNE VOIE
            {
                if (INSTRUCTION[1] == '9') //Si x9
                {


                    DEMANDE_INFOS_CAPTEURS[6] = INSTRUCTION[0] + 1; //(x+1)
                    DEMANDE_INFOS_CAPTEURS[7] = '0'; //0


                } else {//Si xy

                    DEMANDE_INFOS_CAPTEURS[6] = INSTRUCTION[0];
                    DEMANDE_INFOS_CAPTEURS[7] = INSTRUCTION[1] + 1; //(y+1)
                }

            }

            if (INSTRUCTION[3] == 'F') //TOUTES LES VOIES
            {



                if (INSTRUCTION[1] == '9') {
                    INSTRUCTION[1] = '1';
                    INSTRUCTION[0] = INSTRUCTION[0] + 1;
                } else {
                    INSTRUCTION[1] = INSTRUCTION[1] + 2;
                }

                if (INSTRUCTION[0] == '1' && INSTRUCTION[1] == '9') {
                    goto fin3A;
                } else {

                    if ((TMPI == 1) || (TMPI == 0))
                        ENVOIE_DES_TRAMES(1, 315, INSTRUCTION);
                    if ((TMPI == 2) || (TMPI == 0))
                        ENVOIE_DES_TRAMES(2, 315, INSTRUCTION);
                    if ((TMPI == 3) || (TMPI == 0))
                        ENVOIE_DES_TRAMES(3, 315, INSTRUCTION);
                    if ((TMPI == 4) || (TMPI == 0))
                        ENVOIE_DES_TRAMES(4, 315, INSTRUCTION);
                    if ((TMPI == 5) || (TMPI == 0))
                        ENVOIE_DES_TRAMES(5, 315, INSTRUCTION);
                    TEMPO(3);
                    if (RESCRUTATION == 1) //BUG PREMIERE VOIES !!!!!! NUL NUL NUL
                    {

                        if ((INSTRUCTION[0] == '1')&&(INSTRUCTION[1] == '1')) {
                            INSTRUCTION[0] = '0'; //on part de 09 donc 10
                            INSTRUCTION[1] = '9';
                        } else {
                            //INSTRUCTION[0]='0'; //on part de 01
                            INSTRUCTION[1] = INSTRUCTION[1] - 2;
                        }


                        DEMANDE_INFOS_CAPTEURS[1] = '0'; //1 exemple 99 01 03
                        DEMANDE_INFOS_CAPTEURS[2] = '1'; //2
                        RESCRUTATION = 0;
                    }

                    goto debut3;
                }
            }

fin3A:

            if ((TMPI == 1) || (TMPI == 0))
                ENVOIE_DES_TRAMES(1, 315, INSTRUCTION);
            if ((TMPI == 2) || (TMPI == 0))
                ENVOIE_DES_TRAMES(2, 315, INSTRUCTION);
            if ((TMPI == 3) || (TMPI == 0))
                ENVOIE_DES_TRAMES(3, 315, INSTRUCTION);
            if ((TMPI == 4) || (TMPI == 0))
                ENVOIE_DES_TRAMES(4, 315, INSTRUCTION);
            if ((TMPI == 5) || (TMPI == 0))
                ENVOIE_DES_TRAMES(5, 315, INSTRUCTION);
            if (RESCRUTATION == 1) //BUG PREMIERE VOIES !!!!!! NUL NUL NUL
            {
                RESCRUTATION = 0;
                TEMPO(5);
                goto fin3A;
            }

fin3:
            if ((TMPI == 1) || (TMPI == 0))
                ENVOIE_DES_TRAMES(1, 276, DEMANDE_INFOS_CAPTEURS); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE
            if ((TMPI == 2) || (TMPI == 0))
                ENVOIE_DES_TRAMES(2, 276, DEMANDE_INFOS_CAPTEURS); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE
            if ((TMPI == 3) || (TMPI == 0))
                ENVOIE_DES_TRAMES(3, 276, DEMANDE_INFOS_CAPTEURS); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE
            if ((TMPI == 4) || (TMPI == 0))
                ENVOIE_DES_TRAMES(4, 276, DEMANDE_INFOS_CAPTEURS); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE
            if ((TMPI == 5) || (TMPI == 0))
                ENVOIE_DES_TRAMES(5, 276, DEMANDE_INFOS_CAPTEURS); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE



            if ((TMPI == 1) || (TMPI == 0))
                ENVOIE_DES_TRAMES(1, 304, INSTRUCTION);
            if ((TMPI == 2) || (TMPI == 0))
                ENVOIE_DES_TRAMES(2, 304, INSTRUCTION);
            if ((TMPI == 3) || (TMPI == 0))
                ENVOIE_DES_TRAMES(3, 304, INSTRUCTION);
            if ((TMPI == 4) || (TMPI == 0))
                ENVOIE_DES_TRAMES(4, 304, INSTRUCTION);
            if ((TMPI == 5) || (TMPI == 0))
                ENVOIE_DES_TRAMES(5, 304, INSTRUCTION);

            delay_ms(100);
            INFORMATION_ERREUR[0] = 'F'; //ON DIT A L'AUTOMATE QUE L'OPERATION EST TERMINEE
            INFORMATION_ERREUR[1] = 'F';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);

            INTCONbits.GIE = 1;

            break;

        default:
            delay_ms(100);
            break;

    }
}



void identificationINTER(char dequi, char fairequoi){ //F,F  exemple INTER...(6(carte AU),0x00(envoie ERR00))

    char QUI; //DU PRIORITAIRE AU MOINS
    char u;
    SERIALISATION(&ENTREES1, &ENTREES2);
    delay_ms(100);
    ERRMM = 0;
    QUI = 0x01;
    QUI = ENTREES2 & 0x10; //ALIM CHUTE

    QUI = ENTREES2 & 0x04; //POUR ACCRS485 IO

    if ((ENTREES2 == 0)&&(ENTREES1 == 0)) {
        delay_ms(100);
    }
    //MODE MANUEL
    if (dequi != 'F') {
        if (dequi == 1) //CARTE R1
        {
            ENTREES1 = 0x02;
            ENTREES2 = 0x00;
        }
        if (dequi == 7) //CARTE IO
        {
            ENTREES1 = 0x00;
            ENTREES2 = 0x04;
        }
        if (dequi == 6) //AU
        {
            ENTREES1 = 0x00;
            ENTREES2 = 0x02;
        }
    }

    if (fairequoi != 'F') {

        PRIO_AUTO_IO = fairequoi;
    }


    if ((ENTREES2 & 0x02) == 2) //AUTOMATE PRIOTAIRE
    {




        if (PRIO_AUTO_IO == 0xDD) {
            PRIO_AUTO_IO = 0xD1; //DEmande de l'auto pendant une action DIC de la CM->Rx
            //	INTCONbits.GIE= 0; // ON DESACTIVE LES INTERRUPTIONS SERONT REACTIVE SI REA OU A VOIR 21082017
        }
        if (PRIO_AUTO_IO == 0xE1) {
            PRIO_AUTO_IO = 0x00; //DEmande de l'auto pendant une action TEC (table exis.) de la CM->Rx
        }

        if (PRIO_AUTO_IO == 0xEE) {
            PRIO_AUTO_IO = 0xE1; //DEmande de l'auto pendant une action TEC (table exis.) de la CM->Rx

        }

        if (PRIO_AUTO_IO == 0xF1) {
            PRIO_AUTO_IO = 0xE1; //DEmande de l'auto pendant une action ZTE (table exis.) de la IO->CM
        }


        if (PRIO_AUTO_IO == 0x22) {
            PRIO_AUTO_IO = 0x00; //DEmande de l'auto pendant une action ZIG (PARAMETRES) de la IO->CM
        }

        if (PRIO_AUTO_IO == 0xCC) {
            PRIO_AUTO_IO = 0x00; //DEmande de l'auto pendant une action ZIZ de la IO->CM
        }
        if (PRIO_AUTO_IO == 0x77) //MODE HORLOGE
        {
            PRIO_AUTO_IO = 0x00;
        }
        if (PRIO_AUTO_IO == 0x00) //fonctionnement normal aucune action precedente (DIC ou de la carte IO)
            //FAIRE DRS485=1
        {
            if (ETAT_IO == 0xFF) //IO ON
            {
                stopIO(&ETAT_IO);
            }
            NBMM = 0;
            TEMPO(2); //4 toto
            PRIO_AUTO_IO = 0x01; //DEMANDE AUTOMATE EN MODE NORMAL

            INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = '0';
            for (u = 2; u < 20; u++) {
                INFORMATION_ERREUR[u] = 'x';
            }

            ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);
MM:

            TRAMERECEPTIONRS485DONNEES(9, &NORECEPTION, TRAMEIN);
            if ((TRAMEIN[0] != DEBUT_TRAME) || (TRAMEIN[1] != 'A') || (TRAMEIN[2] != 'U')) {
                // TRAME ORDRE INCOMPREHENSIBLE A GERE
                NORECEPTION = 0xFF;
                ERRMM = 1;

                INFORMATION_ERREUR[0] = 'M'; //TT EST BON donc utilisation type d'erreur
                INFORMATION_ERREUR[1] = 'M';
                for (u = 2; u < 20; u++) {
                    INFORMATION_ERREUR[u] = TRAMEIN[u - 2];
                }
                ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);
                INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
                INFORMATION_ERREUR[1] = '0';
                for (u = 2; u < 20; u++) {
                    INFORMATION_ERREUR[u] = 'x';
                }
                //			goto erreurtrame;

                ERRMM = 0;
                NBMM++;
                if (NBMM < 3) {
                    goto MM;
                }
            }

            if (NORECEPTION == 0xFF) {
                ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);
                TRAMERECEPTIONRS485DONNEES(5, &NORECEPTION, TRAMEIN);
            }
            if (NORECEPTION == 0xFF) {
                ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);
                TRAMERECEPTIONRS485DONNEES(5, &NORECEPTION, TRAMEIN);
            }
            if (NORECEPTION == 0xFF) {
                ENVOIE_DES_TRAMES(6, 302, INFORMATION_ERREUR);
                TRAMERECEPTIONRS485DONNEES(5, &NORECEPTION, TRAMEIN);
            }

            if (NORECEPTION != 0xFF)
                RECUPERATION_DES_TRAMES(TRAMEIN); //LA TRAMEOUT EST PRETTE A ETRE ENVOYEE
            else {
                delay_ms(100);
            }

            if (PRIO_AUTO_IO = 0x01)
                PRIO_AUTO_IO = 0x00; //RETOUR MODE NORMAL
        }
    }


    if ((ENTREES1 & 0x02) == 2) //POUR ACCRS485 R1
    {

        if (PRIO_AUTO_IO == 0x00) //fonctionnement normal aucune action precedente (DIC ou de la carte IO)
        {


            TRAMERECEPTIONRS485DONNEES(9, &NORECEPTION, TRAMEIN);
            if (NORECEPTION != 0xFF)
                RECUPERATION_DES_TRAMES(TRAMEIN); //LA TRAMEOUT EST PRETTE A ETRE ENVOYEE
        }
        delay_ms(100);
    }



    if ((ENTREES2 & 0x04) == 0x04) //CARTE IO
    {
        IOTRAVAIL = 1;

        if ((cycleN == 1)&&(IO_EN_COM == 0)) //Si sycle 1 mais pas MINITEL ou telgat
        { // OU DEMANDE IO PENDANT DIC (SCRUTATION) PENDANT LE CYCLE 2

            delay_ms(100);
            DRS485_IO = 1; //ON DESACTIVE LIO
            delay_ms(300);
            DRS485_IO = 0;
            ETAT_IO = 0x00;
            goto INITIO;

        }

        if (PRIO_AUTO_IO == 0xDD) {
            PRIO_AUTO_IO = 0xD2; //DEmande de l'IO pendant une action DIC de la CM->Rx

        }

        if (PRIO_AUTO_IO == 0xEE) {
            PRIO_AUTO_IO = 0xE2; //DEmande de l'IO pendant une action TEC (table exis.) de la CM->Rx

        }

        if (PRIO_AUTO_IO == 0x77) //ACTION HOR PRECEDENTE
        {
            PRIO_AUTO_IO = 0;
        }
        if (PRIO_AUTO_IO == 0x22) //ACTION ZOR HORLOGE PRECEDENTE
        {
            PRIO_AUTO_IO = 0;
        }
        if (PRIO_AUTO_IO == 0xCC) //ACTION ZIZ PRECEDENTE
        {
            PRIO_AUTO_IO = 0;
        }


        if (PRIO_AUTO_IO == 0x00) //fonctionnement normal aucune action precedente (DIC ou de la carte AU)
        {
            PRIO_AUTO_IO = 0x02; //DEMANDE L'IO EN MODE NORMAL
            TEMPO(2);

            INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = '0';
            for (u = 2; u < 20; u++) {
                INFORMATION_ERREUR[u] = 'x';
            }

            ENVOIE_DES_TRAMES(7, 302, INFORMATION_ERREUR);

            TRAMERECEPTIONRS485DONNEES(9, &NORECEPTION, TRAMEIN); //pour 9 environ 6S dattente
            if (NORECEPTION != 0xFF)
                RECUPERATION_DES_TRAMES(TRAMEIN); //LA TRAMEOUT EST PRETTE A ETRE ENVOYEE
            else {
                //delay_ms(100);
            }

            if (PRIO_AUTO_IO == 0x02)
                PRIO_AUTO_IO = 0x00; //RETOUR MODE NORMAL
        }


        delay_ms(100);

    }






INITIO:



    QUI = ENTREES1 & 0x01; //POUR ACCRS485 R2

    QUI = ENTREES2 & 0x80; ////POUR ACCRS485 R3

    QUI = ENTREES2 & 0x40; ////POUR ACCRS485 R4

    QUI = ENTREES2 & 0x20; //POUR ACCRS485 R5

    QUI = ENTREES2 & 0x01; //POUR ACCRS485 ALIM
}
