/* 
 * File:   Tension.c
 * Author: ADECEF
 *
 * Created on 10 mai 2021, 13:10
 */

#include <stdio.h>
#include <stdlib.h>
#include "h/CMS.h"

void CONTROLE_CHARGE_BATT(float fVbat) {

    char testchargeon;

    TRISJbits.TRISJ3 = 0; //STATO
    TRISEbits.TRISE7 = 1; //STAT

    testchargeon = 0;
    if (CHARGE_ON == 1)
        testchargeon = 1;

    //TEST ETAT BATTERIE
    if (testchargeon == 0) //SI ON A ALLUME CHARGE
    {
        STATO = 1; //ON TEST SI CHARGE TERMINEE


        if (STAT == 1) {
            delay_ms(100); //EN CHARGE
        }

        if (STAT == 0) {
            delay_ms(100); //CHARGE COMPLETE
            CHARGE_ON = 1; //ETEINDRE CHARGE
            //goto CONTROLE;
        }
    }


    if (testchargeon == 1) //SI ON A ETEINT CHARGE
    {

        if (fVbat <= 3.3) //ON REGARDE SI BATTERIE FAIBLE charge complete
            CHARGE_ON = 0; //si le cas on rallume charge

    }

    // TEST TEMPERATURE DANGER

    STATO = 0;
    if (testchargeon == 0) //SI CHARGE ALLUMEE
    {
        if (STAT == 1) //TEMPERATURE OK
        {

            delay_ms(100);
            CHARGE_ON = 0; //ALLUMER CHARGE

        }
        if (STAT == 0) //PROBLEME TEMPERATURE test valable si CHARGE_ON=0 (circuit charge aliment�) sinon ne pas prendre en compte
        {



            CHARGE_ON = 1; //ON ETEINT MEME SI C PROTEGE PAR LE CIRCUIT IL FAUDRAIT JUSTE RAJOUTER UNE INFORMATION POUR OPERATEUR A VOIR !!!!!!

        }
    }


    STATO = 0;

    if (testchargeon == 1) //SI CHARGE ETEINT
    {
        //CHARGE_ON=0;  //ON REALLUME LA CHARGE pour tester ????
        if (STAT == 1) //TEMPERATURE OK
        {

            delay_ms(100);
            CHARGE_ON = 0; //ON REALLUME LA CHARGE

        }

        if (STAT == 0) //TEMPERATURE NO OK
        {

            delay_ms(100);
            CHARGE_ON = 0; //ON REALLUME LA CHARGE
        }
    }
}

void MESURE_DES_TENSIONS(float *fV1, float *fV2, float *fV3, float *fV4, float *fV5, float *fVbat) {
    TRISAbits.TRISA0 = 1;
    TRISAbits.TRISA1 = 1;
    TRISAbits.TRISA2 = 1;
    TRISAbits.TRISA3 = 1;

    TRISAbits.TRISA5 = 1;

    TRISFbits.TRISF0 = 1;
    TRISFbits.TRISF1 = 1;
    TRISFbits.TRISF2 = 1;

    ADCON1 = 0x07; //de AN0 � AN7
    ADCON2 = 0xB8; //DROITE ET TEMPS CAN

    *fV1 = 2 * AVERAGE(0, 5, 1); //CARTE VDD1 R1
    *fV2 = 2 * AVERAGE(1, 5, 1); //CARTE VDD2 R2
    *fV3 = 2 * AVERAGE(2, 5, 1); //CARTE VDD3 R3
    *fV4 = 2 * AVERAGE(3, 5, 1); //CARTE VDD4 R4
    *fV5 = 2 * AVERAGE(5, 5, 1); //CARTE VDD5 R5
    *fVbat = 2 * AVERAGE(7, 5, 1); //VBAT //4.055176V tension batterie charg�e on observe >4.05 et ca diminue a 4.05

    //4.131348 a 48.70mA qd on est en charge
    // ca se coupe tt seul descend a 4.05 environ on coupe le mode charge batterie 1ier mosfet
    //et ca descend  jusqu'au niveau souhait�(exemple 3.6V) puis on rebranche mosfet1



    delay_ms(100);
}

float AVERAGE(char canal, char avemax, char megmax) {
    double moyenne = 0;
    double moyennes = 0;
    float tmp;
    char i, j;

    for (j = 0; j < megmax; j++) {

        for (i = 0; i < avemax; i++) {

            tmp = CAN(canal);
            moyenne = tmp + moyenne;
        }
        tmp = moyenne / (i);
        moyennes = moyennes + tmp;
        tmp = 0;
        moyenne = 0;
    }
    tmp = moyennes / (j);

    return (tmp);
}

float CAN(char V) {

    const double q = 0.001220703125;

    char VOIE;
    float tmp;

    switch (V) {
        case 0:
            VOIE = 0b00000000;
            break;
        case 1:
            VOIE = 0b00000100;
            break;
        case 2:
            VOIE = 0b00001000;
            break;
        case 3:
            VOIE = 0b00001100;
            break;
        case 4:
            VOIE = 0b00010000;
            break;
        case 5:
            VOIE = 0b00010100;
            break;
        case 6:
            VOIE = 0b00011000;
            break;

        case 7:
            VOIE = 0b00011100;
            break;

        default:
            // Code
            break;
    }

    ADCON0 = 0x00 & ADCON0;
    ADCON0 = VOIE | ADCON0;
    ADCON0 = 0x01 | ADCON0;
    ADCON0bits.GO_DONE = 1; // lancement conver
    while (ADCON0bits.GO_DONE) {
    } // attendre conver
    tmp = (float) ADRES*q;
    ADCON0bits.ADON = 0;

    return (tmp);
}