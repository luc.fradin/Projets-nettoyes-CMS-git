/* 
 * File:   CMS.h
 * Author: ADECEF
 *
 * Created on 6 mai 2021, 08:26
 */

#ifndef CMS_H
#define	CMS_H

#define DEBUT_TRAME 35  //Ascii de #
#define FIN_TRAME 42    //Ascii de *
#include <p18F8723.h> //bibliothèque du Microcontroleur




#define DERE PORTAbits.RA4 // direction RX TX
#define RX PORTCbits.RC7 // RX
#define INT0 PORTBbits.RB0 // INT0
#define INT1 PORTBbits.RB1 // INT1
#define INT2 PORTBbits.RB2 // INT2

#define I2C_MEMOIRE PORTJbits.RJ1
#define I2C_HORLOGE PORTJbits.RJ0
#define I2C_INTERNE PORTJbits.RJ2

#define CHARGE_ON PORTEbits.RE1


#define DX10 PORTHbits.RH0
#define DX20 PORTHbits.RH1


#define DX1 PORTHbits.RH2
#define DX2 PORTHbits.RH3

#define STATO PORTJbits.RJ3
#define STAT PORTEbits.RE7

//BASCULE
#define SHLD PORTDbits.RD0
#define CLK PORTDbits.RD3
#define INH PORTDbits.RD2

#define IO_EN_COM PORTHbits.RH4


#define DRS485_R1 PORTFbits.RF3
#define DRS485_R2 PORTFbits.RF4
#define DRS485_R3 PORTFbits.RF5
#define DRS485_R4 PORTFbits.RF6
#define DRS485_R5 PORTFbits.RF7


#define CYCLE_R1 PORTJbits.RJ4
#define CYCLE_R2 PORTJbits.RJ5
#define CYCLE_R4 PORTJbits.RJ6
#define CYCLE_R3 PORTJbits.RJ7
#define CYCLE_R5 PORTCbits.RC2


#define DRS485_IO PORTBbits.RB4 /////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#define RIPOWER PORTHbits.RH6

#define RESETCMS PORTHbits.RH5

//Dans Tempo.c
void delay_us(char tempo);  //Ok
void delay_ms(char tempo);  //Ok
void TEMPO(char seconde);   //Ok

//Dans Trames.c
void resetTO(unsigned char *cTrameReset);    //Ok
int recuperer_trame_memoire(char w, char mode, unsigned char *cTrameEntree, unsigned char *cTrameSortie, unsigned char *cVoieCod, unsigned char *cHorloge, char *cEtatZizAuto, char *cEtat);  //Ok
int construire_trame_memoire(unsigned char *cTrameEntree, unsigned char *cTrameSortie, unsigned char *cVoieCod, unsigned char *cDemandeInfoCapteur, char *cEtat, char *cTmp); //Ok
void RECUPERATION_DES_TRAMES(unsigned char *tab);
void ENVOIE_DES_TRAMES(char R, int fonctionRS, unsigned char *tab);
void INTERROGER_TOUT_LES_CAPTEURS(char t, unsigned char *cTmpTab, unsigned char *cDemandeInfoCapteur);  //Ok
void SAV_EXISTENCE(unsigned char *tt, unsigned char *cTmpTab);  //Ok
int VALIDER_CAPTEUR_CM(unsigned char *cTrameEntree, unsigned char *cTrameSortie, unsigned char *cVoieCod, unsigned char *cHorloge, char *cEtatZizAuto, unsigned char *cTmpTab, char *cEtat, char cNombreCapteursCable); //Ok
void intervertirOUT_IN(unsigned char *cTrameEntree, unsigned char *cTrameSortie); //Ok
void RECHERCHE_DONNEES_GROUPE(unsigned char ici, unsigned char *cTmpTab);   //Ok
void PREPARATION_PAGE_AUTO(unsigned char *cPageTab, unsigned char *cTrameEntree, unsigned char *cTrameSortie, unsigned char *cTmpTab);  //Ok
int SDR(unsigned char *cTrameEntree, unsigned char *cTrameSortie, unsigned char *cVoieCod,unsigned char *cInstruction, char *cBitSD, unsigned char *cHorloge);    //Ok
int TRANSFORMER_TRAME_MEM_EN_COM(unsigned char *cTrameEntree, unsigned char *cTrameSortie, unsigned char *cVoieCod); //Ok
void EFFACEMENT(unsigned char *tab, unsigned char *cTrameEfface); //Ok
char trouver_carte_suivantvoie(unsigned char *tab); //Ok
void REPOSAQUIENVOYER(unsigned char *tabo); //Ok
void AQUIENVOYER(unsigned char *tabo);  //Ok
char EFFACER_CAPTEUR_SUR_CM(char gh, unsigned char *cDemandeInfosCapteurs, char *cNombreCapteursCable, char *cCaptExist, unsigned char *cTmpTab, unsigned char *cVoieCod, char *cEtat);    //Ok
void MODIFICATION_TABLE_DEXISTENCE(unsigned char *v, char eff, char etat, unsigned char *cTmpTab, char cNombreCapteursCable); //Ok
void CHERCHER_DONNNEES_CABLES(unsigned char *g, char j, unsigned char *cTmpTab, unsigned char *cVoieCod, char *cEtat, char *cCaptExist);    //Ok
void SERIALISATION(char *cEntree1, char *cEntree2);   //Ok
void SDW(char yui,unsigned char *cTrameEntree, unsigned char *cTrameSortie, char *cTypeCarte, float fV1, float fV2, float fV3, float fV4, float fV5, float fVbat, unsigned char *cSavTab,unsigned char *cTmpTab, char cR1NoRep, char cR2NoRep, char cR3NoRep, char cR4NoRep, char cR5NoRep, unsigned char *cHorloge);
void IDENTIF_TYPE_CARTE(unsigned char *cTmpTab, char *cTypeCarte, float fV1, float fV2, float fV3, float fV4, float fV5, float fVbat, char cR1NoRep, char cR2NoRep, char cR3NoRep, char cR4NoRep, char cR5NoRep);
void SDWBN(unsigned char *cTrameEntree, unsigned char *cTrameSortie, unsigned char *cHorloge);  //Ok
void SDRBN(unsigned char *cTrameEntree, unsigned char *cTrameSortie, unsigned char *cCRC, unsigned char *cHorloge, char *cNoReception); //Ok
void REMISE_EN_FORME_PAGE(unsigned char *cTrameSortie, unsigned char *cCRC, int *iPointeurEntreeSortie);    //Ok
void envoyer_TRAME_PICSD(char SENS, unsigned char *cTrameEntree, unsigned char *cTrameSortie, unsigned char *cHorloge); //Ok
void TROUVER_ROBINET(unsigned char *cDemandeInfoCapteur, unsigned char *cInstruction, unsigned char *cTmpTab);  //Ok

//Dans CRC.c
void calcul_CRC(unsigned char *cCRC, unsigned char *cTrameSortie);    //Ok
void controle_CRC(unsigned char *cCRC, unsigned char *cTrameEntree);  //Ok
char CRC_verif(unsigned char *cCRC, unsigned char *cTrameEntree, int iPointeurEntreeSortie);  //Ok
unsigned int CRC16(unsigned char far *txt, unsigned char lg_txt);    //Ok

//SPI
unsigned char SPI_WriteByte(unsigned char val);
void init_spi(void);
void spi_low(void);

//I2C
void Init_I2C(void);


//EEPROM
unsigned char LIRE_EEPROM(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char r);
unsigned char ECRIRE_EEPROMBYTE(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char g);
void pic_eeprom_write(unsigned char Address, unsigned char contenu);
char pic_eeprom_read(unsigned char Address);
unsigned char putstringI2C(unsigned char *wrptr);

//RS485
void Init_RS485(void);
char RECEVOIR_RS485(unsigned char tout, char *cNoReception);
void RS485(unsigned char carac);
void TRAMERECEPTIONRS485DONNEES(unsigned char tout, char *cNoReception, unsigned char *cTrameEntree);
void TRAMEENVOIRS485DONNEES(unsigned char *cTrameEnvoyee);

//Horloge
unsigned char ECRIRE_HORLOGE(unsigned char zone, unsigned char time);
unsigned char envoyerHORLOGE_i2c(unsigned char NumeroH, unsigned char ADDH, unsigned char dataH0);
unsigned char LIRE_HORLOGE(unsigned char zone);
signed char litHORLOGE_i2c(unsigned char NumeroH, unsigned char AddH);
void creer_trame_horloge(unsigned char *cHorloge);
void entrer_trame_horloge(unsigned char *cHorloge);
void lire_horloge(unsigned char *cHorloge);
void reglerhorlogeautres(unsigned char *cHorloge, float fV1, float fV2, float fV3, float fV4, float fV5, float fVbat, char cR1NoRep, char cR2NoRep, char cR3NoRep, char cR4NoRep, char cR5NoRep);

//Memoire
void GROUPE_EN_MEMOIRE(unsigned char *tab, unsigned char *cTrameSortie, char lui);
void METTRE_EN_MEMOIRE_LISTE(unsigned char *tab, char t);
unsigned int calcul_addmemoire(unsigned char *tab);
unsigned char choix_memoire(unsigned char *tab);


//Tension
void CONTROLE_CHARGE_BATT(float fVbat);
void MESURE_DES_TENSIONS(float *fV1, float *fV2, float *fV3, float *fV4, float *fV5, float *fVbat);
float AVERAGE(char canal, char avemax, char megmax);
float CAN(char V);

//Interruption
void identificationINTER(char dequi, char fairequoi);
void inter(void);

//IO
void stopIO(char *cEtatIO);

//InToChar
void IntToChar(signed int value, unsigned char *chaine, int Precision);
void IntToChar5(unsigned int value, char *chaine, char Precision);

#endif	/* CMS_H */

