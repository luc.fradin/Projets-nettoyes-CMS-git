#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=cof
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/CMS2CarteMereFonctions.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/CMS2CarteMereFonctions.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../project/Tempo.c ../project/Trames.c ../project/CRC.c ../project/SPI.c ../project/Eeprom.c ../project/RS485.c ../project/HORLOGE.c ../project/Memoire.c ../project/Tension.c ../project/IO.c ../project/InToChar.c ../project/I2C.c ../project/CMSModifV1Nettoye.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1989816886/Tempo.o ${OBJECTDIR}/_ext/1989816886/Trames.o ${OBJECTDIR}/_ext/1989816886/CRC.o ${OBJECTDIR}/_ext/1989816886/SPI.o ${OBJECTDIR}/_ext/1989816886/Eeprom.o ${OBJECTDIR}/_ext/1989816886/RS485.o ${OBJECTDIR}/_ext/1989816886/HORLOGE.o ${OBJECTDIR}/_ext/1989816886/Memoire.o ${OBJECTDIR}/_ext/1989816886/Tension.o ${OBJECTDIR}/_ext/1989816886/IO.o ${OBJECTDIR}/_ext/1989816886/InToChar.o ${OBJECTDIR}/_ext/1989816886/I2C.o ${OBJECTDIR}/_ext/1989816886/CMSModifV1Nettoye.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1989816886/Tempo.o.d ${OBJECTDIR}/_ext/1989816886/Trames.o.d ${OBJECTDIR}/_ext/1989816886/CRC.o.d ${OBJECTDIR}/_ext/1989816886/SPI.o.d ${OBJECTDIR}/_ext/1989816886/Eeprom.o.d ${OBJECTDIR}/_ext/1989816886/RS485.o.d ${OBJECTDIR}/_ext/1989816886/HORLOGE.o.d ${OBJECTDIR}/_ext/1989816886/Memoire.o.d ${OBJECTDIR}/_ext/1989816886/Tension.o.d ${OBJECTDIR}/_ext/1989816886/IO.o.d ${OBJECTDIR}/_ext/1989816886/InToChar.o.d ${OBJECTDIR}/_ext/1989816886/I2C.o.d ${OBJECTDIR}/_ext/1989816886/CMSModifV1Nettoye.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1989816886/Tempo.o ${OBJECTDIR}/_ext/1989816886/Trames.o ${OBJECTDIR}/_ext/1989816886/CRC.o ${OBJECTDIR}/_ext/1989816886/SPI.o ${OBJECTDIR}/_ext/1989816886/Eeprom.o ${OBJECTDIR}/_ext/1989816886/RS485.o ${OBJECTDIR}/_ext/1989816886/HORLOGE.o ${OBJECTDIR}/_ext/1989816886/Memoire.o ${OBJECTDIR}/_ext/1989816886/Tension.o ${OBJECTDIR}/_ext/1989816886/IO.o ${OBJECTDIR}/_ext/1989816886/InToChar.o ${OBJECTDIR}/_ext/1989816886/I2C.o ${OBJECTDIR}/_ext/1989816886/CMSModifV1Nettoye.o

# Source Files
SOURCEFILES=../project/Tempo.c ../project/Trames.c ../project/CRC.c ../project/SPI.c ../project/Eeprom.c ../project/RS485.c ../project/HORLOGE.c ../project/Memoire.c ../project/Tension.c ../project/IO.c ../project/InToChar.c ../project/I2C.c ../project/CMSModifV1Nettoye.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/CMS2CarteMereFonctions.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F8723
MP_PROCESSOR_OPTION_LD=18f8723
MP_LINKER_DEBUG_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1989816886/Tempo.o: ../project/Tempo.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Tempo.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Tempo.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/Tempo.o   ../project/Tempo.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/Tempo.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/Tempo.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/Trames.o: ../project/Trames.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Trames.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Trames.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/Trames.o   ../project/Trames.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/Trames.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/Trames.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/CRC.o: ../project/CRC.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/CRC.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/CRC.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/CRC.o   ../project/CRC.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/CRC.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/CRC.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/SPI.o: ../project/SPI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/SPI.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/SPI.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/SPI.o   ../project/SPI.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/SPI.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/SPI.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/Eeprom.o: ../project/Eeprom.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Eeprom.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Eeprom.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/Eeprom.o   ../project/Eeprom.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/Eeprom.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/Eeprom.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/RS485.o: ../project/RS485.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/RS485.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/RS485.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/RS485.o   ../project/RS485.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/RS485.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/RS485.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/HORLOGE.o: ../project/HORLOGE.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/HORLOGE.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/HORLOGE.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/HORLOGE.o   ../project/HORLOGE.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/HORLOGE.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/HORLOGE.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/Memoire.o: ../project/Memoire.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Memoire.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Memoire.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/Memoire.o   ../project/Memoire.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/Memoire.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/Memoire.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/Tension.o: ../project/Tension.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Tension.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Tension.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/Tension.o   ../project/Tension.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/Tension.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/Tension.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/IO.o: ../project/IO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/IO.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/IO.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/IO.o   ../project/IO.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/IO.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/IO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/InToChar.o: ../project/InToChar.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/InToChar.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/InToChar.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/InToChar.o   ../project/InToChar.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/InToChar.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/InToChar.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/I2C.o: ../project/I2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/I2C.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/I2C.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/I2C.o   ../project/I2C.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/I2C.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/I2C.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/CMSModifV1Nettoye.o: ../project/CMSModifV1Nettoye.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/CMSModifV1Nettoye.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/CMSModifV1Nettoye.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/CMSModifV1Nettoye.o   ../project/CMSModifV1Nettoye.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/CMSModifV1Nettoye.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/CMSModifV1Nettoye.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
else
${OBJECTDIR}/_ext/1989816886/Tempo.o: ../project/Tempo.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Tempo.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Tempo.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/Tempo.o   ../project/Tempo.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/Tempo.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/Tempo.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/Trames.o: ../project/Trames.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Trames.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Trames.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/Trames.o   ../project/Trames.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/Trames.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/Trames.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/CRC.o: ../project/CRC.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/CRC.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/CRC.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/CRC.o   ../project/CRC.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/CRC.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/CRC.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/SPI.o: ../project/SPI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/SPI.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/SPI.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/SPI.o   ../project/SPI.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/SPI.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/SPI.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/Eeprom.o: ../project/Eeprom.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Eeprom.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Eeprom.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/Eeprom.o   ../project/Eeprom.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/Eeprom.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/Eeprom.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/RS485.o: ../project/RS485.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/RS485.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/RS485.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/RS485.o   ../project/RS485.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/RS485.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/RS485.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/HORLOGE.o: ../project/HORLOGE.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/HORLOGE.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/HORLOGE.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/HORLOGE.o   ../project/HORLOGE.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/HORLOGE.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/HORLOGE.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/Memoire.o: ../project/Memoire.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Memoire.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Memoire.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/Memoire.o   ../project/Memoire.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/Memoire.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/Memoire.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/Tension.o: ../project/Tension.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Tension.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Tension.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/Tension.o   ../project/Tension.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/Tension.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/Tension.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/IO.o: ../project/IO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/IO.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/IO.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/IO.o   ../project/IO.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/IO.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/IO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/InToChar.o: ../project/InToChar.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/InToChar.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/InToChar.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/InToChar.o   ../project/InToChar.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/InToChar.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/InToChar.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/I2C.o: ../project/I2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/I2C.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/I2C.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/I2C.o   ../project/I2C.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/I2C.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/I2C.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/CMSModifV1Nettoye.o: ../project/CMSModifV1Nettoye.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/CMSModifV1Nettoye.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/CMSModifV1Nettoye.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../project/h" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/CMSModifV1Nettoye.o   ../project/CMSModifV1Nettoye.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/CMSModifV1Nettoye.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/CMSModifV1Nettoye.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/CMS2CarteMereFonctions.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    ../project/18f8723.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "..\project\18f8723.lkr"  -p$(MP_PROCESSOR_OPTION_LD)  -w -x -u_DEBUG -m"${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map"  -z__MPLAB_BUILD=1  -u_CRUNTIME -z__MPLAB_DEBUG=1 $(MP_LINKER_DEBUG_OPTION) -l ${MP_CC_DIR}\\..\\lib  -o dist/${CND_CONF}/${IMAGE_TYPE}/CMS2CarteMereFonctions.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}   
else
dist/${CND_CONF}/${IMAGE_TYPE}/CMS2CarteMereFonctions.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   ../project/18f8723.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "..\project\18f8723.lkr"  -p$(MP_PROCESSOR_OPTION_LD)  -w  -m"${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map"  -z__MPLAB_BUILD=1  -u_CRUNTIME -l ${MP_CC_DIR}\\..\\lib  -o dist/${CND_CONF}/${IMAGE_TYPE}/CMS2CarteMereFonctions.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}   
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
