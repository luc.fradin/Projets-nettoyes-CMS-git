// PIC18F8723 Configuration Bit Settings
// 'C' source line config statements

#include <p18F8723.h> //biblioth�que du Microcontroleur

// CONFIG1H
#pragma config OSC = HS         // Oscillator Selection bits (HS oscillator)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Two-Speed Start-up disabled)

// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = SBORDIS  // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
#pragma config BORV = 3         // Brown-out Voltage bits (Minimum setting)

// CONFIG2H
#pragma config WDT = OFF        // Watchdog Timer (WDT disabled (control is placed on the SWDTEN bit))
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3L
#pragma config MODE = MC        // Processor Data Memory Mode Select bits (Microcontroller mode)
#pragma config ADDRBW = ADDR20BIT// Address Bus Width Select bits (20-bit Address Bus)
#pragma config DATABW = DATA16BIT// Data Bus Width Select bit (16-bit External Bus mode)
#pragma config WAIT = OFF       // External Bus Data Wait Enable bit (Wait selections are unavailable for table reads and table writes)

// CONFIG3H
#pragma config CCP2MX = PORTC   // CCP2 MUX bit (ECCP2 input/output is multiplexed with RC1)
#pragma config ECCPMX = PORTE   // ECCP MUX bit (ECCP1/3 (P1B/P1C/P3B/P3C) are multiplexed onto RE6, RE5, RE4 and RE3 respectively)
#pragma config LPT1OSC = OFF    // Low-Power Timer1 Oscillator Enable bit (Timer1 configured for higher power operation)
#pragma config MCLRE = ON       // MCLR Pin Enable bit (MCLR pin enabled; RG5 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
#pragma config LVP = OFF        // Single-Supply ICSP Enable bit (Single-Supply ICSP disabled)
#pragma config BBSIZ = BB2K     // Boot Block Size Select bits (1K word (2 Kbytes) Boot Block size)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled (Legacy mode))

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not code-protected)
#pragma config CP1 = OFF        // Code Protection bit Block 1 (Block 1 (004000-007FFFh) not code-protected)
#pragma config CP2 = OFF        // Code Protection bit Block 2 (Block 2 (008000-00BFFFh) not code-protected)
#pragma config CP3 = OFF        // Code Protection bit Block 3 (Block 3 (00C000-00FFFFh) not code-protected)
#pragma config CP4 = OFF        // Code Protection bit Block 4 (Block 4 (010000-013FFFh) not code-protected)
#pragma config CP5 = OFF        // Code Protection bit Block 5 (Block 5 (014000-017FFFh) not code-protected)
#pragma config CP6 = OFF        // Code Protection bit Block 6 (Block 6 (01BFFF-018000h) not code-protected)
#pragma config CP7 = OFF        // Code Protection bit Block 7 (Block 7 (01C000-01FFFFh) not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Boot Block Code Protection bit (Boot Block (000000-0007FFh) not code-protected)
#pragma config CPD = OFF        // Data EEPROM Code Protection bit (Data EEPROM not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not write-protected)
#pragma config WRT1 = OFF       // Write Protection bit Block 1 (Block 1 (004000-007FFFh) not write-protected)
#pragma config WRT2 = OFF       // Write Protection bit Block 2 (Block 2 (008000-00BFFFh) not write-protected)
#pragma config WRT3 = OFF       // Write Protection bit Block 3 (Block 3 (00C000-00FFFFh) not write-protected)
#pragma config WRT4 = OFF       // Write Protection bit Block 4 (Block 4 (010000-013FFFh) not write-protected)
#pragma config WRT5 = OFF       // Write Protection bit Block 5 (Block 5 (014000-017FFFh) not write-protected)
#pragma config WRT6 = OFF       // Write Protection bit Block 6 (Block 6 (01BFFF-018000h) not write-protected)
#pragma config WRT7 = OFF       // Write Protection bit Block 7 (Block 7 (01C000-01FFFFh) not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) not write-protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block (000000-007FFF, 000FFF or 001FFFh) not write-protected)
#pragma config WRTD = OFF       // Data EEPROM Write Protection bit (Data EEPROM not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection bit Block 1 (Block 1 (004000-007FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR2 = OFF      // Table Read Protection bit Block 2 (Block 2 (008000-00BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR3 = OFF      // Table Read Protection bit Block 3 (Block 3 (00C000-00FFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR4 = OFF      // Table Read Protection bit Block 4 (Block 4 (010000-013FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR5 = OFF      // Table Read Protection bit Block 5 (Block 5 (014000-017FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR6 = OFF      // Table Read Protection bit Block 6 (Block 6 (018000-01BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR7 = OFF      // Table Read Protection bit Block 7 (Block 7 (01C000-01FFFFh) not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Boot Block Table Read Protection bit (Boot Block (000000-007FFF, 000FFF or 001FFFh) not protected from table reads executed in other blocks)



#include <stdlib.h> //biblioth�que
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <i2c.h>
#include <spi.h>

#pragma udata udata1

// MEMOIRE la taille #03301R111122223333BRA/4444/22/1COMMENTAIRESICIROB03326111525001020000456781108650888* soit 86octets
unsigned char TRAMEOUT[125];

unsigned char TRAMEIN[125];

#pragma udata udata2
//MEMOIRE/////////////////////
char VOIECOD[6];
//char TYPE[1];           //
//char CMOD[4];//
//char CCON[4];//
//char CREP[4];//
//char CONSTITUTION[13];//
//char COMMENTAIRE[20];//
//char CABLE[3];//                      Variables utilis�s qu'en sortie
//char JJ[2];//
//char MM[2];//
//char HH[2];//
//char mm[2];//
//char ii[1];//
//char COD[2];//
//char POS[2];//
//char iiii[4];//
//char DISTANCE[5];//
char ETAT; // ETAT[1] inutilis� donc remplacement de ETAT[2] par un char ETAT
//char VALEUR[4];//
//char SEUIL[4];//
char TYPECARTE[5];
//////////////////////////

////// TRAMES IDENTIQUES
//char START[1]; //Remplacement de START[1] par cDebutTrame
char cDebutTrame;
char ID1[2];
char ID2[2];
//unsigned char FONCTION[3]; // Remplacement du tableau global FONCTION par le tableau cFonctionRecup dans RECUPERATION_DES_TRAMES et par cFonctionEnvoie dans ENVOIE_DES_TRAMES
char CRC[5];
char cFinTrame; //cFinTrame remplace STOP[1]

//PARTIE DE LISTE


////// TRAMES ERREUR
//char TYPE_ERREUR[2];//        //Variable pouvant �tre supprim�e
char INFORMATION_ERREUR[20];
char NBRECAPTEURS_CABLE;
/////

char PRIO_AUTO_IO; //0x00 rien //0x01
///// TRAME  HORLOGE
char HORLOGE[12];
/////TRAME EFFACER CAPTEUR
// char CAPTEUR_A_EFFACER[5];//     //Variable inutilis�e
/////DEMANDE INFOS A LA CARTE MERE ou demande a la carte relais
char DEMANDE_INFOS_CAPTEURS[10];
////OEIL
//char OEIL_CAPTEUR[5];//   //Variable pouvant �tre supprim�e
////
char PAGE[6];

char INSTRUCTION[25]; //pour l'envoi d'instruction

char capt_exist;

float VDD1, VDD2, VDD3, VDD4, VDD5, VBAT;
//float Vtemp, VDD6;  //Variables unniquement en sortie
//
char NORECEPTION;


int pointeur_out_in;
char tmp, tmp2;
unsigned int TMPI;

char ENTREES1, ENTREES2;

//char HEURE, MINUTE, SECONDE, MOIS, JOUR, ANNEE, DATE;//  Remplac�s par cAnneeEntrer, cMoisEntrer, cDateEntrer, cHeureEntrer, cMinuteEntrer, cSecondeEntrer dans entrer_trame_horloge et par cAnneeCree, cMoisCree, cDateCree, cHeureCree, cMinuteCree, cSecondeCree dans creer_trame_horloge

#pragma udata udata3
char TMP[125]; // PR SAV DES TRAMES
//char P_RESERVOIR[15];//
//char P_SORTIE[15];//
//char P_SECOURS[15];//
//char POINT_ROSEE[11];//
//char DEBIT_GLOBAL[11];//                          Inutilis�s
//char TEMPS_CYCLE_MOTEUR[11];//
//char TEMPS_MOTEUR[15];//
//char PRESENCE_220V[6];//
//char PRESENCE_48V[6];//

char DATAFIX[10]; // Bug quand plac� dans le main

#pragma udata udata4
//char SAVTAB[25];//        //Remplac� par cSAVTABSwd dans SWD et par cSAVTABRecup dans RECUPERATION_DES_TRAMES
//int calculBLOC;           // Remplacement par iCalculBLOCRecup dans RECUPERATION_DES_TRAMES et par icalculBLOCSWBN dans SWBN
//char FONCTION_ACTIVATION_AUTO;        //Uniquement en sortie
char numdecarteRI;
char TROPDENONREPONSE;

char CRC_ETAT;
char ETAT_IO;
char FIRST_DEMAR; //PREMIER DEMAR DU  CMS MISE EN ALIM

char MODERAPID;

//char TYPECOMMANDE;                //D�claration dans RECUPERATION_DES_TRAMES
//char temoinfinvoieZIZ;            //D�claration dans RECUPERATION_DES_TRAMES
char IOTRAVAIL;

char PASDE_S;
char cycleN; //CYCLE 1 DIC CYCLE 2 ZIZ
char RESCRUTATION;
char NUMEROCARTE;


//char NBREDEMANDEIO;//     //Inutilis�

char PRESENCEDEBITSD; //POURSAV SD VERS CM

char R1NOREP, R2NOREP, R3NOREP, R4NOREP, R5NOREP;
char UNRESETRI1, UNRESETRI2, UNRESETRI3, UNRESETRI4, UNRESETRI5;
char MINIRESETRI;

//char TYPEDECARTEOEIL;           //D�claration dans ENVOIE_DES_TRAMES car seulement utilis� l�
//int TIMEOUTCYCLE;//             //D�clar� dans le main


char scrutationunevoie;
char ETATZIZCRATIONAUTOMATE;

//char mouchardINT0;//          //Utilis�e qu'en sortie
char DEMANDEZIGIO;

//char resetheurefixe;//            //Declaration dans controleheureRESET
//unsigned char SAVAUTOCYCLE;//     //Utilis�e qu'en sortie
char MAXBITE;
char MAJBLOC;
char DEMANDEZPLIO;


void delay_us(char tempo);
void delay_ms(char tempo);

void resetTO(void);
void calcul_CRC(void);
void controle_CRC(void);

void recuperer_trame_memoire(char r, char mode);
void construire_trame_memoire(void);
void RECUPERATION_DES_TRAMES(char *tab);
void Init_RS485(void);
char RECEVOIR_RS485(unsigned char tout);
void RS485(unsigned char carac);
void ENVOIE_DES_TRAMES(char R, int fonctionRS, char *tab);
void TEMPO(char seconde);
void TRAMERECEPTIONRS485DONNEES(unsigned char tout);
unsigned char putstringI2C(unsigned char *wrptr);

void SAV_EXISTENCE(char *tt);

unsigned char ECRIRE_HORLOGE(unsigned char zone, unsigned char time);
unsigned char envoyerHORLOGE_i2c(unsigned char NumeroH, unsigned char ADDH, unsigned char dataH0);
unsigned char LIRE_HORLOGE(unsigned char zone);
signed char litHORLOGE_i2c(unsigned char NumeroH, unsigned char AddH);
void creer_trame_horloge(void);
void entrer_trame_horloge(void);

char VALIDER_CAPTEUR_CM(void);

//FONCTION POUR LES TRAMES STANDARDS
void intervertirOUT_IN(void);


void RECHERCHE_DONNEES_GROUPE(unsigned char ici);
void Init_I2C(void);
unsigned char LIRE_EEPROM(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char r);
unsigned char ECRIRE_EEPROMBYTE(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char g);
unsigned int calcul_addmemoire(unsigned char *tab);
unsigned char choix_memoire(unsigned char *tab);
char FCYCLE, TESTCYCLE;
void IntToChar(signed int value, char *chaine, int Precision);
void TRANSFORMER_TRAME_MEM_EN_COM(void);
void EFFACEMENT(unsigned char *tab);

char trouver_carte_suivantvoie(char *tab);

void GROUPE_EN_MEMOIRE(unsigned char *tab, char lui);
void EFFACER_LISTE(char t);
void METTRE_EN_MEMOIRE_LISTE(unsigned char *tab, char t);
void INTERROGER_TOUT_LES_CAPTEURS(char t);

void CONTROLE_CHARGE_BATT(void);
void MESURE_DES_TENSIONS(void);
float CAN(char V);
void lire_horloge(void);
//INTERRUPTIONS

void SERIALISATION(void);

void identificationINTER(char dequi, char fairequoi);

void REPOSAQUIENVOYER(unsigned char *tabo);
void AQUIENVOYER(unsigned char *tabo);
void PREPARATION_PAGE_AUTO(void);

float AVERAGE(char canal, char avemax, char megmax);

char EFFACER_CAPTEUR_SUR_CM(char gh);
void MODIFICATION_TABLE_DEXISTENCE(char *v, char eff, char etat); //eff pour effacer (eff=2 alors recopie) alors et etat ligne pour fusible HS ou CC ou surtension
void CHERCHER_DONNNEES_CABLES(char *g, char j);

unsigned char SPI_WriteByte(unsigned char val);
unsigned char SD_SendCommand(char CMD, unsigned long sct, char CRC);
unsigned char SD_ReadSingleBlock(unsigned long sector);
unsigned char SD_WriteSingleBlock(unsigned long sector);
unsigned char sd_reset();
void init_spi(void);

void spi_high(void);
void spi_low(void);

void IDENTIF_TYPE_CARTE(void);
void TROUVER_ROBINET(void);

void inter(void);
//void voir_si_voie_existe(void);
void REMISE_EN_FORME_PAGE(void);
void SDRBN(void);
void SDR(void);
void SDW(char yui);

void envoyer_TRAME_PICSD(char SENS);

unsigned int CRC16(char far *txt, unsigned char lg_txt);
void IntToChar5(unsigned int value, char *chaine, char Precision);
void CRC_verif(void);

void reglerhorlogeautres(void);
void TRAMEENVOIRS485DONNEES(void);
void stopIO(void);

void pic_eeprom_write(unsigned char Address, unsigned char contenu);
char pic_eeprom_read(unsigned char Address);
void SDWBN(void);
void controleheureRESET(void);
//RS485
#define DERE PORTAbits.RA4 // direction RX TX
#define RX PORTCbits.RC7 // RX
#define INT0 PORTBbits.RB0 // INT0
#define INT1 PORTBbits.RB1 // INT1
#define INT2 PORTBbits.RB2 // INT2

#define I2C_MEMOIRE PORTJbits.RJ1
#define I2C_HORLOGE PORTJbits.RJ0
#define I2C_INTERNE PORTJbits.RJ2

#define CHARGE_ON PORTEbits.RE1


#define DX10 PORTHbits.RH0
#define DX20 PORTHbits.RH1


#define DX1 PORTHbits.RH2
#define DX2 PORTHbits.RH3

#define STATO PORTJbits.RJ3
#define STAT PORTEbits.RE7

//BASCULE
#define SHLD PORTDbits.RD0
#define CLK PORTDbits.RD3
#define INH PORTDbits.RD2

#define IO_EN_COM PORTHbits.RH4


#define DRS485_R1 PORTFbits.RF3
#define DRS485_R2 PORTFbits.RF4
#define DRS485_R3 PORTFbits.RF5
#define DRS485_R4 PORTFbits.RF6
#define DRS485_R5 PORTFbits.RF7


#define CYCLE_R1 PORTJbits.RJ4
#define CYCLE_R2 PORTJbits.RJ5
#define CYCLE_R4 PORTJbits.RJ6
#define CYCLE_R3 PORTJbits.RJ7
#define CYCLE_R5 PORTCbits.RC2


#define DRS485_IO PORTBbits.RB4 /////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#define RIPOWER PORTHbits.RH6

#define RESETCMS PORTHbits.RH5
//////////////////////////////////////////////////
// sous routine d'interruptionADCON2

#pragma code HighVector=0x18

void HighVector(void) {
    _asm GOTO inter _endasm
}
#pragma code // return to default code section

#pragma interrupt inter

void inter(void) {
    DX1 = 0;
    INTCONbits.GIE = 0;
    if (INTCONbits.INT0IF == 1) // R�ception de donn�es sur le port B
    {
        INTCONbits.INT0IF = 0; // met le drapeau d'IT � 0
        //        mouchardINT0 = 1;
        identificationINTER('F', 'F');
        delay_ms(100);
    }

    DX1 = 1;
    //    mouchardINT0 = 0;
    DX2 = 1;

    //	INTCONbits.GIE= 1;
}

void main(void) {

    int TIMEOUTCYCLE;
    //char DATAFIX[10];

    PRIO_AUTO_IO = 0x00;
    TRISBbits.TRISB0 = 1;
    MAJBLOC = 'Y';
    TRISCbits.TRISC5 = 0; //ALIMENTATION CDE 12V IO
    DEMANDEZPLIO = 0;
    INTCONbits.GIE = 1; //AUTORISER LES INTERRUPTIONS GENERALES
    INTCONbits.INT0IE = 1; //Enable RB0/INT
    RCONbits.IPEN = 1; //Disable all unmasked peripheral interrupt
    //OPTION_REG.INTEDG = 1; //Interrupt on rising edge
    // PARAM I2C
    //TRISCbits.RC3 = 1;
    //TRISCbits.RC4 = 1;
    SSPADD = 49; //fr�quence 100KHz pour 20Mhz
    INTCON2bits.RBPU = 1; //PUPPULL
    OpenI2C(MASTER, SLEW_OFF);
    TRISCbits.TRISC4 = 1; //SDA
    TRISCbits.TRISC3 = 1; //SCL

    TRISJbits.TRISJ0 = 0; // I2C HORLOGE
    TRISJbits.TRISJ1 = 0; //I2C MEMOIRE
    TRISJbits.TRISJ2 = 0; //I2C INTERNE

    TRISJbits.TRISJ4 = 1; //CYCLE R1
    TRISJbits.TRISJ5 = 1; //CYCLE R2
    TRISJbits.TRISJ6 = 1; //CYCLE R4
    TRISJbits.TRISJ7 = 1; //CYCLE R3

    TRISCbits.TRISC2 = 1; //CYCLE R5

    TRISHbits.TRISH0 = 0;
    TRISHbits.TRISH1 = 0;

    TRISHbits.TRISH2 = 0;
    TRISHbits.TRISH3 = 0;
    TRISHbits.TRISH4 = 1;
    TRISHbits.TRISH5 = 0;
    TRISHbits.TRISH6 = 0;

    TRISEbits.TRISE1 = 0;

    TRISEbits.TRISE7 = 1;
    TRISJbits.TRISJ3 = 0;

    TRISBbits.TRISB0 = 1;
    TRISBbits.TRISB1 = 1;
    TRISBbits.TRISB2 = 1;

    TRISDbits.TRISD0 = 0;
    TRISDbits.TRISD2 = 0;
    TRISDbits.TRISD3 = 0;

    TRISFbits.TRISF3 = 0; //SORTIE DEMANDE VERS R1 .....
    TRISFbits.TRISF4 = 0; //SORTIE DEMANDE VERS R2 .....
    TRISFbits.TRISF5 = 0; //SORTIE DEMANDE VERS R3 .....
    TRISFbits.TRISF6 = 0; //SORTIE DEMANDE VERS R4 .....
    TRISFbits.TRISF7 = 0; //SORTIE DEMANDE VERS R5 .....

    //TRISCbits.RC1=0; //SORTIE AUvsCM
    TRISCbits.TRISC0 = 0; //SORTIE AUvsCM
    TRISBbits.TRISB4 = 0;
    FIRST_DEMAR = 0xFF;
    ETAT_IO = 0xFF; //LIO PEUT PARLER
    ADCON1 = 0x07; // AN0 a AN7
    ADCON2 = 0xB8; //DROITE ET TEMPS CAN
    RESETCMS = 0;
    DRS485_R1 = 0;
    Init_I2C();

    DX1 = 1;
    DX2 = 1;
    DX10 = 0;
    DX20 = 0;



    cDebutTrame = '#';
    cFinTrame = '*';

    //TMPI=0;
    //PORTCbits.RC5=1;
    //TEMPO(1);
    PORTCbits.RC5 = 0; // ON LAISSE ETEINT LA CARTE IO
    //TEMPO(1);
    //PORTCbits.RC5=1;

    MINIRESETRI = 0x00;
    UNRESETRI1 = pic_eeprom_read(0x01);
    if (UNRESETRI1 == 0xAA) //VRAI RESET heure ou a distance CONDITION NORMALE changement carte ou ......
    {
        R1NOREP = 0;
        UNRESETRI1 = 0;
        //pic_eeprom_write(0x21,0xFF); //METTRE DEBLOQUE
    }
    if (UNRESETRI1 == 0x33) //CARTE RI4 TROIS RESET
    {
        R1NOREP = 12; //ON EMPECHE L'INTERROGATION CODE 12
        pic_eeprom_write(0x21, 'd'); //METTRE BLOQUE
    } else {
        R1NOREP = 0; //ON TENTE ENCIRE UNE FOIS
    }
    //MINIRESETRI=0x00;
    if ((UNRESETRI1 >= 1)&&(UNRESETRI1 != 0x33)) //CARTE RI1 RESET une deux ou 3 fois
    {
        MINIRESETRI = pic_eeprom_read(0x21); //voir la valeur
        if ((MINIRESETRI == 0xFF) || (MINIRESETRI == 98)) //SI VIERGE METTRE INIT A ZERO
            MINIRESETRI = 0;

        MINIRESETRI++;
        pic_eeprom_write(0x21, MINIRESETRI); //UN RESET AUTO EN PLUS ENREGISTRE
        MINIRESETRI = 0xFF;
    }


    UNRESETRI2 = pic_eeprom_read(0x02);
    if (UNRESETRI2 == 0xAA) //VRAI RESET heure ou a distance CONDITION NORMALE changement carte ou ......
    {
        R2NOREP = 0;
        UNRESETRI2 = 0;
        //pic_eeprom_write(0x22,0xFF); //METTRE DEBLOQUE
    }
    if (UNRESETRI2 == 0x33) //CARTE RI4 TROIS RESET
    {
        pic_eeprom_write(0x22, 'd'); //METTRE BLOQUE
        R2NOREP = 12; //ON EMPECHE L'INTERROGATION CODE 12
    } else {
        R2NOREP = 0; //ON TENTE ENCIRE UNE FOIS
    }
    //MINIRESETRI=0x00;
    if ((UNRESETRI2 >= 1)&&(UNRESETRI2 != 0x33)) //CARTE RI1 RESET une deux ou 3 fois
    {
        MINIRESETRI = pic_eeprom_read(0x22); //voir la valeur
        if ((MINIRESETRI == 0xFF) || (MINIRESETRI == 98)) //SI VIERGE METTRE INIT A ZERO
            MINIRESETRI = 0;
        MINIRESETRI++;
        pic_eeprom_write(0x22, MINIRESETRI); //UN RESET AUTO EN PLUS ENREGISTRE
        MINIRESETRI = 0xFF;
    }


    UNRESETRI3 = pic_eeprom_read(0x03);
    if (UNRESETRI3 == 0xAA) //VRAI RESET heure ou a distance CONDITION NORMALE changement carte ou ......
    {
        R3NOREP = 0;
        UNRESETRI3 = 0;
        //pic_eeprom_write(0x23,0xFF); //METTRE DEBLOQUE
    }
    if (UNRESETRI3 == 0x33) //CARTE RI4 TROIS RESET
    {
        R3NOREP = 12; //ON EMPECHE L'INTERROGATION CODE 12
        pic_eeprom_write(0x23, 'd'); //METTRE BLOQUE
    } else {
        R3NOREP = 0; //ON TENTE ENCIRE UNE FOIS
    }
    //MINIRESETRI=0x00;
    if ((UNRESETRI3 >= 1)&&(UNRESETRI3 != 0x03)) //CARTE RI1 RESET une deux ou 3 fois ET PAS bloqu�e
    {
        MINIRESETRI = pic_eeprom_read(0x23); //voir la valeur
        if ((MINIRESETRI == 0xFF) || (MINIRESETRI == 98)) //SI VIERGE METTRE INIT A ZERO
            MINIRESETRI = 0;
        MINIRESETRI++;
        pic_eeprom_write(0x23, MINIRESETRI); //UN RESET AUTO EN PLUS ENREGISTRE
        MINIRESETRI = 0xFF;
    }


    UNRESETRI4 = pic_eeprom_read(0x04);
    if (UNRESETRI4 == 0xAA) //VRAI RESET heure ou a distance CONDITION NORMALE changement carte ou ......
    {
        //pic_eeprom_write(0x24,0xFF); //METTRE DEBLOQUE
        R4NOREP = 0;
        UNRESETRI4 = 0;
    }
    if (UNRESETRI4 == 0x33) //CARTE RI4 TROIS RESET
    {
        R4NOREP = 12; //ON EMPECHE L'INTERROGATION CODE 12
        pic_eeprom_write(0x24, 'd'); //METTRE BLOQUE
    } else {
        R4NOREP = 0; //ON TENTE ENCIRE UNE FOIS
    }
    //MINIRESETRI=0x00;
    if ((UNRESETRI4 >= 1)&&(UNRESETRI4 != 0x33)) //CARTE RI1 RESET une deux ou 3 fois
    {
        MINIRESETRI = pic_eeprom_read(0x24); //voir la valeur
        if ((MINIRESETRI == 0xFF) || (MINIRESETRI == 98)) //SI VIERGE METTRE INIT A ZERO
            MINIRESETRI = 0;
        MINIRESETRI++;
        pic_eeprom_write(0x24, MINIRESETRI); //UN RESET AUTO EN PLUS ENREGISTRE
        MINIRESETRI = 0xFF;
    }


    UNRESETRI5 = pic_eeprom_read(0x05);
    if (UNRESETRI5 == 0xAA) //VRAI RESET heure ou a distance CONDITION NORMALE changement carte ou ......
    {
        //pic_eeprom_write(0x25,0xFF); //METTRE DEBLOQUE
        R5NOREP = 0;
        UNRESETRI5 = 0;
    }
    if (UNRESETRI5 == 0x33) //CARTE RI4 TROIS RESET
    {
        R5NOREP = 12; //ON EMPECHE L'INTERROGATION CODE 12
        pic_eeprom_write(0x25, 'd'); //METTRE BLOQUE
    } else {
        R5NOREP = 0; //ON TENTE ENCIRE UNE FOIS
    }
    //MINIRESETRI=0x00;
    if ((UNRESETRI5 >= 1)&&(UNRESETRI5 != 0x33)) //CARTE RI1 RESET une deux ou 3 fois
    {
        MINIRESETRI = pic_eeprom_read(0x25); //voir la valeur
        if ((MINIRESETRI == 0xFF) || (MINIRESETRI == 98)) //SI VIERGE METTRE INIT A ZERO
            MINIRESETRI = 0;
        MINIRESETRI++;
        pic_eeprom_write(0x25, MINIRESETRI); //UN RESET AUTO EN PLUS ENREGISTRE
        MINIRESETRI = 0xFF;
    }
    DX10 = 1;
    DX20 = 1;
    //RIPOWER=1; //POUR ETEINDRE
    //TEMPO(20); //ALLUMLAGE POUR CARTE IO
    while (1) // boucle infinie
    {

        //	resetTO();


        //CMvAU=1; // L'AUTOMATE PEUT RECEVOIR
        //AUvCM=0;

        DRS485_R1 = 1;
        DRS485_R2 = 1;
        DRS485_R3 = 1;
        DRS485_R4 = 1;
        DRS485_R5 = 1;
        //        SAVAUTOCYCLE = 0;
        FCYCLE = 0;
        TESTCYCLE = 0;

        TRAMEIN[0] = cDebutTrame; //#
        TRAMEIN[1] = '0';
        TRAMEIN[2] = '0';
        TRAMEIN[3] = '9';

        DRS485_R1 = 0;
        DRS485_R2 = 0;
        DRS485_R3 = 0;
        DRS485_R4 = 0;
        DRS485_R5 = 0;

        TRAMEIN[4] = '1';
        TRAMEIN[5] = '5';
        TRAMEIN[6] = 'A';
        TRAMEIN[7] = '1';
        TRAMEIN[8] = '1';
        TRAMEIN[9] = '1';
        TRAMEIN[10] = '1';
        TRAMEIN[11] = '2';
        TRAMEIN[12] = '2';
        TRAMEIN[13] = '2';
        TRAMEIN[14] = '2';
        TRAMEIN[15] = '0';
        TRAMEIN[16] = '0';
        TRAMEIN[17] = '0';
        TRAMEIN[18] = '0';
        TRAMEIN[19] = 'B';
        TRAMEIN[20] = 'R';
        TRAMEIN[21] = 'A';
        TRAMEIN[22] = '/';
        TRAMEIN[23] = '8';
        TRAMEIN[24] = '8';
        TRAMEIN[25] = '8';
        TRAMEIN[26] = '8';
        TRAMEIN[27] = '/';
        TRAMEIN[28] = '2';
        TRAMEIN[29] = '2';
        TRAMEIN[30] = '/';
        TRAMEIN[31] = '1';
        TRAMEIN[32] = 'C';
        TRAMEIN[33] = 'O';
        TRAMEIN[34] = 'M';
        TRAMEIN[35] = 'M';
        TRAMEIN[36] = 'E';
        TRAMEIN[37] = 'N';
        TRAMEIN[38] = 'T';
        TRAMEIN[39] = 'A';
        TRAMEIN[40] = 'I';
        TRAMEIN[41] = 'R';
        TRAMEIN[42] = 'E';
        TRAMEIN[43] = 'S';
        TRAMEIN[44] = 'I';
        TRAMEIN[45] = 'C';
        TRAMEIN[46] = 'I';
        TRAMEIN[47] = 'R';
        TRAMEIN[48] = 'O';
        TRAMEIN[49] = 'B';
        TRAMEIN[50] = '9';
        TRAMEIN[51] = '9';
        TRAMEIN[52] = '3';
        TRAMEIN[53] = '2';
        TRAMEIN[54] = '6';
        TRAMEIN[55] = '1';
        TRAMEIN[56] = '1';
        TRAMEIN[57] = '1';
        TRAMEIN[58] = '5';
        TRAMEIN[59] = '2';
        TRAMEIN[60] = '5';
        TRAMEIN[61] = '0';
        TRAMEIN[62] = '0';
        TRAMEIN[63] = '1';
        TRAMEIN[64] = '0';
        TRAMEIN[65] = '2';
        TRAMEIN[66] = '0';
        TRAMEIN[67] = '0';
        TRAMEIN[68] = '0';
        TRAMEIN[69] = '0';
        TRAMEIN[70] = '4';
        TRAMEIN[71] = '5';
        TRAMEIN[72] = '6';
        TRAMEIN[73] = '7';
        TRAMEIN[74] = '8';
        TRAMEIN[75] = '0';
        TRAMEIN[76] = '1';
        TRAMEIN[77] = '0';
        TRAMEIN[78] = '8';
        TRAMEIN[79] = '6';
        TRAMEIN[80] = '5';
        TRAMEIN[81] = '0';
        TRAMEIN[82] = '8';
        TRAMEIN[83] = '8';
        TRAMEIN[84] = '8';
        TRAMEIN[85] = '2';
        TRAMEIN[86] = '2';
        TRAMEIN[87] = '7';
        TRAMEIN[88] = '6';
        TRAMEIN[89] = '7';
        TRAMEIN[90] = '*';
        TRAMEIN[91] = '-';
        TRAMEIN[92] = '-';
        TRAMEIN[93] = '-';
        TRAMEIN[94] = '-';
        TRAMEIN[95] = '-';
        TRAMEIN[96] = '-';
        TRAMEIN[97] = '-';
        TRAMEIN[98] = '-';
        TRAMEIN[99] = '-';
        TRAMEIN[100] = '-';
        TRAMEIN[101] = '-';
        TRAMEIN[102] = '-';
        TRAMEIN[103] = '-';
        TRAMEIN[104] = '-';
        TRAMEIN[105] = '-';
        TRAMEIN[106] = '-';
        TRAMEIN[107] = '-';
        TRAMEIN[108] = '-';
        TRAMEIN[109] = '-';
        TRAMEIN[110] = '-';
        TRAMEIN[111] = '-';
        TRAMEIN[112] = '-';
        TRAMEIN[113] = '-';
        TRAMEIN[114] = '-';
        TRAMEIN[115] = '-';
        TRAMEIN[116] = '-';
        TRAMEIN[117] = '-';
        TRAMEIN[118] = '-';
        TRAMEIN[119] = '-';
        TRAMEIN[120] = '-';
        TRAMEIN[121] = '-';
        TRAMEIN[122] = '-';
        TRAMEIN[123] = '-';
        TRAMEIN[124] = '-';
        TRAMEIN[125] = '-';
        //TRAMEIN[126]='-';
        //TRAMEIN[127]='-';


        //Test
        //voir_si_voie_existe();





        I2C_MEMOIRE = 0;
        I2C_HORLOGE = 0;
        I2C_INTERNE = 1; //ACTIVATION I2C INTERNE
h:

        tmp = LIRE_HORLOGE(0); //INIT HORLOGE
        //ECRIRE_HORLOGE(0x07,0x00); //REGLAGE control la valeur ne sert a rien
        creer_trame_horloge(); // ON LIT LA DATE DANS L'HORLOGE

        PORTJbits.RJ3 = 1; // LES DEUX POUR ETAT CHARGE
        STATO = 1;

        PORTJbits.RJ3 = 0;
        STATO = 0;

        Init_RS485();

        SERIALISATION();

        I2C_MEMOIRE = 0; //AUTORISER ACCES MEMOIRE INTERNE
        I2C_HORLOGE = 0; //INTERDIRE ACCES HORLOGE INTERNE
        I2C_INTERNE = 1; //INTERDIRE ACCES I2C EXTERNE
        TMPI = 0;
        INTCONbits.GIE = 1; //AUTORISER LES INTERRUPTIONS GENERALES

j:
        tmp = LIRE_HORLOGE(0);
        tmp = ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEIN), calcul_addmemoire(&TRAMEIN), &TRAMEIN, 1); //ECRITURE EN EEPROM
        tmp = LIRE_EEPROM(choix_memoire(&TRAMEIN), calcul_addmemoire(&TRAMEIN), &TRAMEOUT, 1); //LIRE DANS EEPROM
        delay_us(1);

        creer_trame_horloge(); //ON LIT L'HEURE DE CM

        //       SAVAUTOCYCLE = 8;
L:

        //        SAVAUTOCYCLE = SAVAUTOCYCLE + 1;


        cycleN = 1; //CYCLE 1 LES DIC ET LES TEC

        PRIO_AUTO_IO = 0x00; //REPOS NI L'AUTO NI LIO EST DANS UNE FOCNTION

        MESURE_DES_TENSIONS();
        CONTROLE_CHARGE_BATT(); //GESTION DE LA CHARGE BATTERIE

        delay_ms(1);
        PORTDbits.RD7 = 1;
        PORTDbits.RD7 = 0; // Enable Chip Select ANCIENNEMENT CARTE SD
        tmp = 0;
hi:

        LIRE_EEPROM(0, TMPI, &TRAMEOUT, 1); //LIRE DANS EEPROM

        delay_ms(4); //indispensable entre erciture et lecture
        TRAMEIN[5] = TRAMEIN[5] + 1;
        if (TRAMEIN[5] == ':') {
            TRAMEIN[5] = '0';
            TRAMEIN[4] = '1';
        }
        if (TRAMEIN[4] == '1') {
            if (TRAMEIN[5] == '7') {
                TRAMEIN[5] = '0';
                TRAMEIN[4] = '0';
                TMPI = TMPI + 1;
                IntToChar(TMPI, PAGE, 3);
                TRAMEIN[1] = PAGE[0];
                TRAMEIN[2] = PAGE[1];
                TRAMEIN[3] = PAGE[2];


            }
        }

        resetTO();
        DRS485_IO = 0; //REPOS DE LA DEMANDE DE SESSION DE L'AUTOMATE POUR DEMANDER A LA CARTE IO LA PAIX

        TEMPO(3); // pour 30 on a 33s donc 100ms delay_ms enriron 110ms
LH:
        MESURE_DES_TENSIONS();
        CONTROLE_CHARGE_BATT();
        creer_trame_horloge(); //ON LIT L'HEURE DE CM

        reglerhorlogeautres(); //20012020
        //goto LH;

        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI PAS PREMIER DEMAR et IO(RB4->1) est AUTORISEE A MARCHER ON LA DESACTIVE
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }

        if (MINIRESETRI == 0xFF)
            goto cy2; // ON VA DIRECTEMENT A LA CARTe IO CAR MINI RESET AUTO
        //------------------------------------------------------------------------------TABLE EXISTENCE RI1

        if ((VDD1 > 2)&&(R1NOREP <= 4)) //SI REPONSE  DE LA CARTE RI1(apres plusieurs tentatives) sinon ne l'interroge plus
        {
            TEMPO(2);
            ENVOIE_DES_TRAMES(1, 304, &INSTRUCTION); //EXISTENCE COMPTER 4Secondes PAR CARTE
            //TEMPO(20);								//DEMANDE INFOS CAPTEUR A LA CARTE RELAIS 1 : ici 1mn environ

        }




        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI PAS PREMIER DEMAR et IO(RB4->1) est AUTORISEE A MARCHER ON LA DESACTIVE
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }






        MESURE_DES_TENSIONS();
        CONTROLE_CHARGE_BATT();

        if ((R1NOREP >= 4)&&(R1NOREP < 12)) //R1 NA PAS REPONDU 4X pour la fonction TABLE EXISTENCE!!!!
        {
            if (UNRESETRI1 >= 2) //SI une resetRI a deja ete fait
            {
                R1NOREP = 12; //ON met cette valeur a 12 comme cela la carte n'est plus interrog�e
                pic_eeprom_write(0x01, 0x33); //DEUX RESET
            } else //si aucun resetRI n'a ete fait on fait un reset donc le RESETri EST FAIT UNE SEUL FOIS
            {
                UNRESETRI1++; //ON MEMORISE LE RESET 1 APRES ELLE EST ZAPPEE mettre dans emplacement eeprom celui ci sera a zero seulement avec un vrai reset
                delay_ms(300);
                //TEMPO(1);
                //RIPOWER=0; //RESET RI
                pic_eeprom_write(0x01, UNRESETRI1); //UN RESET							//METTRE INDICATION DANS EEPROOM QU'IL SAGIT DUN RESET RI OU UTILISER RESETCMS !!!!!!!!
                RESETCMS = 1; //ON REINIT
            }
        }



        //------------------------------------------------------------------------------TABLE EXISTENCE RI2

        if ((VDD2 > 2)&&(R2NOREP <= 4)) {
            TEMPO(3);
            ENVOIE_DES_TRAMES(2, 304, &INSTRUCTION); //EXISTENCE COMPTER 4Secondes PAR CARTE

        }

        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI PAS PREMIER DEMAR et IO(RB4->1) est AUTORISEE A MARCHER ON LA DESACTIVE
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }
        MESURE_DES_TENSIONS();
        CONTROLE_CHARGE_BATT();


        if ((R2NOREP >= 4)&&(R2NOREP < 12)) {

            if (UNRESETRI2 >= 2) //SI DEUXIEME FOIS REPOND PAS APRES RESET ON LINTERDIT
            {
                R2NOREP = 12;
                pic_eeprom_write(0x02, 0x33); //TROISIEME RESET
            } else {
                UNRESETRI2++; //ON MEMORISE LE RESET 1 APRES ELLE EST ZAPPEE
                delay_ms(300);
                //TEMPO(1);
                pic_eeprom_write(0x02, UNRESETRI2); //UN RESET puis deux jusqu'a condition plus haute  ici 3
                RESETCMS = 1; //RESET RI;
            }
        }


        //------------------------------------------------------------------------------TABLE EXISTENCE RI3

        if ((VDD3 > 2)&&(R3NOREP <= 4)) {
            TEMPO(3);
            ENVOIE_DES_TRAMES(3, 304, &INSTRUCTION); //EXISTENCE COMPTER 4Secondes PAR CARTE
            //TEMPO(20);								//DEMANDE INFOS CAPTEUR A LA CARTE RELAIS 1 : ici 1mn environ

        }

        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI PAS PREMIER DEMAR et IO(RB4->1) est AUTORISEE A MARCHER ON LA DESACTIVE
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }


        MESURE_DES_TENSIONS();
        CONTROLE_CHARGE_BATT();



        if ((R3NOREP >= 4)&&(R3NOREP < 12)) {

            if (UNRESETRI3 >= 2) //SI DEUXIEME FOIS REPOND PAS APRES RESET ON LINTERDIT
            {
                R3NOREP = 12;
                pic_eeprom_write(0x03, 0x33); //TROISIEME RESET
            } else {
                UNRESETRI3++; //ON MEMORISE LE RESET 1 APRES ELLE EST ZAPPEE
                delay_ms(300);
                //TEMPO(1);
                pic_eeprom_write(0x03, UNRESETRI3); //UN RESET puis deux jusqu'a condition plus haute  ici 3
                RESETCMS = 1; //RESET RI
            }
        }



        //------------------------------------------------------------------------------TABLE EXISTENCE RI4



        if ((VDD4 > 2)&&(R4NOREP <= 4)) {
            TEMPO(3);
            ENVOIE_DES_TRAMES(4, 304, &INSTRUCTION); //EXISTENCE COMPTER 4Secondes PAR CARTE

        }



        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI PAS PREMIER DEMAR et IO(RB4->1) est AUTORISEE A MARCHER ON LA DESACTIVE
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }








        MESURE_DES_TENSIONS();
        CONTROLE_CHARGE_BATT();


        if ((R4NOREP >= 4)&&(R4NOREP < 12)) {

            if (UNRESETRI4 >= 2) //SI TROISIEME FOIS REPOND PAS APRES RESET ON LINTERDIT
            {
                R4NOREP = 12;
                pic_eeprom_write(0x04, 0x33); //TROISIEME RESET
            } else {
                UNRESETRI4++; //ON MEMORISE LE RESET 1 APRES ELLE EST ZAPPEE
                delay_ms(300);
                pic_eeprom_write(0x04, UNRESETRI4); //UN RESET puis deux jusqu'a condition plus haute  ici 3
                RESETCMS = 1; //RESET RI
            }
        }




        //------------------------------------------------------------------------------TABLE EXISTENCE RI5

        if ((VDD5 > 2)&&(R5NOREP <= 4)) {
            TEMPO(3);
            ENVOIE_DES_TRAMES(5, 304, &INSTRUCTION); //EXISTENCE COMPTER 4Secondes PAR CARTE

        }



        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI PAS PREMIER DEMAR et IO(RB4->1) est AUTORISEE A MARCHER ON LA DESACTIVE
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }

diccc:


        if ((R5NOREP >= 4)&&(R5NOREP < 12)) {

            if (UNRESETRI5 >= 2) //SI DEUXIEME FOIS REPOND PAS APRES RESET ON LINTERDIT
            {
                R5NOREP = 12;
                pic_eeprom_write(0x05, 0x33); //TROISIEME RESET
            } else {
                UNRESETRI5++; //ON MEMORISE LE RESET 1 APRES ELLE EST ZAPPEE

                delay_ms(300);
                //TEMPO(1);
                pic_eeprom_write(0x05, UNRESETRI5); //UN RESET puis deux jusqu'a condition plus haute  ici 3
                RESETCMS = 1; //RESET RI
            }
        }

        //------------------------------------------------------------------------------DONNEES RI1
        DATAFIX[0] = '0';
        DATAFIX[1] = '0';
        DATAFIX[2] = '1';
        DATAFIX[3] = '0';
        DATAFIX[4] = '0';

        DATAFIX[5] = '0';
        DATAFIX[6] = '2';
        DATAFIX[7] = '0';
        DATAFIX[8] = '1';
        DATAFIX[9] = '6'; // il faut aller jusqua cela sinon 16 non incl

        MODERAPID = 1; // / ou -

        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI PAS PREMIER DEMAR et IO(RB4->1) est AUTORISEE A MARCHER ON LA DESACTIVE
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }

        MODERAPID = 1; // / ou -
        MESURE_DES_TENSIONS();
        CONTROLE_CHARGE_BATT();
        //R1NOREP=0;
        if ((VDD1 > 2)&&(R1NOREP <= 4)) {
            TEMPO(1);
            ENVOIE_DES_TRAMES(1, 276, &DATAFIX); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE
            PRIO_AUTO_IO = 0x00; //!!!!!!!!!!!


        }
        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI PAS PREMIER DEMAR et IO(RB4->1) est AUTORISEE A MARCHER ON LA DESACTIVE
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }





        //------------------------------------------------------------------------------DONNEES RI2
        MODERAPID = 1; // / ou -
        MESURE_DES_TENSIONS();
        CONTROLE_CHARGE_BATT();
        //R2NOREP=0;



        if ((VDD2 > 2)&&(R2NOREP <= 4)) {
            TEMPO(1);
            ENVOIE_DES_TRAMES(2, 276, &DATAFIX); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE
            PRIO_AUTO_IO = 0x00;


        }

        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI IO EST ACTIVE (REA ON)
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }




        //------------------------------------------------------------------------------DONNEES RI3

        MODERAPID = 1; // / ou -
        MESURE_DES_TENSIONS();
        CONTROLE_CHARGE_BATT();
        if ((VDD3 > 2)&&(R3NOREP <= 4)) {
            TEMPO(1);
test1:
            ENVOIE_DES_TRAMES(3, 276, &DATAFIX); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE

            PRIO_AUTO_IO = 0x00;
        }

        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI IO EST ACTIVE (REA ON)
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }



        //------------------------------------------------------------------------------DONNEES RI4

        MODERAPID = 1; // / ou -
        MESURE_DES_TENSIONS();
        CONTROLE_CHARGE_BATT();
        if ((VDD4 > 2)&&(R4NOREP <= 4)) {
            TEMPO(1);
            ENVOIE_DES_TRAMES(4, 276, &DATAFIX); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE

            PRIO_AUTO_IO = 0x00;

        }


        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI IO EST ACTIVE (REA ON)
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }




        //------------------------------------------------------------------------------DONNEES RI5
        MODERAPID = 1; // / ou -
        MESURE_DES_TENSIONS();
        CONTROLE_CHARGE_BATT();
        if ((VDD5 > 2)&&(R5NOREP <= 4)) {
            TEMPO(1);
test2:
            ENVOIE_DES_TRAMES(5, 276, &DATAFIX); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE

            PRIO_AUTO_IO = 0x00;
        }





fintest:
        TEMPO(2);


cy2:

        cycleN = 2; //DEUXIEME CYCLE ZIZ ET ZTE ET ZAG ET DEMANDE DE L'IO



        if (IO_EN_COM == 0) // LA CARTE IO NE DIALOGUE PAS AVEC LE MODEM
        {
            if (FIRST_DEMAR == 0XFF) //SI PREMIER DEMMARAGE ON INIT LIO
            {
                PORTCbits.RC5 = 1;
                delay_ms(100);
                PORTCbits.RC5 = 0;
                delay_ms(100);
                PORTCbits.RC5 = 1;
                IOTRAVAIL = 1;
WAITING1:

                PASDE_S = 0;
                if ((IOTRAVAIL == 1)&&(PASDE_S < 5)) {

                    IOTRAVAIL = 0;
                    if (MINIRESETRI != 0xFF) {
                        TEMPO(15); //INIT PAS LES DONNEES
                    } else {
                        TEMPO(15);
                    }
                    PASDE_S++;
                    if ((DEMANDEZPLIO < 2)&&(PASDE_S < 5))
                        goto WAITING1;
                }



                if (DEMANDEZPLIO == 0x02) //FIN CYCLE 2 PARAM IO2iemex DEM
                    DEMANDEZPLIO = 0x00;

                if (ETAT_IO == 0xFF) //SI IO EST ACTIVE (REA ON)
                {
                    TEMPO(1);
                    DRS485_IO = 1; //ON DESACTIVE POUR DEMANDER INFO une deuxieme fois
                    TEMPO(1);
                    DRS485_IO = 0;
                    ETAT_IO = 0x00;
                }
                FIRST_DEMAR = 0x00;
                goto fintest2;
            } else {
                if (ETAT_IO == 0xFF) //SI IO EST ACTIVE (REA ON)
                {
                    TEMPO(1);
                    DRS485_IO = 1; //ON DESACTIVE
                    TEMPO(1);
                    DRS485_IO = 0;
                    ETAT_IO = 0x00;
                    goto finio;
                }
                if (ETAT_IO == 0x00) //SI IO EST DESACTIVE (REA oFF)
                {
                    TEMPO(1);
                    DRS485_IO = 1; //ON ACTIVE POUR DEMANDER INFO
                    TEMPO(1);
                    DRS485_IO = 0;
                    ETAT_IO = 0xFF;

                    //ON ATTEND UNE SCRUTATION COMPLETE OU LE TEMPS DES ZIZ
                    //ON ATTEND UNE SCRUTATION COMPLETE OU LE TEMPS DES ZIZ

                    PASDE_S = 0;
                    IOTRAVAIL = 1;

WAITING2:

                    if ((IOTRAVAIL == 1)&&(PASDE_S < 10)) //ON ATTEND 10x la tempo
                    {
                        IOTRAVAIL = 0;
                        TEMPO(15); //TRES IMPORTANT IL NE FAUT PAS QUE IO DEMANDE UN TRUC A VANT
                        PASDE_S++;
                        if ((DEMANDEZIGIO == 0x00)&&(PASDE_S < 10))
                            goto WAITING2;
                    }
                    TRAMEOUT[3] = 'A'; //
                    TRAMEOUT[4] = 'U';
                    TRAMEOUT[5] = 'I';
                    TRAMEOUT[6] = 'N';
                    TRAMEOUT[7] = 'I';
                    TRAMEOUT[8] = 'T';
                    TRAMEOUT[9] = '*';
                    TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE
                    TEMPO(3);
                    if (DEMANDEZIGIO == 0xFF) //FIN CYCLE 2 GROUPE
                        DEMANDEZIGIO = 0x00;





                    if (ETAT_IO == 0xFF) //SI IO EST ACTIVE (REA ON)
                    {
                        DRS485_IO = 1; //ON DESACTIVE
                        TEMPO(1);
                        DRS485_IO = 0;
                        ETAT_IO = 0x00;
                    }
                    goto fintest2;


                }


            }

        } else {
            TEMPO(1);
        }




finio:
        TEMPO(1); //MINIMUM 32S temps une carte RI (30=33S)


        MESURE_DES_TENSIONS();
        CONTROLE_CHARGE_BATT();

fintest2:
        if ((ETAT_IO == 0xFF)&&(FIRST_DEMAR != 0XFF)) //SI PAS PREMIER DEMAR et IO(RB4->1) est AUTORISEE A MARCHER ON LA DESACTIVE
        {
            TEMPO(1);
            DRS485_IO = 1; //ON DESACTIVE
            TEMPO(1);
            DRS485_IO = 0;
            ETAT_IO = 0x00;

        }

        if ((VDD1 > 2)&&(R1NOREP <= 4))
            TESTCYCLE = TESTCYCLE + 1;
        if ((VDD2 > 2)&&(R2NOREP <= 4))
            TESTCYCLE = TESTCYCLE + 2;
        if ((VDD3 > 2)&&(R3NOREP <= 4))
            TESTCYCLE = TESTCYCLE + 4;
        if ((VDD4 > 2)&&(R4NOREP <= 4))
            TESTCYCLE = TESTCYCLE + 8;
        if ((VDD5 > 2)&&(R5NOREP <= 4))
            TESTCYCLE = TESTCYCLE + 16;
        TIMEOUTCYCLE = 0; //SI PROBLEME
fintest3:

        FCYCLE = 0;

        cycleN = 1;
        TEMPO(5); //MAX 70*x=350
        TIMEOUTCYCLE++;
        if ((VDD1 > 2)&&(R1NOREP <= 4)) {

            if (CYCLE_R1 == 1) {
                FCYCLE = 1 + FCYCLE;

            }
        }
        if ((VDD2 > 2)&&(R2NOREP <= 4)) {

            if (CYCLE_R2 == 1) {
                FCYCLE = 2 + FCYCLE;

            }
        }
        if ((VDD3 > 2)&&(R3NOREP <= 4)) {
            if (CYCLE_R3 == 1) {
                FCYCLE = FCYCLE + 4;
            }
        }
        if ((VDD4 > 2)&&(R4NOREP <= 4)) {
            if (CYCLE_R4 == 1) {
                FCYCLE = FCYCLE + 8;
            }
        }
        if ((VDD5 > 2)&&(R5NOREP <= 4)) {
            if (CYCLE_R5 == 1) {
                FCYCLE = FCYCLE + 16;
            }
        }

        if (MINIRESETRI == 0xFF) //MINI RESET AUTO A ETE FAIT
        {
            MINIRESETRI = 0;
            goto continuercycle;
        }
        if (TIMEOUTCYCLE >= 60) //TIMEOUT CYCLE
            goto continuercycle;
        if (FCYCLE != TESTCYCLE) //TOUTES LES CARTES NONT PAS TERMINEES
            goto fintest3;

continuercycle:

        delay_ms(10);
        FCYCLE = 0;
        TESTCYCLE = 0;
        goto L;
    }


}




//////////////////////////////////////////////////////////////////
// fonction permettant d'initialiser le port s�rie				//
//////////////////////////////////////////////////////////////////

void Init_RS485(void) {

    unsigned int SPEED;

    TRISCbits.TRISC5 = 0;
    TRISCbits.TRISC7 = 1;
    TRISCbits.TRISC6 = 0;
    TRISCbits.TRISC5 = 0; //RC5 en sortie

    TRISAbits.TRISA4 = 0; //RA4 RX_TX en sortie OU DERE



    RCSTA1bits.SPEN = 1; // Enable serial port
    TXSTA1bits.SYNC = 0; // Async mode
    BAUDCON1bits.BRG16 = 1;

    TXSTA1bits.BRGH = 1; //haute vitesse
    SPEED = 624; // 624 set 9600 bauds 311 pour 19230bauds
    SPBRG1 = SPEED; // Write baudrate to SPBRG1
    SPBRGH1 = SPEED >> 8; // For 16-bit baud rate generation


    IPR1bits.RCIP = 1; // Set high priority
    PIR1bits.RCIF = 0; // met le drapeau d'IT � 0 (plus d'IT)
    PIE1bits.RCIE = 0; // ne g�n�re pas d'IT pour la liaison s�rie

    TXSTA1bits.TXEN = 0; // transmission inhib�e
    RCSTA1bits.RX9 = 0; // r�ception sur 8 bits
    TXSTA1bits.TX9 = 0; // transmission sur 8 bits
    RCSTA1bits.CREN = 0; // interdire reception
    RCONbits.IPEN = 1; // Enable priority levels

}





//////////////////////////////////////////////////////////////////
// reception d'un caract�re sur le port s�rie		//
//////////////////////////////////////////////////////////////////

char RECEVOIRST_RS485(void) {
    unsigned char Carac;
    unsigned int TEMPS1, TEMPS2;
    DERE = 0; //AUTORISATION DE RECEVOIR
    RCSTAbits.CREN = 1;

    while ((PIR1bits.RCIF == 0)) // max 6s pour 10
    {
    }; // attend une r�ception de caract�re sur RS232
    //    __no_operation();

    Carac = RCREG1; // caract�re re�u
    PIR1bits.RCIF = 0;
    RCSTAbits.CREN = 0;
    return (Carac); // retourne le caract�re re�u sur la liaison RS232

}


//////////////////////////////////////////////////////////////////
// reception d'un caract�re sur le port s�rie	avec tempo max	//
//////////////////////////////////////////////////////////////////

char RECEVOIR_RS485(unsigned char ttout) {
    unsigned char Carac;
    unsigned int TEMPS1, TEMPS2;
    DERE = 0; //AUTORISATION DE RECEVOIR
    RCSTAbits.CREN = 1;

    TEMPS1 = 0;
    TEMPS2 = 0;
    NORECEPTION = 0x00;
RS485R:

    DX1 = !DX1;
    while ((PIR1bits.RCIF == 0)&&(TEMPS2 <= (ttout + 1))) // max 6s pour 10
    {

        TEMPS1 = TEMPS1 + 1;
        if (TEMPS1 >= 50000) //ENVIRON 500ms
        {
            TEMPS2++;
            TEMPS1 = 0;
        }

    }; // attend une r�ception de caract�re sur RS232

    Carac = RCREG1; // caract�re re�u
    if (Carac == 0x04) // EOT recu par les cartes resitives qd elle bloque
        RESETCMS = 1; //ALORS ON RESET LE CMS

    if (TEMPS2 >= ttout)
        NORECEPTION = 0xff;



    PIR1bits.RCIF = 0;
    RCSTAbits.CREN = 0;
    return (Carac); // retourne le caract�re re�u sur la liaison RS232

}


//////////////////////////////////////////////////////////////////
// �mission d'un caract�re sur le port s�rie					//
//////////////////////////////////////////////////////////////////

void RS485(unsigned char carac) {
    //carac = code(carac);


    DERE = 1; //AUTORISATION TRANSMETTRE
    DX2 = !DX2;

    TXSTA1bits.TXEN = 0;

    RCSTA1bits.CREN = 0; // interdire reception

    TXSTA1bits.TXEN = 1; // autoriser transmission


    TXREG1 = carac;

    while (TXSTA1bits.TRMT == 0); // attend la fin de l'�mission
    //     __no_operation();
    TXSTA1bits.TXEN = 0;

    DERE = 0; //AUTORISATION TRANSMETTRE
}





//////////////////////////////////////////////////////////////////
// RECEPTION D'UNE TRAME COMPLETE RS485          		   		//
//////////////////////////////////////////////////////////////////

//exemple #R1CMDIC03300R111122223333BRA/EEEE/22/1COMMENTAIRESICIROB0332611152500102000045678110865088822222*

// STOCKAGE DU RESULTAT DANS LA TRAMEIN[]

void TRAMERECEPTIONRS485DONNEES(unsigned char tout) {
    unsigned char u;
    unsigned char tmp;


    tmp = 0;

    do {
ret1:
        u = RECEVOIR_RS485(tout);
        //if ((u!='=') && (tmp==0))
        //goto ret1;

        TRAMEIN[tmp] = u;
        tmp = tmp + 1;


    } while ((u != '*') && (tmp < 200) && (NORECEPTION != 0xff) && (u != '$')); //'$' c la fin

    delay_ms(10); //attention a ne pas enlever

}


//////////////////////////////////////////////////////////////////
// ENVOI D'UNE TRAME COMPLETE RS485          		   	        //
//////////////////////////////////////////////////////////////////

//exemple #CMR1DIC03300R111122223333BRA/EEEE/22/1COMMENTAIRESICIROB0332611152500102000045678110865088822222*

// STOCKAGE DU RESULTAT DANS LA TRAMEOUT[]

void TRAMEENVOIRS485DONNEES(void) {
    unsigned char u;
    unsigned char tmp;
    tmp = 0;

    do {
ret1:

        u = TRAMEOUT[tmp];
        if (u == 0xFF) // ON INTERDIT L'ENVOIE DE 0xFF �<NUL>235
            u = 'x';
        RS485(u);
        tmp = tmp + 1;
    } while ((u != '*') && (tmp < 200));
}






//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UNIQUEMENT POUR CARTE MERE ET A LA DEMANDE DE LA CARTE MERE (LORS d'une PREMIERE TRAME CM-->Rx  et  erreurs)  //
//ECRIRE 1 pour carte R1 ,2 pour R2 etc..................
//puis le numero de la fonction exemple

// pour DIC=68*2+73+67=276 (avec code decimal des carac ascii) DEMANDE INFOS CAPTEUR A LA CARTE RELAIS	//
//puis le tableau contenant les donn�es a envoyer suivant le code de la fonction																								                //
// le resultat est stock� dans la TRAMEOUT[]
//////////////////////////////////////////////////////////////////////////////////////////////////////

void ENVOIE_DES_TRAMES(char R, int fonctionRS, char *tab) {

    char TYPEDECARTEOEIL; //D�claration dans ENVOIE_DES_TRAMES car seulement utilis� l�
    unsigned char cFonctionEnvoie[3]; //Remplacement du tableau global FONCTION par le tableau cFonctionEnvoie
    int t;
    unsigned int funct;
    int y;
    char u;
    float h;


    ID1[0] = 'C';
    ID1[1] = 'M';


    switch (R) //analyse de la fonction
    {

        case 1: // R1
            ID2[0] = 'R';
            ID2[1] = '1';
            break;
        case 2: // R2
            ID2[0] = 'R';
            ID2[1] = '2';
            break;
        case 3: // R3
            ID2[0] = 'R';
            ID2[1] = '3';
            break;
        case 4: // R4
            ID2[0] = 'R';
            ID2[1] = '4';
            break;
        case 5: // R5
            ID2[0] = 'R';
            ID2[1] = '5';
            break;

        case 6: // R5
            ID2[0] = 'A';
            ID2[1] = 'U';
            break;

        case 7: // IO
            ID2[0] = 'I';
            ID2[1] = 'O';
            break;

        default:


            break;

    }



    switch (fonctionRS) {


        case 301:

            funct = 1;
forma:
            INTCONbits.GIE = 0; //ON INTERDIT TT
            cFonctionEnvoie[0] = 'F';
            cFonctionEnvoie[1] = 'O';
            cFonctionEnvoie[2] = 'R';
            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = cFonctionEnvoie[0];
            TRAMEOUT[6] = cFonctionEnvoie[1];
            TRAMEOUT[7] = cFonctionEnvoie[2];
            pointeur_out_in = 8;
            calcul_CRC(); //A COMPLETER
            TRAMEOUT[pointeur_out_in + 5] = cFinTrame;
            funct = 0;
            AQUIENVOYER(&ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
            delay_ms(100); //attend un peu avant d'envoyer
            TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE

            TRAMERECEPTIONRS485DONNEES(25); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE


            controle_CRC();
            REPOSAQUIENVOYER(&ID2);
            if (NORECEPTION == 0xFF) {
                TEMPO(1);
                funct++;
                if (funct < 3)
                    goto forma;
            }
            break;


        case 291: //ECRIRE SUR CARTE SD suivant une demande -> ENVOIE_DES_TRAMES (6,291,&INSTRUCTION);

            INTCONbits.GIE = 0; //ON INTERDIT TT
            cFonctionEnvoie[0] = 'D';
            cFonctionEnvoie[1] = 'D';
            cFonctionEnvoie[2] = 'W';
            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = cFonctionEnvoie[0];
            TRAMEOUT[6] = cFonctionEnvoie[1];
            TRAMEOUT[7] = cFonctionEnvoie[2];
            DX10 = 0;
            DX20 = 0;
            if (tab[0] == 'D') //DEMARRAGE FICHIER DEM
            {
                SDW('D'); //PROCEDURE DECRITURE
                INTCONbits.GIE = 1;
            }
            if (tab[0] == 'C') //CYCLIQUE	AUTOMATE OU INTERNE		FICHIER SAV
                SDW('C');
            if (tab[0] == 'M') //CYCLIQUE	MINITEL	ou	FICHIER SAV
            {
                INTCONbits.GIE = 0;
                SDW('M');
                INTCONbits.GIE = 1;
            }
            if (tab[0] == 'N') //CYCLIQUE	MINITEL	ou	FICHIER SAV
            {
                INTCONbits.GIE = 0;
                SDW('N');
                INTCONbits.GIE = 1;
            }




            break;







        case 333:



            //	INTCONbits.GIE= 1;

            cFonctionEnvoie[0] = 'Z';
            cFonctionEnvoie[1] = 'T';
            cFonctionEnvoie[2] = 'E';
            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = cFonctionEnvoie[0];
            TRAMEOUT[6] = cFonctionEnvoie[1];
            TRAMEOUT[7] = cFonctionEnvoie[2];



            h = TMP[1] / 10;
            u = h;
            h = TMP[1] - 10 * u;

            TRAMEOUT[8] = u + 48;


            u = h;

            TRAMEOUT[9] = u + 48;



            for (y = 0; y < 23; y++) {
                TRAMEOUT[10 + y] = TMP[y + 2];
            }


            pointeur_out_in = 31;
            calcul_CRC(); //A COMPLETER
            TRAMEOUT[pointeur_out_in + 5] = cFinTrame;

            TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE
            funct = 0;

            break;





        case 302: // ERREUR


            cFonctionEnvoie[0] = 'E';
            cFonctionEnvoie[1] = 'R';
            cFonctionEnvoie[2] = 'R';
            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = cFonctionEnvoie[0];
            TRAMEOUT[6] = cFonctionEnvoie[1];
            TRAMEOUT[7] = cFonctionEnvoie[2];

            for (t = 8; t < 28; t++) {
                TRAMEOUT[t] = tab[t - 8];
            }

            pointeur_out_in = 28;
            calcul_CRC(); //A COMPLETER
            TRAMEOUT[pointeur_out_in + 5] = cFinTrame;
            AQUIENVOYER(&ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
            TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE
            REPOSAQUIENVOYER(&ID2);
            funct = 0;

            break;

        case 276: //OK DIC

            INTCONbits.GIE = 1; //AUTORISER LES INTERRUPTIONS GENERALES

            TROPDENONREPONSE = 0x00;
            PRIO_AUTO_IO = 0xDD; //ON EST EN MODE INTERROGATION

            cFonctionEnvoie[0] = 'D';
            cFonctionEnvoie[1] = 'I';
            cFonctionEnvoie[2] = 'C';

            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = cFonctionEnvoie[0];
            TRAMEOUT[6] = cFonctionEnvoie[1];
            TRAMEOUT[7] = cFonctionEnvoie[2];
            TRAMEOUT[8] = tab[0];
            TRAMEOUT[9] = tab[1];
            TRAMEOUT[10] = tab[2];
            TRAMEOUT[11] = tab[3];
            TRAMEOUT[12] = tab[4];
            if (MODERAPID == 0)
                TRAMEOUT[13] = '-';
            if (MODERAPID == 1)
                TRAMEOUT[13] = '/';
            TRAMEOUT[14] = tab[5];
            TRAMEOUT[15] = tab[6];
            TRAMEOUT[16] = tab[7];
            TRAMEOUT[17] = tab[8];
            TRAMEOUT[18] = tab[9];



            //	INTCONbits.GIE= 0;

            pointeur_out_in = 19; // SAUVEGARDE POINTEUR DU TABLEAU
            calcul_CRC(); // CALCUL LE CRC ET LE RAJOUTE AU TABLEAU TRAMEOUT[]
            TRAMEOUT[pointeur_out_in + 5] = cFinTrame; // RAJOUT LE BYTE DE STOP


            tmp2 = 0; //SYNCHRONISATION ON ATTEND "%" debut supertrame
            tmp = 0;
rep1:
            if (tmp2 <= 4) //4 ESSAIS
            {
                AQUIENVOYER(&ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
                delay_ms(100); //attend un peu avant d'envoyer
                TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE
                REPOSAQUIENVOYER(&ID2);
                tmp = RECEVOIR_RS485(9); // ON ATTEND '%' //30082017


                if ((PRIO_AUTO_IO == 0xD1) || (PRIO_AUTO_IO == 0xD2)) // ON ARRETE ET ON PASSE EN MODE NORMAL
                {
                    //PRIO_AUTO_IO=0x00;
                    TRAMERECEPTIONRS485DONNEES(9); // SE ATTEND UNE RECEPTION AVANT D'ARRETER ATTENTION SI LA CARTE RI % PUIS 00100 trop rapide le # n'est pas dedans


                    controle_CRC();

                    INFORMATION_ERREUR[0] = 'D'; // ENVOYER A L'AUTO ATTENTION JE SUIS EN MODE DEMANDE RI
                    INFORMATION_ERREUR[1] = 'D';
                    for (t = 2; t < 20; t++) {
                        INFORMATION_ERREUR[t] = 'x';
                    }

                    if (PRIO_AUTO_IO == 0xD1) {
                    }
                    //ICI 1404								ENVOIE_DES_TRAMES (6,302,&INFORMATION_ERREUR); // ENVOYER A L'AUTO
                    if (PRIO_AUTO_IO == 0xD2)
                        ENVOIE_DES_TRAMES(7, 302, &INFORMATION_ERREUR); // ENVOYER A L'IO

                    //ICI 1404					PRIO_AUTO_IO=0x00;
                    //LA CARTE RI RECOIT ERRDD' pas ERR00
                    if (PRIO_AUTO_IO == 0xD2) //ICI 1404
                    {
                        TRAMERECEPTIONRS485DONNEES(9); // SE ATTEND UNE RECEPTION AVANT D'ARRETER //ICI 1404	//ICI 1404 IO
                        controle_CRC(); //ICI 1404
                    }
                    //ICI 1404						TRAMERECEPTIONRS485DONNEES (9);  // SE ATTEND UNE RECEPTION AVANT D'ARRETER
                    //	CRC_verif();
                    //ICI 1404					controle_CRC();

                    INFORMATION_ERREUR[0] = 'A'; // ON ENVOIE UNE TRAME D'ARRET
                    INFORMATION_ERREUR[1] = 'A';
                    for (t = 2; t < 20; t++) {
                        INFORMATION_ERREUR[t] = 'x';
                    }
                    if (TRAMEIN[2] == '1')
                        ENVOIE_DES_TRAMES(1, 302, &INFORMATION_ERREUR); // ON ENVOIE A CARTE 1 ICI voir les autres suivant demande
                    if (TRAMEIN[2] == '2')
                        ENVOIE_DES_TRAMES(2, 302, &INFORMATION_ERREUR); // ON ENVOIE A CARTE 2 ICI voir les autres suivant demande
                    if (TRAMEIN[2] == '3')
                        ENVOIE_DES_TRAMES(3, 302, &INFORMATION_ERREUR); // ON ENVOIE A CARTE 3 ICI voir les autres suivant demande
                    if (TRAMEIN[2] == '4')
                        ENVOIE_DES_TRAMES(4, 302, &INFORMATION_ERREUR); // ON ENVOIE A CARTE 4 ICI voir les autres suivant demande
                    if (TRAMEIN[2] == '5')
                        ENVOIE_DES_TRAMES(5, 302, &INFORMATION_ERREUR); // ON ENVOIE A CARTE 5 ICI voir les autres suivant demande
                    if ((TRAMEIN[2] != '5')&&(TRAMEIN[2] != '4')&&(TRAMEIN[2] != '3')&&(TRAMEIN[2] != '2')&&(TRAMEIN[2] != '1'))
                        ENVOIE_DES_TRAMES(1, 302, &INFORMATION_ERREUR); // securit� arrete la carte au cas ou

                    REPOSAQUIENVOYER(&ID2);
                    goto ARRETERDIC1;


                }

                if (tmp != '%') {
                    tmp2 = tmp2 + 1;
                    goto rep1;
                }
            } else {

                break;
            }





            REPOSAQUIENVOYER(&ID2);





            funct = 0;

            do {
                funct = funct + 1;



                if ((PRIO_AUTO_IO == 0xD1) || (PRIO_AUTO_IO == 0xD2)) // ON ARRETE ET ON PASSE EN MODE NORMAL
                {

                    TRAMERECEPTIONRS485DONNEES(9); // SE ATTEND UNE RECEPTION AVANT D'ARRETER
                    controle_CRC();


                    INFORMATION_ERREUR[0] = 'D'; // ENVOYER A L'AUTO ATTENTION JE SUIS EN MODE DEMANDE RI
                    INFORMATION_ERREUR[1] = 'D';
                    for (t = 2; t < 20; t++) {
                        INFORMATION_ERREUR[t] = 'x';
                    }

                    if (PRIO_AUTO_IO == 0xD1) {
                    }
                    //ICI 1404							ENVOIE_DES_TRAMES (6,302,&INFORMATION_ERREUR); // ENVOYER A L'AUTO
                    if (PRIO_AUTO_IO == 0xD2)
                        ENVOIE_DES_TRAMES(7, 302, &INFORMATION_ERREUR); // ENVOYER A L'IO

                    //ICI 1404					PRIO_AUTO_IO=0x00;
                    //LA CARTE RI RECOIT ERRDD' pas ERR00

                    if (PRIO_AUTO_IO == 0xD2) //ICI 1404
                    {
                        TRAMERECEPTIONRS485DONNEES(9); // SE ATTEND UNE RECEPTION AVANT D'ARRETER //ICI 1404	//ICI 1404
                        controle_CRC(); //ICI 1404

                        ID2[0] = 'R';
                        ID2[1] = TRAMEIN[2];
                    } //ICI 1404


                    //ICI 1404				TRAMERECEPTIONRS485DONNEES (9);  // SE ATTEND UNE RECEPTION AVANT D'ARRETER
                    //ICI 1404				controle_CRC();
                    INFORMATION_ERREUR[0] = 'A'; // ON ENVOIE UNE TRAME D'ARRET
                    INFORMATION_ERREUR[1] = 'A';
                    delay_ms(100); //ON ATTEND UN PEU AVANT D'ARRETER LA CARTE RI
                    for (t = 2; t < 20; t++) {
                        INFORMATION_ERREUR[t] = 'x';
                    }

                    if (ID2[1] == '1')
                        ENVOIE_DES_TRAMES(1, 302, &INFORMATION_ERREUR); // ON ENVOIE A CARTE 1 ICI voir les autres suivant demande
                    if (ID2[1] == '2')
                        ENVOIE_DES_TRAMES(2, 302, &INFORMATION_ERREUR); // ON ENVOIE A CARTE 2 ICI voir les autres suivant demande
                    if (ID2[1] == '3')
                        ENVOIE_DES_TRAMES(3, 302, &INFORMATION_ERREUR); // ON ENVOIE A CARTE 3 ICI voir les autres suivant demande
                    if (ID2[1] == '4')
                        ENVOIE_DES_TRAMES(4, 302, &INFORMATION_ERREUR); // ON ENVOIE A CARTE 4 ICI voir les autres suivant demande
                    if (ID2[1] == '5')
                        ENVOIE_DES_TRAMES(5, 302, &INFORMATION_ERREUR); // ON ENVOIE A CARTE 5 ICI voir les autres suivant demande

                    REPOSAQUIENVOYER(&ID2);
                    goto ARRETERDIC1;


                }







                TRAMERECEPTIONRS485DONNEES(9); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE //ON ATTEND 10ms dans ce sous programme avant denvoyer ERR00
                pointeur_out_in = 92; //DEBUT DU CRC RECU POUR TRAME DIC
                CRC_verif(); //ICI CRC_ETAT PASSE A 0xFF SI CORRECT
                if (NORECEPTION == 0xFF) //TROP DE NON REPONSE DE LA CARTE
                    TROPDENONREPONSE++;
                if (TROPDENONREPONSE == 4) //3x on arrete
                    goto ARRETERDIC1;
                if (TRAMEIN[0] != '$') {
                    INTCONbits.GIE = 0; //INTERDIRE LES INTERRUPTIONS GENERALES
                    //MEMORISER SI ETAT=0xFF a faire
                    if (CRC_ETAT == 0xFF) {
                        if (TRAMEIN[82] == '9') //BUG 98
                            TRAMEIN[82] = '0';
                        if ((TRAMEIN[83] == '8') || (TRAMEIN[83] == '9'))
                            TRAMEIN[83] = '0';
                        //PB HOROLOGE
                        if ((TRAMEIN[60] == '?') || (TRAMEIN[61] == '?') || (TRAMEIN[62] == '?') || (TRAMEIN[63] == '?') || (TRAMEIN[64] == '?') || (TRAMEIN[65] == '?') || (TRAMEIN[66] == '?') || (TRAMEIN[67] == '?')) {
                            TRAMEIN[60] = '2';
                            TRAMEIN[61] = '9';
                            TRAMEIN[62] = '1';
                            TRAMEIN[63] = '1';
                            TRAMEIN[64] = '0';
                            TRAMEIN[65] = '7';
                            TRAMEIN[66] = '1';
                            TRAMEIN[67] = '5';

                        }


                        recuperer_trame_memoire(1, 'r'); //STOCKAGE EN MMOIRE DE L'INFORMATION DU CAPTEUR
                    }
                    INTCONbits.GIE = 1; //AUTRISER LES INTERRUPTIONS GENERALES

                    INFORMATION_ERREUR[0] = '0'; //
                    INFORMATION_ERREUR[1] = '0';
                    for (t = 2; t < 20; t++) {
                        INFORMATION_ERREUR[t] = 'x';
                    }

                    ENVOIE_DES_TRAMES(ID2[1] - 48, 302, &INFORMATION_ERREUR); // ON ENVOIE A CARTE x ICI voir les autres suivant demande
                }





            }

 while (TRAMEIN[1] != '$' && TRAMEIN[0] != '$');
            if (PRIO_AUTO_IO == 0xDD)
                PRIO_AUTO_IO = 0x00; // RETOUR EN ETAT NORMAL

ARRETERDIC1:

            INFORMATION_ERREUR[0] = 'A'; // ON ENVOIE UNE TRAME D'ARRET
            INFORMATION_ERREUR[1] = 'A';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            if (TRAMEIN[2] == '1')
                ENVOIE_DES_TRAMES(1, 302, &INFORMATION_ERREUR); // ON ENVOIE A CARTE 1 ICI voir les autres suivant demande
            if (TRAMEIN[2] == '2')
                ENVOIE_DES_TRAMES(2, 302, &INFORMATION_ERREUR); // ON ENVOIE A CARTE 2 ICI voir les autres suivant demande
            if (TRAMEIN[2] == '3')
                ENVOIE_DES_TRAMES(3, 302, &INFORMATION_ERREUR); // ON ENVOIE A CARTE 3 ICI voir les autres suivant demande
            if (TRAMEIN[2] == '4')
                ENVOIE_DES_TRAMES(4, 302, &INFORMATION_ERREUR); // ON ENVOIE A CARTE 4 ICI voir les autres suivant demande
            if (TRAMEIN[2] == '5')
                ENVOIE_DES_TRAMES(5, 302, &INFORMATION_ERREUR); // ON ENVOIE A CARTE 5 ICI voir les autres suivant demande
            if ((TRAMEIN[2] != '5')&&(TRAMEIN[2] != '4')&&(TRAMEIN[2] != '3')&&(TRAMEIN[2] != '2')&&(TRAMEIN[2] != '1'))
                ENVOIE_DES_TRAMES(1, 302, &INFORMATION_ERREUR); // securit� arrete la carte au cas ou

            funct = 0;


            INTCONbits.GIE = 1; //AUTORISER LES INTERRUPTIONS GENERALES
            if (PRIO_AUTO_IO == 0xD1) //ICI 1404
                identificationINTER(6, 0x00); //ICI 1404

            PRIO_AUTO_IO = 0x00; // RETOUR EN ETAT NORMAL			//ICI 1404




            break;



        case 308: // DEMANDE LISTE DES CAPTEURS A LA CARTE RELAIS




            cFonctionEnvoie[0] = 'L';
            cFonctionEnvoie[1] = 'I';
            cFonctionEnvoie[2] = 'S';

            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = cFonctionEnvoie[0];
            TRAMEOUT[6] = cFonctionEnvoie[1];
            TRAMEOUT[7] = cFonctionEnvoie[2];


            pointeur_out_in = 8;



            calcul_CRC(); //A COMPLETER
            TRAMEOUT[pointeur_out_in + 5] = cFinTrame;

            //LA TRAME EST PRETE
            AQUIENVOYER(&ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
            TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE
            REPOSAQUIENVOYER(&ID2);
            do {
                TRAMERECEPTIONRS485DONNEES(9); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE

                controle_CRC();


                RECUPERATION_DES_TRAMES(&TRAMEIN);
                METTRE_EN_MEMOIRE_LISTE(&TRAMEIN, 0);




                INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
                INFORMATION_ERREUR[1] = '0';
                for (t = 2; t < 20; t++) {
                    INFORMATION_ERREUR[t] = 'x';
                }
                ENVOIE_DES_TRAMES(R, 302, &INFORMATION_ERREUR);


            } while (TRAMEIN[98] != 'F'); // JUSQU A LA DERNIERE TRAME



            // IL Y A UNE TRAME PUIS UNE AUTRES ETC LES RANGER EN MEMOIRES A UN EMPLACEMENT SPECIFIQUE


            break;



        case 290: //DEMANDE LISTE DES CAPTEURS A LA CARTE RELAIS AVEC CHANGEMENTS IMPORTANTS

            cFonctionEnvoie[0] = 'L';
            cFonctionEnvoie[1] = 'I';
            cFonctionEnvoie[2] = 'A';

            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = cFonctionEnvoie[0];
            TRAMEOUT[6] = cFonctionEnvoie[1];
            TRAMEOUT[7] = cFonctionEnvoie[2];


            pointeur_out_in = 8;



            calcul_CRC(); //A COMPLETER
            TRAMEOUT[pointeur_out_in + 5] = cFinTrame;

            //LA TRAME EST PRETE
            AQUIENVOYER(&ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
            TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE
            REPOSAQUIENVOYER(&ID2);
            do {
                TRAMERECEPTIONRS485DONNEES(9); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE

                controle_CRC();


                RECUPERATION_DES_TRAMES(&TRAMEIN);
                METTRE_EN_MEMOIRE_LISTE(&TRAMEIN, 1);




                INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
                INFORMATION_ERREUR[1] = '0';
                for (t = 2; t < 20; t++) {
                    INFORMATION_ERREUR[t] = 'x';
                }
                ENVOIE_DES_TRAMES(R, 302, &INFORMATION_ERREUR);


            } while (TRAMEIN[98] != 'F'); // JUSQU A LA DERNIERE TRAME



            // IL Y A UNE TRAME PUIS UNE AUTRES ETC LES RANGER EN MEMOIRES A UN EMPLACEMENT SPECIFIQUE


            break;






        case 304: // TABLEAU DEXISTENCE


            PRIO_AUTO_IO = 0xEE; //ON EST EN MODE cm table exiST
            INTCONbits.GIE = 1; //AUTORISER LES INTERRUPTIONS GENERALES

            cFonctionEnvoie[0] = 'T';
            cFonctionEnvoie[1] = 'E';
            cFonctionEnvoie[2] = 'C';

            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = cFonctionEnvoie[0];
            TRAMEOUT[6] = cFonctionEnvoie[1];
            TRAMEOUT[7] = cFonctionEnvoie[2];

            pointeur_out_in = 8;
            calcul_CRC(); //A COMPLETER
            TRAMEOUT[pointeur_out_in + 5] = cFinTrame;
            funct = 0;


            tmp2 = 0; //SYNCHRONISATION ON ATTEND "%" debut supertrame
            tmp = 0;
rep3:
            if (tmp2 <= 4) //4 ESSAIS
            {
                AQUIENVOYER(&ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
                delay_ms(100); //attend un peu avant d'envoyer
                TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE
                REPOSAQUIENVOYER(&ID2);
                //TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE
                tmp = RECEVOIR_RS485(9); // ON ATTEND '%'
                if (NORECEPTION == 0xFF) // RIEN REcU
                {
                    if (ID2[1] == '1')
                        R1NOREP = R1NOREP + 1;
                    if (ID2[1] == '2')
                        R2NOREP = R2NOREP + 1;
                    if (ID2[1] == '3')
                        R3NOREP = R3NOREP + 1;
                    if (ID2[1] == '4')
                        R4NOREP = R4NOREP + 1;
                    if (ID2[1] == '5')
                        R5NOREP = R5NOREP + 1;


                    tmp2 = tmp2 + 1;
                    goto rep3;
                } else {
                    if (ID2[1] == '1') {
                        pic_eeprom_write(0x01, 0xAA); //RESET RI A ZERO
                        R1NOREP = 0;
                        UNRESETRI1 = 0;
                    }
                    if (ID2[1] == '2') {
                        pic_eeprom_write(0x02, 0xAA); //RESET RI A ZERO
                        R2NOREP = 0;
                        UNRESETRI2 = 0;
                    }
                    if (ID2[1] == '3') {
                        pic_eeprom_write(0x03, 0xAA); //RESET RI A ZERO
                        R3NOREP = 0;
                        UNRESETRI3 = 0;
                    }
                    if (ID2[1] == '4') {
                        pic_eeprom_write(0x04, 0xAA); //RESET RI A ZERO
                        R4NOREP = 0;
                        UNRESETRI4 = 0;
                    }
                    if (ID2[1] == '5') {
                        pic_eeprom_write(0x05, 0xAA); //RESET RI A ZERO
                        R5NOREP = 0;
                        UNRESETRI5 = 0;
                    }


                }


            } else {

                break;
            }


            REPOSAQUIENVOYER(&ID2);





            funct = 0;

            do {
                funct = funct + 1;
                TRAMERECEPTIONRS485DONNEES(9); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
                pointeur_out_in = 28;
                CRC_verif(); //ETAT_CRC est a 0xFF Si ok
                //controle_CRC();

                if (TRAMEIN[0] != '$') {
                    INTCONbits.GIE = 0; //AUTORISER LES INTERRUPTIONS GENERALES
                    //A FAIRE SI ETAT_CRC EST CORRECT 0xFF SAV
                    if (CRC_ETAT == 0xFF)
                        SAV_EXISTENCE(&TRAMEIN);
                    INTCONbits.GIE = 1; //AUTORISER LES INTERRUPTIONS GENERALES
                }
                //recuperer_trame_memoire (1); //STOCKAGE EN MMOIRE DE L'INFORMATION DU CAPTEUR
            }

 while ((TRAMEIN[0] != '$')&&(NORECEPTION != 0xFF));

            funct = 0;
            //			 LECTURE DES TABLES POUR TEST
            for (funct = 0; funct < 100; funct++) {


                LIRE_EEPROM(3, 128 * funct + 30000, &TRAMEOUT, 4); //LIRE DANS EEPROM
                TRAMEIN[0] != '$';


            }


            if (PRIO_AUTO_IO == 0xEE)
                PRIO_AUTO_IO = 0x00; // RETOUR EN ETAT NORMAL
            //INTCONbits.GIE= 1; //AUTORISER LES INTERRUPTIONS GENERALES




            if ((PRIO_AUTO_IO == 0xE1) || (PRIO_AUTO_IO == 0xE2)) // ON A RECU UNE DEMANDE DE LIO OU AU
            {

                if (PRIO_AUTO_IO == 0xE2) //ON A DETECTER UNE DEMANDE DE L'IO PENDANT LINTERROGATION TEC
                    identificationINTER(7, 0x00); // ON FORCE MANUELLEMENT en MODE NORMALE POUR VALIDER LACTION DE IO
                if (PRIO_AUTO_IO == 0xE1) // ON A DETECTER UNE DEMANDE DE L'AUTO PENDANT LINTERROGATION TEC
                    identificationINTER(6, 0x00);


                PRIO_AUTO_IO == 0x00;
                // VOIR SI ON ARETE OU ON DONNE ERR00 OU DD
            }

            break;

        case 305: //OK HOR


            // REGLER HORLOGE DE LA CARTE RELAIS
            //HORLOGE[8] A REMPIR hh/mm/ss/aa

            //ENVOIE_DES_TRAMES (1,305,&HORLOGE)
            PRIO_AUTO_IO = 0x77; //ON EST EN MODE HORLOGE
            //INTCONbits.GIE = 0; //21102020
            cFonctionEnvoie[0] = 'H';
            cFonctionEnvoie[1] = 'O';
            cFonctionEnvoie[2] = 'R';

            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = cFonctionEnvoie[0];
            TRAMEOUT[6] = cFonctionEnvoie[1];
            TRAMEOUT[7] = cFonctionEnvoie[2];
            TRAMEOUT[8] = tab[0];
            TRAMEOUT[9] = tab[1];
            TRAMEOUT[10] = tab[2];
            TRAMEOUT[11] = tab[3];
            TRAMEOUT[12] = tab[4];
            TRAMEOUT[13] = tab[5];
            TRAMEOUT[14] = tab[6];
            TRAMEOUT[15] = tab[7];
            TRAMEOUT[16] = tab[8];
            TRAMEOUT[17] = tab[9];
            TRAMEOUT[18] = tab[10];
            TRAMEOUT[19] = tab[11];
            pointeur_out_in = 20;
            calcul_CRC(); //A COMPLETER
            TRAMEOUT[pointeur_out_in + 5] = cFinTrame;
            funct = 0;



            tmp2 = 0; //SYNCHRONISATION ON ATTEND "%" debut supertrame
            tmp = 0;
            MAXBITE = 0;
rep2:


            if (tmp2 <= 4) //4 ESSAIS
            {
                if (PRIO_AUTO_IO == 0x00) // UNE DEMANDE DE L'AUTO A ETE 					
                    break;

                AQUIENVOYER(&ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
                delay_ms(100); //attend un peu avant d'envoyer
                TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE
                REPOSAQUIENVOYER(&ID2);
                //TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE
                TRAMERECEPTIONRS485DONNEES(9); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
                controle_CRC();
                if (NORECEPTION == 0xFF) // RIEN REcU
                {
                    creer_trame_horloge(); //LIRE EEPPROM VERIFICATION
                    for (funct = 0; funct < 12; funct++) {
                        TRAMEOUT[funct + 8] = HORLOGE[funct];
                    }

                    tmp2 = tmp2 + 1;




                    goto rep2;
                }
            } else {
                //INTCONbits.GIE = 1; //21102020
                break;
            }


            //// A FAIRE CONTROLE HEURE OU CHANGER PAR TRAME OK???????????????????????????
            PRIO_AUTO_IO == 0x00;
            funct = 0;
            break;









        case 285: // CREATION CAPTEUR (MEME SI ON CHANGE JUSTE LE SEUIL)
            // UTILISATION TRAMEIN[] -> ENVOIE_DES_TRAMES (1,285,TRAMEIN[])

            cFonctionEnvoie[0] = 'C';
            cFonctionEnvoie[1] = 'R';
            cFonctionEnvoie[2] = 'E';




            recuperer_trame_memoire(0, 'a'); //TRANSFORME LA TRAMEIN EN FORMAT MEMOIRE SUR TRAMEOUT
            intervertirOUT_IN();
            TRANSFORMER_TRAME_MEM_EN_COM(); // TRANSFORME LA TRAMEIN MEMOIRE en TRAMEOUUT COM
            //RECURRENT
            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = cFonctionEnvoie[0];
            TRAMEOUT[6] = cFonctionEnvoie[1];
            TRAMEOUT[7] = cFonctionEnvoie[2]; //TRAMEOUT;


            calcul_CRC(); //A COMPLETER
            TRAMEOUT[pointeur_out_in + 5] = cFinTrame;

            //LA TRAME EST PRETE
            tmp2 = 0; //SYNCHRONISATION ON ATTEND "%" debut supertrame
            tmp = 0;
rep6:
            if (tmp2 <= 10) //10 ESSAIS
            {
                if ((PRESENCEDEBITSD == 0x00)&&(TRAMEIN[4] == '0')&&(TRAMEIN[5] == '0')) //Si le debitmetre nexiste pas alors prevenir de SAV en RI mais pas l'existence
                    TRAMEOUT[92] = 0; //sur le CRC mette 02222 au lieu de 22222
                AQUIENVOYER(&ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
                delay_ms(100); //attend un peu avant d'envoyer
                TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE
                REPOSAQUIENVOYER(&ID2);
                //TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE
                TRAMERECEPTIONRS485DONNEES(9); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
                controle_CRC();
                if (NORECEPTION == 0xFF) // RIEN REcU
                {
                    tmp2 = tmp2 + 1;
                    goto rep6;
                }
            } else {

                break;
            }
            //RECUPERATION_DES_TRAMES (&TRAMEIN);



            funct = 0;



            break;

        case 278: // EFFACER CAPTEUR SCR OK

            cFonctionEnvoie[0] = 'E';
            cFonctionEnvoie[1] = 'F';
            cFonctionEnvoie[2] = 'F';

            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = cFonctionEnvoie[0];
            TRAMEOUT[6] = cFonctionEnvoie[1];
            TRAMEOUT[7] = cFonctionEnvoie[2];
            TRAMEOUT[8] = tab[0];
            TRAMEOUT[9] = tab[1];
            TRAMEOUT[10] = tab[2];
            TRAMEOUT[11] = tab[3];
            TRAMEOUT[12] = tab[4];
            TRAMEOUT[13] = tab[5];

            pointeur_out_in = 14;
            calcul_CRC(); //A COMPLETER

            TRAMEOUT[pointeur_out_in + 5] = cFinTrame;

            tmp2 = 0; //SYNCHRONISATION ON ATTEND "%" debut supertrame
            tmp = 0;
rep5:
            if (tmp2 <= 10) //10 ESSAIS
            {
                delay_ms(10); //attend un peu avant d'envoyer
                AQUIENVOYER(&ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
                delay_ms(100); //attend un peu avant d'envoyer
                TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE
                REPOSAQUIENVOYER(&ID2);
                //TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE
                TRAMERECEPTIONRS485DONNEES(9); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
                controle_CRC();
                if (NORECEPTION == 0xFF) // RIEN REcU
                {
                    tmp2 = tmp2 + 1;
                    goto rep5;
                }
            } else {

                break;
            }

            EFFACEMENT(&DEMANDE_INFOS_CAPTEURS); //VOIR SI NECCESSAIRE

            funct = 0;

            break;

        case 315: //LANCER LA DETECTION AUTOMATIQUE


            INTCONbits.GIE = 0;


            cFonctionEnvoie[0] = 'S';
            cFonctionEnvoie[1] = 'C';
            cFonctionEnvoie[2] = 'R';
            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = cFonctionEnvoie[0];
            TRAMEOUT[6] = cFonctionEnvoie[1];
            TRAMEOUT[7] = cFonctionEnvoie[2];
            TRAMEOUT[8] = INSTRUCTION[0];
            TRAMEOUT[9] = INSTRUCTION[1];



            pointeur_out_in = 10;
            calcul_CRC(); //A COMPLETER
            if (scrutationunevoie == 1) {
                TRAMEOUT[10] = '7'; //RUSE SUR LE CRC NON GERE POUR INDIQUER FONCTION CACHEE SCRUTATION UNE SEULE VOIE
                TRAMEOUT[11] = '7';
            }


            TRAMEOUT[pointeur_out_in + 5] = cFinTrame;




            tmp2 = 0; //SYNCHRONISATION ON ATTEND "%" debut supertrame
            tmp = 0;
rep4:
            if (tmp2 <= 10) //10 ESSAIS
            {
                AQUIENVOYER(&ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
                delay_ms(100); //attend un peu avant d'envoyer
                TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE
                REPOSAQUIENVOYER(&ID2);
                //TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE
                TRAMERECEPTIONRS485DONNEES(18); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
                controle_CRC();
                if (NORECEPTION == 0xFF) // RIEN REcU
                {
                    tmp2 = tmp2 + 1;
                    //delay_ms(100); //attend un peu avant d'envoyer
                    goto rep4;
                }
            } else {

                break;
            }

            do {
                tmp = RECEVOIR_RS485(18); //9 ici


                if (NORECEPTION == 0x00)
                    tmp2 = 0;

                if (tmp == '7') //ERREUR CARTE ON RETENTE
                {
                    tmp = '$';
                    TEMPO(2);
                    RESCRUTATION++;
                }

                if (tmp == 0x00) //ERREUR CARTE ON RETENTE
                {
                    tmp = '$';
                    RESCRUTATION++;
                }
            } while (tmp != '$');
            // ON FAIT UN DIC pour avoir les donn�es



            break;



        case 300: //fonction oeil




            INTCONbits.GIE = 0;

            //            OEIL_CAPTEUR[0] = tab[0];
            //            OEIL_CAPTEUR[1] = tab[1];
            //            OEIL_CAPTEUR[2] = tab[2];
            //            OEIL_CAPTEUR[3] = tab[3];
            //            OEIL_CAPTEUR[4] = tab[4];

            do {


                cFonctionEnvoie[0] = 'O';
                cFonctionEnvoie[1] = 'E';
                cFonctionEnvoie[2] = 'I';
                TRAMEOUT[0] = cDebutTrame;
                TRAMEOUT[1] = ID1[0];
                TRAMEOUT[2] = ID1[1];
                TRAMEOUT[3] = ID2[0];
                TRAMEOUT[4] = ID2[1];
                TRAMEOUT[5] = cFonctionEnvoie[0];
                TRAMEOUT[6] = cFonctionEnvoie[1];
                TRAMEOUT[7] = cFonctionEnvoie[2];
                //                TRAMEOUT[8] = OEIL_CAPTEUR[0];
                //                TRAMEOUT[9] = OEIL_CAPTEUR[1];
                //                TRAMEOUT[10] = OEIL_CAPTEUR[2];
                //                TRAMEOUT[11] = OEIL_CAPTEUR[3];
                //                TRAMEOUT[12] = OEIL_CAPTEUR[4];
                TRAMEOUT[8] = tab[0]; //OEIL_CAPTEUR[0] prend forcement la valeur de tab[0]
                TRAMEOUT[9] = tab[1]; //OEIL_CAPTEUR[1] prend forcement la valeur de tab[1]
                TRAMEOUT[10] = tab[2]; //OEIL_CAPTEUR[2] prend forcement la valeur de tab[2]
                TRAMEOUT[11] = tab[3]; //OEIL_CAPTEUR[3] prend forcement la valeur de tab[3]
                TRAMEOUT[12] = tab[4]; //OEIL_CAPTEUR[4] prend forcement la valeur de tab[4]


                pointeur_out_in = 13;


                calcul_CRC(); //A COMPLETER
                TRAMEOUT[pointeur_out_in + 5] = cFinTrame;





                tmp2 = 0; //SYNCHRONISATION ON ATTEND "%" debut supertrame
                tmp = 0;
                IDENTIF_TYPE_CARTE(); //NORMALEMENT SE FAIT PAR LE REA !!!!!!! A VERIFIER
                if (TRAMEOUT[4] == '1') {
                    TYPEDECARTEOEIL = 'E';
                    if (TYPECARTE[0] != 'E') //RESISTIVE
                    {
                        TRAMEOUT[11] = '0';
                        TRAMEOUT[12] = '1';
                        TYPEDECARTEOEIL = 'M';
                    }
                }

                if (TRAMEOUT[4] == '2') {
                    TYPEDECARTEOEIL = 'E';
                    if (TYPECARTE[1] != 'E') //RESISTIVE
                    {
                        TRAMEOUT[11] = '0';
                        TRAMEOUT[12] = '1';
                        TYPEDECARTEOEIL = 'M';
                    }
                }

                if (TRAMEOUT[4] == '3') {
                    TYPEDECARTEOEIL = 'E';
                    if (TYPECARTE[2] != 'E') //RESISTIVE
                    {
                        TRAMEOUT[11] = '0';
                        TRAMEOUT[12] = '1';
                        TYPEDECARTEOEIL = 'M';
                    }
                }

                if (TRAMEOUT[4] == '4') {
                    TYPEDECARTEOEIL = 'E';
                    if (TYPECARTE[3] != 'E') //RESISTIVE
                    {
                        TRAMEOUT[11] = '0';
                        TRAMEOUT[12] = '1';
                        TYPEDECARTEOEIL = 'M';
                    }
                }

                if (TRAMEOUT[4] == '5') {
                    TYPEDECARTEOEIL = 'E';
                    if (TYPECARTE[4] != 'E') //RESISTIVE
                    {
                        TRAMEOUT[11] = '0';
                        TRAMEOUT[12] = '1';
                        TYPEDECARTEOEIL = 'M';
                    }
                }





rep7:
                if (capt_exist == 11) //CAD A DIRE FONCTION OEIL AUTOMATE CONFIRME LEXISTENCE DE CE CAPTEUR
                {
                    if (tmp2 <= 10) //10 ESSAIS
                    {
                        AQUIENVOYER(&ID2); // ACTIVE L'ENVOI LE FLAG DRS485xxx CONCERNE
                        delay_ms(100); //attend un peu avant d'envoyer
                        //METTRE EN MODE RELATIF





                        TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE

                        //TRAMEENVOIRS485DONNEES (); // ENVOI A LA CARTE CONCERNEE
                        TRAMERECEPTIONRS485DONNEES(15); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
                        controle_CRC();
                        if (NORECEPTION == 0xFF) // RIEN REcU
                        {
                            tmp2 = tmp2 + 1;
                            goto rep7;
                        }
                    } else {

                        break;
                    }





                    REPOSAQUIENVOYER(&ID2); // ON ARRETE L'OEIL




                    //IDENTIF_TYPE_CARTE (); //NORMALEMENT SE FAIT PAR LE REA !!!!!!! A VERIFIER

                    tmp2 = 0; // ON ATTEND LA TRAME
                    tmp = 0;
rep9:
                    if (tmp2 <= 3) //10 ESSAIS
                    {

                        if (TYPEDECARTEOEIL != 'E') //RESISTIVE
                            TRAMERECEPTIONRS485DONNEES(15); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE DU CAPTEUR SELECTIONNE POUR (5) environ 7s
                        if (TYPEDECARTEOEIL == 'E') //ADRESSABLE
                            TRAMERECEPTIONRS485DONNEES(41); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE DU CAPTEUR SELECTIONNE POUR (5) environ 7s
                        controle_CRC();
                        recuperer_trame_memoire(1, 'r'); //ENREGISTRE
                        if (NORECEPTION == 0xFF) // RIEN REcU
                        {
                            tmp2 = tmp2 + 1;
                            goto rep9;
                        }
                    } else {

                        break;
                    }







                    TRAMEOUT[1] = 'C';
                    TRAMEOUT[2] = 'M';
                    TRAMEOUT[3] = 'A';
                    TRAMEOUT[4] = 'U';
                    TRAMEOUT[5] = 'O';
                    TRAMEOUT[6] = 'E';
                    TRAMEOUT[7] = 'I';

                    for (tmp2 = 8; tmp2 < 93; tmp2++) {
                        TRAMEOUT[tmp2] = TRAMEIN[tmp2 - 7];


                    }
                    pointeur_out_in = 92;

                    //			construire_trame_memoire();  /// transmet � l'automate les donnees du capteur
                    calcul_CRC(); //A COMPLETER
                    //recuperer_trame_memoire(0); //Recupere les donnees trame in pour les mettres dans trameout

                    delay_ms(50);


                } else //LE CAPTEUR NEXISTE PAS ON ENVOIE UN CODE
                {
                    TRAMEOUT[0] = cDebutTrame;
                    TRAMEOUT[1] = 'C';
                    TRAMEOUT[2] = 'M';
                    TRAMEOUT[3] = 'A';
                    TRAMEOUT[4] = 'U';
                    TRAMEOUT[5] = 'O';
                    TRAMEOUT[6] = 'E';
                    TRAMEOUT[7] = 'I';

                    for (tmp2 = 8; tmp2 < 93; tmp2++) {
                        TRAMEOUT[tmp2] = '9';


                    }
                    TRAMEOUT[84] = '0';
                    TRAMEOUT[85] = '0';
                    TRAMEOUT[86] = '0';
                    TRAMEOUT[87] = '0';
                    if (capt_exist == 20) // IL Y A UN CODAGE AUTRE QUI EXISTE
                    {
                        TRAMEOUT[22] = '0';
                        TRAMEOUT[23] = '0';
                        TRAMEOUT[24] = '0';
                        TRAMEOUT[25] = '7';
                    }


                    pointeur_out_in = 92;

                    //			construire_trame_memoire();  /// transmet � l'automate les donnees du capteur
                    calcul_CRC(); //A COMPLETER
                    //recuperer_trame_memoire(0); //Recupere les donnees trame in pour les mettres dans trameout

                    delay_ms(50);


                    TEMPO(3);
                }
                TRAMEENVOIRS485DONNEES(); // ENVOI A L'AUTOMATE

                //TRAMERECEPTIONRS485DONNEES (); //ATTEND QUE L'AUTOMATE EST OK
                //controle_CRC();

                //					INFORMATION_ERREUR[0]='0'; //TT EST BON donc utilisation type d'erreur pour carte Rx
                //					INFORMATION_ERREUR[1]='0';
                //					for (t=2;t<20;t++)
                //					{
                //					INFORMATION_ERREUR[t]='x';
                //					}
                //					ENVOIE_DES_TRAMES (trouver_carte_suivantvoie(&DEMANDE_INFOS_CAPTEURS),302,&INFORMATION_ERREUR);



                SERIALISATION();


finoeil:
                ENTREES2 = ENTREES2 & 0x02;

                TEMPO(3); // ON ATTEND PLUS RIEN SUR LA LIGNE
            } while (ENTREES2 == 2);



            // TANT QUE LE FLAG DEMANDE DE L'AUTOMATE RESTE A 1 SI LE FLAF PASSE A ZERO LE FLAGUE VERS LA CARTE Rx PASSE A 0

            REPOSAQUIENVOYER(&ID2); // ON ARRETE L'OEIL
            //INTCONbits.GIE= 1; ON ARRETE OEIL SUR AUTOMATE OEIL 322












            //while (TRAMEIN[8]<>'F')    // TRAME ERR 'FF'














            funct = 0;




            break;



        default:


            break;





    }





    //TRAMEENVOIRS485DONNEES();









}






////////////////////////////////////////////////////////////////////////////////////////
// UNIQUEMENT POUR CARTE MERE RECOIT exemple CM->R1
//
//DEMANDE D'INFORMATIONS SUR CAPTEUR A LA CARTE MERE par la carte R1
//suivant la fonction obtenu tab , et apres une interruption sur INT0
//
////////////////////////////////////////////////////////////////////////////////////////

void RECUPERATION_DES_TRAMES(char *tab) {
    char TYPECOMMANDE; //D�claration dans RECUPERATION_DES_TRAMES pour enlever la variable globale
    char temoinfinvoieZIZ; //D�claration dans RECUPERATION_DES_TRAMES pour enlever la variable globale

    int iCalculBLOCRecup; //Remplacement de calculBLOC par iCalculBLOCRecup
    unsigned char cFonctionRecup[3]; //Remplacement du tableau global FONCTION par le tableau cFonctionRecup
    char cSAVTABRecup[25]; //Remplace le tableau global SAVTAB utilis� dans RECUPERATION_DES_TRAMES
    int t;
    int funct;
    char etatIOAU, encoreunefois, AVECEXIST, selectionvoie;
    int voieDEM, codageDEM;
    int voieDEMFINAL, codageDEMFINAL;



    ID1[0] = tab[1];
    ID1[1] = tab[2];

    ID2[0] = tab[3];
    ID2[1] = tab[4];

    cFonctionRecup[0] = tab[5];
    cFonctionRecup[1] = tab[6];
    cFonctionRecup[2] = tab[7];


    funct = (cFonctionRecup[0] << 16)*2; // max 0-255 valeur decimal du code ascii de chaque lettre ex ERR E=69 R=82 E=82
    //methode 69*2 + 82 + 82 = 302
    funct = funct + (cFonctionRecup[1] << 16);
    funct = funct + (cFonctionRecup[2] << 16);






    switch (funct) //analyse de la fonction

 {
        case 291: //ECRIRE SUR SD SUIVANT DEMANDE AU
            INTCONbits.GIE = 0;
            cFonctionRecup[0] = 'D';
            cFonctionRecup[1] = 'D';
            cFonctionRecup[2] = 'R';
            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];

            INSTRUCTION[0] = 'C'; //CYCLIQUE
            ENVOIE_DES_TRAMES(6, 291, &INSTRUCTION); //ERCIRE SUR SD


            //INTCONbits.GIE= 0;


            INFORMATION_ERREUR[0] = 'F'; //ON SIGNALE A LAUTOMATE QUE CEST TERMINE
            INFORMATION_ERREUR[1] = 'F';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);



            INTCONbits.GIE = 1;

            break;

        case 319: //RESET RAZ
            INTCONbits.GIE = 0;
            TEMPO(2);
            if ((TRAMEIN[8] == 'r')&&(TRAMEIN[9] == 'e')) {
                pic_eeprom_write(0x04, 0xAA); //TROISIEME RESET
                pic_eeprom_write(0x03, 0xAA); //TROISIEME RESET
                pic_eeprom_write(0x02, 0xAA); //TROISIEME RESET
                pic_eeprom_write(0x01, 0xAA); //TROISIEME RESET
                pic_eeprom_write(0x05, 0xAA); //TROISIEME RESET
                pic_eeprom_write(0x20, 0xFF); //RAZ COMPTEUR
                pic_eeprom_write(0x21, 0xFF); //RAZ COMPTEUR
                pic_eeprom_write(0x22, 0xFF); //RAZ COMPTEUR
                pic_eeprom_write(0x23, 0xFF);
                pic_eeprom_write(0x24, 0xFF);
                pic_eeprom_write(0x25, 0xFF);


                RESETCMS = 1;
                RESETCMS = 1;
            }
            break;

        case 270:

            INTCONbits.GIE = 0;
            TEMPO(5); // pour 30 on a 33s donc 100ms delay_ms enriron 110ms
            //SDR(); //PROCEDURE DE LECTURE CARTE SD ET PROGRAMMATION CM
            //INTCONbits.GIE= 0;
            cFonctionRecup[0] = 'D';
            cFonctionRecup[1] = 'D';
            cFonctionRecup[2] = 'B';
            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = cFonctionRecup[0];
            TRAMEOUT[6] = cFonctionRecup[1];
            TRAMEOUT[7] = cFonctionRecup[2];

            SDRBN(); //PROCEDURE DE LECTURE CARTE SD ET PROGRAMMATION CM




            INTCONbits.GIE = 1;



            break;






        case 286: //LIRE CARTE SD POUR REPROG LA CM DEMANDE FAITE PAR L'AUTOMATE


            INTCONbits.GIE = 0;
            TEMPO(5); // pour 30 on a 33s donc 100ms delay_ms enriron 110ms
            //SDR(); //PROCEDURE DE LECTURE CARTE SD ET PROGRAMMATION CM
            //INTCONbits.GIE= 0;
            cFonctionRecup[0] = 'D';
            cFonctionRecup[1] = 'D';
            cFonctionRecup[2] = 'R';
            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = cFonctionRecup[0];
            TRAMEOUT[6] = cFonctionRecup[1];
            TRAMEOUT[7] = cFonctionRecup[2];

            SDR(); //PROCEDURE DE LECTURE CARTE SD ET PROGRAMMATION CM

            INFORMATION_ERREUR[0] = 'F'; //ON SIGNALE A LAUTOMATE QUE CEST TERMINE
            INFORMATION_ERREUR[1] = 'F';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);



            INTCONbits.GIE = 1;



            break;



        case 332: //DEMANDE LISTE VOIES PAR ROBINET

            INTCONbits.GIE = 0; // ON INTERDIT TTE DEMANDES
            cFonctionRecup[0] = 'R';
            cFonctionRecup[1] = 'T';
            cFonctionRecup[2] = 'T';
            pointeur_out_in = 14; // SAUVEGARDE POINTEUR DU TABLEAU
            controle_CRC;

            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = cFonctionRecup[0];
            TRAMEOUT[6] = cFonctionRecup[1];
            TRAMEOUT[7] = cFonctionRecup[2];

            INSTRUCTION[0] = TRAMEIN[8];
            INSTRUCTION[1] = TRAMEIN[9];
            TRAMEOUT[8] = INSTRUCTION[0];
            TRAMEOUT[9] = INSTRUCTION[1];
            TROUVER_ROBINET();
            for (t = 0; t <= 10; t++) {
                IntToChar(INSTRUCTION[2 + t], &DEMANDE_INFOS_CAPTEURS, 3);
                TRAMEOUT[10 + 3 * t] = DEMANDE_INFOS_CAPTEURS[0];
                TRAMEOUT[11 + 3 * t] = DEMANDE_INFOS_CAPTEURS[1];
                TRAMEOUT[12 + 3 * t] = DEMANDE_INFOS_CAPTEURS[2];
            }

            pointeur_out_in = 43; // SAUVEGARDE POINTEUR DU TABLEAU
            calcul_CRC();

            TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE


            break;



        case 298: //DEMANDE DE PRISE OU D'ARRET DE L'AUTOMATE REA


            INTCONbits.GIE = 0; // ON INTERDIT TTE DEMANDES
            cFonctionRecup[0] = 'R';
            cFonctionRecup[1] = 'E';
            cFonctionRecup[2] = 'A';
            pointeur_out_in = 14; // SAUVEGARDE POINTEUR DU TABLEAU
            controle_CRC;

            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = cFonctionRecup[0];
            TRAMEOUT[6] = cFonctionRecup[1];
            TRAMEOUT[7] = cFonctionRecup[2];
            //            FONCTION_ACTIVATION_AUTO = 0x00; //ACTIVATION AUTO
            //            if ((TRAMEIN[8] == 'F') && (TRAMEIN[9] == 'F')) //ACTIVATION PRISE DE MAIN AUTOMATE
            //                FONCTION_ACTIVATION_AUTO = 0xFF; //ACTIVATION AUTO
            //
            //            if (FONCTION_ACTIVATION_AUTO == 0xFF) {
            //                if (FIRST_DEMAR != 0XFF) {
            //                    stopIO();
            //                }
            //            }


            IDENTIF_TYPE_CARTE();


            TRAMEOUT[8] = TRAMEIN[8];
            if (ETAT_IO == 0x00)
                TRAMEOUT[9] = 'F'; //MAJ PAT !
            else
                TRAMEOUT[9] = 'F';

            TRAMEOUT[10] = TYPECARTE[0];
            TRAMEOUT[11] = TYPECARTE[1];
            TRAMEOUT[12] = TYPECARTE[2];
            TRAMEOUT[13] = TYPECARTE[3];
            TRAMEOUT[14] = TYPECARTE[4];

            pointeur_out_in = 15; // SAUVEGARDE POINTEUR DU TABLEAU
            calcul_CRC();
            TEMPO(1);
            delay_ms(200); //ATTENTE PR AUTO
            TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE
            TEMPO(1);
            //DRS485_IO=0; //ON SIGNALE A ON SIGNALE A LA CARTE IO DE REPRENDRE SON BOULOT
            DRS485_IO = 0;
            INTCONbits.GIE = 1; //
            break;









        case 336: //IO DEMANDE A VOIR PARAM

            INTCONbits.GIE = 0; // ON INTERDIT TTE DEMANDES
            cFonctionRecup[0] = 'Z';
            cFonctionRecup[1] = 'P';
            cFonctionRecup[2] = 'L';

            DEMANDEZPLIO = DEMANDEZPLIO + 1;




            pointeur_out_in = 14; // SAUVEGARDE POINTEUR DU TABLEAU
            controle_CRC;


            LIRE_EEPROM(6, 1024, &TRAMEOUT, 5);
            pointeur_out_in = 115;
            calcul_CRC(); // CALCUL LE CRC ET LE RAJOUTE AU TABLEAU TRAMEOUT[]
            TRAMEOUT[pointeur_out_in + 5] = cFinTrame; // RAJOUT LE BYTE DE STOP

            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = cFonctionRecup[0];
            TRAMEOUT[6] = cFonctionRecup[1];
            TRAMEOUT[7] = cFonctionRecup[2];

            if (INT0 == 1) //une demande de l'autmate a ete faite !!!!!!!! pas sur interruption juste touche
            {
                SERIALISATION(); // VOIR SI DEMANDE DE L'AUTO
                if ((ENTREES2 & 0x02) == 0x02) {

                    PRIO_AUTO_IO == 0x00;
                    TEMPO(1);
                    identificationINTER(6, 0x00); // ON EST OK POUR LA DEMANDE DE L'AUTO PAR DEFAUT ICI

                    goto STOPPERZPL;
                }
            }
            delay_ms(200);
            TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE
            if (DEMANDEZPLIO == 2) {
                delay_ms(200); //27102020
                TRAMEOUT[3] = 'A';
                TRAMEOUT[4] = 'U';
                TRAMEOUT[5] = 'I';
                TRAMEOUT[6] = 'N';
                TRAMEOUT[7] = 'I';
                TRAMEOUT[8] = 'T';
                TRAMEOUT[9] = '*';
                TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE
            }




STOPPERZPL:

            INTCONbits.GIE = 1;


            break;



        case 341: //IO DEMANDE A VOIR L'HEURE

            INTCONbits.GIE = 0; // ON INTERDIT TTE DEMANDES
            cFonctionRecup[0] = 'Z';
            cFonctionRecup[1] = 'O';
            cFonctionRecup[2] = 'R';





            pointeur_out_in = 14; // SAUVEGARDE POINTEUR DU TABLEAU
            controle_CRC;


            if (INT0 == 1) //une demande de l'autmate a ete faite !!!!!!!! pas sur interruption juste touche
            {
                SERIALISATION(); // VOIR SI DEMANDE DE L'AUTO
                if ((ENTREES2 & 0x02) == 0x02) {

                    PRIO_AUTO_IO == 0x00;
                    TEMPO(1);
                    identificationINTER(6, 0x00); // ON EST OK POUR LA DEMANDE DE L'AUTO PAR DEFAUT ICI

                    goto STOPPERZOR;
                }
            }

            //VOIR L'HEURE
            creer_trame_horloge();
            ENVOIE_DES_TRAMES(7, 305, &HORLOGE); //CARTE IO

STOPPERZOR:


            break;













        case 340: //ZPP RECOIT PARAM CMS

            INTCONbits.GIE = 0; // ON INTERDIT TTE DEMANDES
            cFonctionRecup[0] = 'Z';
            cFonctionRecup[1] = 'P';
            cFonctionRecup[2] = 'P';

            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = cFonctionRecup[0];
            TRAMEOUT[6] = cFonctionRecup[1];
            TRAMEOUT[7] = cFonctionRecup[2];



            pointeur_out_in = 120; // SAUVEGARDE POINTEUR DU TABLEAU
            controle_CRC;

            //	recuperer_trame_memoire(0);
            ECRIRE_EEPROMBYTE(6, 1024, &TRAMEIN, 5); //PARAM 1






            INFORMATION_ERREUR[0] = 'F'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = 'F';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }

            ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);

            LIRE_EEPROM(6, 1024, &TRAMEOUT, 5);

            RESETCMS = 1; //RESET CMS

            INTCONbits.GIE = 1;


            break;



        case 329: //CREATION CAPTEUR DEPUIS AUTOMATE OU IO

            INTCONbits.GIE = 0; // ON INTERDIT TTE DEMANDES
            cFonctionRecup[0] = 'Z';
            cFonctionRecup[1] = 'C';
            cFonctionRecup[2] = 'R';

            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = cFonctionRecup[0];
            TRAMEOUT[6] = cFonctionRecup[1];
            TRAMEOUT[7] = cFonctionRecup[2];


            pointeur_out_in = 8; // SAUVEGARDE POINTEUR DU TABLEAU
            controle_CRC;





            // AUTOM ENV DES ' ' au lieu de '0'
            if (TRAMEIN[8] == 0x20)
                TRAMEIN[8] = '0';
            if (TRAMEIN[9] == 0x20)
                TRAMEIN[9] = '0';
            if (TRAMEIN[10] == 0x20)
                TRAMEIN[10] = '0';
            if (TRAMEIN[11] == 0x20)
                TRAMEIN[11] = '0';
            if (TRAMEIN[12] == 0x20)
                TRAMEIN[12] = '0';

            if (TRAMEIN[69] == 0x20)
                TRAMEIN[69] = '0';
            if (TRAMEIN[70] == 0x20)
                TRAMEIN[70] = '0';
            if (TRAMEIN[71] == 0x20)
                TRAMEIN[71] = '0';
            if (TRAMEIN[72] == 0x20)
                TRAMEIN[72] = '0';

            if (TRAMEIN[77] == 0x20)
                TRAMEIN[77] = '0';

            if (TRAMEIN[78] == 0x20)
                TRAMEIN[78] = '0';
            if (TRAMEIN[79] == 0x20)
                TRAMEIN[79] = '0';
            if (TRAMEIN[80] == 0x20)
                TRAMEIN[80] = '0';
            if (TRAMEIN[81] == 0x20)
                TRAMEIN[81] = '0';


            if (TRAMEIN[57] == 0x20)
                TRAMEIN[57] = '0';
            if (TRAMEIN[58] == 0x20)
                TRAMEIN[58] = '0';
            if (TRAMEIN[59] == 0x20)
                TRAMEIN[59] = '0';
            //SEUIL _ _ _ 0
            if (TRAMEIN[88] == 0x20)
                TRAMEIN[88] = '0';
            if (TRAMEIN[89] == 0x20)
                TRAMEIN[89] = '0';
            if (TRAMEIN[90] == 0x20)
                TRAMEIN[90] = '0';
            if (TRAMEIN[91] == 0x20)
                TRAMEIN[91] = '0';

            /////FIN CORRECTION AUTOMATE



            VOIECOD[0] = TRAMEIN[8];
            VOIECOD[1] = TRAMEIN[9];
            VOIECOD[2] = TRAMEIN[10];
            VOIECOD[3] = TRAMEIN[11];
            VOIECOD[4] = TRAMEIN[12];

            etatIOAU = 0;

            if (TRAMEIN[3] == 'D') // CARTE SD
                etatIOAU = 'D';
            if ((TRAMEIN[1] == 'A')&&(TRAMEIN[3] != 'D')) // AUTOMATE ne pas envoyer ERR00 29/03/18
            {
                etatIOAU = 'U';
                TEMPO(1);
                goto AU1;
            }

            if (TRAMEIN[3] != 'D') //PAS CARTE SD
            {
                INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
                INFORMATION_ERREUR[1] = '0';
                for (t = 2; t < 20; t++) {
                    INFORMATION_ERREUR[t] = 'x';
                }
                etatIOAU = 'U';
                if (TRAMEIN[1] == 'I') //CARTE I/O
                {
                    ENVOIE_DES_TRAMES(7, 302, &INFORMATION_ERREUR);
                    etatIOAU = 'I';
                } else
                    ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);

            }

AU1:


            // ICI ON RECOIT TRAME AVEC VERITABLE NUMERO CABLE ex 041 (carte RI 3)
            // PUIS ON TRANSFORME EN NUMERO VOIE RELATIVE A LA CARTE 001

            //		CALCUL DE RI

            if ((VOIECOD[1] == '1') || (VOIECOD[1] == '0'))
                numdecarteRI = 1;



            if ((VOIECOD[1] == '3') || (VOIECOD[1] == '2'))
                numdecarteRI = 2;



            if ((VOIECOD[1] == '5') || (VOIECOD[1] == '4'))
                numdecarteRI = 3;



            if ((VOIECOD[1] == '7') || (VOIECOD[1] == '6'))
                numdecarteRI = 4;


            if ((VOIECOD[1] == '9') || (VOIECOD[1] == '8'))
                numdecarteRI = 5;



            if ((VOIECOD[1] == '2')&&(VOIECOD[2] == '0'))
                numdecarteRI = 1;
            if ((VOIECOD[1] == '4')&&(VOIECOD[2] == '0'))
                numdecarteRI = 2;
            if ((VOIECOD[1] == '6')&&(VOIECOD[2] == '0'))
                numdecarteRI = 3;
            if ((VOIECOD[1] == '8')&&(VOIECOD[2] == '0'))
                numdecarteRI = 4;
            if ((VOIECOD[0] == '1')&&(VOIECOD[1] == '0')&&(VOIECOD[2] == '0'))
                numdecarteRI = 5;


            //ON TRANSFORME L'ABSOLU EN RELATIF
            if (VOIECOD[0] == '1') // voie 100
            {
                VOIECOD[0] = '0';
                VOIECOD[1] = '2'; //100
                VOIECOD[2] = '0';
            } else
                //voie 0xy
            {

                if (((VOIECOD[1] == '2') || (VOIECOD[1] == '4') || (VOIECOD[1] == '6') || (VOIECOD[1] == '8') || (VOIECOD[1] == '0'))&&(VOIECOD[2] != '0')) //on test x si x paire alors ->Voie relative 0
                    VOIECOD[1] = '0';

                if (((VOIECOD[1] == '2') || (VOIECOD[1] == '4') || (VOIECOD[1] == '6') || (VOIECOD[1] == '8') || (VOIECOD[1] == '0'))&&(VOIECOD[2] == '0')) //on test x si x paire alors ->Voie relative 0
                    VOIECOD[1] = '2';

                if ((VOIECOD[1] == '1') || (VOIECOD[1] == '3') || (VOIECOD[1] == '5') || (VOIECOD[1] == '7') || (VOIECOD[1] == '9')) //on test x si x impaire alors ->Voie relative 1 sauf si y est de 0
                    VOIECOD[1] = '1'; //sinon c la voie relative 01y




            }









            // FIN MULTI A
            TRAMEIN[8] = VOIECOD[0];
            TRAMEIN[9] = VOIECOD[1];
            TRAMEIN[10] = VOIECOD[2];

            TRAMEIN[57] = VOIECOD[0];
            TRAMEIN[58] = VOIECOD[1];
            TRAMEIN[59] = VOIECOD[2];



            //			TMP[0]='#';
            //			TMP[1]=TRAMEIN[8];
            //			TMP[2]=TRAMEIN[9];
            //			TMP[3]=TRAMEIN[10];
            //			TMP[4]=TRAMEIN[11];
            //			TMP[5]=TRAMEIN[12];
            //			LIRE_EEPROM(choix_memoire(&TMP),calcul_addmemoire(&TMP),&TMP,1); //LIRE DANS EEPROM
            //			TRAMEIN[82]=TMP[78]; //ON RECUPERE L'ETAT PRECEDENT


            if (etatIOAU != 'I') //PAS CARTE I/O
            {
                // L'AUTO ENVOIE  XXXX
                if (TRAMEIN[68] != '?')
                    TRAMEIN[68] = '?';
                if (TRAMEIN[73] != '?')
                    TRAMEIN[73] = '?';
                if (TRAMEIN[74] != '?')
                    TRAMEIN[74] = '?';
                if (TRAMEIN[75] != '?')
                    TRAMEIN[75] = '?';
                if (TRAMEIN[76] != '?')
                    TRAMEIN[76] = '?';


            }

            //if (etatIOAU=='D') //TRAVAILLE AVEC CARTE SD
            //{

            //if ((PRESENCEDEBITSD==0X00)&&(VOIECOD[3]=='0')&&(VOIECOD[4]=='0'))
            //goto NEPASCREER;

            //}

            ENVOIE_DES_TRAMES(numdecarteRI, 285, &TRAMEIN);

NEPASCREER:
            // ICI IL FAUT RETRANSFORMER EN ABSOLU

            VOIECOD[1] = (VOIECOD[1] - 48) + 2 * (numdecarteRI - 1);
            if (VOIECOD[1] == 10) //VOIE 100
            {
                VOIECOD[0] = '1';
                VOIECOD[1] = '0';
                VOIECOD[2] = '0';
            } else {
                VOIECOD[1] = VOIECOD[1] + 48;
            }


            //TRAMEOUT CONTIENT LES DONNEES
            TRAMEOUT[8] = VOIECOD[0];
            TRAMEOUT[9] = VOIECOD[1];
            TRAMEOUT[10] = VOIECOD[2];

            if (etatIOAU == 'D') //TRAVAILLE AVEC CARTE SD
            {

                if ((PRESENCEDEBITSD == 0X00)&&(VOIECOD[3] == '0')&&(VOIECOD[4] == '0'))
                    goto NEPASVALIDER;

            }

            VALIDER_CAPTEUR_CM(); // ICI ON VALIDE DANS LA TABLE D'EXISTENCE  ET ON ECRIT LES TRAMES EN CM avec la copie du commentaire cable seulement en codage 0
NEPASVALIDER:
            if (etatIOAU != 'D') //SI PAS CARTE SD
            {
                INFORMATION_ERREUR[0] = 'F'; //TT EST BON donc utilisation type d'erreur
                INFORMATION_ERREUR[1] = 'F';
                for (t = 2; t < 20; t++) {
                    INFORMATION_ERREUR[t] = 'x';
                }

                if (etatIOAU == 'I') //CARTE I/O
                {
                    ENVOIE_DES_TRAMES(7, 302, &INFORMATION_ERREUR);
                } else {


                    ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);


                }


            }
            //VALIDER_CAPTEUR_CM();
            INTCONbits.GIE = 1;




            break;








        case 333: //table existence depuis AU OU IO

            INTCONbits.GIE = 0; //AUTORISER LES INTERRUPTIONS GENERALES
            PRIO_AUTO_IO = 0xF1;
            cFonctionRecup[0] = 'Z';
            cFonctionRecup[1] = 'T';
            cFonctionRecup[2] = 'E';

            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = cFonctionRecup[0];
            TRAMEOUT[6] = cFonctionRecup[1];
            TRAMEOUT[7] = cFonctionRecup[2];

            DEMANDE_INFOS_CAPTEURS[0] = TRAMEIN[8]; // QUELLE CARTE
            DEMANDE_INFOS_CAPTEURS[1] = TRAMEIN[9];




            pointeur_out_in = 10; // SAUVEGARDE POINTEUR DU TABLEAU
            calcul_CRC();


            funct = TRAMEIN[9] - 48;
            selectionvoie = 0x00;
            if (TRAMEIN[8] >= 65) //CODE ASCII DE A a xxxxxxx
            {


                selectionvoie = TRAMEIN[8] - 64; //SI 'A' on a 1 en decimal  donc demande d'une voie



            }




            RS485('%'); //% et $
            voieDEM = funct;

            //if (funct==1)
            //{
            t = 0;
            for (t = (1 + 20 * (voieDEM - 1)); t <= (20 * voieDEM); t++) {

                SERIALISATION(); // VOIR SI DEMANDE DE L'AUTO
                if ((ENTREES2 & 0x02) == 0x02) //CAR DANS LE PRG CARTE IO RH5 DEMANDE EST LONGTEMPS HAUT
                    PRIO_AUTO_IO = 0xE1;


                funct = 128 * (t) + 30000;
                LIRE_EEPROM(3, funct, &TMP, 4); //LIRE DANS EEPROM
                delay_ms(50);

                //	if (INT0==1) //une demande de l'autmate a ete faite !!!!!!!! pas sur interruption juste touche
                //	{
                //	SERIALISATION(); // VOIR SI DEMANDE DE L'AUTO
                //	if ((ENTREES2&0x02)==0x02)
                //	{
                //	delay_ms(100);
                //	RS485('$'); //% et $
                //	RS485('$'); //% et $
                //	RS485('$'); //% et $


                //	delay_ms(100);
                //	RS485('$'); //% et $
                //	RS485('$'); //% et $
                //	RS485('$'); //% et $
                //	PRIO_AUTO_IO==0x00;
                //	identificationINTER(6,0x00); // ON EST OK POUR LA DEMANDE DE L'AUTO PAR DEFAUT ICI
                //DRS485_IO=1; //ON STOPPE LES DEMANDES DE LA CA1RTE IO
                //	goto finZTE2;
                //	}
                //	}

                if ((selectionvoie == (t - (20 * (voieDEM - 1)))) || (selectionvoie == 0x00)) //SI DEMANDE UNE VOIE PAR L'AUTO OU TTE LA TABEL
                {
                    if (TRAMEIN[1] == 'I') //CARTE I/O
                    {
                        ENVOIE_DES_TRAMES(7, 333, &TMP);
                    } else {
                        if (TMP[2] == 'r')
                            TMP[4] = 'R';
                        ENVOIE_DES_TRAMES(6, 333, &TMP);

                    }


                }


                //			if (PRIO_AUTO_IO==0xE1) //ON A DETECTER UNE DEMANDE DE L'AU pendant IO
                //			identificationINTER(6,0x00); // ON FORCE MANUELLEMENT en MODE NORMALE POUR VALIDER LACTION DE IO


            }


finZTE:
            RS485('$'); //% et $
            RS485('$'); //% et $
            RS485('$'); //% et $

            delay_ms(100);

            RS485('$'); //% et $
            RS485('$'); //% et $
            RS485('$'); //% et $



            if (PRIO_AUTO_IO == 0xE1) //ON A DETECTER UNE DEMANDE DE L'AU pendant IO
                identificationINTER(6, 0x00); // ON FORCE MANUELLEMENT en MODE NORMALE POUR VALIDER LACTION DE IO


finZTE2:


            //	}
            if (PRIO_AUTO_IO == 0xE1)
                identificationINTER(6, 0x00); // ON EST OK POUR LA DEMANDE DE L'AUTO PAR DEFAUT ICI







            PRIO_AUTO_IO = 0x00;
            INTCONbits.GIE = 1; //AUTORISER LES INTERRUPTIONS GENERALES

            break;



        case 302: //ERR DU RECEPTEUR

            funct = 0;
            //            TYPE_ERREUR[0] = tab[8];
            //            TYPE_ERREUR[1] = tab[9];


            //            funct = (TYPE_ERREUR[1]) - 48;
            //            funct = funct + 10 * (TYPE_ERREUR[0] - 48);   
            funct = (tab[9]) - 48; //les variables de TYPE_ERREUR prennent forcement les valeurs de tab[8] et [9] 
            funct = funct + 10 * (tab[8] - 48);
            //funct = funct + (TYPE_ERREUR[1]);
            t = 0;
            for (t = 0; t < 18; t++) {
                INFORMATION_ERREUR[t] = tab[t + 10];
            }

            pointeur_out_in = 27;
            controle_CRC();


            switch (funct) //ACTION A FAIRE POUR CHAQUE ERREUR ......
 {
                case 0: // OK CA C EST BIEN PASSEE

                    //delay_ms(100);
                    break;



                case 1: //carte occup�e



                    break;


                case 2: //erreur CRC


                    break;


                case 3: //voies d�fectueuses


                    break;








                default:


                    break;



            }





        case 343: //DEMANDE AU OU IO DES DONNEES DIC capteurs

            INTCONbits.GIE = 0;
            //DRS485_IO=0; //ON STOPPE LES DEMANDES DE LA CA1RTE IO
            //DRS485_IO=0; //ON STOPPE LES DEMANDES DE LA CA1RTE IO
            cFonctionRecup[0] = 'Z';
            cFonctionRecup[1] = 'I';
            cFonctionRecup[2] = 'Z';
            PRIO_AUTO_IO = 0xCC;
            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = cFonctionRecup[0];
            TRAMEOUT[6] = cFonctionRecup[1];
            TRAMEOUT[7] = cFonctionRecup[2];

            MAXBITE = 0;



            //CORRECTION AU
            if (TRAMEIN[8] == 0x20)
                TRAMEIN[8] = '0';
            if (TRAMEIN[9] == 0x20)
                TRAMEIN[9] = '0';
            if (TRAMEIN[10] == 0x20)
                TRAMEIN[10] = '0';
            if (TRAMEIN[11] == 0x20)
                TRAMEIN[11] = '0';
            if (TRAMEIN[12] == 0x20)
                TRAMEIN[12] = '0';

            if (TRAMEIN[14] == 0x20)
                TRAMEIN[14] = '0';
            if (TRAMEIN[15] == 0x20)
                TRAMEIN[15] = '0';
            if (TRAMEIN[16] == 0x20)
                TRAMEIN[16] = '0';
            if (TRAMEIN[17] == 0x20)
                TRAMEIN[17] = '0';
            if (TRAMEIN[18] == 0x20)
                TRAMEIN[18] = '0';
            // FIN AUTO


            temoinfinvoieZIZ = 0;

            DEMANDE_INFOS_CAPTEURS[0] = TRAMEIN[8];
            DEMANDE_INFOS_CAPTEURS[1] = TRAMEIN[9];
            DEMANDE_INFOS_CAPTEURS[2] = TRAMEIN[10]; //VOIE DEPART
            DEMANDE_INFOS_CAPTEURS[3] = TRAMEIN[11];
            DEMANDE_INFOS_CAPTEURS[4] = TRAMEIN[12]; // CODAGE DEPART

            DEMANDE_INFOS_CAPTEURS[5] = TRAMEIN[14];
            DEMANDE_INFOS_CAPTEURS[6] = TRAMEIN[15];
            DEMANDE_INFOS_CAPTEURS[7] = TRAMEIN[16]; //VOIE FINALE
            DEMANDE_INFOS_CAPTEURS[8] = TRAMEIN[17];
            DEMANDE_INFOS_CAPTEURS[9] = TRAMEIN[18]; // CODAGE FINALE
            TYPECOMMANDE = TRAMEIN[13];

            voieDEM = (100 * (DEMANDE_INFOS_CAPTEURS[0] - 48))+(10 * (DEMANDE_INFOS_CAPTEURS[1] - 48))+(DEMANDE_INFOS_CAPTEURS[2] - 48);
            codageDEM = (10 * (DEMANDE_INFOS_CAPTEURS[3] - 48))+(DEMANDE_INFOS_CAPTEURS[4] - 48);
            voieDEMFINAL = (100 * (DEMANDE_INFOS_CAPTEURS[5] - 48))+(10 * (DEMANDE_INFOS_CAPTEURS[6] - 48))+(DEMANDE_INFOS_CAPTEURS[7] - 48);
            codageDEMFINAL = (10 * (DEMANDE_INFOS_CAPTEURS[8] - 48))+(DEMANDE_INFOS_CAPTEURS[9] - 48);

            AVECEXIST = 0x00;
            encoreunefois = 0x00;
            if ((ID1[0] == 'A')&&(ID1[1] == 'U')) {
                AVECEXIST = 0xff; //EXISTENCE CABLE uniquement TP
            }

            if ((voieDEM == voieDEMFINAL)&&(codageDEM == codageDEMFINAL)) //SI ON DEMANDE UN CABLE
            {
                encoreunefois = 0xFF;
                AVECEXIST = 0xff; //EXISTENCE CABLE
                if (TRAMEIN[13] == '+') //DEBIMETRE
                    AVECEXIST = 0xdd; //EXISTENCE DEBITMETRE
            }
            delay_ms(200);

            RS485('%');
            //	 CHERCHER_DONNNEES_CABLES(&VOIECOD,111);


refaire:

            funct = 128 * voieDEM + 30000;
            LIRE_EEPROM(3, funct, &cSAVTABRecup, 4); //LIRE DANS EEPROM lexistance de toutes le voies


CONTINUER:


            TMP[0] = cDebutTrame;
            TMP[1] = DEMANDE_INFOS_CAPTEURS[0];
            TMP[2] = DEMANDE_INFOS_CAPTEURS[1];
            TMP[3] = DEMANDE_INFOS_CAPTEURS[2];
            TMP[4] = DEMANDE_INFOS_CAPTEURS[3];
            TMP[5] = DEMANDE_INFOS_CAPTEURS[4];



            //////////////////
            if (TYPECOMMANDE == '/') {

                if (voieDEM > (voieDEMFINAL)) //R1 R2 ....toujours jusqua la fin	//	if (voieDEM>(voieDEMFINAL+20)) //R1 R2 ....toujours jusqua la fin
                    goto STOPPERZIZ0;



                if (cSAVTABRecup[2] == 'r') //Voie RESISTIVE
                {
                    if (codageDEM == 0) //envoyer le codage 0
                        goto continuer0;
                    if (codageDEM == 1) //envoyer le codage 1
                        goto continuer0;
                }

                if (cSAVTABRecup[2] == 'a') //Voie ADRESSABLE
                {
                    if (codageDEM == 0) //envoyer le codage 0
                        goto continuer0;
                    if (cSAVTABRecup[codageDEM + 3] == 'A') //envoyer le codage
                        goto continuer0;
                }

                if (cSAVTABRecup[2] == 'i') //VOIE INCIDENTE
                {
                    goto continuer0; // ENVOYER TOUT
                }


                if (cSAVTABRecup[2] == 'c') //VOIE CC
                {
                    goto continuer0; // ENVOYER TOUT
                }

                if (cSAVTABRecup[2] == 'h') //FUS
                {
                    goto continuer0; // ENVOYER TOUT
                }




                codageDEM = codageDEM + 1;

                if (codageDEM == 17) {
                    codageDEM = 0;
                    voieDEM = voieDEM + 1;
                }


                IntToChar(voieDEM, &TMP, 3);
                DEMANDE_INFOS_CAPTEURS[0] = TMP[0];
                DEMANDE_INFOS_CAPTEURS[1] = TMP[1];
                DEMANDE_INFOS_CAPTEURS[2] = TMP[2];



                IntToChar(codageDEM, &TMP, 2);
                DEMANDE_INFOS_CAPTEURS[3] = TMP[0];
                DEMANDE_INFOS_CAPTEURS[4] = TMP[1];


                if ((voieDEM < (voieDEMFINAL + 1))) //if ((voieDEM<(voieDEMFINAL+21)))
                    goto refaire;
                else
                    goto STOPPERZIZ0;
            }

continuer0:

            if (INT0 == 1)
                MAXBITE = 1;


            ///////////////////

            LIRE_EEPROM(choix_memoire(&TMP), calcul_addmemoire(&TMP), &TRAMEIN, 1); //LIRE DANS EEPROM
            //ICI

            cFonctionRecup[0] = 'Z';
            cFonctionRecup[1] = 'I';
            cFonctionRecup[2] = 'Z';

            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = cFonctionRecup[0];
            TRAMEOUT[6] = cFonctionRecup[1];
            TRAMEOUT[7] = cFonctionRecup[2];



            TRANSFORMER_TRAME_MEM_EN_COM(); //le pointeur est memoris� dans ce sous prg
            //pointeur_out_in=85;          // SAUVEGARDE POINTEUR DU TABLEAU


            //CRC
            //TRAMEOUT[92]='2';
            //TRAMEOUT[93]='2';
            //TRAMEOUT[94]='2';
            //TRAMEOUT[95]='2';
            calcul_CRC(); // CALCUL LE CRC ET LE RAJOUTE AU TABLEAU TRAMEOUT[]


            if (AVECEXIST == 0xff) {
                if (codageDEM == 0) //CABLE
                    CHERCHER_DONNNEES_CABLES(&DEMANDE_INFOS_CAPTEURS, 111); //VOIR EXITENCE CABLE !!!!!!!
                if (codageDEM > 0) //CAPTEUR
                    CHERCHER_DONNNEES_CABLES(&DEMANDE_INFOS_CAPTEURS, codageDEM); //VOIR EXITENCE CAPTEUR !!!!!!!

                NUMEROCARTE = TMP[1]; //NUMERO DE LA VOIE
                if ((NUMEROCARTE >= 1)&&(NUMEROCARTE <= 20))
                    NUMEROCARTE = 1;
                if ((NUMEROCARTE >= 21)&&(NUMEROCARTE <= 40))
                    NUMEROCARTE = 2;
                if ((NUMEROCARTE >= 41)&&(NUMEROCARTE <= 60))
                    NUMEROCARTE = 3;
                if ((NUMEROCARTE >= 61)&&(NUMEROCARTE <= 80))
                    NUMEROCARTE = 4;
                if ((NUMEROCARTE >= 81)&&(NUMEROCARTE <= 100))
                    NUMEROCARTE = 5;


                ETATZIZCRATIONAUTOMATE = ETAT; //L'ETAT EST EN MEMOIRE
                if (capt_exist == 0x00) {
                    ETATZIZCRATIONAUTOMATE = 'F'; //L'ETAT NEXISTE PAS
                    for (funct = 8; funct < 96; funct++) {
                        TRAMEOUT[funct] = 'F'; //ON EFFACE TT
                    }
                    if ((TYPECARTE[NUMEROCARTE - 1] == 'M')&&(VOIECOD[4] == '1')) //SI VOIE RESISTIVE VIDE !!!!!!! pour AU
                        TRAMEOUT[13] = 'R';
                    if ((TYPECARTE[NUMEROCARTE - 1] == 'E')&&(VOIECOD[4] == '1')) //SI VOIE RESISTIVE VIDE !!!!!!! pour AU
                        TRAMEOUT[13] = 'A';
                    TRAMEOUT[96] = '&'; //POUR L'AUTOMATE QUI SERA QUE LE CABLE OU CAPTEUR NEXISTE PAS !!!!
                    TRAMEOUT[97] = '*'; //FIN
                }
                goto capteursZIZET;
            }


            if (AVECEXIST == 0xdd) {

                CHERCHER_DONNNEES_CABLES(&DEMANDE_INFOS_CAPTEURS, 0); //VOIR EXITENCE DEBITMETRE !!!!!!!


                if (capt_exist == 0x00) {

                    TRAMEOUT[96] = '&'; //POUR L'AUTOMATE QUI SERA QUE LE DEBITMETRE NEXISTE PAS !!!!

                }

            }










            TRAMEOUT[pointeur_out_in + 5] = cFinTrame; // RAJOUT LE BYTE DE STOP


            TRAMEOUT[8] = TMP[1]; //PREFERABLE CAR SI MEMOIRE VIDE ....
            TRAMEOUT[9] = TMP[2];
            TRAMEOUT[10] = TMP[3];
            TRAMEOUT[11] = TMP[4];
            TRAMEOUT[12] = TMP[5];
capteursZIZET:

            //	if (TRAMEOUT[9]>='8') //CARTE 5
            //	{
            delay_ms(100); // modif 10/08/17 ATTENDRE CAR LA CARTE MERE ECRIT DANS EEPROOM 5ms
            delay_ms(100);
            //goto finATTENTEZIZ;

            funct = 0;

finATTENTEZIZ:

            TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE
            if ((ID1[0] == 'A')&&(ID1[1] == 'U'))
                TRAMERECEPTIONRS485DONNEES(2);
            else
                TRAMERECEPTIONRS485DONNEES(15); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE ICI AVEC TIMER  //AVEC ERR00 avec timer reglable 60 origine

            if (TRAMEIN[2] == '$')
                NORECEPTION = 0xFF; //CAS PARTICULIER AVEC COMIO MODEM

            if (NORECEPTION == 0xFF) // SI UNE NON RECEPTION ON ARRETE SANS DOUTE LE MINITEL !!!! IO
            {

                if ((ID1[0] == 'A')&&(ID1[1] == 'U')&&(funct < 3)) {
                    funct++;
                    goto finATTENTEZIZ;
                }
                funct++;
                if (funct >= 1) {

                    goto STOPPERZIZ0;


                } else
                    goto finATTENTEZIZ;
            }

            funct = 0;
            if ((TRAMEIN[8] == 'A')&&(TRAMEIN[9] == 'A')) //CARTE IO DEMANDE DE STOPPER CONTROLER SI CA N'EST PAS L'AUTO ??????????????qui est prioritaire
                goto STOPPERZIZ;

            if (MAXBITE == 1) //une demande de l'autmate a ete faite !!!!!!!! pas sur interruption juste touche
            {
                MAXBITE = 0;
                SERIALISATION(); // VOIR SI DEMANDE DE L'AUTO
                if ((ENTREES2 & 0x02) == 0x02) {
                    RS485('$'); //FIN DE SUPERTRAME
                    RS485('$'); //FIN DE SUPERTRAME
                    RS485('$'); //FIN DE SUPERTRAME
                    RS485('A'); //FIN DE SUPERTRAME
                    RS485('U'); //FIN DE SUPERTRAME	
                    //DRS485_IO=1; //ON STOPPE LES DEMANDES DE LA CA1RTE IO
                    //DRS485_IO=1; //ON STOPPE LES DEMANDES DE LA CA1RTE IO
                    PRIO_AUTO_IO == 0x00;
                    identificationINTER(6, 0x00); // ON EST OK POUR LA DEMANDE DE L'AUTO PAR DEFAUT ICI
                    goto STOPPERZIZ;
                }





            }

            codageDEM = codageDEM + 1;

            if (codageDEM == 17) {
                temoinfinvoieZIZ = 1;
                codageDEM = 0;
                voieDEM = voieDEM + 1;
            }



            IntToChar(voieDEM, &TMP, 3);
            DEMANDE_INFOS_CAPTEURS[0] = TMP[0];
            DEMANDE_INFOS_CAPTEURS[1] = TMP[1];
            DEMANDE_INFOS_CAPTEURS[2] = TMP[2];



            IntToChar(codageDEM, &TMP, 2);
            DEMANDE_INFOS_CAPTEURS[3] = TMP[0];
            DEMANDE_INFOS_CAPTEURS[4] = TMP[1];

            if (((codageDEM == codageDEMFINAL)&&(voieDEM == voieDEMFINAL))&&(encoreunefois != 0xFF)) {

                encoreunefois = 0xFF;




                goto CONTINUER;
            }






            if (encoreunefois == 0x00)//SI PAS FIN
            {


                if (TYPECOMMANDE == '/') {
                    if (temoinfinvoieZIZ == 1) {
                        temoinfinvoieZIZ = 0;
                        goto refaire;
                    }
                }
                goto CONTINUER;
            }

            delay_ms(300); // ATTENDRE CAR LA CARTE MERE ECRIT DANS EEPROOM 5ms
            RS485('$'); //FIN DE SUPERTRAME
            RS485('$'); //FIN DE SUPERTRAME
            RS485('$'); //FIN DE SUPERTRAME

            delay_ms(300); // ATTENDRE CAR LA CARTE MERE ECRIT DANS EEPROOM 5ms
STOPPERZIZ0:
            RS485('$'); //FIN DE SUPERTRAME
            RS485('$'); //FIN DE SUPERTRAME
            RS485('$'); //FIN DE SUPERTRAME

STOPPERZIZ:
            //TEMPO(2);
            DRS485_IO = 0;
            DRS485_IO = 0; // ON REAUTORISE



            INTCONbits.GIE = 1;



            break;



        case 320: //EFFACER CAPTEURS DEPUIS AU OU IO

            INTCONbits.GIE = 0;
            cFonctionRecup[0] = 'Z';
            cFonctionRecup[1] = 'F';
            cFonctionRecup[2] = 'F';

            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = cFonctionRecup[0];
            TRAMEOUT[6] = cFonctionRecup[1];
            TRAMEOUT[7] = cFonctionRecup[2];

            //CORRECTION AU
            if (TRAMEIN[8] == 0x20)
                TRAMEIN[8] = '0';
            if (TRAMEIN[9] == 0x20)
                TRAMEIN[9] = '0';
            if (TRAMEIN[10] == 0x20)
                TRAMEIN[10] = '0';
            if (TRAMEIN[11] == 0x20)
                TRAMEIN[11] = '0';
            if (TRAMEIN[12] == 0x20)
                TRAMEIN[12] = '0';










            DEMANDE_INFOS_CAPTEURS[0] = TRAMEIN[8];
            DEMANDE_INFOS_CAPTEURS[1] = TRAMEIN[9];
            DEMANDE_INFOS_CAPTEURS[2] = TRAMEIN[10];
            DEMANDE_INFOS_CAPTEURS[3] = TRAMEIN[11];
            DEMANDE_INFOS_CAPTEURS[4] = TRAMEIN[12];

            pointeur_out_in = 8; // SAUVEGARDE POINTEUR DU TABLEAU
            calcul_CRC();
            if (TRAMEIN[1] == 'A') //AUTOMATE PAS DE ERR00
            {
                etatIOAU = 'U';
                TEMPO(2);
                goto AU3;
            } else {

                INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
                INFORMATION_ERREUR[1] = '0';
                for (t = 2; t < 20; t++) {
                    INFORMATION_ERREUR[t] = 'x';
                }

                etatIOAU = 'U';
                if (TRAMEIN[1] == 'I') //CARTE I/O
                {
                    ENVOIE_DES_TRAMES(7, 302, &INFORMATION_ERREUR);
                    etatIOAU = 'I';
                } else
                    ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);

            }

AU3:




            //METTRE EN RELATIF
            VOIECOD[0] = DEMANDE_INFOS_CAPTEURS[0];
            VOIECOD[1] = DEMANDE_INFOS_CAPTEURS[1];
            VOIECOD[2] = DEMANDE_INFOS_CAPTEURS[2];
            VOIECOD[3] = DEMANDE_INFOS_CAPTEURS[3];
            VOIECOD[4] = DEMANDE_INFOS_CAPTEURS[4];








            //ON TRANSFORME L'ABSOLU EN RELATIF
            if (VOIECOD[0] == '1') // voie 100
            {
                VOIECOD[0] == '0';
                VOIECOD[1] = '2'; //100
                VOIECOD[2] = '0';
            } else
                //voie 0xy
            {

                if (((VOIECOD[1] == '2') || (VOIECOD[1] == '4') || (VOIECOD[1] == '6') || (VOIECOD[1] == '8')) || (VOIECOD[1] == '0')&&(VOIECOD[2] != '0')) //on test x si x paire alors ->Voie relative 0
                    VOIECOD[1] = '0';

                if (((VOIECOD[1] == '2') || (VOIECOD[1] == '4') || (VOIECOD[1] == '6') || (VOIECOD[1] == '8') || (VOIECOD[1] == '0'))&&(VOIECOD[2] == '0')) //on test x si x paire alors ->Voie relative 0
                    VOIECOD[1] = '2';

                if ((VOIECOD[1] == '1') || (VOIECOD[1] == '3') || (VOIECOD[1] == '5') || (VOIECOD[1] == '7') || (VOIECOD[1] == '9')) //on test x si x impaire alors ->Voie relative 1 sauf si y est de 0
                    VOIECOD[1] = '1'; //sinon c la voie relative 01y




            }

            if (EFFACER_CAPTEUR_SUR_CM(0) == 1) // VOIR SI C'EST LE SEUL CAPTEUR SANS EFFACER SI OUI ENVOYER #AUCMZFF04116F22767* voie effac�e
            {
                VOIECOD[5] = 'F';
                VOIECOD[5] = 'F';
            } else {
                VOIECOD[5] = 'N'; //SINON ENVOYER #AUCMZFF04116N22767*
            }


            ENVOIE_DES_TRAMES(trouver_carte_suivantvoie(&DEMANDE_INFOS_CAPTEURS), 278, &VOIECOD); //sans # 04116 voie 041 codage 16


            INFORMATION_ERREUR[0] = 'F'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = 'F';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }

            if (etatIOAU == 'I') //CARTE I/O

                ENVOIE_DES_TRAMES(7, 302, &INFORMATION_ERREUR);
            else
                ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);









            EFFACER_CAPTEUR_SUR_CM(1); // EFFACER CAPTEUR REELLEMENT
            PRIO_AUTO_IO == 0x00;
            INTCONbits.GIE = 1;

            break;



            break;

        case 334: //REGLER HORLOGE DEPUIS AU OU IO ZRH

            INTCONbits.GIE = 0;

            cFonctionRecup[0] = 'Z';
            cFonctionRecup[1] = 'R';
            cFonctionRecup[2] = 'H';

            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = cFonctionRecup[0];
            TRAMEOUT[6] = cFonctionRecup[1];
            TRAMEOUT[7] = cFonctionRecup[2];

            HORLOGE[0] = TRAMEIN[8];
            HORLOGE[1] = TRAMEIN[9];
            HORLOGE[2] = TRAMEIN[10];
            HORLOGE[3] = TRAMEIN[11];
            HORLOGE[4] = TRAMEIN[12];
            HORLOGE[5] = TRAMEIN[13];
            HORLOGE[6] = TRAMEIN[14];
            HORLOGE[7] = TRAMEIN[15];
            HORLOGE[8] = TRAMEIN[16];
            HORLOGE[9] = TRAMEIN[17];
            HORLOGE[10] = TRAMEIN[18];
            HORLOGE[11] = TRAMEIN[19];




            delay_ms(50);

            pointeur_out_in = 20; // SAUVEGARDE POINTEUR DU TABLEAU
            calcul_CRC();

            INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = '0';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            //		ENVOIE_DES_TRAMES (6,302,&INFORMATION_ERREUR);




            entrer_trame_horloge(); //ECRIRE HORLOGE DANS CM EN EEPROM
            //	reglerhorlogeautres ();
            //	creer_trame_horloge();

            INFORMATION_ERREUR[0] = 'F'; //ON DIT A L'AUTOMATE QUE L'OPERATION EST TERMINEE
            INFORMATION_ERREUR[1] = 'F';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            pic_eeprom_write(0x05, 0xAA); //RESET RI A ZERO
            pic_eeprom_write(0x04, 0xAA);
            pic_eeprom_write(0x03, 0xAA);
            pic_eeprom_write(0x02, 0xAA);
            pic_eeprom_write(0x01, 0xAA);
            pic_eeprom_write(0x20, 0xFF); //RAZ COMPTEUR
            pic_eeprom_write(0x21, 0xFF); //RAZ COMPTEUR
            pic_eeprom_write(0x22, 0xFF); //RAZ COMPTEUR
            pic_eeprom_write(0x23, 0xFF);
            pic_eeprom_write(0x24, 0xFF);
            pic_eeprom_write(0x25, 0xFF);

            if (ID1[0] == 'A') //AUTO
                ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);
            if (ID1[0] == 'I') //IO
                ENVOIE_DES_TRAMES(7, 302, &INFORMATION_ERREUR);
            TEMPO(2);
            if (ID2[0] != 'I') //SI PAS CARTE IO
            {
                RESETCMS = 1; //RESET CMS
            }
            INTCONbits.GIE = 1;


            break;





        case 328:

            INTCONbits.GIE = 0;
            cFonctionRecup[0] = 'Z';
            cFonctionRecup[1] = 'B';
            cFonctionRecup[2] = 'R';
            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = cFonctionRecup[0];
            TRAMEOUT[6] = cFonctionRecup[1];
            TRAMEOUT[7] = cFonctionRecup[2];

            if ((TRAMEIN[8] == '9')&&(TRAMEIN[8] == '9'))
                goto TOUS;
            TRAMEIN[20] = 10 * (TRAMEIN[8] - 48);
            TRAMEIN[21] = TRAMEIN[9] - 48;
            iCalculBLOCRecup = TRAMEIN[20] + TRAMEIN[21];
            iCalculBLOCRecup = 2048 + (iCalculBLOCRecup - 1)*128;
            TEMPO(1);
            LIRE_EEPROM(6, iCalculBLOCRecup, &TRAMEOUT, 6); //LIRE 85caract
            TRAMEENVOIRS485DONNEES(); // ENVOI A L'IO;
            goto UN;

TOUS:



            for (t = 1; t < 22; t++) {
                iCalculBLOCRecup = 2048 + (t - 1)*128;
                //delay_ms(100);
                LIRE_EEPROM(6, iCalculBLOCRecup, &TRAMEOUT, 6); //LIRE 85caract
                TRAMEOUT[1] = 'C';
                TRAMEOUT[2] = 'M';
                TRAMEOUT[3] = 'I';
                TRAMEOUT[4] = 'O';
                // SI MEMOIRE VIDE
                if ((TRAMEOUT[5] != 'Z')&&(TRAMEOUT[6] != 'B')) //MEMOIRE VIDE
                {
                    TRAMEOUT[0] = cDebutTrame;
                    TRAMEOUT[8] = '0';
                    TRAMEOUT[9] = '1';
                    TRAMEOUT[97] = '*';
                }

                TRAMEENVOIRS485DONNEES(); // ENVOI A L'IO;


                delay_ms(100);
                TRAMERECEPTIONRS485DONNEES(18); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
                if ((TRAMEIN[8] == 'F')&&(TRAMEIN[9] == 'F')) {
                    t = t - 1;
                    if (t <= 1)
                        t = 1;
                }


            }

UN:

            MAJBLOC = 'N';

            INTCONbits.GIE = 1;
            break;











        case 313: //Envoi BLOC NOTES DE LAU
            INTCONbits.GIE = 0;
            cFonctionRecup[0] = 'Z';
            cFonctionRecup[1] = 'B';
            cFonctionRecup[2] = 'C';
            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = cFonctionRecup[0];
            TRAMEOUT[6] = cFonctionRecup[1];
            TRAMEOUT[7] = cFonctionRecup[2];



            for (funct = 1; funct < 22; funct++) {

                TRAMEIN[100] = 10 * (TRAMEIN[8] - 48);
                TRAMEIN[101] = (TRAMEIN[9] - 48);
                iCalculBLOCRecup = TRAMEIN[100] + TRAMEIN[101];

                if (TRAMEIN[0] != cDebutTrame) {
                    funct = funct - 1;
                    INFORMATION_ERREUR[0] = 'F'; //TT EST BON donc utilisation type d'erreur
                    INFORMATION_ERREUR[1] = 'F';
                    goto erreurBZ;
                }

                if ((iCalculBLOCRecup > 7)&& (iCalculBLOCRecup < 11)) {
                    funct = funct - 1;
                    INFORMATION_ERREUR[0] = 'F'; //TT EST BON donc utilisation type d'erreur
                    INFORMATION_ERREUR[1] = 'F';
                    goto erreurBZ;
                }
                if ((iCalculBLOCRecup > 17)&& (iCalculBLOCRecup < 21)) {
                    funct = funct - 1;
                    INFORMATION_ERREUR[0] = 'F'; //TT EST BON donc utilisation type d'erreur
                    INFORMATION_ERREUR[1] = 'F';
                    goto erreurBZ;
                }
                if ((iCalculBLOCRecup < 1)&& (iCalculBLOCRecup > 27)) {
                    funct = funct - 1;
                    INFORMATION_ERREUR[0] = 'F'; //TT EST BON donc utilisation type d'erreur
                    INFORMATION_ERREUR[1] = 'F';
                    goto erreurBZ;
                }
                if (funct <= 0)
                    goto finBZ;

                if ((iCalculBLOCRecup > 7)&&(iCalculBLOCRecup <= 17))
                    iCalculBLOCRecup = iCalculBLOCRecup - 3;
                if ((iCalculBLOCRecup > 17)&&(iCalculBLOCRecup <= 27))
                    iCalculBLOCRecup = iCalculBLOCRecup - 6;
                TRAMEOUT[120] = iCalculBLOCRecup + 48;
                iCalculBLOCRecup = 2048 + (iCalculBLOCRecup - 1)*128;


                //TRAMERECEPTIONRS485DONNEES(18); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
                //controle_CRC();
                //TEMPO(1);
                ECRIRE_EEPROMBYTE(6, iCalculBLOCRecup, &TRAMEIN, 6); //ECRITURE EN EEPROM LIGNE 1 85 caract



                INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
                INFORMATION_ERREUR[1] = '0';
erreurBZ:
                //TEMPO(1);
                //		   LIRE_EEPROM(6,iCalculBLOCRecup, &TRAMEOUT, 6); //LIRE 85caract  

                for (t = 2; t < 20; t++) {
                    INFORMATION_ERREUR[t] = 'x';
                }
                //ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);

                //t=0;

                delay_ms(100);
                ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);


                TRAMERECEPTIONRS485DONNEES(18); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE



            }
            MAJBLOC = 'Y';
            SDWBN();
            INFORMATION_ERREUR[0] = 'C'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = 'C';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            //ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);

            //t=0;

            delay_ms(100);
            ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);



finBZ:
            INTCONbits.GIE = 1;

            break;


        case 324: //DEMANDE GROUPE IO OU (cartes type)
            INTCONbits.GIE = 0;
            PRIO_AUTO_IO = 0x22;
            cFonctionRecup[0] = 'Z';
            cFonctionRecup[1] = 'I';
            cFonctionRecup[2] = 'G';

            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = cFonctionRecup[0];
            TRAMEOUT[6] = cFonctionRecup[1];
            TRAMEOUT[7] = cFonctionRecup[2];

            if ((ID1[0] == 'I')&&(ID1[1] == 'O'))
                DEMANDEZIGIO = 0xFF;

            IDENTIF_TYPE_CARTE();
            for (t = 0; t < 3; t++) {
                RECHERCHE_DONNEES_GROUPE(t);
                for (funct = 9; funct < 125; funct++) {
                    TRAMEOUT[funct - 1] = TMP[funct - 1];

                }



                //ON RAJOUTE dans la 3ieme trames LES TYPE DE CARTES pour L'IO	a la fin E ou M ou V
                //#CMIOZIGxxxxxxxxxxx009070000000003000001300010070000000003000001300xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxEVEVV22222*
                if (t == 2) {


                    TRAMEOUT[70] = MAJBLOC;

                    funct = pic_eeprom_read(0x20);
                    if (funct != 100) //'d'
                    {
                        IntToChar(funct + 1, CRC, 2); //voir la valeur	NB RESET,CRCcal,5);
                        TRAMEOUT[97] = CRC[0];
                        TRAMEOUT[98] = CRC[1];
                    } else {
                        TRAMEOUT[97] = 'd';
                        TRAMEOUT[98] = '0';
                    }

                    funct = pic_eeprom_read(0x21);
                    if (funct != 100) //'d'
                    {
                        IntToChar(funct + 1, CRC, 2); //voir la valeur	NB RESET,CRCcal,5);
                        TRAMEOUT[99] = CRC[0];
                        TRAMEOUT[100] = CRC[1];
                    } else {
                        TRAMEOUT[99] = 'd';
                        TRAMEOUT[100] = '1';
                    }

                    funct = pic_eeprom_read(0x22);
                    if (funct != 100) //'d'
                    {
                        IntToChar(funct + 1, CRC, 2); //voir la valeur	NB RESET,CRCcal,5);
                        TRAMEOUT[101] = CRC[0];
                        TRAMEOUT[102] = CRC[1];
                    } else {
                        TRAMEOUT[101] = 'd';
                        TRAMEOUT[102] = '2';
                    }

                    funct = pic_eeprom_read(0x23);
                    if (funct != 100) //'d'
                    {
                        IntToChar(funct + 1, CRC, 2); //voir la valeur	NB RESET,CRCcal,5);
                        TRAMEOUT[103] = CRC[0];
                        TRAMEOUT[104] = CRC[1];
                    } else {
                        TRAMEOUT[103] = 'd';
                        TRAMEOUT[104] = '3';
                    }

                    funct = pic_eeprom_read(0x24);
                    if (funct != 100) //'d'
                    {
                        IntToChar(funct + 1, CRC, 2); //voir la valeur	NB RESET,CRCcal,5);
                        TRAMEOUT[105] = CRC[0];
                        TRAMEOUT[106] = CRC[1];
                    } else {
                        TRAMEOUT[105] = 'd';
                        TRAMEOUT[106] = '4';
                    }


                    funct = pic_eeprom_read(0x25);
                    if (funct != 100) //'d'
                    {
                        IntToChar(funct + 1, CRC, 2); //voir la valeur	NB RESET,CRCcal,5);
                        TRAMEOUT[107] = CRC[0];
                        TRAMEOUT[108] = CRC[1];
                    } else {
                        TRAMEOUT[107] = 'd';
                        TRAMEOUT[108] = '5';
                    }


                    TRAMEOUT[110] = TYPECARTE[0]; //ATTENTION IL FAUT QUE LE GROUPE SOIT ENVOYE UNE FOIS AU MOINS !!!! voir si on met un par defaut
                    TRAMEOUT[111] = TYPECARTE[1];
                    TRAMEOUT[112] = TYPECARTE[2];
                    TRAMEOUT[113] = TYPECARTE[3];
                    TRAMEOUT[114] = TYPECARTE[4];
                }


                pointeur_out_in = 115; // SAUVEGARDE POINTEUR DU TABLEAU
                calcul_CRC();

                SERIALISATION(); // VOIR SI DEMANDE DE L'AUTO
                if ((ENTREES2 & 0x02) == 0x02) //CAR DANS LE PRG CARTE IO RH5 DEMANDE EST LONGTEMPS HAUT
                    PRIO_AUTO_IO = 0xE1;

                //delay_ms(50);

                TRAMEENVOIRS485DONNEES(); // ENVOI A L'IO;
                //INTCONbits.GIE= 0; 	//OBLIGATION SUITE A UNE MICRO INTERRUPTION ?????
                TRAMERECEPTIONRS485DONNEES(9); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE ICI ERR00 pour dire OK
                //INTCONbits.GIE= 1;
                controle_CRC();




            }

            if (PRIO_AUTO_IO == 0xE1) {
                PRIO_AUTO_IO == 0x00;
                identificationINTER(6, 0x00);


            }


            PRIO_AUTO_IO == 0x00;
            INTCONbits.GIE = 1;

            break;













        case 316: //DEMANDE DENVOI DES DONNEES GROUPE DEPUIS L'AU

            INTCONbits.GIE = 0;


            cFonctionRecup[0] = 'Z';
            cFonctionRecup[1] = 'A';
            cFonctionRecup[2] = 'G';

            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = cFonctionRecup[0];
            TRAMEOUT[6] = cFonctionRecup[1];
            TRAMEOUT[7] = cFonctionRecup[2];



            pointeur_out_in = 8; // SAUVEGARDE POINTEUR DU TABLEAU
            //calcul_CRC();




            INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = '0';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = '1';
            }
            ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);




            for (funct = 0; funct < 3; funct++) {
                TRAMEIN[120] = 'E';
                TRAMERECEPTIONRS485DONNEES(18); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
                controle_CRC();
                if ((TRAMEIN[1] == 'A')&&(TRAMEIN[2] == 'U')&&(TRAMEIN[3] == 'C')&&(TRAMEIN[4] == 'M')) //UNIQUEMENT LE GROUPE
                {


                    if ((funct == 0)&&(TRAMEIN[19] == '0')&&(TRAMEIN[20] == '0')&&(TRAMEIN[21] == '1')&&(TRAMEIN[120] == '*')) {



                        GROUPE_EN_MEMOIRE(&TRAMEIN, 0); //ECRIRE EN MEMOIRE 6


                    }
                    if ((funct == 1)&&(TRAMEIN[19] == '0')&&(TRAMEIN[20] == '0')&&(TRAMEIN[21] == '4')&&(TRAMEIN[120] == '*')) {
                        GROUPE_EN_MEMOIRE(&TRAMEIN, 1); //ECRIRE EN MEMOIRE 6
                    }

                    if ((funct == 2)&&(TRAMEIN[19] == '0')&&(TRAMEIN[20] == '0')&&(TRAMEIN[21] == '9')&&(TRAMEIN[120] == '*')) {
                        GROUPE_EN_MEMOIRE(&TRAMEIN, 2); //ECRIRE EN MEMOIRE 6
                    }


                }

                INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
                INFORMATION_ERREUR[1] = '0';
                for (t = 2; t < 20; t++) {
                    INFORMATION_ERREUR[t] = TRAMEIN[19 + t];
                }
                ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);




            }


            //INFORMATION_ERREUR[0]='0'; //TT EST BON donc utilisation type d'erreur
            //INFORMATION_ERREUR[1]='0';
            //for (t=2;t<20;t++)
            //{
            //INFORMATION_ERREUR[t]='x';
            //}
            //ENVOIE_DES_TRAMES (6,302,&INFORMATION_ERREUR);

            LIRE_EEPROM(6, 0, &TRAMEOUT, 3);
            delay_ms(100);

            LIRE_EEPROM(6, 128, &TRAMEOUT, 3);
            delay_ms(100);

            LIRE_EEPROM(6, 256, &TRAMEOUT, 3);
            delay_ms(100);



            INTCONbits.GIE = 1
                    ;


            break;



        case 276: //DIC



            //recuperer_trame_memoire(1);

            controle_CRC();


            //LES INFORMATIONS SONT EN MEMOIRE DANS LES VARIABLES



            break;

        case 308: //LIS



            break;


        case 312: //AUTOMATE DEMANDE UNE PAGE fonction ZAC OK

            INTCONbits.GIE = 0;

            //recuperer_trame_memoire(0);
            pointeur_out_in = 12;
            controle_CRC();


            PAGE[0] = cDebutTrame;
            PAGE[1] = TRAMEIN[9];
            PAGE[2] = TRAMEIN[10];
            PAGE[3] = TRAMEIN[11] - 1;
            PAGE[4] = '0';
            PAGE[5] = '0';

            // LA PAGE DEVIENT LA ZONE DE 0-9 exemple P001 devient zone 0

            TMPI = 100 * (PAGE[1] - 48);
            TMPI = TMPI + 10 * (PAGE[2] - 48);
            TMPI = TMPI + (PAGE[3] - 48);


            if (TMPI == 0) //ORIGINE DE LA ZONE PR LA PAGE1 c la voie 0 pour la page002  c'est la voie 10.........page010 c'est la voie 90
            {
                PAGE[1] = '0';
                PAGE[2] = '0';
                PAGE[3] = '1';
            }


            if (TMPI == 1) {
                PAGE[1] = '0';
                PAGE[2] = '1';
                PAGE[3] = '1';
            }


            if (TMPI == 2) {
                PAGE[1] = '0';
                PAGE[2] = '2';
                PAGE[3] = '1';
            }



            if (TMPI == 3) {
                PAGE[1] = '0';
                PAGE[2] = '3';
                PAGE[3] = '1';
            }



            if (TMPI == 4) {
                PAGE[1] = '0';
                PAGE[2] = '4';
                PAGE[3] = '1';
            }


            if (TMPI == 5) {
                PAGE[1] = '0';
                PAGE[2] = '5';
                PAGE[3] = '1';
            }


            if (TMPI == 6) {
                PAGE[1] = '0';
                PAGE[2] = '6';
                PAGE[3] = '1';
            }

            if (TMPI == 7) {
                PAGE[1] = '0';
                PAGE[2] = '7';
                PAGE[3] = '1';
            }


            if (TMPI == 8) {
                PAGE[1] = '0';
                PAGE[2] = '8';
                PAGE[3] = '1';
            }

            if (TMPI == 9) {
                PAGE[1] = '0';
                PAGE[2] = '9';
                PAGE[3] = '1';
            }


            for (t = 1; t < 11; t++) {

                VOIECOD[0] = PAGE[1];
                VOIECOD[1] = PAGE[2];
                VOIECOD[2] = PAGE[3];

                CHERCHER_DONNNEES_CABLES(&VOIECOD, 111); // on regarde si la voie existe



                if ((capt_exist == 1) || (capt_exist == 2) || (capt_exist == 3) || (capt_exist == 4)) {
                    PREPARATION_PAGE_AUTO(); //ON PREPARE TOUTE LA TRAME
                } else {
                    //TEMPO(2); //TEST
                    TRAMEOUT[8] = PAGE[1];
                    TRAMEOUT[9] = PAGE[2];
                    TRAMEOUT[10] = PAGE[3];
                    TRAMEOUT[29] = 'F'; //TYPE F=VIDE  #CMAUZAC045xxxxxxxxxxxxxxxxxxA0050013102FF03FF04FF05FF06FF07FF08FF09FF10FF11FF12FF13FF14FF15FF16FF22222* 'TEST F
                }
                PAGE[4] = '0';
                PAGE[5] = '0';

                pointeur_out_in = 98;
                calcul_CRC(); //COMPLETE LA TRAME TRAMEOUT[] AVEC LE CRC
                TRAMEOUT[pointeur_out_in + 5] = cFinTrame; //COMPLETE LA TRAME TRAMEOUT[] avec STOP
                TRAMEOUT[0] = cDebutTrame;
                TRAMEOUT[1] = ID2[0];
                TRAMEOUT[2] = ID2[1];
                TRAMEOUT[3] = ID1[0];
                TRAMEOUT[4] = ID1[1];

                TRAMEOUT[5] = cFonctionRecup[0];
                TRAMEOUT[6] = cFonctionRecup[1];
                TRAMEOUT[7] = cFonctionRecup[2]; //TRAMEOUT;


                REMISE_EN_FORME_PAGE();
                //TEMPO(2); //ZAC TEMPO
                //delay_ms(200);
                TRAMEENVOIRS485DONNEES(); //ENVOYER LA TRAME
                //TRAMEENVOIRS485DONNEES(); //ENVOYER LA TRAME

                TRAMEIN[3] = 'Q';
                TRAMERECEPTIONRS485DONNEES(2); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE ICI AVEC TIMER 9origine

                if (NORECEPTION == 0xff) //SI PAS DE RECEPTION on refait
                {
                    //TEMPO(1); //ZAC TEMPO
                    //delay_ms(200);
                    TRAMEENVOIRS485DONNEES(); //ENVOYER LA TRAME

                    TRAMEIN[3] = 'Q';
                    TRAMERECEPTIONRS485DONNEES(2); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE ICI AVEC TIMER 9origine


                    if (NORECEPTION == 0xff) {
                        //TEMPO(1); //ZAC TEMPO
                        //delay_ms(200);
                        TRAMEENVOIRS485DONNEES(); //ENVOYER LA TRAME

                        TRAMEIN[3] = 'Q';
                        TRAMERECEPTIONRS485DONNEES(9); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE ICI AVEC TIMER 9origine
                    }

                    if (NORECEPTION == 0xff) {
                        //TEMPO(1); //ZAC TEMPO
                        delay_ms(200);
                        TRAMEENVOIRS485DONNEES(); //ENVOYER LA TRAME

                        TRAMEIN[3] = 'Q';
                        TRAMERECEPTIONRS485DONNEES(9); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE ICI AVEC TIMER 9origine
                    }





                    if (NORECEPTION == 0xff) //SI PAS DE RECEPTION
                        goto finforzac;


                }

                //if (t==3) //TEST
                //FONCTION[2]='C';

                //controle_CRC(); //
                //RECUPERATION_DES_TRAMES (&TRAMEIN); //A METTRE SUR INTERRUPTION void identificationINTER(void)
                cFonctionRecup[0] = 'Z'; //exeption cylce suivant
                cFonctionRecup[1] = 'A';
                cFonctionRecup[2] = 'C';





                //PAGE[4]='0';
                //PAGE[5]='0';
                if (PAGE[3] == '9') //9
                {
                    if (PAGE[2] == '9') {
                        PAGE[3] = '0';
                        PAGE[2] = '0';
                        PAGE[1] = '1';
                    } else {
                        PAGE[3] = '0'; //soit avant le '0'
                        PAGE[2] = PAGE[2] + 1;
                    }
                } else {
                    PAGE[3] = t + 49;
                }

            }
finforzac:

            INFORMATION_ERREUR[0] = 'F'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = 'F';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);



            INTCONbits.GIE = 1;
            funct = 0;

            break;



        case 305: //HOR

            HORLOGE[0] = tab[8];
            HORLOGE[1] = tab[9];
            HORLOGE[2] = tab[10];
            HORLOGE[3] = tab[11];
            HORLOGE[4] = tab[12];
            HORLOGE[5] = tab[13];
            HORLOGE[6] = tab[14];
            HORLOGE[7] = tab[15];


            pointeur_out_in = 16;
            controle_CRC();

            // TOUT EST OK HORLOGE REGLEE




            break;





        case 278: //EFF
            pointeur_out_in = 8;
            controle_CRC();
            break;






        case 280: //DMC ---------->
            pointeur_out_in = 13;
            controle_CRC(); // on controle le CRC

            //PREPARATION DE LA TRAME A ENVOYER
            funct = 0;

            DEMANDE_INFOS_CAPTEURS[0] = tab[8];
            DEMANDE_INFOS_CAPTEURS[1] = tab[9];
            DEMANDE_INFOS_CAPTEURS[2] = tab[10];
            DEMANDE_INFOS_CAPTEURS[3] = tab[11];
            DEMANDE_INFOS_CAPTEURS[4] = tab[12];

            construire_trame_memoire(); // RECUPERATION DES INFORMATIONS DE L'EEPROM DU CAPTEUR



            //RECURRENT
            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = cFonctionRecup[0];
            TRAMEOUT[6] = cFonctionRecup[1];
            TRAMEOUT[7] = cFonctionRecup[2]; //TRAMEOUT;

            //TRAMEOUT=TRAMEOUT
            //construire_trame_memoire (); //attention la variable Pointeur_out est lendroit ou s'arrete le tableau
            calcul_CRC(); //COMPLETE LA TRAME TRAMEOUT[] AVEC LE CRC
            TRAMEOUT[pointeur_out_in + 5] = cFinTrame; //COMPLETE LA TRAME TRAMEOUT[] avec STOP

            //LA TRAME EST PRETE A ETRE ENVOYEE
            //FAIRE DRS485=1 A VOIR LA QUELLE
            TRAMEENVOIRS485DONNEES(); //ENVOYER LA TRAME


            funct = 0;

            //

            break;


        case 287: //DHO


            pointeur_out_in = 8;
            controle_CRC();


            lire_horloge();

            ENVOIE_DES_TRAMES(1, 305, &HORLOGE);



            break;


        case 285: //CRE

            //recuperer_trame_memoire(1);


            controle_CRC();

            // SI TOUT EST OK ALORS CREATION OK
            break;



        case 322: //LANCE LA FONCTION OEIL PAR AUTOMATE ZEI OK


            INTCONbits.GIE = 0;
            //FONCTION[0]='Z'; //exeption cylce suivant
            //FONCTION[1]='E';
            //FONCTION[2]='I';
            pointeur_out_in = 13;
            DEMANDE_INFOS_CAPTEURS[0] = tab[8];
            DEMANDE_INFOS_CAPTEURS[1] = tab[9];
            DEMANDE_INFOS_CAPTEURS[2] = tab[10];
            DEMANDE_INFOS_CAPTEURS[3] = tab[11];
            DEMANDE_INFOS_CAPTEURS[4] = tab[12];



            controle_CRC(); // on controle le CRC

            INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = '0';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);


            VOIECOD[0] = DEMANDE_INFOS_CAPTEURS[0];
            VOIECOD[1] = DEMANDE_INFOS_CAPTEURS[1];
            VOIECOD[2] = DEMANDE_INFOS_CAPTEURS[2];
            VOIECOD[3] = DEMANDE_INFOS_CAPTEURS[3];
            VOIECOD[4] = DEMANDE_INFOS_CAPTEURS[4];

            codageDEM = 10 * (VOIECOD[3] - 48)+(VOIECOD[4] - 48);
            CHERCHER_DONNNEES_CABLES(&VOIECOD, 111); // on regarde si la voie existe
            if (capt_exist != 1) //CABLE diff de R OU A donc h v i c voir fonction
            {
                capt_exist = 10; //PAS DE CABLE QUI EXISTE
            } else //SI cable existe A ou R
            {
                CHERCHER_DONNNEES_CABLES(&DEMANDE_INFOS_CAPTEURS, codageDEM); //VOIR EXITENCE CAPTEUR sauf debit!!!!!!!
                if (capt_exist == 1) //capteur present
                {
                    capt_exist = 11;
                } else {
                    capt_exist = 20; //PAS DE CAPTEUR POUR CE CODAGE
                }
            }
            //ON TRANSFORME L'ABSOLU EN RELATIF
            if (VOIECOD[0] == '1') // voie 100
            {
                VOIECOD[0] == '0';
                VOIECOD[1] = '2'; //100
                VOIECOD[2] = '0';
            } else
                //voie 0xy
            {

                if (((VOIECOD[1] == '2') || (VOIECOD[1] == '4') || (VOIECOD[1] == '6') || (VOIECOD[1] == '8') || (VOIECOD[1] == '0'))&&(VOIECOD[2] != '0')) //on test x si x paire alors ->Voie relative 0
                    VOIECOD[1] = '0';

                if (((VOIECOD[1] == '2') || (VOIECOD[1] == '4') || (VOIECOD[1] == '6') || (VOIECOD[1] == '8') || (VOIECOD[1] == '0'))&&(VOIECOD[2] == '0')) //on test x si x paire alors ->Voie relative 0
                    VOIECOD[1] = '2';

                if ((VOIECOD[1] == '1') || (VOIECOD[1] == '3') || (VOIECOD[1] == '5') || (VOIECOD[1] == '7') || (VOIECOD[1] == '9')) //on test x si x impaire alors ->Voie relative 1 sauf si y est de 0
                    VOIECOD[1] = '1'; //sinon c la voie relative 01y




            }


            ENVOIE_DES_TRAMES(trouver_carte_suivantvoie(&DEMANDE_INFOS_CAPTEURS), 300, &VOIECOD);
            //SI RIEN


            INFORMATION_ERREUR[0] = 'F'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = 'F';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }

            if (TMP[1] == 'I') //CARTE I/O
                ENVOIE_DES_TRAMES(7, 302, &INFORMATION_ERREUR);
            else
                ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);





            INTCONbits.GIE = 1;
            break;





        case 260: //FONCTION INTERNE PR CARTE MERE #CMCMAAA00122767*  avec 001 la carte 1  000 si ttes
            //TRAMEIN[0]='#';//#
            //TRAMEIN[1]='C';
            //TRAMEIN[2]='M';
            //TRAMEIN[3]='C';
            //TRAMEIN[4]='M';
            //TRAMEIN[5]='A';
            //TRAMEIN[6]='A';
            //TRAMEIN[7]='A';
            //TRAMEIN[8]='0';
            //TRAMEIN[9]='0';
            //TRAMEIN[10]='1';
            //RECUPERATION_DES_TRAMES (&TRAMEIN);




            //PREPARATION DE LA TRAME A ENVOYER
            funct = 0;

            TMPI = 100 * (TRAMEIN[8] - 48);
            TMPI = TMPI + 10 * (TRAMEIN[9] - 48);
            TMPI = TMPI + (TRAMEIN[10] - 48);





            //SI FLAG ACCORDSR1 R2 R3 serialisation ou attendre la trame err000?? a choisir


            if ((TMPI == 1) || (TMPI == 0))
                ENVOIE_DES_TRAMES(1, 290, &TRAMEOUT); //RECUPERATION DE LA LISTE
            if ((TMPI == 2) || (TMPI == 0))
                ENVOIE_DES_TRAMES(2, 290, &TRAMEOUT); //RECUPERATION DE LA LISTE
            if ((TMPI == 3) || (TMPI == 0))
                ENVOIE_DES_TRAMES(3, 290, &TRAMEOUT); //RECUPERATION DE LA LISTE
            if ((TMPI == 4) || (TMPI == 0))
                ENVOIE_DES_TRAMES(4, 290, &TRAMEOUT); //RECUPERATION DE LA LISTE
            if ((TMPI == 5) || (TMPI == 0))
                ENVOIE_DES_TRAMES(5, 290, &TRAMEOUT); //RECUPERATION DE LA LISTE





            INTERROGER_TOUT_LES_CAPTEURS(1);


            //EFFACER_LISTE(); //EFFACER LA LISTE DE LA MEMOIRE A VOIR


            break;

        case 345: //ZSR


            INTCONbits.GIE = 0;
            RESCRUTATION = 0;

            pointeur_out_in = 13;
            controle_CRC(); // on controle le CRC


            delay_ms(100);

            INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = '0';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);



            //PREPARATION DE LA TRAME A ENVOYER
            funct = 0;


            if (TRAMEIN[8] == 0x20)
                TRAMEIN[8] = '0';
            if (TRAMEIN[9] == 0x20)
                TRAMEIN[9] = '0';
            if (TRAMEIN[10] == 0x20)
                TRAMEIN[10] = '0';


            TMPI = 100 * (TRAMEIN[8] - 48);
            TMPI = TMPI + 10 * (TRAMEIN[9] - 48);
            TMPI = TMPI + (TRAMEIN[10] - 48);

            INSTRUCTION[0] = TRAMEIN[11];
            INSTRUCTION[1] = TRAMEIN[12];

            DEMANDE_INFOS_CAPTEURS[0] = '0';
            DEMANDE_INFOS_CAPTEURS[1] = '0';
            DEMANDE_INFOS_CAPTEURS[2] = '1';
            DEMANDE_INFOS_CAPTEURS[3] = '0';
            DEMANDE_INFOS_CAPTEURS[4] = '0';
            DEMANDE_INFOS_CAPTEURS[5] = '0';
            DEMANDE_INFOS_CAPTEURS[6] = '2';
            DEMANDE_INFOS_CAPTEURS[7] = '0';
            DEMANDE_INFOS_CAPTEURS[8] = '1';
            DEMANDE_INFOS_CAPTEURS[9] = '6';

            MODERAPID = 0;


            //ENVOYER LA DEMANDE SCRUTATION
            //	if ((TMPI==3)||(TMPI==0))
            //		{
            if ((INSTRUCTION[0] == 'F')&&(INSTRUCTION[1] == 'F')) //TOUTE LES VOIES
            {
                //	ENVOIE_DES_TRAMES (3,315,&INSTRUCTION) ;
                //	ENVOIE_DES_TRAMES (3,276,&DEMANDE_INFOS_CAPTEURS); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE
                //	ENVOIE_DES_TRAMES (3,304,&INSTRUCTION);

                INSTRUCTION[2] = 'F'; //SAV
                INSTRUCTION[3] = 'F';
                INSTRUCTION[0] = '0'; //on part de 01
                INSTRUCTION[1] = 0x2F;
                DEMANDE_INFOS_CAPTEURS[1] = '0'; //1 exemple 99 01 03
                DEMANDE_INFOS_CAPTEURS[2] = '1'; //2
                scrutationunevoie = 0;
            } else //UNIQUEMENT UNE VOIE
            {
                scrutationunevoie = 1;
                DEMANDE_INFOS_CAPTEURS[0] = TRAMEIN[8]; // ICI IL N'Y A PAS LE NUMERO DE CARTE MAIS LE NUMERO DE VOIE !!!!!
                DEMANDE_INFOS_CAPTEURS[1] = TRAMEIN[9]; //1 exemple 99 01 03 100
                DEMANDE_INFOS_CAPTEURS[2] = TRAMEIN[10]; //2

                if ((DEMANDE_INFOS_CAPTEURS[1] == '0')&&(DEMANDE_INFOS_CAPTEURS[2] != '0')) //x0x mais pas 100
                    TMPI = 1; //carte RI1
                if ((DEMANDE_INFOS_CAPTEURS[1] == '1')) //1x
                    TMPI = 1; //carte RI1

                if ((DEMANDE_INFOS_CAPTEURS[1] == '2')&&(DEMANDE_INFOS_CAPTEURS[2] != '0')) //x2x mais pas 20
                    TMPI = 2; //carte RI2
                if ((DEMANDE_INFOS_CAPTEURS[1] == '3')) //3x
                    TMPI = 2; //carte RI2

                if ((DEMANDE_INFOS_CAPTEURS[1] == '4')&&(DEMANDE_INFOS_CAPTEURS[2] != '0')) //x4x mais pas 40
                    TMPI = 3; //carte RI3
                if ((DEMANDE_INFOS_CAPTEURS[1] == '5')) //5x
                    TMPI = 3; //carte RI3

                if ((DEMANDE_INFOS_CAPTEURS[1] == '6')&&(DEMANDE_INFOS_CAPTEURS[2] != '0')) //x6x mais pas 60
                    TMPI = 4; //carte RI4
                if ((DEMANDE_INFOS_CAPTEURS[1] == '7')) //7x
                    TMPI = 4; //carte RI4

                if ((DEMANDE_INFOS_CAPTEURS[1] == '8')&&(DEMANDE_INFOS_CAPTEURS[2] != '0')) //x8x mais pas 80
                    TMPI = 5; //carte RI5
                if ((DEMANDE_INFOS_CAPTEURS[1] == '9')) //9x
                    TMPI = 5; //carte RI5

                if ((DEMANDE_INFOS_CAPTEURS[0] == '1')&&(DEMANDE_INFOS_CAPTEURS[1] == '0')&&(DEMANDE_INFOS_CAPTEURS[2] == '0')) // 100
                    TMPI = 5; //carte RI5

                if ((DEMANDE_INFOS_CAPTEURS[1] == '2')&&(DEMANDE_INFOS_CAPTEURS[2] == '0')) // 20
                    TMPI = 1; //carte RI1

                if ((DEMANDE_INFOS_CAPTEURS[1] == '4')&&(DEMANDE_INFOS_CAPTEURS[2] == '0')) // 40
                    TMPI = 2; //carte RI2

                if ((DEMANDE_INFOS_CAPTEURS[1] == '6')&&(DEMANDE_INFOS_CAPTEURS[2] == '0')) // 60
                    TMPI = 3; //carte RI1

                if ((DEMANDE_INFOS_CAPTEURS[1] == '6')&&(DEMANDE_INFOS_CAPTEURS[2] == '0')) // 80
                    TMPI = 5; //carte RI1




                // 100
                if ((DEMANDE_INFOS_CAPTEURS[0] == '1')) //SUPERIEUR A 100
                {
                    DEMANDE_INFOS_CAPTEURS[0] = '0';
                    DEMANDE_INFOS_CAPTEURS[1] = '2'; //voie 20
                    DEMANDE_INFOS_CAPTEURS[2] = '0';
                }
                // 02x 04x 06x 08x
                if (((DEMANDE_INFOS_CAPTEURS[1] == '8') || (DEMANDE_INFOS_CAPTEURS[1] == '6') || (DEMANDE_INFOS_CAPTEURS[1] == '4') || (DEMANDE_INFOS_CAPTEURS[1] == '2') || (DEMANDE_INFOS_CAPTEURS[1] == '0'))&&(DEMANDE_INFOS_CAPTEURS[1] != '0')) {
                    DEMANDE_INFOS_CAPTEURS[0] = '0';
                    DEMANDE_INFOS_CAPTEURS[1] = '0'; //voies de 1 � 9
                    //DEMANDE_INFOS_CAPTEURS[1]='0';
                }
                //020 040 060 080
                if (((DEMANDE_INFOS_CAPTEURS[1] == '8') || (DEMANDE_INFOS_CAPTEURS[1] == '6') || (DEMANDE_INFOS_CAPTEURS[1] == '4') || (DEMANDE_INFOS_CAPTEURS[1] == '2') || (DEMANDE_INFOS_CAPTEURS[1] == '0'))&&(DEMANDE_INFOS_CAPTEURS[1] == '0')) {
                    DEMANDE_INFOS_CAPTEURS[0] = '0';
                    DEMANDE_INFOS_CAPTEURS[1] = '2'; //voie 20
                    DEMANDE_INFOS_CAPTEURS[1] = '0';
                }
                //01x 03x 05x 07x 09x
                if ((DEMANDE_INFOS_CAPTEURS[1] == '1') || (DEMANDE_INFOS_CAPTEURS[1] == '3') || (DEMANDE_INFOS_CAPTEURS[1] == '5') || (DEMANDE_INFOS_CAPTEURS[1] == '7') || (DEMANDE_INFOS_CAPTEURS[1] == '9')) {
                    DEMANDE_INFOS_CAPTEURS[0] = '0';
                    DEMANDE_INFOS_CAPTEURS[1] = '1'; //voie de 10 � 19
                    //DEMANDE_INFOS_CAPTEURS[1]='0';
                }
                //LES VOIES SONT TRANSFORMEES EN RELATIVES

                INSTRUCTION[0] = DEMANDE_INFOS_CAPTEURS[1];
                INSTRUCTION[1] = DEMANDE_INFOS_CAPTEURS[2];
                DEMANDE_INFOS_CAPTEURS[5] = DEMANDE_INFOS_CAPTEURS[0];
                DEMANDE_INFOS_CAPTEURS[6] = DEMANDE_INFOS_CAPTEURS[1];
                DEMANDE_INFOS_CAPTEURS[7] = DEMANDE_INFOS_CAPTEURS[2];
                DEMANDE_INFOS_CAPTEURS[8] = '1';
                DEMANDE_INFOS_CAPTEURS[9] = '6';

                goto fin3A;





            }

debut3:
            if (INSTRUCTION[3] != 'F') //UNE VOIE
            {
                if (INSTRUCTION[1] == '9') //Si x9
                {


                    DEMANDE_INFOS_CAPTEURS[6] = INSTRUCTION[0] + 1; //(x+1)
                    DEMANDE_INFOS_CAPTEURS[7] = '0'; //0


                } else {//Si xy

                    DEMANDE_INFOS_CAPTEURS[6] = INSTRUCTION[0];
                    DEMANDE_INFOS_CAPTEURS[7] = INSTRUCTION[1] + 1; //(y+1)
                }

            }

            if (INSTRUCTION[3] == 'F') //TOUTES LES VOIES
            {



                if (INSTRUCTION[1] == '9') {
                    INSTRUCTION[1] = '1';
                    INSTRUCTION[0] = INSTRUCTION[0] + 1;
                } else {
                    INSTRUCTION[1] = INSTRUCTION[1] + 2;
                }

                if (INSTRUCTION[0] == '1' && INSTRUCTION[1] == '9') {
                    goto fin3A;
                } else {

                    if ((TMPI == 1) || (TMPI == 0))
                        ENVOIE_DES_TRAMES(1, 315, &INSTRUCTION);
                    if ((TMPI == 2) || (TMPI == 0))
                        ENVOIE_DES_TRAMES(2, 315, &INSTRUCTION);
                    if ((TMPI == 3) || (TMPI == 0))
                        ENVOIE_DES_TRAMES(3, 315, &INSTRUCTION);
                    if ((TMPI == 4) || (TMPI == 0))
                        ENVOIE_DES_TRAMES(4, 315, &INSTRUCTION);
                    if ((TMPI == 5) || (TMPI == 0))
                        ENVOIE_DES_TRAMES(5, 315, &INSTRUCTION);





                    TEMPO(3);
                    if (RESCRUTATION == 1) //BUG PREMIERE VOIES !!!!!! NUL NUL NUL
                    {

                        if ((INSTRUCTION[0] == '1')&&(INSTRUCTION[1] == '1')) {
                            INSTRUCTION[0] = '0'; //on part de 09 donc 10
                            INSTRUCTION[1] = '9';
                        } else {
                            //INSTRUCTION[0]='0'; //on part de 01
                            INSTRUCTION[1] = INSTRUCTION[1] - 2;
                        }


                        DEMANDE_INFOS_CAPTEURS[1] = '0'; //1 exemple 99 01 03
                        DEMANDE_INFOS_CAPTEURS[2] = '1'; //2
                        RESCRUTATION = 0;
                    }

                    goto debut3;
                }
            }

fin3A:

            if ((TMPI == 1) || (TMPI == 0))
                ENVOIE_DES_TRAMES(1, 315, &INSTRUCTION);
            if ((TMPI == 2) || (TMPI == 0))
                ENVOIE_DES_TRAMES(2, 315, &INSTRUCTION);
            if ((TMPI == 3) || (TMPI == 0))
                ENVOIE_DES_TRAMES(3, 315, &INSTRUCTION);
            if ((TMPI == 4) || (TMPI == 0))
                ENVOIE_DES_TRAMES(4, 315, &INSTRUCTION);
            if ((TMPI == 5) || (TMPI == 0))
                ENVOIE_DES_TRAMES(5, 315, &INSTRUCTION);
            if (RESCRUTATION == 1) //BUG PREMIERE VOIES !!!!!! NUL NUL NUL
            {
                RESCRUTATION = 0;
                TEMPO(5);
                goto fin3A;
            }

fin3:
            if ((TMPI == 1) || (TMPI == 0))
                ENVOIE_DES_TRAMES(1, 276, &DEMANDE_INFOS_CAPTEURS); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE
            if ((TMPI == 2) || (TMPI == 0))
                ENVOIE_DES_TRAMES(2, 276, &DEMANDE_INFOS_CAPTEURS); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE
            if ((TMPI == 3) || (TMPI == 0))
                ENVOIE_DES_TRAMES(3, 276, &DEMANDE_INFOS_CAPTEURS); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE
            if ((TMPI == 4) || (TMPI == 0))
                ENVOIE_DES_TRAMES(4, 276, &DEMANDE_INFOS_CAPTEURS); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE
            if ((TMPI == 5) || (TMPI == 0))
                ENVOIE_DES_TRAMES(5, 276, &DEMANDE_INFOS_CAPTEURS); //COMPTER 2MN15S POUR CETTE OPERATION PAR CARTE



            if ((TMPI == 1) || (TMPI == 0))
                ENVOIE_DES_TRAMES(1, 304, &INSTRUCTION);
            if ((TMPI == 2) || (TMPI == 0))
                ENVOIE_DES_TRAMES(2, 304, &INSTRUCTION);
            if ((TMPI == 3) || (TMPI == 0))
                ENVOIE_DES_TRAMES(3, 304, &INSTRUCTION);
            if ((TMPI == 4) || (TMPI == 0))
                ENVOIE_DES_TRAMES(4, 304, &INSTRUCTION);
            if ((TMPI == 5) || (TMPI == 0))
                ENVOIE_DES_TRAMES(5, 304, &INSTRUCTION);








            //	}

            //	}






















            //SI FLAG ACCORDSR1 R2 R3 serialisation ou attendre la trame err000?? a choisir
            // VOIR SI IL FAUT ATTENDRE UN SIGNAL DES CARTES RI


            delay_ms(100);

            //INTERROGER_TOUT_LES_CAPTEURS(0);






            INFORMATION_ERREUR[0] = 'F'; //ON DIT A L'AUTOMATE QUE L'OPERATION EST TERMINEE
            INFORMATION_ERREUR[1] = 'F';
            for (t = 2; t < 20; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);

            //EFFACER_LISTE(); //EFFACER LA LISTE DE LA MEMOIRE A VOIR

            INTCONbits.GIE = 1;



            break;



















        default:
            delay_ms(100);
            break;

    }


}

void recuperer_trame_memoire(char w, char mode) //RECUPERE TRAME MET DANS LES VARIABLES PUIS ECRIT DANS L'EPROOM
 {

    int t;
    int u;

    u = 8; // pr un TRAMOUT MEMOIRE


    //// MULTI
    //ATTENTION LES CARTES ENVOIES LES INFOS DE LEURS VOIES !!!!
    if (TRAMEIN[2] == '1')
        numdecarteRI = 1; //carte R1
    if (TRAMEIN[2] == '2')
        numdecarteRI = 2; //carte R2
    if (TRAMEIN[2] == '3')
        numdecarteRI = 3; //carte R3
    if (TRAMEIN[2] == '4')
        numdecarteRI = 4; //carte R4
    if (TRAMEIN[2] == '5')
        numdecarteRI = 5; //carte R5
    //// MULTI





    TRAMEOUT[0] = cDebutTrame;











    VOIECOD[0] = TRAMEIN[u];
    //TRAMEOUT[u-7]=TRAMEIN[u];
    u = u + 1;
    VOIECOD[1] = TRAMEIN[u];
    //TRAMEOUT[u-7]=TRAMEIN[u];
    u = u + 1;
    VOIECOD[2] = TRAMEIN[u];
    //TRAMEOUT[u-7]=TRAMEIN[u];
    u = u + 1;
    VOIECOD[3] = TRAMEIN[u];
    //TRAMEOUT[u-7]=TRAMEIN[u];
    u = u + 1;
    VOIECOD[4] = TRAMEIN[u];
    //TRAMEOUT[u-7]=TRAMEIN[u];
    //u=u+1;




    if (mode == 'r') //SI on travaille en mode relatif
    {
        // MULTI
        VOIECOD[1] = VOIECOD[1] + 2 * (numdecarteRI - 1); // EN ASCII 0 devient 2
        if (VOIECOD[1] == 58) // si la voie 20 et 5ieme carte donc 100
        {
            VOIECOD[1] = '0';
            VOIECOD[0] = '1';
        } else {
        }
    }



    u = 8;
    TRAMEOUT[u - 7] = VOIECOD[0];
    u = u + 1;
    TRAMEOUT[u - 7] = VOIECOD[1];
    u = u + 1;
    TRAMEOUT[u - 7] = VOIECOD[2];
    u = u + 1;
    TRAMEOUT[u - 7] = VOIECOD[3];
    u = u + 1;
    TRAMEOUT[u - 7] = VOIECOD[4];
    u = u + 1;





    // MULTI







    //ecrire dans I2C

    //    TYPE[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;


    //    CMOD[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    CMOD[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    CMOD[2] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    CMOD[3] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;


    //    CCON[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    CCON[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    CCON[2] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    CCON[3] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;



    //    CREP[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    CREP[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    CREP[2] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    CREP[3] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;





    t = 0;
    for (t = 0; t < 13; t++) {
        //        CONSTITUTION[t] = TRAMEIN[u];
        TRAMEOUT[u - 7] = TRAMEIN[u];
        u = u + 1;

    }

    t = 0;
    for (t = 0; t < 18; t++) {
        //        COMMENTAIRE[t] = TRAMEIN[u];
        TRAMEOUT[u - 7] = TRAMEIN[u];
        u = u + 1;
    }






    //CABLE[0]=TRAMEIN[u];

    TRAMEOUT[u - 7] = VOIECOD[0];
    u = u + 1;
    //CABLE[1]==TRAMEIN[u];

    TRAMEOUT[u - 7] = VOIECOD[1];
    u = u + 1;
    //CABLE[2]==TRAMEIN[u];

    TRAMEOUT[u - 7] = VOIECOD[2];
    u = u + 1;


    if (TRAMEIN[u] == '?') //si il y a des ? sur la date on met la derniere heure contenue dans la variable horloge lors de cration par exemple avec l'automate
    {
        //        JJ[0] = HORLOGE[6];
        //        JJ[1] = HORLOGE[7];

        //        MM[0] = HORLOGE[8];
        //        MM[1] = HORLOGE[9];

        //        HH[0] = HORLOGE[0];
        //        HH[1] = HORLOGE[1];

        //        mm[0] = HORLOGE[2];
        //        mm[1] = HORLOGE[3];

        //        TRAMEOUT[u - 7] = JJ[0];
        TRAMEOUT[u - 7] = HORLOGE[6]; //Aucune difference � avant car JJ[0] prenait forcement la valeur de HORLOGE[6]
        u = u + 1;
        //        TRAMEOUT[u - 7] = JJ[1];
        TRAMEOUT[u - 7] = HORLOGE[7]; //Aucune difference � avant car JJ[1] prenait forcement la valeur de HORLOGE[7]
        u = u + 1;
        //        TRAMEOUT[u - 7] = MM[0];
        TRAMEOUT[u - 7] = HORLOGE[8]; //Aucune difference � avant car MM[0] prenait forcement la valeur de HORLOGE[8]
        u = u + 1;
        //        TRAMEOUT[u - 7] = MM[1];
        TRAMEOUT[u - 7] = HORLOGE[9]; //Aucune difference � avant car MM[1] prenait forcement la valeur de HORLOGE[9]
        u = u + 1;
        //        TRAMEOUT[u - 7] = HH[0];
        TRAMEOUT[u - 7] = HORLOGE[0]; //Aucune difference � avant car HH[0] prenait forcement la valeur de HORLOGE[0]
        u = u + 1;
        //        TRAMEOUT[u - 7] = HH[1];
        TRAMEOUT[u - 7] = HORLOGE[1]; //Aucune difference � avant car HH[1] prenait forcement la valeur de HORLOGE[1]
        u = u + 1;
        //        TRAMEOUT[u - 7] = mm[0];
        TRAMEOUT[u - 7] = HORLOGE[2]; //Aucune difference � avant car mm[0] prenait forcement la valeur de HORLOGE[2]
        u = u + 1;
        //        TRAMEOUT[u - 7] = mm[1];
        TRAMEOUT[u - 7] = HORLOGE[3]; //Aucune difference � avant car mm[1] prenait forcement la valeur de HORLOGE[3]
        u = u + 1;



    } else {

        //        JJ[0] = TRAMEIN[u];
        TRAMEOUT[u - 7] = TRAMEIN[u];
        u = u + 1;
        //        JJ[1] = TRAMEIN[u];
        TRAMEOUT[u - 7] = TRAMEIN[u];
        u = u + 1;


        //        MM[0] = TRAMEIN[u];
        TRAMEOUT[u - 7] = TRAMEIN[u];
        u = u + 1;
        //        MM[1] = TRAMEIN[u];
        TRAMEOUT[u - 7] = TRAMEIN[u];
        u = u + 1;


        //        HH[0] = TRAMEIN[u];
        TRAMEOUT[u - 7] = TRAMEIN[u];
        u = u + 1;
        //        HH[1] = TRAMEIN[u];
        TRAMEOUT[u - 7] = TRAMEIN[u];
        u = u + 1;


        //        mm[0] = TRAMEIN[u];
        TRAMEOUT[u - 7] = TRAMEIN[u];
        u = u + 1;
        //        mm[1] = TRAMEIN[u];
        TRAMEOUT[u - 7] = TRAMEIN[u];
        u = u + 1;
    }


    //    ii[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;



    //    COD[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    COD[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;




    //    POS[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    POS[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;








    //    iiii[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    iiii[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    iiii[2] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    iiii[3] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;




    //    DISTANCE[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    DISTANCE[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    DISTANCE[2] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    DISTANCE[3] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    DISTANCE[4] = TRAMEIN[u]; //14092020
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;



    //if (ETATZIZCRATIONAUTOMATE=='X')
    //ETATZIZCRATIONAUTOMATE='F';
    if (TRAMEIN[u] == 'X') //SI MODIF OU CREATION SUR L'AUTOMATE
    {
        if (ETATZIZCRATIONAUTOMATE == 'F') //SI nouveau capteur AUTO alors on met etat S/R en mode ZIZ avant CREATION
        {

            ETAT = '5';
            TRAMEOUT[u - 7] = '5';
            u = u + 1;





        } else {
            //ETAT[0]=TRAMEIN[u];
            ETAT = ETATZIZCRATIONAUTOMATE;
            TRAMEOUT[u - 7] = ETATZIZCRATIONAUTOMATE;
            //TRAMEOUT[u-7]=TRAMEIN[u];
            u = u + 1;

        }

    } else //fonctionnement normal
    {
        ETAT = TRAMEIN[u];
        TRAMEOUT[u - 7] = TRAMEIN[u];
        u = u + 1;



    }


    //ETATZIZCRATIONAUTOMATE='0';

    //    ETAT[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;




    //    VALEUR[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    VALEUR[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    VALEUR[2] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    VALEUR[3] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;


    //    SEUIL[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    SEUIL[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    SEUIL[2] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    //    SEUIL[3] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;

    TRAMEOUT[u - 7] = cFinTrame;

    //ecriture ou pas
    if (w == 1)
        ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEOUT), calcul_addmemoire(&TRAMEOUT), &TRAMEOUT, 1); //ECRITURE EN EEPROM

    if (VOIECOD[2] == '9') //TEST
        pointeur_out_in = u;

    LIRE_EEPROM(choix_memoire(&TRAMEOUT), calcul_addmemoire(&TRAMEOUT), &TRAMEIN, 1); //LIRE DANS EEPROM
    pointeur_out_in = u;

    //MAUVAISE MEMOIRE EN A R3 040 ON VA ECRIRE 01000


}

void construire_trame_memoire(void) // ICI ON RECUPERE LE CONTENU DE LA MEMOIRE ET ON LE MET DANS UNE TRAME COM ET DANS LES VARIABLES
 {

    int t;
    int u;


    unsigned char TMPTAB[6]; //TEMPORAIRE


    u = 8;

    VOIECOD[0] = DEMANDE_INFOS_CAPTEURS[0];
    TRAMEOUT[u] = VOIECOD[0];
    u = u + 1;
    VOIECOD[1] = DEMANDE_INFOS_CAPTEURS[1];
    TRAMEOUT[u] = VOIECOD[1];
    u = u + 1;
    VOIECOD[2] = DEMANDE_INFOS_CAPTEURS[2];
    TRAMEOUT[u] = VOIECOD[2];
    u = u + 1;
    VOIECOD[3] = DEMANDE_INFOS_CAPTEURS[3];
    TRAMEOUT[u] = VOIECOD[3];
    u = u + 1;
    VOIECOD[4] = DEMANDE_INFOS_CAPTEURS[4];
    TRAMEOUT[u] = VOIECOD[4];
    u = u + 1;


    // faire fonction recherche infos I2C

    TMPTAB[0] = cDebutTrame;
    TMPTAB[1] = VOIECOD[0];
    TMPTAB[2] = VOIECOD[1];
    TMPTAB[3] = VOIECOD[2];
    TMPTAB[4] = VOIECOD[3];
    TMPTAB[5] = VOIECOD[4];
    tmp = LIRE_EEPROM(choix_memoire(&TMPTAB), calcul_addmemoire(&TMPTAB), &TRAMEIN, 1); //LIRE DANS EEPROM ET METTRE DANS TRAMEIN



    //    TYPE[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7]; // ON RECUPERE LA TRAME MEMOIRE
    u = u + 1;

    //    CMOD[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CMOD[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CMOD[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CMOD[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    //    CCON[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CCON[1] = TRAMEIN[u - 7];
    ;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CCON[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CCON[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    //    CREP[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CREP[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CREP[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CREP[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    t = 0;
    for (t = 0; t < 13; t++) {
        //        CONSTITUTION[t] = TRAMEIN[u - 7];
        TRAMEOUT[u] = TRAMEIN[u - 7];
        u = u + 1;

    }

    t = 0;
    for (t = 0; t < 18; t++) {
        //        COMMENTAIRE[t] = TRAMEIN[u - 7];
        TRAMEOUT[u] = TRAMEIN[u - 7];
        u = u + 1;
    }



    //    CABLE[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CABLE[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CABLE[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    //    JJ[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    JJ[1] = TRAMEIN[u - 7];
    ;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;

    //    MM[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    MM[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;

    //    HH[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    HH[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    //    mm[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    mm[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    //    ii[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    ;
    u = u + 1;

    //    COD[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    COD[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;




    //    POS[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    POS[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;




    //    iiii[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    iiii[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    iiii[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    iiii[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;




    //    DISTANCE[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    DISTANCE[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    DISTANCE[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    DISTANCE[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    DISTANCE[4] = TRAMEIN[u - 7]; //14092020
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    ETAT = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    ETAT[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    //    VALEUR[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    VALEUR[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    VALEUR[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    VALEUR[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    //    SEUIL[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    SEUIL[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    SEUIL[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    SEUIL[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;

    pointeur_out_in = u;

    /////////////////////////////////////////




}

void calcul_CRC(void) {

    CRC[0] = '2';
    CRC[1] = '2';
    CRC[2] = '2';
    CRC[3] = '2';
    CRC[4] = '2';


    TRAMEOUT[pointeur_out_in] = CRC[0];
    TRAMEOUT[pointeur_out_in + 1] = CRC[1];
    TRAMEOUT[pointeur_out_in + 2] = CRC[2];
    TRAMEOUT[pointeur_out_in + 3] = CRC[3];
    TRAMEOUT[pointeur_out_in + 4] = CRC[4];
    TRAMEOUT[pointeur_out_in + 5] = cFinTrame;

}

void controle_CRC(void) {


    CRC[0] = TRAMEIN[pointeur_out_in];
    CRC[1] = TRAMEIN[pointeur_out_in + 1];
    CRC[2] = TRAMEIN[pointeur_out_in + 2];
    CRC[3] = TRAMEIN[pointeur_out_in + 3];
    CRC[4] = TRAMEIN[pointeur_out_in + 4];



}

void resetTO(void) {

    int t;

    for (t = 0; t < 125; t++) {
        TRAMEOUT[t] = 'x';
        //TRAMEIN[t]='x';
    }



}







// CALCUL EMPLACEMENT MEMOIRE

unsigned int calcul_addmemoire(unsigned char *tab) {
    unsigned int t;
    unsigned int i;
    unsigned int r;



    t = 0;
    i = tab[3];
    i = i - 48;
    if (i > 9) {
        t = 0;
        goto nongood; //PAS NORMAL LE NUMERO DE VOIE
    }
    t = t + 1 * i;
    i = tab[2];
    i = i - 48;
    if (i > 9) {
        t = 0;
        goto nongood; //PAS NORMAL LE NUMERO DE VOIE
    }
    t = t + 10 * i;
    i = tab[1];
    i = i - 48;
    if (i > 9) {
        t = 0;
        goto nongood; //PAS NORMAL LE NUMERO DE VOIE
    }
    t = t + 100 * i;


    r = 2176 * (t);
    if (t > 29)
        r = 2176 * (t - 30);
    if (t > 59)
        r = 2176 * (t - 60);
    if (t > 89)
        r = 2176 * (t - 90);


    t = 0;
    i = tab[5] - 48;
    t = 1 * i;
    i = tab[4] - 48;
    t = t + 10 * i;
    i = t;

    t = r + 128 * i;

nongood: // nongood alors on sav a la memoire x en 0 la ou il n'y a rien

    delay_ms(10);



    return t;


}

// choix du numero memoire suivant N�voie

unsigned char choix_memoire(unsigned char *tab) {
    unsigned int i;
    unsigned int t;
    unsigned char r;




    t = 0;
    i = tab[3] - 48;
    if (i > 9) {
        r = 0;
        goto nongoodMEM; //PAS NORMAL LE NUMERO DE VOIE
    }
    t = t + 1 * i;
    i = tab[2] - 48;
    if (i > 9) {
        r = 0;
        goto nongoodMEM; //PAS NORMAL LE NUMERO DE VOIE
    }
    t = t + 10 * i;
    i = tab[1] - 48;
    if (i > 9) {
        r = 0;
        goto nongoodMEM; //PAS NORMAL LE NUMERO DE VOIE
    }
    t = t + 100 * i;

    r = 0;
    if (t > 29)
        r = 1;
    if (t > 59)
        r = 2;
    if (t > 89)
        r = 3;


nongoodMEM: //SI PAS NORAMLE ON ECRIT DANS LA MEMOIRE 0


    delay_ms(10);
    return r;

}





//Lecture dans l'EEPROM N�0 N�1 N�2 ou N�3 envoyer 000 ou 001......

unsigned char LIRE_EEPROM(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char r) {
    char h;
    unsigned char t;
    unsigned char TMP2;
    unsigned char TMP4;
    unsigned char AddH, AddL;


    //MAXERIE A2
    if (ADDE == 6) //GROUPE et bloc notes
        ADDE = 12;
    if (ADDE == 5)
        ADDE = 10;
    if (ADDE == 4)
        ADDE = 8;
    if (ADDE == 3)
        ADDE = 6;
    if (ADDE == 2)
        ADDE = 4;
    if (ADDE == 1)
        ADDE = 2;





    AddL = ADDHL;
    AddH = ADDHL >> 8;



    TMP2 = ADDE;
    //TMP2=TMP2<<1;
    TMP2 = TMP2 | 0xA0;


    IdleI2C();
    StartI2C();
    //IdleI2C();
    while (SSPCON2bits.SEN);

    WriteI2C(TMP2); // 0 sur le bit RW pour indiquer une lecture
    IdleI2C();




    WriteI2C(AddH); // 0 sur le bit RW pour indiquer une lecture
    IdleI2C();

    while (SSPCON2bits.RSEN);

    WriteI2C(AddL); // 0 sur le bit RW pour indiquer une lecture
    IdleI2C();


    RestartI2C();
    while (SSPCON2bits.RSEN);


    //AckI2C();
    TMP2 = TMP2 | 0x01;
    WriteI2C(TMP2); // 1 sur le bit RW pour indiquer une lecture
    IdleI2C();



    //tab[85]=cFinTrame;
    if (r == 1) // pour trame memoire
        getsI2C(tab, 86);
    if (r == 0)
        getsI2C(tab, 108);
    if (r == 3)
        getsI2C(tab, 116);
    if (r == 4)
        getsI2C(tab, 24);
    if (r == 5)
        getsI2C(tab, 120);
    if (r == 6)
        getsI2C(tab, 100);


    NotAckI2C();
    while (SSPCON2bits.ACKEN);



    StopI2C();
    while (SSPCON2bits.PEN);
    return (1);
}






// permet de faire une pause de x ms

void delay_ms(char tempo) {
    char j, k, l;

    for (j = 0; j < tempo; j++) {
        for (k = 0; k < 10; k++) {

            for (l = 0; l < 40; l++) {
                //__no_operation();
                //__no_operation();
                //__no_operation();
                //__no_operation();
                //__no_operation();
            }
        }

    }
}

void Init_I2C(void) {
    //	TRISCbits.TRISC3 = 1;
    //	TRISCbits.TRISC4 = 1;

    //here is the I2C setup from the Seeval32 code.
    DDRCbits.RC3 = 1; //Configure SCL as Input
    DDRCbits.RC4 = 1; //Configure SDA as Input
    SSP1STAT = 0x00; //Disable SMBus & Slew Rate Control 80
    SSP1CON1 = 0x28; //Enable MSSP Master  28
    SSP1CON2 = 0x00; //Clear MSSP Conr// 0x3B for 400kHz  (24mhz/((4*4OOkHz))-1 = 14 ou 3B en hexol Bits
    SSP1ADD = 0x3B; //0x0E
}











//Ecriture dans l'EEPROM N�0 N�1 N�2 ou N�3 envoyer 000 ou 001......adresse haute et basse et une donn�e sur 8bits

unsigned char ECRIRE_EEPROMBYTE(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char g) {
    unsigned char TMP2, TMP;
    unsigned int h;
    unsigned char t;
    unsigned char AddH, AddL;




    //MAXERIE A2
    if (ADDE == 6) //GROUPE ET BLOC NOTES
        ADDE = 12;
    if (ADDE == 5)
        ADDE = 10;
    if (ADDE == 4)
        ADDE = 8;
    if (ADDE == 3)
        ADDE = 6;
    if (ADDE == 2)
        ADDE = 4;
    if (ADDE == 1)
        ADDE = 2;




    TMP2 = ADDE;
    //TMP2=TMP2<<1;
    TMP2 = 0XA0 | TMP2;


    AddL = (ADDHL);
    AddH = (ADDHL) >> 8;

    if (g == 1) // POUR FONCTIONNEMENT TRAME MEMOIRE
        tab[85] = cFinTrame; //FIN DE TRAME MEMOIRE
    if (g == 3) // POUR FONCTIONNEMENT TRAME MEMOIRE
        tab[115] = cFinTrame; //FIN DE TRAME GROUPE
    if (g == 4) // POUR FONCTIONNEMENT TRAME MEMOIRE
        tab[23] = cFinTrame; //FIN DE TRAME EXISTANCE
    if (g == 5) // POUR FONCTIONNEMENT TRAME MEMOIRE
        tab[119] = cFinTrame; //FIN DE TRAME P�RAM1
    if (g == 6) // POUR FONCTIONNEMENT TRAME MEMOIRE
        tab[99] = cFinTrame; //FIN DE TRAME BLOC NOTES



    IdleI2C(); // ensure module is idle
    StartI2C(); // initiate START condition
    while (SSPCON2bits.SEN); // wait until start condition is over
    WriteI2C(TMP2); // write 1 byte - R/W bit should be 0
    IdleI2C(); // ensure module is idle
    WriteI2C(AddH); // write HighAdd byte to EEPROM
    IdleI2C(); // ensure module is idle
    WriteI2C(AddL); // write LowAdd byte to EEPROM
    IdleI2C(); // ensure module is idle
    putstringI2C(tab); // pointer to data for page write
    IdleI2C(); // ensure module is idle
    StopI2C(); // send STOP condition
    while (SSPCON2bits.PEN); // wait until stop condition is over
    return ( 0); // return with no error





    return 0; // FIN DE BOUCLE a *








}

void SERIALISATION(void) {

    char i;
    char TMP = 0X00, TMP2 = 0x00;


    SHLD = 0; //SH/LD=1
    CLK = 0; //CLK=0
    INH = 1; //INH=1
    //PORTDbits.RD2=1;


    delay_ms(100); // ATTENDRE STABILITE ENTREE


    SHLD = 1; //SH/LD=0
    //delay_ms(100);
    CLK = 1;
    INH = 0; //INT=0

    for (i = 0; i < 7; i++) {
        TMP = TMP | INT1;
        TMP2 = TMP2 | INT2;

        CLK = 0; //CLK=0
        //delay_ms(100);     //FRONT CLK MONTANT
        CLK = 1; //CLK=1
        //delay_ms(100);
        TMP = TMP << 1;
        TMP2 = TMP2 << 1;

    }

    TMP = TMP | INT1;
    TMP2 = TMP2 | INT2;
    SHLD = 0; //SH/LD=1
    CLK = 0; //CLK=0
    INH = 1; //INH=1

    ENTREES2 = TMP; //ETAT D7 � D0 int1
    ENTREES1 = TMP2; //ETAT  D1 � D0 int2



}

void identificationINTER(char dequi, char fairequoi) //F,F  exemple INTER...(6(carte AU),0x00(envoie ERR00))
{
    char QUI; //DU PRIORITAIRE AU MOINS
    char u;
    SERIALISATION();
    delay_ms(100);

    QUI = 0x01;
    QUI = ENTREES2 & 0x10; //ALIM CHUTE

    QUI = ENTREES2 & 0x04; //POUR ACCRS485 IO

    if ((ENTREES2 == 0)&&(ENTREES1 == 0)) {
        //SERIALISATION();
        delay_ms(100);

    }
    //MODE MANUEL
    if (dequi != 'F') {
        if (dequi == 1) //CARTE R1
        {
            ENTREES1 = 0x02;
            ENTREES2 = 0x00;
        }
        if (dequi == 7) //CARTE IO
        {
            ENTREES1 = 0x00;
            ENTREES2 = 0x04;
        }
        if (dequi == 6) //AU
        {
            ENTREES1 = 0x00;
            ENTREES2 = 0x02;
        }




    }

    if (fairequoi != 'F') {

        PRIO_AUTO_IO = fairequoi;
    }

    //SINON MODE AUTOMATIQUE
    //////////////MOUCHARD
    //TRAMEOUT[0]='#';
    //TRAMEOUT[1]=PRIO_AUTO_IO;
    // TRAMEOUT[1]=TRAMEOUT[1]>>4;
    //TRAMEOUT[1]=TRAMEOUT[1]&0x0F;
    //if (TRAMEOUT[1]>=10)
    //TRAMEOUT[1]=TRAMEOUT[1]+55;
    //else
    //TRAMEOUT[1]=TRAMEOUT[1]+48;
    //
    //TRAMEOUT[2]=PRIO_AUTO_IO; 
    //TRAMEOUT[2]=TRAMEOUT[2]&0x0F;
    //if (TRAMEOUT[2]>=10)
    //TRAMEOUT[2]=TRAMEOUT[2]+55;
    //else
    //TRAMEOUT[2]=TRAMEOUT[2]+48;
    //
    //TRAMEOUT[3]='+';
    //TRAMEOUT[4]=ENTREES1+48;
    //TRAMEOUT[5]=ENTREES2+48;
    //TRAMEOUT[6]='*'; 
    //TRAMEENVOIRS485DONNEES();





    if ((ENTREES2 & 0x02) == 2) //AUTOMATE PRIOTAIRE
    {




        if (PRIO_AUTO_IO == 0xDD) {
            PRIO_AUTO_IO = 0xD1; //DEmande de l'auto pendant une action DIC de la CM->Rx
            //	INTCONbits.GIE= 0; // ON DESACTIVE LES INTERRUPTIONS SERONT REACTIVE SI REA OU A VOIR 21082017
        }
        if (PRIO_AUTO_IO == 0xE1) {
            PRIO_AUTO_IO = 0x00; //DEmande de l'auto pendant une action TEC (table exis.) de la CM->Rx
        }

        if (PRIO_AUTO_IO == 0xEE) {
            PRIO_AUTO_IO = 0xE1; //DEmande de l'auto pendant une action TEC (table exis.) de la CM->Rx

        }

        if (PRIO_AUTO_IO == 0xF1) {
            PRIO_AUTO_IO = 0xE1; //DEmande de l'auto pendant une action ZTE (table exis.) de la IO->CM

        }


        if (PRIO_AUTO_IO == 0x22) {
            PRIO_AUTO_IO = 0x00; //DEmande de l'auto pendant une action ZIG (PARAMETRES) de la IO->CM

        }

        if (PRIO_AUTO_IO == 0xCC) {
            PRIO_AUTO_IO = 0x00; //DEmande de l'auto pendant une action ZIZ de la IO->CM

        }
        if (PRIO_AUTO_IO == 0x77) //MODE HORLOGE
        {
            PRIO_AUTO_IO = 0x00;
        }
        if (PRIO_AUTO_IO == 0x00) //fonctionnement normal aucune action precedente (DIC ou de la carte IO)
        {
            if (ETAT_IO == 0xFF) //IO ON
            {
                stopIO();
            }
            TEMPO(2); //4 toto
            PRIO_AUTO_IO = 0x01; //DEMANDE AUTOMATE EN MODE NORMAL


            INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = '0';
            for (u = 2; u < 20; u++) {
                INFORMATION_ERREUR[u] = 'x';
            }

            ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);


            TRAMERECEPTIONRS485DONNEES(9);
            if (TRAMEIN[0] != cDebutTrame) {
                // TRAME ORDRE INCOMPREHENSIBLE A GERE
            }

            if (NORECEPTION == 0xFF) {
                //delay_ms(300);
                ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);
                TRAMERECEPTIONRS485DONNEES(5);
            }
            if (NORECEPTION == 0xFF) {
                //delay_ms(300);
                ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);
                TRAMERECEPTIONRS485DONNEES(5);
            }
            if (NORECEPTION == 0xFF) {
                //delay_ms(300);
                ENVOIE_DES_TRAMES(6, 302, &INFORMATION_ERREUR);
                TRAMERECEPTIONRS485DONNEES(5);
            }


            //		CMvAU=1; // L'AUTOMATE PEUT RECEVOIR
            //		AUvCM=0;

            if (NORECEPTION != 0xFF)
                RECUPERATION_DES_TRAMES(&TRAMEIN); //LA TRAMEOUT EST PRETTE A ETRE ENVOYEE
            else {
                delay_ms(100);
            }

            if (PRIO_AUTO_IO = 0x01)
                PRIO_AUTO_IO = 0x00; //RETOUR MODE NORMAL
        }

        //	 if (PRIO_AUTO_IO == 0xEE) {
        //          PRIO_AUTO_IO = 0x00; //DEmande de l'auto pendant une action TEC (table exis.) de la CM->Rx
        //		}





    }


    if ((ENTREES1 & 0x02) == 2) //POUR ACCRS485 R1
    {

        if (PRIO_AUTO_IO == 0x00) //fonctionnement normal aucune action precedente (DIC ou de la carte IO)
            //FAIRE DRS485=1
        {


            TRAMERECEPTIONRS485DONNEES(9);
            if (NORECEPTION != 0xFF)
                RECUPERATION_DES_TRAMES(&TRAMEIN); //LA TRAMEOUT EST PRETTE A ETRE ENVOYEE
        }



        delay_ms(100);

    }



    if ((ENTREES2 & 0x04) == 0x04) //CARTE IO
    {
        IOTRAVAIL = 1;





        //	if ((((cycleN==1)||(PRIO_AUTO_IO==0XCC)&&(cycleN==2))&&(IO_EN_COM==0))||(((cycleN==1)||((PRIO_AUTO_IO==0XDD)&&(cycleN==2)))&&(IO_EN_COM==0))) //LA CARTE IO NE DEVRAIT PAS MARCHER ON DEVRAIT RESET A VOIR et si pendant ZIZ AUTOMATE LIO VEUT CAUSER !!!
        //0xCC = ZIZ et 0XDD = DIC

        if ((cycleN == 1)&&(IO_EN_COM == 0)) //Si sycle 1 mais pas MINITEL ou telgat
        { // OU DEMANDE IO PENDANT DIC (SCRUTATION) PENDANT LE CYCLE 2

            delay_ms(100);
            DRS485_IO = 1; //ON DESACTIVE LIO
            delay_ms(300);
            DRS485_IO = 0;
            ETAT_IO = 0x00;
            //FIRST_DEMAR=0XFF;
            //ON ETEINT CARTE IO POUR DEPART CYCLE POUR LA RALLUMER PLUS TARD
            //PORTCbits.RC5=0;

            goto INITIO;



        }

        if (PRIO_AUTO_IO == 0xDD) {
            PRIO_AUTO_IO = 0xD2; //DEmande de l'IO pendant une action DIC de la CM->Rx

        }

        if (PRIO_AUTO_IO == 0xEE) {
            PRIO_AUTO_IO = 0xE2; //DEmande de l'IO pendant une action TEC (table exis.) de la CM->Rx

        }

        if (PRIO_AUTO_IO = 0x77) //ACTION HOR PRECEDENTE
            PRIO_AUTO_IO = 0;

        if (PRIO_AUTO_IO == 0x22) //ACTION ZOR HORLOGE PRECEDENTE
            PRIO_AUTO_IO = 0;

        if (PRIO_AUTO_IO == 0x00) //fonctionnement normal aucune action precedente (DIC ou de la carte AU)
        {
            //FAIRE DRS485=1
            PRIO_AUTO_IO = 0x02; //DEMANDE L'IO EN MODE NORMAL
            TEMPO(2);



            INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur
            INFORMATION_ERREUR[1] = '0';
            for (u = 2; u < 20; u++) {
                INFORMATION_ERREUR[u] = 'x';
            }

            ENVOIE_DES_TRAMES(7, 302, &INFORMATION_ERREUR);











            TRAMERECEPTIONRS485DONNEES(9); //pour 9 environ 6S dattente
            if (NORECEPTION != 0xFF)
                RECUPERATION_DES_TRAMES(&TRAMEIN); //LA TRAMEOUT EST PRETTE A ETRE ENVOYEE
            else {
                //delay_ms(100);
            }

            if (PRIO_AUTO_IO == 0x02)
                PRIO_AUTO_IO = 0x00; //RETOUR MODE NORMAL
        }


        delay_ms(100);

    }






INITIO:



    QUI = ENTREES1 & 0x01; //POUR ACCRS485 R2

    QUI = ENTREES2 & 0x80; ////POUR ACCRS485 R3

    QUI = ENTREES2 & 0x40; ////POUR ACCRS485 R4


    QUI = ENTREES2 & 0x20; //POUR ACCRS485 R5


    QUI = ENTREES2 & 0x01; //POUR ACCRS485 ALIM




















}

void AQUIENVOYER(unsigned char *tabo) {


    if ((tabo[0] == 'R')&&(tabo[1] == '1')) {
        DRS485_R1 = 0;
        delay_ms(50);
        DRS485_R1 = 1;

    }



    if ((tabo[0] == 'R')&&(tabo[1] == '2')) {
        DRS485_R2 = 0;
        delay_ms(50);
        DRS485_R2 = 1;

    }

    if ((tabo[0] == 'R')&&(tabo[1] == '3')) {
        DRS485_R3 = 0;
        delay_ms(50);
        DRS485_R3 = 1;

    }

    if ((tabo[0] == 'R')&&(tabo[1] == '4')) {
        DRS485_R4 = 0;
        delay_ms(50);
        DRS485_R4 = 1;

    }

    if ((tabo[0] == 'R')&&(tabo[1] == '5')) {
        DRS485_R5 = 0;
        delay_ms(50);
        DRS485_R5 = 1;

    }




















    delay_ms(200);
    delay_ms(200);
    delay_ms(200);




}

void REPOSAQUIENVOYER(unsigned char *tabo) {


    if ((tabo[0] == 'R')&&(tabo[1] == '1')) {
        DRS485_R1 = 0;

    }

    delay_ms(10);



    if ((tabo[0] == 'R')&&(tabo[1] == '2')) {
        DRS485_R2 = 0;

    }

    delay_ms(10);




    if ((tabo[0] == 'R')&&(tabo[1] == '3')) {
        DRS485_R3 = 0;

    }

    delay_ms(10);



    if ((tabo[0] == 'R')&&(tabo[1] == '4')) {
        DRS485_R4 = 0;

    }

    delay_ms(10);



    if ((tabo[0] == 'R')&&(tabo[1] == '5')) {
        DRS485_R5 = 0;

    }

    delay_ms(10);
















}

void reglerhorloge(char *tab) {



}

void lire_horloge(void) {




    HORLOGE[0] = '1';
    HORLOGE[1] = '1';
    HORLOGE[2] = '1';
    HORLOGE[3] = '1';
    HORLOGE[4] = '1';
    HORLOGE[5] = '1';
    HORLOGE[6] = '1';
    HORLOGE[7] = '1';
    HORLOGE[8] = '1';
    HORLOGE[9] = '1';
    HORLOGE[10] = '1';
    HORLOGE[11] = '1';




}

void TRANSFORMER_TRAME_MEM_EN_COM(void) {

    int t;
    int u;
    u = 8;
    TRAMEOUT[0] = cDebutTrame;
    // creer capteur a voir ou il cree et tabel exis
    VOIECOD[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    VOIECOD[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    VOIECOD[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    VOIECOD[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    VOIECOD[4] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    //ecrire dans I2C

    //    TYPE[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    //    CMOD[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CMOD[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CMOD[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CMOD[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    //    CCON[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CCON[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CCON[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CCON[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    //    CREP[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CREP[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CREP[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CREP[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;





    t = 0;
    for (t = 0; t < 13; t++) {
        //        CONSTITUTION[t] = TRAMEIN[u - 7];
        TRAMEOUT[u] = TRAMEIN[u - 7];
        u = u + 1;

    }

    t = 0;
    for (t = 0; t < 18; t++) {
        //        COMMENTAIRE[t] = TRAMEIN[u - 7];
        TRAMEOUT[u] = TRAMEIN[u - 7];
        u = u + 1;
    }



    //    CABLE[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CABLE[1] == TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    CABLE[2] == TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    //    JJ[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    JJ[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    //    MM[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    MM[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    //    HH[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    HH[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    //    mm[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    mm[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    //    ii[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    //    COD[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    COD[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;




    //    POS[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    POS[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;








    //    iiii[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    iiii[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    iiii[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    iiii[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;




    //    DISTANCE[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    DISTANCE[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    DISTANCE[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    DISTANCE[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    DISTANCE[4] = TRAMEIN[u - 7]; ////14092020
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    ETAT = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    ETAT[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    //    VALEUR[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    VALEUR[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    VALEUR[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    VALEUR[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    //    SEUIL[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    SEUIL[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    SEUIL[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    //    SEUIL[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    TRAMEOUT[u] = cFinTrame;

    //ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEIN),calcul_addmemoire(&TRAMEIN),&TRAMEIN,1); //ECRITURE EN EEPROM
    //LIRE_EEPROM(choix_memoire(&TRAMEOUT),calcul_addmemoire(&TRAMEOUT),&TRAMEIN); //LIRE DANS EEPROM
    pointeur_out_in = u;


}

void EFFACEMENT(unsigned char *tab) {

    char t;

    TRAMEOUT[0] = cDebutTrame; //#
    TRAMEOUT[1] = tab[0];
    TRAMEOUT[2] = tab[1];
    TRAMEOUT[3] = tab[2];
    TRAMEOUT[4] = tab[3];
    TRAMEOUT[5] = tab[4];


    for (t = 6; t < 125; t++) {
        TRAMEOUT[t] = 0xFF;
    }

    ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEOUT), calcul_addmemoire(&TRAMEOUT), &TRAMEOUT, 1); //ECRITURE EN EEPROM


}

void PREPARATION_PAGE_AUTO(void)

 {


    unsigned char y, t;
    char i;
    char tati[6];
    unsigned int l;






    tati[4] = PAGE[4];
    tati[3] = PAGE[3];
    tati[2] = PAGE[2];
    tati[1] = PAGE[1];
    tati[0] = PAGE[0];






    resetTO();

    //for (y=0;y<10;y++)
    //{


    //METTRE LA TABLE EXISTENCE EN TMP PUIS SI r alors regarder codage 1 et remplir le reste de FF si a alors trouver les A et mettre FF ailleurs
    l = 100 * (tati[1] - 48) + 10 * (tati[2] - 48) + 1 * (tati[3] - 48);
    l = 128 * l + 30000;
    LIRE_EEPROM(3, l, &TMP, 4); //LIRE DANS EEPROM lexistance du capteur sur la voie



    LIRE_EEPROM(choix_memoire(&PAGE), calcul_addmemoire(&PAGE), &TRAMEIN, 1); //LIRE DANS EEPROM
    TRAMEOUT[0] = cDebutTrame;
    TRAMEOUT[8] = TRAMEIN[1];
    TRAMEOUT[9] = TRAMEIN[2];
    TRAMEOUT[10] = TRAMEIN[3];




    for (t = 11; t < 29; t++) {
        TRAMEOUT[t] = TRAMEIN[t + 21];
    }


    //DEBIT
    TRAMEOUT[29] = TRAMEIN[6]; //TYPE
    TRAMEOUT[30] = TRAMEIN[4]; //CODAGE 0
    TRAMEOUT[31] = TRAMEIN[5]; //CODAGE 0
    TRAMEOUT[32] = TRAMEIN[75]; //ETAT
    TRAMEOUT[33] = TRAMEIN[76]; //ETAT
    if (TMP[3] == 'F') {
        TRAMEOUT[32] = 'F'; //PAS DE CAPTEURS
        TRAMEOUT[33] = 'F'; //PAS DE CAPTEURS

    }
    if (TMP[2] == 'i') //INCIDENT PAT
    {
        TRAMEOUT[32] = '9'; //INCIDENT
        TRAMEOUT[33] = '9';
    }
    if (TMP[2] == 'c') //CC PAT
    {
        TRAMEOUT[32] = '8'; //CC
        TRAMEOUT[33] = '8';
    }
    if (TMP[2] == 'h') //FUS PAT
    {
        TRAMEOUT[32] = '7'; //FUS
        TRAMEOUT[33] = '7';
    }





    //CAPTEURS DE PRESSION
    for (t = 1; t < 17; t++) {
        //1 ier capteur COD1

        PAGE[4] = '0';
        if (t >= 10)
            PAGE[4] = '1';

        if (t < 10)
            PAGE[5] = t + 48;
        if (t == 10)
            PAGE[5] = '0';
        if (t >= 10)
            PAGE[5] = (t - 10) + 48;

        LIRE_EEPROM(choix_memoire(&PAGE), calcul_addmemoire(&PAGE), &TRAMEIN, 1); //LIRE DANS EEPROM
        TRAMEOUT[34 + 4 * (t - 1)] = TRAMEIN[4]; //CODAGE 1
        TRAMEOUT[35 + 4 * (t - 1)] = TRAMEIN[5]; //CODAGE 1


        TRAMEOUT[36 + 4 * (t - 1)] = TRAMEIN[75]; //ETAT 1
        TRAMEOUT[37 + 4 * (t - 1)] = TRAMEIN[76]; //ETAT 1





        if (TMP[3 + t] == 'F') {
            TRAMEOUT[36 + 4 * (t - 1)] = 'F'; //PAS DE CAPTEURS
            TRAMEOUT[37 + 4 * (t - 1)] = 'F'; //PAS DE CAPTEURS
        }

        if ((TMP[2] == 'r')&&(t == 1)) //RESISTIF et codage 1
        {
            TRAMEOUT[36 + 4 * (t - 1)] = TRAMEIN[75]; //ETAT 1
            TRAMEOUT[37 + 4 * (t - 1)] = TRAMEIN[76]; //ETAT 1
        }


        if (TMP[2] == 'i') //INCIDENT PAT
        {
            TRAMEOUT[36 + 4 * (t - 1)] = '9';
            TRAMEOUT[37 + 4 * (t - 1)] = '9'; //PAS DE CAPTEURS
        }
        if (TMP[2] == 'c') //CC PAT
        {
            TRAMEOUT[36 + 4 * (t - 1)] = '8';
            TRAMEOUT[37 + 4 * (t - 1)] = '8'; //PAS DE CAPTEURS
        }
        if (TMP[2] == 'h') //FUS PAT
        {
            TRAMEOUT[36 + 4 * (t - 1)] = '7';
            TRAMEOUT[37 + 4 * (t - 1)] = '7'; //PAS DE CAPTEURS
        }

        if ((TMP[2] == 'r')&&(t != 1)) //CARTE RESISTIVE
        {
            TRAMEOUT[36 + 4 * (t - 1)] = 'F';
            TRAMEOUT[37 + 4 * (t - 1)] = 'F'; //PAS DE CAPTEURS car resistif uniquement en 01
        }


    }




    //delay_ms(1);






    //delay_ms(1);


}





//********************************************************************/

unsigned char putstringI2C(unsigned char *wrptr) {



    unsigned char x;

    unsigned int PageSize;
    PageSize = 128;


    for (x = 0; x < PageSize; x++) // transmit data until PageSize
    {
        if (SSPCON1bits.SSPM3) // if Master transmitter then execute the following
        { //


            if (putcI2C(*wrptr)) // write 1 byte
            {
                return ( -3); // return with write collision error
            }


            IdleI2C(); // test for idle condition
            if (SSPCON2bits.ACKSTAT) // test received ack bit state
            {
                return ( -2); // bus device responded with  NOT ACK
            } // terminateputstringI2C() function
        } else // else Slave transmitter
        {
            PIR1bits.SSPIF = 0; // reset SSPIF bit


            SSPBUF = *wrptr; // load SSPBUF with new data


            SSPCON1bits.CKP = 1; // release clock line
            while (!PIR1bits.SSPIF); // wait until ninth clock pulse received

            if ((!SSPSTATbits.R_W) && (!SSPSTATbits.BF))// if R/W=0 and BF=0, NOT ACK was received
            {
                return ( -2); // terminateputstringI2C() function
            }
        }



        wrptr++; // increment pointer



    } // continue data writes until null character
    return ( 0);
}




//////////////////////////////////////////////
// fonction de conversion INT => CHAR

void IntToChar(signed int value, char *chaine, int Precision) {
    signed int Mil, Cent, Diz, Unit;
    int count = 0;

    // initialisation des variables
    Mil = 0;
    Cent = 0;
    Diz = 0;
    Unit = 0;
    /*
    if (value < 0) // si c'est un nombre n�gatif
        {
        value = value * (-1);
     *chaine = 45; // signe '-'
        count = 1;  // c'est un nombre n�gatif
        }
    else // si c'est un nombre positif
        {
        count = 1;
     *chaine = 32; // code ASCII du signe espace ' '
        }
     */
    // si la valeur n'est pas nulle
    if (value != 0) {
        if (Precision >= 4) // si l'utilisateur d�sire les milliers
        {
            // conversion des milliers
            Mil = value / 1000;
            if (Mil != 0) {
                *(chaine + count) = Mil + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        if (Precision >= 3) // si l'utilisateur d�sire les centaines
        {
            // conversion des centaines
            Cent = value - (Mil * 1000);
            Cent = Cent / 100;
            if (Cent != 0) {
                *(chaine + count) = Cent + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        if (Precision >= 2) // si l'utilisateur d�sire les dizaines
        {
            // conversion des dizaines
            Diz = value - (Mil * 1000) - (Cent * 100);
            Diz = Diz / 10;
            if (Diz != 0) {
                *(chaine + count) = Diz + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        // conversion unit�s
        Unit = value - (Mil * 1000) - (Cent * 100) - (Diz * 10);
        *(chaine + count) = Unit + 48;
        if (*(chaine + count) < 48) // limites : 0 et 9
            *(chaine + count) = 48;
        if (*(chaine + count) > 57)
            *(chaine + count) = 57;
    } else // if (value == 0)
    {
        //*(chaine) = 32; // ecrit un espace devant le nombre
        for (Mil = 0; Mil < Precision; Mil++) // inscription de '0' dans toute la chaine
            *(chaine + Mil) = 48;
    }

} // fin de la fonction de conversion INT => CHAR

void MESURE_DES_TENSIONS(void)
 {
    TRISAbits.TRISA0 = 1;
    TRISAbits.TRISA1 = 1;
    TRISAbits.TRISA2 = 1;
    TRISAbits.TRISA3 = 1;

    TRISAbits.TRISA5 = 1;

    TRISFbits.TRISF0 = 1;
    TRISFbits.TRISF1 = 1;
    TRISFbits.TRISF2 = 1;

    ADCON1 = 0x07; //de AN0 � AN7
    ADCON2 = 0xB8; //DROITE ET TEMPS CAN



    //float VDD1,VDD2,VVD3,VDD4,VDD5,Vtemp,VDD6,VBAT;

    VDD1 = 2 * AVERAGE(0, 5, 1); //CARTE VDD1 R1
    VDD2 = 2 * AVERAGE(1, 5, 1); //CARTE VDD2 R2
    VDD3 = 2 * AVERAGE(2, 5, 1); //CARTE VDD3 R3
    VDD4 = 2 * AVERAGE(3, 5, 1); //CARTE VDD4 R4
    //    Vtemp = AVERAGE(4, 5, 1); //temperature
    VDD5 = 2 * AVERAGE(5, 5, 1); //CARTE VDD5 R5
    //    VDD6 = 2 * AVERAGE(6, 5, 1); //CARTE VDD6 IO
    VBAT = 2 * AVERAGE(7, 5, 1); //VBAT //4.055176V tension batterie charg�e on observe >4.05 et ca diminue a 4.05

    //4.131348 a 48.70mA qd on est en charge
    // ca se coupe tt seul descend a 4.05 environ on coupe le mode charge batterie 1ier mosfet
    //et ca descend  jusqu'au niveau souhait�(exemple 3.6V) puis on rebranche mosfet1



    delay_ms(100);
















}


//////////////////////////////////
// fonction CAN///////////////////
//////////////////////////////////

float CAN(char V) {

    const double q = 0.001220703125;

    char VOIE;
    float tmp;

    switch (V) {
        case 0:
            VOIE = 0b00000000;
            break;
        case 1:
            VOIE = 0b00000100;
            break;
        case 2:
            VOIE = 0b00001000;
            break;
        case 3:
            VOIE = 0b00001100;
            break;
        case 4:
            VOIE = 0b00010000;
            break;
        case 5:
            VOIE = 0b00010100;
            break;
        case 6:
            VOIE = 0b00011000;
            break;

        case 7:
            VOIE = 0b00011100;
            break;










        default:
            // Code
            break;
    }

    //ADCON2=0xB8;             //DROITE ET TEMPS
    //ADCON0bits.ADON=1;  //activation CAN
    //ADCON1=0x8E; //1000 1110 justif � droite 000000xxxx juste AN0

    //delayms(100);
    ADCON0 = 0x00 & ADCON0;
    ADCON0 = VOIE | ADCON0;
    ADCON0 = 0x01 | ADCON0;
    ADCON0bits.GO_DONE = 1; // lancement conver
    while (ADCON0bits.GO_DONE) {
    } // attendre conver
    tmp = (float) ADRES*q;
    ADCON0bits.ADON = 0;

    return (tmp);
}

float AVERAGE(char canal, char avemax, char megmax) {
    double moyenne = 0;
    double moyennes = 0;
    float tmp;
    char i, j;



    for (j = 0; j < megmax; j++) {

        for (i = 0; i < avemax; i++) {

            tmp = CAN(canal);
            moyenne = tmp + moyenne;
        }
        tmp = moyenne / (i);
        moyennes = moyennes + tmp;
        tmp = 0;
        moyenne = 0;
    }
    tmp = moyennes / (j);


    return (tmp);
}

void CONTROLE_CHARGE_BATT(void) {

    char testchargeon;

    TRISJbits.TRISJ3 = 0; //STATO
    TRISEbits.TRISE7 = 1; //STAT

    testchargeon = 0;
    if (CHARGE_ON == 1)
        testchargeon = 1;






    //TEST ETAT BATTERIE
    if (testchargeon == 0) //SI ON A ALLUME CHARGE
    {
        STATO = 1; //ON TEST SI CHARGE TERMINEE


        if (STAT == 1) {
            delay_ms(100); //EN CHARGE
        }

        if (STAT == 0) {
            delay_ms(100); //CHARGE COMPLETE
            CHARGE_ON = 1; //ETEINDRE CHARGE
            //goto CONTROLE;
        }
    }


    if (testchargeon == 1) //SI ON A ETEINT CHARGE
    {

        if (VBAT <= 3.3) //ON REGARDE SI BATTERIE FAIBLE charge complete
            CHARGE_ON = 0; //si le cas on rallume charge

    }






    // TEST TEMPERATURE DANGER

    STATO = 0;

    if (testchargeon == 0) //SI CHARGE ALLUMEE
    {
        if (STAT == 1) //TEMPERATURE OK
        {

            delay_ms(100);
            CHARGE_ON = 0; //ALLUMER CHARGE

        }
        if (STAT == 0) //PROBLEME TEMPERATURE test valable si CHARGE_ON=0 (circuit charge aliment�) sinon ne pas prendre en compte
        {



            CHARGE_ON = 1; //ON ETEINT MEME SI C PROTEGE PAR LE CIRCUIT IL FAUDRAIT JUSTE RAJOUTER UNE INFORMATION POUR OPERATEUR A VOIR !!!!!!


        }
    }


    STATO = 0;

    if (testchargeon == 1) //SI CHARGE ETEINT
    {
        //CHARGE_ON=0;  //ON REALLUME LA CHARGE pour tester ????
        if (STAT == 1) //TEMPERATURE OK
        {

            delay_ms(100);
            CHARGE_ON = 0; //ON REALLUME LA CHARGE

        }

        if (STAT == 0) //TEMPERATURE NO OK
        {

            delay_ms(100);
            CHARGE_ON = 0; //ON REALLUME LA CHARGE

        }




    }











}

void INTERROGER_TOUT_LES_CAPTEURS(char t) {
    unsigned int i, y, ADD;
    unsigned char R;
    //LIRE_EEPROM(7,ADD,&TRAMEOUT,0);








    for (i = 0; i < 13000; i = i + 128) {
        if (t == 0)
            LIRE_EEPROM(7, i, &TMP, 0); //LISTE COMPLETE
        if (t == 1)
            LIRE_EEPROM(5, i, &TMP, 0); //LISTE AVEC GRAND CHANGEMENT
        delay_ms(6); //IMPORTANT
        if (TMP[98] == 'F')
            i = 15000; //FIN D'INTERROGATION


        for (y = 0; y < 17; y++) {
            DEMANDE_INFOS_CAPTEURS[0] = TMP[13 + 5 * y];
            DEMANDE_INFOS_CAPTEURS[1] = TMP[14 + 5 * y];
            DEMANDE_INFOS_CAPTEURS[2] = TMP[15 + 5 * y];
            DEMANDE_INFOS_CAPTEURS[3] = TMP[16 + 5 * y];
            DEMANDE_INFOS_CAPTEURS[4] = TMP[17 + 5 * y];

            if (DEMANDE_INFOS_CAPTEURS[0] == 'x')
                goto finliste;

            ADD = 100 * (DEMANDE_INFOS_CAPTEURS[0] - 48);
            ADD = ADD + 10 * (DEMANDE_INFOS_CAPTEURS[1] - 48); // ON REGARDE LA VOIE
            ADD = ADD + (DEMANDE_INFOS_CAPTEURS[2] - 48);


            R = 5;
            if (ADD < 79)
                R = 4;


            if (ADD < 59)
                R = 3;

            if (ADD < 39)
                R = 2;

            if (ADD < 19)
                R = 1;





            ENVOIE_DES_TRAMES(R, 276, &DEMANDE_INFOS_CAPTEURS);
        }





    }

finliste:

    delay_ms(100);

}

void EFFACER_LISTE(char t) {
    unsigned int i;

    for (i = 0; i < 125; i++) {
        TRAMEIN[i] = 0xFF;

    }


    for (i = 0; i < 13000; i = i + 128) {
        if (t == 0)
            ECRIRE_EEPROMBYTE(7, i, &TRAMEIN, 0); //LISTE AVEC CHANGEMENT IMPORTANT
        if (t == 1)
            ECRIRE_EEPROMBYTE(5, i, &TRAMEIN, 0); //LISTE COMPLETE

        delay_ms(6); //IMPORTANT
    }

    //LIRE_EEPROM(7,128,&TRAMEOUT,0);
    //delay_ms(100);

}

void METTRE_EN_MEMOIRE_LISTE(unsigned char *tab, char t) {
    unsigned int ADD;



    ADD = 100 * (tab[99] - 48);
    ADD = ADD + 10 * (tab[100] - 48);
    ADD = ADD + (tab[101] - 48);


    ADD = 128 * ADD;

    if (t == 0)
        ECRIRE_EEPROMBYTE(7, ADD, tab, 0); //LISTE COMPLETE
    if (t == 1)
        ECRIRE_EEPROMBYTE(5, ADD, tab, 0); //LISTE AVEC CHANGEMENT IMPORTANT


    //delay_ms(100);
    //LIRE_EEPROM(7,128,&TRAMEOUT,0);


}

void GROUPE_EN_MEMOIRE(unsigned char *tab, char lui) {

    int ici;
    if (lui == 0)
        ici = 0;
    if (lui == 1)
        ici = 128;
    if (lui == 2)
        ici = 256;



    ECRIRE_EEPROMBYTE(6, ici, tab, 3);
    delay_ms(100);
    LIRE_EEPROM(6, ici, &TRAMEOUT, 3);
    delay_ms(100);

}

unsigned char envoyerHORLOGE_i2c(unsigned char NumeroH, unsigned char ADDH, unsigned char dataH0) {

    StartI2C();
    WriteI2C(NumeroH); // adresse de l'horloge temps r�el
    WriteI2C(ADDH);
    WriteI2C(dataH0);
    StopI2C();
    delay_ms(11);

}

signed char litHORLOGE_i2c(unsigned char NumeroH, unsigned char AddH) {


    char Data;

    StartI2C();
    WriteI2C(NumeroH & 0xFE); // adresse de la DS1307
    WriteI2C(AddH); // suivant le choix utilisateur
    RestartI2C();
    WriteI2C(NumeroH); // on veut lire
    Data = ReadI2C(); // lit la valeur
    StopI2C();
    return (Data);



}

unsigned char ECRIRE_HORLOGE(unsigned char zone, unsigned char Time) {
    unsigned char TMP;

    //Time=0x00;
    if (zone == 0)
        Time = Time & 0x80; //laisse l'horloge CHbits a 1 secondes tres important si ce bit n'est pas a 1 lhorloge ne marche pas
    if (zone == 1)
        Time = Time & 0x7F; //laisse le bit7 a zero minutes
    if (zone == 2)
        Time = Time & 0x3F; //met a zero les bits 6 et 7 pour le mode 24h
    if (zone == 3)
        Time = Time & 0x07; // laisse ts les bits sauf 0 1 2 a 0
    if (zone == 4)
        Time = Time & 0x3F; // met a 0 les bits 7 et 6 date
    if (zone == 5)
        Time = Time & 0x1F; // met a 0 les bits 7 et 6 et 5 mois
    if (zone == 6)
        Time = Time;
    if (zone == 7)
        Time = Time; //registre control ex: 00010000 =) oscillation de 1hz out





    TMP = envoyerHORLOGE_i2c(0xD0, zone, Time);
    return (TMP);
}

unsigned char LIRE_HORLOGE(unsigned char zone) {
    unsigned char TMP;
    TMP = litHORLOGE_i2c(0xD1, zone);


    if (zone == 0)
        TMP = TMP & 0x7F; // supprimle le bit 7 seconde
    if (zone == 1)
        TMP = TMP & 0x7F; //laisse le bit7 a zero minutes
    if (zone == 2)
        TMP = TMP & 0x3F; //met a zero les bits 6 et 7 pour le mode 24h
    if (zone == 3)
        TMP = TMP & 0x07; // laisse ts les bits sauf 0 1 2 a 0
    if (zone == 4)
        TMP = TMP & 0x3F; // met a 0 les bits 7 et 6 date
    if (zone == 5)
        TMP = TMP & 0x1F; // met a 0 les bits 7 et 6 et 5 mois
    if (zone == 6)
        TMP = TMP;










    return (TMP);
}

void creer_trame_horloge(void) {

    char cAnneeCree, cMoisCree, cDateCree, cHeureCree, cMinuteCree, cSecondeCree;

    cAnneeCree = LIRE_HORLOGE(6);
    delay_ms(10);
    cMoisCree = LIRE_HORLOGE(5);
    delay_ms(10);
    cDateCree = LIRE_HORLOGE(4);
    delay_ms(10);
    //cJourCree = LIRE_HORLOGE(3);
    //delay_ms(10);
    cHeureCree = LIRE_HORLOGE(2);
    delay_ms(10);
    cMinuteCree = LIRE_HORLOGE(1);
    delay_ms(10);
    cSecondeCree = LIRE_HORLOGE(0);


    HORLOGE[0] = ((cHeureCree & 0xF0) >> 4) + 48; // conversion BCD => ASCII
    HORLOGE[1] = (cHeureCree & 0x0F) + 48;
    HORLOGE[2] = ((cMinuteCree & 0xF0) >> 4) + 48;
    HORLOGE[3] = (cMinuteCree & 0x0F) + 48;
    HORLOGE[4] = ((cSecondeCree & 0xF0) >> 4) + 48;
    HORLOGE[5] = (cSecondeCree & 0x0F) + 48;
    HORLOGE[6] = ((cDateCree & 0xF0) >> 4) + 48; // conversion BCD => ASCII
    HORLOGE[7] = (cDateCree & 0x0F) + 48;
    HORLOGE[8] = ((cMoisCree & 0xF0) >> 4) + 48;
    HORLOGE[9] = (cMoisCree & 0x0F) + 48;
    HORLOGE[10] = ((cAnneeCree & 0xF0) >> 4) + 48;
    HORLOGE[11] = (cAnneeCree & 0x0F) + 48;




    if ((HORLOGE[10] == '0')&&(HORLOGE[11] == '0')) {

        HORLOGE[0] = '0'; // conversion BCD => ASCII
        HORLOGE[1] = '7';
        HORLOGE[2] = '1';
        HORLOGE[3] = '5';
        HORLOGE[4] = '0';
        HORLOGE[5] = '0';
        HORLOGE[6] = '2';
        HORLOGE[7] = '9';
        HORLOGE[8] = '1';
        HORLOGE[9] = '1';
        HORLOGE[10] = '0';
        HORLOGE[11] = '2';

    }






    //HORLOGE[12];




}

void entrer_trame_horloge(void) {
    //ASCII->BCD
    char cAnneeEntrer, cMoisEntrer, cDateEntrer, cHeureEntrer, cMinuteEntrer, cSecondeEntrer;

    cHeureEntrer = (HORLOGE[0] - 48) << 4;
    cHeureEntrer = cHeureEntrer + (HORLOGE[1] - 48);
    cMinuteEntrer = (HORLOGE[2] - 48) << 4;
    cMinuteEntrer = cMinuteEntrer + (HORLOGE[3] - 48);
    cSecondeEntrer = (HORLOGE[4] - 48) << 4;
    cSecondeEntrer = cSecondeEntrer + (HORLOGE[5] - 48);
    cDateEntrer = (HORLOGE[6] - 48) << 4;
    cDateEntrer = cDateEntrer + (HORLOGE[7] - 48);
    cMoisEntrer = (HORLOGE[8] - 48) << 4;
    cMoisEntrer = cMoisEntrer + (HORLOGE[9] - 48);
    cAnneeEntrer = (HORLOGE[10] - 48) << 4;
    cAnneeEntrer = cAnneeEntrer + (HORLOGE[11] - 48);





    ECRIRE_HORLOGE(2, cHeureEntrer);
    ECRIRE_HORLOGE(1, cMinuteEntrer);
    ECRIRE_HORLOGE(0, cSecondeEntrer);
    ECRIRE_HORLOGE(4, cDateEntrer);
    ECRIRE_HORLOGE(5, cMoisEntrer);
    ECRIRE_HORLOGE(6, cAnneeEntrer);













    //HORLOGE[12];




}

char trouver_carte_suivantvoie(char *tab) {

    unsigned int i;
    unsigned int t;





    t = 0;
    i = tab[2] - 48;
    t = t + 1 * i;
    i = tab[1] - 48;
    t = t + 10 * i;
    i = tab[0] - 48;
    t = t + 100 * i;





    if (t <= 100)
        i = 5;
    if (t <= 80)
        i = 4;
    if (t <= 60)
        i = 3;
    if (t <= 40)
        i = 2;
    if (t <= 20)
        i = 1;



    return (i);


}

void TEMPO(char seconde) {
    char i;


    for (i = 0; i < seconde; i++) {
        delay_ms(100);
        delay_ms(100);
        delay_ms(100);
        delay_ms(100);
        delay_ms(100);
        delay_ms(100);
        delay_ms(100);
        delay_ms(100);
        delay_ms(100);
        delay_ms(100);
    }

}











































///////////////////////////////////////////////
//write one block*

unsigned char SD_WriteSingleBlock(unsigned long sector) {
    unsigned char r1;
    unsigned int i;
    unsigned char retry = 0;

    sector = sector << 8;

    spi_low();
    do {

        SPI_WriteByte(0xff);
        SPI_WriteByte(0xff);
        PORTDbits.RD7 = 0;
        r1 = SD_SendCommand(0x58, sector, 0xFF); //write command Cmd24.
        PORTDbits.RD7 = 1;

        retry++;
        //if(retry>10) return 1; //overtime,exit.
    } while (r1 == 0xFF);

    //cs=0;
    PORTDbits.RD7 = 0; // Disable Chip Select


    SPI_WriteByte(0xff);
    SPI_WriteByte(0xff);
    SPI_WriteByte(0xff);
    SPI_WriteByte(0xff);
    SPI_WriteByte(0xff);
    SPI_WriteByte(0xff);

    SPI_WriteByte(0xfe); //send start command.

    for (i = 0; i < 512; i++) //send 512 byte data.
    {
        if (i < 255) SPI_WriteByte(i); //send 0--255
        else SPI_WriteByte(i); //send 255--0

    }

    SPI_WriteByte(0x95);
    //SPI_WriteByte(0x95); //16-bits CRC

    r1 = SPI_WriteByte(0xff); //read ack bit.
    if (retry++ > 10)
        return 1; //overtime, exit.

    //while(!((r1&0x0f)==5)); //wait data success return info.

    while ((SPI_WriteByte(0xff) != 0x00)); //wait sd card internal program over.

    return 0;
}



////////////////////////////////////////////////
//read one block.

unsigned char SD_ReadSingleBlock(unsigned long sector) {
    unsigned int r1;
    unsigned int temp;
    unsigned int i, j;
    unsigned char retry = 0;

    sector = sector << 8;
    retry = 0;
    do {
        PORTDbits.RD7 = 0;
        r1 = SD_SendCommand(0x51, sector, 0xFF); //read command Cmd17.
        PORTDbits.RD7 = 1;
        retry++;
        if (retry > 10)
            return 1; //overtime,exit
    } while (r1 == 0xFF);
    retry = 0;
    //cs=0;
    PORTDbits.RD7 = 0; // Disable Chip Select

    while (SPI_WriteByte(0xff) != 0xfe) //wait to receive the start byte.
    {

        if (retry++ > 500)
            return 1; //overtime,exit
    }
    for (i = 0; i < 512; i++) //read 512 data
    {
        temp = SPI_WriteByte(0xff); //read the receiving data.


        TRAMEOUT[i] = temp;
        TRAMEOUT[i + 1] = temp << 8;
        //itoa(temp,aff);


        //delay_ms(3);
        delay_ms(30);

    }

    SPI_WriteByte(0xff); //false 16-bits crc
    SPI_WriteByte(0xff);

    //cs=1;
    PORTDbits.RD7 = 1; // Disable Chip Select

    return 0;
}

unsigned char SD_SendCommand(char CMD, unsigned long sct, char CRC) {
    unsigned char r1;
    unsigned char adrss;
    unsigned char retry1 = 0; //repeat count.
    //cs=0; //enable cs signal.
    //PORTDbits.RD7 = 0;     // Enable Chip Select

    r1 = SPI_WriteByte(CMD); //write command.
    r1 = SPI_WriteByte(sct >> 24); //data segment the 4th byte.
    r1 = SPI_WriteByte(sct >> 16); //data segment the 3th byte.
    r1 = SPI_WriteByte(sct >> 8); //data segment the 2nd byte.
    r1 = SPI_WriteByte(sct); //data segment the 1st byte.
    r1 = SPI_WriteByte(CRC); //CRC check sum.

    //r1 = SPI_WriteByte(0x00);
    //r1 = SPI_WriteByte(0x00);
    //while(() == 0xff)//wait ack.
    //if(retry1++ > 100)
    //break; //overtime exit.
    for (retry1 = 0; retry1 < 8; retry1++) {
        r1 = SPI_WriteByte(0x00);
        if (r1 != 0xFF)
            return r1;

    }





    //PORTDbits.RD7 = 1;     // Disable Chip Select
    //cs=1; //clear cs


    return r1; //return the status value.
}





///////////////////////////////////////////////
//write one byte.  // 8fronts d'horloge

unsigned char SPI_WriteByte(unsigned char val) {
    SSP2BUF = val; // Envoie du caract�re
    while (!PIR3bits.SSP2IF); // Attendre jusque envoi accompli

    PIR3bits.SSP2IF = 0; //clear send finish flag.
    return SSP2BUF; // Retourne le caract�re re�u
}








////////////////////////////////////////////////
//SD reset.

unsigned char sd_reset() {
    unsigned char r3;
    unsigned char i, tmp;
    unsigned char retry; //repeat times.

    retry = 0;

    spi_low(); //use low band.
    do {
        PORTDbits.RD7 = 1; // Disable Chip Select
        for (i = 0; i < 10; i++) {

            SPI_WriteByte(0xFF); //FAIRE 80=10*8 clocks a voir et mettre a 1 CS et DATAOUT

        }
        PORTDbits.RD7 = 0; // Enable Chip Select

        for (i = 0; i < 2; i++) {

            SPI_WriteByte(0xFF); //FAIRE 80=10*8 clocks a voir et mettre a 1 CS et DATAOUT

        }


        r3 = r3 + 1;
        TRAMEOUT[r3] = SD_SendCommand(0x40, 0, 0x95); //REMISE A ZERO DE LA SD SPI MODE
        if (r3 == 100)
            r3 = 0;


    } while (TRAMEOUT[r3] != 0x01); //wait Active command exit.



    //http://patrickleyman.be/blog/sd-card-c-driver-init/
    r3 = 0;
    do {
        r3 = r3 + 1;
        TRAMEOUT[r3] = SD_SendCommand(0x41, 0, 0xFF); //INIT CARTE
        if (r3 == 100)
            r3 = 0;

        //r3 = SD_SendCommand(0x69, 0, 0xFF); //INIT CARTE


    } while (TRAMEOUT[r3] != 0); //wait Active command exit.


    r3 = 0;


    do {
        r3 = r3 + 1;
        TRAMEOUT[r3] = SD_SendCommand(0x7B, 0, 0xFF); //INIT CARTE
        if (r3 == 100)
            r3 = 0;

        //r3 = SD_SendCommand(0x69, 0, 0xFF); //INIT CARTE


    } while (TRAMEOUT[r3] != 0); //wait Active command exit.



    //	do
    //	{
    //	r3=r3+1;
    //	 TRAMEOUT[r3] = SD_SendCommand(0x50, 512, 0xFF); //INIT CARTE
    //	if (r3==100)
    //	r3=0;

    //r3 = SD_SendCommand(0x69, 0, 0xFF); //INIT CARTE


    //	}
    //	while(TRAMEOUT[r3]!=0); //wait Active command exit.







    spi_high(); //use high band.
    return 0; //return normal.
}











////////////////////////////////////////////////
//set low band.

void spi_low() {
    SSP2CON1 = 0x22; //SPI clk use the system  375Khz (32)
}

////////////////////////////////////////////////
//set high band.

void spi_high() {
    SSP2CON1 = 0x21; //SPI clk use the system clk--fosc/64 0x31
}

void init_spi(void) {




    TRISDbits.TRISD7 = 0; // RD7/SS - Output (Chip Select)

    TRISDbits.TRISD4 = 0; // RD4/SDO - Output (Serial Data Out)
    TRISDbits.TRISD5 = 1; // RD5/SDI - Input (Serial Data In)
    TRISDbits.TRISD6 = 0; // RD6/SCK - Output (Clock)



    SSP2STATbits.SMP = 0; // input is valid in the middle of clock
    SSP2STATbits.CKE = 0; // 0  for rising edge is data capture


    SSP2CON1bits.CKP = 0; // high value is passive state

    SSP2CON1bits.SSPM3 = 0; // speed f/64(312kHz), Master
    SSP2CON1bits.SSPM2 = 0;
    SSP2CON1bits.SSPM1 = 1;
    SSP2CON1bits.SSPM0 = 0;

    SSP2CON1bits.SSPEN = 1; // enable SPI



    //SSP2STAT = 0x80;        // 0x80 rising edge,send data.
    //SSP2CON1 = 0x32;        //0x32 high when idle.fosc/64

    PORTDbits.RD7 = 1; // Disable Chip Select








}



// permet de faire une pause de x �s

void delay_us(char tempo) {
    char i;

    for (i = 0; i < tempo; i++) {
        //__no_operation();     // 5 pauses pour 1�s
        //__no_operation();
        //__no_operation();
        //__no_operation();
        //__no_operation();
    }
}

void SAV_EXISTENCE(char *tt) {


    unsigned int l;
    char i;



    l = tt[5] - 48;
    l = l * 10;
    l = l + tt[6] - 48;
    i = l;

    l = 128 * l + 30000; //04/07/2017

    TMP[0] = cDebutTrame;
    TMP[1] = i;
    for (i = 2; i < 34; i++) {
        TMP[i] = tt[5 + i];
    }

    //MULTI #.vFFFFFFFFFFFFFFFFF0vE99999*  .=0x01 en hexa
    if (TMP[20] == '0')
        numdecarteRI = 1;

    if (TMP[20] == '1')
        numdecarteRI = 2;

    if (TMP[20] == '2')
        numdecarteRI = 3;

    if (TMP[20] == '3')
        numdecarteRI = 4;

    if (TMP[20] == '4')
        numdecarteRI = 5;



    l = l + 2560 * (numdecarteRI - 1);






    //#'1'vFFFFFFFFFFFFFFFFF2vE99999* '1 en decimal'
    TMP[1] = 20 * (numdecarteRI - 1) + TMP[1];
    //DEVIENT EN MULTI
    //#'41'vFFFFFFFFFFFFFFFFF2vE99999* '41 en decimal'



    ECRIRE_EEPROMBYTE(3, l, &TMP, 4);

    //delay_ms(100);

    //LIRE_EEPROM(3,l,&TRAMEIN,4); //LIRE DANS EEPROM

    //delay_ms(100);

}

void intervertirOUT_IN(void)
 {

    int h;

    for (h = 0; h < 125; h++) {
        TRAMEIN[h] = TRAMEOUT[h];
    }

}

void RECHERCHE_DONNEES_GROUPE(unsigned char ici)
 {


    //char P_RESERVOIR[15];
    //char P_SORTIE[15];
    //char P_SECOURS[15];
    //char POINT_ROSEE[11];
    //char DEBIT_GLOBAL[11];
    //char TEMPS_CYCLE_MOTEUR[11];
    //char TEMPS_MOTEUR[15];
    //char PRESENCE_220V[6];
    //char PRESENCE_48V[6];




    int lui;
    if (ici == 0)
        lui = 0;
    if (ici == 1)
        lui = 128;
    if (ici == 2)
        lui = 256;



    LIRE_EEPROM(6, lui, &TMP, 3);


}

char EFFACER_CAPTEUR_SUR_CM(char gh) {
    char t, ss, aa, vv;

    ss = VOIECOD[0];
    aa = VOIECOD[1];
    vv = VOIECOD[2];



    VOIECOD[0] = DEMANDE_INFOS_CAPTEURS[0];
    VOIECOD[1] = DEMANDE_INFOS_CAPTEURS[1];
    VOIECOD[2] = DEMANDE_INFOS_CAPTEURS[2];




    NBRECAPTEURS_CABLE = 0;


    // VOIR COMBIEN IL EXISTE DE CAPTEURS
    for (t = 0; t < 17; t++) {
        CHERCHER_DONNNEES_CABLES(&VOIECOD, t); //CAPTEURS UN A UN
        if (capt_exist == 1) {
            NBRECAPTEURS_CABLE = NBRECAPTEURS_CABLE + 1;
        } else {

        }

    }

    if (gh == 1) //SI ON VEUT LE FAIRE
    {

        // LES DONNEES SONT RANGEES DANS DEMANDE_INFOS_CAPTEURS

        DEMANDE_INFOS_CAPTEURS[5] = DEMANDE_INFOS_CAPTEURS[4];
        DEMANDE_INFOS_CAPTEURS[4] = DEMANDE_INFOS_CAPTEURS[3];
        DEMANDE_INFOS_CAPTEURS[3] = DEMANDE_INFOS_CAPTEURS[2];
        DEMANDE_INFOS_CAPTEURS[2] = DEMANDE_INFOS_CAPTEURS[1];
        DEMANDE_INFOS_CAPTEURS[1] = DEMANDE_INFOS_CAPTEURS[0];
        DEMANDE_INFOS_CAPTEURS[0] = cDebutTrame;







        MODIFICATION_TABLE_DEXISTENCE(&DEMANDE_INFOS_CAPTEURS, 1, 0); // ON EFFACE LE CAPTEUR DANS LA MEMOIRE TABLE EXIST DE L'IO
    }


    VOIECOD[0] = ss;
    VOIECOD[1] = aa;
    VOIECOD[2] = vv;


    if (NBRECAPTEURS_CABLE <= 1)
        return (1); //CETAIT LE DERNIER
    else
        return (0);


}

void MODIFICATION_TABLE_DEXISTENCE(char *v, char eff, char etat) //eff pour effacer (eff=2 alors recopie) alors et etat ligne pour fusible HS ou CC ou surtension
{

    unsigned int y, z;
    char fa;




    v[0] = cDebutTrame;
    y = 100 * (v[1] - 48) + 10 * (v[2] - 48)+(v[3] - 48); //VOIE

    y = 128 * y + 30000; //04/07/2017

    LIRE_EEPROM(3, y, &TMP, 4); //LIRE DANS EEPROM









    z = 10 * (v[4] - 48)+(v[5] - 48); //CODAGE

    if (eff == 2) //RECOPIE
    {
        goto recop;
    }



    if (eff == 0) //CREATION attention ici on cr�e le type capteur et le type de cable ic on a pas encore test� si le cable avant etait resistif ou r codage en 2 3 ....il faut juste
    {
        TMP[z + 3] = v[6]; //RECOPIE SI R OU A #00101R
        fa = TMP[z + 3];



    }




    // VOIR voie a 'v' qd il y a que des 'F'
    //ecrire_tab_presence_TPA;
    // ATTENTION SI
    //if (TMP[z+3]=='v')
    // OK ON PEUT
    //if (TMP[z+3]=='a')
    // OK ON PEUT SI v[6]=='A'
    //if(TMP[z+3]=='r')
    // OK ON PEUT SI v[6]=='v'



    if (fa == 'A') {
        TMP[2] = 'a';

    }
    if (fa == 'R') //L'ADRESSABLE NE PEUT QUE EXISTER EN CODAGE1 !!!!!!!!!!!!!!!!!!!!!!
    {
        TMP[2] = 'r';
    }



    if (eff == 1) //EFFACEMENT
    {
        TMP[z + 3] = 'F'; //CAPTEUR EFFACEE DE LA TABLE DEXISTENCE
        // VOIR voie a 'v' qd il y a que des 'F'

        if (NBRECAPTEURS_CABLE <= 1) //SI IL N'Y A QUE UN CAPTEUR VARIABLES INIT DANS MENU6b
            TMP[2] = 'v'; // ON EFFACE LE CABLE



    }


recop:



    if (etat == 1) {
        TMP[2] = 'h';
    }

    if (etat == 2) {
        TMP[2] = 'h';
    }

    if ((etat == 4)&&(TMP[2] != 'c')) {
        TMP[2] = TMP[21];
    }

    if ((etat == 5)&&(TMP[2] != 'c')) {
        TMP[2] = TMP[21];

    }

    if (etat == 6) {
        TMP[2] = 'c';

    }


    if (etat == 7) {
        TMP[2] = 'c';
    }


    if (etat == 8) // SCRUTATION POUR INHIBER DEFAUT CC
    {
        TMP[2] = TMP[21];
    }




ecrire:


    //ecrire_tab_presence_TPR;
    ECRIRE_EEPROMBYTE(3, y, &TMP, 4);

    delay_ms(10);
    LIRE_EEPROM(3, y, &TMP, 4); //LIRE DANS EEPROM pour verifier



}

void CHERCHER_DONNNEES_CABLES(char *g, char j) //051 cable et codage j //DONNE LES DONNEES DU CABLE ET DU CODAGE CHOISSI ET MET UNE VARIABLE EXISTENCE A UN SI CAPTEUR EXISTE
{
    char i;
    char tati[6];
    //tmp=LIRE_EEPROM(choix_memoire(&g),calcul_addmemoire(&g),&TMP,1); //LIRE DANS EEPROM le cable
    unsigned int l;

    //    if (j == 111) // INIT COMMENTAIRE SI MENU 6a
    //    {
    //        for (i = 0; i < 19; i++) {
    //            COMMENTAIRE[i] = 0x20;
    //        }
    //    }




    tati[5] = '0';
    tati[4] = '0';
    tati[3] = g[2];
    tati[2] = g[1];
    tati[1] = g[0];
    tati[0] = cDebutTrame;





    if (j != 111) {
        tati[4] = j / 10 + 48; //CODAGE
        tati[5] = 48 + j - 10 * (tati[4] - 48);
    }


    l = 100 * (tati[1] - 48) + 10 * (tati[2] - 48) + 1 * (tati[3] - 48);
    l = 128 * l + 30000;



    LIRE_EEPROM(3, l, &TMP, 4); //LIRE DANS EEPROM lexistance du capteur




    capt_exist = 0;
    if (j == 111) // ON veut savoir s'il cable
    {
        j = 2; //CABLE
        if (TMP[j] == 'a') //VOIR i etc....
            capt_exist = 1;
        if (TMP[j] == 'r')
            capt_exist = 1;
        if (TMP[j] == 'c')
            capt_exist = 2; //CC
        if (TMP[j] == 'h')
            capt_exist = 3; //FUSIBLE
        if (TMP[j] == 'i')
            capt_exist = 4; //INCIDENT

    } else {
        j = 3 + j; ///CAPTEUR
        if (TMP[j] == 'A')
            capt_exist = 1;
        if ((TMP[2] == 'r')&&(tati[5] == '1')&&(tati[4] == '0')) //AVANT 'R' PAR CODAGE MNT r au debut
            capt_exist = 1;
    }






    if (capt_exist == 1) {
        LIRE_EEPROM(choix_memoire(&tati), calcul_addmemoire(&tati), &TMP, 1); //LIRE DANS EEPROM voir si TRAMEIN DISPO






        //recuperer_trame_memoire (0); //voir utiliser cela pour recuperations des variables VOIR STOCKAGE TRAME IN

        //VOIECOD[0]=TMP[4];
        //VOIECOD[1]=TMP[5];

        //        TYPE[0] = TMP[6];

        //        CMOD[0] = TMP[7];
        //        CMOD[1] = TMP[8];
        //        CMOD[2] = TMP[9];
        //        CMOD[3] = CMODTMP[10];

        //        CCON[0] = TMP[11];
        //        CCON[1] = TMP[12];
        //        CCON[2] = TMP[13];
        //        CCON[3] = TMP[14];
        //
        //        CREP[0] = TMP[15];
        //        CREP[1] = TMP[16];
        //        CREP[2] = TMP[17];
        //        CREP[3] = TMP[18];

        //        CONSTITUTION[0] = TMP[19];
        //        CONSTITUTION[1] = TMP[20];
        //        CONSTITUTION[2] = TMP[21];
        //        CONSTITUTION[3] = TMP[22];
        //        CONSTITUTION[4] = TMP[23];
        //        CONSTITUTION[5] = TMP[24];
        //        CONSTITUTION[6] = TMP[25];
        //        CONSTITUTION[7] = TMP[26];
        //        CONSTITUTION[8] = TMP[27];
        //        CONSTITUTION[9] = TMP[28];
        //        CONSTITUTION[10] = TMP[29];
        //        CONSTITUTION[11] = TMP[30];
        //        CONSTITUTION[12] = TMP[31];

        //VOIR POUR COMMENTAIRE
        //dans afficher_donnees_si_existe
        //        for (i = 0; i < 19; i++) {
        //            COMMENTAIRE[i] = TMP[32 + i];
        //        }



        //        CABLE[0] = TMP[50];
        //        CABLE[1] = TMP[51];
        //        CABLE[2] = TMP[52];
        //        JJ[0] = TMP[53];
        //        JJ[1] = TMP[54];
        //        MM[0] = TMP[55];
        //        MM[1] = TMP[56];
        //        HH[0] = TMP[57];
        //        HH[1] = TMP[58];
        //        mm[0] = TMP[59];
        //        mm[1] = TMP[60];

        //        ii[0] = TMP[61];

        //        COD[0] = TMP[62];
        //        COD[1] = TMP[63];
        //        POS[0] = TMP[64];
        //        POS[1] = TMP[65];
        //        iiii[0] = TMP[66];
        //        iiii[1] = TMP[67];
        //        iiii[2] = TMP[68];
        //        iiii[3] = TMP[69];





        //        DISTANCE[0] = TMP[70];
        //        DISTANCE[1] = TMP[71];
        //        DISTANCE[2] = TMP[72];
        //        DISTANCE[3] = TMP[73];
        //        DISTANCE[4] = TMP[74];

        ETAT = TMP[75];
        //        ETAT[1] = TMP[76];



        //        VALEUR[0] = TMP[77];
        //        VALEUR[1] = TMP[78];
        //        VALEUR[2] = TMP[79];
        //        VALEUR[3] = TMP[80];


        //        SEUIL[0] = TMP[81];
        //        SEUIL[1] = TMP[82];
        //        SEUIL[2] = TMP[83];
        //        SEUIL[3] = TMP[84];







    } else {



        //
        //
        //        CMOD[0] = '0';
        //        CMOD[1] = '0';
        //        CMOD[2] = '0';
        //        CMOD[3] = '0';

        //        CCON[0] = '0';
        //        CCON[1] = '0';
        //        CCON[2] = '0';
        //        CCON[3] = '0';
        //
        //        CREP[0] = '0';
        //        CREP[1] = '0';
        //        CREP[2] = '0';
        //        CREP[3] = '0';


        //        for (i = 0; i < 13; i++) {
        //            CONSTITUTION[i] = 0x20;
        //        }




        //        CABLE[0] = VOIECOD[0];
        //        CABLE[1] = VOIECOD[1];
        //        CABLE[2] = VOIECOD[2];


        //        JJ[0] = '0';
        //        JJ[1] = '0';
        //        MM[0] = '0';
        //        MM[1] = '0';
        //        HH[0] = '0';
        //        HH[1] = '0';
        //        mm[0] = '0';
        //        mm[1] = '0';


        //        ii[0] = '?';



        //        COD[0] = VOIECOD[3];
        //        COD[1] = VOIECOD[4];
        //        l = 10 * (COD[0] - 48)+(COD[1] - 48) + 1;
        l = 10 * (VOIECOD[3] - 48)+(VOIECOD[4] - 48) + 1; //Revient au m�me qu'avant car  COD[0] et COD[1] prenaient forcement les valeurs de VOIECODE[3] et VOIECODE[4]


        //        IntToChar(l, &POS, 2); /// convertir le POS en CHAR pour l'envoyer





        //        iiii[0] = '?';
        //        iiii[1] = '?';
        //        iiii[2] = '?';
        //        iiii[3] = '?';



        //        VALEUR[0] = '?';
        //        VALEUR[1] = '?';
        //        VALEUR[2] = '?';
        //        VALEUR[3] = '?';




        //        SEUIL[0] = '0';
        //        SEUIL[1] = '8';
        //        SEUIL[2] = '0';
        //        SEUIL[3] = '0';

        //        for (i = 0; i < 6; i++) {
        //            DISTANCE[i] = '0';
        //        }

        ETAT = '0';
        //        ETAT[1] = '0';




    }

}

char VALIDER_CAPTEUR_CM(void) {
    char t, u;






    for (t = 0; t < 125; t++) // ON RECOPIE TRAMEOUT
        TMP[t] = TRAMEOUT[t];


    // ON COPIE DABORT LE COMMENTAIRES CABLES EN CODAGE 0
    TRAMEOUT[11] = '0';
    TRAMEOUT[12] = '0';
    TRAMEOUT[69] = '0';
    TRAMEOUT[70] = '0';
    TRAMEOUT[71] = '0';
    TRAMEOUT[72] = '1';
    intervertirOUT_IN();

    recuperer_trame_memoire(1, 'a'); //STOCKAGE EN MMOIRE DE L'INFORMATION DU CAPTEUR ici juste pour commentaire cable



    for (t = 0; t < 125; t++) //ON RECOPIE DANS TRAMEOUT
        TRAMEOUT[t] = TMP[t];
    intervertirOUT_IN();
    recuperer_trame_memoire(1, 'a'); //STOCKAGE EN MMOIRE DE L'INFORMATION DU CAPTEUR

    //ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEOUT),calcul_addmemoire(&TRAMEOUT),&TRAMEOUT,1); //ECRITURE EN EEPROM
    MODIFICATION_TABLE_DEXISTENCE(&TRAMEOUT, 0, 0); //MODIFICATION DE LA TABLE
    //ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEOUT),calcul_addmemoire(&TRAMEOUT),&TRAMEOUT,1); //ECRITURE EN EEPROM LE CABLE
    delay_ms(10);
    //TRAMEOUT[4]='0';
    //TRAMEOUT[5]='0'; //CODAGE 0
    LIRE_EEPROM(choix_memoire(&TRAMEOUT), calcul_addmemoire(&TRAMEOUT), &TRAMEIN, 1); //LIRE DANS EEPROM LIRE LE CAPTEUR 0 C ICI QUON IRA LIRE LE COMMENTAIRE CABLE

    delay_ms(10);







}

void IDENTIF_TYPE_CARTE(void)
 {
    unsigned int nbint;



    MESURE_DES_TENSIONS();

    //de 0 � 20 carte R1

    nbint = 1; //carte R1
    LIRE_EEPROM(3, 128 * nbint + 30000, &TMP, 4); //LIRE DANS EEPROM TABLE EXISTENCE
    if (TMP[22] == 'M') //TYPE RESITIVE
        TYPECARTE[0] = 'M';
    else
        TYPECARTE[0] = 'E'; //TYPE ELECTRIQUE
    if (VDD1 <= 2)
        TYPECARTE[0] = 'V'; //PAS DE TENSION CARTE HS ou absente
    if (R1NOREP == 12)
        TYPECARTE[0] = 'V'; //CARTE DEFECTUEUSE

    nbint = 21; //carte R2
    LIRE_EEPROM(3, 128 * nbint + 30000, &TMP, 4); //LIRE DANS EEPROM
    if (TMP[22] == 'M')
        TYPECARTE[1] = 'M';
    else
        TYPECARTE[1] = 'E';
    if (VDD2 <= 2)
        TYPECARTE[1] = 'V';
    if (R2NOREP == 12)
        TYPECARTE[1] = 'V'; //CARTE DEFECTUEUSE



    nbint = 41; //carte R3
    LIRE_EEPROM(3, 128 * nbint + 30000, &TMP, 4); //LIRE DANS EEPROM
    if (TMP[22] == 'M')
        TYPECARTE[2] = 'M';
    else
        TYPECARTE[2] = 'E';
    if (VDD3 <= 2)
        TYPECARTE[2] = 'V';
    if (R3NOREP == 12)
        TYPECARTE[2] = 'V'; //CARTE DEFECTUEUSE



    nbint = 61; //carte R4
    LIRE_EEPROM(3, 128 * nbint + 30000, &TMP, 4); //LIRE DANS EEPROM
    if (TMP[22] == 'M')
        TYPECARTE[3] = 'M';
    else
        TYPECARTE[3] = 'E';
    if (VDD4 <= 2)
        TYPECARTE[3] = 'V';
    if (R4NOREP == 12)
        TYPECARTE[3] = 'V'; //CARTE DEFECTUEUSE



    nbint = 81; //carte R5
    LIRE_EEPROM(3, 128 * nbint + 30000, &TMP, 4); //LIRE DANS EEPROM
    if (TMP[22] == 'M')
        TYPECARTE[4] = 'M';
    else
        TYPECARTE[4] = 'E';
    if (VDD5 <= 2)
        TYPECARTE[4] = 'V';
    if (R5NOREP == 12)
        TYPECARTE[4] = 'V'; //CARTE DEFECTUEUSE



}

void TROUVER_ROBINET(void) { //MAXIMUM CETTE FONCTION PEUT DURER 5S
    int t;
    char robinn[3];
    char u;

    u = 0;
    for (t = 2; t <= 13; t++)
        INSTRUCTION[t] = 0;



    for (t = 0; t < 100; t++) // LES 100 voies
    {


        IntToChar(t, &DEMANDE_INFOS_CAPTEURS, 3);



        DEMANDE_INFOS_CAPTEURS[3] = DEMANDE_INFOS_CAPTEURS[2];
        DEMANDE_INFOS_CAPTEURS[2] = DEMANDE_INFOS_CAPTEURS[1];
        DEMANDE_INFOS_CAPTEURS[1] = DEMANDE_INFOS_CAPTEURS[0];



        // ON REGARDE SUR LE DEBIT EN PREMIER FAIRE ATTENTION QUE LORS DE LA SCRUTATION SI A OU R LES COMMENTAIRES SOIENT AUSSI ECRIS EN CODAGE 00  et que si on cr�e un capteur on ecrit aussi le commentaire en 0
        DEMANDE_INFOS_CAPTEURS[0] = cDebutTrame;
        DEMANDE_INFOS_CAPTEURS[4] = '0';
        DEMANDE_INFOS_CAPTEURS[5] = '0';


        LIRE_EEPROM(choix_memoire(&DEMANDE_INFOS_CAPTEURS), calcul_addmemoire(&DEMANDE_INFOS_CAPTEURS), &TMP, 1); //LIRE DANS EEPROM
        DEMANDE_INFOS_CAPTEURS[0] = cDebutTrame;

        robinn[0] = TMP[32]; // ON REGARDE LE COMMMENTAIRE ET SI ON TROUVE Rbxx ou xx est le numero du robinet recherch�
        robinn[1] = TMP[33];
        robinn[2] = TMP[34];
        robinn[3] = TMP[35];




        if ((robinn[0] == 'R') &&(robinn[1] == 'o')) //avant Rb
        {
            if ((robinn[2] == INSTRUCTION[0]) &&(robinn[3] == INSTRUCTION[1])) //compare N�robinet dedans a celui souhait�
            {
                u++;
                if (u <= 10) // on arrete AU BOUT DE 10 VOIES SUR LE MEME ROBINET
                {
                    INSTRUCTION[u + 2] = t;
                }
                INSTRUCTION[13] = u; //ICI ON INDIQUE LE NOMBRE TROUVE MEME SI PLUS QUE 10


            }










        }



    }


}





//void voir_si_voie_existe(void)
//{
//VOIECOD[0]='0';
//VOIECOD[1]='0';
//VOIECOD[2]='1';

//CHERCHER_DONNNEES_CABLES(&VOIECOD,111); //CAPTEURS UN A UN
//if (capt_exist==1)
//{

//}

//}

void REMISE_EN_FORME_PAGE(void) {

    //ON CACULE LES ETATS SUIVANT CAHIER DES CHARGES A VOIR PUIS ON REDUIT LA TRAME
    //#CMAUZAC041xxxxxxxxxxxxxxxxxxA00FF013102FF03FF04FF05FF06FF07FF08FF09FF10FF11FF12FF13FF14FF15FF16FF22222*

    if (TRAMEOUT[29] == 'F') {
        TRAMEOUT[30] = 'F';
        TRAMEOUT[31] = 'F';
    }

    //SI 1 etat 30 alors une alarme prioritaire
    //SI 1 int 01 alors intervention mais aucune alarme

    //PAR DEFAUT OK POUR L'INSTANT on pourrait H/G ET CC et FUSIBLE et i incident Res sur adre

    TRAMEOUT[30] = '0';
    TRAMEOUT[31] = '0';



    //SI INCIDENT PAT
    if (TRAMEOUT[32] == '9') {
        TRAMEOUT[30] = '9';
        TRAMEOUT[31] = '9';
    }
    //SI CC PAT
    if (TRAMEOUT[32] == '8') {
        TRAMEOUT[30] = '8';
        TRAMEOUT[31] = '8';
    }
    //SI FUS PAT
    if (TRAMEOUT[32] == '7') {
        TRAMEOUT[30] = '7';
        TRAMEOUT[31] = '7';
    }



    //SI AU MOINS UN H/G SUR LA VOIE

    if ((TRAMEOUT[32] == '4') || (TRAMEOUT[36] == '4') || (TRAMEOUT[40] == '4') || (TRAMEOUT[44] == '4') || (TRAMEOUT[48] == '4') || (TRAMEOUT[52] == '4') || (TRAMEOUT[56] == '4') || (TRAMEOUT[60] == '4') || (TRAMEOUT[64] == '4') || (TRAMEOUT[68] == '4') || (TRAMEOUT[72] == '4') || (TRAMEOUT[76] == '4') || (TRAMEOUT[80] == '4') || (TRAMEOUT[84] == '4') || (TRAMEOUT[88] == '4') || (TRAMEOUT[92] == '4') || (TRAMEOUT[96] == '4')) {
        TRAMEOUT[30] = '3';
        TRAMEOUT[31] = '0';
    }



    //SI AU MOINS UNE NON REPONSE SUR LA VOIE

    if ((TRAMEOUT[32] == '5') || (TRAMEOUT[36] == '5') || (TRAMEOUT[40] == '5') || (TRAMEOUT[44] == '5') || (TRAMEOUT[48] == '5') || (TRAMEOUT[52] == '5') || (TRAMEOUT[56] == '5') || (TRAMEOUT[60] == '5') || (TRAMEOUT[64] == '5') || (TRAMEOUT[68] == '5') || (TRAMEOUT[72] == '5') || (TRAMEOUT[76] == '5') || (TRAMEOUT[80] == '5') || (TRAMEOUT[84] == '5') || (TRAMEOUT[88] == '5') || (TRAMEOUT[92] == '5') || (TRAMEOUT[96] == '5')) {
        TRAMEOUT[30] = '3';
        TRAMEOUT[31] = '0';
    }




    //SI AU MOINS UNE ALARME SUR LA VOIE

    if ((TRAMEOUT[32] == '3') || (TRAMEOUT[36] == '3') || (TRAMEOUT[40] == '3') || (TRAMEOUT[44] == '3') || (TRAMEOUT[48] == '3') || (TRAMEOUT[52] == '3') || (TRAMEOUT[56] == '3') || (TRAMEOUT[60] == '3') || (TRAMEOUT[64] == '3') || (TRAMEOUT[68] == '3') || (TRAMEOUT[72] == '3') || (TRAMEOUT[76] == '3') || (TRAMEOUT[80] == '3') || (TRAMEOUT[84] == '3') || (TRAMEOUT[88] == '3') || (TRAMEOUT[92] == '3') || (TRAMEOUT[96] == '3')) {
        TRAMEOUT[30] = '3';
        TRAMEOUT[31] = '0';
    }





    //SI TT en  INTERVENTION
    if (((TRAMEOUT[33] == '1') || (TRAMEOUT[33] == 'F'))&&((TRAMEOUT[37] == '1') || (TRAMEOUT[37] == 'F'))&&((TRAMEOUT[41] == '1') || (TRAMEOUT[41] == 'F'))&&((TRAMEOUT[45] == '1') || (TRAMEOUT[45] == 'F'))&&((TRAMEOUT[49] == '1') || (TRAMEOUT[49] == 'F'))&&((TRAMEOUT[53] == '1') || (TRAMEOUT[53] == 'F'))&&((TRAMEOUT[57] == '1') || (TRAMEOUT[57] == 'F'))&&((TRAMEOUT[61] == '1') || (TRAMEOUT[61] == 'F'))&&((TRAMEOUT[65] == '1') || (TRAMEOUT[65] == 'F'))&&((TRAMEOUT[69] == '1') || (TRAMEOUT[69] == 'F'))&&((TRAMEOUT[73] == '1') || (TRAMEOUT[73] == 'F'))&&((TRAMEOUT[77] == '1') || (TRAMEOUT[77] == 'F'))&&((TRAMEOUT[81] == '1') || (TRAMEOUT[81] == 'F'))&&((TRAMEOUT[85] == '1') || (TRAMEOUT[85] == 'F'))&&((TRAMEOUT[89] == '1') || (TRAMEOUT[89] == 'F'))&&((TRAMEOUT[93] == '1') || (TRAMEOUT[93] == 'F'))&&((TRAMEOUT[97] == '1') || (TRAMEOUT[97] == 'F'))) {
        if (TRAMEOUT[30] == '3')
            TRAMEOUT[30] = '3'; // SI UNE ALARME PRECEDENTE
        else
            TRAMEOUT[30] = '0';

        TRAMEOUT[31] = '1';

    }

    //si tt en intervention sauf certains OK
    if (((TRAMEOUT[32] == '0') || (TRAMEOUT[33] == '1') || (TRAMEOUT[33] == 'F'))&&((TRAMEOUT[36] == '0') || (TRAMEOUT[37] == '1') || (TRAMEOUT[37] == 'F'))&&((TRAMEOUT[40] == '0') || (TRAMEOUT[41] == '1') || (TRAMEOUT[41] == 'F'))&&((TRAMEOUT[44] == '0') || (TRAMEOUT[45] == '1') || (TRAMEOUT[45] == 'F'))) {

        if (((TRAMEOUT[48] == '0') || (TRAMEOUT[49] == '1') || (TRAMEOUT[49] == 'F'))&&((TRAMEOUT[52] == '0') || (TRAMEOUT[53] == '1') || (TRAMEOUT[53] == 'F'))&&((TRAMEOUT[56] == '0') || (TRAMEOUT[57] == '1') || (TRAMEOUT[57] == 'F'))&&((TRAMEOUT[60] == '0') || (TRAMEOUT[61] == '1') || (TRAMEOUT[61] == 'F'))&&((TRAMEOUT[64] == '0') || (TRAMEOUT[65] == '1') || (TRAMEOUT[65] == 'F'))&&((TRAMEOUT[68] == '0') || (TRAMEOUT[69] == '1') || (TRAMEOUT[69] == 'F'))&&((TRAMEOUT[72] == '0') || (TRAMEOUT[73] == '1') || (TRAMEOUT[73] == 'F'))&&((TRAMEOUT[76] == '0') || (TRAMEOUT[77] == '1') || (TRAMEOUT[77] == 'F'))&&((TRAMEOUT[80] == '0') || (TRAMEOUT[81] == '1') || (TRAMEOUT[81] == 'F'))&&((TRAMEOUT[84] == '0') || (TRAMEOUT[85] == '1') || (TRAMEOUT[85] == 'F'))&&((TRAMEOUT[88] == '0') || (TRAMEOUT[89] == '1') || (TRAMEOUT[89] == 'F'))&&((TRAMEOUT[92] == '0') || (TRAMEOUT[93] == '1') || (TRAMEOUT[93] == 'F'))&&((TRAMEOUT[96] == '0') || (TRAMEOUT[97] == '1') || (TRAMEOUT[97] == 'F'))) {
            //mais si tt est ok avec ou pas des vides
            if (((TRAMEOUT[32] == '0') || (TRAMEOUT[33] == 'F'))&&((TRAMEOUT[36] == '0') || (TRAMEOUT[37] == 'F'))&&((TRAMEOUT[40] == '0') || (TRAMEOUT[41] == 'F'))&&((TRAMEOUT[44] == '0') || (TRAMEOUT[45] == 'F'))&&((TRAMEOUT[48] == '0') || (TRAMEOUT[49] == 'F'))&&((TRAMEOUT[52] == '0') || (TRAMEOUT[53] == 'F'))&&((TRAMEOUT[56] == '0') || (TRAMEOUT[57] == 'F'))&&((TRAMEOUT[60] == '0') || (TRAMEOUT[61] == 'F'))&&((TRAMEOUT[64] == '0') || (TRAMEOUT[65] == 'F'))&&((TRAMEOUT[68] == '0') || (TRAMEOUT[69] == 'F'))&&((TRAMEOUT[72] == '0') || (TRAMEOUT[73] == 'F'))&&((TRAMEOUT[76] == '0') || (TRAMEOUT[77] == 'F'))&&((TRAMEOUT[80] == '0') || (TRAMEOUT[81] == 'F'))&&((TRAMEOUT[84] == '0') || (TRAMEOUT[85] == 'F'))&&((TRAMEOUT[88] == '0') || (TRAMEOUT[89] == 'F'))&&((TRAMEOUT[92] == '0') || (TRAMEOUT[93] == 'F'))&&((TRAMEOUT[96] == '0') || (TRAMEOUT[97] == 'F')))
                goto nepasint;

            if (TRAMEOUT[30] == '3')
                TRAMEOUT[30] = '3'; // SI UNE ALARME PRECEDENTE
            else
                TRAMEOUT[30] = '0';





            TRAMEOUT[31] = '1';
        }

    }
nepasint:

    pointeur_out_in = 32;
    calcul_CRC(); //COMPLETE LA TRAME TRAMEOUT[] AVEC LE CRC


















}








//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////// v�rification du CRC des trames recue ///////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void CRC_verif(void) {

    char CRCcal[5];
    unsigned int fft;
    //ON RECHERCHE LE CRC
    CRC[0] = TRAMEIN[pointeur_out_in];
    CRC[1] = TRAMEIN[pointeur_out_in + 1];
    CRC[2] = TRAMEIN[pointeur_out_in + 2];
    CRC[3] = TRAMEIN[pointeur_out_in + 3];
    CRC[4] = TRAMEIN[pointeur_out_in + 4];





    fft = CRC16(TRAMEIN, pointeur_out_in - 1);

    IntToChar5(fft, CRCcal, 5);

    if ((CRC[0] == CRCcal[0]) && (CRC[1] == CRCcal[1]) && (CRC[2] == CRCcal[2]) && (CRC[3] == CRCcal[3]) && (CRC[4] == CRCcal[4]))
        CRC_ETAT = 0xFF; //CRC CORRECT
    else
        CRC_ETAT = 0x00; //CRC INCORRECT




}




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// calcul du CRC pour /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CRC_POLY 0x01021 //0x01021
#define CRC_START 0xFFFF

unsigned int CRC16(char far *txt, unsigned char lg_txt) {
    unsigned char ii, nn;
    unsigned int crc;

    char far *p;
    crc = CRC_START;
    p = txt;

    //essai[2]=*p;
    ii = 0;
    nn = 0;
    //for( ii=0; ii<lg_txt ; ii++, p++)
    do {
        crc ^= (unsigned int) *p;
        //for(; nn<8; nn++)
        do {
            if (crc & 0x8000) {
                crc <<= 1;
                //UTIL;
                crc ^= CRC_POLY;
            } else {
                crc <<= 1;
            }
            nn++;
        } while (nn < 8);
        ii = ii + 1;
        p++;
        if (nn == 8) {
            nn = 0;
        }
    } while (ii < lg_txt);
    ii = 0;


    return crc;


}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////// CONVERSION UNSIGNED INT TO CHAR/////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void IntToChar5(unsigned int value, char *chaine, char Precision) {
    unsigned int Mil, Cent, Diz, Unit, DMil;
    int count = 0;

    // initialisation des variables
    Mil = 0;
    Cent = 0;
    Diz = 0;
    Unit = 0;
    DMil = 0;
    /*
    if (value < 0) // si c'est un nombre n�gatif
        {
        value = value * (-1);
     *chaine = 45; // signe '-'
        count = 1;  // c'est un nombre n�gatif
        }
    else // si c'est un nombre positif
        {
        count = 1;
     *chaine = 32; // code ASCII du signe espace ' '
        }
     */
    // si la valeur n'est pas nulle
    if (value != 0) {
        if (Precision >= 5) // si l'utilisateur d�sire les milliers
        {
            // conversion des milliers
            DMil = value / 10000;
            if (DMil != 0) {
                *(chaine + count) = DMil + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }
        if (Precision >= 4) // si l'utilisateur d�sire les milliers
        {
            // conversion des milliers
            Mil = value - (DMil * 10000);
            Mil = Mil / 1000;
            if (Mil != 0) {
                *(chaine + count) = Mil + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        if (Precision >= 3) // si l'utilisateur d�sire les centaines
        {
            // conversion des centaines
            Cent = value - (Mil * 1000)-(DMil * 10000);
            Cent = Cent / 100;
            if (Cent != 0) {
                *(chaine + count) = Cent + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        if (Precision >= 2) // si l'utilisateur d�sire les dizaines
        {
            // conversion des dizaines
            Diz = value - (Mil * 1000) - (Cent * 100)-(DMil * 10000);
            Diz = Diz / 10;
            if (Diz != 0) {
                *(chaine + count) = Diz + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        // conversion unit�s
        Unit = value - (Mil * 1000) - (Cent * 100) - (Diz * 10)-(DMil * 10000);
        *(chaine + count) = Unit + 48;
        if (*(chaine + count) < 48) // limites : 0 et 9
            *(chaine + count) = 48;
        if (*(chaine + count) > 57)
            *(chaine + count) = 57;
    } else // if (value == 0)
    {
        //*(chaine) = 32; // ecrit un espace devant le nombre
        for (Mil = 0; Mil < Precision; Mil++) // inscription de '0' dans toute la chaine
            *(chaine + Mil) = 48;
    }

} // fin de la fonction de conversion INT => CHAR

void SDWBN(void) {

    int icalculBLOCSWBN; //Remplacement de calculBLOC par iCalculBLOCSWBN
    unsigned int VV, bb;
    unsigned char tmpvoiecod[6], tmpchar[3];
    char flag;

    TEMPO(5);

    envoyer_TRAME_PICSD(0xA7); //START  ECRIRE DANS SD 


    TEMPO(2);


    for (VV = 1; VV < 22; VV++) {



        DX10 = !DX10;
        DX20 = !DX20;

        icalculBLOCSWBN = 2048 + (VV - 1)*128;
        TEMPO(1);
        LIRE_EEPROM(6, icalculBLOCSWBN, &TRAMEOUT, 6); //LIRE 85caract		
        //TRAMEOUT[90]='*';
        envoyer_TRAME_PICSD(0xEE); //EM

        delay_ms(10);

    }



    envoyer_TRAME_PICSD(0xFF); //FIN DE TRANS.

    DX10 = 1;
    DX20 = 1;


}

void SDW(char yui) {

    char cSAVTABSwd[25]; //Remplace le tableau global SAVTAB utilis� dans SWD
    unsigned int VV, ZO, bb;
    unsigned char tmpvoiecod[6], tmpchar[3];
    char flag;

    //Test
    //voir_si_voie_existe();


    //goto rec;
    TEMPO(5);
    //ECRIRE DES TRAMES EN SD
    if (yui == 'D')
        envoyer_TRAME_PICSD(0xA2); //START  ECRIRE DANS SD MODE DEMARRAGE
    if (yui == 'C')
        envoyer_TRAME_PICSD(0xA3); //START  ECRIRE DANS SD MODE CYCLIQUE
    if (yui == 'M')
        envoyer_TRAME_PICSD(0xA4); //START  ECRIRE DANS SD MODE MINITEL
    if (yui == 'N')
        envoyer_TRAME_PICSD(0xA5); //START  ECRIRE DANS SD MODE MINITEL

    TEMPO(2);

sautecarte:
    for (VV = 1; VV < 103; VV++) {
        flag = 0;
        if (VV == 101) //INFORMATIONS SITE
        {
            LIRE_EEPROM(6, 1024, &TRAMEOUT, 5); //TRAME CONFIG

            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = '1';
            TRAMEOUT[2] = '0';
            TRAMEOUT[3] = '1';
            TRAMEOUT[90] = cFinTrame;
            envoyer_TRAME_PICSD(0xEE); //EM
        }
        if (VV == 102) //INFORMATIONS SITE
        {
            IDENTIF_TYPE_CARTE();
            TRAMEOUT[0] = cDebutTrame;
            TRAMEOUT[1] = '1';
            TRAMEOUT[2] = '0';
            TRAMEOUT[3] = '2';
            TRAMEOUT[4] = TYPECARTE[0];
            TRAMEOUT[5] = TYPECARTE[1];
            TRAMEOUT[6] = TYPECARTE[2];
            TRAMEOUT[7] = TYPECARTE[3];
            TRAMEOUT[8] = TYPECARTE[4];
            TRAMEOUT[90] = cFinTrame;
            envoyer_TRAME_PICSD(0xEE); //EM
        }

        if ((VDD1 <= 2)&&(VV < 21)) //PAS DE CARTE R1 16092020
        {
            flag = 1;
        }
        if ((VDD2 <= 2)&&(VV >= 21)&&(VV < 41)) //PAS DE CARTE R2
        {
            flag = 1;
        }
        if ((VDD3 <= 2)&&(VV >= 41)&&(VV < 61)) //PAS DE CARTE R3
        {
            flag = 1;
        }
        if ((VDD4 <= 2)&&(VV >= 61)&&(VV < 81)) //PAS DE CARTE R4
        {
            flag = 1;
        }
        if ((VDD5 <= 2)&&(VV >= 81)) //PAS DE CARTE R5
        {
            flag = 1;
        }

        if (flag == 0) {
            DX10 = !DX10;
            DX20 = !DX20;
            ZO = 128 * VV + 30000;
            LIRE_EEPROM(3, ZO, &cSAVTABSwd, 4); //LIRE DANS EEPROM lexistance de toutes le voies


            if (cSAVTABSwd[2] == 'a') //Voie ADRESSABLE
            {
                for (bb = 0; bb < 17; bb++) {
                    IntToChar(VV, &tmpchar, 3); //ON TRANSFORME EN CHAR le numero de voie
                    tmpvoiecod[0] = cDebutTrame;
                    tmpvoiecod[1] = tmpchar[0];
                    tmpvoiecod[2] = tmpchar[1];
                    tmpvoiecod[3] = tmpchar[2];
                    IntToChar(bb, &tmpchar, 2); //ON TRANSFORME EN CHAR le numero de codage
                    tmpvoiecod[4] = tmpchar[0];
                    tmpvoiecod[5] = tmpchar[1];

                    LIRE_EEPROM(choix_memoire(&tmpvoiecod), calcul_addmemoire(&tmpvoiecod), &TRAMEOUT, 1); //LIRE DANS EEPROM
                    //TRAMEOUT[3]=VV;
                    TRAMEOUT[85] = '2'; // AJOUT CRC ICI ON RUSE LE CRC=E22222 pour le codage0 -> existence du debitmetre sinon F2222
                    TRAMEOUT[86] = '2';
                    TRAMEOUT[87] = '2';
                    TRAMEOUT[88] = '2';
                    TRAMEOUT[89] = '2';
                    TRAMEOUT[90] = cFinTrame;



                    if (bb == 0) //LE CODAGE 0 est TOUJOURS envoy� car il contient avec certitude les infos cable
                    {
                        if (cSAVTABSwd[3] == 'A') {
                            TRAMEOUT[85] = 'E';
                        }
                        if (cSAVTABSwd[3] != 'A') {
                            TRAMEOUT[85] = 'F';
                        }
                        envoyer_TRAME_PICSD(0xEE); //EM

                    }

                    if (cSAVTABSwd[3 + bb] == 'A') //ON ENVOIE QUE LES CAPTEUS ADRESSABLES
                        envoyer_TRAME_PICSD(0xEE); //EM

                    delay_ms(10);
                }
            }

            if (cSAVTABSwd[2] == 'r') //Voie RESISTIVE
            {

                IntToChar(VV, &tmpchar, 3); //ON TRANSFORME EN CHAR le numero de voie
                tmpvoiecod[0] = cDebutTrame;
                tmpvoiecod[1] = tmpchar[0];
                tmpvoiecod[2] = tmpchar[1];
                tmpvoiecod[3] = tmpchar[2];
                tmpvoiecod[4] = '0';
                tmpvoiecod[5] = '1';
                LIRE_EEPROM(choix_memoire(&tmpvoiecod), calcul_addmemoire(&tmpvoiecod), &TRAMEOUT, 1); //LIRE DANS EEPROM
                TRAMEOUT[85] = '2'; // AJOUT CRC
                TRAMEOUT[86] = '2';
                TRAMEOUT[87] = '2';
                TRAMEOUT[88] = '2';
                TRAMEOUT[89] = '2';
                TRAMEOUT[90] = cFinTrame;



                envoyer_TRAME_PICSD(0xEE); //EM
                delay_ms(10);


            }

            //	if (SAVTAB[2]=='i') //Voie INCIDENT

            //	if (SAVTAB[2]=='h') //Voie FUSIBLE HS

            //	TRAMEOUT[3]=VV;
            //			envoyer_TRAME_PICSD(0xEE); //EM
            //			delay_ms(10);
        }
    }

    //TRAMEOUT[3]='5';
    envoyer_TRAME_PICSD(0xFF); //FIN DE TRANS.
    //RECEVOIR DES TRAMES SD

    DX10 = 1;
    DX20 = 1;


}

void SDRBN(void) {
    char gigi;


recBN:

    TEMPO(3);

    TRAMEIN[1] = '0';
    TRAMEIN[2] = '0';
    do {
        DX10 = !DX10;
        DX20 = !DX20;


        //TEMPO(1);
        delay_ms(100);
        envoyer_TRAME_PICSD(0xA9); //START  RECEVOIR POUR PROGRAMMER
        //TEMPO(1);
        delay_ms(100);
        //delay_ms(10);
        envoyer_TRAME_PICSD(0x00); //RECEP faire Nx jusqua $$$
        if (TRAMEIN[2] == '$') {
            TEMPO(1);
            goto finwhiBN;
            delay_ms(10);
        }


        if (TRAMEIN[4] == 50) {
            delay_ms(10);
            delay_ms(10);
        }


        delay_ms(10);
        for (gigi = 0; gigi < 99; gigi++) //-#021..... ce que lon recoit
        {

            TRAMEOUT[gigi] = TRAMEIN[gigi + 1];

        }


        TRAMEOUT[0] = cDebutTrame;
        TRAMEOUT[1] = 'C';
        TRAMEOUT[2] = 'M';
        TRAMEOUT[3] = 'A'; //DD cad dire carte memoire
        TRAMEOUT[4] = 'U';
        TRAMEOUT[5] = 'D';
        TRAMEOUT[6] = 'D';
        TRAMEOUT[7] = 'B';
        TRAMEENVOIRS485DONNEES(); // ENVOI A LAU
        TRAMERECEPTIONRS485DONNEES(18); // SE MET ENRECPETION POUR ATTENDRE LA REPONSE
finwhiBN:
        controle_CRC();



    } while (TRAMEIN[2] != '$' && TRAMEIN[3] != '$');
    DX10 = 1;
    DX20 = 1;


}

void SDR(void) {
    char gigi;


rec:
    MESURE_DES_TENSIONS();
    TEMPO(3);
    if (VDD1 > 2) {
        ENVOIE_DES_TRAMES(1, 301, &INSTRUCTION); //envoyer ordre formatage carte RI1
        TEMPO(3);
    }
    if (VDD2 > 2) {
        ENVOIE_DES_TRAMES(2, 301, &INSTRUCTION); //envoyer ordre formatage carte RI1
        TEMPO(3);
    }
    if (VDD3 > 2) {
        ENVOIE_DES_TRAMES(3, 301, &INSTRUCTION); //envoyer ordre formatage carte RI1
        TEMPO(3);
    }
    if (VDD4 > 2) {
        ENVOIE_DES_TRAMES(4, 301, &INSTRUCTION); //envoyer ordre formatage carte RI1
        TEMPO(3);
    }
    if (VDD5 > 2) {
        ENVOIE_DES_TRAMES(5, 301, &INSTRUCTION); //envoyer ordre formatage carte RI1
        TEMPO(3);
    }

    //ON RECOIT ERR00 DE CHAQUE CARTE MAIS ON N'EN PREND PAS COMPTE

    TRAMEIN[1] = '0';
    do {
        DX10 = !DX10;
        DX20 = !DX20;

        if (TRAMEIN[1] == '$')
            goto finwhi;
        //TEMPO(1);
        delay_ms(100);
        envoyer_TRAME_PICSD(0xA1); //START  RECEVOIR POUR PROGRAMMER A1
        //TEMPO(1);
        delay_ms(100);
        //delay_ms(10);
        envoyer_TRAME_PICSD(0x00); //RECEP faire Nx jusqua $$$
        if (TRAMEIN[4] == 50) {
            delay_ms(10);
            delay_ms(10);
        }

finwhi:
        delay_ms(10);


        //CREER LE CAPTEUR ! DANS TRAMEIN //si CRC=F22222 (TRAMEIN[86]=F ou E) debitmetre pas present sinon il existe

        if (TRAMEIN[86] == 'F')
            PRESENCEDEBITSD = 0x00;
        if (TRAMEIN[86] == 'E')
            PRESENCEDEBITSD = 0xFF;


        for (gigi = 0; gigi < 93; gigi++) //-#021..... ce que lon recoit
        {

            TRAMEIN[gigi] = TRAMEIN[gigi + 1];

        }




        TRANSFORMER_TRAME_MEM_EN_COM();

        TRAMEOUT[0] = cDebutTrame;
        TRAMEOUT[1] = 'I';
        TRAMEOUT[2] = 'O';
        TRAMEOUT[3] = 'D'; //DD cad dire carte memoire
        TRAMEOUT[4] = 'D';
        TRAMEOUT[5] = 'Z';
        TRAMEOUT[6] = 'C';
        TRAMEOUT[7] = 'R';


        for (gigi = 0; gigi < 93; gigi++) //-#021..... ce que lon recoit
        {
            TRAMEIN[gigi] = TRAMEOUT[gigi];
        }


        TEMPO(1);

        if ((TRAMEIN[9] == '0') || (TRAMEIN[9] == '1')) //001 - 019
        {
            if (VDD1 > 2)
                RECUPERATION_DES_TRAMES(&TRAMEOUT); //ENVOIE VIRTUEL DE COMMANDE DE IO (EN REALITE CARTE SD)
        }

        if ((TRAMEIN[9] == '2') || (TRAMEIN[9] == '3')) //021 - 039
        {
            if (VDD2 > 2)
                RECUPERATION_DES_TRAMES(&TRAMEOUT); //ENVOIE VIRTUEL DE COMMANDE DE IO (EN REALITE CARTE SD)
        }

        if ((TRAMEIN[9] == '4') || (TRAMEIN[9] == '5')) //041-059
        {
            if (VDD3 > 2)
                RECUPERATION_DES_TRAMES(&TRAMEOUT); //ENVOIE VIRTUEL DE COMMANDE DE IO (EN REALITE CARTE SD)
        }

        if ((TRAMEIN[9] == '6') || (TRAMEIN[9] == '7')) //061-079
        {
            if (VDD4 > 2)
                RECUPERATION_DES_TRAMES(&TRAMEOUT); //ENVOIE VIRTUEL DE COMMANDE DE IO (EN REALITE CARTE SD)
        }

        if ((TRAMEIN[9] == '8') || (TRAMEIN[9] == '9')) //081-099
        {
            if (VDD5 > 2)
                RECUPERATION_DES_TRAMES(&TRAMEOUT); //ENVOIE VIRTUEL DE COMMANDE DE IO (EN REALITE CARTE SD)
        }




        if ((TRAMEIN[9] == '2') && (TRAMEIN[10] == '0')) //20
        {
            if (VDD1 > 2)
                RECUPERATION_DES_TRAMES(&TRAMEOUT); //ENVOIE VIRTUEL DE COMMANDE DE IO (EN REALITE CARTE SD)
        }

        if ((TRAMEIN[9] == '3') && (TRAMEIN[10] == '0')) //30
        {
            if (VDD2 > 2)
                RECUPERATION_DES_TRAMES(&TRAMEOUT); //ENVOIE VIRTUEL DE COMMANDE DE IO (EN REALITE CARTE SD)
        }

        if ((TRAMEIN[9] == '4') && (TRAMEIN[10] == '0')) //40
        {
            if (VDD3 > 2)
                RECUPERATION_DES_TRAMES(&TRAMEOUT); //ENVOIE VIRTUEL DE COMMANDE DE IO (EN REALITE CARTE SD)
        }

        if ((TRAMEIN[9] == '5') && (TRAMEIN[10] == '0')) //50
        {
            if (VDD4 > 2)
                RECUPERATION_DES_TRAMES(&TRAMEOUT); //ENVOIE VIRTUEL DE COMMANDE DE IO (EN REALITE CARTE SD)
        }

        if (TRAMEIN[8] == '1') //100
        {
            if (VDD5 > 2)
                RECUPERATION_DES_TRAMES(&TRAMEOUT); //ENVOIE VIRTUEL DE COMMANDE DE IO (EN REALITE CARTE SD)
        }





    } while (TRAMEIN[1] != '$' && TRAMEIN[8] != '$' && TRAMEIN[9] != '$');
    DX10 = 1;
    DX20 = 1;
    PRESENCEDEBITSD = 0x00;
    ///FIN RECETION FICHIER

}

void envoyer_TRAME_PICSD(char SENS) //ENVOIE SIMPLEMENT  #..........* une trame memoire a la SD ou recoit une trame memoire de la SD
{
    unsigned char u, z;
    u = -1;

    //////////////////////// TRANSMETTRE A SD VIA PIC2



    init_spi();
    spi_low();

    delay_ms(1);
    //sd_reset();



    PORTDbits.RD7 = 1;
    PORTDbits.RD7 = 0; // Enable Chip Select
    u = -1;
    creer_trame_horloge();
    z = SPI_WriteByte('-'); // ON active la TRANS.
    z = SPI_WriteByte('-'); // ON active la TRANS
    z = SPI_WriteByte(HORLOGE[6]); // j
    z = SPI_WriteByte(HORLOGE[7]); // j
    z = SPI_WriteByte(HORLOGE[8]); // m
    z = SPI_WriteByte(HORLOGE[9]); // m
    z = SPI_WriteByte(HORLOGE[10]); // a
    z = SPI_WriteByte(HORLOGE[11]); // a
    z = SPI_WriteByte(HORLOGE[0]); // h
    z = SPI_WriteByte(HORLOGE[1]); // h
    z = SPI_WriteByte(HORLOGE[2]); // m
    z = SPI_WriteByte(HORLOGE[3]); // m


    if (SENS == 0xA1) //DEBUT DE TRANSMIT
    {


        z = SPI_WriteByte('S'); // ordre
        z = SPI_WriteByte('R'); // ordre



        //	do
        //	{
        u++;
        TRAMEIN[u] = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte('R'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte(cFinTrame); // ON TRANSMET





        //	}while(TRAMEOUT[u]!='*');



    }


    if (SENS == 0xA9) //DEBUT DE TRANSMIT BLOC NOTES
    {


        z = SPI_WriteByte('S'); // ordre
        z = SPI_WriteByte('R'); // ordre



        //	do
        //	{
        u++;
        TRAMEIN[u] = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte('L'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte(cFinTrame); // ON TRANSMET





        //	}while(TRAMEOUT[u]!='*');



    }


    if (SENS == 0xA2) //DEBUT DE TRANSMIT DEMARRAGE
    {


        z = SPI_WriteByte('S'); // ordre
        z = SPI_WriteByte('W'); // ordre



        //	do
        //	{
        u++;
        TRAMEIN[u] = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte('W'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte(cFinTrame); // ON TRANSMET





        //	}while(TRAMEOUT[u]!='*');



    }


    if (SENS == 0xA3) //DEBUT DE TRANSMIT CYCLIQUE
    {


        z = SPI_WriteByte('S'); // ordre
        z = SPI_WriteByte('W'); // ordre



        //	do
        //	{
        u++;
        TRAMEIN[u] = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte('V'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte(cFinTrame); // ON TRANSMET





        //	}while(TRAMEOUT[u]!='*');



    }


    if (SENS == 0xA4) //DEBUT DE TRANSMIT MINITEL
    {


        z = SPI_WriteByte('S'); // ordre
        z = SPI_WriteByte('W'); // ordre



        //	do
        //	{
        u++;
        TRAMEIN[u] = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte('M'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte(cFinTrame); // ON TRANSMET





        //	}while(TRAMEOUT[u]!='*');



    }

    if (SENS == 0xA5) //DEBUT DE TRANSMIT MINITEL
    {


        z = SPI_WriteByte('S'); // ordre
        z = SPI_WriteByte('W'); // ordre



        //	do
        //	{
        u++;
        TRAMEIN[u] = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte('N'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte(cFinTrame); // ON TRANSMET





        //	}while(TRAMEOUT[u]!='*');



    }

    if (SENS == 0xA7) //DEBUT DE TRANSMIT MINITEL
    {


        z = SPI_WriteByte('S'); // ordre
        z = SPI_WriteByte('W'); // ordre



        //	do
        //	{
        u++;
        TRAMEIN[u] = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte('%'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte('B'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte(cFinTrame); // ON TRANSMET





        //	}while(TRAMEOUT[u]!='*');



    }










    if (SENS == 0xFF) //FIN DE TRANSMIT.
    {


        z = SPI_WriteByte('F'); // ordre
        z = SPI_WriteByte('I'); // ordre



        //	do
        //	{
        u++;
        TRAMEIN[u] = SPI_WriteByte('$'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte('$'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte('$'); // ON TRANSMET
        u++;
        TRAMEIN[u] = SPI_WriteByte(cFinTrame); // ON TRANSMET





        //	}while(TRAMEOUT[u]!=cFinTrame);



    }

    if (SENS == 0xEE) {


        z = SPI_WriteByte('S'); // ordre
        z = SPI_WriteByte('V'); // ordre



        do {
            u++;

            TRAMEIN[u] = SPI_WriteByte(TRAMEOUT[u]); // ON TRANSMET

        } while (TRAMEOUT[u] != cFinTrame);



    }

    if (SENS == 0x00)
 {
        z = SPI_WriteByte('R'); // ordre
        z = SPI_WriteByte('C'); // ordre

        do {
            u++;

            //	delay_ms(1);
            //	if (u==0)
            //	{
            //	z=SPI_WriteByte('!'); // ON RECOIT le premier pas a prendre en compte "-#........*"
            //	}
            //	else
            //	{
            TRAMEIN[u] = SPI_WriteByte('!'); // ON RECOIT
            //	}


        } while (TRAMEIN[u] != cFinTrame);

    }

    /////////////////////// FIN TRANS. VERS SD VIA PIC2

}

void reglerhorlogeautres(void) {


    MESURE_DES_TENSIONS();

    if ((VDD1 > 2)&&(R1NOREP < 12)) {
        creer_trame_horloge(); //LIRE EEPPROM VERIFICATION
        ENVOIE_DES_TRAMES(1, 305, &HORLOGE);
    }
    if ((VDD2 > 2)&&(R2NOREP < 12)) // SI CARTE PAS PROBLEME
    {
        creer_trame_horloge();
        ENVOIE_DES_TRAMES(2, 305, &HORLOGE);
    }
    if ((VDD3 > 2)&&(R3NOREP < 12)) {
        creer_trame_horloge();
        ENVOIE_DES_TRAMES(3, 305, &HORLOGE);
    }
    if ((VDD4 > 2)&&(R4NOREP < 12)) {
        creer_trame_horloge();
        ENVOIE_DES_TRAMES(4, 305, &HORLOGE);
    }
    if ((VDD5 > 2)&&(R5NOREP < 12)) {
        creer_trame_horloge();
        ENVOIE_DES_TRAMES(5, 305, &HORLOGE);
    }


}

void stopIO(void) {

    if (ETAT_IO == 0xFF) //IO ON IL FAUT LETEINDRE POUR NE PLUS AUTORISER LIO A PARLER
    {
        //DRS485_IO=1; //ON SIGNALE A LA CARTE IO DE SE CALMER
        DRS485_IO = 1; //ON CONSIDERE QUE CETAIT A 0 POUR FAIRE UN FRONT MONTANT
        ETAT_IO = 0x00;
    }

}

void pic_eeprom_write(unsigned char Address, unsigned char contenu) {
    //Ecriture en EEPROM


    EEDATA = contenu; //            "
    EECON1bits.EEPGD = 0; //            "
    EECON1bits.CFGS = 0; //            "
    EECON1bits.WREN = 1; //            "

    EEADR = Address; //Configuration de l'�criture
    EECON2 = 0x55; //Sequence d'�criture
    EECON2 = 0xAA; //         "
    //WR=1
    //asm("BSF EECON1, WR");
    _asm
    BSF 0xfa6, 0x01, ACCESS // on met WR � 1pour lancer l'�criture
    _endasm

            //EECON1=EECON1|0x10;
            //EECON1bits.WR=1;
    while (PIR2bits.EEIF == 0) //Attente de la fin de l'�criture
    {
    }
    PIR2bits.EEIF = 0;
    EECON1bits.WREN = 0; //Remise en �tat du proc�d�

}

char pic_eeprom_read(unsigned char Address) {
    // Lecture en EEPROM
    EEADR = Address;
    EECON1bits.EEPGD = 0; // Configuration de la lecture
    EECON1bits.CFGS = 0;
    EECON1 = EECON1 | 0x01; //rd � 1
    /*	_asm
        BSF 0xfa6, 0x01,ACCESS // on met WR � 1pour lancer l'�criture
        _endasm
     */

    return EEDATA; //Donn�e M�moris�e
}

void controleheureRESET(void) {
    char resetheurefixe;
    //INIT mem AA= 0XDD et BB= jour en valeur deci lors du reglage de l'heure !!!!!
    creer_trame_horloge();


    //if ((pic_eeprom_read(0xBB)==1)&&(resetheurefixe==1)) //si on est le 1 et un reset a ete fait le 1
    //pic_eeprom_write(0xAA,0XDD); // ON REMET DD pour ne pas refaire de reset

    resetheurefixe = pic_eeprom_read(0xAA); //voir la valeur

    if (resetheurefixe != 0xDD) //SI AUCUN RESET A ETE FAIT
    {

        resetheurefixe = 10 * (HORLOGE[6] - 48) + HORLOGE[7] - 48; //LIT LE JOUR

        if (resetheurefixe == pic_eeprom_read(0xBB)) //si le jour est egal au reset precedent
        {
            pic_eeprom_write(0xAA, 0XDD); // ON REMET DD pour ne pas refaire de reset
            goto dejafait;

        }


        if (HORLOGE[0] >= '1') //>22h10 ON FAIT UN RESET attention il faut aussi remplir les conditions resetheurefixe!=0xDD debloqu�e si aucun reset n'a ete fait le meme jours ou si l'heure autorisation reset est depass�e
        {
            if (HORLOGE[1] >= '2') {

                //	if (HORLOGE[2]>='2')
                //	{
                resetheurefixe = 0xDD; //INDICATION RESET FAIT
                pic_eeprom_write(0xAA, resetheurefixe);
                TEMPO(1);
                resetheurefixe = 10 * (HORLOGE[6] - 48) + HORLOGE[7] - 48; //ON ECRIT LE JOUR
                pic_eeprom_write(0xBB, resetheurefixe);
                resetheurefixe = 0xDD; //INDICATION RESET FAIT
                TEMPO(1);
                RESETCMS = 1; //ON REINIT


                //	}

            }
        }
    }

dejafait:

    if (resetheurefixe == 0xDD) //SI UN RESET A ETE FAIT ON REAUTORISE UN RESET A PARTIR DE CETTE HEURE
    {

        if (HORLOGE[0] >= '1') //>23h30 ON permet de refaire UN RESET mais
        {

            if (HORLOGE[1] >= '3') //>22h40 ON FAIT UN RESET
            {

                //										if (HORLOGE[2]>='3')
                //										{


                resetheurefixe = 0x00; //INDICATION RESET PEUT ETRE REFAIT
                pic_eeprom_write(0xAA, resetheurefixe);
                //										}
            }
        }
    }
}



